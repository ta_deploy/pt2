<?php

namespace App\Http\Middleware;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\ReportController;
use DB;
use Closure;

class Auth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      $rememberToken = $request->cookie('presistent-token');

      if ($rememberToken) {
        $SQL = '
        SELECT u.id_user, u.password, u.id_karyawan, k.nama, k.atasan_1, k.sto, u.pt2_level, u.psb_remember_token, u.maintenance_level, k.mitra_amija as mitra, u.level, k.Witel_New
        FROM user u
        LEFT JOIN 1_2_employee k ON u.id_karyawan = k.nik
        LEFT JOIN mitra_amija ma ON ma.mitra_amija = k.mitra_amija
        WHERE psb_remember_token = "'.$rememberToken.'"
        GROUP BY u.id_user
        ';
        $user = DB::select($SQL);
        if (count($user)>0) {
          Session::put('auth', $user[0]);
          return $next($request);
        }
      }

      Session::put('auth-originalUrl', $request->fullUrl());
      if ($request->ajax()) {
        return response('UNAUTHORIZED', 401);
      } else {
        return redirect('login');
      }
    }
  }
