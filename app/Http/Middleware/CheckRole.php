<?php

namespace App\Http\Middleware;

use Closure;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\DA\LoginModel;


class CheckRole
{
    public function handle($request, Closure $next, ...$roles)
    {
        $valid_user = ['2' => 'admin', '0' => 'teknisi', '3' => 'monitor', '4' => 'booking', '5' => 'superadmin', '6' => 'monitor_marina', '7' => 'hero', '8' => 'waspang_jointer', '9' => 'TL'];
        $level = Session('auth')->pt2_level;
        if(empty($roles)){
            return redirect('login');
        }

        if (in_array($valid_user[$level], $roles) || $roles[0] == 'all') {
            $check_log_absen = LoginModel::check_absensi();

            if(in_array($level, [0]) && (empty($check_log_absen) || is_null($check_log_absen->approve_by) ) ){
                return redirect('/absensi');
            }

            return $next($request);
        }

        Session::put('auth-originalUrl', $request->fullUrl());
        if ($request->ajax()) {
            return response('UNAUTHORIZED', 401);
        } else {
            Session::put('auth-originalUrl', '');
            return redirect('login');
        }
    }

}
