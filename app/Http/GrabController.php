<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use DB;

class GrabController extends Controller
{
  public function index()
  {
    return view('grab.index');
  }
  public function sync(Request $req)
  {
    //processing
    $link = "https://starclick.telkom.co.id/backend/public/api/tracking?_dc=1480967332984&ScNoss=true&SearchText=".$req->sc."&Field=ORDER_ID&Fieldstatus=&Fieldwitel=&StartDate=&EndDate=&page=1&start=0&limit=1";
      $result = json_decode(@file_get_contents($link));
      echo $link."<br />";
      print_r(count($result->data));

      $order = $result->data;
      foreach ($order as $data){
        if ($data->ORDER_ID == "####"){
          return redirect('/customGrab')->with('alerts', [
            ['type' => 'warning', 'text' => '<strong>gagal</strong> ORDER ID tidak boleh Angka!']
          ]);
        }

        $get_where = DB::table("Data_Pelanggan_Starclick")->where("orderId", $data->ORDER_ID)->first();
        
        $orderDate = "";
        $orderDatePs = "";
        if (!empty($data->ORDER_DATE_PS)) $orderDatePs = $data->ORDER_DATE_PS;
        if (!empty($data->ORDER_DATE)) $orderDate = $data->ORDER_DATE;
        // dd($get_where)
        // dd($data);

        $KCONTACT = str_replace("\\", '', $data->KCONTACT);

        if (!empty($get_where->orderId)){ 
          echo $data->ORDER_ID." UPDATED\n";
          $SQL = "
          UPDATE
            Data_Pelanggan_Starclick
          SET
            orderId='".$data->ORDER_ID."',
            orderKontak='".$data->PHONE_NO."',
            orderDate='".$orderDate."',
            orderDatePs='".$orderDatePs."',
            orderStatus='".$data->STATUS_RESUME."',
            orderStatusId='".$data->STATUS_CODE_SC."',
            orderNcli='".$data->NCLI."',
            orderPaket='".$data->PACKAGE_NAME."',
            lat = '".$data->GPS_LATITUDE."',
            lon = '".$data->GPS_LONGITUDE."',
            orderAddr = '".$data->INS_ADDRESS."',
            orderCity = '".$data->CUSTOMER_ADDR."',
            orderNotel = '".$data->POTS."',
            ndemSpeedy = '".$data->SPEEDY."',
            ndemPots = '".$data->POTS."',
            orderPaketID = '".$data->ODP_ID."',
            kcontact = '".$KCONTACT."',
            username = '".$data->USERNAME."',
            alproname = '".$data->LOC_ID."',
            tnNumber = '".$data->TN_NUMBER."',
            reservePort = '".$data->ODP_ID."',
            jenisPsb = '".$data->JENISPSB."',
            sto = '".$data->XS2."',
            orderName = '".$data->CUSTOMER_NAME."',
            reserveTn = '".$data->RNUM."'
          WHERE
            orderId='".$data->ORDER_ID."'
          ";
          // echo $SQL."\n";
          $update = DB::statement($SQL);
          return redirect('/customGrab')->with('alerts', [
            ['type' => 'success', 'text' => '<strong>SUKSES</strong> merubah SC Lama!']
          ]);
        } else {
          echo $data->ORDER_ID." INSERTED\n";
          $SQL = "
          INSERT IGNORE INTO
            Data_Pelanggan_Starclick
          (orderId,orderName,orderAddr,orderNotel,orderKontak,orderDate,orderDatePs,orderCity,orderStatus,orderStatusId,orderNcli,ndemSpeedy,ndemPots,orderPaketID,kcontact,username,alproName,tnNumber,reserveTn,reservePort,jenisPsb,sto,lat,lon)
          VALUES
          ('".$data->ORDER_ID."','".str_replace("'","",$data->CUSTOMER_NAME)."',
          '".str_replace("'","",$data->INS_ADDRESS)."','".$data->POTS."','".$data->PHONE_NO."','".$orderDate."','".$orderDatePs."','".str_replace("'","",$data->CUSTOMER_ADDR)."','".$data->STATUS_RESUME."',
          '".$data->STATUS_CODE_SC."','".$data->NCLI."','".$data->SPEEDY."','".$data->POTS."','".$data->ODP_ID."','".$KCONTACT."','".$data->USERNAME."','".$data->LOC_ID."','".$data->POTS."',
          '".$data->RNUM."','".$data->ODP_ID."','".$data->JENISPSB."','".$data->XS2."','".$data->GPS_LATITUDE."','".$data->GPS_LONGITUDE."')
          ";
          $insert = DB::statement($SQL);
          echo $SQL."\n";
          return redirect('/customGrab')->with('alerts', [
            ['type' => 'success', 'text' => '<strong>SUKSES</strong> menambah SC baru!']
          ]);
        }
      }

    //to view
    //return view('grab.index');
    return redirect('/customGrab')->with('alerts', [
      ['type' => 'warning', 'text' => '<strong>Gagal</strong> SC tidak ditemukan dalam kurun waktu 30 hari kebelakang!']
    ]);
  }
  public function a2s_absensi(){
    $this->a2s_absen(1);
    $this->a2s_absen(2);
  }
  public function a2s_absen($id){
    date_default_timezone_set('Asia/Makassar');
    $param = "end_date=".date("d")."%2F".date("m")."%2F".date("Y")."&p_regional_ta=%25&p_witel=44&alpro=".$id."&done=Done";
    $ch = curl_init();
    $url = 'http://apps.telkomakses.co.id/assurance/rta_detil_hadir.php?p_witel=44&start_date=&end_date='.date("d")."%2F".date("m")."%2F".date("Y").'&alpro='.$id.'&lama=0';
    echo $url;
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/32.0.1700.107 Chrome/32.0.1700.107 Safari/537.36');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $c = curl_exec($ch);
    //echo $c;
    $columns = array(
            1 =>
            'reg',
            'witel',
            'laborcode',
            'teknisi',
            'nik',
            'supervisor',
            'jenis_teknisi',
            'no_hp',
            'workzone',
            'jadwal',
            'hadir',
            'tgl',
            'div'
        );
    $dom = @\DOMDocument::loadHTML(trim($c));
    $tgl = date('Y-m-d');
    $div = $id;
    $table = $dom->getElementsByTagName('table')->item(1);
    $rows = $table->getElementsByTagName('tr');
    $result = array();
    for ($i = 1, $count = $rows->length; $i < $count; $i++)
    {
        $cells = $rows->item($i)->getElementsByTagName('td');
        $data = array();
        for ($j = 1, $jcount = count($columns); $j <= $jcount; $j++)
        {
          if($j == 12){
            $data[$columns[$j]] = $tgl;
          }else if($j == 13){
            $data[$columns[$j]] = $div;
          }else{
            $td = $cells->item($j);
            $data[$columns[$j]] =  $td->nodeValue;
          }
        }

        $result[] = $data;
    }
    //return $result;
    DB::table('a2s_kehadiran')->where('tgl', $tgl)->where('div', $div)->delete();
    DB::table('a2s_kehadiran')->insert($result);
  }
  public static function odp(){
    ini_set('max_execution_time', 60000);
    //DB::table('1_0_master_odp')->truncate();
    GrabController::odp_regional(6);

  }
  public static function odp_regional($reg){
    ini_set('max_execution_time', 60000);
    $filename = "ftp://UserFTP1:telkom135@10.65.10.238/SupportReport/Harian/odpe2e/".date('Ym')."04/odp_compliance_".date('Ym')."04_Regional 6.xlsx";
    //$handle = fopen($filename, "r");
    //$data_to_insert = array();
    $content = file_get_contents($filename);

    var_dump($content);
    dd();

    while(!feof($handle)){
      $data = explode(";", fgets($handle));
      if (count($data)>1){
          $data_to_insert[] = [
              'NOSS_ID'         => $data[0],
              'ODP_INDEX'       => $data[1],
              'ODP_NAME'        => $data[2],
              'LATITUDE'        => $data[3],
              'LONGITUDE'       => $data[4],
              'CLUSNAME'        => $data[5],
              'CLUSTERSTATUS'   => $data[6],
              'AVAI'            => $data[7],
              'USED'            => $data[8],
              'RSV'             => $data[9],
              'RSK'             => $data[10],
              'IS_TOTAL'        => $data[11],
              'REGIONAL'        => $data[12],
              'WITEL'           => $data[13],
              'DATEL'           => $data[14],
              'STO'             => $data[15],
              'STO_DESC'        => $data[16],
              'ODP_INFO'        => $data[17],
              'UPDATE_DATE'     => $data[18]
            ];

       }
    }
    $records = array_chunk($data_to_insert, 1000);
    $regional = "Regional ".$reg;
    DB::table('1_0_master_odp')->where('REGIONAL', $regional)->delete();
    foreach ($records as $batch) {
          try {
            DB::table('1_0_master_odp')
            ->insert($batch);
          }
          catch (\Exception $e) {

          }
    }

  }
  public function scDetail($id){
    $link = "https://starclick.telkom.co.id/backend/public/api/tracking?_dc=1480967332984&ScNoss=true&SearchText=".$id."&Field=ORDER_ID&Fieldstatus=&Fieldwitel=&StartDate=&EndDate=&page=1&start=0&limit=1";
    $result = json_decode(@file_get_contents($link));
    return $order = $result->data;
  }

  public static function odpRegional(){
    ini_set('max_execution_time', 60000);
    $filename = "ftp://UserFTP1:telkom135@10.65.10.238/SupportReport/Harian/ODPOkupansi/".date('Y')."/".date('Ym')."/REPORT_ODP_TREG6_".date('Ymd').".txt";
    $handle = fopen($filename, "r");
    $data_to_insert = array();
    while(!feof($handle)){
       $data = explode(";", fgets($handle));
      if (count($data)>1){
          $data_to_insert[] = [
              'NOSS_ID'          => $data[0],
              'ODP_INDEX'        => $data[1],
              'ODP_NAME'         => $data[2],
              'LATITUDE'         => $data[3],
              'LONGITUDE'        => $data[4],
              'CLUSNAME'         => $data[5],
              'CLUSTERSTATUS'    => $data[6],
              'AVAI'             => $data[7],
              'USED'             => $data[8],  
              'RSV'              => $data[9],
              'RSK'              => $data[10],
              'IS_TOTAL'         => $data[11],
              'REGIONAL'         => $data[12],
              'WITEL'            => $data[13],
              'DATEL'            => $data[14],
              'STO'              => $data[15],
              'STO_DESC'         => $data[16],
              'ODP_INFO'         => $data[17],
              'UPDATE_DATE'      => $data[18]
            ];
       }
    }
    $records = array_chunk($data_to_insert, 1000);
    $regional = "Regional 6";
    // DB::table('1_0_master_odp')->where('REGIONAL', $regional)->delete();
    DB::table('1_0_master_odp')->truncate();
    foreach ($records as $batch) {
      try {
              DB::table('1_0_master_odp')
               ->insert($batch);
          }
             catch (\Exception $e) {
            
          }  
    }
    // DB::table('1_0_master_odp')->where('REGIONAL', 'REGIONAL')->delete();
  }

  public function grab_kpro_val(){
    // perform header
    date_default_timezone_set('Asia/Makassar'); 
    ini_set('max_execution_time', 60000);
    $ch = curl_init();

    $headers[] = "Accept: */*";
    $headers[] = "Connection: Keep-Alive";
    $headers[] = "Content-type: application/x-www-form-urlencoded;charset=UTF-8";
    curl_setopt($ch, CURLOPT_HTTPHEADER,  $headers);
    curl_setopt($ch, CURLOPT_HEADER,  0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);     

    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_URL, 'http://10.62.170.27/login.php');
    curl_setopt($ch, CURLOPT_POSTFIELDS,"usr=850056&pwd=KalselHibat1");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie-name.txt');  
    curl_setopt($ch, CURLOPT_COOKIEFILE, 'cookie-name.txt');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    $hasil=curl_exec($ch);

    echo $hasil;

    $postdata = http_build_query(
        array(
            "sql" => "SELECT distinct od.ORDER_ID, JENISPSB, REGIONAL, WITEL, DATEL, STO, LOC_ID, NCLI, POTS, SPEEDY, TYPE_TRANS, TYPE_LAYANAN, CUSTOMER_NAME, CONTACT_HP, INS_ADDRESS, GPS_LONGITUDE, GPS_LATITUDE, KCONTACT, STATUS_RESUME, STATUS_MESSAGE, to_char(order_date,'yyyy-mm-dd hh24:mi:ss') as order_date, to_char(last_updated_date,'yyyy-mm-dd hh24:mi:ss') as last_updated_date, STATUS_VOICE, STATUS_INET, STATUS_ONU, STATUS_REDAMAN, OLT_RX, ONU_RX, SNR_UP, SNR_DOWN, UPLOAD, DOWNLOAD, LAST_PROGRAM, CLID, to_char(last_start,'dd-mm-yyyy hh24:mi:ss') last_start, to_char(last_view,'dd-mm-yyyy hh24:mi:ss') last_view , to_char(ukur_time,'dd-mm-yyyy hh24:mi:ss') ukur_time, TINDAK_LANJUT, ISI_COMMENT, USER_ID_TL, to_char(TL_DATE,'dd-mm-yyyy hh24:mi:ss') as tgl_comment, to_char(SCHEDULE_LABOR,'dd-mm-yyyy hh24:mi:ss') as SCHEDULE_LABOR, amcrew, wo_parent, status_wo, status_task, desc_task, tek.device_id
from KPRO_DETAIL_ORDER od LEFT OUTER JOIN kpro_hasil_ukur uk on od.order_id=uk.order_id 
LEFT OUTER JOIN (select order_id, SCHEDULE_LABOR, amcrew, wo_parent, status_wo, status_task, desc_task,device_id from kpro_detail_wfm where wo_parent is not null) tek on od.order_id=tek.order_id where status_resume = 'Process OSS (Provision Issued)' and (tindak_lanjut like 'Management Janji' or tindak_lanjut is null)  and  regional='6' and  1=1  and  1=1  and  type_trans='NEW SALES' and  1=1  and  1=1  and  1=1  order by order_date",
            "tombol" => "Download to Excel"
        )
    );

    curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
    curl_setopt($ch, CURLOPT_URL, 'http://10.62.170.27/downloadreport.php');

    $context=curl_exec($ch);
    curl_close ($ch);
  
    //performe convert to variable
    $dom = @\DOMDocument::loadHTML(trim($context));
    $columns = array("no", "regional", "witel", "datel", "sto", "order_id", "type_transaksi", "jenis_layanan", "alpro", "ncli", "pots","speedy", "status_resume", "status_message", "order_date", "last_update_sts", "nama_cust", "no_hp", "alamat", "kcontact", "lng", "lat","wfm_id", "status_wfm", "desk_task", "status_task", "tgl_install", "amcrew", "teknisi1", "hp_teknisi1", "teknisi2", "hp_teknisi2", "tinjut","ket","user","tgl_tinjut");

    $table = $dom->getElementsByTagName('table')->item(0);
    $rows = $table->getElementsByTagName('tr');
    $result = array();
    $jam    = date('G');

    for ($i = 1, $count = $rows->length; $i < $count; $i++)
    {
        $cells = $rows->item($i)->getElementsByTagName('td');
        $data = array();
        for ($j = 0, $jcount = count($columns); $j < $jcount; $j++)
        {
            $td = $cells->item($j);
            $data[$columns[$j]] =  $td->nodeValue;
        }
        $data['sts'] = "val";
        $data['jam'] = $jam;
        $result[] = $data;
    }
  
    DB::table("kpro_pi")->insert($result);
  }

  public function grab_kpro_tot(){
    // perform header
    date_default_timezone_set('Asia/Makassar');
    ini_set('max_execution_time', 60000);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_URL, 'http://10.62.170.27/login.php');
    curl_setopt($ch, CURLOPT_POSTFIELDS,"usr=850056&pwd=KalselHibat1");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie-name.txt');  
    curl_setopt($ch, CURLOPT_COOKIEFILE, 'cookie-name.txt');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    $hasil=curl_exec($ch);

    echo $hasil;

    $postdata = http_build_query(
        array(
            "sql" => "SELECT distinct od.ORDER_ID, JENISPSB, REGIONAL, WITEL, DATEL, STO, LOC_ID, NCLI, POTS, SPEEDY, TYPE_TRANS, TYPE_LAYANAN, CUSTOMER_NAME, CONTACT_HP, INS_ADDRESS, GPS_LONGITUDE, GPS_LATITUDE, KCONTACT, STATUS_RESUME, STATUS_MESSAGE, to_char(order_date,'yyyy-mm-dd hh24:mi:ss') as order_date, to_char(last_updated_date,'yyyy-mm-dd hh24:mi:ss') as last_updated_date, STATUS_VOICE, STATUS_INET, STATUS_ONU, STATUS_REDAMAN, OLT_RX, ONU_RX, SNR_UP, SNR_DOWN, UPLOAD, DOWNLOAD, LAST_PROGRAM, CLID, to_char(last_start,'dd-mm-yyyy hh24:mi:ss') last_start, to_char(last_view,'dd-mm-yyyy hh24:mi:ss') last_view , to_char(ukur_time,'dd-mm-yyyy hh24:mi:ss') ukur_time, TINDAK_LANJUT, ISI_COMMENT, USER_ID_TL, to_char(TL_DATE,'dd-mm-yyyy hh24:mi:ss') as tgl_comment, to_char(SCHEDULE_LABOR,'dd-mm-yyyy hh24:mi:ss') as SCHEDULE_LABOR, amcrew, wo_parent, status_wo, status_task, desc_task, tek.device_id
            from KPRO_DETAIL_ORDER od LEFT OUTER JOIN kpro_hasil_ukur uk on od.order_id=uk.order_id 
            LEFT OUTER JOIN (select order_id, SCHEDULE_LABOR, amcrew, wo_parent, status_wo, status_task, desc_task,device_id from kpro_detail_wfm where wo_parent is not null) tek on od.order_id=tek.order_id where ((status_resume not like 'Completed (PS)' and status_resume not like 'UN%' and lower(status_resume) not like ('%cancel%') and lower(status_resume) not like ('%revoke%')) or (status_resume like 'Completed (PS)' and TO_CHAR(last_updated_date,'yyyy-mm-dd') = TO_CHAR(CURRENT_DATE,'yyyy-mm-dd')))  and  regional='6' and  witel='BANJARMASIN' and  1=1  and  1=1  and  1=1  and  1=1  and  1=1  order by order_date",
            "tombol" => "Download to Excel"
        )
    );

    curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
    curl_setopt($ch, CURLOPT_URL, 'http://10.62.170.27/downloadreport.php');

    $context=curl_exec($ch);
    curl_close ($ch);
  
    //performe convert to variable
    $dom = @\DOMDocument::loadHTML(trim($context));
    $columns = array("no", "regional", "witel", "datel", "sto", "order_id", "type_transaksi", "jenis_layanan", "alpro", "ncli", "pots","speedy", "status_resume", "status_message", "order_date", "last_update_sts", "nama_cust", "no_hp", "alamat", "kcontact", "lng", "lat","wfm_id", "status_wfm", "desk_task", "status_task", "tgl_install", "amcrew", "teknisi1", "hp_teknisi1", "teknisi2", "hp_teknisi2", "tinjut","ket","user","tgl_tinjut");

    $table = $dom->getElementsByTagName('table')->item(0);
    $rows = $table->getElementsByTagName('tr');
    $result = array();
    $jam    = date('G');

    for ($i = 1, $count = $rows->length; $i < $count; $i++)
    {
        $cells = $rows->item($i)->getElementsByTagName('td');
        $data = array();
        for ($j = 0, $jcount = count($columns); $j < $jcount; $j++)
        {
            $td = $cells->item($j);
            $data[$columns[$j]] =  $td->nodeValue;
        }
        $data['sts'] = "tot";
        $data['jam'] = $jam;
        $result[] = $data;
    }
  
    DB::table("kpro_pi")->insert($result);
  }

  public function grabWifi(){
      $context = file_get_contents('https://dashboard.telkom.co.id/idwifi/public/ajaxdetailresume?kolom=&l0=&l1=&l2=&l3=&sql=&fokus=UBIS&ap_order=ORDER&mon_rep=MONITORING&startdate=2018%2F03%2F01&enddate=2018%2F03%2F07&group_ubis%5B%5D=CONS&group_ubis%5B%5D=EBIS&group_ubis%5B%5D=TSEL&group_ubis%5B%5D=PARTNERSHIP+SALES&group_ubis%5B%5D=PARTNERSHIP+SALES+AND+INSTALASI&group_ubis%5B%5D=PARTNERSHIP+JOIN+SSID&group_ubis%5B%5D=PARTNERSHIP+SALES%2C+INSTALASI+AND+AP+OWNER&group_ubis%5B%5D=OTH&group_ubis%5B%5D=DWS&treg%5B%5D=6&paket=ALL&p_on_air=0&dl=true');

      $dom = @\DOMDocument::loadHTML(trim($context));
      $columns = array("nom", "nsorder_quo", "createdon_quo", "userstatus_quo", "nsorder_ao", "createdon_ao", "userstatus_ao", "sid", "sotp_no", "nipnas", "name_sold", "region_sotp", "segmen_sotp", "shtp", "name_shtp", "region_shtp", "accountnas", "productid", "sero_id_fs", "status_abbreviation_fs", "status_date_fs", "sero_id_prov", "status_abbreviation_prov", "status_date_prov", "witel", "skema_bisnis", "groupstatus_tenoss", "ubis", "groupstatus", "reg_id", "projectname_prov", "projectname_fs", "amount_price", "jml_ap_attr", "billcomdate", "group_ubis", "task_name", "task_name_date", "tgl_pcom_ao", "kcontact", "createdby_quo", "nsorder_nkes", "createdby_nkes", "createdon_nkes", "userstatus_nkes", "createdby_ao", "address_sotp", "postalc_sotp", "city_sotp", "country_sotp", "address_shtp", "postalc_shtp", "city_shtp", "country_shtp", "btp_no", "name_btp", "address_btp", "postalc_btp", "city_btp", "country_btp", "region_btp", "productname", "bpemployresp", "nikemployresp", "name1resp", "cityresp", "nobpcp", "lnamecp", "address", "postalcodecp", "citycp", "regioncp", "cwitel", "process_type_id_ticares", "process_type_id_tenoss", "process_type", "currency", "jml_ap_tenoss", "cntrc_sdate", "billdate", "prodactdate", "tgl_cancel_quo", "tgl_cancel_nkes", "tgl_cancel_ao", "groupstatus_quo", "task_workgroup", "jml_ap_nms", "tgl_backhaul_done", "tgl_instap_done", "durasi_taskname", "durasi_fs_closed", "durasi_ao_closed", "ap_status", "partner_name", "dll");

      $table = $dom->getElementsByTagName('table')->item(0);
      $rows = $table->getElementsByTagName('tr');
      $result = array();

      for ($i = 1, $count = $rows->length; $i < $count; $i++)
      {
          $cells = $rows->item($i)->getElementsByTagName('td');
          $data = array();
          for ($j = 0, $jcount = count($columns); $j < $jcount; $j++)
          {
              $td = $cells->item($j);
              $data[$columns[$j]] =  $td->nodeValue;
          }
          $result[] = $data;
      }
      dd($result);
      DB::table("data_starclick_wifi")->insert($result);
  }

  public function grapA2s(){
    ini_set('max_execution_time', 60000);

    // $ch = curl_init();
    // curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    // curl_setopt($ch, CURLOPT_URL, 'http://10.62.170.27/login.php');
    // curl_setopt($ch, CURLOPT_POSTFIELDS,"usr=850056&pwd=KalselHibat1");
    // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    // curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie-name.txt');  
    // curl_setopt($ch, CURLOPT_COOKIEFILE, 'cookie-name.txt');
    // curl_setopt($ch, CURLOPT_POST, true);
    // curl_setopt($ch, CURLOPT_HEADER, true);
    // $hasil=curl_exec($ch);

    // $ch = curl_init();
    // curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    // curl_setopt($ch, CURLOPT_URL, 'http://10.62.170.27/index.php?treg=6&witel=all&psb=NEW%20SALES&play=23p&hari=all&tl=all&mode=sto');
    // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    // curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie-name.txt');  
    // curl_setopt($ch, CURLOPT_COOKIEFILE, 'cookie-name.txt');
    // curl_setopt($ch, CURLOPT_POST, true);
    // curl_setopt($ch, CURLOPT_HEADER, true);
    // $context=curl_exec($ch); 
    // curl_close($ch);

    // $dom = @\DOMDocument::loadHTML(trim($context));
    // $columns = array("witel","unsc","re","not_va","va","prov_awl","prov_mj","prov_as_ex","prov_as_hi","prov_as_hplus","prov_val","prov_nok","prov_tot","acomp_ok","acomp_nok","prv_com","rw_os","comp_hi","comp_est","comp_dev","comp_h_1","fall_wfm","fall_uim","fall_asp","fall_osm","fall_nok","fall_tot","total_order");

    // dd($dom);
    // // dd($dom);
    // $table = $dom->getElementsByTagName('table')->item(0);
    // $rows = $table->getElementsByTagName('tr');
    // $result = array();

    // for ($i = 1, $count = $rows->length; $i < $count; $i++)
    // {
    //     $cells = $rows->item($i)->getElementsByTagName('td');
    //     $data = array();
    //     for ($j = 0, $jcount = count($columns); $j < $jcount; $j++)
    //     {
    //         $td = $cells->item($j);
    //         $data[$columns[$j]] =  $td->nodeValue;
    //     }
    //     $result[] = $data;
    // };

    // dd($result);

  /////////////////////////////////////////////////////
   
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);


    // assurance
    // curl_setopt($ch, CURLOPT_URL, 'http://apps.telkomakses.co.id/assurance/rta_detil_hadir.php?p_witel=44&start_date=&end_date=06/04/2018&alpro=1&lama=3');

    // provisioning
    curl_setopt($ch, CURLOPT_URL, 'http://apps.telkomakses.co.id/assurance/rta_detil_hadir.php?p_witel=44&start_date=&end_date=06/04/2018&alpro=2&lama=3');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie-name.txt');  
    curl_setopt($ch, CURLOPT_COOKIEFILE, 'cookie-name.txt');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    $context=curl_exec($ch); 
    curl_close($ch);

    $dom = @\DOMDocument::loadHTML(trim($context));
    $columns = array("no","regional","witel","laborcode","teknisi","nik","supervisor","jenis_teknisi","no_hp","workzone","jadwal","hadir");

    $table = $dom->getElementsByTagName('table')->item(1);
    $rows = $table->getElementsByTagName('tr');
    $result = array();

    for ($i = 1, $count = $rows->length; $i < $count; $i++)
    {
        $cells = $rows->item($i)->getElementsByTagName('td');
        $data = array();
        for ($j = 0, $jcount = count($columns); $j < $jcount; $j++)
        {
            $td = $cells->item($j);
            $data[$columns[$j]] =  $td->nodeValue;
        }
        $result[] = $data;
    };
    // dd($result);
    if (empty($result)){
      $text = 'Semua Teknisi Sudah Absen';
    }
    else{
        $text ='';
        foreach($result as $data){
          $text .= 'laborcode : '.$data['laborcode']."<br>".'NIK : '.$data['nik']."<br>".'Teknisi : '.$data['teknisi']."<br><br>";  
        };
    }

    echo $text;
  }

  public function a2sGgnOpen()
  {
    ini_set('max_execution_time', 60000);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

    curl_setopt($ch, CURLOPT_URL, 'http://apps.telkomakses.co.id/assurance/rta_detil_ggn_open_idh.php?p_witel=44&lama=121');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie-name.txt');  
    curl_setopt($ch, CURLOPT_COOKIEFILE, 'cookie-name.txt');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    $context=curl_exec($ch); 
    curl_close($ch);

    $dom = @\DOMDocument::loadHTML(trim($context));
    $columns = array("no","ticketid", "nd", "jns_alpro", "lama_ggn", "witel", "workzone", "tgl_beckend", "trouble_headline", "laborcode", "schedule_date", "nama_plg", "alamat", "jns_ggn", "emosi", "kcontack", "datek");

    $table = $dom->getElementsByTagName('table')->item(1);
    $rows = $table->getElementsByTagName('tr');
    $result = array();

    for ($i = 1, $count = $rows->length; $i < $count; $i++)
    {
        $cells = $rows->item($i)->getElementsByTagName('td');
        $data = array();
        for ($j = 0, $jcount = count($columns); $j < $jcount; $j++)
        {
            $td = $cells->item($j);
            $data[$columns[$j]] =  $td->nodeValue;
        }
        $result[] = $data;
    };

    $count = count($result);
    $sql = "replace into a2s_detail_ggn_open_bank (no,ticketid, nd, jns_alpro, lama_ggn, witel, workzone, tgl_beckend, trouble_headline, laborcode, schedule_date, nama_plg, alamat, jns_ggn, emosi, kcontack, datek) values ";

    // $sql = "replace into a2s_detail_ggn_open_bank values ";
   
    $sparator = "";
    for ($a=0; $a<$count; $a++){
        $sql .= $sparator."('".$result[$a]["no"]."','".$result[$a]["ticketid"]."','".$result[$a]["nd"]."','".$result[$a]["jns_alpro"]."','".$result[$a]["lama_ggn"]."','".$result[$a]["witel"]."','".$result[$a]["workzone"]."','".$result[$a]["tgl_beckend"]."','".$result[$a]["trouble_headline"]."','".$result[$a]["laborcode"]."','".$result[$a]["schedule_date"]."','".$result[$a]["nama_plg"]."','".$result[$a]["alamat"]."','".$result[$a]["jns_ggn"]."','".$result[$a]["emosi"]."','".$result[$a]["kcontack"]."','".$result[$a]["datek"]."'),";
    }

    $sql = substr($sql, 0, -1);

    DB::table('a2s_detail_ggn_open')->truncate();
    DB::table("a2s_detail_ggn_open")->insert($result);
    DB::statement($sql);
  }

}
