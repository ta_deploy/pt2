<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\DA\Pt3Model;
use Telegram;
date_default_timezone_set("Asia/Makassar");

class Pt3Controller extends Controller
{
	public function dashboard(){
		$id = session('auth')->id_user;
		$data = Pt3Model::getDashboard();
		return view('pt3.dashboardpt3', compact('data'));
	}

	public function list($sto, $sts){
		$id = session('auth')->id_user;
		$list = Pt3Model::getlist($sto, $sts);
		return view('pt3.listdashboard', compact('list'));
	}
}
