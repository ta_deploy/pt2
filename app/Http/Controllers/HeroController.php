<?php
namespace App\Http\Controllers;

use App\DA\HeroModel;
use App\DA\ReportModel;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

date_default_timezone_set("Asia/Makassar");
class HeroController extends Controller
{
	public function table_hero(Request $req)
	{
		$data = HeroModel::data_HERO(date('Y-m'), $req);

		if($req->all() )
		{
			$start = $req->tgl_a;
			$end = $req->tgl_f;
		}
		else
		{
			$start = date('Y-m-d', strtotime('-3 month '. date('Y') ) );
			$end = date('Y-m-d');
		}

		$fd_not_ready = $fd_ready = [];
		$count_not_ready = $count_ready = 0;

		foreach($data as $v)
		{
			if($v->status_wo_pt2 == 'ADA' && is_null($v->regu_name) )
			{
				$fd_ready[$v->datel]['datel'] = $v->datel;
				$fd_ready[$v->datel]['list'][] = $v;
				$count_ready += 1;
			}
			else
			{
				$fd_not_ready[$v->datel]['datel'] = $v->datel;
				$fd_not_ready[$v->datel]['list'][] = $v;
				$count_not_ready += 1;
			}
		}

		$datel = ReportModel::get_all_datel();

		if($fd_ready)
		{
			foreach($fd_ready as $k => &$v)
			{
				usort($v['list'], function($a, $b) {
				return $b->tgl_buat <=> $a->tgl_buat;
				});
			}
		}

		if($fd_not_ready)
		{
			foreach($fd_not_ready as $k => &$v)
			{
				usort($v['list'], function($a, $b) {
				return $b->tgl_buat <=> $a->tgl_buat;
				});
			}
		}

		return view('Hero.table_hero', compact('fd_ready', 'fd_not_ready', 'datel', 'count_ready', 'count_not_ready', 'start', 'end') );
	}

	public function post_flagging(Request $req)
	{
		HeroModel::update_flagging($req);
	}

	public function dashboard_hero($date)
	{
		$data = HeroModel::data_HERO($date);
		$fd = [];

		foreach($data as $v)
		{
			$fd[$v->datel]['datel'] = $v->datel;

			if(!isset($fd[$v->datel]['list'][$v->sto]) )
			{
				$fd[$v->datel]['list'][$v->sto]['all']      = 0;
				$fd[$v->datel]['list'][$v->sto]['pt2']      = 0;
				$fd[$v->datel]['list'][$v->sto]['pt2_plus'] = 0;
				$fd[$v->datel]['list'][$v->sto]['kendala']  = 0;
				$fd[$v->datel]['list'][$v->sto]['none']     = 0;
			}

			$fd[$v->datel]['list'][$v->sto]['all'] += 1;

			switch ($v->flagging) {
				case 'non_unsc':
					$fd[$v->datel]['list'][$v->sto]['pt2'] += 1;
				break;
				case 'pt2_plus':
					$fd[$v->datel]['list'][$v->sto]['pt2_plus'] += 1;
				break;
				case 'kendala':
					$fd[$v->datel]['list'][$v->sto]['kendala'] += 1;
				break;
				default:
					$fd[$v->datel]['list'][$v->sto]['none']  += 1;
				break;
			}
		}
		// dd($fd);
		return view('Hero.dashboard_hero', compact('fd') );
	}

	public function dashboard_progress_hero($date)
	{
		$data = HeroModel::progress_dash_hero($date);
		$fd = [];
		$no = 0;

		foreach($data as $k => $v)
		{
			if(!isset($fd[$v->flagging][$v->datel]) )
			{
				$fd[$v->flagging][$v->datel] = new \stdClass();
				$fd[$v->flagging][$v->datel]->datel 		= $v->datel;
				$fd[$v->flagging][$v->datel]->no_regu   = 0;
				$fd[$v->flagging][$v->datel]->no_work   = 0;
				$fd[$v->flagging][$v->datel]->berangkat = 0;
				$fd[$v->flagging][$v->datel]->ogp       = 0;
				$fd[$v->flagging][$v->datel]->kendala   = 0;
				$fd[$v->flagging][$v->datel]->pending   = 0;
				$fd[$v->flagging][$v->datel]->selesai   = 0;
			}

			if(!$v->regu_id)
			{
				$fd[$v->flagging][$v->datel]->no_regu += 1;
			}
			else
			{
				switch ($v->lt_status)
				{
					case 'Berangkat':
						$fd[$v->flagging][$v->datel]->berangkat += 1;
					break;
					case 'Ogp':
						$fd[$v->flagging][$v->datel]->ogp += 1;
					break;
					case 'Kendala':
						$fd[$v->flagging][$v->datel]->kendala += 1;
					break;
					case 'Pending':
						$fd[$v->flagging][$v->datel]->pending += 1;
					break;
					case 'Selesai':
						$fd[$v->flagging][$v->datel]->selesai += 1;
					break;
					default:
						$fd[$v->flagging][$v->datel]->no_work += 1;
					break;
				}
			}
		}

		$fd = array_map('array_values', $fd);

		return view('Hero.progress_HERO', compact('fd') );
	}

}