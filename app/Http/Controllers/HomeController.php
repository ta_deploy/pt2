<?php
namespace App\Http\Controllers;

use App\DA\AdminModel;
use App\DA\JointerModel;
use App\DA\TeknisiModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use DB;

date_default_timezone_set("Asia/Makassar");

class HomeController extends Controller
{
	public function index(Request $req, AdminModel $adminmodel, $year)
	{
		$id = session('auth')->id_user;

		$jenis_wo = null;

		if($req->jenis_pt2)
		{
			if(strcasecmp($req->jenis_pt2, 'All') != 0)
			{
				$jenis_wo = $req->jenis_pt2;
			}
			else
			{
				$jenis_wo = null;
			}
		}

		if (in_array(session('auth')->pt2_level, [2, 5]) )
		{
			if ($req->tgl_a)
			{
				$tgl_a = $req->tgl_a;
			}
			else
			{
				$tgl_a = date('Y-m-d', strtotime('first day of January '. $year) );
			}

			if ($req->tgl_f)
			{
				$tgl_b = $req->tgl_f;
			}
			else
			{
				$tgl_b = date('Y-m-d', strtotime('last day of this month '. $year) );

			}

			$status_header  = AdminModel::status_header_dashboard($tgl_a, $tgl_b, $jenis_wo);
			$table_status   = AdminModel::status_table_dsh($tgl_a, $tgl_b, $jenis_wo);
			$data_pt2_all_2 = AdminModel::get_curve_data($tgl_a, $tgl_b, $jenis_wo);
			$saldo_order    = AdminModel::saldo_order($jenis_wo);

			$table_status = array_filter($table_status, function($x){
				if(count($x) != 0) return $x;
			});

			$jenis_pt2 = DB::SELECT("SELECT DISTINCT(jenis_wo) FROM pt2_master WHERE jenis_wo != '' ORDER BY jenis_wo ASC");

			foreach($jenis_pt2 as $v)
			{
				$list_wo[$v->jenis_wo] = $v->jenis_wo;
			}

			return view('Admin.main_dashboard', compact('status_header', 'table_status', 'tgl_a', 'tgl_b', 'data_pt2_all_2', 'saldo_order', 'list_wo', 'jenis_wo') );
		}
		elseif (in_array(session('auth')->pt2_level, [0]))
		{
			$list            = TeknisiModel::teknisi_join();
			$list_done       = TeknisiModel::teknisi_done();
			$list_kendala    = TeknisiModel::teknisi_kendala();
			$list_ogp        = TeknisiModel::teknisi_ogp();
			$list_ogp_reject = TeknisiModel::teknisi_ogp_reject();
			$list_non_pt2    = TeknisiModel::teknisi_not_pt2();
			$list_pending    = TeknisiModel::teknisi_pending();

			$check_regu_exists = JointerModel::check_regu(session('auth')->id_user);
			$check_validate    = JointerModel::check_regu_active(session('auth')->id_user);

			if(!$check_regu_exists)
			{
				$urgent_msg['msg'] = ['type' => 'danger', 'text' => 'Regu Tidak Atif!!'];
				return redirect('/login')->with('alerts_tele', $urgent_msg);
			}

			if($check_validate)
			{
				$urgent_msg['msg'] = ['type' => 'danger', 'text' => 'Tunggu Aktivasi User!!'];
				return redirect('/jointer/tim_party')->with('alerts_tele', $urgent_msg);
			}

			if($check_regu_exists && !$check_validate)
			{
				return view('Teknisi.list_order', ['list' => $list, 'list_not_pt2' => $list_non_pt2, 'list_done' => $list_done, 'list_pending' => $list_pending, 'list_ken' => $list_kendala, 'list_ogp' => $list_ogp, 'list_ogp_reject' => $list_ogp_reject]);
			}
			else
			{
				$urgent_msg['msg'] = ['type' => 'warning', 'text' => 'Harus Sinkron Regu Sekarang!!'];
				return redirect('/jointer/tim_party')->with('alerts_tele', $urgent_msg);
			}
		}
		elseif (in_array(session('auth')->pt2_level, [3, 6]))
		{
			if (empty($req->status) )
			{
				$status = 'empty';
			}
			else
			{
				$status = implode(',', $req->status);
			}

			if (empty($req->stts_regu))
			{
				$status_rg = '1';
			}
			else
			{
				$status_rg = implode(',', $req->stts_regu);
			}

			if (empty($req->tgl_a) )
			{
				$tgl = date('Y-m-d');
			}
			else
			{
				$tgl = $req->tgl_a;
			}

			if (empty($req->tgl_f))
			{
				$tglb = date('Y-m-d H:i:s');
			}
			else
			{
				$tglb = $req->tgl_f;
			}

			$jenis = '';

			if (empty($req->jenis))
			{
				$jenis = implode(', ', array_keys(AdminModel::status() ) );
			}
			else
			{
				$jenis = implode(',', $req->jenis);
			}

			$that = new ReportController();
			$matrix = $that->matrix_all_flex($tgl, $tglb, $status, $jenis, $status_rg);
			$get_all_status = AdminModel::status('all');
			$data_perform = $that->performansi_jointer($tgl, $tglb);

			return view('Report.Matrix.dashboard', compact('matrix', 'view', 'get_all_status', 'data_perform', 'tgl'), ['req' => $req->all()] );
		}
		elseif (in_array(session('auth')->pt2_level, [4]))
		{
			return redirect('/show/booking_odp');
		}
		elseif (in_array(session('auth')->pt2_level, [8, 9]))
		{
			return redirect('/matrix/'.date('Y-m') );
		}
	}
}