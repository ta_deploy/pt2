<?php

namespace App\Http\Controllers;
use App\DA\RekapPoModel;
use App\DA\AdminModel;

class RekapPoController extends Controller
{
	public function rekapPO($month)
	{
		$report_po = RekapPoModel::report_po($month);
		$report_non_po = RekapPoModel::non_report_po();

		return view('RekapPo.rekap_po', compact('report_po', 'report_non_po'));
	}

	public function rekapPO_search($month)
	{
		$report_po = RekapPoModel::report_po($month);
		return view('Tools.rekap_po_ajax', compact('report_po'));
	}

  public function save_po(Request $req)
	{
		$date = date('Y-m');
		RekapPoModel::simpan_po($req);
		return redirect('/rekapPO/list/' . $date);
	}

  public function po()
	{
		return view('RekapPo.PO');
	}

	public function edit_po($id, AdminModel $adminmodel)
	{
		$show = RekapPoModel::show_edit_po($id);
		$sto = $adminmodel->sto();
		$show_list_project_po = RekapPoModel::show_my_list_po($show->id_project_Ta);
		return view('RekapPo.edit_PO', ['show' => $show, 'list' => $show_list_project_po, 'sto' => $sto]);

	}

	public function update_po(Request $req, $id)
	{
		RekapPoModel::update_po_save($req, $id);
		$date = date('Y-m');
		return redirect('/rekapPO/list/' . $date);
	}

	public function delete_po($id)
	{
		RekapPoModel::delete_po($id);
		return back();
	}

  public function live_search_project(Request $req)
	{
		$term = trim($req->q);

		if (empty($term))
		{
			return \Response::json([]);
		}

		$fetchData = RekapPoModel::value_search($term);

		$formatted_tags = [];

		foreach ($fetchData as $row)
		{
			$formatted_tags[] = ["id" => $row->id, "text" => $row->project_name . '(' . $row->proaktif_id . ')'];
		}

		return \Response::json($formatted_tags);
	}
}