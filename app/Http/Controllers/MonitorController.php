<?php
namespace App\Http\Controllers;

use App\DA\MonitorModel;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use ZipArchive;


date_default_timezone_set("Asia/Makassar");

class MonitorController extends Controller
{
	public function my_team()
	{
		$data = MonitorModel::get_my_tim(session('auth')->mitra);
		// dd(session('auth'), $data);
		return view('Monitor.list_regu', compact('data'));
	}

	public function getRanked_ajax($month1, $month2)
	{
		$ranked = MonitorModel::getRanked($month1, $month2);
		if (empty($ranked))
		{
			$data = array(
				0 => (object)['selesai' => '0',
				'urutan' => 'kosong']
			);
		}
		foreach ($ranked as $row)
		{
			$data[] = $row;
		}
		return \Response::json($data);
	}

	public function read_kmz()
	{
		header('Content-Type: application/vnd.google-earth.kmz');
		header('Content-Disposition: attachment; filename="test.kmz"');

		$kmlString="This is your KML string";

		$file = \File::get(public_path() .'/upload/tes.kmz'); // url of the KMZ file
		$zip = new ZipArchive();

		if ($zip->open($file, ZIPARCHIVE::CREATE)!==TRUE) {
		exit("cannot open <$file>\n");
		}
		$zip->addFromString("doc.kml", $kmlString);
		$zip->close();
		echo file_get_contents($file);
	}
}