<?php
namespace App\Http\Controllers;

use App\DA\AdminModel;
use App\DA\TeknisiModel;
use App\DA\JointerModel;
use App\DA\ReportModel;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

date_default_timezone_set("Asia/Makassar");

class TeknisiController extends Controller
{
	public $photo = ['QRcode_Tiang', 'ODP', 'QRcode_ODP', 'QRcode_SPL', 'Redaman_IN_ODP', 'Arc_Count_Sebelum', 'Arc_Count_Sesudah', 'Redaman_OUT_ODP', 'Port_Feeder', 'Port_Distribusi', 'Closure_Sambung', 'Action', 'OLT', 'FTM_2', 'O_side', 'E_side', 'ODC', 'Progress', 'Photo_Rfc', 'Qc_1_(Jarak_Jauh_ODP_Depan)', 'Qc_2_(Jarak_Jauh_ODP_Samping)', 'Qc_3_(Aksesoris)', 'Qc_4_(Redaman_Out_Port_2)', 'Qc_5_(Redaman_Out_Port_3)', 'Qc_6_(Redaman_Out_Port_4)', 'Qc_7_(Redaman_Out_Port_5)', 'Qc_8_(Redaman_Out_Port_6)', 'Qc_9_(Redaman_Out_Port_7)', 'Qc_10_(Redaman_Out_Port_8)', 'Photo_Rfc_2'];

	public function form_laporan($id)
	{
		$target = '';
		$user_mine = session('auth')->id_user;
		$exists = DB::select('SELECT pm.* FROM pt2_master pm LEFT JOIN regu r ON r.id_regu = pm.regu_id WHERE pm.id = ? AND delete_clm = ?', [$id, 0]);

		if (count($exists) )
		{
			$get_reguid = DB::table('regu')->where('id_regu', $exists[0]->regu_id)->first();

			$get_work_not_done = DB::table('pt2_master')
			->where([
				['regu_id', $get_reguid->id_regu],
				['delete_clm', 0],
				['id', '!=', $id]
			])
			->where(function($join){
				$join->WhereNotIn('lt_status', ['Selesai', 'Kendala', 'Pending'])
				->WhereNotNull('lt_status');
			})
			->get();

			if(count($get_work_not_done) > 1 && (is_null($exists[0]->lt_status)) )
			{
				foreach($get_work_not_done as $k => $v)
				{
					$urgent_msg[] = ['type' => 'danger', 'text' => $v->odp_nama .' Belum Selesai/Kendala. Segera Kerjakan!!'];
				}
				return redirect('/home/'. date('Y') )->with('alerts_tele', $urgent_msg);
			}

			if ( (!in_array($exists[0]->lt_status, ['Selesai']) || is_null($exists[0]->lt_status) ) && (in_array($user_mine, [$get_reguid->nik1, $get_reguid->nik2, $get_reguid->nik3, $get_reguid->nik4]) ) || in_array(session('auth')->pt2_level, [2, 5]) || session('auth')->id_user == 18940469)
			{
				// if (empty($exists[0]->lt_status) )
				// {
				// 	$msg = TeknisiModel::check_jalan($id);
				// }

				$target .= 'Teknisi.laporan';
				$foto = $this->photo;
				$data = $exists[0];
			}
			elseif($exists[0]->lt_status == 'Selesai' && in_array(session('auth')->pt2_level, [0]) )
			{
				dd('Pekerjaan Sudah Selesai!');
			}
			else
			{
				dd('Wrong site, wrong time! NIK mu tidak terdaftar Atau Pekerjaan Selesai');
			}
		}
		else
		{
			$data = new \stdClass();
		}

		$materials = TeknisiModel::select_material();
		$materials_saved = TeknisiModel::load_material($id);

		$load_outside_material = [];

		if($data->id_maint != 0)
		{
			$jenis_wo = 'marina';
			$id_outside_wo = $data->id_maint;
		}
		elseif($data->id_bts != 0)
		{
			$jenis_wo = 'bts';
			$id_outside_wo = $data->id_bts;
		}
		else
		{
			$jenis_wo = 'pt-2';
			$id_outside_wo = $data->id;
		}

		$load_outside_material = TeknisiModel::load_material_outside($jenis_wo, $data->id_maint);

		$get_all_material = ReportModel::list_material();

		$get_all_material = array_map(function($item) {
			return (array)$item;
		}, $get_all_material->toArray() );

		if(!$materials_saved[0]->id)
		{
			$materials_saved = [];

			switch ($jenis_wo) {
				case 'marina':
					foreach($load_outside_material as $k => $v)
					{
						$find_k = array_search($v->id_item, array_column($get_all_material, 'id_item') );

						if($find_k !== FALSE)
						{
							$materials_saved[$k] = new \stdClass();
							$materials_saved[$k]->id = $get_all_material[$find_k]['id'];
							$materials_saved[$k]->pt2_id = $id;
							$materials_saved[$k]->id_item = $v->id_item;
							$materials_saved[$k]->qty = $v->qty;
						}
					}
				break;
				case 'bts':
					foreach($load_outside_material as $k => $v)
					{
						$find_k = array_search($v->id_item, array_column($get_all_material, 'id_barang') );

						if($find_k !== FALSE)
						{
							$materials_saved[$k] = new \stdClass();
							$materials_saved[$k]->id = $get_all_material[$find_k]['id'];
							$materials_saved[$k]->pt2_id = $id;
							$materials_saved[$k]->id_item = $v->id_barang;
							$materials_saved[$k]->qty = $v->qty;
						}
					}
				break;
				default:
					foreach($load_outside_material as $k => $v)
					{
						$find_k = array_search($v->id_item, array_column($get_all_material, 'id_barang') );

						if($find_k !== FALSE)
						{
							$materials_saved[$k] = new \stdClass();
							$materials_saved[$k]->id = $get_all_material[$find_k]['id'];
							$materials_saved[$k]->pt2_id = $id;
							$materials_saved[$k]->id_item = $v->id_item;
							$materials_saved[$k]->qty = $v->qty;
						}
					}
				break;
			}
		}

		$load_log_raw = TeknisiModel::log_teknisi($id);

		$load_log = [];

		foreach($load_log_raw as $k => $v)
		{
			if(!isset($load_log[$v->id_pt2]))
			{
				$load_log[$v->id_pt2] = new \stdClass();
			}

			$load_log[$v->id_pt2]->id = $v->id_pt2;
			$load_log[$v->id_pt2]->nama = $v->nama;
			$load_log[$v->id_pt2]->updated_by = $v->updated_by;
			$load_log[$v->id_pt2]->timestamp_dispatch = $v->timestamp_dispatch;

			if($v->id_item)
			{
				if($v->qty > 0)
				{
					$load_log[$v->id_pt2]->keterangan[] = "<b>$v->id_item <span style='color: #03a9f3;'>Bertambah</span> $v->qty Buah!!</b>";
				}
				else
				{
					$load_log[$v->id_pt2]->keterangan[] = "<b>$v->id_item <span style='color: red;'>Berkurang</span> ".abs($v->qty)." Buah!!</b>";
				}
			}
			else
			{
				$load_log[$v->id_pt2]->keterangan[] = $v->keterangan;
			}
		}

		usort($load_log, function($a, $b) {return strcmp($b->id, $a->id);});

		return view($target, ['id' => $id, 'material_load' => $materials_saved], compact('foto', 'data', 'materials', 'load_outside_material', 'load_log') );
	}

	public function save_laporan(Request $req, $id, TeknisiModel $teknisimodel)
	{
		if($req->btn_isi == 'submit_admin')
		{
			dd('maintenance');
		}
		elseif($req->btn_isi == 'reject_admin')
		{
			$msg = TeknisiModel::reject_dispatch_teknisi($id, $req);
		}
		else
		{
			$msg = $teknisimodel->saveLaporan($req);
		}
		return redirect("/home/" . date('Y'))->with('alerts_tele', $msg);
	}

	public function find_base_rfc(Request $req)
	{
		// $raw_gudang     = explode('/', $req->searchTerm);
		// $get_raw_gudang = TeknisiModel::find_gudang($raw_gudang[3]);
		// $search         = $req->searchTerm;
		// $check_data     = TeknisiModel::rfc_search($search, 'no_wh');
		// $data           = array();
		// if(count($check_data) <= 0){
		//   $gudang  = str_replace(' ', '_', $get_raw_gudang->nama_gudang);
		//   $gudang1 = str_replace('(', '+', $gudang);
		//   $gudang2 = str_replace(')', '?', $gudang1);
		//   exec('cd ..;php artisan byRfc '.$search.' '.$get_raw_gudang->nama_gudang.'> /dev/null &');
		//   sleep(5);
		//   $fetchData = TeknisiModel::rfc_search($search, $req->gudang);
		//   foreach($fetchData as $row){
		//           // dd($row->ORDER_ID);
		//     $data[] = array("id"=>$row->no_rfc, "text"=>$row->no_rfc.' ( '.$row->project.' )');
		//   }
		// }else{
		//   foreach($check_data as $row){
		//           // dd($row->ORDER_ID);
		//     $data[] = array("id"=>$row->no_rfc, "text"=>$row->no_rfc.' ( '.$row->project.' )');
		//   }
		// }
		$data = [];
		return \Response::json($data);

	}

	public function rfc_search(Request $req)
	{
		$find_data = TeknisiModel::rfc_search($req->data, 'single');
		return \Response::json($find_data);
	}
}