<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Facades\Excel;
use DB;

date_default_timezone_set("Asia/Makassar");

class ApiController extends Controller
{
	Public function insert_master(Request $req)
	{
		DB::Table('pt2_master')->insert($req->all() );
	}

	Public function update_master(Request $req)
	{
		DB::Table('pt2_master')->Where('id', $req->id)->update($req->data->all() );
	}

	Public function get_data_hero($date)
	{
		$sql = DB::Table('pt2_master As pm')
		->Leftjoin('regu As r', 'pm.regu_id', '=', 'r.id_regu')
		->Leftjoin('mitra_amija As ma', 'r.mitra', '=', 'ma.mitra_amija')
		->select('pm.*', 'ma.mitra_amija_pt')
		->Where([
			['delete_clm', 0],
			['id_hero', '!=', 0]
		]);

		if(strcasecmp($date, 'All') != 0)
		{
			$sql->where(function($join) use($date){
				$join->where('tgl_selesai', 'like', $date.'%')
				->orwhere('tgl_buat', 'like', $date.'%');
			});
		}

		return $sql->get();
	}

	Public function delete_master($id)
	{
		DB::Table('pt2_master')->Where('id', $id)->update([
			'delete_clm' => 1
		]);
	}

	Public function save_hero(Request $req)
	{
		DB::transaction(function () use ($req){
			$cek_id_hero = DB::table('pt2_master')->where([
				['id_hero', $req->id_hero],
				['delete_clm', 0]
			])->first();

			if($req->jenis_wo == 'PT-2')
			{
				$req->jenis_wo = 'PT-2 SIMPLE';
			}

			if(!$cek_id_hero)
			{
				$id = DB::Table('pt2_master')->insertGetId([
					'id_hero'              => $req->id_hero,
					'project_name'         => $req->nama_jalan,
					'odp_koor'             => $req->koordinat,
					// 'kategory_non_unsc' => $get_status->id,
					'kategory_non_unsc'    => $req->kategory_non_unsc,
					'nomor_sc'             => $req->nomor_sc,
					'nomor_sc_log'         => $req->nomor_sc_log,
					'jenis_wo'             => $req->jenis_wo,
					'status'               => 'Construction',
					'sto'                  => $req->sto,
				]);

				DB::table('pt2_dispatch')->insert([
					'id_order'           => $id,
					'regu_id'            => 0,
					'tgl_kerja_dispatch' => 0,
					'keterangan'         => 'Create Tiket HERO',
					'updated_by'         => $req->created_by
				]);
				return ['status'=>'success', 'msg'=> 'Berhasil Membuat Tiket HERO!!'];
			}
			else
			{
				return ['status'=>'danger', 'msg'=> 'TIKET HERO SUDAH ADA!!'];
			}
		});

	}

	public function get_data_unsc(Request $req)
	{
		$sql = DB::Table('kpro_evaluasi_unsc As keu')
		->leftjoin('pt2_master As pm', 'pm.nomor_sc', '=', 'keu.order_id')
		->select('keu.*')
		->WhereNull('pm.id');

		if(strcasecmp($req->jenis, 'All') != 0)
		{
			$sql->Where('keu.order_id', $req->order_id);
		}

		return $sql->get();
	}

}