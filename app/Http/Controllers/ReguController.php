<?php
namespace App\Http\Controllers;

use App\DA\AdminModel;
use App\DA\ReportModel;
use App\DA\TeknisiModel;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Facades\Excel;

date_default_timezone_set("Asia/Makassar");
class ReguController extends Controller
{
	public function showregu(AdminModel $adminmodel)
	{
		$child_list = AdminModel::list_regu_pt2('aktif');
		$main_list = AdminModel::parent_regu();

		foreach ($main_list as $key => $value)
		{
			$list[$key] = $value;

			foreach ($child_list as $C_k => $C_V)
			{
				if ($value->TL == $C_V->TL)
				{
					$list[$key]->list[] = $C_V;
				}
			}
		}

		$regu_nonaktif = AdminModel::list_regu_pt2('notaktif');
		return view('Regu.list_regu', ['list' => $list, 'disabled' => $regu_nonaktif]);
	}

	public function delete_regu($id)
	{
		$adminmodel = new AdminModel();
		$adminmodel->delete_Regu($id);
		$get_list = AdminModel::get_history_regu($id);
		return view('Regu.detail_delete_regu', compact('get_list'));
	}

	public function edit_regu($id, Adminmodel $adminmodel)
	{
		$edit = $adminmodel->edit_regu($id);
		$sto = $adminmodel->sto();
		$mitra = AdminModel::mitra_pt2();
		return view('Regu.edit_regu', ['edit' => $edit, 'sto' => $sto], compact('mitra') );
	}

	public function reactive_regu($id)
	{
		AdminModel::reactive_regu($id);
	}

	public function update_regu(Request $req, $id, AdminModel $adminmodel)
	{
		$adminmodel->sto();
		$adminmodel->update_regu($id, $req);
		return redirect('/regu/list');
	}

	public function add_regu(AdminModel $adminmodel)
	{
		$sto = $adminmodel->sto();
		$mitra = AdminModel::mitra_pt2();
		return view('Regu.edit_regu', ['sto' => $sto], compact('mitra'));
	}

	public function save_regu(Request $req, AdminModel $adminmodel)
	{
		$adminmodel->simpan_regu($req);
		return redirect('/regu/list');
	}

  public function list_detail_history($id, $stts)
	{
		$data = AdminModel::get_detail_hist($id, $stts);
		return view('Regu.history_regu_wo', compact('data'));
	}
}