<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use DB;
class GrabController extends Controller
{
	public static function grabAlistaByRfc($rfc, $gudang)
	{
		ini_set('max_execution_time', 60000);
		// $noRfc   = str_replace('-', '/', $rfc);
		// $gudangs = str_replace(' ', '%20', $gudang);
		$noRfc = $rfc;
		$gudang1 = str_replace('_', '%20', $gudang);
		$gudang2 = str_replace('+', '(', $gudang1);
		$gudangs = str_replace('?', ')', $gudang2);

		$gudang1 = str_replace('_', ' ', $gudang);
		$gudang2 = str_replace('-', '+', $gudang1);
		$gudangAsli = str_replace('?', ')', $gudang2);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_URL, 'https://alista.telkomakses.co.id/index.php?r=site/login');
		$data = DB::table('akun')->where('id', '8')
			->first();
		curl_setopt($ch, CURLOPT_POSTFIELDS, "LoginForm%5Busername%5D=" . $data->user . "&LoginForm%5Bpassword%5D=" . $data->pwd . "&yt0=");
		//curl_setopt($ch, CURLOPT_POSTFIELDS, "LoginForm%5Busername%5D=18950795&LoginForm%5Bpassword%5D=@telkom1234&yt0=");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie-name.txt');
		curl_setopt($ch, CURLOPT_COOKIEFILE, 'cookie-name.txt');
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HEADER, true);

		$rough_content = curl_exec($ch);
		// var_dump($rough_content);
		// dd($rough_content);
		$err = curl_errno($ch);
		$errmsg = curl_error($ch);
		$header = curl_getinfo($ch);

		$header_content = substr($rough_content, 0, $header['header_size']);
		$body_content = trim(str_replace($header_content, '', $rough_content));
		$pattern = "#Set-Cookie:\\s+(?<cookie>[^=]+=[^;]+)#m";
		preg_match_all($pattern, $header_content, $matches);
		$cookiesOut = $matches['cookie'][0];
		$header['errno'] = $err;
		$header['errmsg'] = $errmsg;
		$header['headers'] = $header_content;
		// $header['cookies'] = $cookiesOut;
		$cookie = null;

		// alista id
		curl_setopt($ch, CURLOPT_URL, 'https://alista.telkomakses.co.id/index.php?Permintaanpengambilanbarang%5Bid_permintaan_ambil%5D=&Permintaanpengambilanbarang%5Bproject_id%5D=&Permintaanpengambilanbarang%5Bnama_gudang%5D=&Permintaanpengambilanbarang%5Btgl_permintaan%5D=&Permintaanpengambilanbarang%5Bnama_requester%5D=&Permintaanpengambilanbarang%5Bpengambil%5D=&Permintaanpengambilanbarang%5Bno_rfc%5D=' . $noRfc . '&Permintaanpengambilanbarang%5Bnik_pemakai%5D=&Permintaanpengambilanbarang%5Bnama_mitra%5D=&Permintaanpengambilanbarang_page=1&r=gudang%2Fhistorypengeluaranproject');

		$result = curl_exec($ch);

		$dom = @\DOMDocument::loadHTML($result);
		$table = $dom->getElementsByTagName('table')
			->item(0);
		$rows = $table->getElementsByTagName('tr')
			->item(2)
			->getElementsByTagName('td');
		$idAlista = $rows->item(0)->nodeValue;

		//
		$err = curl_errno($ch);
		$errmsg = curl_error($ch);
		$header = curl_getinfo($ch);

		$header_content = substr($rough_content, 0, $header['header_size']);
		$body_content = trim(str_replace($header_content, '', $rough_content));
		$pattern = "#Set-Cookie:\\s+(?<cookie>[^=]+=[^;]+)#m";
		preg_match_all($pattern, $header_content, $matches);
		$cookiesOut = $matches['cookie'][0];
		$header['errno'] = $err;
		$header['errmsg'] = $errmsg;
		$header['headers'] = $header_content;
		$cookie = null;

		$sql = 'insert into alista_material_keluar(alista_id,tgl,nama_gudang,project,pengambil,pengambil_nama,mitra,id_barang,nama_barang,jumlah,no_rfc,proaktif_id) values ';

		curl_setopt($ch, CURLOPT_URL, 'https://alista.telkomakses.co.id/index.php?r=site/downloadpengeluaranmaterial&a=&b=&c=' . $gudangs . '&d=&e=&f=' . $noRfc . '&g=&h=');
		curl_setopt($ch, CURLOPT_COOKIE, $cookiesOut);
		$result = curl_exec($ch);
		// var_dump($result);
		// dd($result);
		$dom = @\DOMDocument::loadHTML($result);
		$table = $dom->getElementsByTagName('table')
			->item(0);
		$rows = $table->getElementsByTagName('tr');
		$columns = array(
			0 => 'alista_id',
			'tgl',
			'ket',
			'project',
			'no_rfc',
			'id_gudang',
			'nama_gudang',
			'regional',
			'id_barang',
			'nama_barang',
			'jumlah',
			'harga',
			'harga_total',
			'requester',
			'pengambilMaterial',
			'nik_pemakai',
			'mitra'
		);
		$result = array();
		for ($i = 1, $count = $rows->length;$i < $count;$i++)
		{
			$cells = $rows->item($i)->getElementsByTagName('td');
			$data = array();
			for ($j = 0, $jcount = count($columns);$j < $jcount;$j++)
			{
				$td = $cells->item($j);
				$data[$columns[$j]] = $td->nodeValue;
			}
			$data['alista_id'] = $idAlista;
			$result[] = $data;
		}
		$data = $result;

		if (count($data) <> 0)
		{
			DB::table("alista_material_keluar")->where("no_rfc", $noRfc)->where('nama_gudang', $gudangAsli)->delete();
			foreach ($data as $datax)
			{
				$dataPengambil = explode('#', $datax['pengambilMaterial']);
				if (count($dataPengambil) > 1)
				{
					$nikPengambil = $dataPengambil[0];
					$namaPengambil = $dataPengambil[1];
				}
				else
				{
					$nikPengambil = $dataPengambil[0];
					$namaPengambil = '';
				};

				$idItemBanntu = $datax['id_barang'] . '_' . $datax['no_rfc'];
				$dataNikPengambil = array(
					"pengambil" => $nikPengambil,
					"pengambil_nama" => $namaPengambil,
					"id_item_bantu" => $idItemBanntu
				);

				$datax = array_merge_recursive($datax, $dataNikPengambil);
				unset($datax['ket'], $datax['regional'], $datax['harga'], $datax['harga_total'], $datax['id_gudang'], $datax['pengambilMaterial']);

				DB::table("alista_material_keluar")->insert($datax);
			}
			echo "sukses";
		}
		else
		{
			echo 'gagal';
		}
	}
	public function grab_imon($ex_id)
	{
		$akun = DB::table('akun')->where('app', 'SDI')
			->first();
		$fields = array(
			'username' => $akun->user,
			'password' => $akun->pwd
		);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'https://oss.telkomakses.co.id/imon/index.php/loginbckend');
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HEADER, true);

		$fields_string = http_build_query($fields);
		echo $fields_string;
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_COOKIESESSION, true);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);

		$rough_content = curl_exec($ch);
		$err = curl_errno($ch);
		$errmsg = curl_error($ch);
		$header = curl_getinfo($ch);

		$header_content = substr($rough_content, 0, $header['header_size']);
		$body_content = trim(str_replace($header_content, '', $rough_content));
		var_dump($header_content);
		$pattern = "#Set-Cookie:\\s+(?<cookie>[^=]+=[^;]+)#m";
		preg_match_all($pattern, $header_content, $matches);
		$cookiesOut = $matches['cookie'][1];
		$header['errno'] = $err;
		$header['errmsg'] = $errmsg;
		$header['headers'] = $header_content;
		$header['cookies'] = $cookiesOut;
		curl_setopt($ch, CURLOPT_URL, 'https://oss.telkomakses.co.id/imon/index.php/ticket/detail/' . $ex_id);

		curl_setopt($ch, CURLOPT_COOKIE, $cookiesOut);
		$html = curl_exec($ch);
		//dd($cookiesOut);
		//get text
		$dom = @\DOMDocument::loadHTML(trim($html));

		$form = $dom->getElementsByTagName('form')
			->item(2);
		$input = $form->getElementsByTagName('input');
		$result = array();
		for ($i = 1, $count = $input->length;$i < $count;$i++)
		{
			$name = $input->item($i)->getAttribute("name");
			$value = $input->item($i)->getAttribute("value");
			if ($name) $result[$name] = $value;
		}
		//dd($result);
		//get all file link
		$html = substr_replace($html, '', 0, strpos($html, '<div class="comment-info">'));
		$html = substr_replace($html, '', strpos($html, '<div style="clear:both"></div>') , strlen($html));
		$html = str_replace('<td><a href="https://oss.telkomakses.co.id/imon//index.php/ticket/download_file_ticket/' . $ex_id . '/"></td>', '', $html);

		$dom = @\DOMDocument::loadHTML(trim($html));
		$arr = $dom->getElementsByTagName("a"); // DOMNodeList Object
		foreach ($arr as $no => $item)
		{
			$href = str_replace(" ", "%20", $item->getAttribute("href"));
			//$link = 'https://oss.telkomakses.co.id/imon/index.php/ticket/download_file_ticket/'.$ex_id.'/'.urlencode($item->nodeValue);
			$text = trim(preg_replace("/[\r\n]+/", " ", $item->nodeValue));
			curl_setopt($ch, CURLOPT_COOKIE, $cookiesOut);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
			curl_setopt($ch, CURLOPT_AUTOREFERER, true);
			curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
			curl_setopt($ch, CURLOPT_TIMEOUT, 120);
			curl_setopt($ch, CURLOPT_URL, $href);
			$data = curl_exec($ch);
			$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
			$header = substr($data, 0, $header_size);
			$body = substr($data, $header_size);
			$path = public_path() . "/upload/imon/" . $ex_id . "/";
			if (!file_exists($path))
			{
				if (!mkdir($path, 0770, true)) return 'gagal menyiapkan folder foto evidence';
			}
			file_put_contents("$path$text", $body);

		}
		curl_close($ch);
		return back();
	}
	public static function grabAlista()
	{

		$date = date('Y-m-d');
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'https://alista.telkomakses.co.id/index.php?r=site/login');
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Ubuntu;Linux x86_64;rv:28.0) Gecko/20100101 Firefox/28.0');
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HEADER, true);

		$akun = DB::table('akun')->where('app', 'deployer')
			->first();
		$username = $akun->user;
		$password = $akun->pwd;

		// curl_setopt($ch, CURLOPT_POSTFIELDS, "LoginForm%5Busername%5D=91155635&LoginForm%5Bpassword%5D=@thebonk31&yt0=");
		curl_setopt($ch, CURLOPT_POSTFIELDS, "LoginForm%5Busername%5D=" . $username . "&LoginForm%5Bpassword%5D=" . $password . "&yt0=");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_COOKIESESSION, true);

		$rough_content = curl_exec($ch);
		var_dump($rough_content);
		$err = curl_errno($ch);
		$errmsg = curl_error($ch);
		$header = curl_getinfo($ch);

		$header_content = substr($rough_content, 0, $header['header_size']);
		$body_content = trim(str_replace($header_content, '', $rough_content));
		$pattern = "#Set-Cookie:\\s+(?<cookie>[^=]+=[^;]+)#m";
		preg_match_all($pattern, $header_content, $matches);
		$cookiesOut = $matches['cookie'][1];
		$header['errno'] = $err;
		$header['errmsg'] = $errmsg;
		$header['headers'] = $header_content;
		$header['cookies'] = $cookiesOut;
		$cookie = null;
		$gudang = array(
			["id_gudang" => "GD0245",
			"nama_gudang" => "WH SO BANJARMASIN 2"],
			["id_gudang" => "GD0244",
			"nama_gudang" => "WH SO BANJARMASIN 1"],
			["id_gudang" => "G16",
			"nama_gudang" => "Banjarmasin - Area"],
			["id_gudang" => "GD0246",
			"nama_gudang" => "WH SO BATULICIN"],
			["id_gudang" => "GD0247",
			"nama_gudang" => "WH SO KANDANGAN"],
			["id_gudang" => "GD0248",
			"nama_gudang" => "WH SO TANJUNG TABALONG"],
			["id_gudang" => "GD0243",
			"nama_gudang" => "WH SO BANJARBARU"]
		);
		$sql = 'insert into alista_material_keluar(alista_id,tgl,nama_gudang,project,pengambil,mitra,id_barang,nama_barang,jumlah,no_rfc,proaktif_id) values ';
		foreach ($gudang as $asd => $g)
		{
			curl_setopt($ch, CURLOPT_URL, 'https://alista.telkomakses.co.id/index.php?r=gudang/reportingtransaksi');
			curl_setopt($ch, CURLOPT_POSTFIELDS, "jnstransaksi=4&Gudang%5Bkode_gudang%5D=" . $g['id_gudang'] . "&Gudang%5Bstartdate%5D=" . $date . "&Gudang%5Benddate%5D=" . $date . "&yt0=");
			curl_setopt($ch, CURLOPT_COOKIE, $cookiesOut);
			$result = curl_exec($ch);
			$dom = @\DOMDocument::loadHTML($result);
			$table = $dom->getElementsByTagName('table')
				->item(1);
			$rows = $table->getElementsByTagName('tr');
			$columns = array(
				0 => 'no',
				'id',
				'tgl',
				'nama_gudang',
				'project',
				'pengambil',
				'mitra',
				'id_barang',
				'nama_barang',
				'jumlah',
				'no_rfc'
			);
			$result = array();
			for ($i = 1, $count = $rows->length;$i < $count;$i++)
			{
				$cells = $rows->item($i)->getElementsByTagName('td');
				$data = array();
				for ($j = 0, $jcount = count($columns);$j < $jcount;$j++)
				{
					$td = $cells->item($j);
					$data[$columns[$j]] = $td->nodeValue;
				}
				$result[] = $data;
			}
			$data = $result;
			$count = count($data);
			$sqll = "";
			if ($count)
			{
				for ($i = 0;$i < $count;$i++)
				{
					$sparator = ", ";
					if ($i == 0 && $asd == 0)
					{
						$sparator = "";
					}

					$proaktif_id = explode(" ", $data[$i]["project"]) [0];
					$sqll .= $sparator . '("' . $data[$i]['id'] . '","' . $data[$i]['tgl'] . '","' . $data[$i]['nama_gudang'] . '"
              ,"' . $data[$i]['project'] . '"
              ,"' . $data[$i]['pengambil'] . '"
              ,"' . $data[$i]['mitra'] . '"
              ,"' . $data[$i]['id_barang'] . '"
              ,"' . $data[$i]['nama_barang'] . '"
              ,"' . $data[$i]['jumlah'] . '"
              ,"' . $data[$i]['no_rfc'] . '"
              ,"' . $proaktif_id . '")';
				}
			}
		}
		$sqll = ltrim($sqll, ',');
		$sql .= $sqll;
		echo $sql;
		DB::table("alista_material_keluar")->where("tgl", $date)->delete();
		DB::statement($sql);
	}
	public static function grabAlistaDate($start)
	{

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'https://alista.telkomakses.co.id/index.php?r=site/login');
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Ubuntu;Linux x86_64;rv:28.0) Gecko/20100101 Firefox/28.0');
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HEADER, true);

		$akun = DB::table('akun')->where('app', 'deployer')
			->first();
		$username = $akun->user;
		$password = $akun->pwd;

		// curl_setopt($ch, CURLOPT_POSTFIELDS, "LoginForm%5Busername%5D=91155635&LoginForm%5Bpassword%5D=@thebonk31&yt0=");
		curl_setopt($ch, CURLOPT_POSTFIELDS, "LoginForm%5Busername%5D=" . $username . "&LoginForm%5Bpassword%5D=" . $password . "&yt0=");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_COOKIESESSION, true);

		$rough_content = curl_exec($ch);
		var_dump($rough_content);
		$err = curl_errno($ch);
		$errmsg = curl_error($ch);
		$header = curl_getinfo($ch);

		$header_content = substr($rough_content, 0, $header['header_size']);
		$body_content = trim(str_replace($header_content, '', $rough_content));
		$pattern = "#Set-Cookie:\\s+(?<cookie>[^=]+=[^;]+)#m";
		preg_match_all($pattern, $header_content, $matches);
		$cookiesOut = $matches['cookie'][1];
		$header['errno'] = $err;
		$header['errmsg'] = $errmsg;
		$header['headers'] = $header_content;
		$header['cookies'] = $cookiesOut;
		$cookie = null;
		$gudang = array(
			["id_gudang" => "GD0245",
			"nama_gudang" => "WH SO BANJARMASIN 2"],
			["id_gudang" => "GD0244",
			"nama_gudang" => "WH SO BANJARMASIN 1"],
			["id_gudang" => "G16",
			"nama_gudang" => "Banjarmasin - Area"],
			["id_gudang" => "GD0246",
			"nama_gudang" => "WH SO BATULICIN"],
			["id_gudang" => "GD0247",
			"nama_gudang" => "WH SO KANDANGAN"],
			["id_gudang" => "GD0248",
			"nama_gudang" => "WH SO TANJUNG TABALONG"],
			["id_gudang" => "GD0243",
			"nama_gudang" => "WH SO BANJARBARU"],
			["id_gudang" => "GD175",
			"nama_gudang" => "WH SO INV BARABAI"]
		);

		//<option value="GD175">WH SO INV BARABAI</option>
		DB::table("alista_material_keluar")->where("tgl", $start)->delete();
		foreach ($gudang as $asd => $g)
		{
			curl_setopt($ch, CURLOPT_URL, 'https://alista.telkomakses.co.id/index.php?r=gudang/reportingtransaksi');
			curl_setopt($ch, CURLOPT_POSTFIELDS, "jnstransaksi=4&Gudang%5Bkode_gudang%5D=" . $g['id_gudang'] . "&Gudang%5Bstartdate%5D=" . $start . "&Gudang%5Benddate%5D=" . $start . "&yt0=");
			curl_setopt($ch, CURLOPT_COOKIE, $cookiesOut);
			$result = curl_exec($ch);
			$dom = @\DOMDocument::loadHTML($result);
			$table = $dom->getElementsByTagName('table')
				->item(1);
			$rows = $table->getElementsByTagName('tr');
			$columns = array(
				0 => 'no',
				'id',
				'tgl',
				'nama_gudang',
				'project',
				'pengambil',
				'mitra',
				'id_barang',
				'nama_barang',
				'jumlah',
				'no_rfc'
			);
			$result = array();
			for ($i = 1, $count = $rows->length;$i < $count;$i++)
			{
				$cells = $rows->item($i)->getElementsByTagName('td');
				$data = array();
				for ($j = 0, $jcount = count($columns);$j < $jcount;$j++)
				{
					$td = $cells->item($j);
					$data[$columns[$j]] = $td->nodeValue;
				}
				$result[] = $data;
			}
			$data = $result;
			$count = count($data);
			if ($count)
			{
				$sql = 'insert into alista_material_keluar(alista_id,tgl,nama_gudang,project,pengambil,mitra,id_barang,nama_barang,jumlah,no_rfc,proaktif_id) values ';

				for ($i = 0;$i < $count;$i++)
				{
					$sparator = ", ";
					if ($i == 0)
					{
						$sparator = "";
					}
					$proaktif_id = explode(" ", $data[$i]["project"]) [0];
					$sql .= $sparator . '("' . $data[$i]['id'] . '","' . $data[$i]['tgl'] . '","' . $data[$i]['nama_gudang'] . '"
              ,"' . $data[$i]['project'] . '"
              ,"' . $data[$i]['pengambil'] . '"
              ,"' . $data[$i]['mitra'] . '"
              ,"' . $data[$i]['id_barang'] . '"
              ,"' . $data[$i]['nama_barang'] . '"
              ,"' . $data[$i]['jumlah'] . '"
              ,"' . $data[$i]['no_rfc'] . '"
              ,"' . $proaktif_id . '")';
				}
				echo $sql;
				DB::statement($sql);
			}
		}
	}
	public static function grabAlistaMonth($month)
	{
		$last = date("Y-m-t", strtotime($month));
		$first = date("Y-m-01", strtotime($month));
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'https://alista.telkomakses.co.id/index.php?r=site/login');
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Ubuntu;Linux x86_64;rv:28.0) Gecko/20100101 Firefox/28.0');
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HEADER, true);

		$akun = DB::table('akun')->where('app', 'deployer')
			->first();
		$username = $akun->user;
		$password = $akun->pwd;

		// curl_setopt($ch, CURLOPT_POSTFIELDS, "LoginForm%5Busername%5D=91155635&LoginForm%5Bpassword%5D=@thebonk31&yt0=");
		curl_setopt($ch, CURLOPT_POSTFIELDS, "LoginForm%5Busername%5D=" . $username . "&LoginForm%5Bpassword%5D=" . $password . "&yt0=");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_COOKIESESSION, true);

		$rough_content = curl_exec($ch);
		var_dump($rough_content);
		$err = curl_errno($ch);
		$errmsg = curl_error($ch);
		$header = curl_getinfo($ch);

		$header_content = substr($rough_content, 0, $header['header_size']);
		$body_content = trim(str_replace($header_content, '', $rough_content));
		$pattern = "#Set-Cookie:\\s+(?<cookie>[^=]+=[^;]+)#m";
		preg_match_all($pattern, $header_content, $matches);
		$cookiesOut = $matches['cookie'][1];
		$header['errno'] = $err;
		$header['errmsg'] = $errmsg;
		$header['headers'] = $header_content;
		$header['cookies'] = $cookiesOut;
		$cookie = null;
		$gudang = array(
			["id_gudang" => "GD0245",
			"nama_gudang" => "WH SO BANJARMASIN 2"],
			["id_gudang" => "GD0244",
			"nama_gudang" => "WH SO BANJARMASIN 1"],
			["id_gudang" => "G16",
			"nama_gudang" => "Banjarmasin - Area"],
			["id_gudang" => "GD0246",
			"nama_gudang" => "WH SO BATULICIN"],
			["id_gudang" => "GD0247",
			"nama_gudang" => "WH SO KANDANGAN"],
			["id_gudang" => "GD0248",
			"nama_gudang" => "WH SO TANJUNG TABALONG"],
			["id_gudang" => "GD0243",
			"nama_gudang" => "WH SO BANJARBARU"],
			["id_gudang" => "GD175",
			"nama_gudang" => "WH SO INV BARABAI"]
		);

		//<option value="GD175">WH SO INV BARABAI</option>
		DB::select("delete from alista_material_keluar where tgl like '%" . $month . "%'");
		foreach ($gudang as $asd => $g)
		{
			curl_setopt($ch, CURLOPT_URL, 'https://alista.telkomakses.co.id/index.php?r=gudang/reportingtransaksi');
			curl_setopt($ch, CURLOPT_POSTFIELDS, "jnstransaksi=4&Gudang%5Bkode_gudang%5D=" . $g['id_gudang'] . "&Gudang%5Bstartdate%5D=" . $first . "&Gudang%5Benddate%5D=" . $last . "&yt0=");
			curl_setopt($ch, CURLOPT_COOKIE, $cookiesOut);
			$result = curl_exec($ch);
			$dom = @\DOMDocument::loadHTML($result);
			$table = $dom->getElementsByTagName('table')
				->item(1);
			$rows = $table->getElementsByTagName('tr');
			$columns = array(
				0 => 'no',
				'id',
				'tgl',
				'nama_gudang',
				'project',
				'pengambil',
				'mitra',
				'id_barang',
				'nama_barang',
				'jumlah',
				'no_rfc'
			);
			$result = array();
			for ($i = 1, $count = $rows->length;$i < $count;$i++)
			{
				$cells = $rows->item($i)->getElementsByTagName('td');
				$data = array();
				for ($j = 0, $jcount = count($columns);$j < $jcount;$j++)
				{
					$td = $cells->item($j);
					$data[$columns[$j]] = $td->nodeValue;
				}
				$result[] = $data;
			}
			$data = $result;
			$count = count($data);
			if ($count)
			{
				$sql = 'insert into alista_material_keluar(alista_id,tgl,nama_gudang,project,pengambil,mitra,id_barang,nama_barang,jumlah,no_rfc,proaktif_id) values ';

				for ($i = 0;$i < $count;$i++)
				{
					$sparator = ", ";
					if ($i == 0)
					{
						$sparator = "";
					}
					$proaktif_id = explode(" ", $data[$i]["project"]) [0];
					$sql .= $sparator . '("' . $data[$i]['id'] . '","' . $data[$i]['tgl'] . '","' . $data[$i]['nama_gudang'] . '"
              ,"' . $data[$i]['project'] . '"
              ,"' . $data[$i]['pengambil'] . '"
              ,"' . $data[$i]['mitra'] . '"
              ,"' . $data[$i]['id_barang'] . '"
              ,"' . $data[$i]['nama_barang'] . '"
              ,"' . $data[$i]['jumlah'] . '"
              ,"' . $data[$i]['no_rfc'] . '"
              ,"' . $proaktif_id . '")';
				}
				echo $sql;
				DB::statement($sql);
			}
		}
	}
}