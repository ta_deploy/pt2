<?php

namespace App\Http\Controllers;

use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\DA\PublicModel;
use App\DA\AdminModel;
date_default_timezone_set("Asia/Makassar");
class PublicController extends Controller
{
	public function saran_public(){
		$data = PublicModel::get_list();
		return view('public.saran', ['data' => $data]);
	}

	public function add_saran(){
		return view('public.Tambah_saran');
	}

	public function save_saran(Request $req){
		PublicModel::save_saran($req);
	}

	public function save_update_list(Request $req){
		PublicModel::save_change($req->id);
	}
}
