<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\DA\LoginModel;

class LoginController extends Controller
{
	public $foto = ['Foto_Kehadiran'];

	public function index()
	{

		return view('Login');
	}

	public function login(Request $req, LoginModel $Loginmodel)
	{
		$user = $req->input('user');
		$pass = $req->input('pass');
		$result = $Loginmodel->login($user, $pass);
		if (count($result) > 0)
		{
			$session = $req->session();
			$this->ensureLocalUserHasRememberToken($result[0], $Loginmodel);
			return $this->successResponse($result[0]->psb_remember_token);
		}
		else
		{
			return redirect('/login')->with('alerts', [['type' => 'danger', 'text' => 'Login Gagal']]);
		}
	}

	public function logout()
	{
		Session::forget('auth');
		Session::forget('badges');
		return redirect('/login')
			->withCookie(cookie()
			->forever('presistent-token', ''));
	}

	private function ensureLocalUserHasRememberToken($localUser, LoginModel $Loginmodel)
	{
		$token = $localUser->psb_remember_token;
		if (!$localUser->psb_remember_token)
		{
			$token = $this->generateRememberToken($localUser->id_user);
			$Loginmodel->remembertoke($localUser, $token);
			$localUser->psb_remember_token = $token;
		}

		return $token;
	}

	private function generateRememberToken($nik)
	{
		return md5($nik . microtime());
	}

	private function successResponse($rememberToken)
	{
		if (Session::has('auth-originalUrl'))
		{
			$url = Session::pull('auth-originalUrl');
		}
		else
		{
			$url = '/';
		}

		$response = redirect($url);
		if ($rememberToken)
		{
			$response->withCookie(cookie()
				->forever('presistent-token', $rememberToken));
		}
		return $response;
	}

	public function absensi_check()
	{
		$session = session('auth');
		$check_log_absen = LoginModel::check_absensi();

		if(in_array(@$session->pt2_level, [0]) )
		{
			if(empty($check_log_absen) || $check_log_absen->approval == 0)
			{
				$foto = $this->foto;
				return view('absensi', compact('foto', 'check_log_absen'));
			}
			elseif($check_log_absen->approval == 1)
			{
				return redirect('/');
			}
			else
			{
				return redirect('/login')->with('alerts', [['type' => 'danger', 'text' => 'Silahkan Menunggu Konfirmasi Absen Dari TL '. $check_log_absen->TL] ]);
			}
		}else{
			return redirect('/');
		}
	}

	public function submit_absen(Request $req)
	{
		LoginModel::absen_teknsi($req);
		return redirect('/');
	}

	public function action_absen($jns, $id)
	{
		LoginModel::proses_absen($jns, $id);
		return redirect('/list_absen');
	}
}