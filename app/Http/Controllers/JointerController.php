<?php

namespace App\Http\Controllers;
use App\DA\JointerModel;
use App\DA\AdminModel;
use App\DA\ReportModel;
use Illuminate\Http\Request;
use DB;

class JointerController extends Controller
{
	public function jointer_view()
	{
		$regu_jointer = AdminModel::list_regu_pt2_jointer();
		$all_emp = AdminModel::get_all_emp();
		$get_all_waspang_join = AdminModel::all_waspang_joint();

		$all_emp = array_map(function($item) {
			return (array)$item;
		}, $all_emp->toArray() );

		$get_all_waspang_join = array_map(function($item) {
			return (array)$item;
		}, $get_all_waspang_join->toArray() );

		$data = $wasp = [];

		foreach($get_all_waspang_join as $v)
		{
			$wasp[$v['id_user'] ] = ['status' => 'ok', 'nik' => $v['id_user'], 'nama' => $v['nama'] ,'witel_Waspang' => $v['Witel_New'], 'ACTIVE' => $v['ACTIVE'], 'list' => [] ];
		}

		foreach($regu_jointer as $v)
		{
			$waspang = (empty($v->atasan_1) || $v->atasan_1 == '' ? 'Kosong' : $v->atasan_1);
			$Witel_New = (empty($v->Witel_New) || $v->Witel_New == '' ? 'Kosong' : $v->Witel_New);

			if(!isset($data[$Witel_New]) )
			{
				$data_wasp = array_filter($wasp, function($x) use($v){
					return $x['witel_Waspang'] == $v->Witel_New;
				});
				$data[$Witel_New] = $data_wasp;
			}

			if($waspang != 'Kosong')
			{
				$find_k = array_search($v->atasan_1, array_column($all_emp, 'nik') );

				if(!isset($data[$Witel_New][$waspang]['status']) )
				{
					$data[$Witel_New][$waspang]['status'] = 'nok';
				}

				if($find_k !== FALSE)
				{
					$data[$Witel_New][$waspang]['nik'] = $all_emp[$find_k]['nik'];
					$data[$Witel_New][$waspang]['nama'] = $all_emp[$find_k]['nama'];
				}
				else
				{
					$data[$Witel_New][$waspang]['nik'] = $waspang;
					$data[$Witel_New][$waspang]['nama'] = 'TANPA NAMA';
				}
				$data[$Witel_New][$waspang]['ACTIVE'] = $v->ACTIVE;
			}
			else
			{
				$data[$Witel_New][$waspang]['status'] = 'unknown';
				$data[$Witel_New][$waspang]['nik'] = 'Kosong';
				$data[$Witel_New][$waspang]['nama'] = 'Kosong';
				$data[$Witel_New][$waspang]['ACTIVE'] = null;
			}

			$data[$Witel_New][$waspang]['list'][$v->nik]['nik']        = $v->nik;
			$data[$Witel_New][$waspang]['list'][$v->nik]['nama']       = $v->nama;
			$data[$Witel_New][$waspang]['list'][$v->nik]['ACTIVE']     = $v->ACTIVE;
			$data[$Witel_New][$waspang]['list'][$v->nik]['Witel_New']  = $Witel_New;
			$data[$Witel_New][$waspang]['list'][$v->nik]['dateUpdate'] = $v->dateUpdate;
		}

		return view('Jointer.list_jointer', ['list' => $data]);
	}

	public function tim_party()
	{
		// $xx = DB::SELECT("SELECT id, odp_nama, odc_koor FROM `pt2_master` WHERE (odc_koor IS NULL OR odc_koor = '') AND odp_nama IS NOT NULL AND odp_nama != '' ORDER BY `pt2_master`.`odp_nama` DESC ");

		// $gao = ReportModel::get_info_odc();

		// $gao = array_map(function($o){
		// 	$a = get_object_vars($o);
		// 	return $a;
		// }, $gao);


		// foreach($xx as $k => $v)
		// {
		// 	$exp = explode('/', $v->odp_nama)[0];
		// 	$exp = explode('-', $exp);

		// 	$find_k = array_keys(array_column($gao, 'sto'), $exp[1]);
		// 	$data = [];

		// 	foreach($find_k as $vv)
		// 	{
		// 		$data[] = $gao[$vv];
		// 	}

		// 	if($data)
		// 	{
		// 		$find_k = array_search($exp[2], array_column($data, 'odc') );

		// 		if($find_k !== FALSE)
		// 		{
		// 			$new_k = $data[$find_k];
		// 			$odc_renew = str_replace(',', '.', $data[$find_k]['latitude']) .','. str_replace(',', '.', $data[$find_k]['longitude']);

		// 			DB::table('pt2_master')->where('id', $v->id)->update([
		// 				'odc_koor' => $odc_renew
		// 			]);
		// 		}
		// 	}
		// }

		$data_party = $get_log_party = $data_all_party = $check_status = $nik = [];
		$status = 'leader';

		$check_regu_exists = JointerModel::check_regu(session('auth')->id_user);

		$get_my_party = JointerModel::get_party_user(session('auth') );

		if(count($get_my_party) )
		{
			$get_log_party = JointerModel::get_log_party(session('auth') );
		}
		else
		{
			$status = 'unit';
			$check_status = JointerModel::get_party_user(session('auth'), 'all', $status, 'first');

			if($check_status && $check_status->status == 1 )
			{
				$get_my_party = JointerModel::get_my_party(session('auth') );
			}
			else
			{
				$get_my_party = JointerModel::get_party_user(session('auth'), 'all', 'unit');
			}

			$get_all_party = JointerModel::get_party_user();

			foreach($get_all_party as $k => $v)
			{
				$data_all_party[$v->leader_nik]['list'][] = $v;
			}

			$get_log_party = JointerModel::get_log_party(session('auth'), [2]);
		}

		foreach($get_my_party as $k => $v)
		{
			if($v->status == 1)
			{
				$nik[$v->unit_nik] = $v->unit_nik;
				$nik[$v->unit_nik] = $v->unit_nik;
			}

			$nik[$v->leader_nik] = $v->leader_nik;

			if(!isset($data_party[$v->leader_nik]['list'][$v->leader_nik]) )
			{
				// dd($v);
				$data_party[$v->leader_nik]['list'][$v->leader_nik] = new \stdClass();
				$data_party[$v->leader_nik]['list'][$v->leader_nik]->id = $v->id;
				$data_party[$v->leader_nik]['list'][$v->leader_nik]->party_name  = $v->party_name;
				$data_party[$v->leader_nik]['list'][$v->leader_nik]->leader_nik  = $v->leader_nik;
				$data_party[$v->leader_nik]['list'][$v->leader_nik]->leader_name = $v->leader_name;
				$data_party[$v->leader_nik]['list'][$v->leader_nik]->unit_nik    = $v->leader_nik;
				$data_party[$v->leader_nik]['list'][$v->leader_nik]->unit_name   = $v->leader_name;
				$data_party[$v->leader_nik]['list'][$v->leader_nik]->unit_masuk  = $v->unit_masuk;
				$data_party[$v->leader_nik]['list'][$v->leader_nik]->status      = 99;
				$data_party[$v->leader_nik]['list'][$v->leader_nik]->created_at  = $v->created_at;
				$data_party[$v->leader_nik]['list'][$v->leader_nik]->created_by  = $v->created_by;
			}

			if(!isset($data_party[$v->leader_nik]['list'][$v->unit_nik]) )
			{
				$data_party[$v->leader_nik]['list'][$v->unit_nik] = $v;
			}
		}

		return view('Jointer.tim_party', compact('data_party', 'get_log_party', 'data_all_party', 'status', 'check_status', 'check_regu_exists') );
	}

	public function create_party()
	{
		if(!session('auth')->mitra)
		{
			$urgent_msg['msg'] = ['type' => 'danger', 'text' => 'Tidak Bisa Membuat Party Karena Mitra Anda Kosong!!'];
			return redirect('/jointer/tim_party')->with('alerts_tele', $urgent_msg);
		}
		$get_jointer_nik = JointerModel::get_jointer_user(session('auth')->mitra);
		return view('Jointer.party_create', compact("get_jointer_nik") );
	}

	public function save_party(Request $req)
	{
		JointerModel::save_party($req);
		$urgent_msg['msg'] = ['type' => 'success', 'text' => 'Berhasil Membuat Party!!'];
		return redirect('/jointer/tim_party')->with('alerts_tele', $urgent_msg);
	}

	public function edit_party()
	{
		$get_jointer_nik = JointerModel::get_jointer_user();
		$load_data_tim = JointerModel::load_tim();

		return view('Jointer.party_create', compact('get_jointer_nik', 'load_data_tim') );
	}

	public function update_party(Request $req)
	{
		JointerModel::update_party($req);
		$urgent_msg['msg'] = ['type' => 'success', 'text' => 'Berhasil Ubah Party!!'];
		return redirect('/jointer/tim_party')->with('alerts_tele', $urgent_msg);
	}

	public function dismiss_party()
	{
		JointerModel::dismiss_party();
		$urgent_msg['msg'] = ['type' => 'danger', 'text' => 'Berhasil Membubarkan Party!!'];
		return redirect('/jointer/tim_party')->with('alerts_tele', $urgent_msg);
	}

	public function join_party($id)
	{
		JointerModel::join_party($id);
		$urgent_msg['msg'] = ['type' => 'success', 'text' => 'Berhasil Request Party!!'];
		return redirect('/jointer/tim_party')->with('alerts_tele', $urgent_msg);
	}

	public function abort_join($id)
	{
		JointerModel::abort_join($id);
		$urgent_msg['msg'] = ['type' => 'success', 'text' => 'Sukses Keluar Party!!'];
		return redirect('/jointer/tim_party')->with('alerts_tele', $urgent_msg);
	}

	public function approve_join($id)
	{
		JointerModel::approve_join($id);
		$urgent_msg['msg'] = ['type' => 'success', 'text' => 'Berhasil Menerima Unit!!'];
		return redirect('/jointer/tim_party')->with('alerts_tele', $urgent_msg);
	}

	public function unit_join($id)
	{
		JointerModel::unit_join($id);
		$urgent_msg['msg'] = ['type' => 'success', 'text' => 'Berhasil!!'];
		return redirect('/jointer/tim_party')->with('alerts_tele', $urgent_msg);
	}

	public function out_party()
	{
		JointerModel::out_party();
		$urgent_msg['msg'] = ['type' => 'success', 'text' => 'Berhasil Keluar Dari Tim!!'];
		return redirect('/jointer/tim_party')->with('alerts_tele', $urgent_msg);
	}

	// public function jointer_age($date)
	// {
	// 	$call_sql = JointerModel::umur_jointer($date);
	// 	$data = [];

	// 	foreach($call_sql['get_all_user'] as $v)
	// 	{
	// 		$data['pickup'][$v->nik]['id_regu']     = $v->id_regu;
	// 		$data['pickup'][$v->nik]['uraian']      = $v->uraian;
	// 		$data['pickup'][$v->nik]['mitra_amija'] = $v->mitra_amija_pt;

	// 		$data['progress'][$v->nik]['id_regu']     = $v->id_regu;
	// 		$data['progress'][$v->nik]['uraian']      = $v->uraian;
	// 		$data['progress'][$v->nik]['mitra_amija'] = $v->mitra_amija_pt;

	// 		if(!isset($data[$v->nik]['isi']) )
	// 		{
	// 			$data['pickup'][$v->nik]['isi']['total'] = 0;
	// 			$data['pickup'][$v->nik]['isi']['_1']    = 0;
	// 			$data['pickup'][$v->nik]['isi']['1_3']   = 0;
	// 			$data['pickup'][$v->nik]['isi']['4_7']   = 0;
	// 			$data['pickup'][$v->nik]['isi']['7_']    = 0;

	// 			$data['progress'][$v->nik]['isi']['total'] = 0;
	// 			$data['progress'][$v->nik]['isi']['_1']    = 0;
	// 			$data['progress'][$v->nik]['isi']['1_3']   = 0;
	// 			$data['progress'][$v->nik]['isi']['4_7']   = 0;
	// 			$data['progress'][$v->nik]['isi']['7_']    = 0;
	// 		}
	// 	}

	// 	$data['pickup'] = array_values($data['pickup']);
	// 	$data['progress'] = array_values($data['progress']);

	// 	foreach($call_sql['jointer_marina'] as $v)
	// 	{
	// 		$date2 = strtotime(date('Y-m-d H:i:s') );

	// 		$find_k_pick = array_search($v->regu_id, array_column($data['pickup'], 'id_regu') );

	// 		if($find_k_pick !== FALSE)
	// 		{
	// 			if(is_null($v->id) )
	// 			{
	// 				$date_progg = strtotime($v->tgl_selesai ?? $v->modified_at);
	// 				$seconds_prog = $date2 - $date_progg;

	// 				$daily_progg = abs($seconds_prog / (60 * 60 * 24) );

	// 				$data['pickup'][$find_k_pick]['isi']['total'] += 1;

	// 				if($daily_progg < 1 )
	// 				{
	// 					$data['pickup'][$find_k_pick]['isi']['_1'] += 1;
	// 				}
	// 				elseif($daily_progg >= 1 && $daily_progg <= 3 )
	// 				{
	// 					$data['pickup'][$find_k_pick]['isi']['1_3'] += 1;
	// 				}
	// 				elseif($daily_progg > 3 && $daily_progg <= 7)
	// 				{
	// 					$data['pickup'][$find_k_pick]['isi']['4_7'] += 1;
	// 				}
	// 				elseif($daily_progg > 7)
	// 				{
	// 					$data['pickup'][$find_k_pick]['isi']['7_'] += 1;
	// 				}
	// 			}
	// 		}

	// 		$find_k_prog = array_search($v->regu_id, array_column($data['progress'], 'id_regu') );

	// 		if($find_k_prog !== FALSE)
	// 		{
	// 			if(is_null($v->lt_status) && $v->id)
	// 			{
	// 				$date_progg = strtotime($v->tgl_selesai ?? $v->modified_at);
	// 				$seconds_prog = $date2 - $date_progg;

	// 				$daily_progg = abs($seconds_prog / (60 * 60 * 24) );

	// 				$data['progress'][$find_k_prog]['isi']['total'] += 1;

	// 				if($daily_progg < 1 )
	// 				{
	// 					$data['progress'][$find_k_prog]['isi']['_1'] += 1;
	// 				}
	// 				elseif($daily_progg >= 1 && $daily_progg <= 3 )
	// 				{
	// 					$data['progress'][$find_k_prog]['isi']['1_3'] += 1;
	// 				}
	// 				elseif($daily_progg > 3 && $daily_progg <= 7)
	// 				{
	// 					$data['progress'][$find_k_prog]['isi']['4_7'] += 1;
	// 				}
	// 				elseif($daily_progg > 7)
	// 				{
	// 					$data['progress'][$find_k_prog]['isi']['7_'] += 1;
	// 				}
	// 			}
	// 		}
	// 	}

	// 	foreach($call_sql['jointer_bts'] as $v)
	// 	{
	// 		$date2 = strtotime(date('Y-m-d H:i:s') );

	// 		$find_k_pick = array_search($v->regu_id, array_column($data['pickup'], 'id_regu') );

	// 		if($find_k_pick !== FALSE)
	// 		{
	// 			if(is_null($v->id) )
	// 			{
	// 				$date_progg = strtotime($v->tgl_selesai ?? $v->modified_at);
	// 				$seconds_prog = $date2 - $date_progg;

	// 				$daily_progg = abs($seconds_prog / (60 * 60 * 24) );

	// 				$data['pickup'][$find_k_pick]['isi']['total'] += 1;

	// 				if($daily_progg < 1 )
	// 				{
	// 					$data['pickup'][$find_k_pick]['isi']['_1'] += 1;
	// 				}
	// 				elseif($daily_progg >= 1 && $daily_progg <= 3 )
	// 				{
	// 					$data['pickup'][$find_k_pick]['isi']['1_3'] += 1;
	// 				}
	// 				elseif($daily_progg > 3 && $daily_progg <= 7)
	// 				{
	// 					$data['pickup'][$find_k_pick]['isi']['4_7'] += 1;
	// 				}
	// 				elseif($daily_progg > 7)
	// 				{
	// 					$data['pickup'][$find_k_pick]['isi']['7_'] += 1;
	// 				}
	// 			}
	// 		}

	// 		$find_k_prog = array_search($v->regu_id, array_column($data['progress'], 'id_regu') );

	// 		if($find_k_prog !== FALSE)
	// 		{
	// 			if(is_null($v->lt_status) && $v->id)
	// 			{
	// 				$date_progg = strtotime($v->tgl_selesai ?? $v->modified_at);
	// 				$seconds_prog = $date2 - $date_progg;

	// 				$daily_progg = abs($seconds_prog / (60 * 60 * 24) );

	// 				$data['progress'][$find_k_prog]['isi']['total'] += 1;

	// 				if($daily_progg < 1 )
	// 				{
	// 					$data['progress'][$find_k_prog]['isi']['_1'] += 1;
	// 				}
	// 				elseif($daily_progg >= 1 && $daily_progg <= 3 )
	// 				{
	// 					$data['progress'][$find_k_prog]['isi']['1_3'] += 1;
	// 				}
	// 				elseif($daily_progg > 3 && $daily_progg <= 7)
	// 				{
	// 					$data['progress'][$find_k_prog]['isi']['4_7'] += 1;
	// 				}
	// 				elseif($daily_progg > 7)
	// 				{
	// 					$data['progress'][$find_k_prog]['isi']['7_'] += 1;
	// 				}
	// 			}
	// 		}
	// 	}

	// 	foreach($call_sql['jointer_pt2'] as $v)
	// 	{
	// 		$date2 = strtotime(date('Y-m-d H:i:s') );

	// 		$find_k_pick = array_search($v->regu_id, array_column($data['pickup'], 'id_regu') );

	// 		if($find_k_pick !== FALSE)
	// 		{
	// 			if(is_null($v->id) )
	// 			{
	// 				$date_progg = strtotime($v->tgl_selesai ?? $v->modified_at);
	// 				$seconds_prog = $date2 - $date_progg;

	// 				$daily_progg = abs($seconds_prog / (60 * 60 * 24) );

	// 				$data['pickup'][$find_k_pick]['isi']['total'] += 1;

	// 				if($daily_progg < 1 )
	// 				{
	// 					$data['pickup'][$find_k_pick]['isi']['_1'] += 1;
	// 				}
	// 				elseif($daily_progg >= 1 && $daily_progg <= 3 )
	// 				{
	// 					$data['pickup'][$find_k_pick]['isi']['1_3'] += 1;
	// 				}
	// 				elseif($daily_progg > 3 && $daily_progg <= 7)
	// 				{
	// 					$data['pickup'][$find_k_pick]['isi']['4_7'] += 1;
	// 				}
	// 				elseif($daily_progg > 7)
	// 				{
	// 					$data['pickup'][$find_k_pick]['isi']['7_'] += 1;
	// 				}
	// 			}
	// 		}

	// 		$find_k_prog = array_search($v->regu_id, array_column($data['progress'], 'id_regu') );

	// 		if($find_k_prog !== FALSE)
	// 		{
	// 			if(is_null($v->lt_status) && $v->id)
	// 			{
	// 				$date_progg = strtotime($v->tgl_selesai ?? $v->modified_at);
	// 				$seconds_prog = $date2 - $date_progg;

	// 				$daily_progg = abs($seconds_prog / (60 * 60 * 24) );

	// 				$data['progress'][$find_k_prog]['isi']['total'] += 1;

	// 				if($daily_progg < 1 )
	// 				{
	// 					$data['progress'][$find_k_prog]['isi']['_1'] += 1;
	// 				}
	// 				elseif($daily_progg >= 1 && $daily_progg <= 3 )
	// 				{
	// 					$data['progress'][$find_k_prog]['isi']['1_3'] += 1;
	// 				}
	// 				elseif($daily_progg > 3 && $daily_progg <= 7)
	// 				{
	// 					$data['progress'][$find_k_prog]['isi']['4_7'] += 1;
	// 				}
	// 				elseif($daily_progg > 7)
	// 				{
	// 					$data['progress'][$find_k_prog]['isi']['7_'] += 1;
	// 				}
	// 			}
	// 		}
	// 	}

	// 	return view('Report.jointer_report.umur_jointer', compact('data') );
	// }

	public function matrix($date)
	{
		$call_sql = JointerModel::get_matrix($date);

		$data = [];

		foreach($call_sql as $v)
		{
			if(!isset($data[$v->status_pst]) )
			{
				$data[$v->status_pst]['saldo']   = 0;
				$data[$v->status_pst]['in_tech'] = 0;
				$data[$v->status_pst]['selesai'] = 0;
				$data[$v->status_pst]['kendala'] = 0;
			}

			if($v->lt_status == 'Selesai')
			{
				$data[$v->status_pst]['selesai'] += 1;
			}

			if($v->lt_status == 'Kendala')
			{
				$data[$v->status_pst]['kendala'] += 1;
			}
		}

		return view('Report.jointer_report.matrix', compact('data') );
	}

	public function detail_data($datel, $sto, $mitra, $jenis, $tgl)
	{
		$data = JointerModel::get_matrix($tgl, $mitra, $datel, $sto, $jenis);
		return view('Report.jointer_report.detail', compact('data') );
	}

	public function sync_regu()
	{
		$nik = session('auth')->id_user;
		JointerModel::sync_regu_log_party($nik);
		$urgent_msg['msg'] = ['type' => 'success', 'text' => 'Berhasil Sync dengan Tim Sekarang!!'];
		return redirect('/jointer/tim_party')->with('alerts_tele', $urgent_msg);
	}
}