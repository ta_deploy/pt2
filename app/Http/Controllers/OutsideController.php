<?php
namespace App\Http\Controllers;

use App\DA\AdminModel;
use App\DA\ReportModel;
use App\DA\TeknisiModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Facades\Excel;

date_default_timezone_set("Asia/Makassar");

class OutsideController extends Controller
{
	public function dashboard_outside_pt2()
	{
		$data_nog = ReportModel::list_nog();
		$data_pt2_psb = ReportModel::list_pt2_psb();

		$datel = ReportModel::get_all_datel();
		$datel = json_decode(json_encode($datel), TRUE);

		$list_nog = $list_psb = [];

		foreach($data_nog as $v)
		{
			$find_k =array_search($v->sto_nog, array_column($datel, 'sto') );
			$dt = 'KOSONG';

			if($find_k !== FALSE)
			{
				$dt = $datel[$find_k]['datel'];
			}

			if(!isset($list_nog[$dt]['sto'][$v->sto_nog]) )
			{
				$list_nog[$dt]['sto'][$v->sto_nog] = 0;
			}

			$list_nog[$dt]['sto'][$v->sto_nog] += 1;
		}

		foreach($data_pt2_psb as $v)
		{
			$find_k =array_search($v->sto, array_column($datel, 'sto') );
			$dt = 'KOSONG';

			if($find_k !== FALSE)
			{
				$dt = $datel[$find_k]['datel'];
			}

			if(!isset($list_psb[$dt]['sto'][$v->sto]) )
			{
				$list_psb[$dt]['sto'][$v->sto] = 0;
			}

			$list_psb[$dt]['sto'][$v->sto] += 1;
		}

		return view('Outside.dashboard_Nog', compact('list_nog', 'list_psb') );
	}

  public function input_Nog($jenis, $id, AdminModel $adminmodel)
	{
		$distinct_regu   = AdminModel::list_regu_pt2('aktif');
		$admincontroller = new AdminController();
		$photodispatch   = $admincontroller->photodispatch;

		$PO   = AdminModel::po_select();
		$sto  = AdminModel::get_sto();
		$odc  = AdminModel::get_odc();
		$j_or = AdminModel::status();

		switch ($jenis) {
			case 'NOG':
				$data_d = ReportModel::getdetail_nog($id);
				$sto_split = explode('-', $data_d->nama_odp)[1];

				$data_d->sto = $sto_split;

				$data_d->odp_nama     = $data_d->nama_odp;
				$data_d->odp_koor     = $data_d->koordinat_odp;
				$data_d->aspl_nama    = null;
				$data_d->aspl_koor    = null;
				$data_d->project_name = 'NOG dari '. $data_d->jenis_order . ' port '. $data_d->jml_port;
				$data_d->catatan_HD = 'NOG dari '. $data_d->jenis_order . ' port '. $data_d->jml_port;
				$jenis= 'NOG MARINA';
			break;
			case 'jointer':
				$data_d = ReportModel::getdetail_jointer($id);
				$sto_split = explode('-', $data_d->nama_odp)[1];
				$data_d->sto = $sto_split;

				$data_d->odp_nama     = $data_d->nama_odp;
				$data_d->odp_koor     = $data_d->koordinat;
				$data_d->aspl_nama    = null;
				$data_d->aspl_koor    = null;

				$data_d->project_name = 'JOINTER dari '. $data_d->nama_order;
				$data_d->catatan_HD = 'JOINTER dari '. $data_d->nama_order;
				$jenis = 'JOINTER MARINA';
			break;
			case 'PSB':
				$data_d = ReportModel::getdetail_psb($id);

				if($data_d->nama_odp)
				{
					$sto_split = explode('-', $data_d->nama_odp)[1];
					$data_d->sto = $sto_split;

					$data_d->odp_nama = $data_d->nama_odp;
					$data_d->odp_koor = $data_d->kordinat_odp;
				}

				$data_d->aspl_nama    = null;
				$data_d->aspl_koor    = null;
				$data_d->project_name = $data_d->orderName;
				$data_d->nomor_sc     = $data_d->orderId;
				$jenis= 'PSB';
			break;
			case 'KPRO_SIMPLE':
				$data_d = ReportModel::getdetail_krpo_simple($id);

				if($data_d->odp)
				{
					$odp = explode(' ', $data_d->odp)[0];
					$sto_split = explode('-', $odp)[1];

					$data_d->sto = $sto_split;

					$data_d->odp_nama = $odp;
					$data_d->odp_koor = $data_d->gps_latitude .','. $data_d->gps_longitude;
				}

				$data_d->aspl_nama         = null;
				$data_d->aspl_koor         = null;
				$data_d->project_name      = $data_d->status_resume;
				$data_d->kategory_non_unsc = 6;
				$data_d->nomor_sc          = $data_d->order_id;
				$jenis= 'KPRO SIMPLE';
			break;
		}

		return view('Admin.order_non_unsc', ['regu' => $distinct_regu, 'po' => $PO, 'sto' => $sto, 'odc' => $odc, 'data_d' => $data_d, 'id' => $id], compact('photodispatch' , 'j_or', 'jenis'));
	}

  public function save_Nog(Request $req, $jenis, $id)
	{
		switch ($jenis) {
			case 'NOG':
				$main_data = ['jenis' => $jenis, 'data' => ReportModel::getdetail_nog($id)];
				AdminModel::save_nog($req, $id);
			break;
			case 'PSB':
				$main_data = ['jenis' => $jenis, 'data' => ReportModel::getdetail_psb($id)];
				AdminModel::save_nog($req, $id);
			break;
			case 'KPRO_SIMPLE':
				$main_data = ['jenis' => $jenis, 'data' => ReportModel::getdetail_krpo_simple($id)];
			break;
		}

		$msg = AdminModel::save_dispatch_non_unsc($req, $id, $main_data);
		return redirect("/home/" . date('Y') )->with('alerts_tele', $msg);
	}

  public function list_outside_nog($jenis, $datel, $sto)
	{
		if($jenis == 'nog_m')
		{
			$data = ReportModel::list_nog($datel, $sto);
			$judul = 'NOG MARINA';
		}
		else
		{
			$data = ReportModel::list_pt2_psb($datel, $sto);
			$judul = 'PSB PT-2';
		}

		$datel = urldecode($datel);
		return view('Outside.list_not_outside', compact('data', 'jenis', 'datel', 'sto', 'judul') );
	}

	public function list_outside_pt2($jenis, $regu_jp, $datel, $sto)
	{
		$data = ReportModel::get_kpro_pt2($jenis, $regu_jp, $datel, $sto);

		$judul = 'PSB PT-2';

		if($jenis == 'kpro')
		{
			$judul = 'KPRO X PT-2';
			$data = $data['kpro'];
		}
		else
		{
			$data = $data['pt2'];
		}

		return view('Outside.list_outside_pt2', compact('data', 'jenis', 'judul', 'datel', 'sto') );
	}

	public function list_pt2_kpro()
	{
		$data = ReportModel::get_kpro_pt2();

		$datel = ReportModel::get_all_datel();
		$datel = json_decode(json_encode($datel), TRUE);

		foreach($data['pt2'] as $v)
		{
			$find_k = array_search($v['sto_kpro'], array_column($datel, 'sto') );
			$dt = 'KOSONG';

			if($find_k !== FALSE)
			{
				$dt = $datel[$find_k]['datel'];
			}

			if(!isset($list_pt2[$v['regu_name'] ][$dt]['sto'][$v['sto_kpro'] ]) )
			{
				$list_pt2[$v['regu_name'] ][$dt]['sto'][$v['sto_kpro'] ] = 0;
			}

			$list_pt2[$v['regu_name'] ][$dt]['sto'][$v['sto_kpro'] ] += 1;
		}

		foreach($data['kpro'] as $v)
		{
			$find_k = array_search($v->sto, array_column($datel, 'sto') );
			$dt = 'KOSONG';

			if($find_k !== FALSE)
			{
				$dt = $datel[$find_k]['datel'];
			}

			$jenis_psb = $v->jenispb == '' ? 'Kosong' : $v->jenispb;

			if(!isset($list_kpro[$jenis_psb][$dt]['sto'][$v->sto]) )
			{
				$list_kpro[$jenis_psb][$dt]['sto'][$v->sto] = 0;
			}

			$list_kpro[$jenis_psb][$dt]['sto'][$v->sto] += 1;
		}

		return view('Outside.dashboard_kpro', compact('list_pt2', 'list_kpro') );
	}

	public function jointer_pt2()
	{
		$data_nik1 = AdminModel::check_user_jointer(session('auth')->id_user);

		if(session('auth')->id_user != 18940469)
		{
			if($data_nik1->pt2_level != 0)
			{
				$urgent_msg['msg'] = ['type' => 'danger', 'text' => 'Anda Bukan Tim Jointer!!'];
				return redirect('/jointer/tim_party')->with('alerts_tele', $urgent_msg);
			}
		}

		$datel = ReportModel::get_all_datel();
		$datel = json_decode(json_encode($datel), TRUE);

		$data_jointer_pt2 = ReportModel::list_jointer_pt2();

		$data_jointer = ReportModel::list_jointer_marina();

		$list_jointer_marina = $list_jointer_bts = $list_jointer_pt2 = [];

		foreach($data_jointer_pt2 as $v)
		{
			$find_k = array_search($v->sto, array_column($datel, 'sto') );
			$dt = 'KOSONG';

			if($find_k !== FALSE)
			{
				$dt = $datel[$find_k]['datel'];
			}

			if(!isset($list_jointer_pt2[$dt]['sto'][$v->sto]) )
			{
				$list_jointer_pt2[$dt]['sto'][$v->sto] = 0;
			}

			$list_jointer_pt2[$dt]['sto'][$v->sto] += 1;
		}

		foreach($data_jointer as $v)
		{
			$find_k = array_search($v->sto, array_column($datel, 'sto') );
			$dt = 'KOSONG';

			if($find_k !== FALSE)
			{
				$dt = $datel[$find_k]['datel'];
			}

			if(!isset($list_jointer_marina[$dt]['sto'][$v->sto]) )
			{
				$list_jointer_marina[$dt]['sto'][$v->sto] = 0;
			}

			$list_jointer_marina[$dt]['sto'][$v->sto] += 1;
		}

		$data_jointer_bts = ReportModel::list_jointer_bts();

		foreach($data_jointer_bts as $v)
		{
			$find_k = array_search($v->sto, array_column($datel, 'sto') );
			$dt = 'KOSONG';

			if($find_k !== FALSE)
			{
				$dt = $datel[$find_k]['datel'];
			}

			if(!isset($list_jointer_bts[$dt]['sto'][$v->sto]) )
			{
				$list_jointer_bts[$dt]['sto'][$v->sto] = 0;
			}

			$list_jointer_bts[$dt]['sto'][$v->sto] += 1;
		}

		return view('Outside.dashboard_jointer', compact('list_jointer_marina', 'list_jointer_bts', 'list_jointer_pt2') );
	}

	public function jointer_pt2_list($jenis, $datel, $sto, $odc)
	{
		$data_nik1 = AdminModel::check_user_jointer(session('auth')->id_user);

		if(session('auth')->id_user != 18940469)
		{
			if($data_nik1->pt2_level != 0)
			{
				$urgent_msg['msg'] = ['type' => 'danger', 'text' => 'Anda Bukan Tim Jointer!!'];
				return redirect('/jointer/tim_party')->with('alerts_tele', $urgent_msg);
			}
		}

		if(strcasecmp($jenis, 'marina') == 0)
		{
			$data = ReportModel::list_jointer_marina($datel, $sto, $odc);
			$judul = 'MARINA - JOINTER';
			$datel = urldecode($datel);
		}
		elseif(strcasecmp($jenis, 'bts') == 0)
		{
			$data = ReportModel::list_jointer_bts($datel, $sto, $odc);
			$judul = 'BTS - JOINTER';
			$datel = urldecode($datel);
		}
		elseif(strcasecmp($jenis, 'pt2') == 0)
		{
			$data = ReportModel::list_jointer_pt2($datel, $sto, $odc);
			$judul = 'PT-2 - JOINTER';
			$datel = urldecode($datel);
		}

		return view('Outside.list_jointer', compact('data',  'datel', 'sto', 'judul') );
	}

	public function pickup_jointer($jenis, $id)
	{
		$data_nik1 = AdminModel::check_user_jointer(session('auth')->id_user);

		if(session('auth')->id_user != 18940469)
		{
			if($data_nik1->pt2_level != 0)
			{
				$urgent_msg['msg'] = ['type' => 'danger', 'text' => 'Anda Bukan Tim Jointer!!'];
				return redirect('/jointer/tim_party')->with('alerts_tele', $urgent_msg);
			}
		}

		$check_limit = AdminModel::check_limit_jointer(date('Y-m'), session('auth')->id_user);

		if(count($check_limit) > 50)
		{
			$urgent_msg['msg'] = ['type' => 'danger', 'text' => 'GAGAL!! Pekerjaan Sudah Diambil Sebanyak '. count($check_limit) .' Kali Bulan Ini!!'];
			return redirect('/jointer/tim_party')->with('alerts_tele', $urgent_msg);
		}

		$id_join = TeknisiModel::pickup_jointer($jenis, $id);

		return redirect("/laporan/$id_join");
	}

	public function jointer_pt2_list_odc($jenis, $datel, $sto)
	{
		$data_nik1 = AdminModel::check_user_jointer(session('auth')->id_user);

		$fd = [];

		if(session('auth')->id_user != 18940469)
		{
			if($data_nik1->pt2_level != 0)
			{
				$urgent_msg['msg'] = ['type' => 'danger', 'text' => 'Anda Bukan Tim Jointer!!'];
				return redirect('/jointer/tim_party')->with('alerts_tele', $urgent_msg);
			}
		}

		if(strcasecmp($jenis, 'marina') == 0)
		{
			$data = ReportModel::list_jointer_marina($datel, $sto);
			// dd($data);
			$datel = urldecode($datel);

			foreach($data as $v)
			{
				$odc = explode('/', $v->nama_odp)[0];
				$odc = explode('-', $odc)[2] ?? 'Kosong';

				$fd[$datel]['datel'] = $datel;

				if(!isset($fd[$datel]['list'][$odc]) )
				{
					$fd[$datel]['list'][$odc] = 0;
				}

				$fd[$datel]['list'][$odc] += 1;
			}

			$judul = 'MARINA - JOINTER';
			$sub_judul = 'marina';
		}
		elseif(strcasecmp($jenis, 'bts') == 0)
		{
			$data = ReportModel::list_jointer_bts($datel, $sto);
			$datel = urldecode($datel);

			foreach($data as $v)
			{
				$odc = explode('/', $v->nama_odp)[0];
				$odc = explode('-', $odc)[2] ?? 'Kosong';

				$fd[$datel]['datel'] = $datel;

				if(!isset($fd[$datel]['list'][$odc]) )
				{
					$fd[$datel]['list'][$odc] = 0;
				}

				$fd[$datel]['list'][$odc] += 1;
			}

			$judul = 'BTS - JOINTER';
			$sub_judul = 'bts';
		}
		elseif(strcasecmp($jenis, 'pt2') == 0)
		{
			$data = ReportModel::list_jointer_pt2($datel, $sto);
			$datel = urldecode($datel);

			foreach($data as $v)
			{
				$odc = explode('/', $v->odp_nama)[0];
				$odc = explode('-', $odc)[2] ?? 'Kosong';

				$fd[$datel]['datel'] = $datel;

				if(!isset($fd[$datel]['list'][$odc]) )
				{
					$fd[$datel]['list'][$odc] = 0;
				}

				$fd[$datel]['list'][$odc] += 1;
			}

			$judul = 'PT-2 - JOINTER';
			$sub_judul = 'pt2';
		}

		return view('Outside.list_jointer_odc', compact('fd',  'datel', 'sto', 'judul', 'sub_judul') );
	}
}