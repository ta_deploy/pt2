<?php
namespace App\Http\Controllers;

use App\DA\AdminModel;
use App\DA\ReportModel;
use App\DA\TeknisiModel;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Facades\Excel;
use DB;

date_default_timezone_set("Asia/Makassar");
class AdminController extends Controller
{
	public $photodispatch = ['KML', 'Mcore_1', 'Mcore_2', 'Mcore_3'];

	public function download_photo($id)
	{
		return Adminmodel::download_photo_categ($id);
	}

	public function download_pdf_rfc($id){
	  return Adminmodel::download_pdf_rfc_teknisi($id);
	}

	public function edit_order_nonun_S(AdminModel $adminmodel, $id)
	{
		$distinct_regu = AdminModel::list_regu_pt2('aktif');
		$photodispatch = $this->photodispatch;
		$PO = AdminModel::po_select();
		$sto = $adminmodel->sto();
		$data_d = AdminModel::pt2MasterByid($id);
		$jenis = 'edit data';
		return view('Admin.special_order_unsc', ['regu' => $distinct_regu, 'po' => $PO, 'sto' => $sto, 'id' => $id, 'data_d' => $data_d], compact('photodispatch', 'jenis'));
	}

	public function order_nonun_S(AdminModel $adminmodel, $id)
	{
		$distinct_regu = AdminModel::list_regu_pt2('aktif');
		$photodispatch = $this->photodispatch;
		$PO = AdminModel::po_select();
		$sto = $adminmodel->sto();
		$data_d = AdminModel::add_unsc_psb($id);
		$jenis = 'orderan unsc';
		return view('Admin.special_order_unsc', ['regu' => $distinct_regu, 'po' => $PO, 'sto' => $sto, 'id' => $id, 'data_d' => $data_d], compact('photodispatch', 'jenis'));
	}

	public function order_pt2spl(AdminModel $adminmodel, $id)
	{
		//orderan khusus dari sdi yg pt2spl
		$distinct_regu = AdminModel::list_regu_pt2('aktif');
		$photodispatch = $this->photodispatch;
		$PO = AdminModel::po_select();
		$sto = $adminmodel->sto();
		$data_d = AdminModel::pt2MasterByid($id);
		$jenis = 'spl';
		return view('Admin.special_order_unsc', ['regu' => $distinct_regu, 'po' => $PO, 'sto' => $sto, 'id' => $id, 'data_d' => $data_d], compact('photodispatch', 'jenis'));
	}

	public function order_nonun(AdminModel $adminmodel)
	{
		$distinct_regu = AdminModel::list_regu_pt2('aktif');
		$photodispatch = $this->photodispatch;
		$PO = AdminModel::po_select();
		// $sto = $adminmodel->sto();
		$sto = AdminModel::get_sto();
		$odc = AdminModel::get_odc();
		$j_or = AdminModel::status();

		return view('Admin.order_non_unsc', ['regu' => $distinct_regu, 'po' => $PO, 'sto' => $sto, 'odc' => $odc, 'data_d' => ''], compact('photodispatch', 'j_or'));
	}

	public function booking_odp($jenis_terminal, $jenis_pekerjaan, $sto, $odc, $nama_lop, $regu, $selected)
	{
		$curl = curl_init();

		$post = [
			'jenis_pekerjaan' => $jenis_pekerjaan,
			'nama_lop'        => $nama_lop,
			'sto'             => $sto,
			'odc'             => $odc,
			'id_regu'         => $regu,
			'terminal'        => $jenis_terminal,
			'selected_item'   => $selected,
		];

		curl_setopt_array($curl, [
			CURLOPT_URL            => 'https://promitos.tomman.app/API_PT2_BOOKING',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_POSTFIELDS     => $post,
			CURLOPT_ENCODING       => '',
			CURLOPT_MAXREDIRS      => 10,
			CURLOPT_TIMEOUT        => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST  => 'POST',
		]);

		$response = curl_exec($curl);

		curl_close($curl);
		return $response;
	}

	public function delete_booking($jenis, $item)
	{
		$curl = curl_init();

		$post = [
			'jenis_pekerjaan' => $jenis,
			'delete_item'     => $item,
		];

		curl_setopt_array($curl, [
			CURLOPT_URL            => 'https://promitos.tomman.app/API_PT2_DELETE_BOOKING',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_POSTFIELDS     => $post,
			CURLOPT_ENCODING       => '',
			CURLOPT_MAXREDIRS      => 10,
			CURLOPT_TIMEOUT        => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST  => 'POST',
		]);

		$response = curl_exec($curl);

		curl_close($curl);
		return $response;
	}

	public function edit_order_nonun($id, AdminModel $adminmodel)
	{
		$distinct_regu = AdminModel::list_regu_pt2('aktif');
		$photodispatch = $this->photodispatch;
		$PO = AdminModel::po_select();
		$data_d = $adminmodel->pt2MasterByid($id);
		// $sto = $adminmodel->sto();
		$sto = AdminModel::get_sto();
		$odc = AdminModel::get_odc();
		$j_or = AdminModel::status();
		$jenis= $data_d->jenis_wo;

		$url = 'https://promitos.tomman.app/download_kml/'.$data_d->id_hero;
		$ch = curl_init($url);
    curl_setopt($ch, CURLOPT_NOBODY, true);
    curl_exec($ch);
    $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    if ($code == 200) {
			$status_file = true;
    } else {
			$status_file = false;
    }

    curl_close($ch);

		if($status_file)
		{
			$path = public_path() . '/upload/pt_2_3/' . $id;

			if (!file_exists($path) )
			{
				if (!mkdir($path, 0770, true))
				{
					return 'gagal menyiapkan folder foto evidence';
				}
			}

			$ext = get_headers($url)[5];
			$ext = explode('.', $ext)[1];
			$ext = substr($ext, 0, -1);

			file_put_contents(public_path() . '/upload/pt_2_3/' . $id . '/File KML.'.$ext, fopen($url, 'r') );
		}

		return view('Admin.order_non_unsc', ['regu' => $distinct_regu, 'po' => $PO, 'sto' => $sto, 'odc' => $odc, 'data_d' => $data_d, 'id' => $id], compact('photodispatch' , 'j_or', 'status_file') );
	}

	public function find_one_odp($value, $value2)
	{
		//mencari nama odp di input
		return \Response::json(AdminModel::odp_unsc_search_first($value, $value2));
	}

	public function find_one_odp_layout(Request $req)
	{
		$search = $req->data;
		$data = AdminModel::pt2MasterByid($search);
		$photodispatch = $photodispatch = $this->photodispatch;
		// $photo = ['QRcode_Tiang', 'ODP', 'QRcode_ODP', 'QRcode_SPL', 'Redaman_IN', 'Redaman_OUT', 'Port_Feeder', 'Port_Distribusi', 'OLT', 'FTM_2', 'O_side', 'E_side', 'ODC', 'Progress', 'Photo_Rfc', 'Qc_1', 'Qc_2', 'Qc_3', 'Photo_Rfc_2'];
		$tc = New TeknisiController();
		$photo = $tc->photo;
		if ($req->angka == 1)
		{
			return view('Tools.content_find_layout', compact('data', 'photodispatch', 'photo'));
		}
		elseif ($req->angka == 0)
		{
			return \Response::json(AdminModel::pt2MasterByid($search));
		}
	}

	public function search_Data(Request $req)
	{
		$data = AdminModel::search_data($req->search);
		return view('Tools.result_search', ['cari' => $req->search], compact('data'));
	}

	public function search_DataID(Request $req)
	{
		$data = AdminModel::search_data($req->search, 'id');
		return view('Tools.result_search', ['cari' => $req->search], compact('data'));
	}

	public function save_dispatch_unsc(Request $req, $value)
	{
		$msg = AdminModel::save_dispatch_unsc($req, $value);
		return redirect("/home/" . date('Y'))->with('alerts_tele', $msg);
	}

	public function save_dispatch_nonunsc(Request $req, $value)
	{
		$msg = AdminModel::save_dispatch_non_unsc($req, $value);
		return redirect("/home/" . date('Y'))->with('alerts_tele', $msg);
	}

	public function showregu(AdminModel $adminmodel)
	{
		$child_list = AdminModel::list_regu_pt2('aktif');
		$main_list = AdminModel::parent_regu();

		foreach ($main_list as $key => $value)
		{
			$list[$key] = $value;

			foreach ($child_list as $C_k => $C_V)
			{
				if ($value->TL == $C_V->TL)
				{
					$list[$key]->list[] = $C_V;
				}
			}
		}

		$regu_nonaktif = AdminModel::list_regu_pt2('notaktif');
		return view('Admin.list_regu', ['list' => $list, 'disabled' => $regu_nonaktif]);
	}

	public function delete_regu($id)
	{
		$adminmodel = new AdminModel();
		$adminmodel->delete_Regu($id);
		$get_list = AdminModel::get_history_regu($id);
		return view('Admin.detail_delete_regu', compact('get_list'));
	}

	public function edit_regu($id, Adminmodel $adminmodel)
	{
		$edit = $adminmodel->edit_regu($id);
		$sto = $adminmodel->sto();
		$mitra = AdminModel::mitra_pt2();
		return view('Admin.edit_regu', ['edit' => $edit, 'sto' => $sto], compact('mitra'));
	}

	public function reactive_regu($id)
	{
		AdminModel::reactive_regu($id);
	}

	public function update_regu(Request $req, $id, AdminModel $adminmodel)
	{
		$sto = $adminmodel->sto();
		$update = $adminmodel->update_regu($id, $req);
		return redirect('/regu/list');
	}

	public function add_regu(AdminModel $adminmodel)
	{
		$sto = $adminmodel->sto();
		$mitra = AdminModel::mitra_pt2();
		return view('Admin.edit_regu', ['sto' => $sto], compact('mitra'));
	}

	public function save_regu(Request $req, AdminModel $adminmodel)
	{
		$adminmodel->simpan_regu($req);
		return redirect('/regu/list');
	}

	public function laporan_harian(AdminModel $adminmodel)
	{
		$daily = date('Y-m-d');
		$daily2 = date('Y-m-d') . " 23:59:59";
		$regu = $adminmodel->lh_pt2_master($daily, $daily2);
		$odp = $adminmodel->lh_odp($daily, $daily2);

		foreach ($regu as $as => $new_regu)
		{
			$group[$as] = $new_regu;

			foreach ($odp as $no => $new_odp)
			{
				if ($group[$as]->regu == $new_odp->regu)
				{
					$group[$as]->odp[$no] = $new_odp;
				}
			}

		}
		return view('Admin.table_daily', compact('group'));
	}

	public function live_search_regu(Request $req, AdminModel $adminmodel)
	{
		$term = trim($req->q);

		if (empty($term))
		{
			return \Response::json([]);
		}

		$fetchData = $adminmodel->regu_search($term);

		$formatted_tags = [];

		foreach ($fetchData as $row)
		{
			$formatted_tags[] = ["id" => $row->id, "text" => $row->sto];
		}

		return \Response::json($formatted_tags);
	}

	public function delete_mine_data(Request $req)
	{
		AdminModel::delete_per_odp($req->data);
	}

	public function get_data_progerss($y, $m)
	{
		$year = $y;
		$month = $m;

		if (strlen($month) > 1)
		{
			$month = substr($month, -1);
		}

		if ($month > 13)
		{
			$month = 12;
		}

		$data = AdminModel::getRekap_year($year, $month);
		$detail = AdminModel::getRekap_year_detail($year, $month);

		$rekap_rinci = $detail_data = [];
		if ($data)
		{
			foreach ($data as $no => $d)
			{
				$bulan = date("F", strtotime($d->tgl));
				$rekap_rinci[$bulan][$no] = $d;
				$pecah_b = explode(' ', $d->tgl);
				$rekap_rinci[$bulan][$no]->tgl = $pecah_b[0];

				foreach ($detail as $nom => $det)
				{
					$month_num = date('Y-m', strtotime($bulan));

					if (substr($det->tgl_selesai, 0, 7) == $month_num)
					{
						$pecah_b_d = explode(' ', $det->tgl_selesai);

						if ($pecah_b_d[0] == $pecah_b[0])
						{
							$detail_data[$bulan][$pecah_b_d[0]][$nom] = $det;
						}

					}

				}

			}
		}
		// dd($detail_data);
		return view('Admin.my_team_work', compact('rekap_rinci', 'detail_data', 'bulan', 'year', 'month'));
	}

	public function send_photo_button(Request $req)
	{
		$filteredData = substr($req->data, strpos($req->data, ",") + 1);
		$unencodedData = base64_decode($filteredData);
		file_put_contents($req->name, $unencodedData);
		AdminModel::sentTele($req->name);
		return json_encode($req->name);
	}

	public function kirim_laphar(AdminModel $adminmodel)
	{
		$adminmodel->eksekusi_daily_Table();
	}

	public function ABD(AdminModel $adminmodel, $id)
	{
		$data = $adminmodel->deploybyid($id);
		//parent
		$parent = ['NO', 'STO', 'ODP', 'IP OLT', 'PORT OLT', 'OTB', '', '', '', 'FEEDER', '', 'ODC', '', '', '', '', '', 'ODP', '', '', '', 'DIST', '', 'NAMA TENNOS', 'ID VALINS'];
		//pre-parent
		$value[0] = ['', '', '', '', '', 'ODF', 'PANEL', 'PORT', 'KAP', 'NAMA', 'KAP', 'NAMA', 'PANEL', 'PORT', 'SPL', 'KAP', 'KOORDINAT', 'PANEL', 'PORT', 'SPL', 'KOORDINAT', 'NAMA', 'KAP', '', ''];
		//value
		$photo = ['Port_Feeder', 'ODC'];

		$check_path = "/upload/pt_2/teknisi/$data->id";
		$check_path_2 = "/upload2/pt_2/teknisi/$data->id";
		$note_panel_odc = $note_port_odc = $note_panel_f = $note_port_f = '';

		foreach ($photo as $input)
		{
			if (file_exists(public_path() . $check_path))
			{
				$path = "$check_path/$input";
			}
			else
			{
				$path = "$check_path_2/$input";
			}

			$th = "$path-th.jpg";
			$img = "$path.jpg";
			$nt = "$path-catatan.txt";
			$nt_panel = "$path-panel.txt";
			$flag = "";
			$name = "flag_" . $input;

			if ($input == 'ODC' && file_exists(public_path() . $nt_panel))
			{
				$note_panel_odc .= \File::get(public_path("$path-panel.txt"));
				$note_port_odc .= \File::get(public_path("$path-port.txt"));
			}

			if ($input == 'Port_Feeder' && file_exists(public_path() . $nt_panel))
			{
				$note_panel_f .= \File::get(public_path("$path-panel.txt"));
				$note_port_f .= \File::get(public_path("$path-port.txt"));
			}

		}

		$mydata['no']            = 1;
		$mydata['sto']           = $data->sto;
		$mydata['odp']           = $data->odp_nama;
		$mydata['ip_olt'] = $mydata['port_olt'] = $mydata['otb_odf'] = $mydata['otb_panel'] = $mydata['otb_port'] = $mydata['otb_kap'] = $mydata['feeder_nama'] = $mydata['feeder_kap'] = '';
		$mydata['odc_nama']      = $data->odc_nama;
		$mydata['odc_panel']     = $note_panel_odc;
		$mydata['odc_port']      = $note_port_odc;
		$mydata['odc_spl']       = $mydata['odc_kap']  = '';
		$mydata['odc_koordinat'] = $data->odc_koor;
		$mydata['odp_panel']     = $note_panel_f;
		$mydata['odp_port']      = $note_port_f;
		$mydata['odp_spl']       = '';
		$mydata['odp_koordinat'] = $data->odp_koor;
		$mydata['dist_nama']     = $mydata['dist_kap'] = '';
		$mydata['nama_tenos']    = $data->odp_nama;
		$mydata['id_valins'] = '';

		$value[1] = $mydata;
		$namel = str_replace("/", "-", "$data->odp_nama");
		$material = TeknisiModel::load_material($id);

		foreach ($material as $no => $val)
		{
			$child_value[$no] = (object)['barang' => $val->id_item, 'qty' => $val->qty];
		}
		$child_another = (object)['project_' => $data->project_name];

		return Excel::download(new ExcelExport([$value, $child_value, $child_another], $parent, 'abd') , 'Report ABD ' . $namel . ' Tanggal ' . date('m') . ' ' . date('F', strtotime(date('Y-m-d'))) . ' ' . date('Y') . '.xlsx');
	}

	public function odp_bank()
	{
		$data = AdminModel::bank_odp();
		return view('Admin.bank_order', compact('data'));
	}

	public function error405()
	{
		return view('405');
	}

	public function update_data_sdi(Request $req)
	{
		AdminModel::update_sdi($req);
	}

	public function list_finish()
	{
		$data_golive = AdminModel::finish_list('go_live');

		return view('Admin.list_go_live', compact('data_golive') );
	}

	public function push_final_golive(Request $req, $id)
	{
		AdminModel::push_live($req, $id);
		return redirect('/saldo_check');
	}

	public function recovery_data($jenis, $id)
	{
		AdminModel::recovery_data($jenis, $id);
		return back();
	}

	public function ajax_psb_myir(Request $req)
	{
		$term = trim($req->searchTerm);

		if (empty($term))
		{
			return \Response::json([]);
		}

		$fetchData = Adminmodel::ajax_select2_golive($term);

		$formatted_tags = [];

		foreach ($fetchData as $row)
		{
			$formatted_tags[] = ["id" => $row->orderId, "text" => $row->project_name . '(' . $row->sc . '/ ' . $row->myir . ')'];
		}

		return \Response::json($formatted_tags);
	}

	public function regu_ajax_search(Request $req)
	{
		$term = trim($req->searchTerm);

		if (empty($term))
		{
			return \Response::json([]);
		}

		$fetchData = AdminModel::getRegu_ajx($term);

		$formatted_tags = [];

		foreach ($fetchData as $row)
		{
			$formatted_tags[] = ["id" => $row->id_user, "text" => $row->id_user . ' (' . $row->nama . ')'];
		}

		return \Response::json($formatted_tags);
	}

	public function list_dashboard($jenis, $datel, $tahun1, $tahun2)
	{
		$data = Adminmodel::list_dsh($jenis, $datel, $tahun1, $tahun2);
		return view('Admin.list_main_dsh', compact('data'));
	}

	public function list_detail_dsh($jenis, $datel, $tipe, $sto, $tahun1, $tahun2, $jenis_wo)
	{
		$data = AdminModel::detail_list($jenis, $datel, $tipe, $sto, $tahun1, $tahun2, $jenis_wo);
		return view('Admin.show_list_dh', compact('data', 'tipe', 'jenis_wo') );
	}

	public function download_detail_dsh($jenis, $datel, $tipe, $sto, $tahun1, $tahun2, $jenis_wo)
	{
		$data = AdminModel::detail_list($jenis, $datel, $tipe, $sto, $tahun1, $tahun2, $jenis_wo);
		$jenis = ucwords(str_replace('_', ' ', $jenis));
		$tipe = ucwords(str_replace('_', ' ', $tipe));
		$datel = ReportModel::sdi_datel();

		foreach ($data as $key => $value)
		{
			foreach ($datel as $val2)
			{
				if ($value->sto == $val2->sto)
				{
					$data[$key]->datel = $val2->datel;
				}
			}
		}

		$mitra_sto = [
			'AMT' => 'CUI',
			'BBR' => 'TBN',
			'BJM' => 'STM',
			'BLC' => 'CUI',
			'BRI' => 'GTM',
			'BTB' => 'TBN',
			'GMB' => 'UPATEK',
			'KDG' => 'GTM',
			'KPL' => 'CUI',
			'KYG' => 'UPATEK',
			'LUL' => 'GTM',
			'MRB' => 'CUI',
			'MTP' => 'CUI',
			'NEG' => 'GTM',
			'PGN' => 'CUI',
			'PGT' => 'CUI',
			'PLE' => 'TBN',
			'RTA' => 'GTM',
			'SER' => 'CUI',
			'STI' => 'CUI',
			'TJL' => 'CUI',
			'TKI' => 'TBN',
			'ULI' => 'UPATEK',
		];

		foreach ($data as $no => $value)
		{
			$mydata['nomor']      = ++$no;
			$mydata['tanggal']    = @date('Y-m-d', strtotime($value->tgl_pengerjaan) );
			$mydata['tanggal_jm'] = $value->tgl_pengerjaan;
			$mydata['tgl_buat']   = @date('Y-m-d', strtotime($value->tgl_buat) );
			$mydata['project']    = $value->project_name or '';
			$mydata['scid']       = $value->nomor_sc or '';
			$mydata['datel']      = @$value->datel or '';
			$mydata['sto']        = $value->sto or '';
			$mydata['tim']        = $value->regu_name or '';
			$mydata['Mitra Regu'] = $value->nama_mitra or '';
			$mydata['Mitra STO']  = @$mitra_sto[$value->sto];
			$mydata['odp']        = $value->odp_nama or '';
			$mydata['koor']       = @$value->lt_koordinat_odp or '';
			$mydata['catatan']    = str_replace(' = ', '', $value->lt_catatan);
			$mydata['status']     = $value->lt_status or '';

			if ($value->lanjut_PT1 == 1)
			{
				$stts_golive = 'Sudah Lanjut PT-1';
			}
			elseif($value->GOLIVE == 1)
			{
				$stts_golive = 'Sudah Go-LIVE';
			}
			elseif($value->upload_abd_4 == 1)
			{
				$stts_golive = 'Sudah Upload ABD Valid 4';
			}
			elseif($value->upload_abd_4 == 0)
			{
				$stts_golive = 'Belum Upload ABD Valid 4';
			}
			else
			{
				$stts_golive = 'Belum Go-Live';
			}

			$mydata['jenis_wo']       = $value->jenis_wo or '';
			$mydata['tgl_Selesai']    = $value->tgl_selesai or '';
			$mydata['stts_golive']    = $stts_golive;
			$mydata['kendala_detail'] = $value->kendala_detail or '';
			$tes[$no] = $mydata;
		}

		$parent = ['NO', 'TGL', 'TGL & JAM', 'TGL BUAT', 'NAMA PROYEK', 'SCID', 'DATEL', 'STO', 'NAMA TIM', 'MITRA REGU', 'MITRA STO', 'NAMA OPD', 'KOOR ODP', 'CATATAN TEKNISI', 'STATUS ODP', 'JENIS WO', 'TANGGAL SELESAI', 'STATUS GO-LIVE', 'KENDALA DETAIL'];

		$judul = "Report Rekap Data Status $jenis $tipe.xlsx";

		return Excel::download(new ExcelExport([$tes], $parent, 'rekap_data') , $judul);
	}

	public function get_table_gd($opt, $range, $filter = [])
	{
		$client = new \Google_Client();
		$client->setApplicationName('My PHP App');
		$client->setScopes([\Google_Service_Sheets::SPREADSHEETS]);
		$client->setAccessType('offline');
		$client->setAuthConfig('/srv/htdocs/procurement_dev3/public/google.json');

		$sheets = new \Google_Service_Sheets($client);

		// $headers = $sheets
		// 	->spreadsheets_values
		// 	->get($this->spreadsheetId, $range . '!A1:AG1', ['majorDimension' => 'ROWS']);

		// $header = array_map(function ($x)
		// {
		// 	$x = strtolower($x);
		// 	$x = ltrim($x);
		// 	$x = ucwords($x);
		// 	return $x;
		// }
		// , $headers->values[0]);

		// $rows = $sheets
		// 	->spreadsheets_values
		// 	->get($this->spreadsheetId, $range . '!A2:AG', ['majorDimension' => 'ROWS']);

		$new_r = $main_d = [];

		if (isset($rows['values']))
		{
			foreach ($rows['values'] as $key => $row)
			{
				if (isset($row[1]))
				{
					$data = strtolower($row[1]);
					$row[1] = ucfirst($data);
				}

				$count = count($row);
				if ($count < 32)
				{
					for ($i = 0;$i < (32 - $count);$i++)
					{
						array_push($row, '');
					}
				}

				if ($opt == 'ftable')
				{
					if (count($filter) != 0)
					{
						foreach ($filter as $f)
						{
							if ($row[0] == $f)
							{
								$new_r[] = $row;
							}
						}
					}
				}

				if ($opt == 'table')
				{
					foreach ($row as $m_k => $data)
					{
						// foreach ($header as $key => $h)
						// {
						// 	if ($m_k == $key & ($h == "Nama Mitra" || $h == "Posisi Tagihan" || $h == "No" || $h == "Witel" || $h == "Status Pembayaran"))
						// 	{
						// 		$main_d[$h] = $data;
						// 	}
						// }
					}
					if ($main_d['Posisi Tagihan'] != '')
					{
						$final_data[] = $main_d;
					}
				}

			}

			if ($opt == 'ftable')
			{
				foreach ($new_r as $metadata)
				{
					foreach ($metadata as $m_k => $data)
					{
						// foreach ($header as $key => $h)
						// {
						// 	if ($m_k == $key)
						// 	{
						// 		$main_d[$h] = $data;
						// 	}
						// }
					}
					$final_data[] = $main_d;
				}
			}

		}
		return $final_data;
	}

	public function push_pt2($id)
	{
		AdminModel::re_push_pt2($id);
		return back();
	}

	public function register_check(Request $req)
	{
		return \Response::json(AdminModel::cek_user($req->data));
	}

	public function list_detail_history($id, $stts)
	{
		$data = AdminModel::get_detail_hist($id, $stts);
		return view('Admin.history_regu_wo', compact('data'));
	}

	public function list_absen()
	{
		$data = AdminModel::get_list_Absen();
		return view('Admin.list_absen', compact('data'));
	}

	public function get_checked_book_odp(Request $req)
	{
		$curl = curl_init();

		if($req->jenis_terminal == 'ALL')
		{
			$jt[] = 'ODP';
			$jt[] = 'ASPL';
		}
		else
		{
			$jt[] = $req->jenis_terminal;
		}

		foreach($jt as $v)
		{
			$post = [
				'jenis_terminal'  => $v,
				'sto'             => $req->sto,
				'odc'             => $req->odc,
			];

			curl_setopt_array($curl, [
				CURLOPT_URL => 'https://promitos.tomman.app/API_PT2_CEKSLOT',
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_POSTFIELDS => $post,
				CURLOPT_ENCODING => '',
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 0,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => 'POST',
			]);

			$response[$v] = json_decode(curl_exec($curl));
		}
		curl_close($curl);

		return $response;
	}

	public function monitoring_data(Request $req)
	{
		$jenis = null;
		$start = date('Y-m-d', strtotime('first day of this month') );
		$end = date('Y-m-d', strtotime('last day of this month') );
		$load = [];

		if($req->jenis)
		{
			$jenis = $req->jenis;
		}

		if($req->tgl_a)
		{
			$start = $req->tgl_a;
		}

		if($req->tgl_f)
		{
			$end = $req->tgl_f;
		}

		if($req->all() )
		{
			$load = $req->all();
		}

		$data = ReportModel::get_data_monitor($jenis, $start, $end);
		return view('Monitor.monitoring_wo', compact('data', 'load') );
	}

	public function detail_monitor_wo(Request $req)
	{
		$data = ReportModel::get_data_monitor($req->jenis_wo, $req->start, $req->end, $req->mitra);
		return \Response::json($data['rekap_close']);
	}

	public function post_flagging(Request $req)
	{
		AdminModel::update_flagging($req);
	}

	public function jointer_get()
	{
		$data = AdminModel::get_regu_joint();
		return \Response::json($data);
	}

	public function regu_id_ajax_search(Request $req)
	{
		$term = trim($req->regu);
		$fetchData = AdminModel::get_id_regu($term);
		return \Response::json($fetchData);
	}

	public function sync_pt1()
	{
		$tahun = date('Y');
		// $tahun = '2022';
		$get_not_lanjut_pt1 = DB::SELECT("SELECT id, scid, nomor_sc, nomor_sc_log, tgl_selesai, lt_status, odp_nama, project_name
		FROM pt2_master
		WHERE nomor_sc_log IS NOT NULL AND lt_status = 'Selesai' AND GOLIVE = 1 AND lanjut_PT1 = 0 AND tgl_selesai LIKE '$tahun%'
		ORDER BY nomor_sc ASC");

		// dd($get_not_lanjut_pt1);

		foreach($get_not_lanjut_pt1 as $v)
		{
			$sc = array_map(function($x){
				return preg_replace('/\s+/', '', $x);
			}, explode(',', $v->nomor_sc_log) );

			$post = [
				'order' => json_encode($sc),
			];

			$curl = curl_init();

			//wo nya harus golive di jointer
			curl_setopt_array($curl, [
				CURLOPT_URL => 'https://ops.tomman.app/api/update_jointer',
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_POSTFIELDS => $post,
				CURLOPT_ENCODING => '',
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 0,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => 'POST',
			]);

			$response = curl_exec($curl);
			curl_close($curl);

			$response =json_decode($response);
			// dd($response);

			if($response->status == true)
			{
				DB::table('pt2_master')
				->where('id', $v->id)
				->update([
					'lanjut_PT1' => 1
				]);
			}
		}

		// //cari SC
		// $get_data = DB::table('pt2_master')
		// ->select('id', 'odp_nama', 'odp_koor', 'nomor_sc', 'nomor_sc_log')
		// ->WhereNull('nomor_sc_log')
		// ->WhereNotNull('odp_koor')
		// ->Where([
		// 	['delete_clm', 0],
		// ])
		// ->Where(function($join){
		// 	$join->WhereNull('nomor_sc')
		// 	->OrWhere('nomor_sc', 0);
		// })
		// // ->Where(function($join){
		// // 	$join->WhereNotNull('nomor_sc')
		// // 	->Where('nomor_sc', '!=', 0);
		// // })
		// ->Where(function($join){
		// 	$join->Where('tgl_buat', 'like', '2022%')
		// 	->OrWhere('tgl_selesai', 'like', '2022%');
		// })
		// ->get();
		// // dd($get_data);
		// foreach($get_data as $k => $v)
		// {
		// 	$curl = curl_init();

		// 	$split = explode(',', $v->odp_koor);

		// 	if(count($split) >= 2)
		// 	{
		// 		curl_setopt_array($curl, array(
		// 			CURLOPT_URL => 'https://ops.tomman.app/api/get8pelanggan?type=order&lat='.trim($split[0]).'&long='.trim($split[1]),
		// 			CURLOPT_RETURNTRANSFER => true,
		// 			CURLOPT_SSL_VERIFYHOST => 0,
		// 			CURLOPT_SSL_VERIFYPEER => 0,
		// 			CURLOPT_ENCODING => '',
		// 			CURLOPT_MAXREDIRS => 10,
		// 			CURLOPT_TIMEOUT => 0,
		// 			CURLOPT_FOLLOWLOCATION => true,
		// 			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		// 			CURLOPT_CUSTOMREQUEST => 'GET',
		// 		));

		// 		$response = curl_exec($curl);
		// 		curl_close($curl);
		// 		$data = json_decode($response);

		// 		if(@$data->rows > 0)
		// 		{
		// 			$get_sc = [];

		// 			foreach($data->data as $vv)
		// 			{
		// 				$get_sc[] = $vv->orderId;
		// 			}

		// 			$tes[$k]['data']= $v;
		// 			$tes[$k]['sc']= implode(',', $get_sc);
		// 			// dd(json_decode($response), implode(',', $get_sc), $v);

		// 			DB::table('pt2_master')
		// 			->where('id', $v->id)
		// 			->update([
		// 				'nomor_sc'		=> $get_sc[0],
		// 				'nomor_sc_log' => implode(',', $get_sc)
		// 			]);
		// 		}
		// 	}
		// }
		// dd($tes);
	}

	public function upload_abd(Request $req)
	{
		// $xml = simplexml_load_file($req->file('abd_file') );
		// $childs = $xml->Document->Folder->children();
		// $tes = json_decode(json_encode($childs), FALSE);

		// dd('stop', $tes, $tes->Folder[1]);
		AdminModel::save_abd($req);
		$msg['msg'] = ['type' => 'info', 'text' => "Data Berhasil Diupload!"];
		return back()->with('alerts_tele', $msg);

	}

	public function get_tiang()
	{
		$curl = curl_init();

			curl_setopt_array($curl, array(
				CURLOPT_URL => 'https://ops.tomman.app/map/ajx_tiang_all',
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_SSL_VERIFYHOST => 0,
				CURLOPT_SSL_VERIFYPEER => 0,
				CURLOPT_ENCODING => '',
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 0,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => 'GET',
			));

			$response = curl_exec($curl);
			curl_close($curl);
			$response = json_decode($response);

			return $response;
	}

	public function absen_view($id)
	{
		$data = DB::Table('absen')->Where('absen_id', $id)->first();
		return view('view_absen', compact('data') );
	}
}