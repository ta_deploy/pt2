<?php

namespace App\Http\Controllers;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Events\BeforeExport;

use Maatwebsite\Excel\Events\BeforeSheet;

use Maatwebsite\Excel\Events\BeforeWriting;

use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class ExcelExport implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents
{
	use Exportable;

	public function __construct($data, $head, $type){
		$this->head = $head;
		$this->child = $data[0];

		if(count($data) > 1)
		{
			foreach (array_slice($data,1) as $no => $value)
			{
				$this->child_c[$no] = $value;
			}
		}

		$this->type = $type;
	}

	public function collection(){
		if(!in_array($this->type, ['umur_']) )
		{
			return collect($this->child);
		}
		else
		{
			return collect([]);
		}
	}

	public function headings(): array{
		if(!in_array($this->type, ['umur_', 'material']) )
		{
			return $this->head;
		}
		else
		{
			return [];
		}
	}

	public function registerEvents(): array
	{
		if( $this->type == 'abd')
		{
			return [
				AfterSheet::class => function(AfterSheet $event)
				{
					$event->sheet->getDelegate()->mergeCells('A1:A2');
					$event->sheet->getDelegate()->mergeCells('B1:B2');
					$event->sheet->getDelegate()->mergeCells('C1:C2');
					$event->sheet->getDelegate()->mergeCells('D1:D2');
					$event->sheet->getDelegate()->mergeCells('E1:E2');
					$event->sheet->getDelegate()->mergeCells('F1:I1');
					$event->sheet->getDelegate()->mergeCells('J1:K1');
					$event->sheet->getDelegate()->mergeCells('L1:Q1');
					$event->sheet->getDelegate()->mergeCells('R1:U1');
					$event->sheet->getDelegate()->mergeCells('V1:W1');
					$event->sheet->getDelegate()->mergeCells('X1:X2');
					$event->sheet->getDelegate()->mergeCells('Y1:Y2');

					$fill_border = [
							'fill' => [
									'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
									'startColor' => [
											'argb' => '00137FFF',
									],
							]
					];

					$border_Style = [
							'borders' => [
									'allBorders' => [
											'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
									],
							],
							'alignment' => [
									'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
									'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
							]
					];

					$event->sheet->getDelegate()->getStyle('A1:Y3')->applyFromArray($border_Style);
					$event->sheet->getDelegate()->getStyle('A1:Y2')->applyFromArray($fill_border);
					//JUDUL
					$event->sheet->getDelegate()->mergeCells('B5:C5');
					$event->sheet->getDelegate()->mergeCells('B6:C6');
					//ISI
					$event->sheet->getDelegate()->mergeCells('D5:J5');
					$event->sheet->getDelegate()->mergeCells('D6:J6');
					$event->sheet->getDelegate()->mergeCells('B7:J7');
					$event->sheet->getDelegate()->getStyle('B5:B6')->applyFromArray($fill_border);
					$event->sheet->getDelegate()->getStyle('B5:J7')->applyFromArray($border_Style);
					$event->sheet->getDelegate()->getCell('B5')->setValue("NAMA PROYEK");
					$event->sheet->getDelegate()->getCell('B6')->setValue("PID");
					$event->sheet->getDelegate()->getCell("D5")->setValue($this->child_c[1]->project_);
					$event->sheet->getDelegate()->getCell('B7')->setValue("DEPLOYER");
					//BOQ
					$event->sheet->getDelegate()->mergeCells('B9:C9');
					$event->sheet->getDelegate()->getStyle('B9:C9')->applyFromArray($fill_border);
					$event->sheet->getDelegate()->getCell('B9')->setValue("BOQ");

					foreach($this->child_c[0] as $no => $data)
					{
							$nomor = $no+10;
							$event->sheet->getDelegate()->getCell("B$nomor")->setValue($data->barang);
							$event->sheet->getDelegate()->getCell("C$nomor")->setValue($data->qty);
					}

					$event->sheet->getDelegate()->getStyle('B9:C'.$nomor)->applyFromArray($border_Style);
				},
			];
		}
		elseif( $this->type == 'material')
		{
			return [
				BeforeExport::class  => function(BeforeExport $event)
				{
					$border_Style = [
						'borders' => [
								'allBorders' => [
										'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
								],
						],
						'alignment' => [
								'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
								'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
						]
					];

					$worksheet1 = $event->writer->createSheet();
					$event->writer->setActiveSheetIndex(0)->mergeCells("A1:A6");
					$event->writer->setActiveSheetIndex(0)->mergeCells("B1:B6");
					$event->writer->setActiveSheetIndex(0)->mergeCells("C1:C6");
					$event->writer->setActiveSheetIndex(0)->mergeCells("D1:D6");
					$event->writer->setActiveSheetIndex(0)->mergeCells("E1:E6");
					$event->writer->setActiveSheetIndex(0)->mergeCells("F1:F6");

					$event->writer->setActiveSheetIndex(0)->getCell("A1")->setValue('#');
					$event->writer->setActiveSheetIndex(0)->getCell("B1")->setValue('ID Item');
					$event->writer->setActiveSheetIndex(0)->getCell("C1")->setValue('Uraian');
					$event->writer->setActiveSheetIndex(0)->getCell("D1")->setValue('Jasa');
					$event->writer->setActiveSheetIndex(0)->getCell("E1")->setValue('Material');
					$event->writer->setActiveSheetIndex(0)->getCell("F1")->setValue('Satuan');

					for ($i = 'G'; $i !== 'ZZ'; $i++){
						$index[] = $i;
					}

					foreach (array_values($this->child) as $k => $v) {
						$event->writer->setActiveSheetIndex(0)->getCell($index[$k]."1")->setValue($v['Nama Team']);
						$event->writer->setActiveSheetIndex(0)->getCell($index[$k]."2")->setValue($v['jenis_wo']);
						$event->writer->setActiveSheetIndex(0)->getCell($index[$k]."3")->setValue($v['nama_project']);
						$event->writer->setActiveSheetIndex(0)->getCell($index[$k]."4")->setValue($v['odp_nama']);
						$event->writer->setActiveSheetIndex(0)->getCell($index[$k]."5")->setValue($v['rfc_key']);
						$event->writer->setActiveSheetIndex(0)->getCell($index[$k]."6")->setValue($v['sto']);
					}


					foreach($this->child as &$v)
					{
						unset($v['nomor']);
						unset($v['Nama Team']);
						unset($v['nama_project']);
						unset($v['jenis_wo']);
						unset($v['sto']);
						unset($v['odp_nama']);
						unset($v['rfc_key']);
					}

					for ($i = 'A'; $i !== 'Z'; $i++){
						$index1[] = $i;
					}

					$no_urut = $material_all = $jasa_all = 0;
					$sum_total = $total_all = [];
					foreach (array_values($this->child_c[0]) as $k => $v) {
						$no = $k++;

						$event->writer->setActiveSheetIndex(0)->getCell($index1[0]."".($no + 7) )->setValue(++$no_urut);
						$event->writer->setActiveSheetIndex(0)->getCell($index1[1]."".($no + 7) )->setValue($v['id_item']);
						$event->writer->setActiveSheetIndex(0)->getCell($index1[2]."".($no + 7) )->setValue($v['uraian']);
						$event->writer->setActiveSheetIndex(0)->getCell($index1[3]."".($no + 7) )->setValue($v['jasa']);
						$event->writer->setActiveSheetIndex(0)->getCell($index1[4]."".($no + 7) )->setValue($v['material']);
						$event->writer->setActiveSheetIndex(0)->getCell($index1[5]."".($no + 7) )->setValue($v['satuan']);
					}

					$find_mat_jas = array_values($this->child_c[0]);

					foreach (array_values($this->child) as $k => $v) {
						$no_urut2 = 7;

						foreach ($v as $kk => $vv) {
							$sum_total[$index[$k] ][] = $vv;

							if(!isset($total_all[$kk]) )
							{
								$total_all[$kk] = 0;
							}

							$total_all[$kk] += $vv;
							$event->writer->setActiveSheetIndex(0)->getCell($index[$k]."". $no_urut2++)->setValue( ($vv != 0 ? $vv : '-') );
						}
					}
					// dd($total_all, $sum_total, $this->child);
					$urut_total = 7;
					$event->writer->setActiveSheetIndex(0)->mergeCells($index[count($this->child)]. "1:" .$index[count($this->child)]."6");
					$event->writer->setActiveSheetIndex(0)->getCell($index[count($this->child)]."1")->setValue('Total');

					foreach(array_values($total_all) as $k => $v)
					{
						$material_all += $find_mat_jas[$k]['material'] * $v;
						$jasa_all += $find_mat_jas[$k]['jasa'] * $v;

						$event->writer->setActiveSheetIndex(0)->getCell($index[count($this->child)]."". $urut_total++)->setValue( ($v != 0 ? $v : '-') );
					}

					$event->writer->setActiveSheetIndex(0)->getCell($index[count($this->child)]."". $urut_total)->setValue($material_all);
					$event->writer->setActiveSheetIndex(0)->getCell($index[count($this->child)]."". ($urut_total + 1) )->setValue($jasa_all);
					$event->writer->setActiveSheetIndex(0)->getCell($index[count($this->child)]."". ($urut_total + 2) )->setValue($material_all + $jasa_all);

					$event->writer->setActiveSheetIndex(0)->getStyle('A1:'.$index[count($this->child)]."". ($urut_total + 2) )->applyFromArray($border_Style);
					$event->writer->setActiveSheetIndex(0)->mergeCells("A". $no_urut2 .":F". $no_urut2);
					$event->writer->setActiveSheetIndex(0)->mergeCells("A". ($no_urut2 + 1) .":F". ($no_urut2 + 1) );
					$event->writer->setActiveSheetIndex(0)->mergeCells("A". ($no_urut2 + 2) .":F". ($no_urut2 + 2) );

					$event->writer->setActiveSheetIndex(0)->getCell("A". $no_urut2)->setValue('Total Material');
					$event->writer->setActiveSheetIndex(0)->getCell("A". ($no_urut2 + 1) )->setValue('Total Jasa');
					$event->writer->setActiveSheetIndex(0)->getCell("A". ($no_urut2 + 2) )->setValue('Total');

					foreach($sum_total as $k => $v)
					{
						$total_material = $total_jasa = 0;
						foreach ($v as $kk => $vv) {
							$total_material += $find_mat_jas[$kk]['material'] * $vv;
							$total_jasa += $find_mat_jas[$kk]['jasa'] * $vv;

							$event->writer->setActiveSheetIndex(0)->getCell($k."". $no_urut2)->setValue($total_material);
							$event->writer->setActiveSheetIndex(0)->getCell($k."". ($no_urut2 + 1) )->setValue($total_jasa);
							$event->writer->setActiveSheetIndex(0)->getCell($k."". ($no_urut2 + 2) )->setValue($total_material + $total_jasa);
						}
					}
				}
			];
		}
		elseif( $this->type == 'umur_')
		{
			return [
				BeforeExport::class  => function(BeforeExport $event)
				{
					$event->writer->getProperties()->setCreator('Yudha');
					$worksheet1 = $event->writer->createSheet();
					$worksheet2 = $event->writer->createSheet();

					$event->writer->setActiveSheetIndex(0)->getCell("A1")->setValue('tesss');
					$event->writer->setActiveSheetIndex(1)->getCell("A1")->setValue('sssstesss');
				}
			];
		}
		else
		{
			return [];
		}
	}
}