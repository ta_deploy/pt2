<?php
namespace App\Http\Controllers;

use App\DA\AdminModel;
use App\DA\ReportModel;
use App\DA\JointerModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Facades\Excel;

date_default_timezone_set("Asia/Makassar");

class ReportController extends Controller
{
	public function getRekap_matrix_ajax($year, $month)
	{
		if (strlen($month) < 2 && $month < 10)
		{
			$month = '0' . $month;
		}

		if ($month > 13)
		{
			$month = 12;
		}

		$data = ReportModel::getRekap_year($year, $month);
		$detail = ReportModel::getRekap_year_detail($year, $month);

		if ($data)
		{
			foreach ($data as $no => $d)
			{
				$bulan = date('F', mktime(0, 0, 0, $month, 10));
				$rekap_rinci[$bulan][$no] = $d;
				$pecah_b = explode(' ', $d->tgl_mutlak);

				if (!empty($pecah_b))
				{
					$rekap_rinci[$bulan][$no]->tgl_mutlak = $pecah_b[0];
				}

				$sum = 0;

				foreach ($detail as $nom => $det)
				{
					$date = date_parse($bulan);
					if (strlen($date['month']) == 1)
					{
						$bulan_lahir = '0' . $date['month'];
					}
					else
					{
						$bulan_lahir = $date['month'];
					}
					$month_num = $year . '-' . $bulan_lahir;
					if (substr($det->tgl_mutlak, 0, 7) == $month_num)
					{
						$pecah_b_d = explode(' ', $det->tgl_mutlak);
					}

					if ($pecah_b_d[0] == $pecah_b[0])
					{
						$detail_data[$bulan][$pecah_b_d[0]][$nom] = $det;
						$detail_data[$bulan][$pecah_b_d[0]]['Total'] = $sum += count($det);
					}
				}

			}
		}
		else
		{
			$rekap_rinci = $detail_data = null;
		}
		return \Response::json($detail_data);
	}

	public function get_result_rekap_front($year)
	{
		$data = ReportModel::getRekap_front($year);
		$data2 = ReportModel::getRekapPsb_daily($year);
		$datel = ReportModel::sdi_datel();
		$rekap_rinci = [];

		if (!empty($data))
		{
			foreach ($data as $no => $d)
			{
				$bulan = date("F", strtotime($d->tgl_mutlak));

				foreach ($datel as $numbah => $datelku)
				{
					if ($datelku->datel == $d->datel)
					{
						$raw_rekap_rinci[$bulan][$datelku->datel]['data_utama'][$no] = $d;
						foreach ($data2 as $key => $value2)
						{
							if ($value2->sto_B == $d->sto_n)
							{
								$raw_rekap_rinci[$bulan][$datelku->datel]['data_utama'][$no]->WO_orderan_siap_dispatch = $value2->hitung;
							}
						}
					}
				}

			}

			foreach ($raw_rekap_rinci as $no => $data)
			{
				foreach ($data as $nomor => $data_1)
				{
					$sum = 0;
					$finish = 0;

					foreach ($data_1 as $quer => $main)
					{
						foreach ($main as $Mymain)
						{
							$rekap_rinci[$no]->$nomor->$quer[] = $Mymain;
							$rekap_rinci[$no]->$nomor->Grands = $sum += $Mymain->WO_orderan_siap_dispatch + $Mymain->sudah_dispatch + $Mymain->ogp + $Mymain->kendala + $Mymain->finish + $Mymain->sisa_order_belumselesai;
							$rekap_rinci[$no]->$nomor->finishs = $finish += $Mymain->finish;
							$rekap_rinci[$no]->$nomor->alamat = $no;
						}

						$rekap_rinci[$no]->$nomor->count = count($main);
					}

				}
			}
		}
		return $rekap_rinci;
	}

	public function getRekap($year, $jenis, $month)
	{
		if (strlen($month) < 2 && $month < 10)
		{
			$month = '0' . $month;
		}

		if ($month > 13)
		{
			$month = 12;
		}

		$data   = ReportModel::getRekap_year($year, $month);
		$data2  = ReportModel::getRekapPsb_daily($year);
		$detail = ReportModel::getRekap_year_detail($year, $month);
		$datel  = ReportModel::sdi_datel();
		$detail_data = null;
		$rekap_rinci = [];

		if ($data)
		{
			foreach ($data as $no => $d)
			{
				$bulan = date('F', mktime(0, 0, 0, $month));
				//$rekap_rinci[$bulan][$no] = $d ;
				$pecah_b = explode(' ', $d->tgl_mutlak);
				// if(!empty($pecah_b)){
				//   $rekap_rinci[$bulan][$no]->tgl_mutlak =  $pecah_b[0];
				// }
				foreach ($detail as $nom => $det)
				{
					$date = date_parse($bulan);

					if (strlen($date['month']) == 1)
					{
						$bulan_lahir = '0' . $date['month'];
					}
					else
					{
						$bulan_lahir = $date['month'];
					}

					$month_num = $year . '-' . $bulan_lahir;

					if (substr($det->tgl_mutlak, 0, 7) == $month_num)
					{
						$pecah_b_d = explode(' ', $det->tgl_mutlak);
					}

					if ($pecah_b_d[0] == $pecah_b[0])
					{
						$detail_data[$bulan][$pecah_b_d[0]][$nom] = $det;
					}
				}
			}
		}
		else
		{
			//$rekap_rinci = null;
		}

		if (!empty($data))
		{
			foreach ($data as $no => $d)
			{
				$pecah_b = explode(' ', $d->tgl_mutlak);
				$bulan = date("F", strtotime($d->tgl_mutlak));

				if (!empty($pecah_b))
				{
					$d->tgl_mutlak = $pecah_b[0];
				}

				foreach ($datel as $numbah => $datelku)
				{
					if ($datelku->datel == $d->datel)
					{
						$raw_rekap_rinci[$pecah_b[0]][$datelku->datel]['data_utama'][$no] = $d;

						foreach ($data2 as $key => $value2)
						{
							if ($value2->sto_B == $d->sto_n)
							{
								$raw_rekap_rinci[$pecah_b[0]][$datelku->datel]['data_utama'][$no]->WO_orderan_siap_dispatch = $value2->hitung;
							}
						}
					}
				}
			}

			foreach ($raw_rekap_rinci as $no => $data)
			{
				$hasil_sum = 0;

				foreach ($data as $nomor => $data_1)
				{
					foreach ($data_1 as $quer => $main)
					{
						$sum = $finish = 0;

						foreach ($main as $Mymain)
						{
							$rekap_rinci[$no]->$nomor->$quer[] = $Mymain;
							$rekap_rinci[$no]->$nomor->Grands = $sum += $Mymain->WO_orderan_siap_dispatch + $Mymain->sudah_dispatch + $Mymain->ogp + $Mymain->kendala + $Mymain->finish + $Mymain->sisa_order_belumselesai;
							$rekap_rinci[$no]->$nomor->finish = $finish += $Mymain->finish;
							$rekap_rinci[$no]->$nomor->bulan = $no;
						}

						$rekap_rinci[$no]->$nomor->count = count($main);
					}
				}
			}
		}

		return view('Report.rekap', compact('rekap_rinci', 'detail_data', 'bulan', 'year', 'month'));
	}

	public function getRekap_Year($year)
	{
		$rekap_rinci = $this->get_result_rekap_front($year);
		$data2 = ReportModel::getRekapPsb_daily($year);
		$datel = ReportModel::sdi_datel();

		foreach ($datel as $no => $d_new)
		{
			foreach ($data2 as $no2 => $d_psb)
			{
				if ($d_psb->sto_B == $d_new->sto)
				{
					$raw_datel[$no][$d_new->datel] = $d_psb->hitung;
				}
			}
		}
		return view('Report.front_page_rekap', compact('rekap_rinci'));
	}

	public function getRekap_Year_graph($year)
	{
		$rekap_rinci = $this->get_result_rekap_front($year);
		return \Response::json($rekap_rinci);
	}

	public function getMatrix(Request $req, $tgl)
	{
		// dd($req->all());
		if (empty($req->status) )
		{
			$status = 'empty';
		}
		else
		{
			$status = implode(',', $req->status);
		}

		if (empty($req->stts_regu))
		{
			$status_rg = '1';
		}
		else
		{
			$status_rg = implode(',', $req->stts_regu);
		}

		if (empty($req->tgl_a))
		{
			$tgl = $tgl;
		}
		else
		{
			$tgl = $req->tgl_a;
		}

		if (empty($req->tgl_f))
		{
			$tglb = date('Y-m-d H:i:s');
		}
		else
		{
			$tglb = $req->tgl_f;
		}

		$jenis = '';

		if (empty($req->jenis))
		{
			$jenis = implode(', ', array_keys(AdminModel::status() ) );
		}
		else
		{
			$jenis = implode(',', $req->jenis);
		}

		$matrix = $this->matrix_all_flex($tgl, $tglb, $status, $jenis, $status_rg);
		$get_all_status = AdminModel::status('all');

		$data_perform = $this->performansi_jointer($tgl, $tglb);

		// dd($get_all_status);
		return view('Report.Matrix.dashboard', compact('matrix', 'view', 'get_all_status', 'data_perform'), ['req' => $req->all()] );
	}

	public function performansi_jointer($tgl, $tglb)
	{
		$call_sql = JointerModel::perform_jointer($tgl, $tglb);
		$data_perform = [];

		foreach($call_sql['get_all_user'] as $v)
		{
			$data_perform[$v->nik]['id_regu']		  = $v->id_regu;
			$data_perform[$v->nik]['uraian']		  = $v->uraian;
			$data_perform[$v->nik]['mitra_amija'] = $v->mitra_amija_pt;
			$data_perform[$v->nik]['total_qty'] 	= $v->total;

			if(!isset($data_perform[$v->nik]['isi']) )
			{
				$data_perform[$v->nik]['isi']['pickup']  = [];
				$data_perform[$v->nik]['isi']['selesai'] = [];
				$data_perform[$v->nik]['isi']['core_splice'] = 0;
			}
		}

		$data_perform = array_values($data_perform);

		foreach($call_sql['jointer_marina'] as $v)
		{
			$find_k = array_search($v->regu_id, array_column($data_perform, 'id_regu') );

			if($find_k !== FALSE)
			{
				$data_perform[$find_k]['isi']['pickup'][$v->id] = 1;
				$data_perform[$find_k]['isi']['core_splice'] += $v->qty;

				if($v->lt_status == 'Selesai')
				{
					$data_perform[$find_k]['isi']['selesai'][$v->id] = 1;
				}
			}
		}

		foreach($call_sql['jointer_bts'] as $v)
		{
			$find_k = array_search($v->regu_id, array_column($data_perform, 'id_regu') );

			if($find_k !== FALSE)
			{
				$data_perform[$find_k]['isi']['pickup'][$v->id] = 1;
				$data_perform[$find_k]['isi']['core_splice'] += $v->qty;

				if($v->lt_status == 'Selesai')
				{
					$data_perform[$find_k]['isi']['selesai'][$v->id] = 1;
				}
			}
		}

		foreach($call_sql['jointer_pt2'] as $v)
		{
			$find_k = array_search($v->regu_id, array_column($data_perform, 'id_regu') );

			if($find_k !== FALSE)
			{
				$data_perform[$find_k]['isi']['pickup'][$v->id] = 1;
				$data_perform[$find_k]['isi']['core_splice'] += $v->qty;

				if($v->lt_status == 'Selesai')
				{
					$data_perform[$find_k]['isi']['selesai'][$v->id] = 1;
				}
			}
		}

		foreach($data_perform as $k => $v)
		{
			$data_perform[$k]['isi']['pickup'] = array_sum($v['isi']['pickup']);
			$data_perform[$k]['isi']['selesai'] = array_sum($v['isi']['selesai']);
		}

		return $data_perform;
	}

	public function matrix_all_flex($month1, $month2, $value, $jenis, $stts_regu)
	{
		$order_matrix = ReportModel::getMatrix_search($month1, $month2, $value, $jenis, $stts_regu);

		$matrix = $matrix_2 = $matrix_3 = $matrix_4 = [];
		$sektor = ReportModel::get_sector();

		$sektor = array_map(function($item) {
			return (array)$item;
		}, $sektor->toArray() );

		foreach($order_matrix as $v)
		{
			$sektor_nm = 'KOSONG';

			$find_sk = array_search($v->label_sektor, array_column($sektor, 'label') );

			if($find_sk !== FALSE)
			{
				$sektor_nm = $sektor[$find_sk]['sector'];
			}

			$matrix[$v->datel]['datel'] = $v->datel;

			$matrix_2[$sektor_nm][$v->uraian][$v->datel]['datel'] = $v->datel;
			$matrix_2[$sektor_nm][$v->uraian][$v->datel]['order'][] = $v;

			if (!in_array(session('auth')->pt2_level, [3, 6]))
			{
				$matrix_3[$v->nama_mitra]['mitra'] = $v->mitra;
			}

			if(!isset($matrix_4[$v->status_pst]) )
			{
				$matrix_4[$v->status_pst]['saldo']   = 0;
				$matrix_4[$v->status_pst]['in_tech'] = 0;
				$matrix_4[$v->status_pst]['selesai'] = 0;
				$matrix_4[$v->status_pst]['kendala'] = 0;
			}

			if($v->lt_status == 'Selesai')
			{
				$matrix_4[$v->status_pst]['selesai'] += 1;
			}

			if($v->lt_status == 'Kendala')
			{
				$matrix_4[$v->status_pst]['kendala'] += 1;
			}

			if(!isset($matrix[$v->datel]['selesai']) )
			{
				$matrix[$v->datel]['totalwo'] = 0;
				$matrix[$v->datel]['selesai'] = 0;
				$matrix[$v->datel]['kendala'] = 0;
				$matrix[$v->datel]['ogp']     = 0;
				$matrix[$v->datel]['TS']      = 0;
				$matrix[$v->datel]['pending'] = 0;
				$matrix[$v->datel]['jalan']   = 0;
				$matrix[$v->datel]['sisa']    = 0;
				$matrix[$v->datel]['poin']    = 0;
			}

			if(!isset($matrix_2[$sektor_nm][$v->uraian][$v->datel]['selesai']) )
			{
				$matrix_2[$sektor_nm][$v->uraian][$v->datel]['totalwo'] = 0;
				$matrix_2[$sektor_nm][$v->uraian][$v->datel]['selesai'] = 0;
				$matrix_2[$sektor_nm][$v->uraian][$v->datel]['kendala'] = 0;
				$matrix_2[$sektor_nm][$v->uraian][$v->datel]['ogp']     = 0;
				$matrix_2[$sektor_nm][$v->uraian][$v->datel]['TS']      = 0;
				$matrix_2[$sektor_nm][$v->uraian][$v->datel]['pending'] = 0;
				$matrix_2[$sektor_nm][$v->uraian][$v->datel]['jalan']   = 0;
				$matrix_2[$sektor_nm][$v->uraian][$v->datel]['sisa']    = 0;
				$matrix_2[$sektor_nm][$v->uraian][$v->datel]['poin']    = 0;
			}

			if(!isset($matrix_3[$v->nama_mitra]['selesai']) && !in_array(session('auth')->pt2_level, [3, 6]))
			{
				$matrix_3[$v->nama_mitra]['totalwo'] = 0;
				$matrix_3[$v->nama_mitra]['selesai'] = 0;
				$matrix_3[$v->nama_mitra]['kendala'] = 0;
				$matrix_3[$v->nama_mitra]['ogp']     = 0;
				$matrix_3[$v->nama_mitra]['TS']      = 0;
				$matrix_3[$v->nama_mitra]['pending'] = 0;
				$matrix_3[$v->nama_mitra]['jalan']   = 0;
				$matrix_3[$v->nama_mitra]['sisa']    = 0;
				$matrix_3[$v->nama_mitra]['poin']    = 0;
			}

			switch ($v->lt_status) {
				case 'Selesai':
					$matrix[$v->datel]['selesai'] += 1;
					$matrix_2[$sektor_nm][$v->uraian][$v->datel]['selesai'] += 1;

					if (!in_array(session('auth')->pt2_level, [3, 6]))
					{
						$matrix_3[$v->nama_mitra]['selesai'] += 1;
					}

					$matrix[$v->datel]['poin'] += 25;
					$matrix_2[$sektor_nm][$v->uraian][$v->datel]['poin'] += 25;

					if (!in_array(session('auth')->pt2_level, [3, 6]))
					{
						$matrix_3[$v->nama_mitra]['poin'] += 25;
					}
				break;
				case 'Kendala':
					$matrix[$v->datel]['kendala'] += 1;
					$matrix_2[$sektor_nm][$v->uraian][$v->datel]['kendala'] += 1;

					if (!in_array(session('auth')->pt2_level, [3, 6]))
					{
						$matrix_3[$v->nama_mitra]['kendala'] += 1;
					}
				break;
				case (in_array($v->lt_status, ['Ogp', 'On Progress']) ? TRUE : FALSE):
					if($v->lt_status)
					{
						$matrix[$v->datel]['ogp'] += 1;
						$matrix_2[$sektor_nm][$v->uraian][$v->datel]['ogp'] += 1;

						if (!in_array(session('auth')->pt2_level, [3, 6]))
						{
							$matrix_3[$v->nama_mitra]['ogp'] += 1;
						}
					}
				break;
				case 'Pending':
					$matrix[$v->datel]['pending'] += 1;
					$matrix_2[$sektor_nm][$v->uraian][$v->datel]['pending'] += 1;

					if (!in_array(session('auth')->pt2_level, [3, 6]))
					{
						$matrix_3[$v->nama_mitra]['pending'] += 1;
					}
				break;
				case 'Berangkat':
					$matrix[$v->datel]['jalan'] += 1;
					$matrix_2[$sektor_nm][$v->uraian][$v->datel]['jalan'] += 1;

					if (!in_array(session('auth')->pt2_level, [3, 6]))
					{
						$matrix_3[$v->nama_mitra]['jalan'] += 1;
					}
				break;
				case 'Tidak Sempat':
					$matrix[$v->datel]['TS'] += 1;
					$matrix_2[$sektor_nm][$v->uraian][$v->datel]['TS'] += 1;

					if (!in_array(session('auth')->pt2_level, [3, 6]))
					{
						$matrix_3[$v->nama_mitra]['TS'] += 1;
					}
				break;
				default:
					$matrix[$v->datel]['sisa'] += 1;
					$matrix_2[$sektor_nm][$v->uraian][$v->datel]['sisa'] += 1;

					if (!in_array(session('auth')->pt2_level, [3, 6]))
					{
						$matrix_3[$v->nama_mitra]['sisa'] += 1;
					}
				break;
			}

			if(is_null($v->lt_status) )
			{
				$matrix[$v->datel]['sisa'] += 1;
				$matrix_2[$sektor_nm][$v->uraian][$v->datel]['sisa'] += 1;

				if (!in_array(session('auth')->pt2_level, [3, 6]))
				{
					$matrix_3[$v->nama_mitra]['sisa'] += 1;
				}
			}

			$matrix[$v->datel]['totalwo'] += 1;
			$matrix_2[$sektor_nm][$v->uraian][$v->datel]['totalwo'] += 1;

			if (!in_array(session('auth')->pt2_level, [3, 6]))
			{
				$matrix_3[$v->nama_mitra]['totalwo'] += 1;
			}
		}

		return ['table' => $matrix, 'matrix' => $matrix_2, 'matrix_per_mitra' => $matrix_3, 'matrix_per_pekerjaan' => $matrix_4];
	}

	public function getRanked($tgl)
	{
		if(strlen($tgl) <= 7)
		{
			$tgl = $tgl .'-01';
		}

		$month1 = $tgl;
		$month2 = date('Y-m-t', strtotime($month1) );
		$ranked = ReportModel::getRanked($month1, $month2);

		return view('Report.ranked', compact('ranked', 'month1', 'month2'));
	}

	public function getRanked_search($month1, $month2)
	{
		$ranked = ReportModel::getRanked($month1, $month2);
		return view('Tools.ranked_ajax', compact('ranked'));
	}

	public function getRanked_ajax($month1, $month2)
	{
		$ranked = ReportModel::getRanked($month1, $month2);

		if (empty($ranked))
		{
			$data = array(
				0 => (object)['selesai' => '0',
				'urutan' => 'kosong']
			);
		}

		foreach ($ranked as $row)
		{
			$data[] = $row;
		}
		return \Response::json($data);
	}

	public function reportMaterial($tgl)
	{
		$month1 = $tgl . '-01';
		$month2 = date('Y-m-d H:i:s');
		$mtr = ReportModel::getReportMaterial($month1, $month2);
		$data = array();
		$lastNama = '';
		$head = array();

		foreach ($mtr as $no => $m)
		{
			if (!empty($m->id))
			{
				$head[] = $m->id_item;
			}
		}

		$head = array_unique($head);

		foreach ($mtr as $no => $m)
		{
			if ($lastNama == $m->id)
			{
				$data[count($data) - 1]['id_item'][$m->id_item] = $m->qty;
			}
			else
			{
				$data[] = array(
					"odp_nama" => $m->odp_nama,
					"id_item" => array()
				);

				foreach ($head as $h)
				{
					if ($h == $m->id_item)
					{
						$data[count($data) - 1]['id_item'][$h] = $m->qty;
					}
					else
					{
						$data[count($data) - 1]['id_item'][$h] = 0;
					}
				}
			}
			$lastNama = $m->id;
		}
		// dd($data);
		return view('Report.reportMaterial', compact('head', 'data'));
	}

	public function reportMaterial_search($month1, $month2)
	{
		$mtr = ReportModel::getReportMaterial($month1, $month2);
		$data = array();
		$lastNama = '';
		$head = array();

		foreach ($mtr as $no => $m)
		{
			if (!empty($m->id))
			{
				$head[] = $m->id_item;
			}
		}

		$head = array_unique($head);

		foreach ($mtr as $no => $m)
		{
			if ($lastNama == $m->id)
			{
				$data[count($data) - 1]['id_item'][$m
					->id_item] = $m->qty;
			}
			else
			{
				$data[] = array(
					"odp_nama" => $m->odp_nama,
					"id_item" => array()
				);

				foreach ($head as $h)
				{
					if ($h == $m->id_item)
					{
						$data[count($data) - 1]['id_item'][$h] = $m->qty;
					}
					else
					{
						$data[count($data) - 1]['id_item'][$h] = 0;
					}
				}
			}
			$lastNama = $m->id;
		}
		return view('Tools.reportMaterial_ajax', compact('head', 'data'));
	}

	public function getphoto_Matrix($id, ReportModel $reportmodel)
	{
		// dd($id);
		$tc = New TeknisiController();
		$photo = $tc->photo;
		$photo_report = $reportmodel->photo_matrix($id);
		// dd($photo_report);
		return view('Report.Matrix.photo_pekerjaan', ['id' => $photo_report], compact('photo'));
	}

	public function upload_excel($year, $jenis, $month)
	{
		// dd('tes');
		if (strlen($month) < 2 && $month < 10)
		{
			$month = '0' . $month;
		}

		if ($month > 13)
		{
			$month = 12;
		}

		switch ($jenis) {
			case 'all':
				$add = null;
			break;
			default:
				$w_2 = substr($jenis, 1, 1) - 2;
				$w_1 = substr($jenis, 1, 1) - 1;
				$first_week = date('Y-m-d', strtotime("monday $w_2 week", strtotime($year.'-'.$month.'-01') ) );
				$last_week = date('Y-m-d', strtotime("sunday $w_1 week", strtotime($year.'-'.$month.'-01') ) );
				$add= [$first_week, $last_week];
				// dd($first_week, $last_week );
			break;
		}

		$bulan = array(
			1 => 'Januari',
			'Februari',
			'Maret',
			'April',
			'Mei',
			'Juni',
			'Juli',
			'Agustus',
			'September',
			'Oktober',
			'November',
			'Desember',
		);

		$data_rapung = ReportModel::getRekap_year_detail($year, $month, $add);
		$mydata = $tes = [];

		foreach ($data_rapung as $no => $value)
		{
			$get_b = '';

			if (!empty($value->tgl_mutlak))
			{
				$get_b = $bulan[date('n', strtotime($value->tgl_mutlak)) ];
			}

			$mydata['nomor']   = ++$no;
			$mydata['tanggal'] = date('Y-m-d', strtotime($value->tgl_mutlak) ) or '';
			$mydata['bulan']   = $get_b;
			$mydata['project'] = $value->project_name or '';
			$mydata['myir']    = $value->myir or $value->nomor_sc;
			$mydata['sc']      = $value->sc or $value->nomor_sc;
			$mydata['jenis']   = $value->jenis;
			$mydata['status']  = $value->laporan_status;
			$mydata['datel']   = $value->datel or '';
			$mydata['sto']     = $value->sto or '';
			$mydata['tim']     = $value->regu_name or '';
			$mydata['TL']      = $value->TL or '';
			$mydata['odp']     = $value->odp_nama or '';
			$mydata['koor']    = $value->lt_koordinat_odp or '';
			$mydata['catatan'] = str_replace('=', '', $value->lt_catatan);

			if (empty($value->lt_status))
			{
				$status = 'Need Progress';
				if (empty($value->regu_id))
				{
					$status = 'Belum Pilih Tim';
				}
			}
			else
			{
				$status = $value->lt_status;
			}

			$mydata['status'] = $status;

			if ($value->lanjut_PT1 == 1)
			{
				$stts_golive = 'Sudah Lanjut PT-1';
			}
			elseif($value->GOLIVE == 1)
			{
				$stts_golive = 'Sudah Go-LIVE';
			}
			elseif($value->upload_abd_4 == 1)
			{
				$stts_golive = 'Sudah Upload ABD Valid 4';
			}
			elseif($value->upload_abd_4 == 0)
			{
				$stts_golive = 'Belum Upload ABD Valid 4';
			}
			else
			{
				$stts_golive = 'Belum Go-Live';
			}

			$mydata['jenis_wo']       = $value->jenis_wo or '';
			$mydata['tgl_selesai']    = $value->tgl_selesai or '';
			$mydata['stts_golive']    = $stts_golive;
			$mydata['detail_kendala'] = $value->kendala_detail;
			$tes[$no] = $mydata;
		}

		$parent = ['NO', 'TGL', 'BULAN', 'NAMA PROYEK', 'MYIR', 'SC', 'JENIS', 'STATUS', 'DATEL', 'STO', 'NAMA TIM', 'NIK TEAM LEADER', 'NAMA OPD', 'KOOR ODP', 'CATATAN TEKNISI', 'STATUS ODP', 'JENIS PEKERJAAN', 'TANGGAL SELESAI', 'STATUS GO-LIVE', 'KENDALA DETAIL'];

		$bulan_nama = date('F', mktime(0, 0, 0, $month, 10));

		if ($year == date('Y') && $month == date('m'))
		{
			$judul = 'Report Rekap Data Tanggal ' . date('m') . ' ' . date('F', strtotime(date('Y-m-d'))) . ' ' . date('Y') . '.xlsx';
		}
		else
		{
			$judul = "Report Rekap Data Tahun $year.xlsx";
		}

		return Excel::download(new ExcelExport([$tes], $parent, 'rekap_data') , $judul);
	}

	public function rekap_material($month1, $month2)
	{
		$mtr = ReportModel::getReportMaterial($month1, $month2);
		$data = array();
		$lastNama = '';
		$head = array();

		foreach ($mtr as $no => $m)
		{
			if (!empty($m->id))
			{
				$head[] = $m->id_item;

				$mat_detail[$no]['id_item']  = $m->id_item;
				$mat_detail[$no]['uraian']   = $m->uraian;
				$mat_detail[$no]['material'] = $m->material;
				$mat_detail[$no]['jasa']     = $m->jasa;
				$mat_detail[$no]['satuan']   = $m->satuan;
			}
		}
		// dd($head);
		$head = array_unique($head);

		$temp = array_unique(array_column(array_values($mat_detail), 'id_item'));
		$mat_detail = array_intersect_key(array_values($mat_detail), $temp);

		foreach ($mtr as $no => $m)
		{
			if ($lastNama == $m->id)
			{
				$data[count($data) - 1]['id_item'][$m->id_item] = $m->qty;
			}
			else
			{
				$data[] = array(
					"odp_nama"     => $m->odp_nama,
					"regu_name"    => $m->regu_name,
					"sto"          => $m->sto,
					"rfc_key"			 => $m->rfc_key,
					"jenis_wo" 		 => $m->jenis_wo,
					"project_name" => $m->project_name,
					"id_item"      => array()
				);
				foreach ($head as $h)
				{
					if ($h == $m->id_item)
					{
						$data[count($data) - 1]['id_item'][$h] = $m->qty;
					}
					else
					{
						$data[count($data) - 1]['id_item'][$h] = 0;
					}
				}
			}
			$lastNama = $m->id;
		}

		$value = [];

		foreach ($data as $no => $r)
		{
			$mydata['nomor']        = ++$no;
			$mydata['Nama Team']    = $r['regu_name'];
			$mydata['odp_nama']     = $r['odp_nama'];
			$mydata['sto']          = $r['sto'];
			$mydata['rfc_key']			= $r['rfc_key'];
			$mydata['jenis_wo']     = $r['jenis_wo'];
			$mydata['nama_project'] = $r['project_name'];

			foreach ($r['id_item'] as $no2 => $item)
			{
				$mydata['item ' . $no2] = $item != 0 ? $item : '';
			}

			$value[$no] = $mydata;
		}

		return Excel::download(new ExcelExport([$value, $mat_detail], null, 'material') , 'Report Material Tanggal ' . date('m') . ' ' . date('F', strtotime(date('Y-m-d') ) ) . ' ' . date('Y') . '.xlsx');
	}

	public function ger_rfc($m1, $m2)
	{
		//perbandingan antara RFC yang keluar | material yang diinstal | Material yang masih di teknisi | RFC pengembalian
		//yg masih di teknisi itu saldonya action 1
		//rfc yg keluar tu alista_material_keluar
		//berarti yg ter install action nya 2
		//rfc pengembalian action nya 3
		$data = ReportModel::rfc_rekap($m1, $m2);
		return view('Report.material_rfc', compact('data'));
	}

	public function get_rfc_detail(Request $req)
	{
		$data = ReportModel::search_rfc($req->data);
		return \Response::json($data);
	}

	public function list_material()
	{
		$data = ReportModel::list_material();
		return view('Report.revenue_list', compact('data'));
	}

	public function edit_revenue($id)
	{
		$edit = ReportModel::findrevenue($id);
		return view('Report.revenue_edit', compact('edit'));
	}

	public function save_revenue(Request $req, $id)
	{
		ReportModel::save_revenue($req, $id);
	}

	public function delete_Revenue(Request $req)
	{
		reportmodel::del_rev($req->id);
	}

	public function up_monthly($date)
	{
		$data = ReportModel::monthly_ups($date);
		// dd($data);
		//dd($rowspan_data);
		$all_month = cal_days_in_month(0, date('n', strtotime($date)) , date('Y', strtotime($date)));

		$bulan = array(
			1 => 'Januari',
			'Februari',
			'Maret',
			'April',
			'Mei',
			'Juni',
			'Juli',
			'Agustus',
			'September',
			'Oktober',
			'November',
			'Desember',
		);

		$hari = array(
			1 => 'Senin',
			'Selasa',
			'Rabu',
			'Kamis',
			'Jumat',
			'Sabtu',
			'Minggu',
		);

		$bulan = $bulan[date('n', strtotime($date)) ];

		for ($i = 1;$i <= $all_month;++$i)
		{
			$dayss[date('d', strtotime($date . '-' . $i)) ] = $hari[date('N', strtotime($date . '-' . $i)) ];
		}
		$data_fixed = json_decode(json_encode($data) , true);
        // dd($data_fixed);
		$seenItems = array();
		foreach ($data_fixed as $index => $item)
		{
			// if (in_array($item["id"], $seenItems))
			if (in_array($item["regu_name"], $seenItems))
			{
				unset($data_fixed[$index]);
			}
			else
			{
				unset($data_fixed[$index]['jml_Selesai']);
				unset($data_fixed[$index]['hari']);
				// $seenItems[] = $item["id"];
				$seenItems[] = $item["regu_name"];
			}
		}
		$final_data = [];
		$new_structure = array();
		$data_fixed = array_values($data_fixed);
		// dd($data, $data_fixed);
		foreach ($data_fixed as $no_fixed => $dta_fixed)
		{
			$final_data[$no_fixed] = $dta_fixed;
			foreach ($data as $no_data => $dta)
			{
				// if ($dta_fixed['id'] == $dta->id)
				if ($dta_fixed['regu_name'] == $dta->regu_name)
				{
					$final_data[$no_fixed]['hari'][$dta->hari] = $dta->jml_Selesai;
				}
			}
		}

		$sto = $regu_name = $mitra = '';
		$dafa = [];
		// dd($final_data);
		foreach ($final_data as $no => $m)
		{
			foreach ($m['hari'] as $tgl => $dt)
			{
				// if ($sto == $m['sto'] && $regu_name == $m['regu_name'] && $mitra == $m['mitra'])
				if ($regu_name == $m['regu_name'] && $mitra == $m['mitra'])
				{
					$dafa[count($dafa) - 1]['hari'][$tgl] = $dt;
				}
				else
				{
					$dafa[] = array(
						"datel" => $m['datel'],
						// "sto" => $m['sto'],
						"mitra" => $m['mitra'],
						"regu" => $m['regu_name'],
						"hari" => array()
					);
					$sum = 0;

					foreach ($dayss as $no => $h)
					{
						if ($no == $tgl)
						{
							$dafa[count($dafa) - 1]['hari'][$no] = $dt;
						}
						else
						{
							$dafa[count($dafa) - 1]['hari'][$no] = 0;
						}
					}

				}

				// $sto = $m['sto'];
				$regu_name = $m['regu_name'];
				$mitra = $m['mitra'];
			}
		}
			// dd($dafa);

		$data_go_live = ReportModel::go_live(date('Y') );
		return view('Report.report_masis', compact('bulan', 'dayss', 'data_go_live') , ['data' => $dafa]);
	}

	public function report_daily()
	{
		return view('Report.download_page');
	}

	public function result_report_daily(Request $req)
	{
		$bulan = array(
			1 => 'Januari',
			'Februari',
			'Maret',
			'April',
			'Mei',
			'Juni',
			'Juli',
			'Agustus',
			'September',
			'Oktober',
			'November',
			'Desember',
		);

		if ($req->status == 'umur')
		{
			$date = explode(' - ', $req->date_v);
			$data = ReportModel::Umur_lapor();

			foreach ($data as $no => $value)
			{
				$stts_regu = '';

				if (isset($value->ACTIVE))
				{
					if ($value->ACTIVE == 1)
					{
						$stts_regu = 'Aktif';
					}
					else
					{
						$stts_regu = 'Non-Aktif';
					}
				}

				$mydata['nomor']          = ++$no;
				$mydata['sto']            = $value->sto or '';
				$mydata['nomor_sc']       = $value->nomor_sc or '';
				$mydata['nomor_sc_all']		= $value->nomor_sc_log or '';
				$mydata['regu']           = $value->regu_name or '';
				$mydata['status']         = $stts_regu;
				$mydata['jenis']          = $value->jenis;
				$mydata['status_laporan'] = $value->laporan_status;
				$mydata['tanggal']        = $value->tanggal or '';
				$mydata['umur']           = (int)$value->umur;
				$mydata['odp_nama']       = $value->odp_nama or '';
				$mydata['odp_koor']       = $value->odp_koor or '';
				$mydata['project_name']   = $value->project_name or '';
				$mydata['lt_status']      = $value->lt_status or '';

				if ($value->lanjut_PT1 == 1)
				{
					$stts_golive = 'Sudah Lanjut PT-1';
				}
				elseif($value->GOLIVE == 1)
				{
					$stts_golive = 'Sudah Go-LIVE';
				}
				elseif($value->upload_abd_4 == 1)
				{
					$stts_golive = 'Sudah Upload ABD Valid 4';
				}
				elseif($value->upload_abd_4 == 0)
				{
					$stts_golive = 'Belum Upload ABD Valid 4';
				}
				else
				{
					$stts_golive = 'Belum Go-Live';
				}

				$mydata['tgl_selesai']    = $value->tgl_selesai or '';
				$mydata['stts_golive']    = $stts_golive;
				$mydata['kndl_detail']    = $value->kendala_detail or '';

				$tes[] = $mydata;
			}

			$parent = ['NO', 'STO', 'NOMOR SC', 'SEMUA NOMOR SC', 'REGU', 'STATUS REGU', 'JENIS', 'STATUS PSB', 'TANGGAL', 'UMUR (HARI)', 'ODP NAMA', 'KOOR ODP', 'NAMA PROJECT', 'STATUS ODP', 'TANGGAL SELESAI', 'STATUS GO-LIVE', 'KENDALA DETAIL'];
			$judul_data = 'Umur Laporan';
		}
		elseif ($req->status == 'sync_psb')
		{
			$data = ReportModel::detail_lapor($req);

			foreach ($data as $no => $value)
			{
				$mydata['nomor']        = ++$no;
				$mydata['datel']        = $value->datel;
				$mydata['sto']          = ($value->sto ?: $value->sto_B);
				$mydata['myir']         = $value->Ndem;
				$mydata['status']       = $value->laporan_status;
				$mydata['tanggal']      = ($value->tanggal ?: $value->tanggal_psb);
				$mydata['odp_nama']     = ($value->odp_nama ?: $value->odp_nama_psb);
				$mydata['project_name'] = ($value->project_name ?: $value->project_name_psb);
				$mydata['regu_name']    = $value->regu_name;
				$mydata['lt_status']    = $value->lt_status;

				$tes[$no] = $mydata;
			}

			$parent = ['NO', 'DATEL', 'STO', 'MYIR', 'STATUS', 'TANGGAL', 'ODP NAMA', 'NAMA PELANGGAN', 'REGU', 'STATUS'];
			$judul_data = 'PSB Laporan';

		}
		elseif ($req->status == 'matrix')
		{
			$date = explode(' - ', $req->date_v);
			$that = new AdminModel;
			$id_stts = implode(', ', array_keys($that::status() ) );
			$data = $this->matrix_all_flex($date[0], $date[1], 'empty', $id_stts, '0, 1');
			$no = 0;

			foreach ($data['matrix'] as $no3 => $val3)
			{
				foreach($val3 as $no2 => $val2)
				{
					foreach($val2 as $no1 => $val1)
					{
						foreach ($val1['order'] as $value)
						{
							$stts_regu = '';

							if (isset($value->ACTIVE))
							{
								if ($value->ACTIVE == 1)
								{
									$stts_regu = 'Aktif';
								}
								else
								{
									$stts_regu = 'Non-Aktif';
								}
							}

							$no++;
							$mydata['nomor']          = ++$no;
							$mydata['sto']            = $value->sto or '';
							$mydata['nomor_sc']       = $value->nomor_sc or '';
							$mydata['nomor_sc_all']		= $value->nomor_sc_log or '';
							$mydata['regu']           = $value->regu_name or '';
							$mydata['status']         = $stts_regu;
							$mydata['jenis']          = $value->jenis;
							$mydata['umur']           = $value->durasi;
							$mydata['odp_nama']       = $value->odp_nama or '';
							$mydata['odp_koor']       = $value->odp_koor or '';
							$mydata['project_name']   = $value->project_name or '';
							$mydata['lt_status']      = $value->lt_status or '';

							if ($value->lanjut_PT1 == 1)
							{
								$stts_golive = 'Sudah Lanjut PT-1';
							}
							elseif($value->GOLIVE == 1)
							{
								$stts_golive = 'Sudah Go-LIVE';
							}
							elseif($value->upload_abd_4 == 1)
							{
								$stts_golive = 'Sudah Upload ABD Valid 4';
							}
							elseif($value->upload_abd_4 == 0)
							{
								$stts_golive = 'Belum Upload ABD Valid 4';
							}
							else
							{
								$stts_golive = 'Belum Go-Live';
							}

							$mydata['tgl_selesai']    = $value->tgl_selesai or '';
							$mydata['stts_golive']    = $stts_golive;
							$mydata['kndl_detail']    = $value->kendala_detail or '';

							$tes[] = $mydata;
						}
					}
				}
			}

			$parent = ['NO', 'STO', 'NOMOR SC', 'SEMUA NOMOR SC', 'REGU', 'STATUS REGU', 'JENIS', 'UMUR', 'ODP NAMA', 'KOOR ODP', 'NAMA PROJECT', 'STATUS ODP', 'TANGGAL SELESAI', 'STATUS GO-LIVE', 'KENDALA DETAIL'];
			$judul_data = 'Laporan Matrix';
		}
		elseif ($req->status == 'estimasi_kendala')
		{
			$date = explode(' - ', $req->date_v);

			$data = ReportModel::get_kendala_estimasi();

			foreach ($data as $no => $value)
			{
				$stts_regu = '';
				if (isset($value->ACTIVE))
				{
					if ($value->ACTIVE == 1)
					{
						$stts_regu = 'Aktif';
					}
					else
					{
						$stts_regu = 'Non-Aktif';
					}
				}

				$mydata['nomor']        = ++$no;
				$mydata['datel']        = $value->datel;
				$mydata['sto']          = $value->sto or '';
				$mydata['nomor_sc']     = $value->nomor_sc or '';
				$mydata['jenis']        = $value->jenis_wo or '';
				$mydata['tanggal_buat'] = $value->tgl_buat or '';
				$mydata['tgl_kerja']    = $value->tgl_pengerjaan or '';
				$mydata['odp_nama']     = $value->odp_nama or '';
				$mydata['project_name'] = $value->project_name or '';
				$mydata['odp_koor']     = $value->odp_koor or '';
				$mydata['regu_name']    = $value->regu_name or '';
				$mydata['stts_regu']    = $stts_regu;
				$mydata['lt_status']    = $value->lt_status or '';

				if ($value->lanjut_PT1 == 1)
				{
					$stts_golive = 'Sudah Lanjut PT-1';
				}
				elseif($value->GOLIVE == 1)
				{
					$stts_golive = 'Sudah Go-LIVE';
				}
				elseif($value->upload_abd_4 == 1)
				{
					$stts_golive = 'Sudah Upload ABD Valid 4';
				}
				elseif($value->upload_abd_4 == 0)
				{
					$stts_golive = 'Belum Upload ABD Valid 4';
				}
				else
				{
					$stts_golive = 'Belum Go-Live';
				}

				$mydata['stts_golive'] = $stts_golive;
				$mydata['tgl_submit']	 = $value->tgl_selesai;
				$mydata['estimasi']	   = $value->tgl_estimasi;
				$mydata['kndl_detail'] = $value->kendala_detail or '';
				$tes[$no] = $mydata;
			}

			$parent = ['NO', 'DATEL', 'STO', 'NOMOR SC', 'JENIS', 'TANGGAL BUAT', 'TANGGAL KERJA', 'ODP NAMA', 'NAMA PELANGGAN', 'KOOR ODP', 'REGU', 'STATUS REGU', 'STATUS LAPORAN', 'STATUS GO-LIVE', 'TANGGAL SELESAI', 'ESTIMASI', 'KENDALA DETAIL'];
			$judul_data = 'Kendala Laporan';

		}

		return Excel::download(new ExcelExport([$tes], $parent, 'matrix') , $judul_data.'.xlsx');
	}

	public static function pt2_kpro()
	{ //BUG MASIH BELUM BISA TEMBUS
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_HEADER, true);
		curl_setopt($ch, CURLOPT_URL, 'https://kpro.telkom.co.id/kpro/pt2/detaillmesurvey?status=NOK&periode=2020&mode=STO&reg=6&witel=BANJARMASIN&STO=ALL');
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$result = curl_exec($ch);
		$dom = new \DOMDocument();
		libxml_use_internal_errors(true);
		$dom->loadHTML($result);
		$form = $dom->getElementsByTagName('form')
			->item(0);
		$input = $form->getElementsByTagName('input');
		$csrf = $input->item(1)
			->getAttribute('value');
		$captcha = $input->item(4)
			->getAttribute('value');
		$cookie_file_path = "bigbox12=hvi5u3v7l6a99sft0s1aj155kl; SC_wt_lbqsp_iuuqt=ffffffff0936bf2e45525d5f4f58455e445a4a42378b";
		curl_setopt($ch, CURLOPT_COOKIE, $cookie_file_path);
		curl_setopt($ch, CURLOPT_COOKIESESSION, true);

		// curl_setopt($ch, CURLOPT_POSTFIELDS, "ID=&csrf=$csrf&uname=94160110&passw=telkom135&captcha%5Bid%5D=$captcha&captcha%5Binput%5D=571&agree=0&agree=1");
		// preg_match_all('/^Set-Cookie:\s*([^;]*)/mi', $result, $matches);
		// $cookies = [];
		// foreach ($matches[1] as $item) {
		//     parse_str($item, $cookie);
		//     $cookies = array_merge($cookies, $cookie);
		// }
		$tes2 = curl_exec($ch);

		curl_close($ch);
		dd($tes2);
	}

	public function list_order_outside_pt2()
	{
		$data_nog = ReportModel::list_nog();
		$data_pt2_psb = ReportModel::list_pt2_psb();

		$datel = ReportModel::get_all_datel();
		$datel = json_decode(json_encode($datel), TRUE);

		foreach($data_nog as $v)
		{
			$find_k =array_search($v->sto_nog, array_column($datel, 'sto') );
			$dt = 'KOSONG';

			if($find_k !== FALSE)
			{
				$dt = $datel[$find_k]['datel'];
			}

			if(!isset($list_nog[$dt]['sto'][$v->sto_nog]) )
			{
				$list_nog[$dt]['sto'][$v->sto_nog] = 0;
			}

			$list_nog[$dt]['sto'][$v->sto_nog] += 1;
		}

		foreach($data_pt2_psb as $v)
		{
			$find_k =array_search($v->sto, array_column($datel, 'sto') );
			$dt = 'KOSONG';

			if($find_k !== FALSE)
			{
				$dt = $datel[$find_k]['datel'];
			}

			if(!isset($list_psb[$dt]['sto'][$v->sto]) )
			{
				$list_psb[$dt]['sto'][$v->sto] = 0;
			}

			$list_psb[$dt]['sto'][$v->sto] += 1;
		}
		return view('Report.dashboard_Nog', compact('list_nog', 'list_psb') );
	}


	public function input_Nog($jenis, $id, AdminModel $adminmodel)
	{
		$distinct_regu   = AdminModel::list_regu_pt2('aktif');
		$admincontroller = new AdminController();
		$photodispatch   = $admincontroller->photodispatch;

		$PO   = AdminModel::po_select();
		$sto  = AdminModel::get_sto();
		$odc  = AdminModel::get_odc();
		$j_or = AdminModel::status();

		switch ($jenis) {
			case 'NOG':
				$data_d      = ReportModel::getdetail_nog($id);
				$odp         = explode('-', $data_d->nama_odp)[1];
				$data_d->sto = $odp;

				$data_d->odp_nama     = $data_d->nama_odp;
				$data_d->odp_koor     = $data_d->koordinat_odp;
				$data_d->aspl_nama    = null;
				$data_d->aspl_koor    = null;
				$data_d->odc_nama     = $data_d->nama_odp;
				$data_d->project_name = 'NOG dari '. $data_d->jenis_order . ' port '. $data_d->jml_port;
				$data_d->catatan_HD = 'NOG dari '. $data_d->jenis_order . ' port '. $data_d->jml_port;
				$jenis= 'NOG MARINA';
			break;
			case 'PSB':
				$data_d = ReportModel::getdetail_psb($id);

				if($data_d->nama_odp)
				{
					$odp = explode('-', $data_d->nama_odp)[1];
					$data_d->sto = $odp;

					$data_d->odp_nama = $data_d->nama_odp;
					$data_d->odp_koor = $data_d->kordinat_odp;
					$data_d->odc_nama = $data_d->nama_odp;
				}

				$data_d->aspl_nama    = null;
				$data_d->aspl_koor    = null;
				$data_d->project_name = $data_d->orderName;
				$data_d->nomor_sc     = $data_d->orderId;
				$jenis= 'PSB';
			break;
		}

		return view('Admin.order_non_unsc', ['regu' => $distinct_regu, 'po' => $PO, 'sto' => $sto, 'odc' => $odc, 'data_d' => $data_d, 'id' => $id], compact('photodispatch' , 'j_or', 'jenis'));
	}

	public function save_Nog(Request $req, $jenis, $id)
	{
		switch ($jenis) {
			case 'NOG':
				$main_data = ['jenis' => 'NOG', 'data' => ReportModel::getdetail_nog($id)];
			break;
			case 'PSB':
				$main_data = ['jenis' => 'PSB', 'data' => ReportModel::getdetail_psb($id)];
			break;
		}

		$msg = AdminModel::save_dispatch_non_unsc($req, $id, $main_data);
		AdminModel::save_nog($req, $id);
		return redirect("/home/" . date('Y') )->with('alerts_tele', $msg);
	}

	public function getDetailMatrix($datel, $mitra, $jenis_h, $tgl, Request $req)
	{
		if (empty($req->status))
		{
			$status = 'empty';
		}
		else
		{
			$status = implode(',', $req->status);
		}

		if (empty($req->stts_regu))
		{
			$status_rg = '1';
		}
		else
		{
			$status_rg = implode(',', $req->stts_regu);
		}

		if (empty($req->tgl_a) )
		{
			$tgl = $tgl;
		}
		else
		{
			$tgl = $req->tgl_a;
		}

		if (empty($req->tgl_f))
		{
			$tglb = date('Y-m-d H:i:s');
		}
		else
		{
			$tglb = $req->tgl_f;
		}

		$jenis = '';

		if (empty($req->jenis) || strcasecmp($req->jenis, 'All') != 0)
		{
			$jenis = implode(', ', array_keys(AdminModel::status() ) );
		}
		else
		{
			$jenis = implode(',', $req->jenis);
		}

		$data = ReportModel::getMatrix_search($tgl, $tglb, $status, $jenis, $status_rg, $mitra);

		switch ($jenis_h) {
			case 'selesai':
				$filter_status = 'Selesai';
			break;
			case 'kendala':
				$filter_status = 'Kendala';
			break;
			case 'ogp':
				$filter_status = 'Ogp';
			break;
			case 'pending':
				$filter_status = 'Pending';
			break;
			case 'jalan':
				$filter_status = 'Berangkat';
			break;
			case 'TS':
				$filter_status = 'Tidak Sempat';
			break;
			case 'sisa':
				$filter_status = null;
			break;
			default:
				$filter_status = 'all';
			break;
		}

		foreach($data as $k => $v)
		{
			if(strcasecmp($datel, 'All') != 0 && $v->datel != $datel)
			{
				unset($data[$k]);
			}

			if(strcasecmp($filter_status, 'All') != 0)
			{
				if($filter_status)
				{
					if($filter_status == 'Ogp' && !in_array($v->lt_status, ['Ogp', 'On Progress']) )
					{
						unset($data[$k]);
					}
					elseif($filter_status == null && !in_array($v->lt_status, ['Ogp', 'On Progress']) )
					{
						unset($data[$k]);
					}
					elseif($v->lt_status != $filter_status)
					{
						unset($data[$k]);
					}
				}
				else
				{
					if(in_array($v->lt_status, ['Selesai', 'Kendala', 'Ogp', 'Pending', 'Berangkat', 'Tidak Sempat']) )
					{
						unset($data[$k]);
					}
				}
			}
		}

		$data = array_values($data);
		return view('Admin.show_list_wo', compact('data') );
	}

	public function list_outside_nog($jenis, $datel, $sto)
	{
		if($jenis == 'nog_m')
		{
			$data = ReportModel::list_nog($datel, $sto);
			$judul = 'NOG MARINA';
		}
		else
		{
			$data = ReportModel::list_pt2_psb($datel, $sto);
			$judul = 'PSB PT-2';
		}

		$datel = urldecode($datel);
		return view('Report.list_not_outside', compact('data', 'jenis', 'datel', 'sto', 'judul') );
	}

	public function detail_report($tim, $d)
	{
		$tim = urldecode($tim);
		$data = ReportModel::get_detail_report_masis($tim, $d);
		return view('Admin.show_list_wo', compact('data') );
	}

	public function geo()
	{
		$latlong = new \stdClass();
		$latlong->lat = - 3.43568915592;
		$latlong->lng = 114.742038675;
		$result = json_encode($latlong);
		return view('Admin.geografi', compact('result'));
	}

	public function geo_find($lat, $long)
	{
		$latlong = new \stdClass();
		$latlong->lat = (double)$lat;
		$latlong->lng = (double)$long;
		$result = json_encode($latlong);
		// dd($result);
		return view('Admin.geografi', compact('result'));
	}

	public function geo_getAllOrder(Request $req)
	{
		return json_encode(AdminModel::getAllOrder($req));
	}

	public function naik_core($date)
	{
		$data = ReportModel::core_up($date);

		$fd = [];

		foreach($data as $k => $v)
		{
			$fd[$v->created_by]['nik'] = $v->created_by;
			$fd[$v->created_by]['nama'] = $v->nama;

			if(!isset($fd[$v->created_by]['qty_total']) )
			{
				$fd[$v->created_by]['qty_total'] = 0;
			}

			if(!isset($fd[$v->created_by]['material'][$v->id_item]['qty']) )
			{
				$fd[$v->created_by]['material'][$v->id_item]['qty'] = 0;
			}

			$fd[$v->created_by]['qty_total'] += $v->qty;

			$fd[$v->created_by]['material'][$v->id_item]['id_item'] = $v->id_item;

			$fd[$v->created_by]['material'][$v->id_item]['qty'] += $v->qty;
		}

		usort($fd, function($a, $b) {
			return $b['qty_total'] <=> $a['qty_total'];
		});

		return view('Report.ranked_core', compact('fd') );
	}

	public function umur_pekerjaan()
	{
		$call_sql = ReportModel::umur_kerjaan();

		$data = [];

		foreach($call_sql as $v)
		{
			$data[$v->regu_name]['regu_id']     = $v->regu_id;
			$data[$v->regu_name]['regu']        = $v->regu_name;
			$data[$v->regu_name]['ACTIVE']      = $v->ACTIVE;
			$data[$v->regu_name]['mitra_amija'] = $v->mitra_amija_pt ?? 'Kosong';

			if(!isset($data[$v->regu_name]['isi']) )
			{
				$data[$v->regu_name]['isi']['total'] = 0;
				$data[$v->regu_name]['isi']['_1']    = 0;
				$data[$v->regu_name]['isi']['1_3']   = 0;
				$data[$v->regu_name]['isi']['4_7']   = 0;
				$data[$v->regu_name]['isi']['7_']    = 0;
			}

			$date2 = strtotime(date('Y-m-d H:i:s') );

			$date_progg = strtotime($v->modified_at);
			$seconds_prog = $date2 - $date_progg;

			$daily_progg = floor(abs($seconds_prog / (60 * 60 * 24) ) );

			$data[$v->regu_name]['isi']['total'] += 1;

			if($daily_progg < 1 )
			{
				$data[$v->regu_name]['isi']['_1'] += 1;
			}
			elseif($daily_progg >= 1 && $daily_progg <= 3 )
			{
				$data[$v->regu_name]['isi']['1_3'] += 1;
			}
			elseif($daily_progg > 3 && $daily_progg <= 7)
			{
				$data[$v->regu_name]['isi']['4_7'] += 1;
			}
			elseif($daily_progg > 7)
			{
				$data[$v->regu_name]['isi']['7_'] += 1;
			}
		}

		$data = array_values($data);
		return view('Report.umur_non_jointer', compact('data') );
	}

	public function umur_pekerjaan_detail($mitra, $regu, $jenis)
	{
		$data = ReportModel::umur_kerjaan($mitra, $regu, $jenis);
		return view('Report.detail_umur_non_join', compact('data') );
	}

	public  function DMStoDD($input)
	{
		$deg = " " ;
		$min = " " ;
		$sec = " " ;
		$inputM = " " ;

		for ($i=0; $i < strlen($input); $i++)
		{
			$tempD = $input[$i];
			if ($tempD == iconv("UTF-8", "ISO-8859-1//TRANSLIT", '°') )
			{
				$newI = $i + 1 ;

				$inputM =  substr($input, $newI, -1) ;
				break;
			}

			$deg .= $tempD ;
		}

		for ($j=0; $j < strlen($inputM); $j++)
		{
			$tempM = $inputM[$j];
			if ($tempM == "'")
			{
				$newI = $j + 1 ;
				$sec =  substr($inputM, $newI, -1) ;
				break;
			}
			$min .= $tempM ;
		}

		return trim($deg) + ($min / 60) + ($sec / 3600);
		}

	public function geo_unsc(Request $req)
	{
		$data = AdminModel::getAllOrder_unsc($req->jenis);
		$renew_data = [];

		foreach($data as $key => $new_data)
		{
			if(json_encode($new_data) != false){
				$ex = explode(',', $new_data['koor']);

				$minus = '';

				if (strpos($ex[0], 'S') !== false) {
					$minus = '-';
				}

				if(!@$ex[1])
				{
					$ex = preg_split("/[\s]/", $new_data['koor']);
				}

				if(!@$ex[1])
				{
					$ex = preg_split("/°/", $new_data['koor']);
				}

				$new_data['koor'] = $minus .''. number_format((float)$this->DMStoDD($ex[0]), 6, '.', '') .','. number_format((float)$this->DMStoDD($ex[1]), 6, '.', '');

				$renew_data[$key] = $new_data;
			}else{
				$ex = explode(',', $new_data['koor']);

				$minus = '';
				if (strpos($ex[0], 'S') !== false) {
					$minus = '-';
				}

				if(!@$ex[1])
				{
					$ex = preg_split("/[\s]/", $new_data['koor']);
				}

				if(!@$ex[1])
				{
					$ex = preg_split("/°/", $new_data['koor']);
				}

				$new_data['koor'] = $minus .''. number_format((float)$this->DMStoDD($ex[0]), 6, '.', '') .','. number_format((float)$this->DMStoDD($ex[1]), 6, '.', '');
				$wrong[] = $new_data;

				$renew_data[$key] = preg_replace('/[[:^print:]]/', '', $new_data);
			}
		}

		return json_encode($renew_data);
	}

	public function up_monthly_excel($date)
	{
		$data = ReportModel::monthly_ups_excel($date);
		$datel = ReportModel::sdi_datel();

		foreach ($data as $key => $value)
		{
			foreach ($datel as $val2)
			{
				if ($value->sto == $val2->sto)
				{
					$data[$key]->datel = $val2->datel;
				}
			}
		}

		$mitra_sto = [
			'AMT' => 'CUI',
			'BBR' => 'TBN',
			'BJM' => 'STM',
			'BLC' => 'CUI',
			'BRI' => 'GTM',
			'BTB' => 'TBN',
			'GMB' => 'UPATEK',
			'KDG' => 'GTM',
			'KPL' => 'CUI',
			'KYG' => 'UPATEK',
			'LUL' => 'GTM',
			'MRB' => 'CUI',
			'MTP' => 'CUI',
			'NEG' => 'GTM',
			'PGN' => 'CUI',
			'PGT' => 'CUI',
			'PLE' => 'TBN',
			'RTA' => 'GTM',
			'SER' => 'CUI',
			'STI' => 'CUI',
			'TJL' => 'CUI',
			'TKI' => 'TBN',
			'ULI' => 'UPATEK',
		];

		foreach ($data as $no => $value)
		{
			$mydata['nomor']      = ++$no;
			$mydata['tanggal']    = @date('Y-m-d', strtotime($value->tgl_pengerjaan) );
			$mydata['tanggal_jm'] = ($value->tgl_pengerjaan);
			$mydata['tgl_buat']   = @date('Y-m-d', strtotime($value->tgl_buat) );
			$mydata['project']    = $value->project_name or '';
			$mydata['scid']       = $value->nomor_sc or '';
			$mydata['datel']      = @$value->datel or '';
			$mydata['sto']        = $value->sto or '';
			$mydata['tim']        = $value->regu_name or '';
			$mydata['Mitra Regu'] = $value->nama_mitra or '';
			$mydata['Mitra STO']  = @$mitra_sto[$value->sto];
			$mydata['odp']        = $value->odp_nama or '';
			$mydata['koor']       = @$value->lt_koordinat_odp or '';
			$mydata['catatan']    = str_replace('          = ', '', $value->lt_catatan);
			$mydata['status']     = $value->lt_status or '';

			if ($value->lanjut_PT1 == 1)
			{
				$stts_golive = 'Sudah Lanjut PT-1';
			}
			elseif($value->GOLIVE == 1)
			{
				$stts_golive = 'Sudah Go-LIVE';
			}
			elseif($value->upload_abd_4 == 1)
			{
				$stts_golive = 'Sudah Upload ABD Valid 4';
			}
			elseif($value->upload_abd_4 == 0)
			{
				$stts_golive = 'Belum Upload ABD Valid 4';
			}
			else
			{
				$stts_golive = 'Belum Go-Live';
			}

			$mydata['tgl_selesai']    = $value->tgl_selesai or '';
			$mydata['jenis_wo']       = $value->jenis_wo or '';
			$mydata['status_go_live'] = $stts_golive;
			$mydata['kendala_detail'] = $value->kendala_detail or '';
			$tes[$no] = $mydata;
		}

		$parent = ['NO', 'TGL', 'TGL & JAM', 'TGL BUAT', 'NAMA PROYEK', 'SCID', 'DATEL', 'STO', 'NAMA TIM', 'MITRA REGU', 'MITRA STO', 'NAMA OPD', 'KOOR ODP', 'CATATAN TEKNISI', 'STATUS ODP', 'TANGGAL SELESAI', 'JENIS WO', 'STATUS GO-LIVE', 'KENDALA DETAIL'];

		$judul = "Report Pekerjaan Selesai.xlsx";

		return Excel::download(new ExcelExport([$tes], $parent, 'rekap_data') , $judul);
	}
}