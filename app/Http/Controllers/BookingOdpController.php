<?php
namespace App\Http\Controllers;

use App\DA\AdminModel;
use App\DA\ReportModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Facades\Excel;

date_default_timezone_set("Asia/Makassar");
class BookingOdpController extends Controller
{
	protected $sheetid = "1obn3WZF2wSN48_tNOHpDI5Fha_jRf7lasT9Jw_TaXbc";
	protected $f_o = '<- FREE ODP';
	protected $f_o_id = '1107974403';
	protected $b_o = 'Booking ODP';
	protected $b_o_id = '199670277';

	public function show_gd()
	{
		$client = new \Google_Client();
		$client->setApplicationName('My PHP App');
		$client->setScopes([\Google_Service_Sheets::SPREADSHEETS]);
		$client->setAccessType('offline');
		$client->setAuthConfig('/srv/htdocs/d_liauw/pt2_dev/public/google_pt2.json');
		$spreadsheetId = $this->sheetid;

		$sheets = new \Google_Service_Sheets($client);
		$sheet = $sheets
			->spreadsheets
			->get($this->sheetid)
			->getSheets();

		foreach ($sheet as $sheetl)
		{
			$list[] = $sheetl
				->properties->title;
		}

		$rows = $sheets
			->spreadsheets_values
			->get($this->sheetid, $this->b_o, ['majorDimension' => 'ROWS']);

		unset($rows->values[0]);

		$data = array_reverse($rows->values, true);

		return view('BookingOdp.booking_odp', ['data' => $data]);
	}

	public function edit_data_booking($id)
	{
		$client = new \Google_Client();
		$client->setApplicationName('My PHP App');
		$client->setScopes([\Google_Service_Sheets::SPREADSHEETS]);
		$client->setAccessType('offline');
		$client->setAuthConfig('/srv/htdocs/d_liauw/pt2_dev/public/google_pt2.json');
		$spreadsheetId = $this->sheetid;

		$sheets = new \Google_Service_Sheets($client);
		$sheet = $sheets
			->spreadsheets
			->get($this->sheetid)
			->getSheets();

		foreach ($sheet as $sheetl)
		{
			$list[] = $sheetl
				->properties->title;
		}

		$rows = $sheets
			->spreadsheets_values
			->get($this->sheetid, $this->b_o, ['majorDimension' => 'ROWS']);

		$rows->values = array_values($rows->values);

		$get_dt_free['ket'] = $rows->values[$id][10];
		$get_dt_free['mitra'] = $rows->values[$id][1];
		$get_dt_free['site_id'] = $rows->values[$id][2];
		$get_dt_free['site_nm'] = $rows->values[$id][3];
		$get_dt_free['koor'] = $rows->values[$id][6];
		$get_dt_free['req'] = $rows->values[$id][8];
		$get_dt_free['project'] = $rows->values[$id][7];
		$result['mydata'] = $get_dt_free;
		$result['free_odp_msg'] = null;
		$result['odp'][] = $rows->values[$id][4];

		$rows_check_m = $sheets
			->spreadsheets_values
			->get($this->sheetid, 'V Data', ['majorDimension' => 'ROWS']);

		$unset_array = range(0, 29);
		foreach ($unset_array as $arrayun)
		{
			unset($rows_check_m->values[$arrayun]);
		}

		$mitra = $rows_check_m->values;

		return view('BookingOdp.booking_odp_create', compact('result', 'mitra'));
	}

	public function update_data_booking(Request $req, $id)
	{
		$client = new \Google_Client();
		$client->setApplicationName('My PHP App');
		$client->setScopes([\Google_Service_Sheets::SPREADSHEETS]);
		$client->setAccessType('offline');
		$client->setAuthConfig('/srv/htdocs/d_liauw/pt2_dev/public/google_pt2.json');
		$spreadsheetId = $this->sheetid;

		$sheets = new \Google_Service_Sheets($client);
		$sheet = $sheets
			->spreadsheets
			->get($this->sheetid)
			->getSheets();

		foreach ($sheet as $sheetl)
		{
			$list[] = $sheetl
				->properties->title;
		}

		$rows = $sheets
			->spreadsheets_values
			->get($this->sheetid, $this->b_o, ['majorDimension' => 'ROWS']);
		$arr_odp = json_decode($req->odp_nama, true);
		$exp = explode('/', $arr_odp[0]);
		$exp_odp = explode('-', $exp[0]);
		//XXX: insert latest data
		$row_skrg = $id + 1;
		$data_booking[] = new \Google_Service_Sheets_ValueRange(['range' => 'Sheet1!A' . $row_skrg, 'values' => [[$rows->values[$id][0], $req->mitra, $req->site_id, $req->site_nm, $arr_odp[0], \Google_Model::NULL_VALUE, $req->odp_koor, '=VLOOKUP(E' . $row_skrg . ',\'REPORT ODP KALSEL\'!C:P,14,false)', $req->req, \Google_Model::NULL_VALUE, $req->ket, date('d/m/Y') , \Google_Model::NULL_VALUE, \Google_Model::NULL_VALUE, \Google_Model::NULL_VALUE, \Google_Model::NULL_VALUE, \Google_Model::NULL_VALUE, Session::get('auth')->nama, \Google_Model::NULL_VALUE, $exp_odp[1], $exp_odp[2], ]]]);

		$body = new \Google_Service_Sheets_BatchUpdateValuesRequest(['valueInputOption' => 'USER_ENTERED', 'data' => $data_booking]);
		$sheets
			->spreadsheets_values
			->batchUpdate($spreadsheetId, $body);
		return redirect('/show/booking_odp')->with('alerts', [['type' => 'success', 'text' => implode(' dan ', $arr_odp) . " Berhasil Ditambah!"]]);

	}

	public function create_booking_odp()
	{
		return view('BookingOdp.check_odp_booking');
	}

	public function save_data_booking_odp(Request $req)
	{
		$arr_odp = json_decode($req->odp_nama, true);
		$client = new \Google_Client();
		$client->setApplicationName('My PHP App');
		$client->setScopes([\Google_Service_Sheets::SPREADSHEETS]);
		$client->setAccessType('offline');
		$client->setAuthConfig('/srv/htdocs/d_liauw/pt2_dev/public/google_pt2.json');
		$spreadsheetId = $this->sheetid;

		$sheets = new \Google_Service_Sheets($client);
		$sheet = $sheets
			->spreadsheets
			->get($this->sheetid)
			->getSheets();

		foreach ($sheet as $sheetl)
		{
			$list[] = $sheetl
				->properties->title;
		}

		$rows = $sheets
			->spreadsheets_values
			->get($this->sheetid, $this->b_o, ['majorDimension' => 'ROWS']);

		//cek row
		$get_all_row = $sheets
			->spreadsheets
			->get($spreadsheetId, ['ranges' => $this
			->b_o])
			->sheets[0]
			->properties
			->gridProperties->rowCount;

		if ($get_all_row < count($arr_odp) + count($rows->values))
		{
			$body_insert_row_BO = new \Google_Service_Sheets_BatchUpdateSpreadsheetRequest(["requests" => ["insertDimension" => ["range" => ["sheetId" => $this->b_o_id, "dimension" => "ROWS", "startIndex" => $get_all_row, "endIndex" => count($arr_odp) + count($rows->values) ], "inheritFromBefore" => true]]]);
			$sheets
				->spreadsheets
				->batchUpdate($spreadsheetId, $body_insert_row_BO);
		}

		$get_all_row_FO = $sheets
			->spreadsheets
			->get($spreadsheetId, ['ranges' => $this
			->f_o])
			->sheets[0]
			->properties
			->gridProperties->rowCount;

		if ($get_all_row_FO < count($arr_odp) + count($rows->values))
		{
			$body_insert_row_FO = new \Google_Service_Sheets_BatchUpdateSpreadsheetRequest(["requests" => ["insertDimension" => ["range" => ["sheetId" => $this->f_o_id, "dimension" => "ROWS", "startIndex" => $get_all_row_FO, "endIndex" => count($arr_odp) + count($rows->values) ], "inheritFromBefore" => true]]]);
			$sheets
				->spreadsheets
				->batchUpdate($spreadsheetId, $body_insert_row_FO);
		}

		$rows_check = $sheets
			->spreadsheets_values
			->get($this->sheetid, $this->f_o, ['majorDimension' => 'ROWS']);

		//XXX: mencari di free odp
		unset($rows_check->values[0]);
		$rows_check->values = array_values($rows_check->values);

		foreach ($rows_check->values as $data)
		{
			$odp_list[] = $data[0];
		}

		$no = 1;
		foreach ($arr_odp as $odp)
		{
			$search_raw = trim($odp);
			$result_list = array_filter($odp_list, function ($item) use ($search_raw)
			{
				if (stripos($item, $search_raw) !== false)
				{
					return true;
				}
				return false;
			});

			if ($result_list)
			{
				foreach ($result_list as $k => $d)
				{
					$get_num[$k] = $d;
				}

				foreach ($get_num as $key => $val)
				{
					$get_data = $rows_check->values[$key];
					$get_data[3] = 'BOOKING';
					$key = $key + 2;
					$data_free_odp[] = new \Google_Service_Sheets_ValueRange(['range' => 'Sheet3!A' . $key, 'values' => [$get_data]]);

					$body = new \Google_Service_Sheets_BatchUpdateValuesRequest(['valueInputOption' => 'RAW', 'data' => $data_free_odp]);

					$sheets
						->spreadsheets_values
						->batchUpdate($spreadsheetId, $body);
				}
			}

			$no_ex = end($rows->values);
			$exp = explode('/', $odp);
			$exp_odp = explode('-', $exp[0]);
			//XXX: insert latest data
			$row_skrg = count($rows->values) + $no;
			$data_booking[] = new \Google_Service_Sheets_ValueRange(['range' => 'Sheet1!A' . $row_skrg, 'values' => [[$no_ex[0] + $no, $req->mitra, $req->site_id, $req->site_nm, $odp, \Google_Model::NULL_VALUE, $req->odp_koor, '=VLOOKUP(E' . $row_skrg . ',\'REPORT ODP KALSEL\'!C:P,14,false)', $req->req, \Google_Model::NULL_VALUE, $req->ket, date('d/m/Y') , \Google_Model::NULL_VALUE, \Google_Model::NULL_VALUE, \Google_Model::NULL_VALUE, \Google_Model::NULL_VALUE, \Google_Model::NULL_VALUE, Session::get('auth')->nama, \Google_Model::NULL_VALUE, $exp_odp[1], $exp_odp[2], ]]]);
			++$no;

			$body = new \Google_Service_Sheets_BatchUpdateValuesRequest(['valueInputOption' => 'USER_ENTERED', 'data' => $data_booking]);
			$sheets
				->spreadsheets_values
				->batchUpdate($spreadsheetId, $body);
		}
		return redirect('/show/booking_odp')->with('alerts', [['type' => 'success', 'text' => implode(' dan ', $arr_odp) . " Berhasil Ditambah!"]]);
	}

	public function check_odp_booking(Request $req)
	{
		$client = new \Google_Client();
		$client->setApplicationName('My PHP App');
		$client->setScopes([\Google_Service_Sheets::SPREADSHEETS]);
		$client->setAccessType('offline');
		$client->setAuthConfig('/srv/htdocs/d_liauw/pt2_dev/public/google_pt2.json');
		$spreadsheetId = $this->sheetid;

		$sheets = new \Google_Service_Sheets($client);
		$sheet = $sheets
			->spreadsheets
			->get($this->sheetid)
			->getSheets();

		foreach ($sheet as $sheetl)
		{
			$list[] = $sheetl
				->properties->title;
		}

		$rows = $sheets
			->spreadsheets_values
			->get($this->sheetid, $this->b_o, ['majorDimension' => 'ROWS']);

		unset($rows->values[0]);

		$rows->values = array_values($rows->values);
		foreach ($rows->values as $data)
		{
			if (isset($data[4]))
			{
				$odp_list[] = $data[4];
			}
		}
		foreach ($req->odp_nama as $d)
		{
			$arr_search_raw[] = trim($d);
		}

		$result = [];
		$result['free_odp_msg'] = null;
		$result['mydata'] = null;
		$result_search = [];

		foreach ($arr_search_raw as $search_raw)
		{
			$search_raw = trim($search_raw);
			$split = explode('/', $search_raw);
			$search_raw_split = $split[0];

			//mengumpulkan semua list2 alpro yg diinput
			$result_list[$search_raw_split] = array_filter($odp_list, function ($item) use ($search_raw_split)
			{
				if (stripos($item, $search_raw_split) !== false)
				{
					return true;
				}
				return false;
			});

			//mencari sesuai inputan
			foreach ($odp_list as $ol)
			{
				if (trim($ol) == $search_raw)
				{
					$result_search[$search_raw_split][] = $ol;
				}
			}

			$rows_check = $sheets
				->spreadsheets_values
				->get($this->sheetid, $this->f_o, ['majorDimension' => 'ROWS']);

			//XXX: mencari di free odp
			unset($rows_check->values[0]);
			$rows_check->values = array_values($rows_check->values);

			foreach ($rows_check->values as $keys => $data)
			{
				$odp_list_free[$keys] = $data[0];
			}
			$result['odp'][] = $search_raw;
		}

		$collect_num = [];

		foreach ($result_list as $p_key => $d)
		{
			foreach ($d as $ckey => $cd)
			{
				$recon = explode('/', $cd);
				$collect_num[$recon[0]][intVal($recon[1]) ] = intVal($recon[1]);
			}
		}

		if ($collect_num)
		{
			foreach ($collect_num as $key => & $value)
			{
				sort($value);
				ksort($value);
			}

			function fix_keys($array)
			{
				$numberCheck = false;
				foreach ($array as $k => $val)
				{
					if (is_array($val))
					{
						$array[$k] = fix_keys($val);
					} //recurse
					if (is_numeric($k))
					{
						$numberCheck = true;
					}
				}
				if ($numberCheck === true)
				{
					return array_values($array);
				}
				else
				{
					return $array;
				}
			}
			fix_keys($collect_num);
		}

		function missing_number($num_list)
		{
			// $new_arr = range($num_list[0],max($num_list));
			$new_arr = range(min($num_list) , max($num_list));
			return array_diff($new_arr, $num_list);
		}

		if (array_filter($result_search))
		{
			foreach ($arr_search_raw as $mkey => $data)
			{
				$get_alpro = explode('/', $data);
				foreach ($result_list as $keyp => $datap)
				{
					if ($keyp == $get_alpro[0])
					{
						$number_list = $collect_num[$keyp];
						if (in_array($get_alpro[1], $number_list))
						{
							//XXX:jika angka nya ada
							if (!in_array(1, $number_list))
							{
								$suggest_odp[$mkey] = $get_alpro[0] . '/' . str_pad(1, 3, '0', STR_PAD_LEFT);
								$collect_num[$keyp][] = 1;
								foreach ($collect_num as $key => & $value)
								{
									sort($value);
									ksort($value);
								}
								fix_keys($collect_num);
							}
							else
							{
								$get_missing = missing_number($number_list);
								if ($get_missing)
								{
									$gm = current($get_missing);
								}
								else
								{
									$gm = end($number_list) + 1;
								}
								$suggest_odp[$mkey] = $get_alpro[0] . '/' . str_pad($gm, 3, '0', STR_PAD_LEFT);
								$collect_num[$keyp][] = $gm;
								foreach ($collect_num as $key => & $value)
								{
									sort($value);
									ksort($value);
								}
								fix_keys($collect_num);
							}
						}
						else
						{
							$suggest_odp[$mkey] = $data;
							$collect_num[$keyp][] = intVal($get_alpro[1]);
							foreach ($collect_num as $key => & $value)
							{
								sort($value);
								ksort($value);
							}
							fix_keys($collect_num);

						}
					}
				}
			}

			$msg['pesan']['alert'] = ['type' => 'danger', 'text' => '<b><u>' . implode('<br/>', $arr_search_raw) . "</u></b><br/> Sudah Ada!"];
			foreach ($suggest_odp as $key => $suop)
			{
				$result_list_free = array_filter($odp_list_free, function ($item) use ($suop)
				{
					if (stripos($item, $suop) !== false)
					{
						return true;
					}
					return false;
				});

				$msg[$key]['msg'] = ['type' => 'success', 'text' => "Direkomendasikan Memakai <u>$suop</u>"];

				if ($result_list_free)
				{
					$msg[$key]['free'] = ['type' => 'info', 'text' => "<u>$suop</u> Ada Di Free ODP!"];
				}
			}
			return back()->with('alerts', $msg)->with('value_suggest', $suggest_odp);
		}
		else
		{
			$rows_check_m = $sheets
				->spreadsheets_values
				->get($this->sheetid, 'V Data', ['majorDimension' => 'ROWS']);

			$unset_array = range(0, 29);
			foreach ($unset_array as $arrayun)
			{
				unset($rows_check_m->values[$arrayun]);
			}

			$mitra = $rows_check_m->values;

			foreach ($arr_search_raw as $key => $search_raw)
			{
				$result_list_free = array_filter($odp_list_free, function ($item) use ($search_raw)
				{
					if (stripos($item, $search_raw) !== false)
					{
						return true;
					}
					return false;
				});

				if ($result_list_free)
				{
					$result['free_odp_msg'][$key] = "$search_raw Ada Di Free ODP";
					$get_dt_free['ket'][$key] = $rows_check->values[key($result_list_free) ][4];
					$get_dt_free['mitra'][$key] = $rows_check->values[key($result_list_free) ][8];
					$result['mydata'][$key] = $get_dt_free;
				}
			}

			return view('BookingOdp.booking_odp_create', compact('result', 'mitra'));
		}

	}

	public function delete_booking_odp($id)
	{
		$client = new \Google_Client();
		$client->setApplicationName('My PHP App');
		$client->setScopes([\Google_Service_Sheets::SPREADSHEETS]);
		$client->setAccessType('offline');
		$client->setAuthConfig('/srv/htdocs/d_liauw/pt2_dev/public/google_pt2.json');
		$spreadsheetId = $this->sheetid;

		$sheets = new \Google_Service_Sheets($client);
		$sheet = $sheets
			->spreadsheets
			->get($this->sheetid)
			->getSheets();

		foreach ($sheet as $sheetl)
		{
			$list[] = $sheetl
				->properties->title;
		}

		$rows = $sheets
			->spreadsheets_values
			->get($this->sheetid, $this->b_o, ['majorDimension' => 'ROWS']);

		$rows_free = $sheets
			->spreadsheets_values
			->get($this->sheetid, $this->f_o, ['majorDimension' => 'ROWS']);

		$prev_data = $rows->values[$id];
		$exp = explode('/', $prev_data[4]);
		$exp_odp = explode('-', $exp[0]);

		$row_skrg = count($rows_free->values) + 1;
		$data[] = new \Google_Service_Sheets_ValueRange(['range' => 'Sheet3!A' . $row_skrg, 'values' => [[$prev_data[4], $exp_odp[1], $exp_odp[2], "FREE", $prev_data[10], date('d/m/Y') , "NAMA", \Google_Model::NULL_VALUE, $prev_data[1]]]]);
		$body = new \Google_Service_Sheets_BatchUpdateValuesRequest(['valueInputOption' => 'RAW', 'data' => $data]);
		$sheets
			->spreadsheets_values
			->batchUpdate($spreadsheetId, $body);

		//XXX:delete data
		$body_del = new \Google_Service_Sheets_BatchUpdateSpreadsheetRequest(['requests' => ['deleteDimension' => ['range' => ['sheetId' => $this->b_o_id, // the ID of the sheet/tab shown after 'gid=' in the URL
		'dimension' => "Rows", "startIndex" => $id, // baris pertama (tidak terhapus)
		"endIndex" => $id + 1 //baris 3 & 4 terhapus
		]]]]);

		$sheets
			->spreadsheets
			->batchUpdate($spreadsheetId, $body_del);
	}

	public function deepS_odp()
	{
		return view('BookingOdp.deepsearch', ['data' => [], 'split' => []]);
	}

	public function find_deepS_odp(Request $req)
	{
		$client = new \Google_Client();
		$client->setApplicationName('My PHP App');
		$client->setScopes([\Google_Service_Sheets::SPREADSHEETS]);
		$client->setAccessType('offline');
		$client->setAuthConfig('/srv/htdocs/d_liauw/pt2_dev/public/google_pt2.json');
		$spreadsheetId = $this->sheetid;

		$sheets = new \Google_Service_Sheets($client);
		$sheet = $sheets
			->spreadsheets
			->get($this->sheetid)
			->getSheets();

		foreach ($sheet as $sheetl)
		{
			$list[] = $sheetl
				->properties->title;
		}

		$srch = trim($req->search);

		$rows_booking = $sheets
			->spreadsheets_values
			->get($this->sheetid, $this->b_o, ['majorDimension' => 'ROWS']);

		unset($rows_booking->values[0]);

		$odpB_list = $rows_booking->values;

		$rows_free = $sheets
			->spreadsheets_values
			->get($this->sheetid, $this->f_o, ['majorDimension' => 'ROWS']);

		unset($rows_free->values[0]);

		$odpF_list = $rows_free->values;
		$data = [];
		$data = [];
		if ($req->type == 'odp')
		{
			foreach ($odpB_list as $ol)
			{
				if (isset($ol[4]))
				{
					if (trim($ol[4]) == $srch)
					{
						$result_listB[] = $ol;
					}
				}
			}

			foreach ($odpF_list as $ol)
			{
				if (trim($ol[0]) == $srch)
				{
					$result_listF[] = $ol;
				}
			}
		}
		else
		{
			$result_listB = array_filter($odpB_list, function ($item) use ($srch)
			{
				if (stripos($item[4], $srch) !== false)
				{
					return true;
				}
				return false;
			});

			$result_listF = array_filter($odpF_list, function ($item) use ($srch)
			{
				if (stripos($item[0], $srch) !== false)
				{
					return true;
				}
				return false;
			});
		}

		$data = ['Hasil Result Booking ODP Sebanyak <u><b>' . count($result_listB) . '</b></u> Buah', 'Hasil Result Free ODP Sebanyak <u><b>' . count($result_listF) . '</b></u> Buah'];

		$split = [$result_listB, $result_listF];
		return view('BookingOdp.deepsearch', ['data' => $data, 'split' => $split]);

	}
}