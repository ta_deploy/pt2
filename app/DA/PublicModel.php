<?php
namespace App\DA;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Telegram;
use ZipArchive;
use RecursiveIteratorIterator;
use RecursiveDirectoryIterator;
date_default_timezone_set("Asia/Makassar");
class PublicModel
{
	public static function save_saran($req)
	{
		$updated_by = Session::get('auth')->id_user;
		DB::table('saran_glob')
			->insert(['pesan' => $req->saran, 'modul' => 2, 'created_by' => $updated_by]);
	}

	public static function get_list()
	{
		return DB::table('saran_glob')->orderBy('created_at', 'DESC')
			->get();
	}

	public static function save_change($id)
	{
		return DB::table('saran_glob')->where('id', $id)->update(['status' => 1]);
	}
}