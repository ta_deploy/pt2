<?php
namespace App\DA;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use App\DA\ReportModel;

date_default_timezone_set('Asia/Makassar');

class HeroModel
{
	public static function data_HERO($date, $req = null)
	{
		$pt2 = DB::table('pt2_master As pm')
		->select('pm.*')
		->Where([
			['id_hero', '!=', 0],
			['delete_clm', 0]
		])
		->Where(function($join){
			$join->Where('lt_status' , '!=', 'Selesai')
			->OrWhereNull('lt_status');
		})
		->get();

		$pt2 = array_map(function($item) {
			return (array)$item;
		}, $pt2->toArray() );

		if($req && $req->all() )
		{
			if($req->rangedate)
			{
				$post = [
					'jenis' => 'range',
					'start' => $req->tgl_a,
					'end'   => $req->tgl_f,
				];
			}
			else
			{
				$post = [
					'jenis' => 'single',
					'start' => $date,
				];
			}

		}
		else
		{
			$post = [
				'jenis' => 'single',
				'start' => $date,
			];
		}

		$curl = curl_init();

		curl_setopt_array($curl, [
			CURLOPT_URL => 'https://promitos.tomman.app/API_HERO_DATA',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_POSTFIELDS => $post,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
		]);

		$response = curl_exec($curl);
		curl_close($curl);

		$data = (array)json_decode($response);
		$data = array_values($data);

		foreach($data as $k => $v)
		{
			$find_k = array_search($v->id, array_column($pt2, 'id_hero') );

			$data[$k]->project_name = $v->nama_jalan;
			$data[$k]->odp_koor = $v->koordinat;
			$data[$k]->sto = $v->sto;
			$data[$k]->status_wo_pt2 = 'kosong';

			if($find_k !== FALSE)
			{
				$data_pt2 = $pt2[$find_k];

				$data[$k]->id         = $data_pt2['id'];
				$data[$k]->tgl_buat   = $data_pt2['tgl_buat'];
				$data[$k]->catatan_HD = $data_pt2['catatan_HD'];
				$data[$k]->regu_name  = $data_pt2['regu_name'];
				$data[$k]->lt_status  = $data_pt2['lt_status'];
				$data[$k]->status_wo_pt2 = 'ADA';

			}
			else
			{
				$data[$k]->tgl_buat   = $v->modified_at;
				$data[$k]->catatan_HD = '-';
				$data[$k]->regu_name  = null;
				$data[$k]->lt_status  = null;
			}
		}

		if(@$req->datel && !$req->sto)
		{
			$get_all_sto = ReportModel::get_all_datel();
			foreach($get_all_sto as $v)
			{
				if($v->datel == $req->datel)
				{
					$sto[] = $v->sto;
				}
			}
		}
		else
		{
			$sto = @$req->sto;
		}

		$data = array_map(function($o){
			$a = get_object_vars($o);
			return $a;
		}, $data);

		if(@$req->flag)
		{
			$data = array_filter($data, function($x) use($req){
				return in_array($x['flagging'], $req->flag);
			});
		}

		if($sto)
		{
			$data = array_filter($data, function($x) use($sto){
				return in_array($x['sto'], $sto);
			});
		}

		foreach($data as $k => $v)
		{
			$data[$k] = (object)$v;
		}

		return $data;
	}

	public static function update_flagging($req)
	{
		DB::transaction(function () use ($req){
			DB::Table('pt2_hero')->where('id', $req->id_hero)->update([
				'flagging' => $req->isi
			]);

			if(in_array($req->isi, ['non_unsc', 'pt2_plus']) )
			{
				$find_k = DB::table('pt2_status')->where('id_status', $req->isi)->first();

				DB::Table('pt2_master')->where('id_hero', $req->id_hero)->update([
					'kategory_non_unsc' => $find_k->id,
					'lt_status' => null
				]);
			}
			else
			{
				DB::Table('pt2_master')->where('id_hero', $req->id_hero)->update([
					'lt_status' => 'Kendala'
				]);
			}
		});
	}

	public static function progress_dash_hero($date)
	{

		$curl = curl_init();

		$pt2 = DB::table('pt2_master As pm')
		->select('pm.*')
		->Where('id_hero', '!=', 0)
		->WhereNotNull('lt_status')
		->get();

		$pt2 = array_map(function($item) {
			return (array)$item;
		}, $pt2->toArray() );

		$post = [
			'jenis' => 'single',
			'start' => $date,
		];

		curl_setopt_array($curl, [
			CURLOPT_URL            => 'https://promitos.tomman.app/API_HERO_DATA',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_POSTFIELDS     => $post,
			CURLOPT_ENCODING       => '',
			CURLOPT_MAXREDIRS      => 10,
			CURLOPT_TIMEOUT        => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST  => 'POST',
		]);

		$response = curl_exec($curl);
		curl_close($curl);

		$data = (array)json_decode($response);
		$data = array_values($data);

		foreach($data as $k => $v)
		{
			$find_k = array_search($v->id, array_column($pt2, 'id_hero') );

			$data[$k]->project_name = $v->nama_jalan;
			$data[$k]->odp_koor = $v->koordinat;
			$data[$k]->sto = $v->sto;

			if($find_k !== FALSE)
			{
				$data_pt2 = $pt2[$find_k];
				$data[$k]->id         = $data_pt2['id'];
				$data[$k]->tgl_buat   = $data_pt2['tgl_buat'];
				$data[$k]->catatan_HD = $data_pt2['catatan_HD'];
				$data[$k]->regu_id    = $data_pt2['regu_id'];
				$data[$k]->lt_status  = $data_pt2['lt_status'];
				$data[$k]->regu_name  = $data_pt2['regu_name'];
				// dd($data[$k]);
				continue;
			}
			else
			{
				$data[$k]->tgl_buat   = '-';
				$data[$k]->catatan_HD = '-';
				$data[$k]->regu_name  = '-';
				$data[$k]->regu_id    = '-';
				$data[$k]->lt_status  = '-';
				continue;
			}
		}

		return $data;
	}

}