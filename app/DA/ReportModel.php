<?php
namespace App\DA;

use App\Http\Controllers\ExcelExport;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Telegram;

date_default_timezone_set("Asia/Makassar");
class ReportModel
{
	// public static function getMaterialOutstanding($month1, $month2){
	//   Return DB::select("SELECT *,
	//     ( SELECT count(*) FROM alista_material_keluar WHERE proaktif_id = pt2_master.proaktif_id AND delete_clm = 0 AND id_item = pt2_material.id_item ) as bon,
	//     ( select count(*) from pt2_material pm left join pt2_master pm2 on pm.pt2_id = pm2.id where delete_clm = 0 AND pm2.proaktif_id = proaktif_id and id_item = pt2_material.id_item ) as terpakai
	//     from pt2_material left join pt2_master on pt2_material.pt2_id = pt2_master.id LEFT JOIN alista_material_keluar ON alista_material_keluar.proaktif_id = pt2_master.proaktif_id WHERE delete_clm = 0  AND pt2_master.tgl_selesai IS NOT NULL AND tgl_selesai Between '$month1' AND '$month2' group by pt2_master.proaktif_id");
	// }
	public $nama_status = "CASE WHEN kategory_non_unsc = 0 THEN 'UNSC'
	WHEN kategory_non_unsc = 1 THEN 'NON UNSC'
	WHEN kategory_non_unsc = 2 THEN 'IN'
	WHEN kategory_non_unsc = 3 THEN 'PT2-PLUS'
	WHEN kategory_non_unsc = 4 THEN 'UNSC MANUAL'
	WHEN kategory_non_unsc = 5 THEN 'PT2 SUB'
	WHEN kategory_non_unsc = 6 THEN 'PT2 KPRO'
	WHEN kategory_non_unsc = 7 THEN 'PT2 NORMALISASI'
	WHEN kategory_non_unsc = 8 THEN 'PT2 HEM'
	ELSE 'Tidak Terdeteksi'
	END";

	public static function getRekap_year($year, $month)
	{
		$date = $year . '-' . $month;

		$data = "datel, maintenance_datel.sto as sto_n,
    (CASE WHEN tgl_selesai  IS NOT NULL THEN tgl_selesai ELSE tgl_pengerjaan END) as tgl_mutlak,
    SUM(CASE WHEN bekas_sdi = 1 AND pt2_master.regu_id IS NULL THEN 1 ELSE 0 END)  AS WO_orderan_siap_dispatch,
    SUM(CASE WHEN lt_status IS NULL AND pt2_master.regu_id IS NOT NULL AND ACTIVE = 1 THEN 1 ELSE 0 END ) as sudah_dispatch,
    SUM(CASE WHEN lt_status = 'Ogp' THEN 1 ELSE 0 END ) as ogp,
    SUM(CASE WHEN lt_status = 'Pending' THEN 1 ELSE 0 END ) as pending,
    SUM(CASE WHEN lt_status = 'Kendala' THEN 1 ELSE 0 END ) as kendala,
    SUM(CASE WHEN lt_status IS NULL AND pt2_master.regu_id IS NOT NULL AND ACTIVE = 1 THEN 1 ELSE 0 END ) as need_progress,
    SUM(CASE WHEN lt_status = 'Selesai' AND GOLIVE = 0 THEN 1 ELSE 0 END ) as selesai_fisik,
    SUM(CASE WHEN lt_status = 'Selesai' AND GOLIVE = 1 THEN 1 ELSE 0 END ) as selesai_live,
    SUM(CASE WHEN lt_status = 'Selesai' THEN 1 ELSE 0 END ) as finish,
    SUM(CASE WHEN lt_status IS NULL OR lt_status != 'Selesai' THEN 1 ELSE 0 END ) as sisa_order_belumselesai";

		if ($month != 0)
		{
			return DB::Select("SELECT $data FROM pt2_master
      LEFT JOIN maintenance_datel ON pt2_master.sto = maintenance_datel.sto
      LEFT JOIN regu r ON r.id_regu = pt2_master.regu_id
			WHERE delete_clm = 0 AND DATE_FORMAT( (CASE WHEN tgl_selesai  IS NOT NULL THEN tgl_selesai ELSE tgl_pengerjaan END), '%Y-%m') = '$date'
      GROUP BY DATE_FORMAT((CASE WHEN tgl_selesai  IS NOT NULL THEN tgl_selesai ELSE tgl_pengerjaan END), '%Y-%m-%d'), maintenance_datel.sto
			ORDER BY (CASE WHEN tgl_selesai  IS NOT NULL THEN tgl_selesai ELSE tgl_pengerjaan END) DESC");
		}
		else
		{
			return DB::Select("SELECT $data FROM pt2_master
      LEFT JOIN maintenance_datel ON pt2_master.sto = maintenance_datel.sto
      LEFT JOIN regu r ON r.id_regu = pt2_master.regu_id
			WHERE delete_clm = 0 AND YEAR(CASE WHEN tgl_selesai  IS NOT NULL THEN tgl_selesai ELSE tgl_pengerjaan END) = $year
      GROUP BY DATE_FORMAT((CASE WHEN tgl_selesai  IS NOT NULL THEN tgl_selesai ELSE tgl_pengerjaan END), '%Y-%m-%d'), maintenance_datel.sto
			ORDER BY CASE WHEN tgl_selesai  IS NOT NULL THEN tgl_selesai ELSE tgl_pengerjaan END DESC");
		}

	}

	public static function getRekap_psb_all($year)
	{
		$data = "datel, maintenance_datel.sto as sto_n,
    (CASE WHEN tgl_selesai  IS NOT NULL THEN tgl_selesai ELSE tgl_pengerjaan END) as tgl_mutlak,
    SUM(CASE WHEN bekas_sdi = 1 AND pt2_master.regu_id IS NULL THEN 1 ELSE 0 END)  AS WO_orderan_siap_dispatch,
    SUM(CASE WHEN lt_status IS NULL AND delete_clm = 0 AND scid IS NOT NULL AND pt2_master.regu_id IS NOT NULL THEN 1 ELSE 0 END ) as sudah_dispatch,
    SUM(CASE WHEN lt_status = 'Ogp' AND delete_clm = 0 AND scid is not null AND tgl_selesai LIKE '$year%' THEN 1 ELSE 0 END ) as ogp,
    SUM(CASE WHEN lt_status = 'Pending' AND delete_clm = 0 AND scid is not null AND tgl_selesai LIKE '$year%' THEN 1 ELSE 0 END ) as pending,
    SUM(CASE WHEN lt_status = 'Kendala' AND delete_clm = 0 AND scid is not null AND tgl_selesai LIKE '$year%' THEN 1 ELSE 0 END ) as kendala,
    SUM(CASE WHEN lt_status IS NULL AND pt2_master.regu_id IS NOT NULL AND delete_clm = 0 AND scid is not null AND tgl_pengerjaan LIKE '$year%' THEN 1 ELSE 0 END ) as need_progress,
    SUM(CASE WHEN lt_status = 'Selesai' AND delete_clm = 0 AND scid is not null AND GOLIVE = 0 AND tgl_selesai LIKE '$year%' THEN 1 ELSE 0 END ) as selesai_fisik,
    SUM(CASE WHEN lt_status = 'Selesai' AND delete_clm = 0 AND scid is not null AND tgl_selesai LIKE '$year%' AND GOLIVE = 1 THEN 1 ELSE 0 END ) as selesai_live,
    SUM(CASE WHEN lt_status IS NULL AND scid IS NOT NULL AND delete_clm = 0 and tgl_pengerjaan LIKE '$year%' OR lt_status NOT IN ('Kendala', 'Selesai') AND delete_clm = 0 AND scid is not null AND tgl_selesai LIKE '$year%' THEN 1 ELSE 0 END ) as sisa_order_belumselesai";
		return DB::Select("SELECT $data FROM maintenance_datel LEFT JOIN pt2_master ON maintenance_datel.sto = pt2_master.sto WHERE regional = 6 GROUP BY maintenance_datel.datel ORDER BY pt2_master.sto DESC");
	}

	public static function getRekapPsb_daily($year)
	{
		return DB::SELECT("SELECT COUNT(dt.id) as hitung,
			(CASE WHEN pl.odp_plan IS NOT NULL AND LENGTH(pl.odp_plan) > 6 THEN SUBSTR(pl.odp_plan, 5, 3)
			WHEN my.sto IS NOT NULL THEN my.sto
			WHEN my2.sto IS NOT NULL THEN my2.sto ELSE f.area END) AS sto_B
			FROM
			dispatch_teknisi dt
			LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
			LEFT JOIN Data_Pelanggan_Starclick e ON dt.Ndem = e.orderId
			LEFT JOIN mdf f ON e.sto = f.mdf
			LEFT JOIN pt2_master pts ON pts.scid = pl.id_tbl_mj
			LEFT JOIN psb_myir_wo my ON my.myir = dt.Ndem
			LEFT JOIN psb_myir_wo my2 ON my2.sc = dt.Ndem
			WHERE pl.status_laporan = 75 AND (dt.dispatch_by= '5' OR dt.dispatch_by IS NULL) AND pts.scid IS NULL AND DATE(pl.modified_at) LIKE '$year%' GROUP BY sto_B");
	}

	public static function getRekap_front($year)
	{

		return DB::Select("SELECT datel, sdi_datel.sto as sto_n,
		(CASE WHEN tgl_selesai  IS NOT NULL THEN tgl_selesai ELSE tgl_pengerjaan END) as tgl_mutlak,
		0 as WO_orderan_siap_dispatch,
		pl.status_laporan,
		ACTIVE,
		SUM(CASE WHEN lt_status IS NULL AND ACTIVE = 1 AND pt2_master.regu_id IS NOT NULL THEN 1 ELSE 0 END ) as sudah_dispatch,
		SUM(CASE WHEN lt_status = 'Ogp' THEN 1 ELSE 0 END ) as ogp,
		SUM(CASE WHEN lt_status = 'Kendala' THEN 1 ELSE 0 END ) as kendala,
		SUM(CASE WHEN lt_status = 'Pending' THEN 1 ELSE 0 END ) as pending,
		SUM(CASE WHEN lt_status = 'Selesai' THEN 1 ELSE 0 END ) as finish,
		SUM(CASE WHEN (lt_status IS NULL OR lt_status != 'Selesai') AND ACTIVE = 1 THEN 1 ELSE 0 END ) as sisa_order_belumselesai
		FROM sdi_datel
		LEFT JOIN pt2_master on sdi_datel.sto = pt2_master.sto
		LEFT JOIN psb_laporan pl ON pl.id_tbl_mj = pt2_master.scid
		LEFT JOIN regu r ON r.id_regu = pt2_master.regu_id
		WHERE delete_clm = 0 AND sdi_datel.datel != '0DATEL' AND regional = 6 AND (CASE
		WHEN pt2_master.tgl_selesai IS NOT NULL THEN YEAR(pt2_master.tgl_selesai) = '$year'
		WHEN pt2_master.tgl_pengerjaan IS NOT NULL THEN YEAR(pt2_master.tgl_pengerjaan) = '$year'
		WHEN pt2_master.tgl_buat IS NOT NULL THEN YEAR(pt2_master.tgl_buat) = '$year'
		ELSE 1 = 1
		END)
		GROUP BY DATE_FORMAT( (CASE
		WHEN lt_status = 'Selesai' THEN tgl_selesai
		WHEN tgl_selesai IS NULL THEN tgl_pengerjaan
		ELSE tgl_buat END) , '%Y-%m'), sdi_datel.sto
		ORDER BY MONTH( (CASE
		WHEN lt_status = 'Selesai' THEN tgl_selesai
		WHEN tgl_selesai IS NULL THEN tgl_pengerjaan
		ELSE tgl_buat END) ) DESC");
		//AND CONCAT(WO + ogp_survey + finish + saldo) != 0
	}

	public static function sdi_datel()
	{
		return DB::table('sdi_datel')->where([['datel', '!=', '0DATEL'], ['regional', 6]])
			->get();
	}

	public static function getRekap_year_detail($year, $month, $add = null)
	{
		$date = $year . '-' . $month;
		$that = new ReportModel();
		$jns = $that->nama_status;
		$data = "project_name, odp_nama, tgl_selesai, pt2_master.sto, pt2_master.tgl_buat, pt2_master.regu_id, nomor_sc, regu.id_regu, TL, regu_name, lt_status, lt_catatan, pt2_master.scid, lt_koordinat_odp, tgl_pengerjaan, pt2_master.id, pt2_master.GOLIVE, maintenance_datel.datel, kendala_detail, pls.laporan_status,
    CONCAT($jns) as jenis, pt2_master.jenis_wo, my.myir, my2.sc,
    (CASE
		WHEN pt2_master.tgl_selesai IS NOT NULL THEN tgl_selesai
		WHEN pt2_master.tgl_pengerjaan IS NOT NULL THEN tgl_pengerjaan
		ELSE pt2_master.tgl_buat
		END) as tgl_mutlak";

		if ($month != 0)
		{
			$sql = "DATE_FORMAT( (CASE
			WHEN lt_status = 'Selesai' THEN tgl_selesai
			WHEN tgl_selesai IS NULL THEN tgl_pengerjaan
			ELSE tgl_buat END), '%Y-%m') = '$date'";

			if($add)
			{
				$sql = "DATE_FORMAT( (CASE
				WHEN lt_status = 'Selesai' THEN tgl_selesai
				WHEN tgl_selesai IS NULL THEN tgl_pengerjaan
				ELSE tgl_buat END), '%Y-%m-%d') >= '$add[0]' && DATE_FORMAT( (CASE
				WHEN lt_status = 'Selesai' THEN tgl_selesai
				WHEN tgl_selesai IS NULL THEN tgl_pengerjaan
				ELSE tgl_buat END), '%Y-%m-%d') <= '$add[1]'";
			}

			return DB::Select("SELECT $data FROM pt2_master
				LEFT JOIN maintenance_datel ON pt2_master.sto = maintenance_datel.sto
				LEFT JOIN regu ON pt2_master.regu_id = regu.id_regu
				LEFT JOIN dispatch_teknisi dt on dt.id = pt2_master.scid
				LEFT JOIN psb_laporan pl ON pl.id_tbl_mj = dt.id
				LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
				LEFT JOIN psb_myir_wo my ON dt.Ndem = my.myir
				LEFT JOIN psb_myir_wo my2 ON my2.sc = dt.Ndem
				WHERE (ACTIVE = 1 AND lt_status IS NULL OR lt_status IS NOT NULL OR status = 'Completed') AND delete_clm = 0 AND (dt.dispatch_by= '5' OR dt.dispatch_by IS NULL) HAVING $sql ORDER BY (CASE
				WHEN lt_status = 'Selesai' THEN tgl_selesai
				WHEN tgl_selesai IS NULL THEN tgl_pengerjaan
				ELSE tgl_buat END) DESC ");
		}
		else
		{
			return DB::Select("SELECT $data FROM pt2_master
				LEFT JOIN maintenance_datel ON pt2_master.sto = maintenance_datel.sto
				LEFT JOIN regu ON pt2_master.regu_id = regu.id_regu
				LEFT JOIN dispatch_teknisi dt on dt.id = pt2_master.scid
				LEFT JOIN psb_laporan pl ON pl.id_tbl_mj = dt.id
				LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
				LEFT JOIN psb_myir_wo my ON dt.Ndem = my.myir
				LEFT JOIN psb_myir_wo my2 ON my2.sc = dt.Ndem
				WHERE (ACTIVE = 1 AND lt_status IS NULL OR lt_status IS NOT NULL OR status = 'Completed') AND delete_clm = 0 AND (dt.dispatch_by= '5' OR dt.dispatch_by IS NULL) AND (CASE
				WHEN pt2_master.tgl_selesai IS NOT NULL THEN YEAR(pt2_master.tgl_selesai) = '$year'
				WHEN pt2_master.tgl_pengerjaan IS NOT NULL THEN YEAR(pt2_master.tgl_pengerjaan) = '$year'
				WHEN pt2_master.tgl_buat IS NOT NULL THEN YEAR(pt2_master.tgl_buat) = '$year'
				ELSE 1 = 1
				END) ORDER BY (CASE
				WHEN lt_status = 'Selesai' THEN tgl_selesai
				WHEN tgl_selesai IS NULL THEN tgl_pengerjaan
				ELSE tgl_buat END) DESC");
		}

	}

	public static function getMatrix_search($month1, $month2, $value, $jenis, $stts_regu, $mitra = null)
	{
		if ($value == 'empty')
		{
			$value = "'need_progres','Ogp','Selesai','Kendala','Pending','Berangkat','>7','Tidak Sempat'";
		}

		if ($stts_regu == 'all')
		{
			$stts_regu = "0, 1";
		}

		$array = explode(',', $value);
		$common_stts = array_diff($array, ["'need_progres'", "'>7'", "'Berangkat'"]);
		// dd($array, $common_stts, $value);
		$glue_status = [];

		// if(array_search('\'Pending\'', $common_stts) !== FALSE)
		// {
		// 	$glue_status[] =  $common_stts[array_search('\'Pending\'', $common_stts)];
		// }

		// if(array_search('\'Ogp\'', $common_stts) !== FALSE)
		// {
		// 	$glue_status[] =  $common_stts[array_search('\'Ogp\'', $common_stts)];
		// }

		$glue_status = ["'Pending'", "'Ogp'", "'On Progress'"];

		$that = new ReportModel();
		$jns = $that->nama_status;
		$data0 = [];
		$sql = $sql_join = '';

		if(session('auth')->pt2_level == 3)
		{
			$sql .= 'AND r.mitra = "'. session('auth')->mitra . '"';
		}

		if(session('auth')->pt2_level == 8 )
		{
			$sql .= 'AND emp.atasan_1 = "'. session('auth')->mitra . '"';
			$sql_join .= ' LEFT JOIN 1_2_employee emp ON emp.nik = pt2_master.nik1 ';
		}

		if($mitra)
		{
			if(strcasecmp($mitra, 'All') != 0)
			{
				$sql .= ' AND r.mitra = "'.$mitra.'"';
			}
		}

		if (count($common_stts) != 0)
		{
			//INI ALL STATUS
			$stts    = implode(',', $common_stts);
			$stts_gl = implode(',', $glue_status);
			$m1      = date('Y-m', strtotime($month1) );
			$m2      = date('Y-m', strtotime($month2) );

			$data0 = DB::select("SELECT pt2_master.*, pstat.status As status_pst, datel, pl.id_tbl_mj as renew_id, md.kota, r.uraian, r.label_sektor, ma.mitra_amija_pt as nama_mitra, r.mitra,
      CONCAT($jns) as jenis,
        CONCAT(
        FLOOR(
        SUM(
        UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(tgl_pengerjaan) - 86400 * (
        (FLOOR(DATEDIFF(NOW(), tgl_pengerjaan) / 7) * 2) +
        IF(WEEKDAY(NOW()) > 5, WEEKDAY(NOW()) - 5, 0) +
        IF(WEEKDAY(NOW()) < WEEKDAY(tgl_pengerjaan), 2, 0)
        )
        ) / 86400
        ), ' Hari ',
        TIME_FORMAT(
        SEC_TO_TIME(
        SUM(
        UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(tgl_pengerjaan) - 86400 * (
        (FLOOR(DATEDIFF(NOW(), tgl_pengerjaan) / 7) * 2) +
        IF(WEEKDAY(NOW()) > 5, WEEKDAY(NOW()) - 5, 0) +
        IF(WEEKDAY(NOW()) < WEEKDAY(tgl_pengerjaan), 2, 0)
        )
        ) % 86400
        ),
        '%H Jam'
        )
        ) AS durasi,
        r.ACTIVE,
         (CASE WHEN pl.status_laporan != 75 AND kategory_non_unsc = 0 AND (lt_status NOT IN('Kendala', 'Selesai') OR lt_status IS NULL) THEN CONCAT('Status: ', pls.laporan_status) ELSE CONCAT(NULL) END) as pt2_stts
         FROM pt2_master
				 $sql_join
				 LEFT JOIN pt2_status as pstat ON pstat.id = pt2_master.kategory_non_unsc
         LEFT JOIN regu r ON r.id_regu = pt2_master.regu_id
         LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija
         LEFT JOIN psb_laporan pl ON pl.id_tbl_mj = pt2_master.scid
         LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
         LEFT JOIN maintenance_datel md ON md.sto = pt2_master.sto
         WHERE delete_clm = 0 AND pt2_master.regu_id IS NOT NULL $sql AND kategory_non_unsc IN ($jenis) AND r.ACTIVE IN($stts_regu) AND (pt2_master.tgl_selesai BETWEEN '$month1' AND '$month2' AND lt_status IN($stts) OR lt_status IN($stts_gl) OR lt_status IN ('Selesai', 'Kendala') AND DATE_FORMAT(pt2_master.tgl_selesai, '%Y-%m') >= '$m1' AND DATE_FORMAT(pt2_master.tgl_selesai, '%Y-%m') <= '$m2' ) GROUP BY pt2_master.id");
		}

		$data1 = [];

		if (strpos($value, 'need_progres') !== false)
		{
			//INI NEED PROGRESS
			$data1 = DB::select("SELECT pt2_master.*, pstat.status As status_pst, datel, datel, pl.id_tbl_mj as renew_id, md.kota, r.uraian, r.label_sektor, ma.mitra_amija_pt as nama_mitra, r.mitra,
      CONCAT($jns) as jenis,
        CONCAT(
        FLOOR(
        SUM(
        UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(tgl_pengerjaan) - 86400 * (
        (FLOOR(DATEDIFF(NOW(), tgl_pengerjaan) / 7) * 2) +
        IF(WEEKDAY(NOW()) > 5, WEEKDAY(NOW()) - 5, 0) +
        IF(WEEKDAY(NOW()) < WEEKDAY(tgl_pengerjaan), 2, 0)
        )
        ) / 86400
        ), ' Hari ',
        TIME_FORMAT(
        SEC_TO_TIME(
        SUM(
        UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(tgl_pengerjaan) - 86400 * (
        (FLOOR(DATEDIFF(NOW(), tgl_pengerjaan) / 7) * 2) +
        IF(WEEKDAY(NOW()) > 5, WEEKDAY(NOW()) - 5, 0) +
        IF(WEEKDAY(NOW()) < WEEKDAY(tgl_pengerjaan), 2, 0)
        )
        ) % 86400
        ),
        '%H Jam'
        )
        ) AS durasi,
				CONCAT(
					FLOOR(
          SUM(
          UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(tgl_pengerjaan) - 86400 * (
          (FLOOR(DATEDIFF(NOW(), tgl_pengerjaan) / 7) * 2) +
          IF(WEEKDAY(NOW()) > 5, WEEKDAY(NOW()) - 5, 0) +
          IF(WEEKDAY(NOW()) < WEEKDAY(tgl_pengerjaan), 2, 0)
          )
          ) / 86400
          )
				) as jml_hari,
        r.ACTIVE,
        (CASE WHEN pl.status_laporan != 75 AND kategory_non_unsc = 0 AND (lt_status NOT IN('Kendala', 'Selesai') OR lt_status IS NULL) THEN CONCAT('Status: ', pls.laporan_status) ELSE CONCAT(NULL) END) as pt2_stts
         FROM pt2_master
				 $sql_join
				 LEFT JOIN pt2_status as pstat ON pstat.id = pt2_master.kategory_non_unsc
         LEFT JOIN regu r ON r.id_regu = pt2_master.regu_id
         LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija
         LEFT JOIN psb_laporan pl ON pl.id_tbl_mj = pt2_master.scid
         LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
         LEFT JOIN maintenance_datel md ON md.sto = pt2_master.sto
         WHERE delete_clm = 0 $sql AND kategory_non_unsc IN ($jenis) AND r.ACTIVE IN($stts_regu) AND pt2_master.regu_id IS NOT NULL AND lt_status IS NULL GROUP BY pt2_master.id HAVING jml_hari < 7 AND jml_hari >= 0");
		}

		$data2 = [];

		if (strpos($value, '>7') !== false)
		{
			//INI SALDO
			$data2 = DB::select("SELECT pt2_master.*, pstat.status As status_pst, datel, datel, pl.id_tbl_mj as renew_id, md.kota, r.uraian, r.label_sektor, ma.mitra_amija_pt as nama_mitra, r.mitra,
      CONCAT($jns) as jenis,
        CONCAT(
        FLOOR(
        SUM(
        UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(tgl_pengerjaan) - 86400 * (
        (FLOOR(DATEDIFF(NOW(), tgl_pengerjaan) / 7) * 2) +
        IF(WEEKDAY(NOW()) > 5, WEEKDAY(NOW()) - 5, 0) +
        IF(WEEKDAY(NOW()) < WEEKDAY(tgl_pengerjaan), 2, 0)
        )
        ) / 86400
        ), ' Hari ',
        TIME_FORMAT(
        SEC_TO_TIME(
        SUM(
        UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(tgl_pengerjaan) - 86400 * (
        (FLOOR(DATEDIFF(NOW(), tgl_pengerjaan) / 7) * 2) +
        IF(WEEKDAY(NOW()) > 5, WEEKDAY(NOW()) - 5, 0) +
        IF(WEEKDAY(NOW()) < WEEKDAY(tgl_pengerjaan), 2, 0)
        )
        ) % 86400
        ),
        '%H Jam'
        )
        ) AS durasi,
				CONCAT(
					FLOOR(
          SUM(
          UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(tgl_pengerjaan) - 86400 * (
          (FLOOR(DATEDIFF(NOW(), tgl_pengerjaan) / 7) * 2) +
          IF(WEEKDAY(NOW()) > 5, WEEKDAY(NOW()) - 5, 0) +
          IF(WEEKDAY(NOW()) < WEEKDAY(tgl_pengerjaan), 2, 0)
          )
          ) / 86400
          )
				) as jml_hari,
        r.ACTIVE,
        (CASE WHEN
          FLOOR(
          SUM(
          UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(tgl_pengerjaan) - 86400 * (
          (FLOOR(DATEDIFF(NOW(), tgl_pengerjaan) / 7) * 2) +
          IF(WEEKDAY(NOW()) > 5, WEEKDAY(NOW()) - 5, 0) +
          IF(WEEKDAY(NOW()) < WEEKDAY(tgl_pengerjaan), 2, 0)
          )
          ) / 86400
          ) >= 7 THEN 'saldo' ELSE lt_status END) AS lt_status,
          (CASE WHEN pl.status_laporan != 75 AND kategory_non_unsc = 0 AND (lt_status NOT IN('Kendala', 'Selesai') OR lt_status IS NULL) THEN CONCAT('Status: ', pls.laporan_status) ELSE CONCAT(NULL) END) as pt2_stts,
          r.ACTIVE
          FROM pt2_master
					$sql_join
					LEFT JOIN pt2_status as pstat ON pstat.id = pt2_master.kategory_non_unsc
          LEFT JOIN regu r ON r.id_regu = pt2_master.regu_id
         	LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija
          LEFT JOIN psb_laporan pl ON pl.id_tbl_mj = pt2_master.scid
          LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
          LEFT JOIN maintenance_datel md ON md.sto = pt2_master.sto
          WHERE delete_clm = 0 $sql AND kategory_non_unsc IN ($jenis) AND r.ACTIVE IN($stts_regu) AND pt2_master.regu_id IS NOT NULL AND lt_status IS NULL GROUP BY pt2_master.id HAVING jml_hari >= 7 ");
		}

		$data_0 = [];

		if (strpos($value, 'Berangkat') !== false)
		{
			$data_0 = DB::select("SELECT pt2_master.*, pstat.status As status_pst, datel, pl.id_tbl_mj as renew_id, md.kota, r.uraian, r.label_sektor, ma.mitra_amija_pt as nama_mitra, r.mitra,
			CONCAT($jns) as jenis,
				CONCAT(
				FLOOR(
				SUM(
				UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(tgl_pengerjaan) - 86400 * (
				(FLOOR(DATEDIFF(NOW(), tgl_pengerjaan) / 7) * 2) +
				IF(WEEKDAY(NOW()) > 5, WEEKDAY(NOW()) - 5, 0) +
				IF(WEEKDAY(NOW()) < WEEKDAY(tgl_pengerjaan), 2, 0)
				)
				) / 86400
				), ' Hari ',
				TIME_FORMAT(
				SEC_TO_TIME(
				SUM(
				UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(tgl_pengerjaan) - 86400 * (
				(FLOOR(DATEDIFF(NOW(), tgl_pengerjaan) / 7) * 2) +
				IF(WEEKDAY(NOW()) > 5, WEEKDAY(NOW()) - 5, 0) +
				IF(WEEKDAY(NOW()) < WEEKDAY(tgl_pengerjaan), 2, 0)
				)
				) % 86400
				),
				'%H Jam'
				)
				) AS durasi,
				r.ACTIVE,
					(CASE WHEN pl.status_laporan != 75 AND kategory_non_unsc = 0 AND (lt_status NOT IN('Kendala', 'Selesai') OR lt_status IS NULL) THEN CONCAT('Status: ', pls.laporan_status) ELSE CONCAT(NULL) END) as pt2_stts
					FROM pt2_master
					$sql_join
					LEFT JOIN pt2_status as pstat ON pstat.id = pt2_master.kategory_non_unsc
					LEFT JOIN regu r ON r.id_regu = pt2_master.regu_id
         	LEFT JOIN mitra_amija ma ON r.mitra = ma.mitra_amija
					LEFT JOIN psb_laporan pl ON pl.id_tbl_mj = pt2_master.scid
					LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
					LEFT JOIN maintenance_datel md ON md.sto = pt2_master.sto
					WHERE delete_clm = 0 AND pt2_master.regu_id IS NOT NULL $sql AND kategory_non_unsc IN ($jenis) AND r.ACTIVE IN($stts_regu) AND lt_status = 'Berangkat' GROUP BY pt2_master.id");
		}

		$data = array_merge($data_0, $data0, $data1, $data2);

		if(session('auth')->pt2_level == 9)
		{
			$find_sektor = DB::table('group_telegram')
			->Where('TL_NIK', session('auth')->id_user)
			->OrWhere('Nik_Atl', session('auth')->id_user)
			->OrWhere('Nik_Atl2', session('auth')->id_user)
			->get();

			$get_sektor = [];

			foreach($find_sektor as $v)
			{
				$get_sektor[$v->sektorx] = $v->sektorx;
			}

			foreach ($data as $k => &$v)
			{
				if(!in_array($v->label_sektor, $get_sektor) )
				{
					unset($data[$k]);
				}
			}
			unset($v);
		}
		return $data;
	}

	public static function getRanked($month1, $month2)
	{
		$mitra = session('auth')->mitra;

		$sql = '';

		if(Session('auth')->pt2_level == 3)
		{
			$sql ="AND mitra ='$mitra'";
		}

		return DB::select("SELECT pt2_master.regu_id, uraian, ACTIVE, TL,
        SUM(CASE WHEN lt_status ='Selesai' THEN 1 ELSE 0 END ) as selesai,
        SUM(CASE WHEN lt_status ='Kendala' THEN 1 ELSE 0 END ) as Kendala,
        SUM(CASE WHEN lt_status ='Pending' THEN 1 ELSE 0 END ) as Pending,
        SUM(CASE WHEN lt_status IS NULL THEN 1 ELSE 0 END ) as np
        FROM pt2_master
        LEFT JOIN regu r ON pt2_master.regu_id = r.id_regu
        WHERE ACTIVE = 1 $sql AND job = 'PT2'AND delete_clm = 0 AND DATE(tgl_selesai) >= '$month1' AND DATE(tgl_selesai) <= '$month2'
				GROUP BY r.id_regu HAVING selesai != 0 OR Kendala != 0 OR Pending != 0
				ORDER BY selesai DESC");
	}

	public static function col_spn($month)
	{
		return DB::select("SELECT sto,
		(SELECT COUNT(sto)) as col,
		(SELECT SUM(material_value)) as sum
		FROM pt2_master WHERE delete_clm = 0  AND regu_id IS NOT NULL AND (tgl_selesai IS NULL OR tgl_selesai LIKE '$month%') GROUP BY sto ORDER BY sto ASC");
	}

	public static function getReportMaterial($month1, $month2)
	{
		$mtr = DB::select("SELECT ptm.project_name, ptm.id, pm.id_item, ptm.rfc_key, odp_nama, ptm.regu_name, ptm.jenis_wo, ptm.sto, ptm.project_name, pm.qty
		FROM pt2_material pm
		LEFT JOIN pt2_master ptm ON ptm.id = pm.pt2_id
		WHERE delete_clm = 0  AND DATE_FORMAT(ptm.tgl_selesai, '%Y-%m-%d') BETWEEN '$month1' AND '$month2' ORDER BY pm.pt2_id asc");

		$get_detail = DB::select("SELECT pk.id_item, pk.satuan, pk.material_ta As material, pk.jasa_ta As jasa, pk.uraian
		FROM pt2_khs pk");

		$final_data = [];
		$mtr = json_decode(json_encode($mtr), TRUE);
		$get_detail = json_decode(json_encode($get_detail), TRUE);

		foreach ($mtr as $k => $v) {
			$final_data[$k] = $v;

			$find_detail = array_search($v['id_item'], array_column($get_detail, 'id_item') );

			$final_data[$k]['uraian'] = $get_detail[$find_detail]['uraian'];
			$final_data[$k]['material'] = $get_detail[$find_detail]['material'];
			$final_data[$k]['jasa'] = $get_detail[$find_detail]['jasa'];
			$final_data[$k]['satuan'] = $get_detail[$find_detail]['satuan'];
		}

		return json_decode(json_encode($final_data), FALSE);
	}

	public function photo_matrix($id)
	{
		return DB::table('pt2_master')->where([['id', $id], ['delete_clm', '=', 0]])->first();
	}

	public static function report_excel()
	{
		$date_A = date("Y-m-d", strtotime("first day of this month") );
		$date_AK = date("Y-m-d H:i:s");

		return DB::table('pt2_master')
		->leftjoin('regu', 'pt2_master.regu_id', 'regu.id_regu')
		->LeftJoin('maintenance_datel As c', 'pt2_master.sto', '=', 'c.sto')
		->leftjoin('1_2_employee', '1_2_employee.nik', '=', 'regu.TL')
		->select('pt2_master.*', 'c.datel', 'regu.TL', 'regu.id_regu')
		->where('pt2_master.status', '=', 'Completed')
		->where('delete_clm', '=', 0)
		->WhereBetween('tgl_selesai', [$date_A, $date_AK])->OrderBy('tgl_selesai', 'ASC')
		->get();
	}

	public function list_odp_naik_download()
	{
		$date_A = date("m-d-Y", strtotime("first day of this month") );
		$date_AK = date("m-d-Y");
		//data
		$data_rapung = self::report_excel();

		foreach ($data_rapung as $no => $value)
		{
			$stts_regu = '';
			if (isset($value->ACTIVE))
			{
				if ($value->ACTIVE == 1)
				{
					$stts_regu = 'Aktif';
				}
				else
				{
					$stts_regu = 'Non-Aktif';
				}
			}

			$mydata['nomor']        = ++$no;
			$mydata['datel']        = $value->datel;
			$mydata['sto']          = $value->sto or '';
			$mydata['nomor_sc']     = $value->nomor_sc or '';
			$mydata['jenis']        = $value->jenis_wo or '';
			$mydata['tanggal_buat'] = $value->tgl_buat or '';
			$mydata['tgl_kerja']    = $value->tgl_pengerjaan or '';
			$mydata['odp_nama']     = $value->odp_nama or '';
			$mydata['status_odp']   = $value->kategory_non_unsc == 1 ? 'MIGRASI': 'UNSC';
			$mydata['project_name'] = $value->project_name or '';
			$mydata['odp_koor']     = $value->odp_koor or '';
			$mydata['regu_name']    = $value->regu_name or '';
			$mydata['stts_regu']    = $stts_regu;
			$mydata['lt_status']    = $value->lt_status or '';

			if ($value->lanjut_PT1 == 1)
			{
				$stts_golive = 'Sudah Lanjut PT-1';
			}
			elseif($value->GOLIVE == 1)
			{
				$stts_golive = 'Sudah Go-LIVE';
			}
			elseif($value->upload_abd_4 == 1)
			{
				$stts_golive = 'Sudah Upload ABD Valid 4';
			}
			elseif($value->upload_abd_4 == 0)
			{
				$stts_golive = 'Belum Upload ABD Valid 4';
			}
			else
			{
				$stts_golive = 'Belum Go-Live';
			}

			$mydata['stts_golive'] = $stts_golive;
			$mydata['tgl_submit']	 = $value->tgl_selesai;
			$mydata['kndl_detail'] = $value->kendala_detail or '';
			$mydata['keterangan'] = $value->kategory_non_unsc == 1 ? 'R2C' : 'SDI';

			$tes[$no] = $mydata;
		}
		//parent
		$parent = ['NO', 'DATEL', 'STO', 'NOMOR SC', 'JENIS', 'TANGGAL BUAT', 'TANGGAL KERJA', 'ODP NAMA', 'STATUS ODP', 'NAMA PELANGGAN', 'KOOR ODP', 'REGU', 'STATUS REGU', 'STATUS LAPORAN', 'STATUS GO-LIVE', 'TANGGAL SELESAI', 'KENDALA DETAIL', 'KETERANGAN'];

		Excel::store(new ExcelExport([$tes], $parent, 'list_odp_naik') , "Excel_DM/LIST ODP NAIK $date_A sampai $date_AK.xlsx");
		$contents = "storage/app/Excel_DM/LIST ODP NAIK $date_A sampai $date_AK.xlsx";
		Telegram::sendDocument([
		  // 'chat_id' => '519446576',
		  'chat_id' => '-586733089',
		  // 'chat_id' => '-1001114802851',
		  'document' => $contents,
		  'caption' => "Dokumen ODP naik tanggal $date_AK",
		]);

	}

	public function list_odp_3_status()
	{
		$date_A = date("Y-m-d", strtotime("first day of this month") );
		$date_AK = date("Y-m-d H:i:s");

		$data = DB::table('pt2_master')
		->leftjoin('regu', 'pt2_master.regu_id', 'regu.id_regu')
		->leftjoin('1_2_employee', '1_2_employee.nik', '=', 'regu.TL')
		->LeftJoin('maintenance_datel As c', 'pt2_master.sto', '=', 'c.sto')
		->select('pt2_master.*', 'c.datel', 'regu.TL', 'regu.id_regu', '1_2_employee.nama As nama_tl')
		->WhereIn('pt2_master.lt_status', ['Selesai', 'Kendala', 'Pending'])
		->where('delete_clm', '=', 0)
		->WhereBetween('tgl_selesai', [$date_A, $date_AK])
		->OrderBy('tgl_selesai', 'ASC')
		->get();

		$data_selesai = $data_pending = $data_kendala = [];

		foreach ($data as $no => $value)
		{
			$get_status = DB::table('pt2_status')->where('id', $value->kategory_non_unsc)->first();

			switch ($value->lt_status) {
				case 'Selesai':
					$stts_regu = '';
					if (isset($value->ACTIVE))
					{
						if ($value->ACTIVE == 1)
						{
							$stts_regu = 'Aktif';
						}
						else
						{
							$stts_regu = 'Non-Aktif';
						}
					}

					$mydata['nomor']        = ++$no;
					$mydata['datel']        = $value->datel;
					$mydata['sto']          = $value->sto or '';
					$mydata['nomor_sc']     = $value->nomor_sc or '';
					$mydata['jenis']        = $value->jenis_wo or '';
					$mydata['tanggal_buat'] = $value->tgl_buat or '';
					$mydata['tgl_kerja']    = $value->tgl_pengerjaan or '';
					$mydata['odp_nama']     = $value->odp_nama or '';
					$mydata['status_odp']   = $value->kategory_non_unsc == 1 ? 'MIGRASI': 'UNSC';
					$mydata['project_name'] = $value->project_name or '';
					$mydata['odp_koor']     = $value->odp_koor or '';
					$mydata['regu_name']    = $value->regu_name or '';
					$mydata['stts_regu']    = $stts_regu;
					$mydata['lt_status']    = $value->lt_status or '';

					if ($value->lanjut_PT1 == 1)
					{
						$stts_golive = 'Sudah Lanjut PT-1';
					}
					elseif($value->GOLIVE == 1)
					{
						$stts_golive = 'Sudah Go-LIVE';
					}
					elseif($value->upload_abd_4 == 1)
					{
						$stts_golive = 'Sudah Upload ABD Valid 4';
					}
					elseif($value->upload_abd_4 == 0)
					{
						$stts_golive = 'Belum Upload ABD Valid 4';
					}
					else
					{
						$stts_golive = 'Belum Go-Live';
					}

					$mydata['stts_golive'] = $stts_golive;
					$mydata['tgl_submit']	 = $value->tgl_selesai;
					$mydata['kndl_detail'] = $value->kendala_detail or '';
					$mydata['keterangan'] = $value->kategory_non_unsc == 1 ? 'R2C' : 'SDI';

					$tes[$no] = $mydata;

					$data_selesai[$no] = $mydata;
				break;
				case 'Pending':
					$stts_regu = '';
					if (isset($value->ACTIVE))
					{
						if ($value->ACTIVE == 1)
						{
							$stts_regu = 'Aktif';
						}
						else
						{
							$stts_regu = 'Non-Aktif';
						}
					}

					$mydata['nomor']        = ++$no;
					$mydata['datel']        = $value->datel;
					$mydata['sto']          = $value->sto or '';
					$mydata['nomor_sc']     = $value->nomor_sc or '';
					$mydata['jenis']        = $value->jenis_wo or '';
					$mydata['tanggal_buat'] = $value->tgl_buat or '';
					$mydata['tgl_kerja']    = $value->tgl_pengerjaan or '';
					$mydata['odp_nama']     = $value->odp_nama or '';
					$mydata['status_odp']   = $value->kategory_non_unsc == 1 ? 'MIGRASI': 'UNSC';
					$mydata['project_name'] = $value->project_name or '';
					$mydata['odp_koor']     = $value->odp_koor or '';
					$mydata['regu_name']    = $value->regu_name or '';
					$mydata['stts_regu']    = $stts_regu;
					$mydata['lt_status']    = $value->lt_status or '';

					if ($value->lanjut_PT1 == 1)
					{
						$stts_golive = 'Sudah Lanjut PT-1';
					}
					elseif($value->GOLIVE == 1)
					{
						$stts_golive = 'Sudah Go-LIVE';
					}
					elseif($value->upload_abd_4 == 1)
					{
						$stts_golive = 'Sudah Upload ABD Valid 4';
					}
					elseif($value->upload_abd_4 == 0)
					{
						$stts_golive = 'Belum Upload ABD Valid 4';
					}
					else
					{
						$stts_golive = 'Belum Go-Live';
					}

					$mydata['stts_golive'] = $stts_golive;
					$mydata['tgl_submit']	 = $value->tgl_selesai;
					$mydata['kndl_detail'] = $value->kendala_detail or '';
					$mydata['keterangan'] = $value->kategory_non_unsc == 1 ? 'R2C' : 'SDI';

					$tes[$no] = $mydata;

					$data_pending[$no] = $mydata;
				break;
				case 'Kendala':
					$stts_regu = '';
					if (isset($value->ACTIVE))
					{
						if ($value->ACTIVE == 1)
						{
							$stts_regu = 'Aktif';
						}
						else
						{
							$stts_regu = 'Non-Aktif';
						}
					}

					$mydata['nomor']        = ++$no;
					$mydata['datel']        = $value->datel;
					$mydata['sto']          = $value->sto or '';
					$mydata['nomor_sc']     = $value->nomor_sc or '';
					$mydata['jenis']        = $value->jenis_wo or '';
					$mydata['tanggal_buat'] = $value->tgl_buat or '';
					$mydata['tgl_kerja']    = $value->tgl_pengerjaan or '';
					$mydata['odp_nama']     = $value->odp_nama or '';
					$mydata['status_odp']   = $value->kategory_non_unsc == 1 ? 'MIGRASI': 'UNSC';
					$mydata['project_name'] = $value->project_name or '';
					$mydata['odp_koor']     = $value->odp_koor or '';
					$mydata['regu_name']    = $value->regu_name or '';
					$mydata['stts_regu']    = $stts_regu;
					$mydata['lt_status']    = $value->lt_status or '';

					if ($value->lanjut_PT1 == 1)
					{
						$stts_golive = 'Sudah Lanjut PT-1';
					}
					elseif($value->GOLIVE == 1)
					{
						$stts_golive = 'Sudah Go-LIVE';
					}
					elseif($value->upload_abd_4 == 1)
					{
						$stts_golive = 'Sudah Upload ABD Valid 4';
					}
					elseif($value->upload_abd_4 == 0)
					{
						$stts_golive = 'Belum Upload ABD Valid 4';
					}
					else
					{
						$stts_golive = 'Belum Go-Live';
					}

					$mydata['stts_golive'] = $stts_golive;
					$mydata['tgl_submit']	 = $value->tgl_selesai;
					$mydata['kndl_detail'] = $value->kendala_detail or '';
					$mydata['keterangan'] = $value->kategory_non_unsc == 1 ? 'R2C' : 'SDI';

					$tes[$no] = $mydata;

					$data_kendala[$no] = $mydata;
				break;
			}
		}

		$generate = ['Selesai' => $data_selesai, 'Pending' => $data_pending, 'Kendala' => $data_kendala];

		foreach ($generate as $k => $v)
		{
			if(count($v) )
			{
				$parent = ['NO', 'DATEL', 'STO', 'NOMOR SC', 'JENIS', 'TANGGAL BUAT', 'TANGGAL KERJA', 'ODP NAMA', 'STATUS ODP', 'NAMA PELANGGAN', 'KOOR ODP', 'REGU', 'STATUS REGU', 'STATUS LAPORAN', 'STATUS GO-LIVE', 'TANGGAL SELESAI', 'KENDALA DETAIL', 'KETERANGAN'];

				Excel::store(new ExcelExport([$v], $parent, 'list_odp_3') , "Excel_DM/LIST ODP $k $date_A sampai $date_AK.xlsx");
				$contents = "storage/app/Excel_DM/LIST ODP $k $date_A sampai $date_AK.xlsx";
				Telegram::sendDocument([
					// 'chat_id' => '519446576',
					'chat_id' => '-586733089',
					// 'chat_id' => '-1001114802851',
					'document' => $contents,
					'caption' => "Dokumen ODP $k tanggal $date_AK",
				]);
				sleep(3);
			}
		}
	}

	public static function rfc_rekap($m1, $m2)
	{
		return DB::SELECT("SELECT msr.rfc AS rfc_n, regu_name, nik1, nik2,
        SUM(CASE WHEN action = 2 THEN 1 ELSE 0 END) AS terpakai,
        SUM(CASE WHEN action = 1 THEN 1 ELSE 0 END) AS keluar_gudang,
        SUM(CASE WHEN action = 3 THEN 1 ELSE 0 END) AS kembali
        FROM maintenance_saldo_rfc msr
        WHERE MONTH(msr.created_at) BETWEEN MONTH('$m1') AND MONTH('$m2') AND LENGTH(rfc) > 2
        GROUP BY msr.rfc ORDER BY msr.id DESC
        ");
	}

	public static function search_rfc($data)
	{
		// return DB::SELECT("SELECT msr.rfc AS rfc_n, msr.*, amk.nama_barang,
		//  (SELECT COUNT(*) FROM maintenance_saldo_rfc WHERE rfc_n = rfc AND action = 2) AS terpakai,
		//  (SELECT COUNT(*) FROM maintenance_saldo_rfc WHERE rfc_n = rfc AND action = 1) AS keluar_gudang,
		//  (SELECT COUNT(*) FROM maintenance_saldo_rfc WHERE rfc_n = rfc AND action = 3) AS kembali
		//  FROM maintenance_saldo_rfc msr LEFT JOIN alista_material_keluar amk ON rfc = no_rfc WHERE msr.id_item = amk.id_barang AND rfc = '$data' GROUP BY id_barang
		//  ");
		return DB::SELECT("SELECT msr.rfc AS rfc_n, msr.*, amk.nama_barang,
        SUM(CASE WHEN action = 2 THEN 1 ELSE 0 END ) AS terpakai,
        SUM(CASE WHEN action = 1 THEN 1 ELSE 0 END ) AS keluar_gudang,
        SUM(CASE WHEN action = 3 THEN 1 ELSE 0 END ) AS kembali
        FROM maintenance_saldo_rfc msr LEFT JOIN alista_material_keluar amk ON rfc = no_rfc WHERE msr.id_item = amk.id_barang AND rfc = '$data' GROUP BY id_barang
        ");
	}

	public static function list_material()
	{
		return DB::table('pt2_khs')->where('terpakai', 1)->get();
	}

	public static function findrevenue($id)
	{
		return DB::table('pt2_khs')->where('id_item', $id)->first();
	}

	public static function del_rev($id)
	{
		return DB::table('pt2_khs')->where('id_item', $id)->update([
			'terpakai' => 0
		]);
	}

	public static function save_revenue($req, $id)
	{
		$check = self::findrevenue($id);

		if ($check)
		{
			DB::table('pt2_khs')->where('id_item', $id)->update([
				'id_item' => $req->id_item,
				'uraian' => $req->uraian,
				'satuan' => $req->satuan,
				'material_telkom' => $req->material_telkom,
				'jasa_telkom' => $req->jasa_telkom,
				'material_ta' => $req->material_ta,
				'jasa_ta' => $req->jasa_ta,
				'telkom' => $req->asal,

			]);
		}
		else
		{
			DB::table('pt2_khs')
				->insert([
					'id_item' => $req->id_item,
					'uraian' => $req->uraian,
					'satuan' => $req->satuan,
					'material_telkom' => $req->material_telkom,
					'jasa_telkom' => $req->jasa_telkom,
					'material_ta' => $req->material_ta,
					'jasa_ta' => $req->jasa_ta,
					'telkom' => $req->asal,
				]);
		}

	}

	public static function monthly_ups_excel($d)
	{
		$data = DB::table('maintenance_datel')
			->leftjoin('pt2_master', 'pt2_master.sto', '=', 'maintenance_datel.sto')
			->leftjoin('regu', 'pt2_master.regu_id', '=', 'regu.id_regu')
			->select('pt2_master.*', 'regu.mitra', 'regu.nama_mitra', 'regu.nik1', 'regu.nik2', 'regu.nama1', 'regu.nama2', 'regu.TL', 'regu.label_sektor')
			->where([
				['tgl_selesai', 'LIKE', "$d%"],
				['status', '=', 'Completed'],
			])
			->OrderBy('maintenance_datel.datel', 'ASC')
			->get();

		return $data;
	}

	public static function monthly_ups($d)
	{
		$data = DB::table('maintenance_datel')
			->leftjoin('pt2_master', 'pt2_master.sto', '=', 'maintenance_datel.sto')
			->leftjoin('regu', 'pt2_master.regu_id', '=', 'regu.id_regu')
			->select('maintenance_datel.sto', DB::RAW("CONCAT(maintenance_datel.sto, '&', datel, '&', regu_name) AS id") , 'maintenance_datel.datel', DB::RAW("(CASE WHEN mitra IS NULL THEN 'kosong' ELSE mitra END) as mitra") , 'pt2_master.regu_name', DB::RAW("DATE_FORMAT(tgl_selesai, '%d') AS hari") , DB::RAW("COUNT(*) AS jml_Selesai"))
			->where([
				['tgl_selesai', 'LIKE', "$d%"],
				['status', '=', 'Completed'],
				['delete_clm', 0]
			])
			// ->groupby(DB::RAW("DATE_FORMAT(tgl_selesai, '%d')") , 'regu_id', 'maintenance_datel.sto')
			->groupby(DB::RAW("DATE_FORMAT(tgl_selesai, '%d')") , 'regu_name')
			->OrderBy('maintenance_datel.datel', 'ASC')
			->get();

		return $data;
	}

	public static function Umur_lapor()
	{
		$that = new ReportModel();
		$jns = $that->nama_status;

		$data1 = DB::SELECT("SELECT pt2_master.*, laporan_status,
			pt2_master.modified_at as tanggal,
			CONCAT($jns) as jenis,
			DATEDIFF( NOW() , (CASE WHEN tgl_selesai IS NOT NULL THEN tgl_selesai
			WHEN tgl_pengerjaan IS NOT NULL THEN tgl_pengerjaan
			ELSE tgl_buat END) ) AS umur,
			pt2_master.odp_nama, odp_koor, project_name, jenis_wo, GOLIVE,
			ACTIVE,
			status, lt_status
			FROM pt2_master
			LEFT JOIN regu ON regu.id_regu = pt2_master.regu_id
			LEFT JOIN psb_laporan ON psb_laporan.id_tbl_mj = pt2_master.scid
			LEFT JOIN psb_laporan_status ON psb_laporan_status.laporan_status_id = psb_laporan.status_laporan
			WHERE delete_clm = 0 AND (lt_status NOT IN ('Selesai', 'Kendala') AND ACTIVE = 1 OR lt_status IS NULL) ORDER BY umur ASC");

		return $data1;
	}

	public static function go_live($y)
	{
		$data = DB::Table('pt2_master')
		->select(DB::RAW("DATE_FORMAT(tgl_selesai, '%m') As bulan"), 'pt2_master.*')
		->Where([
			['lt_status', 'Selesai'],
			['delete_clm', 0],
			['GOLIVE', 1],
			['tgl_selesai', 'LIKE', "$y%"]
		])
		->get();

		$bulan = array(
			1 => 'Januari',
			'Februari',
			'Maret',
			'April',
			'Mei',
			'Juni',
			'Juli',
			'Agustus',
			'September',
			'Oktober',
			'November',
			'Desember',
		);

		$fd = [];

		$bulan_flip = array_map(function($x){
			return $x = 0;
		}, array_flip($bulan) );

		$fd = $bulan_flip;

		foreach($data as $v)
		{
			$fd[$bulan[(int)$v->bulan] ] += 1;
		}

		return $fd;
	}

	public static function detail_lapor($req)
	{
		$data = explode(' - ', $req->date_v);
		if ($req->status == 'umur')
		{
			return DB::SELECT("SELECT pt2_master.sto, pt2_master.nomor_sc, laporan_status,
          (CASE WHEN tgl_selesai IS NOT NULL THEN tgl_selesai
          WHEN tgl_pengerjaan IS NOT NULL THEN tgl_pengerjaan
          ELSE tgl_buat END)as tanggal,
          ACTIVE,
          pt2_master.odp_nama, project_name, odp_koor, regu_name, maintenance_datel.datel,
          (CASE WHEN lt_status IS NULL THEN 'Belum Progress' ELSE lt_status END) as lt_status
          FROM pt2_master
          LEFT JOIN regu r ON r.id_regu = pt2_master.regu_id
          LEFT JOIN maintenance_datel ON sd.sto = pt2_master.sto
          LEFT JOIN psb_laporan ON psb_laporan.id_tbl_mj = pt2_master.scid
        LEFT JOIN psb_laporan_status ON psb_laporan_status.laporan_status_id = psb_laporan.status_laporan
        WHERE delete_clm = 0 AND kategory_non_unsc = 0 AND pt2_master.regu_id IS NOT NULL HAVING tanggal BETWEEN '$data[0]' AND '$data[1]' ORDER BY tanggal ASC");
		}
		elseif ($req->status == 'sync_psb')
		{
			return DB::SELECT("SELECT
          sd.sto,
          sd.datel,
          pts.nomor_sc,
          laporan_status,
          (CASE WHEN pts.tgl_selesai IS NOT NULL THEN pts.tgl_selesai
          WHEN pts.tgl_pengerjaan IS NOT NULL THEN pts.tgl_pengerjaan
          ELSE pts.tgl_buat END)as tanggal,
          pts.odp_nama, pts.project_name, pts.odp_koor,
          (CASE WHEN pts.regu_id IS NULL THEN 'Belum Dispatch' ELSE pts.regu_name END) as regu_name,
          (CASE WHEN pts.lt_status IS NULL AND pts.regu_id IS NOT NULL THEN 'Belum Progress'
          WHEN pts.lt_status IS NULL AND pts.regu_id IS NULL THEN 'Belum Dispatch'
          ELSE pts.lt_status END) as lt_status,
          pl.odp_plan as odp_nama_psb,
          pl.kordinat_odp as odp_koor_psb,
          pl.modified_at as tanggal_psb,
          (CASE WHEN my.customer IS NOT NULL THEN my.customer ELSE my2.customer END) as project_name_psb,
          e.myir,
          dt.Ndem,
          e.orderId,
          (CASE WHEN pl.odp_plan IS NOT NULL AND LENGTH(pl.odp_plan) > 6 THEN SUBSTR(pl.odp_plan, 5, 3)
          WHEN my.sto IS NOT NULL THEN my.sto
          WHEN my2.sto IS NOT NULL THEN my2.sto ELSE f.area END) AS sto_B
          FROM
          dispatch_teknisi dt
          LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
          LEFT JOIN psb_laporan_status ON psb_laporan_status.laporan_status_id = pl.status_laporan
          LEFT JOIN Data_Pelanggan_Starclick e ON dt.Ndem = e.orderId
          LEFT JOIN mdf f ON e.sto = f.mdf
          LEFT JOIN psb_myir_wo my ON my.myir = dt.Ndem
          LEFT JOIN psb_myir_wo my2 ON my2.sc = dt.Ndem
          LEFT JOIN pt2_master pts ON pts.scid = pl.id_tbl_mj
          LEFT JOIN maintenance_datel sd on sd.sto = pts.sto
          WHERE
          pl.status_laporan = 75 AND (dt.dispatch_by= '5' OR dt.dispatch_by IS NULL)
          ");
		}
	}

	public static function list_nog($datel = null, $sto = null)
	{
		$sql = DB::table('nog_master As m')
		->leftjoin('pt2_master As pm', 'm.id', '=', 'pm.id_nog_m')
		->select('m.*')
		->whereNull('pm.id')
		->where('m.tindak_lanjut', 'PT-2');

		if($sto != null)
		{
			if(strcasecmp($sto, 'All') != 0)
			{
				$sql->where('sto_nog', $sto);
			}
			else
			{
				$dt = self::get_all_datel();
				$dt = json_decode(json_encode($dt), TRUE);
				$dt = array_filter($dt, function($x) use($datel){
					return $x['datel'] == $datel;
				});

				$sql->WhereIn('sto_nog', array_column($dt, 'sto') );
			}
		}

		return $sql->get();
	}

	public static function list_jointer_pt2($datel = null, $sto = null, $odc = null)
	{
		$sql = DB::table('pt2_master As pm')
		->leftjoin('regu As r', 'r.id_regu', '=', 'pm.regu_id')
		->select('pm.*', 'pm.odp_nama As nama_odp', 'pm.odp_koor As koordinat', 'pm.project_name As nama_order', 'pm.tgl_buat As created_at', 'pm.id As id_pm')
		->where(function($join){
			$join->whereNull('pm.id')
			->OrwhereNotNull('pm.id')
			->WhereNull('pm.regu_id')
			->Where([
				['delete_clm', 0],
				['id_maint', 0],
				['id_bts', 0]
			]);
		})
		->where(function($join){
			$join->WhereNull('pm.regu_id');
		})
		->where(function($join){
			$join->whereNull('lt_status')
			->OrwhereNotIn('lt_status', ['Selesai', 'Kendala']);
		});

		if($sto && strcasecmp($sto, 'All') != 0)
		{
			$sql->where('pm.sto', $sto);
		}

		if($datel && strcasecmp($datel, 'All') != 0)
		{
			$dt = self::get_all_datel();
			$dt = json_decode(json_encode($dt), TRUE);
			$dt = array_filter($dt, function($x) use($datel){
				return $x['datel'] == $datel;
			});

			$sql->WhereIn('pm.sto', array_column($dt, 'sto') );
		}

		if($odc && strcasecmp($odc, 'All') != 0 && strcasecmp($odc, 'kosong') != 0)
		{
			$sql->where(DB::RAW("SUBSTRING_INDEX(SUBSTRING_INDEX(pm.odp_nama, '/', 1), '-', -1)"), $odc);
		}

		return $sql->get();
	}

	public static function list_jointer_marina($datel = null, $sto = null, $odc = null)
	{
		$sql = DB::table('maintaince As m')
		->leftjoin('pt2_master As pm', 'm.id', '=', 'pm.id_maint')
		->leftjoin('regu As r', 'r.id_regu', '=', 'pm.regu_id')
		->leftjoin('pt2_ready_jointer As prj', function($join){
			$join->On('prj.id_wo', '=', 'm.id')
			->Where('prj.jenis', '=', 'marina');
		})
		->select('m.*', 'pm.id As id_pm')
		->WhereNotNull('prj.id')
		->where(function($join){
			$join->whereNull('pm.id')
			->OrwhereNotNull('pm.id')
			->WhereNull('pm.regu_id')
			->Where([
				['delete_clm', 0],
			]);
		})
		->where(function($join){
			$join->whereNull('lt_status')
			->OrwhereNotIn('lt_status', ['Selesai', 'Kendala']);
		});

		if($sto && strcasecmp($sto, 'All') != 0)
		{
			$sql->where('m.sto', $sto);
		}

		if($datel && strcasecmp($datel, 'All') != 0)
		{
			$dt = self::get_all_datel();
			$dt = json_decode(json_encode($dt), TRUE);
			$dt = array_filter($dt, function($x) use($datel){
				return $x['datel'] == $datel;
			});

			$sql->WhereIn('m.sto', array_column($dt, 'sto') );
		}

		if($odc && strcasecmp($odc, 'All') != 0)
		{
			$sql->where(DB::RAW("SUBSTRING_INDEX(SUBSTRING_INDEX(m.nama_odp, '/', 1), '-', -1)"), $odc);
		}

		return $sql->get();
	}

	public static function list_jointer_bts($datel = null, $sto = null, $odc = null)
	{
		$sql = DB::table('mpromise_lop As ml')
		->Leftjoin('mpromise_core As mc', 'mc.LOP_ID', '=', 'ml.id')
		->leftjoin('pt2_master As pm', 'mc.ID_Sys', '=', 'pm.id_bts')
		->leftjoin('regu As r', 'r.id_regu', '=', 'pm.regu_id')
		->leftjoin('pt2_ready_jointer As prj', function($join){
			$join->On('prj.id_wo', '=', 'mc.ID_Sys')
			->Where('prj.jenis', '=', 'bts');
		})
		->select('mc.ID_Sys As id', 'ml.sto', 'ml.created_at', 'mc.Plan_Odp_Nama As nama_odp', DB::RAW("CONCAT(mc.Plan_Odp_LAT,',',mc.Plan_Odp_LON) As koordinat"), 'ml.nama_lop As nama_order', 'pm.id As id_pm')
		->WhereNotNull('prj.id')
		->WhereNotNull('mc.Plan_Odp_Nama')
		->where(function($join){
			$join->whereNull('pm.id')
			->OrwhereNotNull('pm.id')
			->WhereNull('pm.regu_id')
			->Where([
				['delete_clm', 0],
			]);
		})
		->where(function($join){
			$join->whereNull('lt_status')
			->OrwhereNotIn('lt_status', ['Selesai', 'Kendala']);
		});

		if($sto && strcasecmp($sto, 'All') != 0)
		{
			$sql->where('ml.sto', $sto);
		}

		if($datel && strcasecmp($datel, 'All') != 0)
		{
			$dt = self::get_all_datel();
			$dt = json_decode(json_encode($dt), TRUE);
			$dt = array_filter($dt, function($x) use($datel){
				return $x['datel'] == $datel;
			});

			$sql->WhereIn('ml.sto', array_column($dt, 'sto') );
		}

		if($odc && strcasecmp($odc, 'All') != 0)
		{
			$sql->where(DB::RAW("SUBSTRING_INDEX(SUBSTRING_INDEX(mc.Plan_Odp_Nama, '/', 1), '-', -1)"), $odc);
		}

		return $sql->get();
	}

	public static function get_all_datel()
	{
		$curl = curl_init();

		curl_setopt_array($curl, [
			CURLOPT_URL => 'https://promitos.tomman.app/API_DATEL',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
		]);

		$response = curl_exec($curl);
		curl_close($curl);

		$data = json_decode($response);

		return $data;
	}

	public static function get_info_odc($odc = 'all',  $sto = 'all', $datel= 'all')
	{
		$curl = curl_init();

		$post = [
			'datel' => $datel,
			'sto'   => $sto,
			'odc'   => $odc,
		];

		curl_setopt_array($curl, [
			CURLOPT_URL            => 'https://promitos.tomman.app/API_ODC',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_POSTFIELDS     => $post,
			CURLOPT_ENCODING       => '',
			CURLOPT_MAXREDIRS      => 10,
			CURLOPT_TIMEOUT        => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST  => 'POST',
		]);

		$response = curl_exec($curl);
		curl_close($curl);

		$data = json_decode($response);

		return $data;
	}

	public static function getdetail_nog($id)
	{
		return DB::table('nog_master As m')
		->where([
			['id', $id],
			['tindak_lanjut', 'PT-2']
			])
		->first();
	}

	public static function getdetail_jointer($id)
	{
		return DB::table('maintaince As m')
		->where([
			['id', $id]
		])
		->first();
	}

	public static function getdetail_psb($id)
	{
		return DB::SELECT("SELECT
		dt.id as id_dispatch,
		pl.id as id_laporan,
		dps.orderId,
		dps.orderName,
		dps.sto,
		pl.kordinat_pelanggan,
		pl.nama_odp,
		pl.kordinat_odp,
		pl.modified_at as tgl_laporan,
		dt.tgl as tgl_dispatch
		FROM Data_Pelanggan_Starclick dps
		LEFT JOIN dispatch_teknisi dt ON dps.orderIdInteger = dt.NO_ORDER
		LEFT JOIN regu r ON dt.id_regu = r.id_regu
		LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
		LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
		WHERE
		pls.laporan_status = 'PT2 OK' AND dps.orderId = $id
		ORDER BY pl.modified_at ASC")[0];
	}

	public static function list_pt2_psb($datel = null, $sto = null)
	{
		$sql = '';

		if($sto != null )
		{
			if(strcasecmp($sto, 'All') != 0)
			{
				$sql .= "AND dps.sto = '$sto'";
			}
			else
			{
				$dt = self::get_all_datel();
				$dt = json_decode(json_encode($dt), TRUE);
				$dt = array_filter($dt, function($x) use($datel){
					return $x['datel'] == $datel;
				});

				$dt = implode("', '", array_column($dt, 'sto') );

				$sql .= "AND dps.sto IN $dt";
			}
		}

		return DB::SELECT("SELECT
		dt.id as id_dispatch,
		pl.id as id_laporan,
		dps.orderId,
		dps.orderName,
		dps.sto,
		pl.kordinat_pelanggan,
		pl.nama_odp,
		pl.kordinat_odp,
		pl.modified_at as tgl_laporan,
		dt.tgl as tgl_dispatch
		FROM Data_Pelanggan_Starclick dps
		LEFT JOIN dispatch_teknisi dt ON dps.orderIdInteger = dt.NO_ORDER
		LEFT JOIN regu r ON dt.id_regu = r.id_regu
		LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
		LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
		LEFT JOIN pt2_master pm ON pm.nomor_sc = dps.orderId
		WHERE
		pls.laporan_status = 'PT2 OK' AND pm.id IS NULL $sql
		ORDER BY pl.modified_at ASC");
	}

	public static function get_kpro_pt2($jenis = null, $regu_jp = null, $datel = null, $sto = null)
	{
		$get_data = DB::table('pt2simple_kpro_tr6');
		$all_pt2 = DB::table('pt2_master');

		if($sto != null)
		{
			if(strcasecmp($sto, 'All') != 0)
			{
				$get_data->where('sto', $sto);
			}
			else
			{
				if(strcasecmp($datel, 'All') != 0)
				{
					$dt = self::get_all_datel();
					$dt = json_decode(json_encode($dt), TRUE);
					$dt = array_filter($dt, function($x) use($datel){
						return $x['datel'] == $datel;
					});
					$get_data->WhereIn('sto', array_column($dt, 'sto') );
				}
			}
		}

		if($regu_jp != null)
		{
			if(strcasecmp($regu_jp, 'All') != 0)
			{
				if($jenis == 'pt')
				{
					$all_pt2->where('regu_name', urldecode($regu_jp) );
				}
				else
				{
					if($regu_jp == 'Kosong')
					{
						$regu_jp = '';
					}
					$get_data->where('jenispb', urldecode($regu_jp) );
				}
			}
		}

		$all_pt2 = $all_pt2->where('delete_clm', 0)->get();
		$get_data = $get_data->GroupBy('odp')->get();

		$all_pt2 = array_map(function($item) {
			return (array)$item;
		}, $all_pt2->toArray() );

		$data_load_scid = $data_load_scid_h = [];
		$dlc = $dlch = [];

		foreach($get_data as $k => $v)
		{
			$data_load_scid[$k] = array_filter($all_pt2, function($vv) use($v){
				if($vv['nomor_sc'] != 0 && $v->order_id == $vv['nomor_sc'] )
				{
					return $vv;
				}
			});
		}

		$data_load_scid = array_filter($data_load_scid);

		foreach($data_load_scid as $kk => $vx)
		{
			$dlc[$kk]['status_kpro']      = $get_data[$kk]->status_prj;
			$dlc[$kk]['id_kpro']          = $get_data[$kk]->id;
			$dlc[$kk]['odp_kpro']         = $get_data[$kk]->odp;
			$dlc[$kk]['sto_kpro']         = $get_data[$kk]->sto;
			$dlc[$kk]['jenispb']          = $get_data[$kk]->jenispb;
			$dlc[$kk]['last_update_kpro'] = $get_data[$kk]->last_updated_date;

			foreach($vx as $k => $vvx)
			{
				$dlc[$kk]['id_pt2']          = $vvx['id'];
				$dlc[$kk]['project_name']    = $vvx['project_name'];
				$dlc[$kk]['lt_status']       = $vvx['lt_status'];
				$dlc[$kk]['odp_nama']        = $vvx['odp_nama'];
				$dlc[$kk]['kendala_detail']	 = $vvx['kendala_detail'];
				$dlc[$kk]['tgl_selesai']     = $vvx['tgl_selesai'];
				$dlc[$kk]['lt_project_nama'] = $vvx['lt_project_nama'];
				$dlc[$kk]['regu_id']         = $vvx['regu_id'];
				$dlc[$kk]['regu_name']       = $vvx['regu_name'];
				$dlc[$kk]['sto_pt2']	       = $vvx['sto'];

				unset($all_pt2[$k]);
			}
		}

		foreach(array_keys(array_filter($data_load_scid) ) as $v)
		{
			unset($get_data[$v]);
		}

		foreach($get_data as $k => $v)
		{
			$data_load_scid_h[$k] = array_filter($all_pt2, function($vv) use($v){
				if(str_contains($v->order_id, $vv['project_name']) )
				{
					return $vv;
				}
			});
		}

		$data_load_scid_h = array_filter($data_load_scid_h);

		foreach($data_load_scid_h as $kk => $vx)
		{
			$dlch[$kk]['status_kpro']      = $get_data[$kk]->status_prj;
			$dlch[$kk]['id_kpro']          = $get_data[$kk]->id;
			$dlch[$kk]['odp_kpro']         = $get_data[$kk]->odp;
			$dlch[$kk]['sto_kpro']         = $get_data[$kk]->sto;
			$dlch[$kk]['jenispb']          = $get_data[$kk]->jenispb;
			$dlch[$kk]['last_update_kpro'] = $get_data[$kk]->last_updated_date;

			foreach($vx as $k => $vvx)
			{
				$dlch[$kk]['id_pt2']          = $vvx['id'];
				$dlch[$kk]['project_name']    = $vvx['project_name'];
				$dlch[$kk]['lt_status']       = $vvx['lt_status'];
				$dlch[$kk]['odp_nama']        = $vvx['odp_nama'];
				$dlch[$kk]['kendala_detail']	= $vvx['kendala_detail'];
				$dlch[$kk]['tgl_selesai']     = $vvx['tgl_selesai'];
				$dlch[$kk]['lt_project_nama'] = $vvx['lt_project_nama'];
				$dlch[$kk]['regu_id']         = $vvx['regu_id'];
				$dlch[$kk]['regu_name']       = $vvx['regu_name'];
				$dlch[$kk]['sto_pt2']	        = $vvx['sto'];

				unset($all_pt2[$k]);
			}
		}

		foreach(array_keys(array_filter($data_load_scid_h) ) as $v)
		{
			unset($get_data[$v]);
		}

		return ['pt2' => array_merge($dlc, $dlch), 'kpro' => $get_data];
	}

	public static function get_data_monitor($knu, $start, $end, $mitra = 'all')
	{
		//revenue per mitra
		$data = DB::Table('pt2_master As pmas')
		->leftjoin('pt2_status As pstat', 'pstat.id', '=', 'pmas.kategory_non_unsc')
		->leftjoin('pt2_material As pmat', 'pmas.id', '=', 'pmat.pt2_id')
		->leftjoin('regu As r', 'r.id_regu', '=', 'pmas.regu_id')
		->leftjoin('mitra_amija As ma', 'ma.mitra_amija', '=', 'r.mitra')
		->select('pmas.*', 'pstat.status as status_pst', 'pmat.id_item', 'pmat.qty', 'ma.mitra_amija_pt', DB::RAW("CONCAT(r.TL, ' (', (SELECT nama FROM 1_2_employee WHERE nik = r.TL) , ')') as tl_nama ") )
		->where([
			['lt_status', 'Selesai'],
			['pmas.delete_clm', 0],
		])
		->whereBetween(DB::Raw("DATE_FORMAT(tgl_selesai, '%Y-%m-%d')"), [$start, $end]);

		if($knu)
		{
			$data->where('jenis_wo', $knu);
		}

		if($mitra != 'all')
		{
			$data->where('ma.mitra_amija_pt', $mitra);
		}

		$data = $data->get();

		$get_khs_procurement = DB::table('procurement_designator')
		->where([
			[DB::RAW("date_format(tgl_start, '%Y-%m')"), '<=', date('Y-m', strtotime($start) )],
			[DB::RAW("date_format(tgl_end, '%Y-%m')"), '>=', date('Y-m', strtotime($end) )],
			['witel', 'KALSEL']
		])
		->OrderBy('tgl_start', 'DESC')
		->OrderBy('id', 'DESC')
		->get();

		$get_khs_procurement = array_map(function($item) {
			return (array)$item;
		}, $get_khs_procurement->toArray() );

		$data_khs = DB::Table('pt2_khs')->where('terpakai', 1)->get();

		$rekap_revenue = $rekap_close = $status_pekerjaan = [];

		foreach($data as $v)
		{
			$status_pekerjaan[$v->jenis_wo] = [];
		}

		foreach($data as $v)
		{
			$find_khs = array_search($v->id_item, array_column(json_decode(json_encode($data_khs), TRUE), 'id_item' ) );

			if($find_khs !== FALSE)
			{
				$find_harga = array_keys(array_column($get_khs_procurement, 'designator' ), $v->id_item);

				if(count($find_harga) )
				{
					$material = $get_khs_procurement[$find_harga[0] ]['material'];
					$jasa     = $get_khs_procurement[$find_harga[0] ]['jasa'];
				}
				else
				{
					$dkn = $data_khs[$find_khs];

					$material = $dkn->material_ta;
					$jasa     = $dkn->jasa_ta;
				}

				$rekap_revenue[$v->jenis_wo][$v->mitra_amija_pt]['mitra'] = $v->mitra_amija_pt;

				if(!isset($rekap_revenue[$v->jenis_wo][$v->mitra_amija_pt]['material_ta']) )
				{
					$rekap_revenue[$v->jenis_wo][$v->mitra_amija_pt]['material_ta'] = 0;
					$rekap_revenue[$v->jenis_wo][$v->mitra_amija_pt]['jasa_ta']     = 0;
				}

				// $rekap_revenue[$v->jenis_wo][$v->mitra_amija_pt]['knc'] = $v->kategory_non_unsc;

				$rekap_revenue[$v->jenis_wo][$v->mitra_amija_pt]['jml_close'][$v->id] = 1;

				$rekap_revenue[$v->jenis_wo][$v->mitra_amija_pt]['material_ta'] += $v->qty * $material;
				$rekap_revenue[$v->jenis_wo][$v->mitra_amija_pt]['jasa_ta']     += $v->qty * $jasa;

				$rekap_revenue_all['ALL'][$v->mitra_amija_pt]['mitra'] = $v->mitra_amija_pt;

				if(!isset($rekap_revenue_all['ALL'][$v->mitra_amija_pt]['material_ta']) )
				{
					$rekap_revenue_all['ALL'][$v->mitra_amija_pt]['material_ta'] = 0;
					$rekap_revenue_all['ALL'][$v->mitra_amija_pt]['jasa_ta']     = 0;
				}

				// $rekap_revenue_all['ALL'][$v->mitra_amija_pt]['knc'] = $v->kategory_non_unsc;

				$rekap_revenue_all['ALL'][$v->mitra_amija_pt]['jml_close'][$v->id] = 1;

				$rekap_revenue_all['ALL'][$v->mitra_amija_pt]['material_ta'] += $v->qty * $material;
				$rekap_revenue_all['ALL'][$v->mitra_amija_pt]['jasa_ta']     += $v->qty * $jasa;
			}

			$rekap_close[$v->regu_id]['mitra'] = $v->mitra_amija_pt;
			$rekap_close[$v->regu_id]['regu'] = $v->regu_name;
			$rekap_close[$v->regu_id]['job'][$v->id]['pekerjaan'] = $v->project_name;
			$rekap_close[$v->regu_id]['job'][$v->id]['odp_nama'] = $v->odp_nama;
		}

		return ['rekap_close' => $rekap_close, 'rekap_revenue' => array_merge($rekap_revenue_all, $rekap_revenue), 'status_pekerjaan' => $status_pekerjaan];
	}

	public static function get_detail_report_masis($tim, $d)
	{
		$data = DB::Table('pt2_master')
		->where('lt_status', 'Selesai');

		if(strcasecmp($tim, 'all') != 0)
		{
			$data->where('regu_name', 'LIKE', "%$tim%");
		}

		if(strlen($d) > 7)
		{
			$data->where(DB::RAW("DATE_FORMAT(tgl_selesai, '%Y-%m-%d')"), $d);
		}
		else
		{
			$data->where(DB::RAW("DATE_FORMAT(tgl_selesai, '%Y-%m')"), $d);
		}

		return $data->get();
	}

	public static function get_kendala_estimasi()
	{
		return DB::table('pt2_master As a')
		->LeftJoin('regu As b', 'a.regu_id', '=', 'b.id_regu')
		->LeftJoin('maintenance_datel As c', 'a.sto', '=', 'c.sto')
		->Select('a.*', 'b.ACTIVE', 'c.datel', DB::RAW("DATE_ADD(DATE(tgl_selesai), INTERVAL 1 DAY) As tgl_estimasi") )
		->WhereIn('kendala_detail' , ['Core Ccd', 'Feeder Reti', 'Tercover Odp Lain', 'Feeder Full', 'Olt Full', 'Distribusi Full', 'Kendala Jalur Tidak Ada Tiang Kosong', 'Kendala Cuaca Hujan', 'Pindah Distribusi/Core'])
		->get();
	}

	public static function core_up($date)
	{
		return DB::table('pt2_core_log As pcl')
		->Leftjoin('pt2_dispatch As pd', 'pd.id_pt2', '=', 'pcl.id_dispatch')
		->leftjoin('1_2_employee As emp', 'emp.nik', '=', 'pcl.created_by')
		->Leftjoin('pt2_master As pm', 'pd.id_order', '=', 'pm.id')
		->select('pcl.*', 'emp.nama')
		->whereNotNull('pm.regu_id')
		->get();
	}

	public static function umur_kerjaan($mitra = null, $regu = null, $jenis = null)
	{
		$data = DB::table("pt2_master As pm")
		->leftjoin('pt2_status As pstat', 'pstat.id', '=', 'pm.kategory_non_unsc')
		->leftjoin('user As u', 'u.id_user', '=', 'pm.nik1')
		->leftjoin('regu As r', 'r.id_regu', '=', 'pm.regu_id')
		->Leftjoin('mitra_amija As mm', 'mm.mitra_amija', '=', 'r.mitra')
		->select('pm.*', 'mm.mitra_amija_pt', 'pstat.status as status_pst', 'r.ACTIVE')
		->where(function($join){
			$join->whereNull('lt_status')
			->OrwhereNotIn('lt_status', ['Selesai', 'Kendala']);
		})
		->Where([
			['r.job', 'PT2'],
			['delete_clm', 0]
		]);

		if($mitra && strcasecmp($mitra, 'All') != 0 && strcasecmp($mitra, 'kosong') != 0)
		{
			$data->where('mm.mitra_amija_pt', $mitra);
		}

		if($regu && strcasecmp($regu, 'All') != 0)
		{
			$data->where('pm.regu_id', $regu);
		}

		if($jenis && strcasecmp($jenis, 'All') != 0)
		{
			switch ($jenis) {
				case '_1':
					$data->where(DB::RAW("DATEDIFF(NOW() ,  pm.modified_at)"), '<', 1);
				break;
				case '1_3':
					$data->where([
						[DB::RAW("DATEDIFF(NOW() ,  pm.modified_at)"), '>=', 1],
						[DB::RAW("DATEDIFF(NOW() ,  pm.modified_at)"), '<=', 3]
					]);
				break;
				case '4_7':
					$data->where([
						[DB::RAW("DATEDIFF(NOW() ,  pm.modified_at)"), '>', 3],
						[DB::RAW("DATEDIFF(NOW() ,  pm.modified_at)"), '<=', 7]
					]);
				break;
				case '7_':
					$data->where([
						[DB::RAW("DATEDIFF(NOW() ,  pm.modified_at)"), '>', 7],
					]);
				break;
			}
		}

		$data = $data->get();

		return $data;
	}

	public static function get_sector()
	{
		return DB::Table('maintenance_sector')->get();
	}

	public static function getdetail_krpo_simple($id)
	{
		return DB::Table('pt2simple_kpro_tr6')->where('id', $id)->first();
	}
}