<?php
namespace App\DA;

use File;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use Telegram;
use ZipArchive;
use App\Http\Controllers\TeknisiController;
use App\Http\Controllers\AdminController;
use App\DA\ReportModel;

date_default_timezone_set("Asia/Makassar");
class TeknisiModel
{

	protected $phototelegram_undone = ['Port_Distribusi', 'ODC', 'Progress'];

	public $photo_submit_tele = ['QRcode_Tiang', 'ODP', 'Qc_1', 'Qc_2', 'QRcode_ODP', 'QRcode_SPL', 'Redaman_IN', 'Redaman_OUT', 'Port_Feeder', 'Port_Distribusi', 'OLT', 'FTM_2', 'O_side', 'E_side', 'ODC', 'Progress', 'Photo_Rfc', 'Photo_Rfc_2'];

	public static function rfc_search($value, $gudang)
	{
		$mainD = DB::table('alista_material_keluar')->where('no_rfc', 'like', '%' . $value . '%');
		if ($gudang == 'no_wh')
		{
			return $mainD->groupBy('no_rfc')
				->limit(7)
				->get();
		}
		elseif ($gudang == 'single')
		{
			return $mainD->get();
		}
		else
		{
			return $mainD->where('nama_gudang', 'like', '%' . $gudang . '%')->groupBy('no_rfc')
				->limit(7)
				->get();
		}
	}

	public static function find_gudang($gudang)
	{
		return DB::table('alista_gudang')->where('id_alista_gudang', $gudang)->first();
	}

	public static function check_jalan($id)
	{
		$user_mine = session('auth')->id_user;
		$urgent_msg = [];
		DB::transaction(function () use ($user_mine, $id, &$urgent_msg)
		{
			DB::table('pt2_master')->where('id', $id)->update(['lt_status' => 'Berangkat', 'tgl_pengerjaan' => date('Y-m-d H:i:s') , ]);

			$save = DB::table('pt2_master')->where('id', $id)->first();
			$message = '';
			$message .= "<b>Laporan Teknisi PT2</b> \n";
			$message .= "\n \n \n";
			$message .= "<b>Tanggal Laporan Jalan: </b>" . date('Y-m-d H:i:s') . "\n";

			if ($save->pelanggan_koor)
			{
				$message .= "<b>Koordinat Pelanggan</b> : $save->pelanggan_koor\n";
			}

			$message .= "<b>Id User Yang Memulai: </b> $user_mine \n";
			$message .= "<b>Team Regu: </b> $save->regu_name \n";

			if ($save->odp_nama_before)
			{
				$message .= "<b>Nama ODP Sebelumnya</b>: $save->odp_nama_before \n";
			}

			$message .= "<b>Nama ODP :</b> $save->odp_nama \n";

			if ($save->odp_koor_before)
			{
				$message .= "<b>Koordinat ODP Sebelumnya</b>: $save->odp_koor_before \n";
			}

			$message .= '#' . preg_replace('/[^a-z0-9]/i', '', $save->regu_name);

			$that = new TeknisiModel();

			try
			{
				Telegram::sendMessage([
					'chat_id' => AdminModel::telegramId($save->id) ,
					'parse_mode' => 'html',
					'text' => "$message"
				]);
			}
			catch(\Symfony\Component\HttpFoundation\File\Exception\FileException $e)
			{
				$urgent_msg['msg'] = ['type' => 'danger', 'text' => 'Laporan Tidak Terkirim, Lapor Grup Agar selalu up-to-date!'];
			}

			DB::table('pt2_dispatch')->insert([
				'id_order'           => $save->id,
				'regu_id'            => $save->regu_id,
				'tgl_kerja_dispatch' => $save->tgl_pengerjaan,
				'Keterangan'         => 'Update Teknisi',
				'status'             => 'Berangkat',
				'updated_by'         => $user_mine,
			]);

		});
		return $urgent_msg;
	}

	public static function teknisi_join()
	{
		$id = session('auth')->id_user;
		$now = DB::raw('NOW()');
		$rm = new ReportModel();
		$ns = $rm->nama_status;
		return DB::table('pt2_master')
		->leftjoin('pt2_status As ps', 'pt2_master.kategory_non_unsc', '=', 'ps.id')
		->leftjoin('psb_laporan', 'pt2_master.scid', '=', 'psb_laporan.id_tbl_mj')
		->leftjoin('regu', 'regu.id_regu', '=', 'pt2_master.regu_id')
		->select("pt2_master.*", DB::RAW("($ns) As jenis"), 'ps.status as status_nama' )
		->whereNotNull('pt2_master.regu_id')
		->where(function ($join)
		{
			$join->whereNull('psb_laporan.status_laporan')
			->orWhere('psb_laporan.status_laporan', 75);
		})
		->where('pt2_master.status', '=', 'Construction')
		->where('delete_clm', '=', 0)
		->where(function ($join) use ($now)
		{
			$join->whereNull('lt_status')
			->orwhere('lt_status', '=', 'Berangkat')
			->orwhere('lt_status', '=', 'Tidak Sempat')
			->orwhere('lt_status', '=', 'Tiba');
		})
		->where(function ($join) use ($id)
		{
			$join->where('regu.nik1', $id)
			->orWhere('regu.nik2', $id)
			->orWhere('regu.nik3', $id)
			->orWhere('regu.nik4', $id);
		})
		->orderBy('pt2_master.id', 'desc')
		->get();
	}

	public static function teknisi_not_pt2()
	{
		$id = session('auth')->id_user;
		$now = DB::raw('NOW()');

		return DB::table('pt2_master')
			->leftjoin('pt2_status As ps', 'pt2_master.kategory_non_unsc', '=', 'ps.id')
			->leftjoin('psb_laporan', 'pt2_master.scid', '=', 'psb_laporan.id_tbl_mj')
			->select('pt2_master.*', 'ps.status as status_nama')
			->Where('psb_laporan.status_laporan', '!=', 75)
			->where('delete_clm', '=', 0)->where(function ($join) use ($now)
		{
			$join->whereNull('lt_status')
				->OrWhereNotIn('lt_status', ['Kendala', 'Selesai']);
		})->where(function ($join) use ($id)
		{
			$join->where('nik1', $id)->orWhere('nik2', $id);
		})->orderBy('pt2_master.id', 'desc')
			->get();
	}

	public static function teknisi_done()
	{
		$id = session('auth')->id_user;

		return DB::table('pt2_master')
			->leftjoin('pt2_status As ps', 'pt2_master.kategory_non_unsc', '=', 'ps.id')
			->select('pt2_master.*', 'ps.status as status_nama')
			->where(function ($join)
			{
				$join->where('pt2_master.status', 'Completed');
				// ->orWhere('pt2_master.lt_status', 'Odp Naik');
			})
			->where('delete_clm', '=', 0)
			->where(function ($join) use ($id)
			{
				$join->where('nik1', $id)
				->orWhere('nik2', $id);
			})
			->orderBy('pt2_master.id', 'desc')
			->get();
	}

	public static function teknisi_kendala()
	{
		$id = session('auth')->id_user;

		return DB::table('pt2_master')
			->leftjoin('pt2_status As ps', 'pt2_master.kategory_non_unsc', '=', 'ps.id')
			->select('pt2_master.*', 'ps.status as status_nama')
			->where('lt_status', '=', 'Kendala')
			->where('delete_clm', '=', 0)->where(function ($join) use ($id)
		{
			$join->where('nik1', $id)->orWhere('nik2', $id);
		})->orderBy('pt2_master.id', 'desc')
			->get();
	}

	public static function teknisi_ogp()
	{
		$id = session('auth')->id_user;

		return DB::table('pt2_master')
			->leftjoin('pt2_status As ps', 'pt2_master.kategory_non_unsc', '=', 'ps.id')
			->leftjoin('psb_laporan', 'pt2_master.scid', '=', 'psb_laporan.id_tbl_mj')->where(function ($join)
		{
			$join->whereNull('psb_laporan.status_laporan')
				->orWhere('psb_laporan.status_laporan', 75);
		})
			->whereIn('lt_status', ['Ogp', 'On Progress'])
			->where('delete_clm', '=', 0)
			->where(function ($join) use ($id)
			{
				$join->where('nik1', $id)
				->orWhere('nik2', $id);
			})
		->select('pt2_master.*', 'ps.status as status_nama')
		->orderBy('pt2_master.id', 'desc')
		->get();
	}

	public static function teknisi_ogp_reject()
	{
		$id = session('auth')->id_user;

		return DB::table('pt2_master')
			->leftjoin('pt2_status As ps', 'pt2_master.kategory_non_unsc', '=', 'ps.id')
			->leftjoin('psb_laporan', 'pt2_master.scid', '=', 'psb_laporan.id_tbl_mj')->where(function ($join)
		{
			$join->whereNull('psb_laporan.status_laporan')
				->orWhere('psb_laporan.status_laporan', 75);
		})
			->whereIn('lt_status', ['Ogp', 'On Progress'])
			->where('delete_clm', '=', 0)
			->where('reject', '=', 1)
			->where(function ($join) use ($id)
			{
				$join->where('nik1', $id)
				->orWhere('nik2', $id);
			})
		->select('pt2_master.*', 'ps.status as status_nama')
		->orderBy('pt2_master.id', 'desc')
		->get();
	}

	public static function teknisi_pending()
	{
		$id = session('auth')->id_user;

		return DB::table('pt2_master')
			->leftjoin('pt2_status As ps', 'pt2_master.kategory_non_unsc', '=', 'ps.id')
			->leftjoin('psb_laporan', 'pt2_master.scid', '=', 'psb_laporan.id_tbl_mj')
			->where(function ($join)
		{
			$join->whereNull('psb_laporan.status_laporan')
				->orWhere('psb_laporan.status_laporan', 75);
		})
			->where('lt_status', '=', 'Pending')
			->where('delete_clm', '=', 0)->where(function ($join) use ($id)
		{
			$join->where('nik1', $id)->orWhere('nik2', $id);
		})
		->select('pt2_master.*', 'ps.status as status_nama')
		->orderBy('pt2_master.id', 'desc')
		->get();
	}

	public static function reject_dispatch_teknisi($id, $req)
	{
		$po = DB::table('pt2_master')
		->leftjoin("regu As r", 'r.id_regu', '=', 'pt2_master.regu_id')
		->select("pt2_master.*")
		->where('id', $id)
		->first();

		if(session('auth')->pt2_level == 2)
		{
			$ket = 'Update Admin';
		}

		if(session('auth')->pt2_level == 5)
		{
			$ket = 'Update SuperAdmin';
		}

		DB::table('pt2_master')->Where('id', $id)->update([
			'status'		=> 'Construction',
			'reject'	  => 1,
			'lt_status' => 'Ogp',
		]);

		DB::table('pt2_dispatch')->insert([
			'id_order'           => $po->id,
			'regu_id'            => $po->regu_id,
			'nik1'							 => session('auth')->id_user,
			'tgl_kerja_dispatch' => $po->tgl_pengerjaan,
			'Keterangan'         => $ket ." \n". $req->catatan_admin,
			'status'             => $req->status,
			'updated_by'         => $id,
		]);

		$data = DB::table('pt2_master')->Where('id', $id)->first();

		$message = "";
		$message .= "<b>Tanggal Reject: </b>" .date('Y-m-d H:i:s'). " \n";
		$message .= "<b>Pekerjaan: </b> $data->project_name \n";
		$message .= "<b>Team Regu: </b> $data->regu_name \n";
		$message .= "<b>Keterangan: </b> $req->catatan_admin \n";

		Telegram::sendMessage([
			'chat_id' => AdminModel::telegramId($data->id) ,
			'parse_mode' => 'html',
			'text' => "$message"
		]);

		$urgent_msg['msg'] = ['type' => 'danger', 'text' => 'Laporan Berhasil Ditolak!!'];
	}

	public function saveLaporan($req)
	{
		$id = session('auth')->id_user;
		$ide = $req->serial;
		$this->handleFileUpload($req, $ide);
		$po = DB::table('pt2_master')
		->leftjoin("regu As r", 'r.id_regu', '=', 'pt2_master.regu_id')
		->select("pt2_master.*")
		->where('id', $ide)
		->first();
		// $check_po = DB::table('pt2_po')->where('id_project_Ta', $po->proaktif_id)->first();

		if ($req->rfc_number)
		{
			$rfc = implode(',', $req->rfc_number);
		}
		else
		{
			$rfc = null;
		}

		$urgent_msg = [];

		DB::transaction(function () use ($ide, $id, $req, $po, $rfc, &$urgent_msg)
		{
			//update ke psb
			//perform update pt2_master
			$save_pt2 = [
				'lt_status'        => $req->status,
				'lt_koordinat_odp' => $req->koordinat_odp,
				'lt_project_nama'  => $req->project_name,
				'lt_catatan'       => $req->catatan,
				'catatan_O_side'   => $req->catatan_O_side,
				'catatan_E_side'   => $req->catatan_E_side,
				'cht_bot_valins'	 => $req->bot_valins,
				'tgl_selesai'      => date('Y-m-d H:i:s')
			];

			$save_pt2['odp_nama']   = $req->nama_odp_santai;
			$save_pt2['sn_splicer'] = $req->sn_splicer;

			DB::table('pt2_master')->where('id', $ide)->update($save_pt2);

			$id_log_dispatch = DB::table('pt2_dispatch')->insertGetId([
				'id_order'           => $po->id,
				'regu_id'            => $po->regu_id,
				'nik1'							 => session('auth')->id_user,
				'tgl_kerja_dispatch' => $po->tgl_pengerjaan,
				'Keterangan'         => 'Update Teknisi',
				'status'             => $req->status,
				'updated_by'         => $id,
			]);

			$nama_teknisi = DB::Table('1_2_employee')->where('nik', $id)->first();

			if (!empty($nama_teknisi))
			{
				$teknisi_nm = $id . '/' . $nama_teknisi->nama;
			}
			else
			{
				$teknisi_nm = $id;
			}

			if (empty($rfc))
			{
				$msg_rfc = 'Tidak Ada';
			}
			else
			{
				$msg_rfc = $rfc;
			}

			$so = null;

			$materials = json_decode($req->input('materials') );

			$get_log_material = DB::table('pt2_material')->where('pt2_id', $ide)->get();
			$get_all_core = DB::table('pt2_khs')->where([
				['satuan', 'core'],
				['terpakai', 1]
			])
			->get();

			DB::table('pt2_material')->where('pt2_id', $ide)->delete();

			foreach($materials as $v)
			{
				DB::table('pt2_material')->insert(['pt2_id' => $ide, 'id_item' => $v->id_item, 'qty' => $v->qty]);

				$find_k_core = array_search($v->id_item, array_column(json_decode(json_encode($get_all_core), TRUE), 'id_item') );

				if($find_k_core !== FALSE)
				{
					$find_k = array_search($v->id_item, array_column(json_decode(json_encode($get_log_material), TRUE), 'id_item') );

					$kurang = 0;

					if($find_k !== FALSE)
					{
						$kurang = $get_log_material[$find_k]->qty;
					}

					if( ($v->qty - $kurang) != 0)
					{
						DB::Table('pt2_core_log')->insert([
							'id_dispatch' => $id_log_dispatch,
							'id_item'     => $v->id_item,
							'qty'         => $v->qty - $kurang,
							'created_by'  => session('auth')->id_user
						]);
					}
				}
			}

			$regu = $po->regu_name;

			$tgl_lapor = $po->tgl_selesai ?? date('Y-m-d H:i:s');

			if ($req->status != "Selesai")
			{
				$order = DB::table('pt2_master')->where('id', $ide)->first();

				DB::table('pt2_master')->where('id', $ide)->Update(['status' => 'Construction']);

				$stts_psb = null;

				if ($req->status == "Kendala")
				{
					$stts_psb = 76;
					$arr_kendala['kendala_detail'] = $req->kendala_detail;

					if($req->kendala_detail == 'Tercover Odp Lain')
					{
						$arr_kendala['odp_cover_kendala'] = $req->odp_cover_kendala;
					}

					DB::table('pt2_master')->where('id', $ide)->update($arr_kendala);

					$so = 'KENDALA';

					// if ((!empty($check_po->juml_odp)) || $check_po->juml_odp == 0)
					// {
					// 	$hasil = $check_po->juml_odp + 1;

					// 	DB::table('pt2_po')
					// 		->where('id_project_Ta', '=', $order->proaktif_id)
					// 		->update(['juml_odp' => $hasil]);

					// }

					$get_tenos = explode('/', $po->odp_nama);
					$get_odp = explode('-', $get_tenos[0]);

					$terminal_delete[] = (object)['jenis_terminal' => $get_odp[0], 'koordinat' => $po->odp_koor, 'sto' => $get_odp[1], 'odc' => $get_odp[2], 'index_terminal' => $get_tenos[1] ];

					$terminal[] = (object)['jenis_terminal' => 'ODP', 'koordinat' => $po->odp_koor];

					$that_AC = new AdminController();

					$that_AC->delete_booking('PT-2', json_encode($terminal_delete) );
				}
				elseif (in_array($req->status, ['Ogp']) )
				{
					$stts_psb = 75;
				}

				//psb
				if (!empty($po->scid) || $po->scid != '')
				{
					DB::table('psb_laporan')
						->where('id_tbl_mj', $po->scid)
						->update(['psb_laporan.status_laporan' => $stts_psb]);
				}

				$message = "";

				$message .= "<b>Tanggal Laporan $req->status: </b> $tgl_lapor \n";
				$message .= "<b>Nomor RFC: </b> " . $msg_rfc . "\n";
				$message .= "<b>Id User Yang Melakukan Dispatch: </b> $teknisi_nm  \n";
				$message .= "<b>Team Regu: </b> $regu \n";
				$message .= "<b>status :</b> $req->status \n";

				if ($req->status == 'Kendala')
				{
					$message .= "<b>Detail Kendala: </b> $req->kendala_detail \n";

					if($req->odp_cover_kendala)
					{
						$message .= "<b>Odp Cover: </b> $req->odp_cover_kendala \n";
					}

					if($req->distrib_ccd)
					{
						$message .= "<b>Distribusi: </b> $req->distrib_ccd \n";
					}
				}

				if ($po->pelanggan_koor)
				{
					$message .= "<b>Koordinat Pelanggan</b> : $po->pelanggan_koor\n";
				}

				$message .= "<b>Nama Project: </b> $req->project_name \n";
				$message .= "\n \n \n";
				$message .= "<b>Action :</b> $req->action \n";
				$message .= "\n \n \n";

				if ($po->odp_nama_before)
				{
					$message .= "<b>Nama ODP Sebelumnya</b>: $po->odp_nama_before \n";
				}

				$message .= "<b>Nama ODP :</b> $po->odp_nama \n";

				if ($po->odp_koor_before)
				{
					$message .= "<b>Koordinat ODP Sebelumnya</b>: $po->odp_koor_before \n";
				}

				if($po->nomor_sc_log)
				{
					$message .= "<b>Nomor SC yang Ikut</b>: $po->nomor_sc_log \n";
				}

				$message .= "<b>Koordinat ODP :</b> $req->koordinat_odp \n";
				$message .= "<b>Catatan :</b> $req->catatan \n";

				$matterial = DB::select("SELECT (pm.id_item) as material_id, pk.satuan, pm.qty, pk.uraian
				FROM pt2_material pm
				LEFT JOIN pt2_khs pk ON pm.id_item = pk.id_item
				WHERE pt2_id = $ide");

				if ($matterial)
				{
					$message .= "<b>Laporan Material dan Jasa</b> \n";
					foreach ($matterial as $pesan)
					{
						$message .= "\n \n";
						$message .= "$pesan->material_id ($pesan->qty <b>$pesan->satuan</b>) \n *$pesan->uraian";
					}
				}
				else
				{
					$message .= "<b> Material Tidak Di input</b>";
				}

				$message .= '#' . preg_replace('/[^a-z0-9]/i', '', $po->regu_name);

				try
				{
					Telegram::sendMessage([
						'chat_id' => AdminModel::telegramId($order->id) ,
						'parse_mode' => 'html',
						'text' => "$message"
					]);
				}
				catch(\Symfony\Component\HttpFoundation\File\Exception\FileException $e)
				{
					$urgent_msg['msg'] = ['type' => 'danger', 'text' => 'Laporan Tidak Terkirim, Lapor Grup Agar selalu up-to-date!'];
				}

				$phototelegram_undone = $this->phototelegram_undone;

				for ($i = 0;$i < count($phototelegram_undone);$i++)
				{
					$public = public_path() . '/upload/pt_2_3/teknisi/' . $ide;
					$foto = "$public/$phototelegram_undone[$i].jpg";

					if (file_exists($foto))
					{
						if (!in_array($phototelegram_undone[$i], ['Port_Feeder', 'Port_Distribusi']))
						{
							$file_text = "$public/$phototelegram_undone[$i]-catatan.txt";

							if (file_exists($file_text))
							{
								$note = File::get("$public/$phototelegram_undone[$i]-catatan.txt");
							}
							else
							{
								$note = '';
							}
						}
						else
						{
							$file_port = "$public/$phototelegram_undone[$i]-port.txt";
							$file_panel = "$public/$phototelegram_undone[$i]-panel.txt";

							if (file_exists($file_port))
							{
								$note_port = File::get("$public/$phototelegram_undone[$i]-port.txt");
							}
							else
							{
								$note_port = '\'Tidak Ada\'';
							}

							if (file_exists($file_panel))
							{
								$note_panel = File::get("$public/$phototelegram_undone[$i]-panel.txt");
							}
							else
							{
								$note_panel = '\'Tidak Ada\'';
							}

							$note = "Foto Port Distribusi Panel $note_panel, Port $note_port";
						}
					}

					$photo_caption = str_replace('_', ' ', $phototelegram_undone[$i]);

					if(file_exists($foto) )
					{
						try
						{
							Telegram::sendPhoto([
								'chat_id' => AdminModel::telegramId($order->id) ,
								'caption' => "Label $photo_caption \n $note \n #" . preg_replace('/[^a-z0-9]/i', '', $regu),
								'photo' => $foto
							]);
						}
						catch(\Symfony\Component\HttpFoundation\File\Exception\FileException $e)
						{
							$urgent_msg['foto'] = ['type' => 'danger', 'text' => 'Foto Tidak Terkirim, Lapor Grup Agar selalu up-to-date!'];
						}
					}
				}
			}
			//update tgl selesai kalau status laporan teknisi selesai
			if ($req->status == "Selesai")
			{
				DB::table('pt2_master')->where('id', $ide)->update(['rfc_key' => $rfc, 'status' => 'Completed']);

				if (!empty($po->odp_cover))
				{
					DB::table('pt2_master')
					->where('odp_nama', $po->odp_cover)
					->update(['status' => 'Completed', 'lt_status' => 'Selesai']);
				}

				$so = 'DONE';

				//update materials

				DB::table('pt2_material')
				->where('pt2_id', $ide)->delete();

				foreach ($materials as $m)
				{
					DB::table('pt2_material')->insert(['pt2_id' => $ide, 'id_item' => $m->id_item, 'qty' => $m->qty]);

					$array = $req->rfc_number;

					if ($array)
					{
						DB::table('maintenance_saldo_rfc')->whereIn('rfc', $req->rfc_number)
							->delete();

						$data_saldo_rfc = DB::table('pt2_master')->leftjoin('regu', 'pt2_master.regu_id', 'regu.id_regu')
							->where('id', $ide)->first();

						foreach ($array as $value)
						{
							$data = DB::table('alista_material_keluar')->where([['no_rfc', $value], ['id_barang', $m
								->id_item]])
								->first();

							if ($data)
							{
								$amk = DB::TABLE('alista_material_keluar')->where('no_rfc', $value)->first();
								DB::table('maintenance_saldo_rfc')
								->insert(['id_pengeluaran' => $amk->alista_id, 'rfc' => $data_saldo_rfc->rfc_key, 'regu_id' => $data_saldo_rfc->regu_id, 'regu_name' => $data_saldo_rfc->regu_name, 'nik1' => $data_saldo_rfc->nik1, 'nik2' => $data_saldo_rfc->nik2, 'niktl' => $data_saldo_rfc->TL, 'created_at' => date('Y-m-d H:i:s') , 'created_by' => $id, 'value' => '-' . $m->qty, 'id_item' => $m->id_item, 'action' => 2, 'bantu' => "$data_saldo_rfc->rfc_key#$m->id_item", ]);
							}
						}
					}
				}

				// if( ($po->lanjut_PT1 === 0 || is_null($po->lanjut_PT1) ) && $po->GOLIVE == 1 )
				// {
				// 	$post = [
				// 		'order' => json_encode(explode(',', $po->nomor_sc_log)),
				// 	];

				// 	$curl = curl_init();

				// 	//wo nya harus golive di jointer
				// 	curl_setopt_array($curl, [
				// 		CURLOPT_URL => 'https://ops.tomman.app/api/update_jointer',
				// 		CURLOPT_RETURNTRANSFER => true,
				// 		CURLOPT_POSTFIELDS => $post,
				// 		CURLOPT_ENCODING => '',
				// 		CURLOPT_MAXREDIRS => 10,
				// 		CURLOPT_TIMEOUT => 0,
				// 		CURLOPT_FOLLOWLOCATION => true,
				// 		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				// 		CURLOPT_CUSTOMREQUEST => 'POST',
				// 	]);

				// 	$response = curl_exec($curl);
				// 	curl_close($curl);

				// 	$response =json_decode($response);
				// 	// dd($response);

				// 	if($response->status == true)
				// 	{
				// 		DB::table('pt2_master')
				// 		->where('id', $po->id)
				// 		->update([
				// 			'lanjut_PT1' => 1
				// 		]);
				// 	}
				// }

				$message = '';
				$message .= "<b>Laporan Teknisi PT2</b> \n";
				$message .= "\n \n \n";
				$message .= "<b>Tanggal Laporan $req->status: </b> $tgl_lapor \n";
				$message .= "<b>Nomor RFC: </b> " . $msg_rfc . "\n";
				$message .= "<b>Id User Yang Melakukan Dispatch: </b> $teknisi_nm  \n";
				$message .= "<b>Team Regu: </b> $regu \n";
				$message .= "<b>status :</b> $req->status \n";

				if ($po->pelanggan_koor)
				{
					$message .= "<b>Koordinat Pelanggan</b> : $po->pelanggan_koor\n";
				}

				$message .= "<b>Nama Project: </b> $req->project_name \n";
				$message .= "\n \n \n";
				$message .= "<b>Action :</b> $req->action \n";
				$message .= "\n \n \n";

				if ($po->odp_nama_before)
				{
					$message .= "<b>Nama ODP Sebelumnya</b>: $po->odp_nama_before \n";
				}

				$message .= "<b>Nama ODP :</b> $po->odp_nama \n";

				if ($po->odp_koor_before)
				{
					$message .= "<b>Koordinat ODP Sebelumnya</b>: $po->odp_koor_before \n";
				}

				if($po->nomor_sc_log)
				{
					$message .= "<b>Nomor SC yang Ikut</b>: $po->nomor_sc_log \n";
				}

				$message .= "<b>Koordinat ODP :</b> $req->koordinat_odp \n";
				$message .= "<b>Catatan :</b> $req->catatan \n";
				$message .= "\n \n \n";

				$matterial = DB::select("SELECT (pm.id_item) as material_id, pk.satuan, pm.qty, pk.uraian
        FROM pt2_material pm
				LEFT JOIN pt2_khs pk ON pm.id_item = pk.id_item
				WHERE pt2_id = $ide");

				if ($matterial)
				{
					$message .= "<b>Laporan Material dan Jasa</b> \n";
					foreach ($matterial as $pesan)
					{
						$message .= "\n \n";
						$message .= "$pesan->material_id ($pesan->qty <b>$pesan->satuan</b>) \n *$pesan->uraian";
					}
				}
				else
				{
					$message .= "<b> Material Tidak Di input</b>";
				}

				$message .= "\n #" . preg_replace('/[^a-z0-9]/i', '', $po->regu_name);

				try
				{
					Telegram::sendMessage([
						'chat_id' => AdminModel::telegramId($po->id) ,
						'parse_mode' => 'html',
						'text' => "$message"
					]);
				}
				catch(\Symfony\Component\HttpFoundation\File\Exception\FileException $e)
				{
					$urgent_msg['msg'] = ['type' => 'danger', 'text' => 'Foto Tidak Terkirim, Lapor Grup Agar selalu up-to-date!'];
				}

				// $botToken="606798799:AAGGB7ru3x-9WWLgSTomb2Y9EYYFFLNRTcs";
				// $website="https://api.telegram.org/bot".$botToken;
				// $params=[
				//   'chat_id'=> AdminModel::telegramId($po->id),
				//   'parse_mode' => 'html',
				//   'text'=> $message,
				// ];
				// $ch = curl_init();
				// $optArray = array(
				//   CURLOPT_URL => $website.'/sendMessage',
				//   CURLOPT_RETURNTRANSFER => 1,
				//   CURLOPT_POST => 1,
				//   CURLOPT_POSTFIELDS => $params,
				// );
				// curl_setopt_array($ch, $optArray);
				// $result = curl_exec($ch);
				// curl_close($ch);
				$photo_teleee = $this->photo_submit_tele;
				// $tc = new TeknisiController();
				// $photo_teleee = $tc->photo;
				$raw_pphoto = array_chunk($photo_teleee, 9);
				foreach ($raw_pphoto as $pphoto) {
					for ($i = 0;$i < count($pphoto); $i++)
					{
						$public = public_path() . '/upload/pt_2_3/teknisi/' . $ide;
						$foto = "$public/$pphoto[$i].jpg";

						if (file_exists($foto))
						{
							if (!in_array($pphoto[$i], ['Port_Feeder', 'Port_Distribusi']))
							{
								$file_text = "$public/$pphoto[$i]-catatan.txt";

								if (file_exists($file_text))
								{
									$note = File::get("$public/$pphoto[$i]-catatan.txt");
								}
								else
								{
									$note = '';
								}

							}
							else
							{
								$file_port = "$public/$pphoto[$i]-port.txt";
								$file_panel = "$public/$pphoto[$i]-panel.txt";

								if (file_exists($file_port))
								{
									$note_port = File::get("$public/$pphoto[$i]-port.txt");
								}
								else
								{
									$note_port = '\'Tidak Ada\'';
								}

								if (file_exists($file_panel))
								{
									$note_panel = File::get("$public/$pphoto[$i]-panel.txt");
								}
								else
								{
									$note_panel = '\'Tidak Ada\'';
								}

								$note = "Foto Feeder Panel $note_panel, Port $note_port";
							}
						}

						$photo_caption = str_replace('_', ' ', $pphoto[$i]);

						if (file_exists($foto))
						{
							try
							{
								Telegram::sendPhoto([
									'chat_id' => AdminModel::telegramId($po->id) ,
									'caption' => "Label $photo_caption \n $note \n #" . preg_replace('/[^a-z0-9]/i', '', $regu),
									'photo' => $foto
								]);

							}
							catch(\Symfony\Component\HttpFoundation\File\Exception\FileException $e)
							{
								$urgent_msg['foto'] = ['type' => 'danger', 'text' => 'Foto Tidak Terkirim, Lapor Grup Agar selalu up-to-date!'];
							}
						}
					}
				}

				// try
				// {
					// Telegram::sendmessage([
					// 	'chat_id' => AdminModel::telegramId($po->id),
					// 	'parse_mode' => 'html',
					// 	'text' => "Untuk mendapatkan semua photo, klik <a href=\"https://pt2.bjm.tomman.app/Download/file/$po->id\">disini</a>"
					// ]);
				// }
				// catch(\Symfony\Component\HttpFoundation\File\Exception\FileException $e)
				// {
				// 	$urgent_msg['foto'] = ['type' => 'danger', 'text' => 'Foto Tidak Terkirim, Lapor Grup Agar selalu up-to-date!'];
				// }

				$check = DB::table('psb_laporan_log')->where('Ndem', '=', $po->Ndem)->first();

				if (!isset($check) )
				{
					$id_log = DB::table('psb_laporan')->insertGetId(['created_at' => date('Y-m-d H:i:s') , 'created_by' => $id, 'catatan' => $req->catatan, 'kordinat_pelanggan' => $req->koordinat_odp, 'nama_odp' => $po->odp_nama, 'kordinat_odp' => $po->odp_koor, ]);

					DB::table('psb_laporan_log')
						->insert(['id_regu' => $po->regu_id, 'Ndem' => $po->Ndem, 'psb_laporan_id' => $id_log, 'catatan' => $req->catatan, 'created_by' => $id, 'created_at' => date('Y-m-d H:i:s') , ]);
				}
				else
				{
					DB::table('psb_laporan')->where('id', '=', $check->psb_laporan_id)
						->update(['modified_at' => date('Y-m-d H:i:s') , 'modified_by' => $id, 'catatan' => $req->catatan, 'kordinat_pelanggan' => $req->koordinat_odp, 'nama_odp' => $po->odp_nama, 'kordinat_odp' => $po->odp_koor, ]);

					DB::table('psb_laporan_log')
						->insert(['id_regu' => $po->regu_id, 'Ndem' => $po->Ndem, 'psb_laporan_id' => $check->psb_laporan_id, 'catatan' => $req->catatan, 'created_by' => $id, 'created_at' => date('Y-m-d H:i:s') , ]);
				}
			}

			if($po->id_nog_m != 0)
			{
				DB::table('nog_master')->where('id', $po->id_nog_m)->update([
					'status_order' => $so
				]);
			}

		});
		return $urgent_msg;
	}

	private function handleFileUpload($req, $ide)
	{
		$that = new TeknisiController();

		foreach ($that->photo as $name)
		{
			$input = 'photo_' . $name;
			$path = public_path() . '/upload/pt_2_3/teknisi/' . $ide;

			if (!file_exists($path))
			{
				if (!mkdir($path, 0770, true))
				{
					return 'gagal menyiapkan folder foto ODP';
				}
			}

			if (!in_array($name, ['Port_Feeder', 'Port_Distribusi']))
			{
				$kmntr = 'catatan_' . $name;
				$komentar = $req->$kmntr;

				if ($komentar != '')
				{
					file_put_contents("$path/$name-catatan.txt", $komentar);
				}

			}
			else
			{
				$panel = 'panel_' . $name;
				$port = 'port_' . $name;
				$panel_req = $req->$panel;
				$port_req = $req->$port;

				if ($panel_req != '')
				{
					file_put_contents("$path/$name-panel.txt", $panel_req);
				}

				if ($port_req != '')
				{
					file_put_contents("$path/$name-port.txt", $port_req);
				}
			}

			if ($req->hasFile($input))
			{
				$file = $req->file($input);
				// dd($file);
				$dokumen_pdf = $req->upload_rfc;
				$ext = 'jpg';

				try
				{
					$moved = $file->move("$path", "$name.$ext");
					$img = new \Imagick($moved->getRealPath());
					$img->scaleImage(100, 150, true);
					$img->writeImage("$path/$name-th.$ext");
				}
				catch(\Symfony\Component\HttpFoundation\File\Exception\FileException $e)
				{
					return 'gagal menyimpan foto ODP ' . $name;
				}
			}

		}

	}

	// private function handleFileUpload_rfc($req, $ide)
	// {
	//   $path = public_path().'/upload/pt_2_3/teknisi/'.$ide;
	//   if (!file_exists($path)) {
	//     if (!mkdir($path, 0770, true))
	//       return 'gagal menyiapkan folder foto ODP';
	//   }
	//   $dokumen_pdf = $req->upload_rfc;
	//   try {
	//         // $nama = $file->getClientOriginalName();
	//     $moved_upload = $dokumen_pdf->move("$path", "file_upload_RFC.pdf");
	//   }
	//   catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
	//     return 'gagal menyimpan foto ODP '.$name;
	//   }
	// }
	public static function select_material()
	{
		return DB::select("SELECT id_item, telkom, uraian as nama_item, 0 as qty, '' as rfc, 500 as total FROM pt2_khs WHERE terpakai = 1 ");
	}

	public static function load_material($id)
	{
		// dd($id);
		return DB::table('pt2_master')
		->leftjoin('pt2_material', 'pt2_master.id', 'pt2_material.pt2_id')
		->select('pt2_material.*')
		->where('pt2_master.id', $id)->get();
	}

	public static function get_design($value)
	{
		return DB::Table('pt2_khs')
		->where([
			['terpakai', 1],
			['id_item', 'like', '%' . $value . '%']
		])
		->get();
	}

	public static function get_jointer_user()
	{
		$regu_jointer = JointerModel::get_jointer_user();

		$data = [];

		$get_all_party = DB::Table('pt2_party')->get();
		$get_all_party = json_decode(json_encode($get_all_party), TRUE);

		foreach($regu_jointer as $k => $v)
		{
			$find_used_l = array_search($v->nik, array_column($get_all_party, 'leader_nik') );
			$find_used = array_search($v->nik, array_column($get_all_party, 'unit_nik') );

			if($find_used !== FALSE || $find_used_l !== FALSE)
			{
				continue;
			}

			$data[$k]['id'] = $v->nik;
			$data[$k]['text'] = $v->nama .' ('. $v->nik .')';
		}

		return $data;
	}

	public static function save_party($req)
	{
		$get_emp = DB::table('1_2_employee')->get();

		$get_emp = array_map(function($item) {
			return (array)$item;
		}, $get_emp->toArray() );

		$find_name_k_l = array_search(session('auth')->id_user, array_column($get_emp, 'nik' ) );

		$emp_l_nama = 'Kosong_'. session('auth')->id_user;

		if($find_name_k_l !== FALSE)
		{
			$emp_l_nama = $get_emp[$find_name_k_l]['nama'];
		}

		foreach($req->unit_id as $v)
		{
			$find_name_k = array_search($v, array_column($get_emp, 'nik' ) );
			$emp_nama = 'Kosong_'.$v;

			if($find_name_k !== FALSE)
			{
				$emp_nama = $get_emp[$find_name_k]['nama'];
			}

			DB::Table('pt2_party')->insert([
				'party_name'  => $req->party_name,
				'leader_nik'  => session('auth')->id_user,
				'leader_name' => $emp_l_nama,
				'unit_nik'    => $v,
				'unit_name'   => $emp_nama,
				'created_by'  => session('auth')->id_user,
			]);

			DB::Table('pt2_party_log')->insert([
				'nik'  => $v,
				'nama' => $emp_nama
			]);
		}

		DB::Table('pt2_party_log')->insert([
			'nik'     => session('auth')->id_user,
			'kondisi' => 1,
			'nama'    => $emp_l_nama
		]);
	}

	public static function load_material_outside($jenis, $id)
	{
		switch ($jenis) {
			case 'marina':
				return DB::table('maintaince_mtr')->where('maintaince_id', $id)->get();
			break;
			case 'bts':
				$get_lop_id = DB::Table('mpromise_core')->where('ID_Sys', $id)->first();
				return DB::table('mpromise_material_instalasi')->where('lop_id', $get_lop_id->LOP_ID)->get();
			break;
			default:
				return DB::Table('pt2_master As pm1')
				->leftjoin('pt2_master As pm2', 'pm2.id_refer', '=', 'pm1.id')
				->leftjoin('pt2_material', 'pm2.id', 'pt2_material.pt2_id')
				->select('pt2_material.*')
				->where('pm1.id', $id)
				->get();
			break;

		}
	}

	public static function pickup_jointer($jenis, $id_main)
	{
		$order = [];

		$updated_by = Session::get('auth')->id_user;

		$order['nik1']         = Session::get('auth')->id_user;
		$order['jointer_name'] = Session::get('auth')->nama;
		$order['jenis_wo']     = 'PT-2 SIMPLE';

		$get_regu = DB::table('regu As r')->Where([
			['job', 'PT2'],
			['ACTIVE', 1]
		])->where(function($join) use($updated_by){
			$join->where('r.nik1', $updated_by)
			->Orwhere('r.nik2', $updated_by)
			->Orwhere('r.nik3', $updated_by)
			->Orwhere('r.nik4', $updated_by);
		})->first();

		$order['regu_id']   = $get_regu->id_regu;
		$order['nik1']      = $get_regu->nik1;
		$order['nik2']      = $get_regu->nik2;
		$order['regu_name'] = $get_regu->uraian;

		//===================================================
		if(strcasecmp($jenis, 'marina') == 0)
		{
			$check_data = DB::TABLE('pt2_master')->where('id_maint', $id_main)->first();

			$data_outside = DB::TABLE('maintaince')->where('id', $id_main)->first();

			$order['id_maint']     = $id_main;
			$order['odp_nama']     = $data_outside->nama_odp;
			$order['odp_koor']     = $data_outside->koordinat;
			$order['catatan_HD']   = $data_outside->headline;
			$order['nomor_sc']     = $data_outside->no_tiket;
			$order['sto']          = $data_outside->sto;
			$order['project_name'] = $data_outside->no_tiket;

			$get_odc = explode('/', $data_outside->nama_odp)[0];
			$get_odc = explode('-', $get_odc);
			$get_odc = 'ODC-'.$get_odc[1].'-'.$get_odc[2];
		}

		if(strcasecmp($jenis, 'bts') == 0)
		{
			$check_data = DB::TABLE('pt2_master')->where('id_bts', $id_main)->first();

			$data_outside =  DB::table('mpromise_lop As ml')
			->Leftjoin('mpromise_core As mc', 'mc.LOP_ID', '=', 'ml.id')
			->leftjoin('pt2_master As pm', 'mc.ID_Sys', '=', 'pm.id_bts')
			->select('mc.ID_Sys As id', 'ml.sto', 'ml.created_at', 'mc.Plan_Odp_Nama As nama_odp', DB::RAW("CONCAT(mc.Plan_Odp_LAT,',',mc.Plan_Odp_LON) As koordinat"), 'ml.nama_lop As nama_order' )
			->where('ID_Sys', $id_main)
			->first();

			$order['id_bts']     	 = $id_main;
			$order['odp_nama']     = $data_outside->nama_odp;
			$order['odp_koor']     = $data_outside->koordinat;
			$order['catatan_HD']   = $data_outside->nama_order;
			$order['sto']          = $data_outside->sto;
			$order['project_name'] = $data_outside->nama_order;

			$get_odc = explode('/', $data_outside->nama_odp)[0];
			$get_odc = explode('-', $get_odc);
			$get_odc = 'ODC-'.$get_odc[1].'-'.$get_odc[2];
		}

		//===================================================

		$order['odc_nama'] = $get_odc;

		$split_odc = explode('-', $order['odc_nama']);

		$get_info_odc = ReportModel::get_info_odc($split_odc[2], $split_odc[1]);

		if(count($get_info_odc) )
		{
			$order['odc_koor'] = str_replace(',', '.', $get_info_odc[0]->latitude) .','. str_replace(',', '.', $get_info_odc[0]->longitude);
		}

		$order['kategory_non_unsc'] = 1;
		$order['delete_clm']        = 0;
		$order['tgl_pengerjaan']    = date('Y-m-d H:i:s');

		return DB::transaction(function () use ($order, $updated_by, $check_data)
		{
			$id = $message = '';

			if (!isset($check_data) )
			{
				//insert
				$order['status'] = 'Construction';
				$order['tgl_buat'] = date('Y-m-d H:i:s');

				$id = DB::table('pt2_master')->insertGetId($order);

				DB::table('pt2_dispatch')->insert([
					'id_order'           => $id,
					'nik1'               => $order['nik1'],
					'keterangan'         => 'Pickup Order Jointer',
					'tgl_kerja_dispatch' => $order['tgl_pengerjaan'],
					'updated_by'         => $updated_by
				]);
			}
			else
			{
				$id = $check_data->id;

				$order['jenis_wo'] = $check_data->jenis_wo;

				if($order['jenis_wo'] == 'PT-2')
				{
					$order['jenis_wo'] = 'PT-2 SIMPLE';
				}

				if ($check_data->nik1 != Session::get('auth')->id_user)
				{
					$message .= "<b>Pergantian Regu WO</b>\n";
				}
				DB::table('pt2_master')->where('id', $check_data->id)->update($order);

				if ($check_data->lt_status == 'Kendala')
				{
					DB::table('pt2_master')->where('id', '=', $check_data->id)->update([
						'status' => 'Construction',
						'lt_status' => null
					]);
				}

				DB::table('pt2_dispatch')->insert([
					'id_order'           => $id,
					'nik1'               => $order['nik1'],
					'keterangan'         => 'Pergantian User Jointer',
					'tgl_kerja_dispatch' => $order['tgl_pengerjaan'],
					'updated_by'         => $updated_by
				]);
			}

			$telegram_data = DB::table('pt2_master')->where('id', '=', $id)->first();

			$message .= "<b>Pekerjaan Jointer</b> : $updated_by \n";
			$message .= "======================== \n";
			$message .= "<b>$telegram_data->jointer_name</b> \n";
			$message .= "<b>Tanggal Pekerjaan</b> : ".$order['tgl_pengerjaan']."\n";
			$message .= "<b>Nama Project</b> : $telegram_data->project_name\n";

			$message .= "<b>Nama Jointer</b> : $telegram_data->jointer_name\n";
			$message .= "<b>Catatan</b> : $telegram_data->catatan_HD\n \n \n \n";

			$message .= "<b>Nama ODP</b> : $telegram_data->odp_nama\n";

			$message .= "<b>Koordinat ODP</b> \n";
			$message .= "<a href=\"maps.google.com/?q=$telegram_data->odp_koor\">$telegram_data->odp_koor</a>\n";

			try
			{
				Telegram::sendMessage([
					'chat_id' => AdminModel::telegramId($id) ,
					'parse_mode' => 'html',
					'text' => "$message"
				]);
			}
			catch(\Symfony\Component\HttpFoundation\File\Exception\FileException $e)
			{
			}
			return $id;
		});
	}

	public static function log_teknisi($id)
	{
		return DB::table('pt2_dispatch As pd')
		->leftjoin('pt2_core_log As pcl', 'pcl.id_dispatch', '=', 'pd.id_pt2')
		->leftjoin('1_2_employee As emp', 'pd.updated_by', '=', 'emp.nik')
		->select('pd.*', 'emp.nama', 'pcl.id_item', 'pcl.qty')
		->where('id_order', $id)
		->OrderBy('id_pt2', 'DESC')
		->get();
	}
}