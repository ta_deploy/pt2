<?php
namespace App\DA;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\LoginController;

class LoginModel
{
	public function login($user, $pass)
	{
		return DB::select('SELECT u.id_user, u.password, u.id_karyawan, k.nama, r.id_regu, u.pt2_level, u.psb_remember_token, u.maintenance_level
		FROM user u
		LEFT JOIN 1_2_employee k ON u.id_karyawan = k.nik
		LEFT JOIN regu r ON (r.nik1 = u.id_karyawan OR r.nik2 = u.id_karyawan)
		WHERE id_user = ? AND password = MD5(?)
		GROUP BY u.id_user', [$user, $pass]);
	}

	public function remembertoke($localUser, $token)
	{
		return DB::table('user')->where('id_user', $localUser->id_user)
    ->update(['psb_remember_token' => $token]);
	}

  public static function check_absensi()
  {
    return DB::table('absen As a')
    ->leftjoin('regu As b', function($join){
			$join->on('b.nik1', '=', 'a.nik')
			->OrOn('b.nik2', '=', 'a.nik');
		})
    ->Select('a.*', 'b.TL')
    ->where([
      ['a.nik', session('auth')->id_user],
      ['a.divisi', '=', 'PT2'],
      [DB::RAW("DATE_FORMAT(a.date_created, '%Y-%m-%d')"), date('Y-m-d')]
    ])
    ->first();
  }

  public static function countMenu()
  {
    $data = new \stdClass();

		$matrix = DB::table('pt2_master')->where('status', '=', 'BisaPT2')
    ->get();

		$data->bank_pt2 = count($matrix);

		$matrix = ReportModel::list_nog();
		$data_pt2_psb = ReportModel::list_pt2_psb();


    $nik_raw = DB::table('pt2_party')->Where('leader_nik', session('auth')->id_user)->orWhere('unit_nik', session('auth')->id_user)->get();

    $nik = [];

    foreach($nik_raw as $v)
    {
      $nik[$v->leader_nik] = $v->leader_nik;

      if($v->status == 1)
      {
        $nik[$v->unit_nik] = $v->unit_nik;
      }
    }

		$data->order_outside_pt2 = count($matrix) + count($data_pt2_psb);

		$data_golive_waiting_list = AdminModel::finish_list('go_live');

    $data->count_golive_wl = count($data_golive_waiting_list);

		return $data;
  }

	public static function absen_teknsi($req){
    $session = session('auth');
    $botToken = "606798799:AAGGB7ru3x-9WWLgSTomb2Y9EYYFFLNRTcs";
    $website = "https://api.telegram.org/bot".$botToken;

		DB::table('absen')->insert([
      'nik' => Session('auth')->id_user,
      'divisi' => 'PT2',
    ]);

    $check_teknisi = DB::table('user')
    ->leftjoin('1_2_employee', 'user.id_user', '1_2_employee.nik')
    ->select('user.*', '1_2_employee.nama')
    ->where('id_user', Session('auth')->id_user)->first();

    $teknisi_nm = $check_teknisi->nama ? "$check_teknisi->id_user ($check_teknisi->nama)" : $check_teknisi->id_user;

		$message = "Teknisi $teknisi_nm absen! Segera ADMIN melakukan Verifikasi \n";
    $list_Work = DB::Table('pt2_master')
    ->Where('delete_clm', 0)
    ->where(function($j) use ($check_teknisi){
      $j->where('nik1', $check_teknisi->id_user)
      ->orWhere('nik2', $check_teknisi->id_user);
    })
    ->where(function($join){
			$join->whereNull('lt_status')
			->OrwhereNotIn('lt_status', ['Selesai', 'Kendala']);
		})
    ->get();

    if($list_Work)
    {
      $message .= "Berikut List Pekerjaan Yang Berjalan!\n";
    }

    foreach ($list_Work as $v) {
      $message .= "$v->project_name\n";
      $message .= "<b>ODP</b> : $v->odp_nama\n";
      $message .= "<b>Status</b> : $v->lt_status\n";
      $message .= "<b>Terakhir Update</b> : $v->modified_at\n \n";
    }

    $message .= '#TEKNISI'. preg_replace('/[\/\s+()\&%#\$]/', '', $teknisi_nm);
    $chat_id = '-1001114802851';
    // $chat_id = '519446576';

    $params=[
      'chat_id' => $chat_id,
      'parse_mode' => 'html',
      'text' => "$message",
    ];
    $ch = curl_init();
    $optArray = array(
      CURLOPT_URL => $website.'/sendMessage',
      CURLOPT_RETURNTRANSFER => 1,
      CURLOPT_POST => 1,
      CURLOPT_POSTFIELDS => $params,
    );
    curl_setopt_array($ch, $optArray);
    $result = curl_exec($ch);
    curl_close($ch);

    $that = new LoginModel();

    $that->handleFileabsensi($req, $session->id_user);

    $LoginController = new LoginController();
    foreach ($LoginController->foto as $pht) {
      $public = public_path() . '/upload/absensi/' . $session->id_user;
      $file = $public . "/" . $pht . ".jpg";
      $photo_caption = str_replace('_', ' ', $pht);
      $params_photo=[
        'chat_id' => $chat_id,
        'caption' => "Label $photo_caption \n #TEKNISI".preg_replace('/[\/\s+()\&%#\$]/', '', $teknisi_nm)."\n",
        'photo' => new \CURLFile($file),
      ];
      $ch = curl_init();
      $optArray_photo = array(
        CURLOPT_URL => $website.'/sendPhoto',
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_POST => 1,
        CURLOPT_POSTFIELDS => $params_photo,
        CURLOPT_HTTPHEADER => ['Content-Type: multipart/form-data'],
      );
      curl_setopt_array($ch, $optArray_photo);
      $result = curl_exec($ch);
      curl_close($ch);
    }
  }

  private function handleFileabsensi($req, $id)
  {
    $LoginController = new LoginController();
    foreach ($LoginController->foto as $name) {
      $input = 'photo-' . $name;
      $path = public_path() . '/upload/absensi/' . $id;

      if ($req->hasFile($input)) {
        $file = $req->file($input);
        $ext = 'jpg';
        try {
          $moved = $file->move("$path", "$name.$ext");

          $img = new \Imagick($moved->getRealPath());

          $img->scaleImage(100, 150, true);
          $img->writeImage("$path/$name-th.$ext");
        } catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
          return 'gagal menyimpan foto evidence ' . $name;
        }
      }
    }
  }

  public static function proses_absen($jns, $id)
  {
    $get_nik = DB::table('absen')->where([
      ['absen_id', $id],
      ['divisi', '=', 'PT2'],
    ])
    ->first();

    $check_teknisi = DB::table('user')
    ->leftjoin('1_2_employee', 'user.id_user', '1_2_employee.nik')
    ->select('user.*', '1_2_employee.nama')
    ->where('id_user', $get_nik->nik)
    ->first();

    $teknisi_absen = $check_teknisi->nama ? "$check_teknisi->id_user ($check_teknisi->nama)" : $check_teknisi->id_user;

    $check_teknisi = DB::table('user')
    ->leftjoin('1_2_employee', 'user.id_user', '1_2_employee.nik')
    ->select('user.*', '1_2_employee.nama')
    ->where('id_user', Session('auth')->id_user)
    ->first();

    $approval_by = $check_teknisi->nama ? "$check_teknisi->id_user ($check_teknisi->nama)" : $check_teknisi->id_user;

    if($jns == 'appr')
    {
      DB::table('absen')->where([
        ['absen_id', $id],
        ['divisi', '=', 'PT2'],
      ])->update([
        'date_approval' => date('Y-m-d H:i:s'),
        'approval' => 1,
        'approve_by' => Session('auth')->id_user
      ]);

      $message = "Teknisi $teknisi_absen sudah diverifikasi oleh <b>$approval_by</b>, selamat bekerja! \n";

      $list_Work = DB::Table('pt2_master')
      ->where(function($j) use ($check_teknisi){
        $j->where('nik1', $check_teknisi->id_user)
        ->orWhere('nik2', $check_teknisi->id_user);
      })
      ->where(function($join){
        $join->whereNull('lt_status')
        ->OrwhereNotIn('lt_status', ['Selesai', 'Kendala']);
      })
      ->get();

      if($list_Work)
      {
        $message .= "Berikut List Pekerjaan Yang Berjalan!\n";
      }

      foreach ($list_Work as $v) {
        $message .= "$v->project_name\n";
        $message .= "<b>ODP</b> : $v->odp_nama\n";
        $message .= "<b>Status</b> : $v->lt_status\n";
        $message .= "<b>Terakhir Update</b> : $v->modified_at\n";
      }

      $message .= '#TEKNISI'. preg_replace('/[\/\s+()\&%#\$]/', '', $teknisi_absen);
    }
    else
    {
      DB::table('absen')->where([
        ['absen_id', $id],
        ['divisi', '=', 'PT2'],
        [DB::RAW(" DATE_FORMAT(date_created, '%Y-%m-%d')"), date('Y-m-d')]
      ])->delete();
      $message = "Teknisi $teknisi_absen sudah Dihapus! \n";
    }

    $botToken="606798799:AAGGB7ru3x-9WWLgSTomb2Y9EYYFFLNRTcs";
    $website="https://api.telegram.org/bot".$botToken;

    $params=[
      // 'chat_id' => '519446576',
      'chat_id' => '-1001114802851',
      'parse_mode' => 'html',
      'text' => "$message",
    ];
    $ch = curl_init();
    $optArray = array(
      CURLOPT_URL => $website.'/sendMessage',
      CURLOPT_RETURNTRANSFER => 1,
      CURLOPT_POST => 1,
      CURLOPT_POSTFIELDS => $params,
    );
    curl_setopt_array($ch, $optArray);
    $result = curl_exec($ch);
    curl_close($ch);
  }
}