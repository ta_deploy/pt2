<?php
namespace App\DA;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

date_default_timezone_set('Asia/Makassar');

class RekapPoModel
{
	public static function report_po($month)
	{
		// if($month1 == date('Y-m-d') || $month2 == date('Y-m-d H:i:s')){
		//   return DB::select('SELECT pt2_po.*,
		//     (SELECT count("proaktif_id") FROM pt2_master where delete_clm = 0  AND proaktif_id = id_project_Ta)as odp_namnya
		//     FROM pt2_po
		//     WHERE pt2_po.juml_odp != 0 GROUP BY pt2_po.id_project_Ta ORDER BY pt2_po.insert_by ASC ');
		// }else{
		//   return DB::select('SELECT pt2_po.*,
		//     (SELECT count("proaktif_id") FROM pt2_master where delete_clm = 0  AND proaktif_id = id_project_Ta)as odp_namnya
		//     FROM pt2_po
		//     WHERE pt2_po.insert_by BETWEEN "'. $month1.'" AND "'. $month2.'" GROUP BY pt2_po.id_project_Ta ORDER BY pt2_po.insert_by ASC ');
		// }

		if($month)
		{
			$sql = "DATE_FORMAT(pt2_po.insert_by, '%Y-%m') = $month";
		}
		else
		{
			$sql = "pt2_po.juml_odp != 0";
		}

		return DB::select("SELECT pt2_po.*,
		SUM(CASE WHEN delete_clm = 0 THEN 1 ELSE 0 END )AS odp_namnya
		FROM pt2_po LEFT JOIN pt2_master ON pt2_master.proaktif_id = pt2_po.id_project_Ta
		WHERE $sql GROUP BY pt2_po.id_project_Ta ORDER BY pt2_po.insert_by ASC");
	}

	public static function non_report_po()
	{
		return DB::SELECT('SELECT * from pt2_master LEFT JOIN pt2_po on pt2_master.proaktif_id = pt2_po.id_project_Ta where delete_clm = 0  AND pt2_po.id_project_Ta IS null ORDER BY sto ASC, status DESC');
	}

	public static function simpan_po($req)
	{
		//$this->POExcelUpload($id);
		$date = date('Y-m-d H:i:s');
		DB::table('pt2_po')->insert(['no_po' => $req->nomor, 'id_project_Ta' => str_replace(',', '', $req->id_project) , 'juml_odp' => $req->juml_odp, 'nilai_po' => $req->po_val, 'project_nama' => $req->nama_project, 'insert_by' => $date]);
	}

	public static function show_edit_po($id)
	{
		return DB::table('pt2_po')->where('id_tes', '=', $id)->first();
	}

	public static function show_my_list_po($project_ta)
	{
		return DB::table('pt2_master')
		->where([
			['proaktif_id', '=', $project_ta],
			['delete_clm', '=', 0]
		])->Orderby('status', 'Desc')
			->get();
	}

	public static function update_po_save($req, $id)
	{
		DB::table('pt2_po')->where('id_tes', '=', $id)->update(['no_po' => $req->nomor, 'id_project_Ta' => $req->id_project, 'juml_odp' => $req->juml_odp, 'nilai_po' => $req->po_val, 'project_nama' => $req->nama_project]);
	}

	public static function update_po_item($list, $id)
	{
		$jumlah = DB::table('pt2_po')->where('id_tes', '=', $id)->first();

		DB::table('pt2_master')->leftjoin('pt2_po', 'pt2_po.id_project_Ta', '=', 'pt2_master.proaktif_id')
		->where('pt2_master.proaktif_id', '=', $list)->update(['pt2_po.juml_odp' => 1 + $jumlah->juml_odp, 'pt2_master.proaktif_id' => '']);
	}

	public static function delete_po($id)
	{
		$po = DB::table('pt2_po')->leftjoin('pt2_master', 'pt2_master.proaktif_id', '=', 'pt2_po.id_project_Ta')
			->where('pt2_master.id', '=', $id)->first();

		DB::table('pt2_po')->leftjoin('pt2_master', 'pt2_master.proaktif_id', '=', 'pt2_po.id_project_Ta')
		->where('pt2_master.id', '=', $id)->update(['juml_odp' => $po->juml_odp + 1, 'proaktif_id' => '']);
	}

	public static function value_search($search)
	{
		return DB::table('deployer')->where('project_name', 'like', '%' . $search . '%')->get();
	}
}