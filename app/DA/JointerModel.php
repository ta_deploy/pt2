<?php
namespace App\DA;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

date_default_timezone_set("Asia/Makassar");

class JointerModel
{
	public static function get_jointer_user($mitra = 'All')
	{
		$regu_jointer = DB::table('user As u')
		->Leftjoin('1_2_employee As emp', 'u.id_user', '=', 'emp.nik')
		->select('emp.*')
		->where([
			['u.is_jointer', 1],
			['emp.ACTIVE', 1],
			['emp.nik', '!=', session('auth')->id_user],
		])
		->WhereNotNull('emp.nik');

		if(strcasecmp($mitra, 'All') != 0)
		{
			$regu_jointer->where('emp.mitra_amija', 'like', '%'.$mitra.'%');
		}

		$regu_jointer = $regu_jointer->get();

		$data = [];

		$get_all_party = DB::Table('pt2_party')->get();
		$get_all_party = json_decode(json_encode($get_all_party), TRUE);

		foreach($regu_jointer as $k => $v)
		{
			$find_used_l = array_search($v->nik, array_column($get_all_party, 'leader_nik') );
			$find_used = array_search($v->nik, array_column($get_all_party, 'unit_nik') );

			if(($find_used !== FALSE || $find_used_l !== FALSE) && session('auth')->pt2_level != 0 || $find_used !== FALSE && $get_all_party[$find_used]['leader_nik'] != session('auth')->id_user)
			{
				continue;
			}

			$data[$k]['id'] = $v->nik;
			$data[$k]['text'] = $v->nama .' ('. $v->nik .')';
		}

		return $data;
	}

	public static function save_party($req)
	{
		$get_emp = DB::table('1_2_employee')->get();

		$get_emp = array_map(function($item) {
			return (array)$item;
		}, $get_emp->toArray() );

		$find_name_k_l = array_search(session('auth')->id_user, array_column($get_emp, 'nik' ) );

		$emp_l_nama = 'Kosong_'. session('auth')->id_user;

		if($find_name_k_l !== FALSE)
		{
			$emp_l_nama = $get_emp[$find_name_k_l]['nama'];
		}

		DB::table('regu')->insert([
			'uraian'   => $req->party_name,
			'nik1'     => session('auth')->id_user,
			'job'      => 'PT2',
			'mitra'    => session('auth')->mitra,
			'TL'       => session('auth')->atasan_1,
			'sto'      => session('auth')->sto,
			'ACTIVE'   => 1
		]);

		$field_nik = [];
		$num_nik = 2;

		foreach($req->unit_id as $k => $v)
		{
			$find_name_k = array_search($v, array_column($get_emp, 'nik' ) );
			$emp_nama = 'Kosong_'.$v;

			if($find_name_k !== FALSE)
			{
				$emp_nama = $get_emp[$find_name_k]['nama'];
			}

			$field_nik[$k]['nik'.$num_nik] = $v;
			if($num_nik == 2)
			{
				$field_nik[$k]['nama'.$num_nik] = $emp_nama;
			}
			++$num_nik;

			DB::Table('pt2_party')->insert([
				'party_name'  => $req->party_name,
				'leader_nik'  => session('auth')->id_user,
				'leader_name' => $emp_l_nama,
				'unit_nik'    => $v,
				'unit_name'   => $emp_nama,
				'created_by'  => session('auth')->id_user,
			]);

			DB::Table('pt2_party_log')->insert([
				'nik'  => $v,
				'nama' => $emp_nama
			]);
		}

		foreach($field_nik as $v)
		{
			DB::table('regu')->where([
				['uraian', $req->party_name],
				['nik1', session('auth')->id_user]
			])
			->update($v);
		}

		DB::Table('pt2_party_log')->insert([
			'nik'     => session('auth')->id_user,
			'kondisi' => 1,
			'nama'    => $emp_l_nama
		]);
	}

	public static function update_party($req)
	{
		$get_emp = DB::table('1_2_employee')->get();

		$get_emp = array_map(function($item) {
			return (array)$item;
		}, $get_emp->toArray() );

		$find_name_k_l = array_search(session('auth')->id_user, array_column($get_emp, 'nik' ) );

		$emp_l_nama = 'Kosong_'. session('auth')->id_user;

		if($find_name_k_l !== FALSE)
		{
			$emp_l_nama = $get_emp[$find_name_k_l]['nama'];
		}

		$load_party = DB::Table('pt2_party')->were('leader_nik', session('auth')->id_user)->get();
		$load_party = json_decode(json_encode($load_party), TRUE);

		foreach($req->unit_id as $k => &$v)
		{
			$find_k = array_search($v, array_column($load_party, 'unit_nik') );

			if($find_k !== FALSE)
			{
				unset($req->unit_id[$k]);
			}
		}

		unset($v);

		foreach($req->unit_id as $v)
		{
			$find_name_k = array_search($v, array_column($get_emp, 'nik' ) );
			$emp_nama = 'Kosong_'.$v;

			if($find_name_k !== FALSE)
			{
				$emp_nama = $get_emp[$find_name_k]['nama'];
			}

			DB::Table('pt2_party')->insert([
				'party_name'  => $req->party_name,
				'leader_nik'  => session('auth')->id_user,
				'leader_name' => $emp_l_nama,
				'unit_nik'    => $v,
				'unit_name'   => $emp_nama,
				'created_by'  => session('auth')->id_user,
			]);

			DB::Table('pt2_party_log')->insert([
				'nik'  => $v,
				'nama' => $emp_nama
			]);
		}

		DB::Table('pt2_party_log')->insert([
			'nik'     => session('auth')->id_user,
			'kondisi' => 1,
			'nama'    => $emp_l_nama
		]);
	}

	public static function get_party_user($user = null, $status = 'All', $jenis = null, $tipe = 'get')
	{
		$sql = DB::table('pt2_party As pp')
		->select('pp.*');

		if(strcasecmp($status, 'All') != 0)
		{
			$sql->where('pp.status', $status);
		}

		if(!$jenis)
		{
			if($user && $user->pt2_level == 0)
			{
				$sql->where('leader_nik', $user->id_user);
			}
		}
		else
		{
			$sql->where('unit_nik', $user->id_user);
		}

		if($tipe == 'get')
		{
			return $sql->get();
		}
		else
		{
			return $sql->first();
		}
	}

	public static function check_regu($id)
	{
		$sql = DB::table('regu As r')->Where([
			['job', 'PT2'],
			['ACTIVE', 1]
		]);

		$sql->where(function($join) use($id){
			$join->where('r.nik1', $id)
			->Orwhere('r.nik2', $id)
			->Orwhere('r.nik3', $id)
			->Orwhere('r.nik4', $id);
		});

		return $sql->first();
	}

	public static function check_regu_active($id)
	{
		$sql = DB::table('pt2_party As pp')->Where([
			['status', '!=', 1],
		])
		->where(function($join) use($id){
			$join->where('pp.unit_nik', $id)
			->Orwhere('pp.leader_nik', $id);
		});

		return $sql->first();
	}

	public static function sync_regu_log_party($nik)
	{
		$find_data = self::check_regu(($nik) );

		$insert_data = [];

		$get_emp = DB::table('1_2_employee')->get();

			$get_emp = array_map(function($item) {
				return (array)$item;
			}, $get_emp->toArray() );

			$find_name_l = array_search($find_data->nik1, array_column($get_emp, 'nik' ) );

			$emp_nm_l = 'Kosong_'. $find_data->nik1;

			if($find_name_l !== FALSE)
			{
				$emp_nm_l = $get_emp[$find_name_l]['nama'];
			}

			if($find_data->nik2)
			{
				$find_name = array_search($find_data->nik2, array_column($get_emp, 'nik' ) );

				$emp_nm = 'Kosong_'. $find_data->nik2;

				if($find_name !== FALSE)
				{
					$emp_nm = $get_emp[$find_name]['nama'];
				}

				$insert_data[$find_data->nik2]['party_name']  = $find_data->uraian;
				$insert_data[$find_data->nik2]['leader_nik']  = $find_data->nik1;
				$insert_data[$find_data->nik2]['leader_name'] = $emp_nm_l;
				$insert_data[$find_data->nik2]['unit_nik']    = $find_data->nik2;
				$insert_data[$find_data->nik2]['unit_name']   = $emp_nm;
				$insert_data[$find_data->nik2]['unit_masuk']  = date('Y-m-d H:i:s');
				$insert_data[$find_data->nik2]['status']      = 1;
				$insert_data[$find_data->nik2]['created_at']  = date('Y-m-d H:i:s');
				$insert_data[$find_data->nik2]['created_by']  = $find_data->nik1;
			}

			if($find_data->nik3)
			{
				$find_name = array_search($find_data->nik3, array_column($get_emp, 'nik' ) );

				$emp_nm = 'Kosong_'. $find_data->nik3;

				if($find_name !== FALSE)
				{
					$emp_nm = $get_emp[$find_name]['nama'];
				}

				$insert_data[$find_data->nik3]['party_name']  = $find_data->uraian;
				$insert_data[$find_data->nik3]['leader_nik']  = $find_data->nik1;
				$insert_data[$find_data->nik3]['leader_name'] = $emp_nm_l;
				$insert_data[$find_data->nik3]['unit_nik']    = $find_data->nik3;
				$insert_data[$find_data->nik3]['unit_name']   = $emp_nm;
				$insert_data[$find_data->nik3]['unit_masuk']  = date('Y-m-d H:i:s');
				$insert_data[$find_data->nik3]['status']      = 1;
				$insert_data[$find_data->nik3]['created_at']  = date('Y-m-d H:i:s');
				$insert_data[$find_data->nik3]['created_by']  = $find_data->nik1;
			}

			if($find_data->nik4)
			{
				$find_name = array_search($find_data->nik4, array_column($get_emp, 'nik' ) );

				$emp_nm = 'Kosong_'. $find_data->nik4;

				if($find_name !== FALSE)
				{
					$emp_nm = $get_emp[$find_name]['nama'];
				}

				$insert_data[$find_data->nik4]['party_name']  = $find_data->uraian;
				$insert_data[$find_data->nik4]['leader_nik']  = $find_data->nik1;
				$insert_data[$find_data->nik4]['leader_name'] = $emp_nm_l;
				$insert_data[$find_data->nik4]['unit_nik']    = $find_data->nik4;
				$insert_data[$find_data->nik4]['unit_name']   = $emp_nm;
				$insert_data[$find_data->nik4]['unit_masuk']  = date('Y-m-d H:i:s');
				$insert_data[$find_data->nik4]['status']      = 1;
				$insert_data[$find_data->nik4]['created_at']  = date('Y-m-d H:i:s');
				$insert_data[$find_data->nik4]['created_by']  = $find_data->nik1;
			}

		if(count($insert_data) == 0)
		{
			$find_name = array_search($find_data->nik1, array_column($get_emp, 'nik' ) );

			$emp_nm = 'Kosong_'. $find_data->nik1;

			if($find_name !== FALSE)
			{
				$emp_nm = $get_emp[$find_name]['nama'];
			}

			$insert_data[$find_data->nik1]['party_name']  = $find_data->uraian;
			$insert_data[$find_data->nik1]['leader_nik']  = $find_data->nik1;
			$insert_data[$find_data->nik1]['leader_name'] = $emp_nm_l;
			$insert_data[$find_data->nik1]['unit_nik']    = $find_data->nik1;
			$insert_data[$find_data->nik1]['unit_name']   = $emp_nm;
			$insert_data[$find_data->nik1]['unit_masuk']  = date('Y-m-d H:i:s');
			$insert_data[$find_data->nik1]['status']      = 1;
			$insert_data[$find_data->nik1]['created_at']  = date('Y-m-d H:i:s');
			$insert_data[$find_data->nik1]['created_by']  = $find_data->nik1;
		}

		foreach($insert_data as $v)
		{
			DB::table('pt2_party')->insert($v);
		}
	}

	public static function get_log_party($user, $status = [])
	{
		$sql_raw = DB::table('pt2_party_log As ppl')
		->LeftJoin('pt2_party As pp', 'pp.unit_nik', '=', 'ppl.nik')
		->select('ppl.*')
		->OrderBy('ppl.kondisi', 'DESC')
		->OrderBy('ppl.created_at', 'DESC');

		if($user->pt2_level == 0)
		{
			$sql_raw->where('leader_nik', $user->id_user)
			->Orwhere('ppl.nik', $user->id_user);
		}

		if($status)
		{
			$sql_raw->whereIn('ppl.kondisi', $status);
		}

		$get_log_pt2 = DB::Table('pt2_master As pm')
		->leftjoin('pt2_dispatch As pd', 'pm.nik1', '=', 'pd.nik1')
		->leftjoin('regu As r', 'r.id_regu', '=', 'pm.regu_id')
		->select('pd.*', 'pm.jointer_name')
		->where([
			['pm.nik1', $user->id_user],
			['job', 'PT2'],
			['pm.delete_clm', 0]
		])
		->get();

		$sql_raw = $sql_raw->get();

		$sql = [];

		foreach($sql_raw as $k => $v)
		{
			$sql[$k] = $v;
		}

		$log_dispatch = [];

		foreach($get_log_pt2 as $k => $v)
		{
			$log_dispatch[$k] = new \stdClass();
			$log_dispatch[$k]->nik = $v->nik1;
			$log_dispatch[$k]->nama = $v->jointer_name;
			$log_dispatch[$k]->kondisi = 2;
			$log_dispatch[$k]->keterangan = $v->keterangan;
			$log_dispatch[$k]->created_at = $v->tgl_kerja_dispatch;
		}

		$fd = (array) array_merge(
			(array) $log_dispatch, (array) $sql);

		usort($fd, function($a, $b) {return strcmp($b->created_at, $a->created_at);});

		return $fd;
	}

	public static function get_my_party($user)
	{
		return DB::table('pt2_party As pp')
		->leftjoin('pt2_party As pp2', 'pp2.leader_nik', '=', 'pp.leader_nik')
		->select('pp.*')
		->where('pp2.unit_nik', $user->id_user)
		->GroupBy('pp.id')
		->get();
	}

	public static function get_my_party_all($user)
	{
		return DB::table('pt2_party As pp')
		->leftjoin('pt2_party As pp2', 'pp2.leader_nik', '=', 'pp.leader_nik')
		->select('pp.*')
		->where('pp2.unit_nik', $user->id_user)
		->Orwhere('pp2.leader_nik', $user->id_user)
		->GroupBy('pp.id')
		->get();
	}

	public static function load_tim()
	{
		return DB::table('pt2_party')->where('leader_nik', session('auth')->id_user)->get();
	}

	public static function dismiss_party()
	{
		DB::transaction(function (){
			$get_emp = DB::table('1_2_employee')->get();

			$get_emp = array_map(function($item) {
				return (array)$item;
			}, $get_emp->toArray() );

			$find_name_k_l = array_search(session('auth')->id_user, array_column($get_emp, 'nik' ) );

			$emp_l_nama = 'Kosong_'. session('auth')->id_user;

			if($find_name_k_l !== FALSE)
			{
				$emp_l_nama = $get_emp[$find_name_k_l]['nama'];
			}

			$get_all_user = DB::table('pt2_party')->where('leader_nik', session('auth')->id_user)->get();

			foreach($get_all_user as $v)
			{
				$nik[$v->leader_nik] = $v->leader_nik;
				$nik[$v->unit_nik] = $v->unit_nik;
			}

			DB::Table('pt2_party_log')->insert([
				'nik'     => session('auth')->id_user,
				'kondisi' => 7,
				'nama'    => $emp_l_nama
			]);

			$load_kerja = DB::Table('pt2_master')->whereIn('nik1', $nik)->get();

			foreach($load_kerja as $v)
			{
				$find_name_pt = array_search($v->nik1, array_column($get_emp, 'nik' ) );

				$emp_name_pt = 'Kosong_'. $v->nik1;

				if($find_name_pt !== FALSE)
				{
					$emp_name_pt = $get_emp[$find_name_pt]['nama'];
				}

				DB::Table('pt2_party_log')->insert([
					'nik'        => $v->nik1,
					'kondisi'    => 7,
					'id_pt2'     => $v->id,
					'keterangan' => 'Mengosongkan Pekerjaan karena Tim Bubar',
					'nama'       => $emp_name_pt
				]);
			}

			$nik_arr = "'".implode("','", $nik)."'";

			DB::SELECT("UPDATE pt2_party_log ppl
			LEFT JOIN pt2_master pm ON pm.nik1 = ppl.nik
			LEFT JOIN regu r ON r.id_regu = pm.regu_id
			SET ppl.id_pt2 = pm.id, ppl.keterangan = 'Keluar Dari Regu!'
			WHERE ppl.nik IN ($nik_arr) AND (lt_status NOT IN ('Selesai', 'Kendala') AND ACTIVE = 1 OR lt_status IS NULL) AND delete_clm = 0");

			DB::Table('pt2_master')
			->leftjoin('regu As r', 'r.id_regu', '=', 'pt2_master.regu_id')
			->select('pt2_master.*')
			->whereIn('r.nik1', $nik)
			->where([
				['delete_clm', 0],
				['job', 'PT2'],
			])
			->where(function($join){
				$join->whereNull('lt_status')
				->OrwhereNotIn('lt_status', ['Selesai', 'Kendala']);
			})
			->update([
				'lt_status'       => null,
				'regu_id'         => null,
				'pt2_master.nik1' => null,
				'pt2_master.nik2' => null,
				'regu_name'       => null,
				'jointer_name'    => null,
			]);

			$id_u = session('auth')->id_user;

			DB::table('regu As r')->Where([
				['job', 'PT2'],
				['ACTIVE', 1]
			])->where(function($join) use($id_u){
				$join->where('r.nik1', $id_u);
			})->update([
				'ACTIVE' => 0
			]);

			DB::table('pt2_party')->where('leader_nik', session('auth')->id_user)->delete();
		});
	}

	public static function out_party()
	{
		DB::transaction(function (){
			$get_emp = DB::table('1_2_employee')->get();

			$get_emp = array_map(function($item) {
				return (array)$item;
			}, $get_emp->toArray() );

			$nik[] = session('auth')->id_user;

			$nik_arr = "'".implode("','", $nik)."'";

			DB::SELECT("UPDATE pt2_party_log ppl
			LEFT JOIN pt2_master pm ON pm.nik1 = ppl.nik
			LEFT JOIN regu r ON r.id_regu = pm.regu_id
			SET ppl.id_pt2 = pm.id, ppl.keterangan = 'Keluar Dari Regu!'
			WHERE ppl.nik IN ($nik_arr) AND (lt_status NOT IN ('Selesai', 'Kendala') AND ACTIVE = 1 OR lt_status IS NULL) AND delete_clm = 0");

			DB::Table('pt2_master')
			->leftjoin('regu As r', 'r.id_regu', '=', 'pt2_master.regu_id')
			->select("pt2_master.*")
			->whereIn('pt2_master.nik2', $nik)
			->where([
				['job', 'PT2'],
				['delete_clm', 0]
			])
			->where(function($join){
				$join->whereNull('lt_status')
				->OrwhereNotIn('lt_status', ['Selesai', 'Kendala']);
			})
			->update([
				'lt_status'       => null,
				'pt2_master.nik2' => null,
			]);

			DB::table('pt2_party')->where('unit_nik', session('auth')->id_user)->delete();

			$load_kerja = DB::Table('pt2_master')->whereIn('nik1', $nik)->get();

			foreach($load_kerja as $v)
			{
				$find_name_pt = array_search($v->nik1, array_column($get_emp, 'nik' ) );

				$emp_name_pt = 'Kosong_'. $v->nik1;

				if($find_name_pt !== FALSE)
				{
					$emp_name_pt = $get_emp[$find_name_pt]['nama'];
				}

				$id_u = session('auth')->id_user;

				DB::table('regu As r')->Where([
					['job', 'PT2'],
					['ACTIVE', 1]
				])->where(function($join) use($id_u){
					$join->where('r.nik2', $id_u);
				})->update([
					'r.nik2' => null,
					'r.nama2' => null
				]);

				DB::table('regu As r')->Where([
					['job', 'PT2'],
					['ACTIVE', 1]
				])->where(function($join) use($id_u){
					$join->where('r.nik3', $id_u);
				})->update([
					'r.nik3' => null,
				]);

				DB::table('regu As r')->Where([
					['job', 'PT2'],
					['ACTIVE', 1]
				])->where(function($join) use($id_u){
					$join->where('r.nik4', $id_u);
				})->update([
					'r.nik4' => null,
				]);

				DB::Table('pt2_party_log')->insert([
					'nik'        => $v->nik1,
					'kondisi'    => 4,
					'id_pt2'     => $v->id,
					'keterangan' => 'Mengosongkan Pekerjaan karena Keluar Tim',
					'nama'       => $emp_name_pt
				]);
			}
		});
	}

	public static function join_party($id)
	{
		$get_emp = DB::table('1_2_employee')->get();

		$get_emp = array_map(function($item) {
			return (array)$item;
		}, $get_emp->toArray() );

		$find_name_k_u = array_search(session('auth')->id_user, array_column($get_emp, 'nik' ) );

		$emp_u_nama = 'Kosong_'. $id;

		if($find_name_k_u !== FALSE)
		{
			$emp_u_nama = $get_emp[$find_name_k_u]['nama'];
		}

		$find_name_k_l = array_search($id, array_column($get_emp, 'nik' ) );

		$emp_l_nama = 'Kosong_'. $id;

		if($find_name_k_l !== FALSE)
		{
			$emp_l_nama = $get_emp[$find_name_k_l]['nama'];
		}


		$get_data = DB::table('pt2_party')->where('leader_nik', $id)->first();

		$check_regu = DB::table('regu As r')->Where([
			['nik1', $id],
			['job', 'PT2'],
			['ACTIVE', 1]
		])->first();


		if(!$check_regu->nik2)
		{
			DB::table('regu As r')->Where([
				['nik1', $id],
				['job', 'PT2'],
				['ACTIVE', 1]
			])->update([
				'nik2'      => session('auth')->id_user,
				'unit_name' => $emp_u_nama,
			]);
		}
		elseif(!$check_regu->nik3)
		{
			DB::table('regu As r')->Where([
				['nik1', $id],
				['job', 'PT2'],
				['ACTIVE', 1]
			])->update([
				'nik3' => session('auth')->id_user,
			]);
		}
		else
		{
			DB::table('regu As r')->Where([
				['nik1', $id],
				['job', 'PT2'],
				['ACTIVE', 1]
			])->update([
				'nik4' => session('auth')->id_user,
			]);
		}

		DB::Table('pt2_party')->insert([
			'party_name'  => $get_data->party_name,
			'leader_nik'  => $id,
			'leader_name' => $emp_l_nama,
			'unit_nik'    => session('auth')->id_user,
			'unit_name'   => $emp_u_nama,
			'status'   		=> 3,
			'created_by'  => session('auth')->id_user,
		]);

		DB::Table('pt2_party_log')->insert([
			'nik'     => session('auth')->id_user,
			'kondisi' => 3,
			'nama'    => $emp_u_nama
		]);
	}

	public static function abort_join($id)
	{
		$get_emp = DB::table('1_2_employee')->get();

		$get_emp = array_map(function($item) {
			return (array)$item;
		}, $get_emp->toArray() );

		$find_k = array_search($id, array_column($get_emp, 'nik' ) );

		$emp_u_nama = 'Kosong_'. $id;

		if($find_k !== FALSE)
		{
			$emp_u_nama = $get_emp[$find_k]['nama'];
		}

		DB::table('regu As r')->Where([
			['job', 'PT2'],
			['ACTIVE', 1]
		])->where(function($join) use($id){
			$join->where('r.nik2', $id);
		})->update([
			'r.nik2' => 0,
			'r.nama2' => null
		]);

		DB::table('regu As r')->Where([
			['job', 'PT2'],
			['ACTIVE', 1]
		])->where(function($join) use($id){
			$join->where('r.nik3', $id);
		})->update([
			'r.nik3' => null,
		]);

		DB::table('regu As r')->Where([
			['job', 'PT2'],
			['ACTIVE', 1]
		])->where(function($join) use($id){
			$join->where('r.nik4', $id);
		})->update([
			'r.nik4' => null,
		]);

		DB::Table('pt2_party')->where('unit_nik', $id)->delete();

		DB::Table('pt2_party_log')->insert([
			'nik'     => $id,
			'kondisi' => 8,
			'nama'    => $emp_u_nama
		]);
	}

	public static function approve_join($id)
	{
		$get_emp = DB::table('1_2_employee')->get();

		$get_emp = array_map(function($item) {
			return (array)$item;
		}, $get_emp->toArray() );

		$find_k = array_search($id, array_column($get_emp, 'nik' ) );

		$emp_u_nama = 'Kosong_'. $id;

		if($find_k !== FALSE)
		{
			$emp_u_nama = $get_emp[$find_k]['nama'];
		}

		DB::Table('pt2_party')->where([
			['leader_nik', session('auth')->id_user],
			['unit_nik', $id]
		])->update([
			'status' => 1
		]);

		DB::Table('pt2_party_log')->insert([
			'nik'     => $id,
			'kondisi' => 2,
			'nama'    => $emp_u_nama
		]);
	}

	public static function unit_join($id)
	{
		$get_emp = DB::table('1_2_employee')->get();

		$get_emp = array_map(function($item) {
			return (array)$item;
		}, $get_emp->toArray() );

		$find_k = array_search(session('auth')->id_user, array_column($get_emp, 'nik' ) );

		$emp_u_nama = 'Kosong_'. session('auth')->id_user;

		if($find_k !== FALSE)
		{
			$emp_u_nama = $get_emp[$find_k]['nama'];
		}

		if($id == 1)
		{
			DB::Table('pt2_party')->where([
				['unit_nik', session('auth')->id_user],
			])->update([
				'status' => $id
			]);

			$status_log = 2;
		}
		else
		{
			DB::Table('pt2_party')->where('unit_nik', session('auth')->id_user)->delete();

			$status_log = 6;
		}

		DB::Table('pt2_party_log')->insert([
			'nik'     => session('auth')->id_user,
			'kondisi' => $status_log,
			'nama'    => $emp_u_nama
		]);
	}

	public static function get_jointer_wo($nik)
	{
		return DB::table('pt2_master')
		->leftjoin('regu As r', 'r.id_regu', '=', 'pt2_master.regu_id')
		->select("pt2_master.*")
		->where('r.nik1', $nik)
		->where(function($join){
			$join->Where([
				['r.job', 'PT2'],
				['delete_clm', 0],
			])
			->OrwhereNull('regu_id');
		})
		->where(function($join){
			$join->whereNull('lt_status')
			->OrwhereNotIn('lt_status', ['Selesai', 'Kendala']);
		})
		->get();
	}

	public static function get_all_work($id)
	{
		$id = "'".implode("','", $id) ."'";

		$sql = DB::SELECT("SELECT pm.*, pd.*, (CASE WHEN pm.id_maint != 0 THEN 'MARINA' WHEN pm.id_bts != 0 THEN 'BTS' WHEN pm.id_refer != 0 THEN 'PT-2 JOINTER' END) As jenis_pickup_outside
		FROM pt2_master pm
		LEFT JOIN regu r ON r.id_regu = pm.regu_id
		LEFT JOIN (SELECT id_order,
			SUBSTRING_INDEX(GROUP_CONCAT(updated_by ORDER BY id_pt2 DESC), ',', 1 ) AS pd_aktor,
			SUBSTRING_INDEX(GROUP_CONCAT(timestamp_dispatch ORDER BY id_pt2 DESC), ',', 1 ) AS timestamp_dispatch
			FROM pt2_dispatch WHERE nik1 IN($id) GROUP BY id_order) As pd ON pm.id = pd.id_order
		WHERE (r.job = 'PT2' OR pm.regu_id IS NULL) AND pm.nik1 IS NOT NULL AND id_order IS NOT NULL AND pm.delete_clm = 0 AND (lt_status NOT IN ('Selesai', 'Kendala') AND ACTIVE = 1 OR lt_status IS NULL)");

		$get_emp = DB::table('1_2_employee')->get();

		$get_emp = array_map(function($item) {
			return (array)$item;
		}, $get_emp->toArray() );

		foreach($sql as $k => $v)
		{
			$find_name_pd_aktor = array_search($v->pd_aktor, array_column($get_emp, 'nik' ) );

			$emp_pd_aktor_nm = 'Kosong_'. $v->pd_aktor;

			if($find_name_pd_aktor !== FALSE)
			{
				$emp_pd_aktor_nm = $get_emp[$find_name_pd_aktor]['nama'];
			}

			$sql[$k]->pd_aktor_nama = $emp_pd_aktor_nm;
		}

		return $sql;
	}

	public static function perform_jointer($date1, $date2, $mitra = null, $datel = null, $sto = null)
	{
		$jointer_pt2 = DB::table('pt2_master As pt2')
		->leftjoin('pt2_core_log As pcl', 'pt2.id', '=', 'pcl.id_dispatch')
		->leftjoin('user As u', 'u.id_user', '=', 'pt2.nik1')
		->Leftjoin('1_2_employee As emp', 'u.id_user', '=', 'emp.nik')
		->Leftjoin('mitra_amija As mm', 'mm.mitra_amija', '=', 'emp.mitra_amija')
		->leftjoin('regu As r', 'r.id_regu', '=', 'pt2.regu_id')
		->select('pt2.*', 'r.label_sektor', 'pcl.qty', 'mm.mitra_amija_pt')
		->Where([
			['delete_clm', 0],
			['id_maint', 0],
			['id_bts', 0]
		])
		->where(function($join) use($date1, $date2){
			$join->whereNull('pt2.lt_status')
			->OrWhereBetween(DB::RAW("DATE_FORMAT(pt2.tgl_selesai, '%Y-%m-%d')"), [$date1, $date2])
			->where('lt_status', 'Selesai');
		})
		->where(function($join){
			$join->Where('r.job', 'PT2');
			// ->OrWhereNull('pt2.regu_id');
		});

		if(session('auth')->pt2_level == 3)
		{
			$jointer_pt2->Where('r.mitra', session('auth')->mitra);
		}

		if($sto && strcasecmp($sto, 'All') != 0)
		{
			$jointer_pt2->where('pt2.sto', $sto);
		}

		if($mitra && strcasecmp($mitra, 'All') != 0)
		{
			$jointer_pt2->where('mm.mitra_amija_pt', $mitra);
		}

		if($datel && strcasecmp($datel, 'All') != 0)
		{
			$dt = self::get_all_datel();
			$dt = json_decode(json_encode($dt), TRUE);
			$dt = array_filter($dt, function($x) use($datel){
				return $x['datel'] == $datel;
			});

			$jointer_pt2->WhereIn('pt2.sto', array_column($dt, 'sto') );
		}

		if(session('auth')->pt2_level == 8 )
		{
			$jointer_pt2->where('emp.atasan_1', session('auth')->id_user);
		}

		$jointer_pt2 = $jointer_pt2->get();

		if(session('auth')->pt2_level == 9)
		{
			$find_sektor = DB::table('group_telegram')
			->Where('TL_NIK', session('auth')->id_user)
			->OrWhere('Nik_Atl', session('auth')->id_user)
			->OrWhere('Nik_Atl2', session('auth')->id_user)
			->get();

			$get_sektor = [];

			foreach($find_sektor as $v)
			{
				$get_sektor[$v->sektorx] = $v->sektorx;
			}

			foreach ($jointer_pt2 as $k => &$v)
			{
				if(!in_array($v->label_sektor, $get_sektor) )
				{
					unset($jointer_pt2[$k]);
				}
			}
			unset($v);
		}

		$jointer_marina = DB::table('pt2_master As pt2')
		->leftjoin('pt2_core_log As pcl', 'pt2.id', '=', 'pcl.id_dispatch')
		->leftjoin('user As u', 'u.id_user', '=', 'pt2.nik1')
		->Leftjoin('1_2_employee As emp', 'u.id_user', '=', 'emp.nik')
		->Leftjoin('mitra_amija As mm', 'mm.mitra_amija', '=', 'emp.mitra_amija')
		->leftjoin('regu As r', 'r.id_regu', '=', 'pt2.regu_id')
		->select('pt2.*', 'r.label_sektor', 'pcl.qty', 'mm.mitra_amija_pt')
		->Where([
			['delete_clm', 0],
		])
		->Where('id_maint', '!=', 0)
		->where(function($join){
			$join->Where('r.job', 'PT2');
			// ->OrWhereNull('pt2.regu_id');
		})
		->where(function($join) use($date1, $date2){
			$join->whereNull('pt2.lt_status')
			->OrWhereBetween(DB::RAW("DATE_FORMAT(pt2.tgl_selesai, '%Y-%m-%d')"), [$date1, $date2])
			->where('lt_status', 'Selesai');
		});

		if(session('auth')->pt2_level == 3)
		{
			$jointer_marina->Where('r.mitra', session('auth')->mitra);
		}

		if($sto && strcasecmp($sto, 'All') != 0)
		{
			$jointer_marina->where('pt2.sto', $sto);
		}

		if($mitra && strcasecmp($mitra, 'All') != 0)
		{
			$jointer_marina->where('mm.mitra_amija_pt', $mitra);
		}

		if($datel && strcasecmp($datel, 'All') != 0)
		{
			$dt = self::get_all_datel();
			$dt = json_decode(json_encode($dt), TRUE);
			$dt = array_filter($dt, function($x) use($datel){
				return $x['datel'] == $datel;
			});

			$jointer_marina->WhereIn('pt2.sto', array_column($dt, 'sto') );
		}

		if(session('auth')->pt2_level == 8 )
		{
			$jointer_marina->where('emp.atasan_1', session('auth')->id_user);
		}

		$jointer_marina = $jointer_marina->get();

		if(session('auth')->pt2_level == 9)
		{
			$find_sektor = DB::table('group_telegram')
			->Where('TL_NIK', session('auth')->id_user)
			->OrWhere('Nik_Atl', session('auth')->id_user)
			->OrWhere('Nik_Atl2', session('auth')->id_user)
			->get();

			$get_sektor = [];

			foreach($find_sektor as $v)
			{
				$get_sektor[$v->sektorx] = $v->sektorx;
			}

			foreach ($jointer_marina as $k => &$v)
			{
				if(!in_array($v->label_sektor, $get_sektor) )
				{
					unset($jointer_marina[$k]);
				}
			}
			unset($v);
		}

		$jointer_bts = DB::table('pt2_master As pt2')
		->leftjoin('pt2_core_log As pcl', 'pt2.id', '=', 'pcl.id_dispatch')
		->leftjoin('user As u', 'u.id_user', '=', 'pt2.nik1')
		->Leftjoin('1_2_employee As emp', 'u.id_user', '=', 'emp.nik')
		->Leftjoin('mitra_amija As mm', 'mm.mitra_amija', '=', 'emp.mitra_amija')
		->leftjoin('regu As r', 'r.id_regu', '=', 'pt2.regu_id')
		->select('pt2.*', 'r.label_sektor', 'pcl.qty', 'mm.mitra_amija_pt')
		->Where([
			['delete_clm', 0],
		])
		->Where('id_bts', '!=', 0)
		->where(function($join){
			$join->Where('r.job', 'PT2');
			// ->OrWhereNull('pt2.regu_id');
		})
		->where(function($join) use($date1, $date2){
			$join->whereNull('pt2.lt_status')
			->OrWhereBetween(DB::RAW("DATE_FORMAT(pt2.tgl_selesai, '%Y-%m-%d')"), [$date1, $date2])
			->where('lt_status', 'Selesai');
		});

		if($sto && strcasecmp($sto, 'All') != 0)
		{
			$jointer_bts->where('pt2.sto', $sto);
		}

		if($mitra && strcasecmp($mitra, 'All') != 0)
		{
			$jointer_marina->where('mm.mitra_amija_pt', $mitra);
		}

		if($datel && strcasecmp($datel, 'All') != 0)
		{
			$dt = self::get_all_datel();
			$dt = json_decode(json_encode($dt), TRUE);
			$dt = array_filter($dt, function($x) use($datel){
				return $x['datel'] == $datel;
			});

			$jointer_bts->WhereIn('pt2.sto', array_column($dt, 'sto') );
		}

		if(session('auth')->pt2_level == 8 )
		{
			$jointer_bts->where('emp.atasan_1', session('auth')->id_user);
		}

		$jointer_bts = $jointer_bts->get();

		if(session('auth')->pt2_level == 9)
		{
			$find_sektor = DB::table('group_telegram')
			->Where('TL_NIK', session('auth')->id_user)
			->OrWhere('Nik_Atl', session('auth')->id_user)
			->OrWhere('Nik_Atl2', session('auth')->id_user)
			->get();

			$get_sektor = [];

			foreach($find_sektor as $v)
			{
				$get_sektor[$v->sektorx] = $v->sektorx;
			}

			foreach ($jointer_bts as $k => &$v)
			{
				if(!in_array($v->label_sektor, $get_sektor) )
				{
					unset($jointer_bts[$k]);
				}
			}
			unset($v);
		}

		$get_all_user = DB::table('user As u')
		->Leftjoin('1_2_employee As emp', 'u.id_user', '=', 'emp.nik')
		->Leftjoin('regu As r', 'u.id_user', '=', 'r.nik1')
		->Leftjoin('mitra_amija As mma', 'mma.mitra_amija', '=', 'emp.mitra_amija')
		->leftjoin('pt2_master As pm', 'pm.nik1', '=', 'u.id_user')
		->select('emp.nama', 'r.uraian', 'r.id_regu', 'r.label_sektor', 'emp.nik', 'emp.mitra_amija', 'mma.mitra_amija_pt', DB::RAW("SUM(CASE WHEN pm.id IS NOT NULL AND (pm.regu_id IS NULL OR r.job = 'PT2') THEN 1 ELSE 0 END) As total") )
		->where([
			['r.ACTIVE', 1],
			['r.job', 'PT2'],
		])
		->Where(function($join){
			$join->whereNotNull('emp.mitra_amija')
			->Where('emp.mitra_amija', '!=', '');
		})
		->GroupBy('r.id_regu');

		if(session('auth')->pt2_level == 3)
		{
			$get_all_user->Where('r.mitra', session('auth')->mitra);
		}

		if(session('auth')->pt2_level == 8 )
		{
			$get_all_user->where('emp.atasan_1', session('auth')->id_user);
		}

		$get_all_user = $get_all_user->get();

		if(session('auth')->pt2_level == 9)
		{
			$find_sektor = DB::table('group_telegram')
			->Where('TL_NIK', session('auth')->id_user)
			->OrWhere('Nik_Atl', session('auth')->id_user)
			->OrWhere('Nik_Atl2', session('auth')->id_user)
			->get();

			$get_sektor = [];

			foreach($find_sektor as $v)
			{
				$get_sektor[$v->sektorx] = $v->sektorx;
			}

			foreach ($get_all_user as $k => &$v)
			{
				if(!in_array($v->label_sektor, $get_sektor) )
				{
					unset($get_all_user[$k]);
				}
			}
			unset($v);
		}

		return ['get_all_user' => $get_all_user, 'jointer_marina' => $jointer_marina, 'jointer_bts' => $jointer_bts, 'jointer_pt2' => $jointer_pt2];
	}

	public static function umur_jointer($date, $mitra = null, $datel = null, $sto = null)
	{
		$jointer_pt2 = DB::table('pt2_master As pt2')
		->leftjoin('pt2_core_log As pcl', 'pt2.id', '=', 'pcl.id_dispatch')
		->leftjoin('user As u', 'u.id_user', '=', 'pt2.nik1')
		->Leftjoin('1_2_employee As emp', 'u.id_user', '=', 'emp.nik')
		->Leftjoin('mitra_amija As mm', 'mm.mitra_amija', '=', 'emp.mitra_amija')
		->leftjoin('regu As r', 'r.id_regu', '=', 'pt2.regu_id')
		->select('pt2.*', 'pcl.qty', 'mm.mitra_amija_pt')
		->Where([
			['delete_clm', 0],
			['id_maint', 0],
			['id_bts', 0]
		])
		->where(function($join){
			$join->Where('r.job', 'PT2')
			->OrWhereNull('pt2.regu_id');
		})
		->where(function($join){
			$join->whereNull('pt2.lt_status')
			->OrwhereNotIn('lt_status', ['Selesai', 'Kendala']);
		});

		if($sto && strcasecmp($sto, 'All') != 0)
		{
			$jointer_pt2->where('pt2.sto', $sto);
		}

		if($mitra && strcasecmp($mitra, 'All') != 0)
		{
			$jointer_pt2->where('mm.mitra_amija_pt', $mitra);
		}

		if($datel && strcasecmp($datel, 'All') != 0)
		{
			$dt = self::get_all_datel();
			$dt = json_decode(json_encode($dt), TRUE);
			$dt = array_filter($dt, function($x) use($datel){
				return $x['datel'] == $datel;
			});

			$jointer_pt2->WhereIn('pt2.sto', array_column($dt, 'sto') );
		}

		if(session('auth')->pt2_level == 8 )
		{
			$jointer_pt2->where('emp.atasan_1', session('auth')->id_user);
		}

		$jointer_pt2 = $jointer_pt2->get();

		$jointer_marina = DB::table('pt2_master As pt2')
		->leftjoin('pt2_core_log As pcl', 'pt2.id', '=', 'pcl.id_dispatch')
		->leftjoin('user As u', 'u.id_user', '=', 'pt2.nik1')
		->Leftjoin('1_2_employee As emp', 'u.id_user', '=', 'emp.nik')
		->leftjoin('regu As r', 'r.id_regu', '=', 'pt2.regu_id')
		->Leftjoin('mitra_amija As mm', 'mm.mitra_amija', '=', 'emp.mitra_amija')
		->select('pt2.*', 'pcl.qty', 'mm.mitra_amija_pt')
		->Where([
			['delete_clm', 0],
		])
		->Where('id_maint', '!=', 0)
		->where(function($join){
			$join->Where('r.job', 'PT2');
			// ->OrWhereNull('pt2.regu_id');
		})
		->where(function($join){
			$join->whereNull('pt2.lt_status')
			->OrwhereNotIn('lt_status', ['Selesai', 'Kendala']);
		});

		if($sto && strcasecmp($sto, 'All') != 0)
		{
			$jointer_marina->where('pt2.sto', $sto);
		}

		if($mitra && strcasecmp($mitra, 'All') != 0)
		{
			$jointer_marina->where('mm.mitra_amija_pt', $mitra);
		}

		if($datel && strcasecmp($datel, 'All') != 0)
		{
			$dt = self::get_all_datel();
			$dt = json_decode(json_encode($dt), TRUE);
			$dt = array_filter($dt, function($x) use($datel){
				return $x['datel'] == $datel;
			});

			$jointer_marina->WhereIn('pt2.sto', array_column($dt, 'sto') );
		}

		if(session('auth')->pt2_level == 8 )
		{
			$jointer_marina->where('emp.atasan_1', session('auth')->id_user);
		}

		$jointer_marina = $jointer_marina->get();

		$jointer_bts = DB::table('pt2_master As pt2')
		->leftjoin('pt2_core_log As pcl', 'pt2.id', '=', 'pcl.id_dispatch')
		->leftjoin('user As u', 'u.id_user', '=', 'pt2.nik1')
		->leftjoin('regu As r', 'r.id_regu', '=', 'pt2.regu_id')
		->Leftjoin('1_2_employee As emp', 'u.id_user', '=', 'emp.nik')
		->Leftjoin('mitra_amija As mm', 'mm.mitra_amija', '=', 'emp.mitra_amija')
		->select('pt2.*', 'pcl.qty', 'mm.mitra_amija_pt')
		->Where([
			['delete_clm', 0],
		])
		->Where('id_bts', '!=', 0)
		->where(function($join){
			$join->Where('r.job', 'PT2');
			// ->OrWhereNull('pt2.regu_id');
		})
		->where(function($join){
			$join->whereNull('pt2.lt_status')
			->OrwhereNotIn('lt_status', ['Selesai', 'Kendala']);
		});

		if($sto && strcasecmp($sto, 'All') != 0)
		{
			$jointer_bts->where('pt2.sto', $sto);
		}

		if($mitra && strcasecmp($mitra, 'All') != 0)
		{
			$jointer_marina->where('mm.mitra_amija_pt', $mitra);
		}

		if($datel && strcasecmp($datel, 'All') != 0)
		{
			$dt = self::get_all_datel();
			$dt = json_decode(json_encode($dt), TRUE);
			$dt = array_filter($dt, function($x) use($datel){
				return $x['datel'] == $datel;
			});

			$jointer_bts->WhereIn('pt2.sto', array_column($dt, 'sto') );
		}

		if(session('auth')->pt2_level == 8 )
		{
			$jointer_bts->where('emp.atasan_1', session('auth')->id_user);
		}

		$jointer_bts = $jointer_bts->get();

		$get_all_user = DB::table('user As u')
		->Leftjoin('1_2_employee As emp', 'u.id_user', '=', 'emp.nik')
		->Leftjoin('regu As r', 'u.id_user', '=', 'r.nik1')
		->Leftjoin('mitra_amija As mma', 'mma.mitra_amija', '=', 'emp.mitra_amija')
		->leftjoin('pt2_master As pm', 'pm.nik1', '=', 'u.id_user')
		->select('emp.nama', 'emp.nik', 'r.id_regu', 'r.uraian', 'emp.mitra_amija', 'mitra_amija_pt', DB::RAW("SUM(CASE WHEN pm.id IS NOT NULL THEN 1 ELSE 0 END) As total") )
		->where([
			['r.job', 'PT2'],
			['r.ACTIVE', 1]
		])
		->Where(function($join){
			$join->whereNotNull('emp.mitra_amija')
			->Where('emp.mitra_amija', '!=', '');
		})
		->GroupBy('id_people');

		if(session('auth')->pt2_level == 8 )
		{
			$get_all_user->where('emp.atasan_1', session('auth')->id_user);
		}

		$get_all_user = $get_all_user->get();

		return ['get_all_user' => $get_all_user, 'jointer_marina' => $jointer_marina, 'jointer_bts' => $jointer_bts, 'jointer_pt2' => $jointer_pt2];
	}

	public static function get_matrix($date, $mitra = null, $datel = null, $sto = null, $jenis = null)
	{
		$data = DB::Table('pt2_master As pmas')
		->leftjoin('user As u', 'u.id_user', '=', 'pmas.nik1')
		->leftjoin('regu As r', 'r.id_regu', '=', 'pmas.regu_id')
		->leftjoin('pt2_status As pstat', 'pstat.id', '=', 'pmas.kategory_non_unsc')
		->Leftjoin('1_2_employee As emp', 'pmas.nik1', '=', 'emp.nik')
		->Leftjoin('mitra_amija As mma', 'mma.mitra_amija', '=', 'emp.mitra_amija')
		->select('pmas.*', 'r.label_sektor', 'pstat.status as status_pst', 'mma.mitra_amija_pt', DB::RAW("CONCAT(emp.atasan_1, ' (', (SELECT nama FROM 1_2_employee WHERE nik = emp.atasan_1) , ')') as tl_nama") )
		->where([
			['lt_status', 'Selesai'],
			['pmas.delete_clm', 0],
		])
		->where(DB::Raw("DATE_FORMAT(tgl_selesai, '%Y-%m')"), $date)
		->GroupBy('pmas.id');

		if($sto && strcasecmp($sto, 'All') != 0)
		{
			$data->where('m.sto', $sto);
		}

		if($mitra && strcasecmp($mitra, 'All') != 0)
		{
			$data->where('mma.mitra_amija_pt', $mitra);
		}

		if($jenis && strcasecmp($jenis, 'All') != 0)
		{
			$data->where('pstat.status', $jenis);
		}

		if($datel && strcasecmp($datel, 'All') != 0)
		{
			$dt = self::get_all_datel();
			$dt = json_decode(json_encode($dt), TRUE);
			$dt = array_filter($dt, function($x) use($datel){
				return $x['datel'] == $datel;
			});

			$data->WhereIn('m.sto', array_column($dt, 'sto') );
		}

		if(session('auth')->pt2_level == 8 )
		{
			$data->where('emp.atasan_1', session('auth')->id_user);
		}

		$data = $data->get();

		if(session('auth')->pt2_level == 9)
		{
			$find_sektor = DB::table('group_telegram')
			->Where('TL_NIK', session('auth')->id_user)
			->OrWhere('Nik_Atl', session('auth')->id_user)
			->OrWhere('Nik_Atl2', session('auth')->id_user)
			->get();

			$get_sektor = [];

			foreach($find_sektor as $v)
			{
				$get_sektor[$v->sektorx] = $v->sektorx;
			}

			foreach ($data as $k => &$v)
			{
				if(!in_array($v->label_sektor, $get_sektor) )
				{
					unset($data[$k]);
				}
			}
			unset($v);
		}

		return $data;
	}

	public static function get_data_request_rfc($d)
	{
		switch ($d) {
			case 0:
				$data = DB::table('pt2_rekap_rfc As a')
				->LeftJoin('pt2_rekap_rfc_detail As b', 'a.id', '=', 'b.id_req_rfc')
				->select('a.*', 'b.designator', 'b.qty', 'b.qty_used')
				->Where('status', 0);

				if (!in_array(session('auth')->pt2_level, [2, 5]) )
				{
					$data->Where('pt2_rekap_rfc', session('auth')->pt2_level);
				}

				return $data->get();
			break;
			default:
				# code...
			break;
		}
	}

	public static function get_data_stok($id)
	{
		return DB::table('pt2_rekap_rfc As a')
		->LeftJoin('pt2_rekap_rfc_detail As b', 'a.id', '=', 'b.id_req_rfc')
		->select('a.*', 'b.designator', 'b.qty', 'b.qty_used')
		->Where([
			['status', 0],
			['a.id', $id],
		])
		->get();
	}

	public static function save_reserv($req, $id)
	{
		$get_data_rfc = DB::table('pt2_rekap_rfc')->Where('id', $id)->first();
		$that = new JointerModel();
    $that->handlereserv($req, $id);
	}

	private function handlereserv($req, $id)
  {
    $input = 'reservasi_up';
		$path = public_path() . '/upload/file_reserv/' . $id;

		if ($req->hasFile($input)) {
			$file = $req->file($input);
			$ext = $file->getClientOriginalExtension();
			try {
				$file->move("$path", "reserv.$ext");
			} catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
				return 'gagal menyimpan reservasi ';
			}
		}
  }
}