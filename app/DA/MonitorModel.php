<?php
namespace App\DA;

use Illuminate\Support\Facades\DB;

date_default_timezone_set("Asia/Makassar");
class MonitorModel
{
	public static function get_my_tim($mitra)
	{
		return DB::table('regu')
		->select(DB::Raw("(SELECT CONCAT(nama, ' (', nik1, ')') FROM 1_2_employee WHERE nik = nik1)as nik_1, (SELECT CONCAT(nama, ' (', nik2, ')') FROM 1_2_employee WHERE nik = nik2)as nik_2") , 'regu.*')
		->where([
			['mitra', $mitra],
			['job', 'PT2'],
			['id_regu', '!=', 766]
		])->get();
	}
}