<?php
namespace App\DA;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use Telegram;
use ZipArchive;
use App\DA\TeknisiModel;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\TeknisiController;

date_default_timezone_set('Asia/Makassar');
class AdminModel
{
	protected $phototelegram = ['KML', 'Mcore_1', 'Mcore_2', 'Mcore_3'];

	protected $photo_golive = ['Foto_1', 'Foto_2', 'Foto_3'];

	public static function status($id = 1)
	{
		$get_status = DB::Table('pt2_status');

		if(strcasecmp($id, 'All') != 0)
		{
			$get_status->Where('active', $id);
		}

		$get_status = $get_status->get();

		foreach($get_status as $v)
		{
			$data[$v->id] = $v->status;
		}
		return $data;
	}

	public static function telegramId($id)
	{
		// return '519446576';

		if (in_array($id, [3716, 4053]))
		{
			return '519446576';
		}
		else
		{
			return '-1001114802851';
		}
	}

	public static function regu_pt2()
	{
		return DB::table('regu')->orderBy('uraian', 'DESC')
			->get();
	}

	public static function po_select()
	{
		return DB::table('pt2_po')
			->whereNotNull('juml_odp')
			->where('juml_odp', '>', 0)
			->orwhereNull('juml_odp')
			->get();
	}

	public static function search_odp($id)
	{
		return DB::table('pt2_master')->where([['odp_nama', $id], ['delete_clm', 0]])->first();
	}

	public function bon($proaktif_id)
	{
		return DB::table('alista_material_keluar')->where('proaktif_id', $proaktif_id)->first();
	}

	public function deploybyid($id)
	{
		return DB::table('pt2_master')->leftjoin('regu', 'regu.id_regu', '=', 'pt2_master.regu_id')
			->select('pt2_master.*', 'regu.ACTIVE')
			->where('pt2_master.id', $id)->whereNotNull('project_name')
			->first();
	}

	public function lh_odp($daily, $daily2)
	{
		return DB::table('pt2_master as pm')->select('pm.regu_name as regu', 'sto', 'tgl_pengerjaan', 'lt_catatan', 'lt_status', 'odp_nama', 'tgl_selesai', 'delete_clm')
			->where('delete_clm', '=', 0)
			->whereBetween('modified_at', [$daily, $daily2])->get();
	}

	public static function odp_unsc_search_first($value, $value2)
	{
		if ($value2 == 'orderan unsc')
		{
			//mencari menggunakan id dpu
			$unsc_table = DB::TABLE('Data_Pelanggan_UNSC As dpu')->leftjoin('unsc_tinjut As ut', 'ut.DPU_ID', '=', 'dpu.ID_System')
				->select('ut.*', 'dpu.*')
				->selectRAW('(CASE WHEN dpu.orderId = 0 THEN dpu.MYIR_ID ELSE dpu.orderId END) AS unsc')
				->where('dpu.ID_System', '=', $value)->first();

			if (empty($unsc_table))
			{
				$link = "https://starclick.telkom.co.id/backend/public/api/tracking?_dc=1480967332984&ScNoss=true&SearchText=$value&Field=ORDER_ID&Fieldstatus=&Fieldwitel=&StartDate=&EndDate=&page=1&start=0&limit=1";
				$result = json_decode(@file_get_contents($link));
				$value_data['unsc'] = $result->data[0]->ORDER_ID;
				$value_data['STATUS_RESUME'] = $result->data[0]->STATUS_RESUME;
				$value_data['orderName'] = $result->data[0]->CUSTOMER_NAME;
				$value_data['koordinat_odp'] = $result->data[0]->GPS_LATITUDE . ', ' . $result->data[0]->GPS_LONGITUDE;
				$value_data['label_odp'] = $result->data[0]->LOC_ID;
				$value_data['sto_un'] = $result->data[0]->XS2;
				$value_data['kml'] = null;
				$value_data['mcore'] = null;
				$value_text = $value_data['unsc'];
				$object = json_decode(json_encode($value_data) , false);
				return $object;
			}
			else
			{
				return $unsc_table;
			}

		}
		elseif ($value2 == 'spl')
		{
			$data = DB::TABLE('pt2_master')->leftjoin('survey_pt2spl', 'pt2_master.id_referen', '=', 'survey_pt2spl.mt_id')
				->where('pt2_master.id', $value)
				->select('pt2_master.*')
				->first();
			$value_data['unsc'] = $data->id;
			$value_data['orderName'] = $data->project_name;
			$value_data['koordinat_odp'] = $data->odp_koor;
			$value_data['label_odp'] = $data->odp_nama;
			$value_data['sto_un'] = $data->sto;
			$value_data['id_referen'] = $data->id_referen;
			$files = scandir(public_path() . '/upload/files_spl/5/');
			$value_data['kml'] = $files[2];
			$value_data['mcore'] = $files[3];
			$value_text = $value_data['unsc'];
			$object = json_decode(json_encode($value_data) , false);
			return $object;
		}

	}

	public static function search_data($data, $pt2_search_jenis = 'non_id')
	{
		$end_value = strtoupper($data);
		$pt2_search = [];

		if($pt2_search_jenis == 'id' )
		{
			$pt2_search = DB::SELECT("SELECT pts.*,
				CONCAT('id') As jenis,
        (CASE WHEN tgl_selesai IS NOT NULL THEN tgl_selesai
        WHEN tgl_pengerjaan IS NOT NULL THEN tgl_pengerjaan
        ELSE tgl_buat END)as tanggal,
        (
        FLOOR(
        SUM(
        UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(tgl_pengerjaan) - 86400 * (
        (FLOOR(DATEDIFF(NOW(), tgl_pengerjaan) / 7) * 2) +
        IF(WEEKDAY(NOW()) > 5, WEEKDAY(NOW()) - 5, 0) +
        IF(WEEKDAY(NOW()) < WEEKDAY(tgl_pengerjaan), 2, 0)
        )
        ) / 86400
        )
        ) as umur,
        pl.status_laporan as sl,
        pl.id_tbl_mj as renew_id,
        concat('pt2') as note,
        r.ACTIVE as stts_regu
        FROM pt2_master pts
        LEFT JOIN regu r ON r.id_regu = pts.regu_id
        LEFT JOIN psb_laporan pl ON pts.scid=pl.id_tbl_mj
        Where pts.id = $end_value GROUP BY pts.id");

			$pt2_search_jenis = 'id';
		}
		else
		{
			if (strpos($end_value, 'ODP') !== false || preg_match('/^[a-zA-Z]+/', $end_value) )
			{
				$pt2_search = DB::SELECT("SELECT pts.*,
					(CASE WHEN tgl_selesai IS NOT NULL THEN tgl_selesai
					WHEN tgl_pengerjaan IS NOT NULL THEN tgl_pengerjaan
					ELSE tgl_buat END)as tanggal,
					(
					FLOOR(
					SUM(
					UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(tgl_pengerjaan) - 86400 * (
					(FLOOR(DATEDIFF(NOW(), tgl_pengerjaan) / 7) * 2) +
					IF(WEEKDAY(NOW()) > 5, WEEKDAY(NOW()) - 5, 0) +
					IF(WEEKDAY(NOW()) < WEEKDAY(tgl_pengerjaan), 2, 0)
					)
					) / 86400
					)
					) as umur,
					pl.status_laporan as sl,
					pl.id_tbl_mj as renew_id,
					concat('pt2') as note,
					r.ACTIVE as stts_regu
					FROM pt2_master pts
					LEFT JOIN regu r ON r.id_regu = pts.regu_id
					LEFT JOIN psb_laporan pl ON pts.scid=pl.id_tbl_mj
					Where odp_nama = '$end_value' GROUP BY pts.id");

				if (empty($pt2_search))
				{
					$pt2_search = DB::SELECT("SELECT
						dt.id as id,
						dt.id as renew_id,
						pl.odp_plan as odp_nama,
						pl.catatan as project_name,
						null as regu_name,
						null AS lt_status,
						pl.modified_at as tanggal,
						pl.catatan as catatan_HD,
						concat('psb') as note,
						null as proaktif_id,
						pls.laporan_status as status,
						pl.status_laporan as sl,
						null as odc_koor,
						null as regu_name,
						pl.kordinat_odp as odp_koor,
						(CASE WHEN pl.odp_plan IS NOT NULL AND LENGTH(pl.odp_plan) > 6 THEN SUBSTR(pl.odp_plan, 5, 3)
						WHEN my.sto IS NOT NULL THEN my.sto ELSE SUBSTR(pts.odp_nama, 5, 3) END) AS sto,
						dt.Ndem AS nomor_sc,
						1 as stts_regu
						FROM
						dispatch_teknisi dt
						LEFT JOIN psb_myir_wo my ON (dt.Ndem=my.myir OR dt.Ndem=my.sc)
						LEFT JOIN regu r ON dt.id_regu=r.id_regu
						LEFT JOIN psb_laporan pl ON dt.id=pl.id_tbl_mj
						LEFT JOIN psb_laporan_status pls ON pl.status_laporan=pls.laporan_status_id
						LEFT JOIN pt2_master pts ON pts.scid = dt.id
						Where pts.regu_id IS NULL AND pl.odp_plan = '$end_value'");
				}

				if(empty($pt2_search))
				{
					$pt2_search = DB::SELECT("SELECT pts.*,
					(CASE WHEN tgl_selesai IS NOT NULL THEN tgl_selesai
					WHEN tgl_pengerjaan IS NOT NULL THEN tgl_pengerjaan
					ELSE tgl_buat END)as tanggal,
					(
					FLOOR(
					SUM(
					UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(tgl_pengerjaan) - 86400 * (
					(FLOOR(DATEDIFF(NOW(), tgl_pengerjaan) / 7) * 2) +
					IF(WEEKDAY(NOW()) > 5, WEEKDAY(NOW()) - 5, 0) +
					IF(WEEKDAY(NOW()) < WEEKDAY(tgl_pengerjaan), 2, 0)
					)
					) / 86400
					)
					) as umur,
					ACTIVE as stts_regu,
					pl.status_laporan as sl,
					pl.id_tbl_mj as renew_id,
					concat('pt2') as note
					FROM pt2_master pts
					LEFT JOIN regu r ON pts.regu_id = r.id_regu
					LEFT JOIN dispatch_teknisi dt ON pts.scid = dt.id
					LEFT JOIN psb_laporan pl ON dt.id=pl.id_tbl_mj
					Where project_name LIKE '%$end_value%' GROUP BY pts.id");
				}
			}
			else
			{
				$pt2_search = DB::SELECT("SELECT pts.*,
					(CASE WHEN tgl_selesai IS NOT NULL THEN tgl_selesai
					WHEN tgl_pengerjaan IS NOT NULL THEN tgl_pengerjaan
					ELSE tgl_buat END)as tanggal,
					(
					FLOOR(
					SUM(
					UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(tgl_pengerjaan) - 86400 * (
					(FLOOR(DATEDIFF(NOW(), tgl_pengerjaan) / 7) * 2) +
					IF(WEEKDAY(NOW()) > 5, WEEKDAY(NOW()) - 5, 0) +
					IF(WEEKDAY(NOW()) < WEEKDAY(tgl_pengerjaan), 2, 0)
					)
					) / 86400
					)
					) as umur,
					pl.status_laporan as sl,
					pl.id_tbl_mj as renew_id,
					concat('pt2') as note,
					r.ACTIVE as stts_regu
					FROM pt2_master pts
					LEFT JOIN regu r ON r.id_regu = pts.regu_id
					LEFT JOIN psb_laporan pl ON pts.scid=pl.id_tbl_mj
					Where nomor_sc = $end_value GROUP BY pts.id");

				if (empty($pt2_search))
				{
					$pt2_search = DB::SELECT("SELECT pts.*,
						(CASE WHEN tgl_selesai IS NOT NULL THEN tgl_selesai
						WHEN tgl_pengerjaan IS NOT NULL THEN tgl_pengerjaan
						ELSE tgl_buat END)as tanggal,
						(
						FLOOR(
						SUM(
						UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(tgl_pengerjaan) - 86400 * (
						(FLOOR(DATEDIFF(NOW(), tgl_pengerjaan) / 7) * 2) +
						IF(WEEKDAY(NOW()) > 5, WEEKDAY(NOW()) - 5, 0) +
						IF(WEEKDAY(NOW()) < WEEKDAY(tgl_pengerjaan), 2, 0)
						)
						) / 86400
						)
						) as umur,
						ACTIVE as stts_regu,
						pl.status_laporan as sl,
						pl.id_tbl_mj as renew_id,
						concat('pt2') as note
						FROM pt2_master pts
						LEFT JOIN regu r ON pts.regu_id = r.id_regu
						LEFT JOIN dispatch_teknisi dt ON pts.scid = dt.id
						LEFT JOIN psb_laporan pl ON dt.id=pl.id_tbl_mj
						Where dt.Ndem = '$end_value' GROUP BY dt.id");

					if (empty($pt2_search))
					{
						$pt2_search = DB::SELECT("SELECT
							dt.id as id,
							dt.id as renew_id,
							pl.odp_plan as odp_nama,
							my.customer as project_name,
							pl.modified_at as tanggal,
							null as regu_name,
							null AS lt_status,
							pl.catatan as catatan_HD,
							concat('psb') as note,
							null as proaktif_id,
							pls.laporan_status as status,
							pl.status_laporan as sl,
							null as regu_name,
							pl.kordinat_odp as odp_koor,
							(CASE WHEN pl.odp_plan IS NOT NULL AND LENGTH(pl.odp_plan) > 6 THEN SUBSTR(pl.odp_plan, 5, 3)
							WHEN my.sto IS NOT NULL THEN my.sto ELSE SUBSTR(pts.odp_nama, 5, 3) END) AS sto,
							dt.Ndem AS nomor_sc,
							1 as stts_regu
							FROM
							dispatch_teknisi dt
							LEFT JOIN psb_myir_wo my ON (dt.Ndem=my.myir OR dt.Ndem=my.sc)
							LEFT JOIN regu r ON dt.id_regu=r.id_regu
							LEFT JOIN psb_laporan pl ON dt.id=pl.id_tbl_mj
							LEFT JOIN psb_laporan_status pls ON pl.status_laporan=pls.laporan_status_id
							LEFT JOIN pt2_master pts ON pts.scid = dt.id
							Where pts.regu_id IS NULL AND (my.myir = '$end_value' OR my.sc ='$end_value')");

						if (empty($pt2_search))
						{
							$pt2_search = DB::SELECT("SELECT
								dt.id as id,
								dt.id as renew_id,
								pl.odp_plan as odp_nama,
								my.customer as project_name,
								pl.modified_at as tanggal,
								null as regu_name,
								null AS lt_status,
								pl.catatan as catatan_HD,
								concat('psb') as note,
								pl.status_laporan as sl,
								null as proaktif_id,
								pls.laporan_status as status,
								null as regu_name,
								pl.kordinat_odp as odp_koor,
								(CASE WHEN pl.odp_plan IS NOT NULL AND LENGTH(pl.odp_plan) > 6 THEN SUBSTR(pl.odp_plan, 5, 3)
								WHEN my.sto IS NOT NULL THEN my.sto ELSE SUBSTR(pts.odp_nama, 5, 3) END) AS sto,
								dt.Ndem AS nomor_sc,
								1 as stts_regu
								FROM
								dispatch_teknisi dt
								LEFT JOIN psb_myir_wo my ON (dt.Ndem=my.myir OR dt.Ndem=my.sc)
								LEFT JOIN regu r ON dt.id_regu=r.id_regu
								LEFT JOIN psb_laporan pl ON dt.id=pl.id_tbl_mj
								LEFT JOIN psb_laporan_status pls ON pl.status_laporan=pls.laporan_status_id
								LEFT JOIN pt2_master pts ON pts.scid = dt.id
								Where pts.regu_id IS NULL AND dt.Ndem ='$end_value'");
						}
					}
				}
			}
		}
		return ['data' => $pt2_search, 'jenis' => $pt2_search_jenis];
	}

	public function lh_status($daily)
	{
		return DB::SELECT("SELECT regu_name as regu, 'sto', 'tgl_buat', 'lt_status',
      SUM(CASE status LIKE '%Completed%' THEN 1 ELSE 0 END) as selesai
      FROM pt2_master Group BY regu");
	}

	public function lh_pt2_master($daily, $daily2)
	{
		return DB::SELECT("SELECT regu_name as regu,
      COUNT(*) AS COUNT,
      SUM(CASE WHEN STATUS LIKE '%Completed%' THEN 1 ELSE 0 END) AS selesai
      FROM pt2_master WHERE delete_clm = 0 AND modified_at between '$daily' AND '$daily2' GROUP BY regu");
	}

	public static function pt2MasterByid($id)
	{
		$post = [
			'jenis' => 'id',
			'id' => 'all',
		];

		$curl = curl_init();

		curl_setopt_array($curl, [
			CURLOPT_URL => 'https://promitos.tomman.app/API_HERO_DATA',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_POSTFIELDS => $post,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
		]);

		$response = curl_exec($curl);
		curl_close($curl);

		$hero = (array)json_decode($response);
		$hero = array_values($hero);

		$hero = array_map(function($o){
			$a = get_object_vars($o);
			return $a;
		}, $hero);

		$data = DB::table('pt2_master As pm')
		->leftjoin('regu As r', 'pm.regu_id', '=', 'r.id_regu')
		->select('pm.*', 'r.is_saber')
		->where('pm.id', '=', $id)
		->first();

		$find_k = array_search($data->id_hero, array_column($hero, 'id') );
		$data_hero = $hero[$find_k];
		$data->keterangan_sdi = $data_hero['keterangan_sdi'];
		$data->flagging = $data_hero['flagging'];
		return $data;
	}

	public static function pt2MasterByscid($id)
	{
		return DB::table('pt2_master')->where('scid', '=', $id)->first();
	}

	public static function save_dispatch_unsc($req, $val)
	{
		$updated_by = Session::get('auth')->id_user;
		$regu = $req->regu;
		// $odp_po = $req->odp_po;
		$koordinat_odp = $req->no_koor_odp;
		$koordinat_odc = $req->no_koor_odc;
		$nama_odp = strtoupper($req->nama_odp);
		$pecah_1 = (explode('-', $nama_odp));
		$pecah_2 = 'ODC-' . $pecah_1[1] . '-' . $pecah_1[2];
		$pecah_3 = (explode('/', $pecah_2));
		$nama_odc = $pecah_3[0];
		$nama_project = $req->project_nama;
		$catatan_HD = $req->catatan;
		$sto = $req->sto;
		$regutable = DB::table('regu')->where('id_regu', $regu)->first();
		$order = [];
		$order['regu_id'] = $regu;
		$order['regu_name'] = $regutable->uraian;
		$order['nik1'] = $regutable->nik1;
		$order['odp_cover'] = $req->odp_cover;
		$order['nik2'] = $regutable->nik2;
		$order['odp_koor'] = $koordinat_odp;
		$order['odc_koor'] = $koordinat_odc;
		$order['odc_nama'] = $nama_odc;
		$order['odp_nama'] = $nama_odp;
		$order['catatan_HD'] = $catatan_HD;
		// $order['proaktif_id'] = $odp_po;
		$order['nomor_sc'] = $req->nomor_unsc;
		$order['delete_clm'] = 0;
		$order['sto'] = $sto;
		$order['project_name'] = $nama_project;

		if (preg_match('/^[a-z]$/', $req->tgl_kerja))
		{
			$order['tgl_pengerjaan'] = date('Y-m-d H:i:s');
		}
		else
		{
			if (strlen($req->tlg_kerja) < 10)
			{
				if ($req->tgl_kerja == date('Y-m-d'))
				{
					$order['tgl_pengerjaan'] = $req->tgl_kerja . ' ' . date('H:i:s');
				}
				else
				{
					$order['tgl_pengerjaan'] = $req->tgl_kerja . ' 08:00:00';
				}
			}
			else
			{
				$order['tgl_pengerjaan'] = $req->tgl_kerja;
			}
		}

		// $check_po = DB::table('pt2_po')->where('id_project_Ta', $odp_po)->first();

		$urgent_msg = [];

		DB::transaction(function () use ($order, $updated_by, $req, $val, &$urgent_msg)
		{
			$id = $message = $mr_msg = $tim = '';
			$check_data = DB::TABLE('pt2_master')->where('id', $req->id)->first();
			$regu = $order['regu_name'];

			if (!isset($check_data) )
			{
				//insert
				if ($val)
				{
					DB::table('psb_laporan')->where('id_tbl_mj', $val)->update(['modified_at' => date('Y-m-d H:i:s') , 'status_laporan' => 75]);
				}

				$id .= DB::table('pt2_master')->insertGetId(['scid' => $val]);

				DB::table('pt2_master')->where('scid', $val)->update($order);

				DB::table('pt2_master')->where('scid', $val)->update(['status' => 'Construction', 'tgl_buat' => date('Y-m-d H:i:s') , 'Ndem' => 'DEP' . $id]);

				// if ((!empty($check_po->juml_odp)) || $check_po->juml_odp == 0)
				// {
				// 	$hasil = $check_po->juml_odp - 1;
				// }

				// DB::table('pt2_po')
				// 	->where('id_project_Ta', '=', $order['proaktif_id'])->update(['juml_odp' => $hasil]);

				DB::table('pt2_dispatch')->insert([
					'id_order'           => $id,
					'regu_id'            => $order['regu_id'],
					'keterangan'         => 'Dispatch Regu',
					'tgl_kerja_dispatch' => $order['tgl_pengerjaan'],
					'updated_by'         => $updated_by
				]);

				$tim .= "<b>Tim</b> : $regu\n";
			}
			else
			{
				$id .= $check_data->id;

				// if ($order['proaktif_id'] != $check_data->proaktif_id)
				// {
				// 	if ((!empty($check_po->juml_odp)) || $check_po->juml_odp == 0)
				// 	{
				// 		$hasil = $check_po->juml_odp - 1;

				// 		DB::table('pt2_po')
				// 			->where('id_project_Ta', '=', $order['proaktif_id'])->update(['juml_odp' => $hasil]);
				// 	}

				// 	if ((!empty($check_po->juml_odp)) || $check_po->juml_odp == 0)
				// 	{
				// 		$before = $check_po->juml_odp + 1;

				// 		DB::table('pt2_po')
				// 			->where('id_project_Ta', '=', $check_data->proaktif_id)
				// 			->update(['juml_odp' => $before]);
				// 	}

				// }

				if ($check_data->regu_id != $req->regu)
				{
					$mr_msg .= "<b>Pergantian Regu WO dari Tim</b> : $check_data->regu_name\n";
					$tim .= "<b>Tim Baru</b> : $regu\n";
				}

				DB::table('pt2_master')->where('id', $req->id)
					->update($order);

				if ($check_data->lt_status == 'Kendala')
				{
					DB::table('pt2_master')->where('id', '=', $req->id)->update([
						'status' => 'Construction',
						'lt_status' => null
					]);
				}

				DB::table('pt2_master')->where('id', '=', $req->id)->update([
					'tgl_pengerjaan' => $order['tgl_pengerjaan']
				]);

				DB::table('pt2_dispatch')->insert([
					'id_order'           => $check_data->id,
					'regu_id'            => $order['regu_id'],
					'tgl_kerja_dispatch' => $order['tgl_pengerjaan'],
					'keterangan'         => 'Pergantian Regu',
					'status'             => $check_data->lt_status,
					'updated_by'         => $updated_by
				]);
			}

			$telegram_data = DB::table('pt2_master')->where('id', '=', $id)->first();
			$message .= "<b>Di Dispatch Oleh ID User</b> : $updated_by \n";
			$message .= "======================== \n";
			$message .= "<b>$telegram_data->regu_name</b> \n";
			$message .= "<b>Tanggal Pekerjaan</b> : $req->tgl_kerja\n";
			$message .= "<b>Nama Project</b> : $telegram_data->project_name\n";
			$message .= $mr_msg;
			$message .= $tim;
			$message .= "<b>Catatan</b> : $telegram_data->catatan_HD\n \n \n \n ";
			$message .= "<b>Nama ODP</b> : $telegram_data->odp_nama\n";
			$message .= "<b>Koordinat ODP</b> \n";
			$message .= "<a href=\"maps.google.com/?q=$telegram_data->odp_koor\">$telegram_data->odp_koor </a>\n";
			$message .= "<b>Nama ODC</b> : $telegram_data->odc_nama\n";
			$message .= "<b>Koordinat ODC</b> \n";
			$message .= "<a href=\"maps.google.com/?q=$telegram_data->odc_koor\">$telegram_data->odc_koor </a>\n";

			try
			{
				Telegram::sendMessage([
					'chat_id' => self::telegramId($id) ,
					'parse_mode' => 'html',
					'text' => "$message"
				]);
			}
			catch(\Symfony\Component\HttpFoundation\File\Exception\FileException $e)
			{
				$urgent_msg['msg'] = ['type' => 'danger', 'text' => 'Laporan Tidak Terkirim, Lapor Grup Agar selalu up-to-date!'];
			}

			$that = new AdminModel();

			$that->handleFileUpload($req, $id);
			$phototelegram_s = $that->phototelegram;

			for ($i = 0;$i < count($phototelegram_s);$i++)
			{

				$file = public_path() . "/upload/pt_2_3/$id/$phototelegram_s[$i].jpg";

				if (file_exists($file))
				{
					try
					{
						Telegram::sendPhoto([
							// 'chat_id' => '519446576', -1001114802851
							'chat_id' => self::telegramId($id) ,
							'caption' => $phototelegram_s[$i],
							'photo' => $file
						]);
					}
					catch(\Symfony\Component\HttpFoundation\File\Exception\FileException $e)
					{
						$urgent_msg['foto'] = ['type' => 'danger', 'text' => 'Foto Tidak Terkirim, Lapor Grup Agar selalu up-to-date!'];
					}
				}

			}
		});
		return $urgent_msg;
	}

	public static function save_dispatch_non_unsc($req, $value, $add_data = [])
	{
		$check_data = DB::TABLE('pt2_master')->where('id', $value)->first();

		$updated_by = Session::get('auth')->id_user;
		// $nama_odp = strtoupper($req->nama_odp);
		// $pecah_1 = (explode('-', $nama_odp));
		// $pecah_2 = 'ODC-' . @$pecah_1[1] . '-' . @$pecah_1[2];
		// $pecah_3 = (explode('/', $pecah_2));
		// $nama_odc = @$pecah_3[0];
		$nama_project     = $req->project_nama;
		$catatan_HD       = $req->catatan;
		$sto              = $req->sto;
		$order            = [];
		$order['regu_id'] = $req->regu;

		if($req->jenis_wo)
		{
			$order['jenis_wo'] = $req->jenis_wo;
		}
		else
		{
			$order['jenis_wo'] = $req->j_pekerjaan;
		}

		if($order['jenis_wo'] == 'PT-2')
		{
			$order['jenis_wo'] = 'PT-2 SIMPLE';
		}

		if($req->regu)
		{
			$regutable = DB::table('regu')->where('id_regu', $req->regu)->first();
			$order['regu_name']         = $regutable->uraian;
			$order['nik1']              = $regutable->nik1;
			$order['nik2']              = $regutable->nik2;
		}

		$order['odp_cover']         = $req->odp_cover;
		// $order['odp_nama']          = $nama_odp;
		$order['odp_koor']          = $req->no_koor_odp;
		// $order['odc_nama']          = $nama_odc;
		$order['catatan_HD']        = $catatan_HD;
		// $order['proaktif_id']    = $odp_po;
		if($req->nomor_id)
		{
			$no_sc = trim($req->nomor_id);
			$order['nomor_sc'] = $no_sc;

			if($check_data)
			{
				$split_sc = explode(',', $check_data->nomor_sc_log);

				if(!in_array($no_sc, $split_sc) )
				{
					array_push($split_sc, $no_sc);

					$order['nomor_sc_log'] = implode(',', $split_sc);
				}
			}
		}

		if($check_data && $check_data->id_hero == 0 || !$check_data)
		{
			$order['kategory_non_unsc'] = $req->j_order;
		}

		switch ($req->j_order)
		{
			case 6:
				$order['pelanggan_koor'] = $req->no_koor_pelanggan;
			break;
			case 7:
				$order['odp_nama_before'] = $req->nama_odp_before;
				$order['odp_koor_before'] = $req->no_koor_odp_before;
			break;
		}

		$order['delete_clm']   = 0;
		$order['sto']          = $sto;
		$order['project_name'] = $nama_project;

		if (preg_match('/^[a-z]$/', $req->tgl_kerja))
		{
			$order['tgl_pengerjaan'] = date('Y-m-d H:i:s');
		}
		else
		{
			if (strlen($req->tlg_kerja) < 10)
			{

				if ($req->tgl_kerja == date('Y-m-d'))
				{
					$order['tgl_pengerjaan'] = $req->tgl_kerja . ' ' . date('H:i:s');
				}
				else
				{
					$order['tgl_pengerjaan'] = $req->tgl_kerja . ' 08:00:00';
				}
			}
			else
			{
				$order['tgl_pengerjaan'] = $req->tgl_kerja;
			}
		}
		// dd($order, $value);
		$urgent_msg = [];

		$id = '';

		$that2 = new AdminModel();

		DB::transaction(function () use (&$id, $order, $that2, $updated_by, $req, &$urgent_msg, $add_data, $check_data)
		{
			$terminal = $selected = $terminal_delete = [];

			$that = new AdminController();

			if($add_data)
			{
				switch ($add_data['jenis']) {
					case 'NOG':
						$nama_odp = strtoupper($add_data['data']->nama_odp);

						$pecah_1  = (explode('-', $nama_odp) );
						$pecah_2  = 'ODC-' . @$pecah_1[1] . '-' . @$pecah_1[2];
						$pecah_3  = (explode('/', $pecah_2) );
						$nama_odc = @$pecah_3[0];

						$order['odp_nama'] = $nama_odp;
						$order['odc_nama'] = $nama_odc;
					break;
					case 'PSB':
						$nama_odp = strtoupper($add_data['data']->nama_odp);

						if($nama_odp)
						{
							$pecah_1  = (explode('-', $nama_odp) );
							$pecah_2  = 'ODC-' . @$pecah_1[1] . '-' . @$pecah_1[2];
							$pecah_3  = (explode('/', $pecah_2) );
							$nama_odc = @$pecah_3[0];

							$order['odp_nama']          = $nama_odp;
							$order['odc_nama']          = $nama_odc;
							$order['nomor_sc']          = $add_data['data']->orderId;
							$order['kategory_non_unsc'] = 6;
						}
					break;
					case 'KPRO_SIMPLE':
						$nama_odp = strtoupper(explode(' ', $add_data['data']->odp)[0]);

						if($nama_odp)
						{
							$pecah_1  = (explode('-', $nama_odp) );
							$pecah_2  = 'ODC-' . @$pecah_1[1] . '-' . @$pecah_1[2];
							$pecah_3  = (explode('/', $pecah_2) );
							$nama_odc = @$pecah_3[0];

							$order['odp_nama']          = $nama_odp;
							$order['odc_nama']          = $nama_odc;
							$order['nomor_sc']          = $add_data['data']->order_id;
							$order['kategory_non_unsc'] = 6;
						}
					break;
				}
			}

			if($check_data)
			{
				if($check_data->id_hero != 0)
				{
					$order['kategory_non_unsc'] = $check_data->kategory_non_unsc;

					// DB::connection('bpp')->table('hero_master')->Where('id', $check_data->id_hero)->update([
					// 	'flagging' => $req->j_pekerjaan
					// ]);

					DB::table('hero_master')->Where('id', $check_data->id_hero)->update([
						'flagging' => $req->j_pekerjaan
					]);
				}

				if($req->change_odp)
				{
					if($check_data->odp_nama)
					{
						$get_tenos = explode('/', $check_data->odp_nama);
						$get_odp = explode('-', $get_tenos[0]);

						$terminal_delete[] = (object)['jenis_terminal' => $get_odp[0], 'koordinat' => $req->no_koor_odp, 'sto' => $get_odp[1], 'odc' => $get_odp[2], 'index_terminal' => $get_tenos[1] ];
					}

					$terminal[] = (object)['jenis_terminal' => 'ODP', 'koordinat' => $req->no_koor_odp];
				}

				if($terminal_delete)
				{
					$that->delete_booking('PT-2', json_encode($terminal_delete) );
				}
			}
			else
			{
				if(!array_key_exists('odp_nama', $order) )
				{
					$terminal[] = (object)['jenis_terminal' => 'ODP', 'koordinat' => $req->no_koor_odp];
				}
			}

			if(@!$check_data->aspl_nama && $req->booking_aspl)
			{
				$terminal[] = (object)['jenis_terminal' => 'ASPL', 'koordinat' => $req->no_koor_aspl];
				$order['aspl_koor'] = $req->no_koor_aspl;
			}

			if($req->nama_odp_selected_id)
			{
				$selected[] = (object)['id' => $req->nama_odp_selected_id, 'koordinat' => $req->no_koor_odp];
			}

			if($terminal)
			{
				$get_data   = $that->booking_odp(json_encode($terminal), 'PT-2', $req->sto, $req->nama_odc, $req->project_nama, $order['regu_id'], json_encode($selected) );
				$get_data   = json_decode($get_data);
				$array_data = json_decode(json_encode($get_data->booked), TRUE);

				$find_key_odp = array_search('ODP', array_column($array_data, 'jenis_terminal') );
				$item = [];

				if($find_key_odp !== FALSE)
				{
					$data_book = $array_data[$find_key_odp];
					$order['odp_nama'] = $data_book['jenis_terminal'] .'-'. $data_book['sto'] .'-'. $data_book['odc'] .'/'. $data_book['index_terminal'];

					$order['odc_nama'] = 'ODC-'. $data_book['sto'] .'-'. $data_book['odc'];
					$order['id_ml']    = $data_book['mitos_lop_id'];

					$item[] = 'ODP '. $order['odp_nama'];
				}

				$find_key_aspl = array_search('ASPL', array_column($array_data, 'jenis_terminal') );

				if($find_key_aspl !== FALSE)
				{
					$data_book = $array_data[$find_key_aspl];
					$order['aspl_nama'] = $data_book['jenis_terminal'] .'-'. $data_book['sto'] .'-'. $data_book['odc'] .'/'. $data_book['index_terminal'];

					$item[] = 'ASPL '. $order['aspl_nama'];
				}

				$urgent_msg['msg'] = ['type' => 'success', 'text' => 'Pekerjaan berhasil diolah dengan '.implode(' dan ', $item).'!'];
			}

			if (!isset($check_data) )
			{
				//insert

				$order['status'] = 'Construction';

				if($add_data)
				{
					switch ($add_data['jenis']) {
						case 'NOG':
							$order['id_maint'] = $add_data['data']->mt_id;
						break;
						case 'PSB':
							$order['scid'] = $add_data['data']->id_dispatch;
							$order['Ndem'] = $add_data['data']->orderId;
						break;
					}
				}

				$order['tgl_buat'] = date('Y-m-d H:i:s');

				$split_odc = explode('-', $order['odc_nama']);

				$get_info_odc = ReportModel::get_info_odc($split_odc[2], $split_odc[1]);

				if(count($get_info_odc) )
				{
					$order['odc_koor'] = $get_info_odc[0]->latitude .','. $get_info_odc[0]->longitude;
				}

				$id = DB::table('pt2_master')->insertGetId($order);

				// if ((!empty($check_po->juml_odp)) || $check_po->juml_odp == 0)
				// {
				// 	$pobef = DB::table('pt2_po')->where('id_project_Ta', '=', $order['proaktif_id'])->first();
				// 	$hasil = $pobef->juml_odp - 1;
				// 	DB::table('pt2_po')
				// 		->where('id_project_Ta', '=', $order['proaktif_id'])->update(['juml_odp' => $hasil]);
				// }

				DB::table('pt2_dispatch')->insert([
					'id_order'           => $id,
					'regu_id'            => $order['regu_id'],
					'keterangan'         => 'Dispatch Regu',
					'tgl_kerja_dispatch' => $order['tgl_pengerjaan'],
					'updated_by'         => $updated_by
				]);
			}
			else
			{
				$id = $check_data->id;

				// if ($order['proaktif_id'] != $check_data->proaktif_id)
				// {
				// 	if ((!empty($check_po->juml_odp)) || $check_po->juml_odp == 0)
				// 	{
				// 		$hasil = $check_po->juml_odp - 1;

				// 		DB::table('pt2_po')
				// 			->where('id_project_Ta', '=', $order['proaktif_id'])->update(['juml_odp' => $hasil]);
				// 	}

				// 	if ((!empty($check_po->juml_odp)) || $check_po->juml_odp == 0)
				// 	{
				// 		$before = $check_po->juml_odp + 1;

				// 		DB::table('pt2_po')
				// 			->where('id_project_Ta', '=', $check_data->proaktif_id)
				// 			->update(['juml_odp' => $before]);
				// 	}
				// }

				if($check_data->aspl_nama && !$req->booking_aspl)
				{
					$get_aspl = explode('/', $check_data->aspl_nama);
					$get_aspl_data = explode('-', $get_aspl[0]);

					// DB::Table('mitos_master_odp')->where([
					// 	['jenis_terminal', 'ASPL'],
					// 	['sto', $get_aspl_data[1] ],
					// 	['odc', $get_aspl_data[2] ],
					// 	['index_terminal', $get_aspl[1] ]
					// ])->update([
					// 	'status' => 'Free'
					// ]);
				}

				DB::table('pt2_master')->where('id', $check_data->id)->update($order);

				if ($check_data->lt_status == 'Kendala')
				{
					DB::table('pt2_master')->where('id', '=', $check_data->id)->update([
						'status' => 'Construction',
						'lt_status' => null
					]);
				}

				DB::table('pt2_dispatch')->insert([
					'id_order'           => $check_data->id,
					'regu_id'            => $order['regu_id'],
					'tgl_kerja_dispatch' => $order['tgl_pengerjaan'],
					'keterangan'         => 'Pergantian Regu',
					'status'             => $check_data->lt_status,
					'updated_by'         => $updated_by
				]);
			}

			$that2->handleFileUpload($req, $id);
			$that2->handleFileUpload_kml($req, $id);
		});

		$message = '';

		if ($check_data && $check_data->regu_id != $req->regu)
		{
			$message .= "<b>Pergantian Regu WO</b>\n";
		}

		$telegram_data = DB::table('pt2_master')->where('id', '=', $id)->first();
		$message .= "<b>Di Dispatch Oleh ID User</b> : $updated_by \n";
		$message .= "======================== \n";
		$message .= "<b>$telegram_data->regu_name</b> \n";
		$message .= "<b>Tanggal Pekerjaan</b> : $req->tgl_kerja\n";
		$message .= "<b>Nama Project</b> : $telegram_data->project_name\n";

		if ($req->no_koor_pelanggan)
		{
			$message .= "<b>Koordinat Pelanggan</b> : $req->no_koor_pelanggan\n";
		}

		$message .= "<b>Tim</b> : $telegram_data->regu_name\n";
		$message .= "<b>Catatan</b> : " . nl2br($telegram_data->catatan_HD, false) . "\n \n \n \n";

		if ($req->j_order == 7 && isset($req->nama_odp_before))
		{
			$message .= "<b>Nama ODP Sebelumnya</b>: $req->nama_odp_before \n";
		}

		$message .= "<b>Nama ODP</b> : $telegram_data->odp_nama\n";

		if($telegram_data->aspl_nama)
		{
			$message .= "<b>Nama ASPL</b> : $telegram_data->aspl_nama\n";
		}

		if ($req->no_koor_odp_before)
		{
			$message .= "<b>Koordinat ODP Sebelumnya</b>: $req->no_koor_odp_before \n";
			$message .= "<a href=\"maps.google.com/?q=$req->no_koor_odp_before\">$req->no_koor_odp_before </a>\n";
		}

		if($telegram_data->nomor_sc_log)
		{
			$message .= "<b>Nomor SC yang Ikut</b>: $telegram_data->nomor_sc_log \n";
		}

		$message .= "<b>Koordinat ODP</b> \n";
		$message .= "<a href=\"maps.google.com/?q=$telegram_data->odp_koor\">$telegram_data->odp_koor </a>\n";
		$message .= "<b>Nama ODC</b> : $telegram_data->odc_nama\n";
		$message .= "<b>Koordinat ODC</b> \n";
		$message .= "<a href=\"maps.google.com/?q=$telegram_data->odc_koor\">$telegram_data->odc_koor </a>\n";

		try
		{
			Telegram::sendMessage([
				'chat_id' => self::telegramId($id) ,
				'parse_mode' => 'html',
				'text' => "$message"
			]);

			$check_path = public_path() . '/upload/pt_2_3/' . $id . '/';
			$rfc_file = @preg_grep('~^File KML.*$~', scandir($check_path) );

			if(count($rfc_file) != 0)
			{
				$files = array_values($rfc_file);
				$file_kml = public_path().'/upload/pt_2_3/' . $id . '/'.$files[0];

				if (file_exists($file_kml) )
				{
					Telegram::sendDocument([
						// 'chat_id' => '519446576',
						'chat_id' => self::telegramId($id),
						'caption' => 'File KML',
						'document' => $file_kml
					]);
				}
			}

		}
		catch(\Symfony\Component\HttpFoundation\File\Exception\FileException $e)
		{
			$urgent_msg['msg'] = ['type' => 'danger', 'text' => 'Laporan Tidak Terkirim, Lapor Grup Agar selalu up-to-date!'];
		}

		$phototelegram_s = $that2->phototelegram;

		for ($i = 0;$i < count($phototelegram_s);$i++)
		{
			$file = public_path() . "/upload/pt_2_3/$id/$phototelegram_s[$i].jpg";
			if (file_exists($file))
			{
				try
				{
					Telegram::sendDocument([
						// 'chat_id' => '519446576', -1001114802851
						'chat_id' => self::telegramId($id),
						'caption' => $phototelegram_s[$i],
						'document' => $file
					]);
				}
				catch(\Symfony\Component\HttpFoundation\File\Exception\FileException $e)
				{
					$urgent_msg['foto'] = ['type' => 'danger', 'text' => 'Foto Tidak Terkirim, Lapor Grup Agar selalu up-to-date!'];
				}

			}
		}
		return $urgent_msg;
	}

	private function handleFileUpload($req, $id)
	{
		foreach ($this->phototelegram as $name)
		{
			$input = 'photo-' . $name;

			if ($req->hasFile($input))
			{
				$path = public_path() . '/upload/pt_2_3/' . $id;
				if (!file_exists($path))
				{
					if (!mkdir($path, 0770, true))
					{
						return 'gagal menyiapkan folder foto evidence';
					}
				}
				$file = $req->file($input);
				$ext = 'jpg';
				try
				{
					$moved = $file->move("$path", "$name.$ext");

					$img = new \Imagick($moved->getRealPath());

					$img->scaleImage(100, 150, true);
					$img->writeImage("$path/$name-th.$ext");
				}
				catch(\Symfony\Component\HttpFoundation\File\Exception\FileException $e)
				{
					return 'gagal menyimpan foto evidence ' . $name;
				}
			}
		}
	}

	private function handleFileUpload_kml($req, $id)
	{
		if ($req->hasFile('kml_file'))
		{
			$path = public_path() . '/upload/pt_2_3/' . $id;
			if (!file_exists($path))
			{
				if (!mkdir($path, 0770, true))
				{
					return 'gagal menyiapkan folder foto evidence';
				}
			}
			$file = $req->file('kml_file');
			try
			{
				$filename = 'File KML.'.strtolower( $file->getClientOriginalExtension() );
				$file->move("$path", "$filename");
			}
			catch(\Symfony\Component\HttpFoundation\File\Exception\FileException $e)
			{
				return 'gagal menyimpan file evidence KML';
			}
		}
	}

	public function ogp($id, $locate)
	{
		return DB::table('pt2_dispatch')
		->leftjoin('pt2_master', 'pt2_master.id', '=', 'pt2_dispatch.id_order')
		->where([
			['updated_by', '=', $id],
			['pt2_master.lt_status', '=', 'Ogp'],
			['pt2_master.sto', '=', $locate],
			['delete_clm', '=', 0]
		])->get();
	}

	public static function list_regu_pt2($jns)
	{
		if ($jns == 'aktif')
		{
			$jns_id = 1;
		}
		else
		{
			$jns_id = 0;
		}

		return DB::table('regu')->select('regu.*')
			->selectRaw(" (select nama from 1_2_employee where nik = nik1) as nama1,
      (select nama from 1_2_employee where nik = nik2) as nama2,
      (select nama from 1_2_employee where nik = TL) as namatl")
			->where([
				['job', 'PT2'],
				['ACTIVE', $jns_id]
			])->orderBy('uraian', 'DESC')
			->get();
	}

	public static function parent_regu()
	{
		return DB::table('regu')
			->select('TL')
			->selectRaw(" (select nama from 1_2_employee where nik = nik1) as nama1,
      (select nama from 1_2_employee where nik = nik2) as nama2,
      (select nama from 1_2_employee where nik = TL) as namatl")
			->where([['job', 'PT2'], ['ACTIVE', 1]])
			->groupBy('TL')
			->orderBy('TL', 'DESC')
			->get();
	}

	public static function get_history_regu($id)
	{
		return DB::table('pt2_master')->leftjoin('regu', 'regu.id_regu', '=', 'pt2_master.regu_id')
			->select('lt_status', 'ACTIVE')
			->selectRaw('COUNT(*) as total')
			->where('regu_id', $id)->groupby('lt_status')
			->get();
	}

	public function delete_Regu($id)
	{
		DB::table('regu')->where('id_regu', $id)->update(['ACTIVE' => 0]);
	}

	public function edit_regu($id)
	{
		return DB::table('regu')->where('id_regu', $id)->first();
	}

	public function update_regu($id, $req)
	{
		$sttf = '';

		if ($req->check_stat)
		{
			$sttf .= '(STTF) ';
		}

		DB::table('regu')->where('id_regu', $id)->update([
			'uraian' => $sttf . '' . $req->uraian,
			'nik1'   => $req->nik1,
			'mitra'  => $req->mitra,
			'TL'     => $req->nik_tl,
			'sto'    => $req->sto,
			'nik2'   => $req->nik2,
			'ACTIVE' => $req->ACTIVE
		]);

		DB::table('pt2_master')
		->where('regu_id', $id)
		->where(function($join){
			$join->whereNull('lt_status')
			->OrwhereNotIn('lt_status', ['Selesai', 'Kendala']);
		})
		->update([
		'regu_name' => $req->uraian,
		'nik1'      => $req->nik1,
		'nik2'      => $req->nik2
		]);
	}

	public function simpan_regu($req)
	{
		DB::transaction(function () use ($req)
		{
			$sttf = '';
			$is_saber = 0;

			if ($req->check_stat)
			{
				$sttf .= '(STTF) ';
			}
			if ($req->saber)
			{
				$is_saber = 1;
			}

			DB::table('regu')->insert([
				'uraian'   => $sttf . '' . $req->uraian,
				'is_saber' => $is_saber,
				'nik1'     => $req->nik1,
				'job'      => 'PT2',
				'mitra'    => $req->mitra,
				'TL'       => $req->nik_tl,
				'sto'      => $req->sto,
				'nik2'     => $req->nik2,
				'ACTIVE'   => 1
			]);

			$data_nik1 = DB::table('user')->where('id_user', '=', $req->nik1)
				->first();
			if (empty($data_nik1))
			{
				DB::table('user')->insert([
					'id_user'     => $req->nik1,
					'password'    => md5('telkomakses123') ,
					'level'       => 10,
					'id_karyawan' => $req->nik1,
					'witel'       => 1,
					'status'      => 0,
					'pt2_level'   => 0
				]);
			}

			$data_nik2 = DB::table('user')->where('id_user', '=', $req->nik2)
				->first();
			if (empty($data_nik2))
			{
				DB::table('user')->insert([
					'id_user'     => $req->nik2,
					'password'    => md5('telkomakses123') ,
					'level'       => 10,
					'id_karyawan' => $req->nik2,
					'witel'       => 1,
					'status'      => 0,
					'pt2_level'   => 0
				]);
			}
		});
	}

	public function regu_search($search)
	{
		return DB::table('maintenance_datel')->where('sto', 'like', '%' . $search . '%')->get();
	}

	public function sto()
	{
		return DB::table('sdi_datel')
			->where([['regional', '=', 6], ['datel', '!=', '0DATEL']])
			->orderBy('sto', 'ASC')
			->get();
	}

	public static function prog_Saldo()
	{
		$q = DB::table('pt2_master')->whereNotNull('regu_id')->where(function ($join)
		{
			$join->where('lt_status', '!=', 'Kendala')
				->where('lt_status', '!=', '')
				->orWhereNull('lt_status');
		})
			->where('delete_clm', 0)
			->orderBy('id', 'desc');

		return DB::Select("SELECT *,
      FROM pt2_master
      LEFT JOIN pt2_dispatch ON pt2_master.id = pt2_dispatch.id_order
      LEFT JOIN regu ON pt2_master.regu_id = regu.id_regu AND delete_clm IN (0,2) AND ( lt_status IS NULL OR lt_status = 'Tidak Sempat') AND (pt2_dispatch.status IS NULL OR pt2_dispatch.status = 'Tidak Sempat')
      ORDER BY id DESC ");
		return $q->get();
	}

	public static function add_unsc_psb($id)
	{
		return DB::SELECT("SELECT
      dt.id as scid,
      my.customer as project_name,
      pl.odp_plan as odp_nama,
      pl.kordinat_odp as koordinat_odp,
      (CASE WHEN pl.odp_plan IS NOT NULL AND LENGTH(pl.odp_plan) > 6 THEN SUBSTR(pl.odp_plan, 5, 3)
      WHEN my.sto IS NOT NULL THEN my.sto ELSE f.area END) AS sto,
      (CASE WHEN dt.Ndem IS NOT NULL THEN dt.Ndem
      WHEN e.myir IS NOT NULL THEN e.myir
      ELSE e.orderId END) AS nomor_sc
      FROM
      dispatch_teknisi dt
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      LEFT JOIN Data_Pelanggan_Starclick e ON dt.Ndem = e.orderId
      LEFT JOIN mdf f ON e.sto = f.mdf
      LEFT JOIN psb_myir_wo my ON (dt.Ndem=my.myir OR dt.Ndem=my.sc)
      WHERE
      pl.id_tbl_mj = $id") [0];
	}

	public static function getAllOrder($req)
	{
		$q = $v = $data = [];
		$sql = '';

		$mitra = session('auth')->mitra;

		if(Session('auth')->pt2_level == 3)
		{
			$sql ="AND mitra ='$mitra'";
		}

		if($req->val)
		{
			if(in_array("'need_progres'", $req->val) )
			{
				$q[] ="lt_status NOT IN ('Ogp', 'Selesai', 'Kendala')";
			}

			$v = implode(', ', $req->val);
			$q[] = "lt_status IN ($v)";

			$q = 'AND ('. implode(' OR ', $q) . ')';
			$tgl_awal = $req->tgl[0];
			$tgl_ah = $req->tgl[1];

			$data = DB::SELECT("SELECT * FROM pt2_master
			LEFT JOIN regu r ON pt2_master.regu_id = r.id_regu
			WHERE odp_nama != '' AND delete_clm = 0 $sql AND DATE_FORMAT(modified_at, '%Y-%m-%d') >= '$tgl_awal' && DATE_FORMAT(modified_at, '%Y-%m-%d') <= '$tgl_ah' $q");
		}

		return $data;
	}

	public static function getAllOrder_unsc($jenis)
	{

		$data = [];
		$no = 0;

		if($jenis)
		{
			if(in_array('unsc', $jenis) )
			{
				$sql = DB::Table('kpro_evaluasi_unsc As keu')
				->leftjoin('pt2_master As pm', 'pm.nomor_sc', '=', 'keu.order_id')
				->select('keu.*')
				->WhereNull('pm.id')
				->Where('lat', '!=', '')
				->Where('long', '!=', '')
				->WhereIn('order_id', [
					'511025928',
					'512711499',
					'526360922',
					'526585932',
					'526445232',
					'512324854',
					'506277115',
					'512219670',
					'514494107',
					'524876410',
					'525089377',
					'521998711',
					'525033298',
					'524548620',
					'524938496',
					'513946865',
					'513947791',
					'525054238',
					'522675886',
					'524135204',
					'522152080',
					'524135393',
					'524938675',
					'524394588',
					'522333507',
					'524769338',
					'524299082',
					'524299146',
					'521466095',
					'524876374',
					'521323760',
					'524135465',
					'526517287',
					'524805773',
					'524887691',
					'525071551',
					'519991579',
					'524135245',
					'525071830',
					'525071294',
					'525071347',
					'525089345',
					'524234953',
					'524830316',
					'524887736',
					'524695129',
					'521988265',
					'524969305',
					'524501157',
					'524887645',
					'526223628',
					'525054402',
					'524135496',
					'524135476',
					'519149500',
					'526371541',
					'526323507',
					'526442389',
					'526428008',
					'526296660',
					'526428911',
					'526103810',
					'526313833',
					'526553954',
					'526711789',
					'526690878',
					'526107778',
					'526147698',
					'526643231',
					'526667312',
					'526314454',
					'526122776',
					'526302506',
					'526342210',
					'526716521',
					'526626882',
					'526505964',
					'526506049',
					'526611707',
					'516775347',
					'526675081',
					'509802576',
					'526333416',
					'526266241',
					'526174923',
					'526126059',
					'525986330',
					'526588500'
				])
				->get();

				foreach($sql as $v)
				{
					$data[$no]['jenis']        = 'unsc';
					$data[$no]['koor']         = $v->lat .','. $v->long;
					$data[$no]['project_name'] = $v->order_id;
					$data[$no]['odp_nama']     = $v->alpro;
					$data[$no]['sto']          = $v->sto;
					$data[$no]['alamat']       = $v->alamat;
					$data[$no]['nama_cust']    = $v->nama_cust;
					++$no;
				}
			}

			if(in_array('near_odp', $jenis) )
			{
				$get_go_live = DB::Table('pt2_master')
				->Where('GOLIVE', 1)
				->Where(DB::RAW("DATE(tgl_selesai)"), '>=', '2020-01-01')
				->WhereNotNull('odp_koor')
				->get();

				foreach($get_go_live as $v)
				{
					++$no;
					$data[$no]['jenis']        = 'go_live';
					$data[$no]['koor']         = $v->odp_koor;
					$data[$no]['project_name'] = $v->project_name;
					$data[$no]['odp_nama']     = $v->odp_nama;
					$data[$no]['sto']          = $v->sto;
					$data[$no]['tgl_selesai']  = $v->tgl_selesai;
					$data[$no]['lt_status']    = $v->lt_status;
				}
			}
		}

		return $data;
	}

	public function telegram_sent_dispatch()
	{
		$sto = DB::SELECT("SELECT (maintenance_datel.sto) as tes_sto,
      maintenance_datel.*
      FROM maintenance_datel LEFT JOIN pt2_master on pt2_master.sto = maintenance_datel.sto
      WHERE delete_clm = 0 AND pt2_master.regu_id IS NOT NULL AND lt_status IS NULL AND status IS NOT NULL AND YEAR(tgl_pengerjaan) = YEAR(CURDATE()) AND tgl_pengerjaan <= NOW()
      GROUP BY maintenance_datel.sto ORDER BY `pt2_master`.`id` DESC");

		$database = DB::SELECT('SELECT *
			FROM pt2_master LEFT JOIN maintenance_datel on pt2_master.sto = maintenance_datel.sto
			WHERE delete_clm = 0 AND pt2_master.regu_id IS NOT NULL AND lt_status IS NULL AND status IS NOT NULL AND YEAR(tgl_pengerjaan) = YEAR(CURDATE()) AND tgl_pengerjaan <= NOW() ORDER BY `pt2_master`.`id` DESC');

		// $chat_id = '-1001114802851';
		$chat_id = '519446576';
		foreach ($sto as $no => $new)
		{
			if (!empty($new))
			{
				$list[$no] = '';
				$list[$no] .= "Daftar ODP Belum Update \n";
				$list[$no] .= "STO $new->sto \n";
				foreach ($database as $telegram_data)
				{
					if ($new->sto == $telegram_data->sto)
					{
						$list[$no] .= "🚨<b> $telegram_data->regu_name</b> : $telegram_data->project_name\n <b> $telegram_data->odp_koor </b>";
					}
				}
				Telegram::sendMessage([
					'chat_id'    => $chat_id,
					'parse_mode' => 'html',
					'text'       => "$list[$no]"
				]);

			}
			else
			{
				Telegram::sendMessage([
				  'chat_id'    => $chat_id,
				  'parse_mode' => 'html',
				  'text'       => 'Semua Orderan Sudah Tuntas! 👍'
				]);

			}
		}
	}

	public function send_laporan()
	{
		exec('cd /srv/htdocs/puppet_rend;nodejs pt2_odp_naik_M.js ');
		$file = '/srv/htdocs/d_liauw/pt2_dev/public/month_graph_pt2.png';
		// dd($file)
		// $chat_id = '519446576';
		// $chat_id = '-1001114802851';
		$chat_id = '-586733089';
		if (file_exists($file))
		{
			Telegram::sendPhoto([
				'chat_id' => $chat_id,
				'caption' => 'Laporan Matrix Grafik Bulan ' . date('F', strtotime(date('Y-m'))) ,
				'photo' => $file,
			]);
		}

		exec('cd /srv/htdocs/puppet_rend;nodejs pt2_odp_naik_D.js ');
		$file = '/srv/htdocs/d_liauw/pt2_dev/public/daily_graph_pt2.png';
		// dd($file)
		if (file_exists($file))
		{
			Telegram::sendPhoto([
				'chat_id' => $chat_id,
				'caption' => 'Laporan Matrix Grafik Harian ' . date('Y-m-d') ,
				'photo' => $file,
			]);
		}

		exec('cd /srv/htdocs/puppet_rend;nodejs pt2_M_up.js ');
		$file = '/srv/htdocs/d_liauw/pt2_dev/public/month_up_pt2.png';
		// dd($file)
		if (file_exists($file))
		{
			Telegram::sendPhoto([
				'chat_id' => $chat_id,
				'caption' => 'Laporan Produktifitas PT-2 ' . date('Y-m-d') ,
				'photo'   => $file
			]);
		}

		exec('cd /srv/htdocs/puppet_rend;nodejs ranked_matrix.js ');
		$file = '/srv/htdocs/d_liauw/pt2_dev/public/ranked_matrix_monthly.png';
		// dd($file)
		if (file_exists($file))
		{
			Telegram::sendPhoto([
				'chat_id' => $chat_id,
				'caption' => 'Laporan Matrix Ranked Bulan ' . date('F', strtotime(date('Y-m') ) ) ,
				'photo'   => $file
			]);
		}
	}

	public function eksekusi_pending_order()
	{
		DB::table('pt2_master')->where('lt_status', '=', 'Pending')
				->update(['lt_status' => null, 'tgl_pengerjaan' => date('Y-m-d') . ' 06:00:00']);
	}

	public function eksekusi_tidak_smpt_order()
	{
		DB::table('pt2_master')
		->whereNull('lt_status')
		->update(['lt_status' => 'Tidak Sempat']);
	}

	public static function delete_per_odp($id)
	{

		DB::table('pt2_master')->where('id', $id)->update(['delete_clm' => 1]);
		$get_data = DB::table('pt2_master')->where('id', $id)->first();

		if($get_data->id_hero != 0)
		{
			// DB::connection('bpp')->table('hero_master')->Where('id', $get_data->id_hero)->update([
			// 	'flagging' => null
			// ]);

			DB::table('hero_master')->Where('id', $get_data->id_hero)->update([
				'flagging' => null
			]);
		}
	}

	public static function create_zip($data)
	{
		$id = round(microtime(true) * 1000);
		\File::copyDirectory((public_path() . '/upload/pt_2_3/teknisi/' . $data) , (public_path() . '/upload/pt_2_3/teknisi/' . $data . '_copyan_' . $id));
		$nama = DB::table('pt2_master')->where('id', $data)->first();
		$tes = new TeknisiController();
		$photo_Save = $tes->photo;

		foreach ($photo_Save as $pho)
		{
			$file = public_path() . '/upload/pt_2_3/teknisi/' . $data . '_copyan_' . $id . '/' . $pho . '.jpg';
			if (file_exists($file))
			{
				if ($pho != 'Port_Feeder' && $pho != 'Port_Distribusi')
				{
					$file_text = public_path() . '/upload/pt_2_3/teknisi/' . $data . '_copyan_' . $id . '/' . $pho . '-catatan.txt';

					if (file_exists($file_text))
					{
						$note = \File::get(public_path() . '/upload/pt_2_3/teknisi/' . $data . '_copyan_' . $id . '/' . $pho . '-catatan.txt');
            $note = preg_replace("/[\/\&%#\$]/", '', $note);

						\File::move((public_path() . '/upload/pt_2_3/teknisi/' . $data . '_copyan_' . $id . '/' . $pho . '.jpg') , (public_path() . '/upload/pt_2_3/teknisi/' . $data . '_copyan_' . $id . '/'. $pho . ' ' . $note . '.jpg'));
						\File::delete(public_path() . '/upload/pt_2_3/teknisi/' . $data . '_copyan_' . $id . '/' . $pho . '-catatan.txt');
					}
				}
				else
				{
					$file_port = public_path() . '/upload/pt_2_3/teknisi/' . $data . '_copyan_' . $id . '/' . $pho . '-port.txt';
					$file_panel = public_path() . '/upload/pt_2_3/teknisi/' . $data . '_copyan_' . $id . '/' . $pho . '-panel.txt';
					if (file_exists($file_port))
					{
						$note_port = \File::get(public_path() . '/upload/pt_2_3/teknisi/' . $data . '_copyan_' . $id . '/' . $pho . '-port.txt');
					}
					else
					{
						$note_port = 'Tidak Ada';
					}

					if (file_exists($file_panel))
					{
						$note_panel = \File::get(public_path() . '/upload/pt_2_3/teknisi/' . $data . '_copyan_' . $id . '/' . $pho . '-panel.txt');
					}
					else
					{
						$note_panel = 'Tidak Ada';
					}

					\File::move((public_path() . '/upload/pt_2_3/teknisi/' . $data . '_copyan_' . $id . '/' . $pho . '.jpg') , (public_path() . '/upload/pt_2_3/teknisi/' . $data . '_copyan_' . $id . '/' . $pho . ' Panel ' . $note_panel . ', Port ' . $note_port . '.jpg'));

					if (file_exists($file_port))
					{
						\File::delete(public_path() . '/upload/pt_2_3/teknisi/' . $data . '_copyan_' . $id . '/' . $pho . '-port.txt');
					}

					if (file_exists($file_panel))
					{
						\File::delete(public_path() . '/upload/pt_2_3/teknisi/' . $data . '_copyan_' . $id . '/' . $pho . '-panel.txt');
					}
				}
				\File::delete(public_path() . '/upload/pt_2_3/teknisi/' . $data . '_copyan_' . $id . '/' . $pho . '-th.jpg');
			}
		}

		$rootPath = public_path() . '/upload/pt_2_3/teknisi/' . $data . '_copyan_' . $id;

		$new_name_file = null;

		if(file_exists($rootPath) )
		{
			$namel = str_replace('/', '-', "$nama->odp_nama");
			$new_name_file = $namel . ' ' . $nama->project_name . '.zip';

			$zip = new ZipArchive();
			$zip->open($new_name_file, ZipArchive::CREATE | ZipArchive::OVERWRITE);

			$files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($rootPath) , RecursiveIteratorIterator::LEAVES_ONLY);

			foreach ($files as $name => $file)
			{
				if (!$file->isDir())
				{
					$filePath = $file->getRealPath();
					$relativePath = substr($filePath, strlen($rootPath) + 1);
					$zip->addFile($filePath, $file->GetFileName());
				}
			}
			$zip->close();
			\File::deleteDirectory($rootPath);
		}

		return $new_name_file;
	}

	public static function download_photo_categ($data)
	{
		$nama_file = self::create_zip($data);
		$response = null;

		if(!empty($nama_file) )
		{
			$headers =['Content-type: application/zip'];

			$response = response()->download(public_path() .'/'. $nama_file, $nama_file, $headers);
			register_shutdown_function('unlink', public_path() .'/'. $nama_file);
		}
		return $response;
	}

	public static function list_regu_pt2_jointer()
	{
		$sql = DB::table('user As u')
		->Leftjoin('1_2_employee As emp', 'u.id_user', '=', 'emp.nik')
		->select('emp.*')
		->where([
			['u.is_jointer', 1],
			['emp.ACTIVE', 1],
		])
		->WhereNotNull('emp.nik');

		if(session('auth')->pt2_level == 8 )
		{
			$sql->where('emp.atasan_1', session('auth')->id_user);
		}

		$sql = $sql->get();


		foreach($sql as $v)
		{
			// $fd = [];

			// $fd['uraian']      = $v->nama .' ('. $v->nik .')';
			// $fd['TL']          = $v->atasan_1;
			// $fd['mitra']       = $v->mitra_amija;
			// $fd['job']         = 'PT2';
			// $fd['nik1']        = $v->nik;
			// $fd['nama1']       = $v->nama;
			// $fd['status_team'] = 'ACTIVE';
			// // DB::table('regu')->insert($fd);
		}
		return $sql;
	}

	public static function all_waspang_joint()
	{
		$sql = DB::table('user As u')
		->Leftjoin('1_2_employee As emp', 'u.id_user', '=', 'emp.nik')
		->select('emp.nama', 'u.id_user', 'emp.Witel_New', 'emp.ACTIVE')
		->where('pt2_level', 8);

		if(session('auth')->pt2_level == 8 )
		{
			$sql->where('emp.nik', session('auth')->id_user);
		}

		return $sql->get();
	}

	public static function get_all_emp()
	{
		return DB::Table('1_2_employee')
		->select("nik", 'nama')
		->get();
	}

	public static function download_pdf_rfc_teknisi($data)
	{
		$rootPath = (public_path() . '/upload/pt_2_3/teknisi/' . $data . '/file_upload_RFC.pdf');
		$headers = ['Content-Type' => 'application/pdf'];

		return response()->download($rootPath, 'file_upload_RFC.pdf', $headers);
	}

	public static function getRekap_year($year, $month)
	{
		$date = $year . '-' . $month;
		$id_tl = Session::get('auth')->id_user;
		if ($month != 0)
		{
			return DB::Select("SELECT tgl_selesai as tgl, pt2_master.sto as sto_n,
			(CASE WHEN lt_status = 'Berangkat' THEN DATE_FORMAT(tgl_pengerjaan, '%Y-%m') ELSE DATE_FORMAT(tgl_selesai, '%Y-%m') END) as tgl_mee,
        SUM(CASE WHEN lt_status = 'Kendala' THEN 1 ELSE 0 END ) as kendala,
        SUM(CASE WHEN lt_status = 'Selesai' THEN 1 ELSE 0 END ) as Selesai,
        SUM(CASE WHEN lt_status = 'Ogp' THEN 1 ELSE 0 END ) as ogp,
        SUM(CASE WHEN lt_status = 'Pending' THEN 1 ELSE 0 END ) as pending,
        SUM(CASE WHEN lt_status = 'Berangkat' THEN 1 ELSE 0 END ) as jalan,
        SUM(CASE WHEN lt_status IS NOT NULL THEN 1 ELSE 0 END ) as total
        FROM pt2_master
				LEFT JOIN regu ON pt2_master.regu_id = regu.id_regu
				WHERE regu.TL = '$id_tl' AND delete_clm = 0 AND (CASE WHEN lt_status = 'Berangkat' THEN DATE_FORMAT(tgl_pengerjaan, '%Y-%m') ELSE DATE_FORMAT(tgl_selesai, '%Y-%m') END) = '$date'
				GROUP BY tgl_mee, pt2_master.sto ORDER BY tgl_selesai DESC");
		}
		else
		{
			return DB::Select("SELECT tgl_selesai as tgl, pt2_master.sto as sto_n,
			(CASE WHEN lt_status = 'Berangkat' THEN DATE_FORMAT(tgl_pengerjaan, '%Y-%m') ELSE DATE_FORMAT(tgl_selesai, '%Y-%m') END) as tgl_mee,
        SUM(CASE WHEN lt_status = 'Kendala' THEN 1 ELSE 0 END ) as kendala,
        SUM(CASE WHEN lt_status = 'Selesai' THEN 1 ELSE 0 END ) as Selesai,
        SUM(CASE WHEN lt_status = 'Ogp' THEN 1 ELSE 0 END ) as ogp,
        SUM(CASE WHEN lt_status = 'Pending' THEN 1 ELSE 0 END ) as pending,
        SUM(CASE WHEN lt_status = 'Berangkat' THEN 1 ELSE 0 END ) as jalan,
        SUM(CASE WHEN lt_status IS NOT NULL THEN 1 ELSE 0 END ) as total
        FROM pt2_master
				LEFT JOIN regu ON pt2_master.regu_id = regu.id_regu
				WHERE regu.TL = '$id_tl' AND delete_clm = 0  AND YEAR(tgl_selesai) = $year
				GROUP BY tgl_mee, pt2_master.sto ORDER BY tgl_selesai DESC");
		}
	}

	public static function getRekap_year_detail($year, $month)
	{
		$id_tl = Session::get('auth')->id_user;
		if ($month != 0)
		{
			return DB::Select("SELECT project_name, odp_nama, tgl_selesai, pt2_master.sto, regu_id, id_regu, TL, regu_name, lt_status, lt_catatan, scid, lt_koordinat_odp, pt2_master.id
          FROM pt2_master LEFT JOIN regu ON pt2_master.regu_id = regu.id_regu
          WHERE regu.TL = '$id_tl' AND delete_clm = 0  AND YEAR(tgl_selesai) = $year AND month(tgl_selesai) LIKE '%$month%' ORDER BY tgl_Selesai ASC");
		}
		else
		{
			return DB::Select("SELECT project_name, odp_nama, tgl_selesai, pt2_master.sto, regu_id, regu_name, id_regu, TL, lt_status, lt_catatan, scid, lt_koordinat_odp, pt2_master.id
        FROM pt2_master LEFT JOIN regu ON pt2_master.regu_id = regu.id_regu
        WHERE regu.TL = '$id_tl' AND delete_clm = 0  AND YEAR(tgl_selesai) = $year ORDER BY tgl_Selesai ASC");
		}
	}

	public static function sentTele($nama)
	{
		$file = public_path() . '/' . $nama;
		Telegram::sendPhoto([
			// 'chat_id' => '519446576',
			'chat_id' => '-1001114802851',
			'caption' => 'Foto Matrix' . date('Y-m-d H:i:s') ,
			'photo'   => $file,
			]);

	}

	public static function chart_golive($m1, $m2, $l)
	{
		return DB::Select("SELECT MONTH(tgl_selesai) AS tanggal, tgl_selesai,
      SUM(CASE WHEN mo.ODP_NAME IS NOT NULL THEN 1 ELSE 0) AS go_live,
      SUM(CASE WHEN mo.ODP_NAME IS NULL THEN 1 ELSE 0) AS not_go_live
      FROM pt2_master LEFT JOIN 1_0_master_odp mo ON pt2_master.odp_nama = mo.ODP_NAME
      WHERE tgl_selesai BETWEEN '$m1' AND '$m2' AND pt2_master.sto = '$l' AND MONTH(tgl_selesai) = tanggal AND tgl_selesai IS NOT NULL
      GROUP BY MONTH(tgl_selesai) HAVING go_live > 0 OR not_go_live > 0
      ORDER BY tgl_selesai DESC");
	}

	public static function bank_odp()
	{
		return DB::table('pt2_master')->where('status', '=', 'BisaPT2')
			->get();
	}

	public static function finish_list($jenis)
	{
		if ($jenis == 'go_live')
		{
			$stts_golive = 0;
		}
		else
		{
			$stts_golive = 1;
		}

		$data = DB::TABLE('pt2_master As pts')
		->select('pts.*')
		->where([['status', 'Completed']])
		->Where('pts.GOLIVE', $stts_golive)
		->orderby('tgl_selesai', 'DESC')
		->get();

		return $data;
	}

	public static function ajax_select2_golive($search)
	{
		return DB::SELECT("SELECT
      dt.id as orderId,
      (CASE WHEN my.myir IS NOT NULL THEN my.myir ELSE 'Tidak ada MYIR' END) AS myir,
      (CASE WHEN my.sc IS NOT NULL THEN my.sc ELSE 'Tidak ada SC' END) AS sc,
      pl.odp_plan as odp_plan,
      pl.id as id_pl,
      pls.laporan_status_id,
      pl.kordinat_odp as koordinat_odp,
      my.customer as project_name
      FROM
      dispatch_teknisi dt
      LEFT JOIN psb_myir_wo my ON (dt.Ndem = my.myir OR dt.Ndem = my.sc)
      LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
      LEFT JOIN psb_laporan_status pls ON pl.status_laporan = pls.laporan_status_id
      Where dt.Ndem LIKE '$search%' ");
	}

	public static function recovery_data($j, $id)
	{
		if ($j == 'remove')
		{
			DB::table('pt2_master')->where('id', $id)->update(['delete_clm' => 0, 'tgl_buat' => date('Y-m-d H:i:s') ]);
		}
		elseif ($j == 'age')
		{
			DB::table('pt2_master')->where('id', $id)->update(['tgl_buat' => date('Y-m-d H:i:s') ]);
		}
	}

	public static function getRegu_ajx($data)
	{
		return DB::table('user')->leftjoin('1_2_employee', 'user.id_user', '=', '1_2_employee.nik')
			->select('user.id_user', '1_2_employee.nama')
			->where('user.id_user', 'LIKE', '%' . $data . '%')->get();
	}

	public static function mitra_pt2()
	{
		return DB::table('mitra_amija')
			->select('mitra_amija as id', 'mitra_amija as text')
			->where('witel', 'KALSEL')
			->get();
	}

	public static function status_header_dashboard($tgl_a, $tgl_b, $jenis_wo)
	{
		$id_stts = implode(', ', array_keys(self::status() ) );
		$data_b = [];
		$sql = '';

		if($jenis_wo)
		{
			$sql .= " AND jenis_wo = '$jenis_wo' ";
		}

		$date_nya = "AND (CASE
		WHEN tgl_selesai IS NOT NULL THEN DATE(pt2_master.tgl_selesai) BETWEEN '$tgl_a' AND '$tgl_b'
		WHEN tgl_pengerjaan IS NOT NULL THEN DATE(pt2_master.tgl_pengerjaan) BETWEEN '$tgl_a' AND '$tgl_b'
		WHEN tgl_buat IS NOT NULL THEN DATE(pt2_master.tgl_buat) BETWEEN '$tgl_a' AND '$tgl_b'
		ELSE 1 = 1
		END)";

		$data_b = DB::SELECT("SELECT
		SUM(CASE WHEN (pt2_master.regu_id IS NOT NULL AND (lt_status NOT IN ('Selesai', 'Kendala') ) OR lt_status IS NULL) THEN 1 ELSE 0 END) as Sisa_Order,
		SUM(CASE WHEN lt_status IS NULL THEN 1 ELSE 0 END) as Need_Progres,
		SUM(CASE WHEN lt_status = 'Selesai' THEN 1 ELSE 0 END) as Selesai,
		SUM(CASE WHEN lt_status = 'Selesai' AND GOLIVE = 0 AND upload_abd_4 = 0 THEN 1 ELSE 0 END) as belum_abd,
		SUM(CASE WHEN lt_status = 'Pending' THEN 1 ELSE 0 END) as Pending,
		SUM(CASE WHEN lt_status = 'Kendala' THEN 1 ELSE 0 END) as Kendala
		FROM pt2_master
		LEFT JOIN regu ON regu_id = id_regu
		WHERE delete_clm = 0 $sql $date_nya") [0];

		$header = new \stdClass();

		$header->Sisa_Order  = $data_b->Sisa_Order;
		$header->Need_Progres = $data_b->Need_Progres;
		$header->Selesai      = $data_b->Selesai;
		$header->Pending      = $data_b->Selesai;
		$header->Kendala      = $data_b->Kendala;
		return $header;
	}

	public static function status_table_dsh($tgl_a, $tgl_b, $jenis_wo)
	{
		$id_stts = implode(', ', array_keys(self::status() ) );

		$sql = '';

		if($jenis_wo)
		{
			$sql .= " AND jenis_wo = '$jenis_wo' ";
		}

		$date_nya = "AND (CASE
		WHEN pts.tgl_selesai IS NOT NULL THEN DATE(pts.tgl_selesai) BETWEEN '$tgl_a' AND '$tgl_b'
		WHEN pts.tgl_pengerjaan IS NOT NULL THEN DATE(pts.tgl_pengerjaan) BETWEEN '$tgl_a' AND '$tgl_b'
		WHEN pts.tgl_buat IS NOT NULL THEN DATE(pts.tgl_buat) BETWEEN '$tgl_a' AND '$tgl_b'
		ELSE 1 = 1
		END)";

		$data_pt2_all = DB::SELECT("SELECT maintenance_datel.datel,
		SUM(CASE WHEN pts.id IS NOT NULL THEN 1 ELSE 0 END) As total_order,
		SUM(CASE WHEN (pts.regu_id IS NOT NULL AND (lt_status NOT IN ('Selesai', 'Kendala') ) OR lt_status IS NULL) THEN 1 ELSE 0 END) as Sisa_Order,
		SUM(CASE WHEN pts.regu_id IS NOT NULL AND lt_status IS NULL THEN 1 ELSE 0 END) as Need_Progres,
		SUM(CASE WHEN pts.regu_id IS NULL AND lt_status IS NULL THEN 1 ELSE 0 END) as undispatch,
		SUM(CASE WHEN lt_status IN ('Ogp', 'On Progress') THEN 1 ELSE 0 END) as Ogp,
		SUM(CASE WHEN lt_status = 'Berangkat' THEN 1 ELSE 0 END) as Berangkat,
		SUM(CASE WHEN lt_status = 'Pending' THEN 1 ELSE 0 END) as Pending,
		SUM(CASE WHEN lt_status = 'Kendala' THEN 1 ELSE 0 END) as Kendala,
		SUM(CASE WHEN lt_status = 'Selesai' AND GOLIVE = 0 THEN 1 ELSE 0 END) as Selesai_fisik,
		SUM(CASE WHEN lt_status = 'Selesai' AND GOLIVE = 0 AND upload_abd_4 = 0 THEN 1 ELSE 0 END) as belum_abd,
		SUM(CASE WHEN lt_status = 'Selesai' AND GOLIVE = 1 THEN 1 ELSE 0 END) as selesai_golive,
		SUM(CASE WHEN lt_status = 'Selesai' AND GOLIVE = 1 AND lanjut_PT1 = 1 THEN 1 ELSE 0 END) as lanjut_pt1
		FROM maintenance_datel
		LEFT JOIN pt2_master pts ON pts.sto = maintenance_datel.sto
		LEFT JOIN regu r ON r.id_regu = pts.regu_id
		WHERE delete_clm = 0 AND datel IS NOT NULL AND datel != 'test' $sql $date_nya
		GROUP BY maintenance_datel.datel
		");

		$all_data = [];

		foreach ($data_pt2_all as $key1 => $val1)
		{
			$all_data[$key1] = $val1;
		}

		$data_b['all_data'] = $all_data;

		$status = self::status('all');

		$date_nya = "AND (CASE
		WHEN pt2_master.tgl_selesai IS NOT NULL THEN DATE(pt2_master.tgl_selesai) BETWEEN '$tgl_a' AND '$tgl_b'
		WHEN pt2_master.tgl_pengerjaan IS NOT NULL THEN DATE(pt2_master.tgl_pengerjaan) BETWEEN '$tgl_a' AND '$tgl_b'
		WHEN pt2_master.tgl_buat IS NOT NULL THEN DATE(pt2_master.tgl_buat) BETWEEN '$tgl_a' AND '$tgl_b'
		ELSE 1 = 1
		END)";

		foreach ($status as $key => $val)
		{
			$data_b[$val] = DB::SELECT("SELECT maintenance_datel.datel,
			SUM(CASE WHEN pt2_master.id IS NOT NULL THEN 1 ELSE 0 END) As total_order,
			SUM(CASE WHEN (pt2_master.regu_id IS NOT NULL AND (lt_status NOT IN ('Selesai', 'Kendala') ) OR lt_status IS NULL) THEN 1 ELSE 0 END) as Sisa_Order,
			SUM(CASE WHEN pt2_master.regu_id IS NOT NULL AND lt_status IS NULL THEN 1 ELSE 0 END) as Need_Progres,
			SUM(CASE WHEN pt2_master.regu_id IS NULL AND lt_status IS NULL THEN 1 ELSE 0 END) as undispatch,
			SUM(CASE WHEN lt_status IN('Ogp', 'On Progress') THEN 1 ELSE 0 END) as Ogp,
			SUM(CASE WHEN lt_status = 'Berangkat' THEN 1 ELSE 0 END) as Berangkat,
			SUM(CASE WHEN lt_status = 'Pending' THEN 1 ELSE 0 END) as Pending,
			SUM(CASE WHEN lt_status = 'Kendala' THEN 1 ELSE 0 END) as Kendala,
			SUM(CASE WHEN lt_status = 'Selesai' AND GOLIVE = 0 THEN 1 ELSE 0 END) as Selesai_fisik,
			SUM(CASE WHEN lt_status = 'Selesai' AND GOLIVE = 0 AND upload_abd_4 = 0 THEN 1 ELSE 0 END) as belum_abd,
			SUM(CASE WHEN lt_status = 'Selesai' AND GOLIVE = 1 THEN 1 ELSE 0 END) as selesai_golive,
			SUM(CASE WHEN lt_status = 'Selesai' AND GOLIVE = 1 AND lanjut_PT1 = 1 THEN 1 ELSE 0 END) as lanjut_pt1
			FROM maintenance_datel
			LEFT JOIN pt2_master ON maintenance_datel.sto = pt2_master.sto
			LEFT JOIN regu r ON pt2_master.regu_id = r.id_regu
			WHERE delete_clm = 0 AND kategory_non_unsc = $key AND datel IS NOT NULL AND datel != 'test' $date_nya
			GROUP BY datel
			HAVING Sisa_Order != 0 OR Need_Progres != 0 OR Ogp != 0 OR Pending != 0 OR Kendala != 0 OR Selesai_fisik != 0 OR selesai_golive != 0");
		}

		return $data_b;
	}

	public static function get_curve_data($tgl_a, $tgl_b, $jenis_wo)
	{
		$id_stts = implode(', ', array_keys(self::status() ) );

		$sql = '';

		if($jenis_wo)
		{
			$sql .= " AND jenis_wo = '$jenis_wo' ";
		}

		$date_nya = "AND (CASE
		WHEN pts.tgl_selesai IS NOT NULL THEN DATE(pts.tgl_selesai) BETWEEN '$tgl_a' AND '$tgl_b'
		WHEN pts.tgl_pengerjaan IS NOT NULL THEN DATE(pts.tgl_pengerjaan) BETWEEN '$tgl_a' AND '$tgl_b'
		WHEN pts.tgl_buat IS NOT NULL THEN DATE(pts.tgl_buat) BETWEEN '$tgl_a' AND '$tgl_b'
		ELSE 1 = 1
		END)";

		$data = DB::SELECT("SELECT maintenance_datel.datel, DATE_FORMAT(pts.tgl_selesai, '%m') As bulan_selesai, DATE_FORMAT(pts.tgl_pengerjaan, '%m') As bulan_kerja, DATE_FORMAT(pts.modified_at, '%m') As bulan_modif,
		SUM(CASE WHEN (pts.regu_id IS NOT NULL AND (lt_status NOT IN ('Selesai', 'Kendala') ) OR lt_status IS NULL) AND kategory_non_unsc IN($id_stts) THEN 1 ELSE 0 END) as Sisa_Order,
		SUM(CASE WHEN pl.status_laporan != 75 AND pts.regu_id IS NOT NULL AND (lt_status NOT IN ('Kendala', 'Selesai') OR lt_status IS NULL) THEN 1 ELSE 0 END) as not_pt2,
		SUM(CASE WHEN lt_status = 'Ogp' THEN 1 ELSE 0 END) as Ogp,
		SUM(CASE WHEN lt_status = 'Pending' THEN 1 ELSE 0 END) as Pending,
		SUM(CASE WHEN lt_status = 'Kendala' THEN 1 ELSE 0 END) as Kendala,
		SUM(CASE WHEN lt_status = 'Selesai' AND GOLIVE = 0 THEN 1 ELSE 0 END) as Selesai_fisik,
		SUM(CASE WHEN lt_status = 'Selesai' AND GOLIVE = 0 AND upload_abd_4 = 0 THEN 1 ELSE 0 END) as belum_abd,
		SUM(CASE WHEN pts.regu_id IS NOT NULL AND lt_status IS NULL THEN 1 ELSE 0 END) as Need_Progres,
		0 as Selesai_pt2,
		SUM(CASE WHEN lt_status = 'Selesai' AND GOLIVE = 1 THEN 1 ELSE 0 END) as selesai_golive
		FROM maintenance_datel
		LEFT JOIN pt2_master pts ON pts.sto = maintenance_datel.sto
		LEFT JOIN regu r ON r.id_regu = pts.regu_id
		LEFT JOIN psb_laporan pl ON pts.scid = pl.id_tbl_mj
		WHERE delete_clm = 0 $sql $date_nya
		GROUP BY pts.id");

		$bulan = array(
			'01' => 'Januari',
			'02' => 'Februari',
			'03' => 'Maret',
			'04' => 'April',
			'05' => 'Mei',
			'06' => 'Juni',
			'07' => 'Juli',
			'08' => 'Agustus',
			'09' => 'September',
			'10' => 'Oktober',
			'11' => 'November',
			'12' => 'Desember',
		);

		foreach($bulan as $k => $v)
		{
			$final_data[(int)$k]['bulan'] = $v;
			$final_data[(int)$k]['selesai'] = 0;
			$final_data[(int)$k]['kendala'] = 0;
		}

		foreach($final_data as $k => $v)
		{
			$find_k_selesai = array_keys(array_column($data, 'bulan_selesai'), $k);

			foreach($find_k_selesai as $vv)
			{
				$final_data[(int)$k]['selesai'] += (int)$data[$vv]->Selesai_fisik + (int)$data[$vv]->selesai_golive;
				$final_data[(int)$k]['kendala'] += (int)$data[$vv]->Kendala;
			}
		}

		return $final_data;
	}

	public static function list_dsh($jenis, $datel, $tgl_a, $tgl_b)
	{
		$datel_list = ReportModel::sdi_datel();
		$data = new \stdClass();

		if (preg_match('/(?i)(unsc_psb|all_data)/', $jenis))
		{
			if (preg_match('/(?i)all/', $datel))
			{
				$sql_dtl = '';
			}
			else
			{
				$sql_dtl = "AND sd.datel = '$datel'";
			}

			$cari = DB::SELECT("SELECT * FROM maintenance_datel WHERE datel = '$datel'");
			$sto = "'" . implode("','", array_column($cari, 'sto')) . "'";

			if ($jenis == 'unsc_psb')
			{
				$sql_jenis = 'kategory_non_unsc = 0 AND';
				$sql_select = "0 as Sisa_Order,";
			}
			elseif ($jenis == 'all_data')
			{
				$id_stts = implode(', ', array_keys(self::status() ) );
				$sql_jenis = "kategory_non_unsc IN ($id_stts) AND";
				$sql_select = "SUM(CASE WHEN ( (regu_id IS NOT NULL AND lt_status NOT IN ('Selesai', 'Kendala') ) OR lt_status IS NULL) AND kategory_non_unsc IN ($id_stts) THEN 1 ELSE 0 END) as Sisa_Order,";
			}

			$selesai_pt2 = DB::SELECT("SELECT
			sd.datel,
			sd.sto,
			COUNT(dt.id) as hitung
			FROM
			dispatch_teknisi dt
			LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
			LEFT JOIN pt2_master pts ON pts.scid = pl.id_tbl_mj
			LEFT JOIN regu r ON r.id_regu = pts.regu_id
			LEFT JOIN maintenance_datel sd ON sd.sto = pts.sto
			WHERE
			$sql_jenis pts.lt_status ='Selesai' AND (DATE(tgl_selesai) BETWEEN '$tgl_a' AND '$tgl_b') $sql_dtl
			GROUP BY sd.sto");

			$date_nya = "AND (CASE
			WHEN pts.tgl_selesai IS NOT NULL THEN DATE(pts.tgl_selesai) BETWEEN '$tgl_a' AND '$tgl_b'
			WHEN pts.tgl_pengerjaan IS NOT NULL THEN DATE(pts.tgl_pengerjaan) BETWEEN '$tgl_a' AND '$tgl_b'
			WHEN pts.tgl_buat IS NOT NULL THEN DATE(pts.tgl_buat) BETWEEN '$tgl_a' AND '$tgl_b'
			ELSE 1 = 1
			END)";

			$data_a = DB::SELECT("SELECT sd.datel, sd.sto,
			$sql_select
			0 as WO_tersedia,
			SUM(CASE WHEN pl.status_laporan != 75 AND pts.regu_id IS NOT NULL AND (lt_status NOT IN ('Kendala', 'Selesai') OR lt_status IS NULL) THEN 1 ELSE 0 END) as not_pt2,
			SUM(CASE WHEN lt_status = 'Ogp' THEN 1 ELSE 0 END) as Ogp,
			SUM(CASE WHEN lt_status = 'Pending' THEN 1 ELSE 0 END) as Pending,
			SUM(CASE WHEN lt_status = 'Berangkat' THEN 1 ELSE 0 END) as Berangkat,
			SUM(CASE WHEN lt_status = 'Kendala' THEN 1 ELSE 0 END) as Kendala,
			SUM(CASE WHEN lt_status = 'Kendala' AND kendala_detail = 'Distribusi Full' THEN 1 ELSE 0 END) AS DF,
			SUM(CASE WHEN lt_status = 'Kendala' AND kendala_detail = 'Feeder Reti' THEN 1 ELSE 0 END) AS FT,
			SUM(CASE WHEN lt_status = 'Kendala' AND kendala_detail = 'Core Ccd' THEN 1 ELSE 0 END) AS CC,
			SUM(CASE WHEN lt_status = 'Kendala' AND kendala_detail = 'Tercover Odp Lain' THEN 1 ELSE 0 END) AS TOL,
			SUM(CASE WHEN lt_status = 'Kendala' AND kendala_detail = 'Kendala Jalur Tidak Ada Tiang Kosong' THEN 1 ELSE 0 END) AS KK,
			SUM(CASE WHEN lt_status = 'Kendala' AND kendala_detail = 'Kendala Cuaca' THEN 1 ELSE 0 END) AS KC,
			SUM(CASE WHEN lt_status = 'Kendala' AND kendala_detail = 'Cancel' THEN 1 ELSE 0 END) AS C,
			SUM(CASE WHEN lt_status = 'Kendala' AND kendala_detail = 'Feeder Full' THEN 1 ELSE 0 END) AS FF,
			SUM(CASE WHEN lt_status = 'Kendala' AND kendala_detail = 'Olt Full' THEN 1 ELSE 0 END) AS OF,
			SUM(CASE WHEN lt_status = 'Selesai' THEN 1 ELSE 0 END) as Selesai_fisik,
			SUM(CASE WHEN pts.regu_id IS NOT NULL AND lt_status IS NULL THEN 1 ELSE 0 END) as Need_Progres,
			0 as Selesai_pt2,
			SUM(CASE WHEN lt_status = 'Selesai' AND GOLIVE = 1 THEN 1 ELSE 0 END) as selesai_golive,
			SUM(CASE WHEN lt_status = 'Selesai' AND GOLIVE = 0 THEN 1 ELSE 0 END) as selesai_blum_golive
			SUM(CASE WHEN lt_status = 'Selesai' AND GOLIVE = 0 AND upload_abd_4 = 0 THEN 1 ELSE 0 END) as belum_abd,
			FROM maintenance_datel sd
			LEFT JOIN pt2_master pts ON sd.sto = pts.sto
			LEFT JOIN regu r ON pts.regu_id = r.id_regu
			LEFT JOIN psb_laporan pl ON pts.scid = pl.id_tbl_mj
			WHERE ($sql_jenis delete_clm = 0) $sql_dtl $date_nya
			GROUP BY sd.sto");

			$data_psb = DB::SELECT("SELECT
			SUM(CASE WHEN (lt_status NOT IN ('Selesai', 'kendala') AND pts.regu_id IS NOT NULL) OR lt_status IS NULL THEN 1 ELSE 0 END) as Sisa_Order,
			SUM(CASE WHEN pts.regu_id IS NULL THEN 1 ELSE 0 END) as WO_tersedia,
			(CASE WHEN pl.odp_plan IS NOT NULL AND LENGTH(pl.odp_plan) > 6 THEN SUBSTR(pl.odp_plan, 5, 3)
			WHEN my.sto IS NOT NULL THEN my.sto
			WHEN my2.sto IS NOT NULL THEN my2.sto ELSE f.area END) AS sto_B
			FROM
			dispatch_teknisi dt
			LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
			LEFT JOIN Data_Pelanggan_Starclick e ON dt.Ndem = e.orderId
			LEFT JOIN mdf f ON e.sto = f.mdf
			LEFT JOIN pt2_master pts ON pts.scid = pl.id_tbl_mj
			LEFT JOIN regu r ON r.id_regu = pts.regu_id
			LEFT JOIN psb_myir_wo my ON dt.Ndem=my.myir
			LEFT JOIN psb_myir_wo my2 ON my2.sc = dt.Ndem
			WHERE
			pl.status_laporan = 75 AND (dt.dispatch_by = '5' OR dt.dispatch_by IS NULL) AND (ACTIVE = 1 AND pts.id IS NOT NULL OR ACTIVE = 0 AND pts.id IS NULL) AND ( (DATE(pl.modified_At) BETWEEN '$tgl_a' AND '$tgl_b') OR DATE(pts.modified_at) BETWEEN '$tgl_a' AND '$tgl_b') AND pts.delete_clm = 0 GROUP BY sto_B HAVING sto_B IN($sto)");

			foreach ($data_a as $key1 => $val1)
			{
				if ($selesai_pt2)
				{
					foreach ($selesai_pt2 as $val2)
					{
						$raw_psb[$key1] = $val1;
						if ($val1->sto == $val2->sto)
						{
							$raw_psb[$key1]->Selesai_pt2 = $val2->hitung;
						}
					}
				}
				else
				{
					$raw_psb[$key1] = $val1;
				}
			}

			foreach ($raw_psb as $key1 => $val1)
			{
				$data->$datel[$key1] = $val1;
				foreach ($data_psb as $key => $val3)
				{
					if ($val1->sto == $val3->sto_B)
					{
						$data->$datel[$key1]->Sisa_Order += $val3->Sisa_Order;
						$data->$datel[$key1]->WO_tersedia += $val3->WO_tersedia;
					}
				}
			}

		}
		else
		{
			$status = self::status('all');

			$key = array_search($jenis, $status);

			if (preg_match('/(?i)all/', $datel))
			{
				$sql_dtl = '';
			}
			else
			{
				$sql_dtl = "AND sd.datel = '$datel'";
			}

			$date_nya = "AND (CASE
			WHEN tgl_selesai IS NOT NULL THEN DATE(tgl_selesai) BETWEEN '$tgl_a' AND '$tgl_b'
			WHEN tgl_pengerjaan IS NOT NULL THEN DATE(tgl_pengerjaan) BETWEEN '$tgl_a' AND '$tgl_b'
			WHEN tgl_buat IS NOT NULL THEN DATE(tgl_buat) BETWEEN '$tgl_a' AND '$tgl_b'
			ELSE 1 = 1
			END)";

			$raw_data = DB::SELECT("SELECT sd.datel, sd.sto,
			SUM(CASE WHEN ( (lt_status NOT IN ('Selesai', 'Kendala') AND regu_id IS NOT NULL) OR lt_status IS NULL) THEN 1 ELSE 0 END) as Sisa_Order,
			SUM(CASE WHEN lt_status IS NULL THEN 1 ELSE 0 END) as Need_Progres,
			SUM(CASE WHEN lt_status = 'Selesai' THEN 1 ELSE 0 END) as Selesai_fisik,
			SUM(CASE WHEN lt_status = 'Selesai' AND GOLIVE = 0 THEN 1 ELSE 0 END) as selesai_blum_golive,
			SUM(CASE WHEN lt_status = 'Selesai' AND GOLIVE = 0 AND upload_abd_4 = 0 THEN 1 ELSE 0 END) as belum_abd,
			SUM(CASE WHEN lt_status = 'Selesai' AND GOLIVE = 1 THEN 1 ELSE 0 END) as selesai_golive,
			SUM(CASE WHEN lt_status = 'Ogp' THEN 1 ELSE 0 END) as Ogp,
			SUM(CASE WHEN lt_status = 'Berangkat' THEN 1 ELSE 0 END) as Berangkat,
			SUM(CASE WHEN lt_status = 'Kendala' THEN 1 ELSE 0 END) as Kendala,
			SUM(CASE WHEN lt_status = 'Pending' THEN 1 ELSE 0 END) as Pending,
			SUM(CASE WHEN lt_status = 'Kendala' AND kendala_detail = 'Distribusi Full' THEN 1 ELSE 0 END) AS DF,
			SUM(CASE WHEN lt_status = 'Kendala' AND kendala_detail = 'Feeder Reti' THEN 1 ELSE 0 END) AS FT,
			SUM(CASE WHEN lt_status = 'Kendala' AND kendala_detail = 'Core Ccd' THEN 1 ELSE 0 END) AS CC,
			SUM(CASE WHEN lt_status = 'Kendala' AND kendala_detail = 'Tercover Odp Lain' THEN 1 ELSE 0 END) AS TOL,
			SUM(CASE WHEN lt_status = 'Kendala' AND kendala_detail = 'Kendala Jalur Tidak Ada Tiang Kosong' THEN 1 ELSE 0 END) AS KK,
			SUM(CASE WHEN lt_status = 'Kendala' AND kendala_detail = 'Kendala Cuaca' THEN 1 ELSE 0 END) AS KC,
			SUM(CASE WHEN lt_status = 'Kendala' AND kendala_detail = 'Cancel' THEN 1 ELSE 0 END) AS C,
			SUM(CASE WHEN lt_status = 'Kendala' AND kendala_detail = 'Feeder Full' THEN 1 ELSE 0 END) AS FF,
			SUM(CASE WHEN lt_status = 'Kendala' AND kendala_detail = 'Olt Full' THEN 1 ELSE 0 END) AS OF
			FROM maintenance_datel sd
			LEFT JOIN pt2_master ON sd.sto = pt2_master.sto
			LEFT JOIN regu r ON r.id_regu = pt2_master.regu_id
			WHERE delete_clm = 0 AND kategory_non_unsc = $key $sql_dtl
			GROUP BY sto");

			foreach ($datel_list as $key => $val)
			{
				foreach ($raw_data as $key2 => $val2)
				{
					$datel = $val->datel;
					if ($val2->sto == $val->sto)
					{
						$data->$datel[$key] = $val2;
					}
				}
			}

		}
		return $data;
	}

	public static function detail_list($jenis, $datel, $tipe, $sto, $tgl_a, $tgl_b, $jenis_wo)
	{
		$status_kendala = ['Distribusi Full' => 'DF', 'Feeder Reti' => 'FT', 'Core Ccd' => 'CC', 'Tercover Odp Lain' => 'TOL', 'Kendala Jalur Tidak Ada Tiang Kosong' => 'KK', 'Kendala Cuaca Hujan' => 'KC', 'Cancel' => 'C', 'Feeder Full' => 'FF', 'Olt Full' => 'OF', ];
		$spesifik_status = ['Need_Progres', 'undispatch', 'total_order', 'Ogp', 'selesai_golive', 'selesai_blum_golive', 'WO_tersedia', 'Sisa_Order', 'not_pt2', 'lanjut_pt1', 'Selesai_fisik', 'belum_abd'];

		$add_sql = '1 = 1 ';

		if (!preg_match('/(?i)all/', $datel))
		{
			$add_sql .= "AND sd.datel = '$datel' ";
		}

		if (!preg_match('/(?i)all/', $jenis_wo) )
		{
			$add_sql .= "AND jenis_wo = '$jenis_wo' ";
		}

		if (!preg_match('/(?i)all/', $sto))
		{
			$add_sql .= "AND sd.sto = '$sto' ";
		}

		if ($jenis == 'unsc_psb')
		{
			$add_sql .= 'AND kategory_non_unsc = 0 ';
		}
		elseif ($jenis == 'all_data')
		{
			$add_sql .= 'AND 1 = 1 ';
		}
		// dd($jenis, $tipe);
		if (preg_match('/(?i)(unsc_psb|all_data)/', $jenis) )
		{
			$add_sql .= 'AND 1 = 1 ';
		}
		else
		{
			$status = self::status('all');
			$key = array_search($jenis, $status);

			$add_sql .= "AND kategory_non_unsc = $key ";
		}

		$add_sql .= "AND (CASE
		WHEN tgl_selesai IS NOT NULL THEN DATE(tgl_selesai) BETWEEN '$tgl_a' AND '$tgl_b'
		WHEN tgl_pengerjaan IS NOT NULL THEN DATE(tgl_pengerjaan) BETWEEN '$tgl_a' AND '$tgl_b'
		WHEN tgl_buat IS NOT NULL THEN DATE(tgl_buat) BETWEEN '$tgl_a' AND '$tgl_b'
		ELSE 1 = 1
		END) ";

		if (preg_match('/(?i)all/', $tipe) )
		{
			$add_sql .= "AND sd.sto IN(' " . $sto . " ') ";

			if (preg_match('/(?i)all/', $sto) )
			{
				$cari = DB::SELECT("SELECT * FROM maintenance_datel WHERE datel = '$datel' ");
				$add_sql .=  "AND sd.sto IN (' " . implode(" ',' ", array_column($cari, 'sto') ) . " ') ";
			}

			if (preg_match('/(?i)all/', $datel))
			{
				$cari = DB::SELECT('SELECT * FROM maintenance_datel WHERE regional = 6');
				$add_sql .=  "AND sd.sto IN (' " . implode(" ',' ", array_column($cari, 'sto') ) . " ') ";
			}

			$data_pt2 = DB::SELECT("SELECT pts.*, r.nama_mitra, r.ACTIVE As active_regu, pl.status_laporan,
			pts.id as id_pt2,
			ACTIVE
			FROM maintenance_datel sd
			LEFT JOIN pt2_master pts ON sd.sto = pts.sto
			LEFT JOIN regu r ON r.id_regu = pts.regu_id
			LEFT JOIN psb_laporan pl ON pts.scid = pl.id_tbl_mj
			WHERE $add_sql AND pts.regu_id IS NOT NULL AND ACTIVE = 1 delete_clm = 0 ");

			return $data_pt2;
		}
		else
		{
			if (!in_array($tipe, $spesifik_status) )
			{
				$search_kndl = array_search($tipe, $status_kendala);

				if ($search_kndl == true)
				{
					$add_sql .= "AND lt_status = 'Kendala' AND kendala_detail = '$search_kndl' AND ";
				}
				else
				{
					$add_sql .= "AND lt_status = '$tipe' AND";
				}

				return DB::SELECT("SELECT pts.*, r.nama_mitra, r.ACTIVE As active_regu,
					pts.id as id_pt2
					FROM maintenance_datel sd
					LEFT JOIN pt2_master pts ON sd.sto = pts.sto
					LEFT JOIN regu r ON r.id_regu = pts.regu_id
					WHERE $add_sql delete_clm = 0
				");
			}
			else
			{
				if ($tipe == 'Need_Progres')
				{
					return DB::SELECT("SELECT pts.*, r.nama_mitra, r.ACTIVE As active_regu,
					pts.id as id_pt2
					FROM maintenance_datel sd
					LEFT JOIN pt2_master pts ON sd.sto = pts.sto
					LEFT JOIN regu r ON r.id_regu = pts.regu_id
					LEFT JOIN psb_laporan pl ON pl.id_tbl_mj = pts.scid
					WHERE $add_sql AND delete_clm = 0 AND lt_status IS NULL AND regu_id IS NOT NULL");
				}
				if ($tipe == 'undispatch')
				{
					return DB::SELECT("SELECT pts.*, r.nama_mitra, r.ACTIVE As active_regu,
					pts.id as id_pt2
					FROM maintenance_datel sd
					LEFT JOIN pt2_master pts ON sd.sto = pts.sto
					LEFT JOIN regu r ON r.id_regu = pts.regu_id
					LEFT JOIN psb_laporan pl ON pl.id_tbl_mj = pts.scid
					WHERE $add_sql AND delete_clm = 0 AND lt_status IS NULL AND pts.regu_id IS NULL");
				}
				if ($tipe == 'total_order')
				{
					return DB::SELECT("SELECT pts.*, r.nama_mitra, r.ACTIVE As active_regu,
					pts.id as id_pt2
					FROM maintenance_datel sd
					LEFT JOIN pt2_master pts ON sd.sto = pts.sto
					LEFT JOIN regu r ON r.id_regu = pts.regu_id
					LEFT JOIN psb_laporan pl ON pl.id_tbl_mj = pts.scid
					WHERE $add_sql AND delete_clm = 0");
				}
				elseif ($tipe == 'Ogp')
				{
					return DB::SELECT("SELECT pts.*, r.nama_mitra, r.ACTIVE As active_regu,
					pts.id as id_pt2
					FROM maintenance_datel sd
					LEFT JOIN pt2_master pts ON sd.sto = pts.sto
					LEFT JOIN regu r ON r.id_regu = pts.regu_id
					LEFT JOIN psb_laporan pl ON pl.id_tbl_mj = pts.scid
					WHERE $add_sql AND delete_clm = 0 AND pts.regu_id IS NOT NULL AND lt_status IN('On Progress', 'Ogp')");
				}
				elseif ($tipe == 'selesai_golive')
				{
					return DB::SELECT("SELECT pts.*, r.nama_mitra, r.ACTIVE As active_regu,
					pts.id as id_pt2
					FROM maintenance_datel sd
					LEFT JOIN pt2_master pts ON sd.sto = pts.sto
					LEFT JOIN regu r ON pts.regu_id = r.id_regu
					WHERE $add_sql AND delete_clm = 0 AND lt_status ='Selesai' AND GOLIVE = 1");
				}
				elseif (in_array($tipe, ['selesai_blum_golive', 'Selesai_fisik']) )
				{
					return DB::SELECT("SELECT pts.*, r.nama_mitra, r.ACTIVE As active_regu,
					pts.id as id_pt2
					FROM maintenance_datel sd
					LEFT JOIN pt2_master pts ON sd.sto = pts.sto
					LEFT JOIN regu r ON r.id_regu = pts.regu_id
					WHERE $add_sql AND delete_clm = 0 AND lt_status ='Selesai' AND GOLIVE = 0");
				}
				elseif (in_array($tipe, ['lanjut_pt1']) )
				{
					return DB::SELECT("SELECT pts.*, r.nama_mitra, r.ACTIVE As active_regu,
					pts.id as id_pt2
					FROM maintenance_datel sd
					LEFT JOIN pt2_master pts ON sd.sto = pts.sto
					LEFT JOIN regu r ON r.id_regu = pts.regu_id
					WHERE $add_sql AND delete_clm = 0 AND lanjut_PT1 = 1");
				}
				elseif (in_array($tipe, ['belum_abd']) )
				{
					return DB::SELECT("SELECT pts.*, r.nama_mitra, r.ACTIVE As active_regu,
					pts.id as id_pt2
					FROM maintenance_datel sd
					LEFT JOIN pt2_master pts ON sd.sto = pts.sto
					LEFT JOIN regu r ON r.id_regu = pts.regu_id
					WHERE $add_sql AND delete_clm = 0 AND upload_abd_4 = 0 AND lt_status ='Selesai' AND GOLIVE = 0");
				}
				elseif ($tipe == 'Sisa_Order')
				{
					if (preg_match('/(?i)all/', $sto))
					{
						$cari = DB::SELECT("SELECT * FROM maintenance_datel WHERE datel = '$datel'");
						$sto = "'" . implode("','", array_column($cari, 'sto')) . "'";
					}
					else
					{
						$sto = "'" . $sto . "'";
					}

					if (preg_match('/(?i)all/', $datel))
					{
						$cari = DB::SELECT('SELECT * FROM maintenance_datel WHERE regional = 6');
						$sto = "'" . implode("','", array_column($cari, 'sto')) . "'";
					}

					if (preg_match('/(?i)all_data/', $jenis) )
					{
						// $id_stts = implode(', ', array_keys(self::status() ) );
						$data_pt2 = DB::SELECT("SELECT pts.*, r.nama_mitra, r.ACTIVE As active_regu,
						pts.id as id_pt2,
						ACTIVE
						FROM pt2_master pts
						LEFT JOIN maintenance_datel sd ON sd.sto = pts.sto
						LEFT JOIN regu r ON pts.regu_id = r.id_regu
						WHERE $add_sql AND delete_clm = 0 AND (pts.regu_id IS NOT NULL AND (lt_status NOT IN ('Selesai', 'Kendala') ) OR lt_status IS NULL)");
					}
					else
					{
						$data_pt2 = [];
					}
					return $data_pt2;
				}
				elseif ($tipe == 'not_pt2')
				{
					return DB::SELECT("SELECT pts.*, r.nama_mitra, r.ACTIVE As active_regu,
					pl.status_laporan as sl,
					pts.id as id_pt2
					FROM maintenance_datel sd
					LEFT JOIN pt2_master pts ON sd.sto = pts.sto
					LEFT JOIN regu r ON r.id_regu = pts.regu_id
					LEFT JOIN psb_laporan pl ON pl.id_tbl_mj = pts.scid
					WHERE $add_sql AND pl.status_laporan != 75 AND delete_clm = 0 AND GOLIVE = 0 AND (pts.regu_id IS NOT NULL AND (lt_status NOT IN ('Kendala', 'Selesai') ) OR lt_status IS NULL)");
				}
				else
				{
					if ($tipe == 'WO_tersedia')
					{
						$stts = 'AND pts.regu_id IS NULL';
					}
					else
					{
						$stts = "AND (lt_status NOT IN ('Kendala', 'Selesai') OR lt_status IS NULL)";
					}

					if (preg_match('/(?i)all/', $sto) )
					{
						$sql_sto = 'sto IS NOT NULL';
					}
					else
					{
						$sql_sto = "sto = '$sto' ";
					}

					if (preg_match('/(?i)all/', $datel))
					{
						$sql_dtl = 'AND sto IS NOT NULL';
					}
					else
					{
						$cari = DB::SELECT("SELECT * FROM maintenance_datel WHERE datel = '$datel'");
						$sto = "'" . implode("','", array_column($cari, 'sto')) . "'";
						$sql_dtl = "AND sto IN($sto)";
					}

					return DB::SELECT("SELECT dt.id,
					pts.id as id_pt2,
					pl.odp_plan as odp_nama,
					pl.kordinat_odp as odp_koor,
					(CASE WHEN my.customer IS NOT NULL THEN my.customer ELSE my2.customer END) as project_name,
					pl.modified_at as tgl_pengerjaan,
					pts.regu_name as regu_name,
					pts.regu_id as regu_id,
					pts.lt_status as lt_status,
					pts.kategory_non_unsc as kategory_non_unsc,
					pts.lt_catatan as lt_catatan,
					pts.kendala_detail,
					r.nama_mitra, r.ACTIVE As active_regu,
					(CASE WHEN dt.Ndem IS NOT NULL THEN dt.Ndem
					WHEN e.myir IS NOT NULL THEN e.myir
					ELSE e.orderId END) AS nomor_sc,
					(CASE WHEN pl.odp_plan IS NOT NULL AND LENGTH(pl.odp_plan) > 6 THEN SUBSTR(pl.odp_plan, 5, 3)
					WHEN my.sto IS NOT NULL THEN my.sto
					WHEN my2.sto IS NOT NULL THEN my2.sto ELSE f.area END) AS sto_B
					FROM
					dispatch_teknisi dt
					LEFT JOIN psb_laporan pl ON dt.id = pl.id_tbl_mj
					LEFT JOIN Data_Pelanggan_Starclick e ON dt.Ndem = e.orderId
					LEFT JOIN mdf f ON e.sto = f.mdf
					LEFT JOIN psb_myir_wo my ON my.myir = dt.Ndem
					LEFT JOIN psb_myir_wo my2 ON my2.sc = dt.Ndem
					LEFT JOIN pt2_master pts ON pts.scid = pl.id_tbl_mj
					LEFT JOIN regu r ON r.id_regu = pts.regu_id
					WHERE
					pl.status_laporan = 75 AND (dt.dispatch_by = '5' OR dt.dispatch_by IS NULL) AND (ACTIVE = 1 AND pts.id IS NOT NULL OR ACTIVE = 0 AND pts.id IS NULL) $stts AND (DATE(pl.modified_At) BETWEEN '$tgl_a' AND '$tgl_b') HAVING $sql_sto $sql_dtl");
				}
			}
		}
	}

	public static function re_push_pt2($id)
	{
		DB::table('psb_laporan')->where('id_tbl_mj', $id)->update(['modified_at' => date('Y-m-d H:i:s') , 'status_laporan' => 75]);
	}

	public static function cek_user($id)
	{
		return DB::table('user')->leftjoin('1_2_employee', '1_2_employee.nik', '=', 'user.id_user')
		->where('id_user', $id)->first();
	}

	public static function reactive_regu($id)
	{
		DB::table('regu')
			->where('id_regu', $id)
			->update(['ACTIVE' => 1]);
	}

	public static function get_detail_hist($id, $stts)
	{
		if($stts == 'belum_dikerjakan')
		{
			$stts = NULL;
		}
		else
		{
			$stts = $stts;
		}
		return DB::table('pt2_master')->select('pt2_master.*', 'pt2_master.id as id_pt2')
			->where([
				['regu_id', $id],
				['lt_status', $stts]
			])
			->get();
	}

	public static function remind_unfinish()
	{
		$data = DB::SELECT("SELECT *, HOUR(TIMEDIFF(NOW(), modified_at)) as jml_jam
		FROM pt2_master WHERE delete_clm = 0 ORDER BY id DESC");

		$msg_prob = $msg_ogp = $msg_arrival = '';
		// $chat_id = '-1001114802851';
		$chat_id = 519446576;

		foreach($data as $k => $v)
		{
			if($v->lt_status == 'Kendala' && $v->jml_jam >= 1)
			{
				$final_data_k['kendala'][$v->sto][] = $v;
			}

			if($v->lt_status == 'Ogp' && $v->jml_jam >= 3)
			{
				$final_data_k['Ogp'][$v->sto][] = $v;
			}

			if($v->lt_status == 'Berangkat' && $v->jml_jam >= 2)
			{
				$final_data_k['Berangkat'][$v->sto][] = $v;
			}
		}

		if(!empty($final_data_k['kendala']) )
		{
			$msg_prob .= "Daftar Pekerjaan Kendala Lebih Dari 1 Jam \n";
			Telegram::sendMessage([
				'chat_id' 	 => $chat_id,
				'parse_mode' => 'html',
				'text'       => "$msg_prob"
			]);

			foreach($final_data_k['kendala'] as $k => $v)
			{
				$msg_prob = '';
				$msg_prob .= "STO $k \n";
				$batch = array_chunk($v, 20);
				// dd($batch);
				foreach($batch as $vv)
				{
					$add_msg = '';
					foreach($vv as $vvv)
					{
						$add_msg .= "🚨<b> $vvv->regu_name</b> : \nJudul Pekerjaan: $vvv->project_name\n <b>$vvv->odp_nama ($vvv->odp_koor)</b>\nDurasi: $vvv->jml_jam Jam\n";
					}
				}
				$msg_prob .= $add_msg;
				Telegram::sendMessage([
					'chat_id' 	 => $chat_id,
					'parse_mode' => 'html',
					'text'       => "$msg_prob"
				]);
			}
		}

		sleep(10);

		if(!empty($final_data_k['Ogp']) )
		{
			$msg_ogp .= "Daftar Pekerjaan OGP Lebih Dari 3 Jam \n";
			Telegram::sendMessage([
				'chat_id' 	 => $chat_id,
				'parse_mode' => 'html',
				'text'       => "$msg_ogp"
			]);

			foreach($final_data_k['Ogp'] as $k => $v)
			{
				$msg_ogp = '';
				$msg_ogp .= "STO $k \n";
				$batch = array_chunk($v, 20);
				// dd($batch);
				foreach($batch as $vv)
				{
					$add_msg = '';
					foreach($vv as $vvv)
					{
						$add_msg .= "🚨<b> $vvv->regu_name</b> : \nJudul Pekerjaan: $vvv->project_name\n <b>$vvv->odp_nama ($vvv->odp_koor)</b>\nDurasi: $vvv->jml_jam Jam\n";
					}
				}
				$msg_ogp .= $add_msg;
				Telegram::sendMessage([
					'chat_id' 	 => $chat_id,
					'parse_mode' => 'html',
					'text'       => "$msg_ogp"
				]);
			}
		}

		sleep(10);

		if(!empty($final_data_k['Berangkat']) )
		{
			$msg_arrival .= "Daftar Pekerjaan Berangkat Lebih Dari 2 Jam \n";
			Telegram::sendMessage([
				'chat_id' 	 => $chat_id,
				'parse_mode' => 'html',
				'text'       => "$msg_arrival"
			]);

			foreach($final_data_k['Berangkat'] as $k => $v)
			{
				$msg_arrival = '';
				$msg_arrival .= "STO $k \n";
				$batch = array_chunk($v, 20);
				// dd($batch);
				foreach($batch as $vv)
				{
					$add_msg = '';
					foreach($vv as $vvv)
					{
						$add_msg .= "🚨<b> $vvv->regu_name</b> : \nJudul Pekerjaan: $vvv->project_name\n <b>$vvv->odp_nama ($vvv->odp_koor)</b>\nDurasi: $vvv->jml_jam Jam\n";
					}
				}
				$msg_arrival .= $add_msg;
				Telegram::sendMessage([
					'chat_id' 	 => $chat_id,
					'parse_mode' => 'html',
					'text'       => "$msg_arrival"
				]);
			}
		}

		if(empty($msg_prob) && empty($msg_ogp) && empty($msg_arrival) )
		{
			Telegram::sendMessage([
				'chat_id' 	 => $chat_id,
				'parse_mode' => 'html',
				'text'       => 'Semua Orderan Sudah Tuntas! 👍'
			]);
		}

		// 		519446576
		// 1001114802851
	}

	public static function get_list_Absen()
	{
		$data = DB::table('absen As a')
		->leftjoin('regu As b', function($join){
			$join->on('b.nik1', '=', 'a.nik')
			->OrOn('b.nik2', '=', 'a.nik');
		})
		->leftjoin('1_2_employee As c', 'c.nik', '=', 'a.nik')
		->select('a.*', 'c.nama', 'b.uraian', 'b.label_sektor')
		->where([
			['b.job', 'PT2'],
      ['a.divisi', 'PT2'],
      [DB::RAW("DATE_FORMAT(a.date_created, '%Y-%m-%d')"), date('Y-m-d')],
			['b.ACTIVE', 1],
    ])
		->WhereNull('a.approve_by');

		if(!in_array(session('auth')->pt2_level, [2, 5]) )
		{
			$data->Where('b.Tl', session('auth')->id_user);
		}

		if(in_array(session('auth')->pt2_level, [3]) )
		{
			$data->Where('c.mitra_amija', session('auth')->mitra);
		}

		$data = $data->get();

		if(session('auth')->pt2_level == 9)
		{
			$find_sektor = DB::table('group_telegram')
			->Where('TL_NIK', session('auth')->id_user)
			->OrWhere('Nik_Atl', session('auth')->id_user)
			->OrWhere('Nik_Atl2', session('auth')->id_user)
			->get();

			$get_sektor = [];

			foreach($find_sektor as $v)
			{
				$get_sektor[$v->sektorx] = $v->sektorx;
			}

			foreach ($data as $k => &$v)
			{
				if(!in_array($v->label_sektor, $get_sektor) )
				{
					unset($data[$k]);
				}
			}
			unset($v);
		}

		return $data;

	}

	public static function get_rekap_rekon($y)
	{
		$time = date('Y-m', strtotime($y) );
		return DB::select("SELECT ptm.project_name, (pm.id_item) as material_id, pk.satuan, pk.material_ta As material, pk.jasa_ta As jasa, pm.qty
		FROM pt2_material pm
		LEFT JOIN pt2_master ptm ON ptm.id = pm.pt2_id
		LEFT JOIN pt2_khs pk ON pm.id_item = pk.id_item
		WHERE DATE_FORMAT(ptm.tgl_selesai, '%Y-%m') = $time");
	}

	public static function save_nog($req, $id)
	{
		$regu = $req->regu;
		$regutable = DB::table('regu')->where('id_regu', $regu)->first();

		DB::table('nog_master')->where('id', $id)->update([
			'dispatch_regu_id' => $regu,
			'regu_nama'        => $regutable->uraian
		]);
	}

	public static function get_sto()
	{
		return DB::table('mitos_odc_master')->select('sto')->GroupBy('sto')->OrderBy('sto', 'ASC')->get();
	}

	public static function get_odc()
	{
		return DB::table('mitos_odc_master')->select('odc')->GroupBy('odc')->OrderBy('odc', 'ASC')->get();
	}

	public static function update_flagging($req)
	{
		DB::transaction(function () use ($req){
			DB::Table('pt2_hero')->where('id', $req->id_hero)->update([
				'flagging' => $req->isi
			]);

			if(in_array($req->isi, ['non_unsc', 'pt2_plus']) )
			{
				$find_k = DB::table('pt2_status')->where('id_status', $req->isi)->first();

				DB::Table('pt2_master')->where('id_hero', $req->id_hero)->update([
					'kategory_non_unsc' => $find_k->id,
					'lt_status' => null
				]);
			}
			else
			{
				DB::Table('pt2_master')->where('id_hero', $req->id_hero)->update([
					'lt_status' => 'Kendala'
				]);
			}
		});
	}

	public static function check_user_jointer($id)
	{
		return DB::table('user As u')
		->leftjoin('pt2_party As pp', 'pp.unit_nik', '=', 'u.id_user')
		->Leftjoin('regu As r', 'u.id_user', '=', 'r.nik1')
		->select('pp.*', 'u.level', 'u.is_jointer', 'u.pt2_level')
		->where('id_user', '=', $id)
		->first();
	}

	public static function check_limit_jointer($date, $nik)
	{
		return DB::table('pt2_master')->where([
			[DB::RAW("DATE_FORMAT(tgl_buat,'%Y-%m')"), $date],
			['nik1', $nik]
		])
		->get();
	}

	public static function get_regu_joint()
	{
		return DB::Table('regu')->where([
			['job', 'PT2'],
			['ACTIVE', 1]
		])
		->get();
	}

	public static function get_id_regu($id)
	{
		return DB::table('regu')->where('id_regu', $id)->first();
	}

	public static function saldo_order($jenis_wo)
	{
		$data = DB::table('pt2_master')
		->Where(function($join){
			$join->WhereNotIn('lt_status', ['Selesai', 'Kendala'])
			->OrWhereNull('lt_status');
		})
		->Where('delete_clm', 0)
		->WhereNotNull('regu_id');

		if($jenis_wo)
		{
			$data->Where('jenis_wo', $jenis_wo);
		}

		$data = $data->get();

		$fd = [];

		foreach($data as $v)
		{
			if(!isset($fd[$v->sto]) )
			{
				$fd[$v->sto] = 0;
			}

			$fd[$v->sto] += 1;
		}

		asort($fd);

		return $fd;
	}

	public static function save_abd($req)
	{
		$that = new AdminModel();
		$that->handleFileUpload_abd($req);

		DB::table('pt2_master')->Where('id', $req->id_pt2)->update([
			'upload_abd_4' => 1,
			'catatan_abd' => $req->catatan_abd
		]);

		// $files = array_values($rfc_file);
		// $file_kml = public_path().'/upload/pt_2_3/' . $id . '/'.$files[0];

		// if (file_exists($file_kml) )
		// {
		// 	Telegram::sendDocument([
		// 		// 'chat_id' => '519446576',
		// 		'chat_id' => self::telegramId($id),
		// 		'caption' => 'File KML',
		// 		'document' => $file_kml
		// 	]);
		// }
	}

	private function handleFileUpload_abd($req)
	{
		$id = $req->id_pt2;
		if ($req->hasFile('abd_file') )
		{
			$path = public_path() . '/upload/pt_2_3/' . $id;
			if (!file_exists($path) )
			{
				if (!mkdir($path, 0770, true))
				{
					return 'gagal menyiapkan File ABD';
				}
			}
			$file = $req->file('abd_file');
			try
			{
				$filename = 'File ABD.'.strtolower( $file->getClientOriginalExtension() );
				$file->move("$path", "$filename");
			}
			catch(\Symfony\Component\HttpFoundation\File\Exception\FileException $e)
			{
				return 'gagal menyimpan file ABD';
			}
		}
	}
}