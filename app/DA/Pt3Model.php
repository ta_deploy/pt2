<?php
namespace App\DA;

use DB;

class Pt3Model
{
	public static function getDashboard()
	{
		return DB::select('SELECT *,
	  (select count(*) from mpromise_lop LEFT JOIN mpromise_project ON mpromise_lop.mpromise_project_id = mpromise_project.id where loker = "Pengiriman_Material" and sto = mps.STO) as Pengiriman_Material,
  		(select count(*) from mpromise_lop LEFT JOIN mpromise_project ON mpromise_lop.mpromise_project_id = mpromise_project.id where loker = "Pragelaran" and sto = mps.STO) as Pragelaran,
  		(select count(*) from mpromise_lop LEFT JOIN mpromise_project ON mpromise_lop.mpromise_project_id = mpromise_project.id where loker = "Tanam_Tiang" and sto = mps.STO) as Tanam_Tiang,
  		(select count(*) from mpromise_lop LEFT JOIN mpromise_project ON mpromise_lop.mpromise_project_id = mpromise_project.id where loker = "Gelar_Kabel" and sto = mps.STO) as Gelar_Kabel,
  		(select count(*) from mpromise_lop LEFT JOIN mpromise_project ON mpromise_lop.mpromise_project_id = mpromise_project.id where loker = "Pasang_Terminal" and sto = mps.STO) as Pasang_Terminal,
  		(select count(*) from mpromise_lop LEFT JOIN mpromise_project ON mpromise_lop.mpromise_project_id = mpromise_project.id where loker = "Terminasi" and sto = mps.STO) as Terminasi,
  		(select count(*) from mpromise_lop LEFT JOIN mpromise_project ON mpromise_lop.mpromise_project_id = mpromise_project.id where loker = "TestComm" and sto = mps.STO) as TestComm,
  		(select count(*) from mpromise_lop LEFT JOIN mpromise_project ON mpromise_lop.mpromise_project_id = mpromise_project.id where sto = mps.STO) as Total
  	 from promise_sto mps GROUP BY STO having Total > 0');
	}
	public static function getlist($sto, $sts)
	{
		$query = DB::table('mpromise_lop');

		if ($sto)
		{
			$query->where('sto', $sto);
		}

		if ($sts)
		{
			$query->where('loker', $sts);
		}
		
		return $query->get();
	}
}