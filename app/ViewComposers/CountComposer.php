<?php

namespace App\ViewComposers;

use Illuminate\Http\Request;
use Illuminate\View\View;
use App\DA\LoginModel;

class CountComposer
{
    protected $req;

    public function __construct(Request $req)
    {
        $this->req = $req;
    }

    public function compose(View $view)
    {
        $auth = $this->req->session()->get('auth');
        $countMenu = LoginModel::countMenu();
        // dd($countMenu);
        $view->with('countMenu', $countMenu);
    }
}