<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="testinggggg">
    <meta name="author" content="asdfasdfasdf">
    <link rel="icon" type="image/png" sizes="16x16" href="/images/logo.png">
    <title>@yield('title')</title>
    <link href="/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    @yield('headerS')
    <link href="/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
    <link href="/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
    <link href="/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
    <link href="/bower_components/morrisjs/morris.css" rel="stylesheet">
    <link href="/css/animate.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">
    <link href="/css/colors/blue.css" id="theme" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    @yield('style')
    <style type="text/css">
        .modal_detail_layout{
            overflow: auto;
            overflow-y: auto;
        }

        .swal2-container {
            z-index: 1999;
        }

        /* .select2-results { background-color: #353c48; } */

    </style>
</head>
<body class="">
    <div class="preloader">
        <div class="cssload-speeding-wheel"></div>
    </div>
    <div id="parent">
        <div id="child"></div>
    </div>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-static-top m-b-0">
            <div class="navbar-header"> <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse"><i class="ti-menu"></i></a>
                <div class="top-left-part">
                    <a class="logo" href="/"><b>
                        <img src="{{ asset('/images/logo.png/')}}" alt="home" class="dark-logo" style="width: 50px; height: 50px;" /></b>
                        <span class="hidden-xs">
                            <img src="{{ asset('/images/logo_ta.png/')}}" alt="home" class="dark-logo" style="margin-left:6px; width: 150px; height: 60px; position: absolute;"/>
                        </span>
                    </a>
                </div>
                <ul class="nav navbar-top-links navbar-left hidden-xs">
                    <li><a href="javascript:void(0)" class="open-close hidden-xs waves-effect waves-light"><i class="icon-arrow-left-circle ti-menu"></i></a></li>
                </ul>
                @if(in_array(session('auth')->pt2_level, [2, 5]))
                    <ul class="nav navbar-top-links navbar-right pull-right">
                        <li>
                            <form role="search" class="app-search hidden-xs" action="/search/data">
                                <input class="form-control odp_layout_search" name="search" placeholder="Cari UNSC atau ODP Dari Jointer"><a href="#"><i class="fa fa-search search_layout"></i></a>
                            </form>
                        </li>
                    </ul>
                @endif
            </div>
        </nav>
        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse slimscrollsidebar">
                {{-- menunya --}}
                <ul class="nav" id="side-menu">
                    {{-- <li class="sidebar-search hidden-sm hidden-md hidden-lg">
                        <div class="input-group custom-search-form">
                            <input type="text" class="form-control" placeholder="Search..."> <span class="input-group-btn">
                                <button type="button" class="btn btn-default" type="button"> <i class="fa fa-search"></i> </button>
                            </span>
                        </div>
                    </li> --}}
                    <li class="nav-small-cap m-t-10">--- Main Menu</li>
                    {{-- @if(in_array(session('auth')->pt2_level, [2, 5]) )
                        <li>
                            <a href="{{ URL::to('/admin/add/non_un/input') }}"><i data-icon="D" class="linea-icon linea-ecommerce fa-fw"></i><span class="hide-menu">&nbsp;Tambah Order</span></a>
                        </li>
                    @endif --}}
                    @if(in_array(session('auth')->pt2_level, [0, 1, 2, 3, 5, 6, 8, 9]) )
                        <li>
                            <a href="/home/{{ date('Y') }}"><i data-icon="&#xe025;" class="linea-icon linea-basic fa-fw"></i><span class="hide-menu">{{ in_array(session('auth')->pt2_level, [2, 5]) ? 'Dashboard' : ( in_array(session('auth')->pt2_level, [3, 6, 8, 9]) ? 'Matrix Tim Saya' : 'List Order')}}</span></a>
                        </li>
                    @endif
                    @if(in_array(session('auth')->pt2_level, [2, 5, 0]) || session('auth')->id_user == 18940469)
                        <li><a href="/outside/jointer/pt2" class="waves-effect"><i data-icon="u " class="linea-icon linea-basic fa-fw"></i><span class="hide-menu">WO Jointer</span></a></li>
                        <li><a href="/jointer/tim_party" class="waves-effect"><i data-icon="&#xe006;" class="linea-icon linea-basic fa-fw"></i><span class="hide-menu">Party Jointer</span></a></li>
                        <li><a href="/jointer/list_rfc_teknisi" class="waves-effect"><i data-icon="&#xe005;" class="linea-icon linea-basic fa-fw"></i><span class="hide-menu">List RFC</span></span></a></li>
                    @endif
                    @if(in_array(session('auth')->pt2_level, [2 , 5, 7]) )
                        <li>
                            <a href="#" class="waves-effect"><i data-icon="&#xe013;" class="linea-icon linea-basic fa-fw"></i><span class="hide-menu">Hero<span class="fa arrow"></span></span></a>
                            <ul class="nav nav-second-level">
                                <li><a href="/hero/table_HERO"><i data-icon="&#xe027;" class="linea-icon linea-basic fa-fw"></i>List WO Hero</a></li>
                                <li><a href="/hero/dashboard_HERO/{{ date('Y-m') }}"><i data-icon="S" class="linea-icon linea-ecommerce fa-fw"></i>Dashboard Hero</a></li>
                                <li><a href="/hero/progress/{{ date('Y-m') }}"><i data-icon="5" class="linea-icon linea-basic fa-fw"></i>Dashboard Progress</a></li>
                            </ul>
                        </li>
                        @endif
                    @if(in_array(session('auth')->pt2_level, [2, 5, 8]) )
                        <li>
                            <a href="#" class="waves-effect"><i data-icon="Z" class="linea-icon linea-basic fa-fw"></i><span class="hide-menu">User dan Regu<span class="fa arrow"></span></span></a>
                            <ul class="nav nav-second-level">
                                 @if(in_array(session('auth')->pt2_level, [2, 5]) )
                                    <li><a href="/regu/list"><i data-icon="&#xe019;" class="linea-icon linea-basic fa-fw"></i>List Jointer</a></li>
                                @endif
                                <li><a href="/jointer/list"><i data-icon="8" class="linea-icon linea-basic fa-fw"></i>User Jointer</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#" class="waves-effect"><i data-icon="&#xe008;" class="linea-icon linea-basic fa-fw"></i><span class="hide-menu">Report<span style="display: none;" class="label label-rouded label-info pull-right"><span class="jumlah_bon"></span></span><span class="fa arrow"></span></span></a>
                            <ul class="nav nav-second-level">
                                @if(in_array(session('auth')->pt2_level, [2, 5, 8]) )
                                    {{-- <li><a href="/rekapPO/list/{{ date('Y-m') }}"><i data-icon="&#xe018;" class="linea-icon linea-basic fa-fw"></i>Rekap PO</a></li> --}}
                                    <li><a href="/report/age"><i data-icon="&#xe011;" class="linea-icon linea-weather fa-fw"></i>Umur Jointer</a></li>
                                    {{-- <li><a href="/jointer_report/matrix/{{ date('Y-m') }}"><i data-icon="R" class="linea-icon linea-ecommerce fa-fw"></i>Matrix By Pekerjaan</a></li> --}}
                                    <li><a href="/pt3/dashboard"><i data-icon="r" class="linea-icon linea-basic fa-fw"></i>Rekap PT3</a></li>
                                    <li><a href="/matrix/{{ date('Y-m-d') }}"><i data-icon="Q" class="linea-icon linea-ecommerce fa-fw"></i>Matrix</a> </li>
                                    <li><a href="/ranked/{{ date('Y-m') }}"><i data-icon="R" class="linea-icon linea-ecommerce fa-fw"></i>Ranked (Monthly)</a> </li>
                                    <li><a href="/admin/progress/team/{{ date('Y') }}/{{ date('m') }}"><i data-icon="&#xe015;" class="linea-icon linea-aerrow fa-fw"></i>Progress My Team</a></li>
                                    {{-- <li><a href="/reportMaterial/{{ date('Y-m') }}"><i data-icon="&#xe005;" class="linea-icon linea-basic fa-fw"></i>Pemakaian Material(Monthly)</a> </li> --}}
                                    <li><a href="/rekap/Y={{ date('Y') }}"><i data-icon="&#xe012;" class="linea-icon linea-basic fa-fw"></i>Rekap PT2 Yearly</a> </li>
                                    <li><a href="/admin/monitoring"><i data-icon="H" class="linea-icon linea-basic fa-fw"></i>Monitoring Revenue</a></li>
                                    <li><a href="/saldo_finish"><i data-icon="&#xe001;" class="linea-icon linea-basic fa-fw"></i>List No Go-Live<span style="display: {{ $countMenu->count_golive_wl == 0 ? 'none' : 'block' }};" class="label label-rouded label-info pull-right">{{ $countMenu->count_golive_wl ?? 0 }}</span></a></li>
                                    {{-- <li><a href="/rekap_pt2_kpro"><i data-icon="&#xe012;" class="linea-icon linea-basic fa-fw"></i>Rekap PT2 Simple K-Pro</a> </li> --}}
                                    <li><a href="/geo/map"><i data-icon="R" class="linea-icon linea-basic fa-fw"></i>Titik Lokasi ODP</a> </li>
                                    {{-- <li><a href="/report/age"><i data-icon="&#xe011;" class="linea-icon linea-weather fa-fw"></i>Umur Non Jointer</a></li> --}}
                                    {{-- <li><a href="/rfc/{{ date('Y-m-d',strtotime('first day of this month')) }}/{{ date('Y-m-d') }}"><i data-icon="x" class="linea-icon linea-ecommerce fa-fw"></i>Rekap Material RFC</a> </li> --}}
                                    {{-- <li><a href="/report/naik_core/{{ date('Y-m') }}"><i data-icon="|" class="linea-icon linea-software fa-fw"></i>Rekap Jumlah Core</a> </li> --}}
                                    <li><a href="/report/naik_bulanan/{{ date('Y-m') }}"><i data-icon="R" class="linea-icon linea-ecommerce fa-fw"></i>Kalender UP</a> </li>
                                    <li><a href="/download/laporan"><i data-icon="n" class="linea-icon linea-elaborate fa-fw"></i>Download Data</a> </li>
                                @endif
                            </ul>
                        </li>
                        {{-- <li>
                            <a href="/admin/order/bank" class="waves-effect"><i data-icon="L" class="linea-icon linea-ecommerce fa-fw"></i><span class="hide-menu"><span class="hide-menu text-warning">Bank Order</span><div style="display: {{ $countMenu->bank_pt2 == 0 ? 'none' : 'block' }};" class="label label-rouded label-danger bank_angka pull-right">{{ $countMenu->bank_pt2 ?? 0 }}</div></span></a>
                        </li> --}}
                    @endif
                    @if(in_array(session('auth')->id_user, [18900164, 18940469]) || in_array(session('auth')->pt2_level, [4]) )
                        {{-- <li>
                            <a href="#" class="waves-effect"><i data-icon="O" class="linea-icon linea-basic fa-fw"></i><span class="hide-menu"><span class="hide-menu">Booking ODP</span>
                            <span class="fa arrow"></span></span></a>
                            <ul class="nav nav-second-level">
                            <li><a href="/show/booking_odp"><i data-icon="&#xe026;" class="linea-icon linea-basic fa-fw"></i>List</a></li>
                            <li><a href="/searchD/booking_odp"><i data-icon="L" class="linea-icon linea-elaborate fa-fw"></i>Deep Search</a></li>
                            </ul>
                        </li> --}}
                    @endif
                    @if(in_array(session('auth')->pt2_level, [3, 5, 6]) )
                        <li><a href="/monitor/my_team" class="waves-effect"><i data-icon="-" class="linea-icon linea-basic fa-fw"></i><span class="hide-menu">Regu Saya</span></a></li>
                        <li><a href="/monitor/ranked/{{ date('Y-m') }}"><i data-icon="R" class="linea-icon linea-ecommerce fa-fw"></i><span class="hide-menu">Ranked {{ session('auth')->mitra }} (Monthly)</span></a></li>
                        <li><a href="/monitor/geo" class="waves-effect"><i data-icon="R" class="linea-icon linea-basic fa-fw"></i><span class="hide-menu">Titik Lokasi WO</span></a></li>
                    @endif
                    @if(!in_array(session('auth')->pt2_level, [0, 6, 7]) )
                        <li><a href="/list_absen" class="waves-effect"><i data-icon="&#xe027;" class="linea-icon linea-basic fa-fw"></i><span class="hide-menu">Absensi</span></a></li>
                    @endif
                    @if(in_array(session('auth')->pt2_level, [2, 5]) )
                        <li>
                            <a href="/qc_pt2"><i data-icon="/" class="linea-icon linea-basic fa-fw"></i><span class="hide-menu">QC PT-2</span></a>
                        </li>
                    @endif
                    {{-- <li><a href="/saran" class="waves-effect"><i data-icon="&#xe027;" class="linea-icon linea-basic fa-fw"></i><span class="hide-menu"><span class="hide-menu text-primary">Saran & Fitur</span></span></a></li> --}}
                    @if(in_array(session('auth')->pt2_level, [2, 5]) )
                        <li><a href="/outside/order_outs/pt2" class="waves-effect"><i data-icon="-" class="linea-icon linea-elaborate fa-fw"></i><span class="hide-menu">Pickup WO<span style="display: {{ $countMenu->order_outside_pt2 == 0 ? 'none' : 'block' }};" class="label label-rouded label-info pull-right">{{ $countMenu->order_outside_pt2 ?? 0 }}</span></span></a></li>
                        <li><a href="/outside/pt2_kpro" class="waves-effect"><i data-icon="y" class="linea-icon linea-elaborate fa-fw"></i><span class="hide-menu">WO Simple KPRO</span></a></li>
                    @endif
                    <li>
                        <a href="https://perwira.tomman.app/input_potensi_b/{{ session('auth')->id_karyawan }}"><i data-icon="W" class="linea-icon linea-basic fa-fw"></i><span class="hide-menu">Lapor Potensi Bahaya</span></a>
                    </li>
                    <li class="user-pro">
                        <a href="#" class="waves-effect"><img src="/images/user.png" alt="user-img" class="img-circle"> <span class="hide-menu"> {{ session('auth')->id_user }}<span class="fa arrow"></span></span>
                        </a>
                        <ul class="nav nav-second-level">
                            <li><a href="/logout"><i class="fa fa-power-off"></i> Logout</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <div id="page-wrapper">
            @yield('content')
            <footer class="footer text-center">{{ date('Y') }} &copy;TIM TOMMAN</footer>
        </div>
    </div>
    <script src="/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="/bootstrap/dist/js/tether.min.js"></script>
    <script src="/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
    <script src="/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
    <script src="/js/jquery.slimscroll.js"></script>
    <script src="/js/waves.js"></script>
    <script src="/bower_components/waypoints/lib/jquery.waypoints.js"></script>
    <script src="/bower_components/counterup/jquery.counterup.min.js"></script>
    <script src="/bower_components/raphael/raphael-min.js"></script>
    <script src="/js/custom.min.js"></script>
    <script src="/bower_components/toast-master/js/jquery.toast.js"></script>
    <script src="/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    @yield('footerS')
    <script>
        $(function(){
            $('.search_layout').click(function(){
                var e = jQuery.Event("keypress");
                e.which = 13;
                $(".odp_layout_search").trigger(e);
            });
        });
    </script>
</body>
</html>
