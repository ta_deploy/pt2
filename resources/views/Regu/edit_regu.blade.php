@extends('layout')
@section("title", empty($edit) ? "Tambah Regu" : "Edit Regu")
@section('headerS')
<link href="/bower_components/bootstrap-switch/bootstrap-switch.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css" integrity="sha512-nMNlpuaDPrqlEls3IX/Q56H36qvBASwb3ipuo3MxeWbsQB1881ox0cRv7UPTgBlriqoynt35KjEwgGUeUXIPnw==" crossorigin="anonymous" />
@endsection
@section('content')
<div class="form-group">
	<a type="button" class="btn btn-default" href="{{URL::to('/regu/list')}}"><i data-icon="&#xe00f;" class="linea-icon linea-aerrow fa-fw" style="font-size: 20px; vertical-align:middle;"></i>&nbsp;Kembali</a>
</div>
<div class="container-fluid" style="padding-top: 25px;">
	<form method="post" id="regu_form">
		<div class="panel panel-info">
			<div class="panel-heading">{{ empty($edit) ? 'Tambah Regu' : 'Edit Regu' }}</div>
			<div class="panel-body">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<div class="form-group">
					<label>Nama Regu</label>
					<input type="text" class="form-control" id="uraian" name="uraian" required placeholder="Masukkan Nama Regu" value="{{ old('uraian', $edit->uraian ?? '') }}">
				</div>
				<div class="m-b-30 form-group">
					<div class="bt-switch">
						<input type="checkbox" name="check_stat" class="toogletogle" data-on-color="primary" data-off-color="info" data-on-text="STTF" data-off-text="PT2">
						<input type="checkbox" name="saber" class="toogletogle" data-on-color="success" data-off-color="danger" data-on-text="SABER" data-off-text="BUKAN">
					</div>
				</div>
				<div class="form-group">
					<label>STO</label>
					<select class="form-control" id="sto" name="sto" required>
						@if(!empty($data))
							<option value="{{$edit->sto}}">{{$edit->sto}}</option>
						@endif
						@foreach($sto as $esto)
							<option value="{{$esto->sto}}">{{$esto->sto}}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group">
					<label>NIK1</label>
					<input type="text" class="form-control" id="nik1" name="nik1" required placeholder="Masukkan NIK User 1" value="{{ old('nik1', $edit->nik1 ?? '') }}">
				</div>
				<div class="form-group">
					<label>NIK2</label>
					<input type="text" class="form-control" id="nik2" name="nik2" required placeholder="Masukkan NIK User 2" value="{{ old('nik2', $edit->nik2 ?? '') }}">
				</div>
				<div class="form-group">
					<label>Nik Team Leader</label>
					<input type="text" class="form-control" id="nik_tl" name="nik_tl" required placeholder="Masukkan NIK TL" value="{{ old('nik_tl', $edit->TL ?? '') }}">
				</div>
				<div class="form-group">
					<label>Mitra</label>
					<select id="mitra" name="mitra" class="form-control" required>
						@if(!empty($data))
							<option value="{{$edit->mitra}}">{{$edit->mitra}}</option>
						@endif
						@foreach($mitra as $m)
							<option value="{{$m->id}}">{{$m->text}}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group">
					<label>Status</label>
					<select id="ACTIVE" name="ACTIVE" class="form-control" required>
						<option value="1" {{ isset($edit->ACTIVE) == 1 ? "selected" : '' }}>Active</option>
						<option value="0" {{ isset($edit->ACTIVE) == 0 ? "selected" : '' }}>Non-Active</option>
					</select>
				</div>
				<div class="form-group">
						<button type="submit" class="btn btn-block btn-info" style="text-align: center;"><span data-icon="M" class="linea-icon linea-basic fa-fw" style="font-size: 20px; color: #ffffff; "></span>&nbsp;Simpan Perubahan</button>
				</div>
			</div>
		</div>
	</form>
</div>
@endsection
@section('footerS')
<script src="/bower_components/bootstrap-switch/bootstrap-switch.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js" integrity="sha512-2ImtlRlf2VVmiGZsjm9bEyhjGW4dU7B6TNwh/hx/iSByxNENtj3WVE6o/9Lj4TJeVXPi4bnOIMXFIJJAeufa0A==" crossorigin="anonymous"></script>
<script>
	$(function(){
		$(".bt-switch input[type='checkbox']").bootstrapSwitch();

		var usedNames = {};
		$("select[name='sto'] > option").each(function () {
			if (usedNames[this.value]) {
				$(this).remove();
			} else {
				usedNames[this.value] = this.text;
			}
		});

        $('#sto').select2({ width: '100%'});

        $('#mitra').select2({
            width: '100%'
        })

    });
</script>
@endsection