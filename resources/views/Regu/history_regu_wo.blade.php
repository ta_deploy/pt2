@extends('layout')
@section('title', "Data Keseluruhan ".ucwords(str_replace('_', ' ', Request::segment(5))))
@section('style')
<style type="text/css">
    th, td{
        text-align: center;
    }

    .table{
        font-size: 0.924em;
    }

    .btn {
        font-size: 1em;
    }
</style>
@endsection
@section('content')
<div class="container-fluid" style="padding-top: 25px;">
    <div class="form-group">
        <a type="button" class="btn btn-default" href="/download_data_detail/{{ Request::segment(2) }}/{{ Request::segment(3) }}/{{ Request::segment(4) }}/{{ Request::segment(5) }}/{{ Request::segment(6) }}/{{ Request::segment(7) }}"><i data-icon="&#xe02d;" class="linea-icon linea-elaborate fa-fw" style="font-size: 20px;"></i>&nbsp;Download Excel {{ ucwords(str_replace('_', ' ', Request::segment(2))) }}</a>
    </div>
    <div class="panel panel-warning">
        <div class="panel-heading">List Keseluruhan {{ str_replace('_', ' ', Request::segment(2)) }} {{ str_replace('_', ' ', Request::segment(5)) }}</div>
        <div class="panel-body">
            @if($data)
            <div class="table-responsive">
                <table id="groupPT2" class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>STO</th>
                            <th>SC</th>
                            <th>Tanggal Pengerjaan</th>
                            <th>Label Odp</th>
                            <th>Nama Pelanggan</th>
                            <th>Koordinat Odp</th>
                            <th>Regu</th>
                            <th>status</th>
                            <th colspan="2">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $no = 0;
                        @endphp
                        @foreach($data as $no => $un)
                        <tr>
                            <td>{{ ++$no }}</td>
                            <td>{{ $un->sto }}</td>
                            <td>{{ $un->nomor_sc or 0 }}</td>
                            <td>{{ $un->tgl_pengerjaan }}</td>
                            <td>{{ $un->odp_nama }}</td>
                            <td>{{ $un->project_name }}</td>
                            <td>{{ $un->odp_koor }}</td>
                            <td>{{ $un->regu_name }}</td>
                            <td>{{ $un->lt_status }}</td>
                            @php
                                if (!empty($un->regu_id)) {
                                    $cols = 1;
                                } else {
                                    $cols = 2;
                                }
                            @endphp
                            {{-- ini buat tombol pt2 --}}
                            @if (in_array(Session::get('auth')->pt2_level, [2, 5]))
                                @if(@$un->sl != 75 && !empty($un->sl) && $un->kategory_non_unsc == 0)
                                <td colspan="2"><a href="/admin/push/pt2/{{ $un->scid }}" type="button" class="btn btn-light" data-id="{{$un->id}}"><span data-icon="8" class="linea-icon linea-basic fa-fw" style="font-size: 17px; "></span>Dorong Lagi Ke PT-2</a>
                                @else
                                <td colspan="{{ $cols }}"><a type="button" class="btn btn-light" href='{{ ($un->kategory_non_unsc == 0 && !empty($un->regu_id)) ? URL::to("/admin/dispatch/edit/add_s/{$un->id_pt2}") : (empty($un->regu_id) ? URL::to("/admin/dispatch/add_s/{$un->id}") : URL::to("/admin/edit/non_un/{$un->id}")) }}'><span data-icon="#" class="linea-icon linea-basic fa-fw" style="font-size: 17px; "></span>&nbsp; {{($un->regu_id)?($un->regu_name):'Dispatch'}}</a>
                                </td>
                                @endif
                                @if(!empty($un->regu_id))
                                <td><a style="color: #CA3A34FF;" type="button" class="btn delete_mine delete btn-light" data-id="{{$un->id}}"><span data-icon="&#xe01c;" class="linea-icon linea-basic fa-fw" style="font-size: 17px; "></span>Delete</a>
                                </td>
                                @endif
                            @endif
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            @else
            Tidak Ada Data!
            @endif
        </div>
    </div>
</div>
@endsection
@section('footerS')
<script type="text/javascript">
    $(function(){
        $('.delete_mine').on('click', function () {
            var valuen = $(this).attr('data-id');
            Swal.fire({
                title: 'Seriusan?',
                text: "Tidak akan bisa dikembalikan jika terhapus",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, hapus!'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: "GET",
                        data: {data : valuen},
                        url: "/admin/delete/mydispatch&",
                        cache: false,
                        success: function(response) {
                            Swal.fire(
                                'Terhapus!',
                                'Orderan berhasil terhapus',
                                'success'
                                )
                            $('#detail_layout').trigger('click.dismiss.bs.modal')
                            location.reload();
                        }
                    });
                }
            });
        });
    });
</script>
@endsection

