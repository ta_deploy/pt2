@extends('layout')
@section('title', 'Detail Hapus Regu')
@section('style')
<style type="text/css">
    th, td{
        text-align: center;
        white-space:nowrap;
    }

    .panel .panel-action a{
        opacity: 1;
    }

    .badge-green{
        background-color: #66A955FF;
    }

    .badge-dark_orange{
        background-color: #FF8C00;
    }

    .badge-warna_jalan{
        background-color: #004A7FFF;
    }

    .badge-tidak_sempat{
        background-color: #FF00D3FF;
    }

    .badge-primary{
        background-color: #ab8ce4;
    }

    .badge-default{
        background-color: #98a6ad;
    }
</style>
@endsection
@section('content')
<div class="container-fluid" style="padding-top: 25px;">
    <ul class="list-group list-group-full">
        @foreach ($get_list as $data)
            @switch($data->lt_status)
                @case('Selesai')
                    @php
                        $badge = 'success';
                    @endphp
                @break
                @case('Ogp')
                    @php
                        $badge = 'primary';
                    @endphp
                @break
                @case('Kendala')
                    @php
                        $badge = 'danger';
                    @endphp
                @break
                @case('Pending')
                    @php
                        $badge = 'green';
                    @endphp
                @break
                @case('Tidak Sempat')
                    @php
                        $badge = 'tidak_sempat';
                    @endphp
                @break
                @case('Berangkat')
                    @php
                        $badge = 'warna_jalan';
                    @endphp
                @break
                @case('saldo')
                    @php
                        $badge = 'info';
                    @endphp
                @break
                @default
                    @php
                        $badge = '';
                    @endphp
            @endswitch
            <a href="/regu/history/{{ Request::segment(3) }}/{{ $data->lt_status ?: 'belum_dikerjakan' }}">
                <li class="list-group-item">
                        <span style="cursor: pointer" class="badge badge-{{ $badge }}">{{ $data->total }}</span> {{ $data->lt_status ?: 'Belum Dikerjakan' }}
                </li>
            </a>
        @endforeach
        @if (@$data->ACTIVE == 1)
             <a style="margin-top: 10px;" type="button" class="delete_mine btn btn-danger" data-regu="{{ Request::segment(3) }}"><i data-icon="&#xe01c;" class="linea-icon linea-basic fa-fw" style="color:#ffffff; font-size: 20px;"></i><span style="color:#ffffff;">&nbsp;Hapus</span></a>
        @else
             <a style="margin-top: 10px;" type="button" class="reactive btn btn-primary" data-regu="{{ Request::segment(3) }}"><i data-icon="!" class="linea-icon linea-basic fa-fw" style="color:#ffffff; font-size: 20px;"></i><span style="color:#ffffff;">&nbsp;Aktifkan Kembali</span></a>
        @endif
    </ul>
</div>
@endsection
@section('footerS')
<script type="text/javascript">
    $(function(){
        $('.delete_mine').on('click', function () {
            var valuen = $(this).attr('data-regu');
            Swal.fire({
                title: 'Peringatan!',
                text: "Regu akan di NON-Aktifkan!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, hapus!'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: "GET",
                        url: "/regu/hapus/"+valuen,
                        cache: false,
                        success: function(response) {
                            Swal.fire(
                                'Terhapus!',
                                'Regu berhasil terhapus',
                                'success'
                                )
                            location.reload();
                        }
                    });
                }
            });
        });

        $('.reactive').on('click', function () {
            var valuen = $(this).attr('data-regu'),
            url = "/regu/reactive/"+valuen;
            console.log(valuen, url)
            Swal.fire({
                title: 'Peringatan!',
                text: "Regu akan di Aktifkan Kembali!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, Kembalikan!'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: "GET",
                        url: url,
                        success: function(response) {
                            Swal.fire(
                                'Terhapus!',
                                'Regu berhasil dikembalikan',
                                'success'
                            );

                            location.href = "/regu/list"
                        }
                    });
                }
            });
        });
    });
</script>
@endsection
