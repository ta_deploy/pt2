@extends('layout')
@section('title', 'List Regu')
@section('style')
<style type="text/css">
    th, td{
        text-align: center;
        white-space:nowrap;
    }

    .panel .panel-action a{
        opacity: 1;
    }
</style>
@endsection
@section('content')
<div class="container-fluid" style="padding-top: 25px;">
    <div class="form-group">
        <a type="button" class="btn btn-default" href="{{URL::to('/regu/add')}}"><i data-icon="&#xe02d;" class="linea-icon linea-elaborate fa-fw" style="font-size: 20px;"></i>&nbsp;Tambah</a>
    </div>
    <div class="panel panel-default">
            <div class="panel-heading">Regu Tidak Aktif <b>{{ count($disabled) }} Buah</b>
                <div class="panel-action">
                    <a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a>
                </div>
            </div>
            <div class="panel-wrapper collapse in" aria-expanded="true">
                <div class="panel-body">
                    @foreach ($disabled as $regu)
                        <div class="col-md-4" style="margin-bottom: 10px;">
                            <div class="card text-center card-custom">
                                <div class="card-header">
                                    <ul class="nav nav-tabs card-header-tabs">
                                        <li class="nav-item">
                                            <a class="nav-link active" href="#">Regu</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" style="background-color: #03a9f3; color: white;"
                                                href="/regu/edit/{{ $regu->id_regu }}">Edit</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" style="background-color: #ff6849; color: white;"
                                                href="/regu/hapus/{{ $regu->id_regu }}">Lihat List WO</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="card-block">
                                    <h4 class="card-title">{{ $regu->uraian }}</h4>
                                    <p class="card-text">
                                        @php
                                            $get_data = [$regu->nik1, $regu->nama1 ? "<b>$regu->nama1</b>" : '<b style="color: red;">Tidak ada nama!</b>'];
                                        @endphp
                                        {!! implode(' => ', $get_data) !!}
                                        <br/>
                                        @php
                                            $get_data2 = [$regu->nik2, $regu->nama2 ? "<b>$regu->nama2</b>" : '<b style="color: red;">Tidak ada nama!</b>'];
                                        @endphp
                                        {!! implode(' => ', $get_data2) !!}
                                    </p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    @foreach ($list as $data)
        <div class="panel panel-default">
            <div class="panel-heading">TL: {{ $data->TL }} ({{ $data->namatl }}) <b>{{ count($data->list) }} Buah</b>
                <div class="panel-action">
                    <a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a>
                </div>
            </div>
            <div class="panel-wrapper collapse in" aria-expanded="true">
                <div class="panel-body">
                    @foreach ($data->list as $regu)
                        <div class="col-md-4" style="margin-bottom: 10px;">
                            <div class="card text-center card-custom">
                                <div class="card-header">
                                    <ul class="nav nav-tabs card-header-tabs">
                                        <li class="nav-item">
                                            <a class="nav-link active" href="#">Regu</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" style="background-color: #03a9f3; color: white;"
                                                href="/regu/edit/{{ $regu->id_regu }}">Edit</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" style="background-color: #ff6849; color: white;"
                                                href="/regu/hapus/{{ $regu->id_regu }}">Non Aktif</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="card-block">
                                    <h4 class="card-title">{{ $regu->uraian }}</h4>
                                    <p class="card-text">
                                        @php
                                            $get_data = [$regu->nik1, $regu->nama1 ? "<b>$regu->nama1</b>" : '<b style="color: red;">Tidak ada nama!</b>'];
                                        @endphp
                                        {!! implode(' => ', $get_data) !!}
                                        <br/>
                                        @php
                                            $get_data2 = [$regu->nik2, $regu->nama2 ? "<b>$regu->nama2</b>" : '<b style="color: red;">Tidak ada nama!</b>'];
                                        @endphp
                                        {!! implode(' => ', $get_data2) !!}
                                    </p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    @endforeach
</div>
@endsection
@section('footerS')
<script type="text/javascript">
    $(function(){
        // $('.delete_mine').on('click', function () {
        //     var valuen = $(this).attr('data-regu');
        //     Swal.fire({
        //         title: 'Seriusan?',
        //         text: "Tidak akan bisa dikembalikan jika terhapus",
        //         type: 'warning',
        //         showCancelButton: true,
        //         confirmButtonColor: '#3085d6',
        //         cancelButtonColor: '#d33',
        //         confirmButtonText: 'Ya, hapus!'
        //     }).then((result) => {
        //         if (result.value) {
        //             $.ajax({
        //                 type: "GET",
        //                 url: "/regu/hapus/"+valuen,
        //                 cache: false,
        //                 success: function(response) {
        //                     Swal.fire(
        //                         'Terhapus!',
        //                         'Orderan berhasil terhapus',
        //                         'success'
        //                         )
        //                     location.reload();
        //                 }
        //             });
        //         }
        //     });
        // });
    });
</script>
@endsection
