@extends('layout')
@section('title', "Data Keseluruhan ".ucwords(str_replace('_', ' ', Request::segment(2))) )
@section('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.css">
<style type="text/css">
    th, td{
        text-align: center;
    }

    .table{
        font-size: 0.924em;
    }

    .btn {
        font-size: 1em;
    }
</style>
@endsection
@section('content')
<div class="modal fade modal_detail_layout detail_layout modal_lg" id="detail_layout">
    <div class="modal_lg" role="document" style="width: 95%; margin: auto;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel" style="width: 400px;"><div id="nama_project"></div></h5>
                <div style="float: left; clear: both;" id='download_photo_'>
                    <a type="button" class="btn btn-info download_zip_photo" href="" style="text-align: center; color: #ffffff;"><span data-icon="&#xe035;" class="linea-icon linea-aerrow fa-fw" style="font-size: 20px; color: #ffffff; "></span>&nbsp;Unduh Foto Teknisi</a>
                    <a type="button" class="btn btn-info download_abd_file" href="" style="margin-left: 10px; text-align: center; color: #ffffff;"><span data-icon="M" class="linea-icon linea-software fa-fw" style="font-size: 20px; color: #ffffff; "></span>&nbsp;Unduh ABD</a>
                </div>
                <div class="row">
                    <div id="download_file_rfc">
                        <a type="button" class="btn btn-warning download_file_pdf" href="" download style="text-align: center; margin-left: 6px; color: #ffffff;"><span data-icon="&#xe00b;" class="linea-icon linea-basic fa-fw" style="font-size: 20px; color: #ffffff; "></span>&nbsp;Unduh PDF RFC</a>
                    </div>
                    <div id="delete_mine">
                        <a type="button" style="text-align: center; margin-left: 6px; color: #ffffff;" class="btn btn-danger layout_del" style="float: right;"><i data-icon="&#xe01d;" class="linea-icon linea-basic fa-fw"></i>Delete</a>
                    </div>
                    <div id="saldo_wo">
                        <a type="button" class="btn btn-info layout_saldo" style="float: right;margin-right: 12px;"></a>
                    </div>
                </div>
            </div>
            <div class="modal-body" style="overflow-y:auto;padding: 0px 0px 0px 0px;">
                <div id="content"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid" style="padding-top: 25px;">
    <div class="form-group">
        @if(Request::segment(7) )
            <a type="button" class="btn btn-default" href="/download_data_detail/{{ Request::segment(2) }}/{{ Request::segment(3) }}/{{ Request::segment(4) }}/{{ Request::segment(5) }}/{{ Request::segment(6) }}/{{ Request::segment(7) }}"><i data-icon="&#xe02d;" class="linea-icon linea-elaborate fa-fw" style="font-size: 20px;"></i>&nbsp;Download Excel {{ ucwords(str_replace('_', ' ', Request::segment(2))) }}</a>
        @endif
    </div>
    <div class="panel panel-warning">
        <div class="panel-heading">List Keseluruhan {{ str_replace('_', ' ', urldecode(Request::segment(2) ) ) }}</div>
        <div class="panel-body">
            @if($data)
            <div class="table-responsive">
                <table id="groupPT2" class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>STO</th>
                            <th>SC</th>
                            <th>Tanggal Pengerjaan</th>
                            <th>Label Odp</th>
                            <th>Label ASPL</th>
                            <th>Nama Pelanggan</th>
                            <th>Koordinat Odp</th>
                            <th>Regu</th>
                            <th>status</th>
                            <th>status GO-LIVE</th>
                            <th>Detail Kendala</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $no = 0;
                        @endphp
                        @foreach($data as $no => $un)
                        <tr>
                            <td>{{ ++$no }}</td>
                            <td>{{ $un->sto }}</td>
                            <td>{{ $un->nomor_sc or 0 }}</td>
                            <td>{{ $un->tgl_pengerjaan }}</td>
                            <td>{{ $un->odp_nama }}</td>
                            <td>{{ $un->aspl_nama ?? '-' }}</td>
                            <td>{{ $un->project_name }}</td>
                            <td>{{ $un->odp_koor }}</td>
                            <td>{{ $un->regu_name }}</td>
                            <td>{{ $un->lt_status ?? '-' }}</td>
                            <td>
                                @if ($un->lanjut_PT1 == 1)
                                    Sudah Lanjut PT-1
                                @elseif($un->GOLIVE == 1)
                                    Sudah Go-LIVE
                                @elseif($un->upload_abd_4 == 1)
                                    Sudah Upload ABD Valid 4
                                @elseif($un->upload_abd_4 == 0)
                                    Belum Upload ABD Valid 4
                                @else
                                    Belum Go-Live
                                @endif
                            </td>
                            <td>{{ $un->kendala_detail ?? '-' }}</td>
                            @php
                                if (!empty($un->regu_id)) {
                                    $cols = 1;
                                } else {
                                    $cols = 2;
                                }
                            @endphp
                            <td>
                                @if($un->lt_status != 'Selesai')
                                    @if (in_array(Session::get('auth')->pt2_level, [2, 5]))
                                        @if(@$un->sl != 75 && !empty($un->sl) && $un->kategory_non_unsc == 0)
                                            <a href="/admin/push/pt2/{{ $un->scid }}" type="button" class="btn btn-sm btn-light" data-id="{{$un->id}}"><span data-icon="8" class="linea-icon linea-basic fa-fw" style="font-size: 17px; "></span>Dorong Lagi Ke PT-2</a>
                                        @else
                                            <a type="button" class="btn btn-sm btn-light" href='{{ ($un->kategory_non_unsc == 0 && !empty($un->regu_id)) ? URL::to("/admin/dispatch/edit/add_s/{$un->id_pt2}") : (empty($un->regu_id) ? URL::to("/admin/dispatch/add_s/{$un->id}") : URL::to("/admin/edit/non_un/{$un->id}")) }}'><span data-icon="U" class="linea-icon linea-basic fa-fw" style="font-size: 17px; "></span>&nbsp; {{($un->regu_id)  ? 'Edit' : 'Dispatch'}}</a>
                                        @endif
                                        @if(!empty($un->regu_id) )
                                            <a type='button' style='margin-left:12px; color: #03a9f3;' class='btn btn-light btn-sm' target='_blank' href='/search/data_id?search={{ $un->id }}'><i data-icon='%' class='linea-icon linea-basic fa-fw' style='font-size: 17px;'></i>&nbsp;lihat</a>
                                        @endif
                                    @endif
                                @elseif($un->lt_status == 'Selesai')
									<a type="button" style="color: #03a9f3;" data-id="{{ $un->id }}" class="btn btn-light btn-sm liat_dunk"><i data-icon='%' class='linea-icon linea-basic fa-fw' style='font-size: 17px;'></i>&nbsp;Lihat Detail</a>
                                @else
                                    <a style="color: #CA3A34FF;" type="button" class="btn delete_mine delete btn-light" data-id="{{$un->id}}"><span data-icon="&#xe01c;" class="linea-icon linea-basic fa-fw" style="font-size: 17px; "></span>Delete</a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            @else
                Tidak Ada Data!
            @endif
        </div>
    </div>
</div>
@endsection
@section('footerS')
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.js"></script>
<script type="text/javascript">
    $(function(){
        var detch_del = $('.layout_del').detach(),
		detch_saldo = $('.layout_saldo').detach(),
		detch_download_phto = $('.download_zip_photo').detach(),
		detch_abd = $('.download_abd_file').detach(),
		detch_download_rfc = $('.download_file_pdf').detach();

        $('.liat_dunk').on('click', function(e){
			value = $(this).attr('data-id'),
			res = value.toUpperCase();
			$.ajax({
				type: 'GET',
				data: {data : value, angka : 0},
				url : "/admin/get_odp/LY",
				beforeSend: function() {
					$("#parent").toggleClass('preloader');
					$("#child").toggleClass('cssload-speeding-wheel');
					$("#parent").css({
						'background-color' : 'rgba(0,0,0,.8)'
					});
				},
				success: function(data) {
					$("#parent").toggleClass('preloader');
					$("#child").toggleClass('cssload-speeding-wheel');
					$("#parent").css({
						'background-color' : ''
					});
					if(data.lt_status == 'Selesai'){
						$.ajax({
							url:"/Download/pdf/"+data.id,
							type:'HEAD',
							error: function()
							{
							},
							success: function()
							{
								detch_download_rfc.appendTo( "#download_file_rfc" );
								$('.download_file_pdf').attr("href", '/Download/pdf/'+data.id);
							}
						});
					}
					$.ajax({
						url:"/Download/file/"+data.id,
						type:'HEAD',
						error: function(e)
						{
						},
						success: function()
						{
							detch_download_phto.appendTo( "#download_photo_" );
							$('.download_zip_photo').attr("href", "/Download/file/"+data.id);

							detch_abd.appendTo( "#download_photo_" );
							$('.download_abd_file').attr("href", "/Download/abd/"+data.id);
						}
					});

                    function convertMiliseconds(miliseconds, format) {
                        var days, hours, minutes, seconds, total_hours, total_minutes, total_seconds;

                        total_seconds = parseInt(Math.floor(miliseconds / 1000));
                        total_minutes = parseInt(Math.floor(total_seconds / 60));
                        total_hours = parseInt(Math.floor(total_minutes / 60));
                        days = parseInt(Math.floor(total_hours / 24));

                        seconds = parseInt(total_seconds % 60);
                        minutes = parseInt(total_minutes % 60);
                        hours = parseInt(total_hours % 24);

                        switch(format) {
                            case 's':
                            return total_seconds;
                            case 'm':
                            return total_minutes;
                            case 'h':
                            return total_hours;
                            case 'd':
                            return days;
                            default:
                            return { d: days, h: hours, m: minutes, s: seconds };
                        }
                    };

					var today = new Date().getTime(),
					raw_today = new Date(),
					Christmas = new Date(data.tgl_pengerjaan).getTime(),
					raw_Christmas = new Date(data.tgl_pengerjaan),
					total_kerja = Math.floor((raw_today - raw_Christmas) / 86400000),
					work = total_kerja;
					console.log(raw_Christmas, raw_today)
					var HOLIDAYS = [new Date(2015,1-1,1).getTime(), new Date(2015,1-1,26).getTime(),
					new Date(2015,4-1,3).getTime(), new Date(2015,4-1,6).getTime(),
					new Date(2015,4-1,25).getTime(), new Date(2015,12-1,25).getTime(),
					new Date(2015,12-1,26).getTime()],
					d = raw_Christmas;
					while (d <= raw_today) {
						if ((d.getDay() || 7) > 6) {
							work--;
						}
						else if ($.inArray(d.getTime(), HOLIDAYS) > -1) {
							work--;
						}
						d.setDate(d.getDate() + 1);
					}
					var diffMs = (today - Christmas - ((total_kerja - work) * 86400000)),
					diffDays = convertMiliseconds(diffMs, 'd');
					console.log(diffMs, diffDays)
					if(data.lt_status != 'Selesai' || data.lt_status !== null){
						detch_del.appendTo( "#delete_mine" );
						$('.layout_del').attr('data-id', data.id);
					}

				},
				error: function(e){
				}
			});

			$.ajax({
				type: 'GET',
				data: {data : value, angka : 1},
				url : "/admin/get_odp/LY",
				success: function(data) {
					$('#detail_layout').modal('show');
					$('#nama_project').text('Detail Data');
					$('#content').html(data);
				},
				error: function(e){
					console
				}
			});
		});

        $('.table').DataTable( {
            drawCallback: function () {
                $( 'table.import_list tbody tr td' ).css( 'padding', '5px 8px 5px 8px' );
            },
            fixedHeader: {
                header: true,
                footer: true
            },
            order:	[
                [ 0, "asc" ]
            ],
            deferRender: true,
            scrollCollapse: true,
            scroller: true,
        }).columns.adjust().draw();

        $('.delete_mine').on('click', function () {
            var valuen = $(this).attr('data-id');
            Swal.fire({
                title: 'Seriusan?',
                text: "Tidak akan bisa dikembalikan jika terhapus",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, hapus!'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: "GET",
                        data: {data : valuen},
                        url: "/admin/delete/mydispatch&",
                        cache: false,
                        success: function(response) {
                            Swal.fire(
                                'Terhapus!',
                                'Orderan berhasil terhapus',
                                'success'
                                )
                            $('#detail_layout').trigger('click.dismiss.bs.modal')
                            location.reload();
                        }
                    });
                }
            });
        });
    });
</script>
@endsection

