@extends('layout')
@section('title', 'List Lanjut Go-Live')
@section('style')
<style type="text/css">
	th, td{
		text-align: center;
		white-space:nowrap;
	}

</style>
@endsection
@section('headerS')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="/bower_components/select2-bootstrap/select2-bootstrap.css" />
@endsection
@section('content')
@if (Session::has('alerts_tele'))
	@foreach(Session::get('alerts_tele') as $alert)
		<div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
	@endforeach
@endif
<div class="modal fade" id="upload_abd">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="upload_abdLabel">Upload ABD Valid 4</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
				<form action="/upload_abd" id="form_abd" method="POST" enctype="multipart/form-data">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type="hidden" name="id_pt2" value="">
					<input type="file" class="form-control" id="abd_file" name="abd_file" data-toggle="tooltip" data-placement="left" title="Harus di isi!">
					<textarea class="form-control" name="catatan_abd" id="catatan_abd" placeholder="Catatan Abd"></textarea>
				</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary submit_abd">Submit File</button>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid" style="padding-top: 25px;">
	<div class="panel panel-danger">
		<div class="panel-heading">List Data Belum GO-LIVE</div>
		<div class="panel-body">
			<table id="teknisi" class="table table-bordered">
				<thead>
					<tr>
						<th>No</th>
						<th>Tanggal Selesai</th>
						<th>Nama Team</th>
						<th>Nama ODP</th>
						<th>SC</th>
						<th>File ABD</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php $result = 1; ?>
					@foreach($data_golive as $no => $m_o)
					<tr>
						<td class="align-middle">{{ $result++ }}</td>
						<td class="align-middle">{{ $m_o->tgl_selesai }}</td>
						<td class="align-middle">{{ $m_o->regu_name }}</td>
						<td class="align-middle">{{ $m_o->odp_nama }}</td>
						<td class="align-middle">{{ $m_o->nomor_sc }}</td>
						<td class="align-middle">
							@php
								$check_path = public_path() . '/upload/pt_2_3/' . $m_o->id . '/';
								$rfc_file = @preg_grep('~^File ABD.*$~', scandir($check_path) );

								$path = null;

								if(count($rfc_file) != 0)
								{
									$files = array_values($rfc_file);
									$path = public_path() . '/upload/pt_2_3/' . $m_o->id . '/'.$files[0];
								}
							@endphp
							@if($path)
								<a href="{{ $path }}" type="button" class="btn btn-light"><span data-icon="F" class="linea-icon linea-basic fa-fw" style="font-size: 17px; "></span>Download</a>
							@endif
						</td>
						<td class="align-middle">
							@if(!$path)
								<a href="#" type="button" data-toggle="modal" data-target="#upload_abd" data-id="{{ $m_o->id }}" class="btn btn-light btn_upload_abd4"><span data-icon="&#xe003;" class="linea-icon linea-basic fa-fw" style="font-size: 17px;"></span>Upload ABD Valid 4</a>
							@else
								Dokumen Sudah Diupload!
							@endif
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection
@section('footerS')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.full.min.js"></script>
<script type="text/javascript">
	$(function() {
		$('.check_data').on('click', function(){
			$('#reinput_sc').modal('show');
			$("input[name='dep_id']").val($(this).attr('data-id'));
		});

		$('.btn_upload_abd4').on('click', function(){
			$("input[name='id_pt2']").val($(this).data('id') )
		})

		$('.submit_abd').on('click', function(){
			$("#form_abd").submit()
		})

		$('#sc_no').select2({
			width: '100%',
			placeholder: "Masukkan SC/MYIR Yang Sesuai Dengan WorkOrder GO LIVE",
			dropdownParent: $("#reinput_sc"),
			allowClear: true,
			minimumInputLength: 7,
			ajax: {
				url: "/admin/find_live/sc",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						searchTerm: params.term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true,
				success: function(value) {
					// callback(value);
				}
			}
		});
	});
</script>
@endsection