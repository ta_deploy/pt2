@extends('layout')
@section('title', 'Order Non UNSC')
@section('style')
<style type="text/css">
	.input-photos img {
		width: 100px;
		height: 150px;
		margin-bottom: 5px;
	}
	.select2-results { background-color: #353c48; }
</style>
@endsection
@section('headerS')
<link rel="stylesheet" href="/bower_components/select2/select2.css" />
<link rel="stylesheet" href="/bower_components/select2-bootstrap/select2-bootstrap.css" />
<link rel="stylesheet" href="/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" />
@endsection
@section('content')
<div class="form-group">
	<a type="button" class="btn btn-default" href="/"><i data-icon="&#xe00f;" class="linea-icon linea-aerrow fa-fw" style="font-size: 20px; vertical-align:middle;"></i>&nbsp;Kembali</a>
</div>
<div class="container-fluid" style="padding-top: 25px;">
	<form id="formbuatG" name="formbuatG" method="post" enctype="multipart/form-data" action="{{route('save_dispatch')}}" >
		<div class="panel panel-primary">
			<div class="panel-heading">Tambah Order Non UNSC</div>
			<div class="panel-body">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" class="form-control" id="serial" name="serial" value="{{$data_d->id or ''}}">
				<div class="form-group col-md-12">
					<label class="control-label" for="input-ont">Pilih PO</label>
					<select class="listkontak form-control" style="border: 2px solid #424a56" name="odp_po">
						<option value="{{ $data_d->proaktif_id }}">{{ $data_d->proaktif_id}}</option>
						@foreach ($po as $po_c)
						<option value="{{ $po_c->id_project_Ta }}">{{ $po_c->id_project_Ta.' ('.$po_c->juml_odp.' buah)' }}</option>
						@endforeach
					</select>
				</div>
				{{-- <div class="form-group">
					<label>Nomor Tenos</label>
					<input type="text" class="form-control" id="serial" name="serial" value="">
				</div> --}}
				<div class="form-group col-md-12">
					<label class="control-label" for="input-ont">Nama Project</label>
					<div class="alert project_nama alert-danger" role="alert" style="display: none;">
						<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
						<span class="sr-only">Error:</span>
						Nama Project Tidak Boleh Kosong!
					</div>
					<input type="text" class="form-control" id="project_nama" name="project_nama" value="{{ $data_d->project_name or ''}}">
				</div>
				<div class="form-group col-md-12">
					<label class="control-label" for="input-ont">Nama ODP</label>
					<div class="alert nama_odp alert-danger" role="alert" style="display: none;">
						<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
						<span class="sr-only">Error:</span>
						Nama ODP Tidak Boleh Kosong
					</div>
					<input type="text" class="form-control" id="nama_odp" name="nama_odp" data-inputmask="'mask': 'AAA-AAA-A{2,3}/9{3,4}'" value="{{ $data_d->odp_nama or '' }}">
					<span id="hasil"></span>
				</div>	
				<div class="form-group col-md-12">
					<label class="control-label" for="input-ont">Regu</label>
					<select class="listkontak form-control" name="regu" style="border: 2px solid #424a56">
						<option value="{{ $data_d->regu_id }}">{{ ($data_d->regu_name) }}</option>
						@foreach ($regu as $regu_c)
						<option value="{{ $regu_c->id_regu }}">{{ $regu_c->uraian }}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group col-md-12">
					<label class="control-label" for="input-ont">Tanggal Pengerjaan Teknisi</label>
					<div class="input-group">
						<input type="text" class="form-control" id="tgl_kerja" readonly name="tgl_kerja" value="{{ substr($data_d->tgl_pengerjaan,0,10 ) ?: date('Y-m-d') }}">
						<div class="input-group-addon">
							<span class="glyphicon glyphicon-th"></span>
						</div>
					</div>
				</div>
				<div class="form-group col-md-12">
					<label class="control-label" for="input-ont">STO</label>
					<select class="form-control" id="sto" name="sto" style="border: 2px solid #424a56">
						<option value="{{$data_d->sto}}">{{$data_d->sto}}</option>
						@foreach($sto as $esto)
						<option value="{{$esto->sto}}">{{$esto->sto}}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group col-md-12">
					<label class="control-label" for="input-ont">Koordinat ODP</label>
					<div class="alert no_koor_odp alert-danger" role="alert" style="display: none;">
						<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
						<span class="sr-only">Error:</span>
						Nomor Koordinat ODP tidak Boleh
					</div>
					<div class="alert validate_no_koor_odp alert-danger" role="alert" style="display: none;">
						<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
						<span class="sr-only">Error:</span>
						Gunakan Format Seperti Contoh Ini, -3.327974, 114.583997
					</div>
					<input type="text" class="form-control" id="No_koor_odp" name="no_koor_odp" value="{{ $data_d->odp_koor or '' }}">
				</div>
				<div class="form-group col-md-12">
					<label class="control-label" for="input-ont">Koordinat ODC</label>
					<div class="alert no_koor_odc alert-danger" role="alert" style="display: none;">
						<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
						<span class="sr-only">Error:</span>
						Koordinat ODC Tidak Boleh Kosong
					</div>
					<div class="alert validate_no_koor_odc alert-danger" role="alert" style="display: none;">
						<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
						<span class="sr-only">Error:</span>
						Gunakan Format Seperti Contoh Ini, -3.327974, 114.583997
					</div>
					<input type="text" class="form-control" id="No_koor_odc" name="no_koor_odc" value="{{ $data_d->odc_koor or '' }}">
				</div>
				<div class="form-group col-md-12">
					<label class="control-label" for="input-ont">Catatan HD</label>
					<textarea class="form-control" id="catatan" name="catatan">{{ $data_d->catatan_HD or '' }}</textarea>
				</div>
			</div>
			<div class="col-sm-12">
				<div class="panel panel-default">
					<div class="panel-heading">Screenshot</div>
					<div class="panel-body">
						<div class="row text-center input-photos" style="margin: 20px 0">
							<?php
							$number = 1;
							clearstatcache();
							?>
							@foreach($photodispatch as $input)
							<div class="col-6 col-sm-3">

								<?php
								$path = "/upload/pt_2/{$id}/$input";
								$th   = "$path-th.jpg";
								$img  = "$path.jpg";
								$path2 = "/upload2/pt_2/{$id}/$input";
								$th2   = "$path2-th.jpg";
								$img2  = "$path2.jpg";
								$flag = "";
								$name = "flag_".$input;
								?>
								@if (file_exists(public_path().$th))
								<a href="{{ $img }}">
									<img src="{{ $th }}" alt="{{ $input }}" id="img-{{$input}}" />
								</a>
								<?php
								$flag = 1;
								?>
								@elseif (file_exists(public_path().$th2))
								<a href="{{ $img2 }}">
									<img src="{{ $th2 }}" alt="{{ $img2 }}" id="img-{{$input}}"/>
								</a>
								<?php
								$flag = 1;
								?>
								@else
								<img src="/images/placeholder.gif" alt="" id="img-{{$input}}"/>
								@endif
								<br />
								<input type="text" class="hidden" name="flag_{{ $input }}" value="{{ $flag }}"/>
								<input type="file" class="hidden" name="photo-{{ $input }}" accept="image/jpeg" />
								<button type="button" class="btn btn-sm btn-info">
									<i class="glyphicon glyphicon-camera"></i>
								</button>
								<p>{{ str_replace('_',' ',$input) }}</p>
								{!! $errors->first($name, '<span class="label label-danger">:message</span>') !!}
							</div>
							<?php
							$number++;
							?>
							@endforeach
						</div>
					</div>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="form-group col-md-12">
						<button type="submit" class="btn btn-block save_me btn-primary" style="text-align: center;"><span data-icon="M" class="linea-icon linea-basic fa-fw" style="font-size: 20px; color: #ffffff; "></span>&nbsp;Dispatch</button>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
@endsection
@section('footerS')
<script src="/bower_components/select2/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>
<script src="/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>

<script>
	$(function(){
		function isEmpty(obj) {
			return !obj || Object.keys(obj).length === 0;
		}

		$("#nama_odp").keyup(function()	{		
			var order = $(this).val();
			$("#hasil").html('');
			$.ajax({
				type : 'POST',
				url  : '/check/odp/unique',
				data : {data :order,
					"_token": "{{ csrf_token() }}",
				},
				success : function(data){
					if(isEmpty(data) === false){
						$("#hasil").html(order+' Sudah Pernah Digunakan Sebelumnya');
					}else{
						$("#hasil").html('');

					}
				}
			});
		});

		$('#tgl_kerja').datepicker({
			format: 'yyyy-mm-dd'
		});

		var usedNames = {};
		$("select[name='regu'] > option").each(function () {
			if (usedNames[this.value]) {
				$(this).remove();
			} else {
				usedNames[this.value] = this.text;
			}
		});
		
		$("select[name='odp_po'] > option").each(function () {
			if (usedNames[this.value]) {
				$(this).remove();
			} else {
				usedNames[this.value] = this.text;
			}
		});

		$("select[name='sto'] > option").each(function () {
			if (usedNames[this.value]) {
				$(this).remove();
			} else {
				usedNames[this.value] = this.text;
			}
		});

		$('input[type=file]').change(function() {
			/*console.log(this.name);*/
			var inputEl = this;
			if (inputEl.files && inputEl.files[0]) {
				$(inputEl).parent().find('input[type=text]').val(1);
				var reader = new FileReader();
				reader.onload = function(e) {
					$(inputEl).parent().find('img').attr('src', e.target.result);

				}
				reader.readAsDataURL(inputEl.files[0]);
			}
		});

		$('.input-photos').on('click', 'button', function() {
			$(this).parent().find('input[type=file]').click();
		});

		$('.listkontak').select2({ width: '100%' });

		$('#sto').select2({ width: '100%' });

		$('.save_me').click(function(e){
			e.preventDefault();
			var project_nama = $.trim($('#project_nama').val()),
			nama_odp = $.trim($('#nama_odp').val()),
			No_koor_odp = $.trim($('#No_koor_odp').val()),
			No_koor_odc = $.trim($('#No_koor_odc').val()),
			c_project_nama = $('.project_nama'),
			c_nama_odp = $('.nama_odp'),
			c_No_koor_odp = $('.no_koor_odp'),
			c_No_koor_odc = $('.no_koor_odc'),
			form = $('#formbuatG')[0],
			check_odp,
			regex =/^[-+]?([1-8]?\d(\.\d+)?|90(\.0+)?)[,°]\s*[-+]?(180(\.0+)?|((1[0-7]\d)|([1-9]?\d))(\.\d+)?°?)$/;
			photo = $("#img-KML").attr('src');

			var order = $('#nama_odp').val();
				$.ajax({
					type : 'POST',
					url  : '/check/odp/unique',
					data : {data :order,
						"_token": "{{ csrf_token() }}",
					},
					success : function(data){
						if(isEmpty(data) === false){
							check_odp = 'aman bos';
						}else{
							check_odp = 'santai cu';

						}
					}
				});

			if (project_nama === '') {
				$('.project_nama').css({display: "block"});
				$('#project_nama').css({border: "2px solid red"});
				$('#project_nama').focus();
			}else{
				$('.project_nama').css({display: "none"});
				$('#project_nama').css({border: ""});
			}

			if ((photo) == '/images/placeholder.gif'){
				$.toast({
					heading: 'Foto KML Kosong!',
					text: 'Silahkan Masukkan Foto KML!',
					position: 'top-right',
					stack: false,
					icon: 'error',
					hideAfter: 10000,
					showHideTransition: 'slide'
				})
			}
			
			if (nama_odp === '') {
				$('.nama_odp').css({display: "block"});
				$('#nama_odp').css({border: "2px solid red"});
				$('#nama_odp').focus();
			}else{
				$('.nama_odp').css({display: "none"});
				$('#nama_odp').css({border: ""});
			}

			if(!regex.test(No_koor_odp) && No_koor_odp !== ''){
				$('.validate_no_koor_odp').css({display: "block"});
				$('#No_koor_odp').css({border: "2px solid red"});
				$('#No_koor_odp').focus();
			}else{
				$('.validate_no_koor_odp').css({display: "none"});
				$('#No_koor_odp').css({border: ""})
			}

			if(!regex.test(No_koor_odc) && No_koor_odc !== ''){
				$('.validate_no_koor_odc').css({display: "block"});
				$('#No_koor_odc').css({border: "2px solid red"});
				$('#No_koor_odc').focus();
			}else{
				$('.validate_no_koor_odc').css({display: "none"});
				$('#No_koor_odc').css({border: ""});
			}

			if (No_koor_odp === '') {
				$('.no_koor_odp').css({display: "block"});
				$('#No_koor_odp').css({border: "2px solid red"});
				$('#No_koor_odp').focus();
			}else{
				$('.no_koor_odp').css({display: "none"});
				$('#No_koor_odp').css({border: ""});
			}

			if (No_koor_odc === '') {
				$('.no_koor_odc').css({display: "block"});
				$('#No_koor_odc').css({border: "2px solid red"});
				$('#No_koor_odc').focus();
			}else{
				$('.no_koor_odc').css({display: "none"});
				$('#No_koor_odc').css({border: ""});
			}
			if (project_nama === '' || nama_odp === '' || No_koor_odp === '' || No_koor_odc === '' || !regex.test(No_koor_odp) || !regex.test(No_koor_odc) || ((photo) == '/images/placeholder.gif' || check_odp == 'aman bos')){
				return false;
			}else{
				var notif=$.toast({
					position: 'mid-center',
					showHideTransition: 'plain',
					hideAfter: false
				});
				$.ajax({
					type: 'POST',
					url : "{{route('save_dispatch')}}",
					beforeSend: function(){
						notif.update({
							heading: 'Pemberitahuan',
							text: 'Sedang Disimpan!',
							position: 'mid-center',
							icon: 'info',
							showHideTransition: 'plain',
							stack: false
						});
					},
					data : new FormData(form),
					contentType: false,
					cache: false,
					processData: false,
					success: function(data) {
						notif.update({
							heading: 'Pemberitahuan',
							text: 'Order NON UNSC dengan ODP '+nama_odp+' Telah Berhasil Diperbaharui!',
							position: 'mid-center',
							icon: 'success',
							showHideTransition: 'plain',
							stack: false
						});
						window.location = "/";
						/*console.log(data);*/
					},
					error: function(e){
						var exception = e.responseJSON.exception;
						var file = e.responseJSON.file;
						var line = e.responseJSON.line;
						var message = e.responseJSON.message;
						/*console.log(e);*/
						$.ajax({
							type: 'POST',
							url: "/send/err",
							data: {
								"_token": "{{ csrf_token() }}",
								"exception" : exception,
								"file" : file,
								"line" : line,
								"message" : message
							},
							success: function (data) {
								/*console.log(data);*/
							},
							error: function(e){
								/*console.log(e);*/
							}
						});
						notif.update({
							heading: 'Error',
							text: 'Data Kesalahan Telah Dikirim. Untuk Lebih Lanjut, Klik link <a href="https://t.me/RendyL">Ini</a>',
							showHideTransition: 'fade',
							icon: 'error',
							showHideTransition: 'plain',
							hideAfter: false,
							stack: false
						});
					}
				});
			}
		});
});
</script>
@endsection