@extends('layout')
@section('title', "List Absen")
@section('style')
<style type="text/css">
    th, td{
        text-align: center;
    }

    .table{
        font-size: 0.924em;
    }
    .btn {
        font-size: 1em;
    }
</style>
@endsection
@section('content')
<div class="container-fluid" style="padding-top: 25px;">
    <div class="panel panel-warning">
        <div class="panel-heading">List Menunggu Verifikasi Absen</div>
        <div class="panel-body">
            @if($data)
            <div class="table-responsive">
                <table id="groupPT2" class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>NIK</th>
                            <th>Regu</th>
                            <th>Tanggal Absen</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $no = 0;
                        @endphp
                        @foreach($data as $no => $un)
                        <tr>
                            <td>{{ ++$no }}</td>
                            <td>{{ $un->nama }} ({{ $un->nik }})</td>
                            <td>{{ $un->uraian }}</td>
                            <td>{{ $un->date_created }}</td>
                            <td><a href="/absen_view/{{ $un->absen_id }}" style="color: rgb(33, 121, 203);" type="button" class="btn btn-light"><span data-icon="H" class="linea-icon linea-basic fa-fw" style="font-size: 17px; "></span>Lihat</a></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            @else
                Tidak Ada Data!
            @endif
        </div>
    </div>
</div>
@endsection
