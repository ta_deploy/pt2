@extends('layout')
@section("title", ($jenis == 'orderan unsc' ? "Order Dispatch" :( $jenis == 'edit data' ? "Edit Dispatch" :( $jenis == 'spl' ? "Tambah Data SPL" : ''))))
@section('style')
<style type="text/css">
	.input-photos img {
		width: 100px;
		height: 150px;
		margin-bottom: 5px;
	}
	/* .select2-results {
		background-color: #353c48;
	} */
</style>
@endsection
@section('headerS')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="/bower_components/select2-bootstrap/select2-bootstrap.css" />
<link rel="stylesheet" href="/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" />
<link rel="stylesheet" href="/bower_components/bootstrap-switch/bootstrap-switch.min.css">
@endsection
@section('content')
<div class="form-group">
	<a type="button" class="btn btn-default" href="/"><i data-icon="&#xe00f;" class="linea-icon linea-aerrow fa-fw" style="font-size: 20px; vertical-align:middle;"></i>&nbsp;Kembali</a>
</div>
<div class="container-fluid" style="padding-top: 25px;">
	<form id="formbuatG" name="formbuatG" method="post" enctype="multipart/form-data" autocomplete="off">
		<div class="panel panel-primary">
			<div class="panel-heading">{{ ($jenis == 'orderan unsc' ? "Order Dispatch" :( $jenis == 'edit data' ? "Edit Data" :( $jenis == 'spl' ? "Tambah Data SPL" : ''))) }}</div>
			<div class="panel-body">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" class="form-control" id="serial" name="id" value="{{ old('id', $data_d->id ?? '') }}">
				{{-- <div class="form-group col-md-12">
					<label class="control-label" for="input-ont">Pilih PO</label>
					<select class="listkontak form-control" name="odp_po" id="odp_po" style="border: 2px solid #424a56">
						@foreach ($po as $po_c)
							<option value="{{ $po_c->id_project_Ta }}">{{ $po_c->id_project_Ta. ($po_c->juml_odp != 0 ? ' ('.$po_c->juml_odp .' buah)' : '' )}}</option>
						@endforeach

					</select>
				</div> --}}
				<div class="form-group col-md-12">
					<label class="control-label" for="input-ont">Nama Project</label>
					<div class="alert project_nama alert-danger" role="alert" style="display: none;">
						<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
						<span class="sr-only">Error:</span>
						Nama Project Tidak Boleh Kosong!
					</div>
					<input type="text" class="form-control" id="project_nama" name="project_nama" value="{{ old('project_nama', $data_d->project_name ?? '') }}">
				</div>
				<div class="form-group col-md-12">
					<label class="control-label" for="input-ont">Nama ODP</label>
					<div class="alert nama_odp alert-danger" role="alert" style="display: none;">
						<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
						<span class="sr-only">Error:</span>
						Nama ODP Tidak Boleh Kosong
					</div>
					<input type="text" class="form-control" id="nama_odp" name="nama_odp" data-inputmask="'mask': 'ODP-AAA-A{2,3}/9{3,4}'" value="{{ old('nama_odp', $data_d->odp_nama ?? '') }}">
					<div class="alert twice_odp alert-danger" role="alert" style="display: none;">
						<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
						<span class="sr-only">Error:</span>
						<span id="hasil"></span> Sudah Pernah Digunakan Sebelumnya
					</div>
				</div>	
				<div class="form-group col-md-12">
					<label class="control-label" for="input-ont">Regu</label>
					<select class="form-control" name="regu" id="regu" style="border: 2px solid #424a56;">
						@if(!empty($data_d->regu_id))
							<option value="{{$data_d->regu_id}}">{{$data_d->regu_name}}</option>
						@endif
						@foreach ($regu as $regu_c)
							<option style="background-color:green;" value="{{ $regu_c->id_regu }}">{{ $regu_c->uraian }}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group col-md-12">
					<label class="control-label" for="input-ont">Tanggal Pengerjaan Teknisi</label>
					<div class="input-group date">
						<input type="text" class="form-control" id="tgl_kerja" readonly name="tgl_kerja" value="{{ date('Y-m-d') }}">
						<div class="input-group-addon">
							<span class="glyphicon glyphicon-th tgl_kerja_wkwk" style="cursor: pointer"></span>
						</div>
					</div>
				</div>
				<div class="form-group col-md-12">
					<label class="control-label" for="input-ont">STO</label>
					<select class="form-control" id="sto" name="sto" style="border: 2px solid #424a56">
						@if(!empty($data_d))
							<option value="{{$data_d->sto}}">{{$data_d->sto}}</option>
						@endif
						@foreach($sto as $esto)
							<option value="{{$esto->sto}}">{{$esto->sto}}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group row col-md-12 col-12" style="padding-right: 0px;">
						<div class=" col-md-12">
								<div class="card">
										<div class="card-header">
												Input ODP COVER
										</div>
										<div class="card-body">
												<div class="col-md-12">
														<span style="color: red;">*Jika WO adalah PT-2 Cover, masukkan ODP yang meng Cover!</span>
												</div>
												<div class="col-md-12" style="margin-bottom: 12px;">
														<h5 class="card-title">Masukkan ODP</h5>
														<div class="card-text">
																<input type="text" class="form-control" id="odp_cover" name="odp_cover" value="{{ old('odp_cover', $data_d->odp_cover ?? '') }}">
														</div>
												</div>
										</div>
								</div>
						</div>
				</div>
				<div class="form-group col-md-12">
					<label class="control-label" for="input-ont">Koordinat ODP</label>
					<div class="alert no_koor_odp alert-danger" role="alert" style="display: none;">
						<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
						<span class="sr-only">Error:</span>
						Nomor Koordinat ODP tidak Boleh
					</div>
					<div class="alert validate_no_koor_odp alert-danger" role="alert" style="display: none;">
						<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
						<span class="sr-only">Error:</span>
						Gunakan Format Seperti Contoh Ini, -3.327974, 114.583997
					</div>
					<input type="text" class="form-control" id="No_koor_odp" name="no_koor_odp" value="{{ old('no_koor_odp', $data_d->odp_koor ?? '')	 }}">
				</div>
					{{-- <div class="form-group">
						<label class="control-label" for="input-ont">Nama ODC</label>
						<input type="text" class="form-control" id="nama_odc" name="nama_odc" value="">
					</div> --}}
					<div class="form-group col-md-12">
						<label class="control-label" for="input-ont">Koordinat ODC</label>
						<div class="alert no_koor_odc alert-danger" role="alert" style="display: none;">
							<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
							<span class="sr-only">Error:</span>
							Koordinat ODC Tidak Boleh Kosong
						</div>
						<div class="alert validate_no_koor_odc alert-danger" role="alert" style="display: none;">
							<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
							<span class="sr-only">Error:</span>
							Gunakan Format Seperti Contoh Ini, -3.327974, 114.583997
						</div>
						<input type="text" class="form-control" id="No_koor_odc" name="no_koor_odc" value="{{ old('no_koor_odc', $data_d->odc_koor ?? '') }}">
					</div>
					<div class="form-group col-md-12">
						<label class="control-label" for="input-ont">Nomor Unsc</label>
						<div class="alert notif_unsc alert-warning" role="alert" style="display: none;">
							<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
							<span class="sr-only">Error:</span>
							Data Ini Menggunakan Nomor MYIR, Gunakan Nomor UNSC Jika Ada
						</div>
						<input type="text" class="form-control" id="nomor_unsc" name="nomor_unsc" value="{{ old('nomor_unsc', $data_d->nomor_sc ?? '') }}" data-toggle="tooltip" data-placement="left" title="Harus di isi jika order UNSC">
					</div>
					<div class="form-group col-md-12">
						<label class="control-label" for="input-ont">Catatan HD</label>
						<textarea class="form-control" id="catatan" name="catatan">{{ old('catatan', $data_d->catatan_HD ?? '') }}</textarea>
					</div>
				</div>
				<div class="col-sm-12">
					<div class="panel panel-default">
						<div class="panel-heading">Screenshot</div>
						<div class="panel-body">
							<div class="row text-center input-photos" style="margin: 20px 0">
								<?php
								$number = 1;
								clearstatcache();
								?>
								@foreach($photodispatch as $input)
								<div class="col-6 col-sm-3">

									<?php
									if($jenis == 'edit data'){
										$path = "/upload/pt_2/{$id}/$input";
										$th   = "$path-th.jpg";
										$img  = "$path.jpg";
										$path2 = "/upload/pt_2_2/{$id}/$input";
										$th2   = "$path2-th.jpg";
										$img2  = "$path2.jpg";
										$path3 = "/upload/pt_2_3/{$id}/$input";
										$th3   = "$path3-th.jpg";
										$img3  = "$path3.jpg";
										$flag = "";
										$name = "flag_".$input;
									}
									
									?>
									@if($jenis == 'edit data')

									{{-- data edit --}}
									@if (file_exists(public_path().$th))
									<a href="{{ $img }}">
										<img src="{{ $th }}" alt="{{ $input }}" id="img-{{$input}}"/>
									</a>
									<?php
									$flag = 1;
									?>
									@elseif (file_exists(public_path().$th2))
									<a href="{{ $img2 }}">
										<img src="{{ $th2 }}" alt="{{ $img2 }}" id="img-{{$input}}"/>
									</a>
									<?php
									$flag = 1;
									?>
									@elseif (file_exists(public_path().$th3))
									<a href="{{ $img3 }}">
										<img src="{{ $th3 }}" alt="{{ $img3 }}" id="img-{{$input}}"/>
									</a>
									<?php
									$flag = 1;
									?>
									@else
									<img src="/images/placeholder.gif" alt="" id="img-{{$input}}"/>
									@endif
									{{-- data edit --}}

									@else
									<img src="/images/placeholder.gif" alt=""  id="img-KML"/>
									@endif
									<br />
									<input type="text" class="hidden" name="flag_{{ $input }}" value="{{ $flag or '' }}"/>
									<input type="file" class="hidden" name="photo-{{ $input }}" accept="image/jpeg" />
									<button type="button" class="btn btn-sm btn-info">
										<i class="glyphicon glyphicon-camera"></i>
									</button>
									<p>{{ str_replace('_',' ',$input) }}</p>
									{!! $errors->first("flag_".$input, '<span class="label label-danger">:message</span>') !!}
								</div>
								<?php
								$number++;
								?>
								@endforeach
							</div>
							@if($jenis == 'spl')
							<div class="col-md-6 block_download" style="margin: 20px 0;">
								<div class="col-md-12 form-group">
									<input type="text" class="hidden" name="kml_download" value="{{$data_d->kml or ''}}"/>
									<a type="button" href="#" class="btn btn-success downlaod_kml" style="text-align: center; color: #ffffff; display: none;"><span data-icon="f" class="linea-icon linea-elaborate fa-fw" style="font-size: 20px; color: #ffffff; "></span>&nbsp;Download KML</a>
								</div>
								<div class="col-md-12 form-group">
									<input type="text" class="hidden" name="mcore_download" value="{{$data_d->mcore or ''}}"/>
									<a type="button" href="#" class="btn btn-success downlaod_mcore" style="text-align: center; color: #ffffff; display: none;"><span data-icon="f" class="linea-icon linea-elaborate fa-fw" style="font-size: 20px; color: #ffffff; "></span>&nbsp;Download MCORE</a>
								</div>
							</div>
							@endif
						</div>
					</div>
				</div>
                 @if(@$data_d->lt_status != 'Selesai')
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="form-group col-md-12">
							<button type="submit" class="btn btn-block btn-primary save_me" style="text-align: center;"><span data-icon="M" class="linea-icon linea-basic fa-fw" style="font-size: 20px; color: #ffffff; "></span>&nbsp;Dispatch</button>
						</div>
					</div>
				</div>
                @endif
			</div>
		</form>
	</div>
	@endsection
	@section('footerS')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/jquery.inputmask.bundle.js"></script>
	<script src="/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
	<script src="/bower_components/bootstrap-switch/bootstrap-switch.min.js"></script>
	<script src="/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
	<script>
		$(function(){	
			var usedNames = {};
			$("select[name='regu'] > option").each(function () {
				if (usedNames[this.value]) {
					$(this).remove();
				} else {
					usedNames[this.value] = this.text;
				}
			});

			$("#nomor_unsc").on("keypress keyup",function (e){
				$(this).val($(this).val().replace(/\D/g, ''));
				var charCode = (e.which) ? e.which : e.keyCode;
				if (charCode > 31 && (charCode < 48 || charCode > 57)) {
					return false;
				}
			});

			$("select[name='sto'] > option").each(function () {
				if (usedNames[this.value]) {
					$(this).remove();
				} else {
					usedNames[this.value] = this.text;
				}
			});

			// $("select[name='odp_po'] > option").each(function () {
			// 	if (usedNames[this.value]) {
			// 		$(this).remove();
			// 	} else {
			// 		usedNames[this.value] = this.text;
			// 	}
			// });

			$('.tgl_kerja_wkwk').on('click', function(e){
				e.preventDefault();
				$("#tgl_kerja").datepicker("show");
			});

			function isEmpty(obj) {
				return !obj || Object.keys(obj).length === 0;
			}

			$("#nama_odp").keyup(function()	{
				var order = $(this).val();
				$("#hasil").html('');
				$.ajax({
					type : 'POST',
					url  : '/check/odp/unique',
					data : {data :order,
						"_token": "{{ csrf_token() }}",
					},
					success : function(data){
						if(isEmpty(data) === false){
							$("#hasil").html(order);
							$('.twice_odp').css({
								display : 'block'
							});
							$('#nama_odp').css({border: "2px solid red"});
							$('#nama_odp').focus();
						}else{
							$("#hasil").html('');
							$('#nama_odp').css({border: ""});
							$('.twice_odp').css({
								display : 'none'
							});
						}
					}
				});
			});

			$('#tgl_kerja').datepicker({
				format: 'yyyy-mm-dd'
			});

			var check_jenis = <?= json_encode($jenis) ?>;
			var dpu_id;
			if(check_jenis == "orderan unsc" || check_jenis == 'spl' ){
				var return_first;
				function callback(response) {
					return_first = response;
					dpu_id = return_first.DPU_ID;
				}


				// function start_press(){
				// 	var value = $('#serial').val();
				// 	return $.ajax({
				// 		type: 'Get',
				// 		url : "/admin/get/odp/"+value+'/'+check_jenis
				// 	});

				// }

				// async function main(){
				// 	try{
				// 		const data = await start_press();
				// 		var url_kml,
				// 		url_mcore;
				// 		callback(data);
				// 		$('#project_nama').val("SC"+data.unsc+"("+data.orderName+")");
				// 		$('#nama_odp').val(data.label_odp);
				// 		$('#No_koor_odp').val(data.koordinat_odp);
				// 		var unsc_string = new String(data.unsc);
				// 		if(unsc_string >= 9 ){
				// 			$('.notif_unsc').css({display: "block"});
				// 		}
				// 		$('#nomor_unsc').val(data.unsc);
				// 		if ($("#sto").find("option[value=" + data.sto_un + "]").length) {
				// 			$("#sto").val(data.sto_un).trigger("change");
				// 		} else { 
				// 			var sto_un = new Option(data.sto_un, data.sto_un, true, true);
				// 			$("#sto").append(sto_un).trigger('change');
				// 		}
				// 		if(check_jenis == "orderan unsc"){
				// 			url_kml = "/upload/files/"+data.kml;
				// 			url_mcore = "/upload/files/"+data.mcore;
				// 		}else{
				// 			url_kml = "/upload/files_spl/"+data.id_referen+'/'+data.kml;;
				// 			url_mcore = "/upload/files_spl/"+data.id_referen+'/'+data.mcore;;
				// 		}
				// 		if(data.kml != null && data.kml.length > 0){
				// 			if (data.kml.indexOf(".PNG") > -1){
				// 				$('#img-KML').prop('src', url_kml);
				// 			}else{
				// 				$('.input-photos').addClass('col-md-6');
				// 				$('.downlaod_kml').css({display: "block"});
				// 				$('.downlaod_kml').prop('href',url_kml);
				// 			}
				// 		}

				// 		if(data.mcore != null && data.mcore.length > 0){
				// 			$('.downlaod_mcore').css({display: "block"});
				// 			$('.downlaod_mcore').attr('href',url_mcore);
				// 		}

				// 	}catch(e){
				// 		/*console.log(e)*/
				// 	}
				// }
				// main();
			}

			$(":input").inputmask();

			$('input[type=file]').change(function() {
				var phptes = <?php echo json_encode($photodispatch); ?>;
				/*console.log(phptes[0]);
				console.log(this.name);*/
				var inputEl = this;
				if (inputEl.files && inputEl.files[0]) {
					$(inputEl).parent().find('input[type=text]').val(1);
					var reader = new FileReader();
					reader.onload = function(e) {
						$(inputEl).parent().find('img').attr('src', e.target.result);

					}
					reader.readAsDataURL(inputEl.files[0]);
				}
			});

			$('.input-photos').on('click', 'button', function() {
				$(this).parent().find('input[type=file]').click();
			});

			$('.listkontak').select2({ width : '100%'});

			$('#sto').select2({ width : '100%'});

			$('#regu').select2({ width : '100%'});

			$('.save_me').on('click', function(e){
				var project_nama = $.trim($('#project_nama').val()),
				nama_odp = $.trim($('#nama_odp').val()),
				No_koor_odp = $.trim($('#No_koor_odp').val()),
				No_koor_odc = $.trim($('#No_koor_odc').val()),
				c_project_nama = $('.project_nama'),
				c_nama_odp = $('.nama_odp'),
				c_No_koor_odp = $('.no_koor_odp'),
				c_No_koor_odc = $('.no_koor_odc'),
				v_No_koor_odp = $('.validate_no_koor_odp'),
				v_No_koor_odc = $('.validate_no_koor_odc'),
				form = $('#formbuatG')[0],
				check_odp,
				photo = $("#img-KML").attr('src'),
				regex =/^[-+]?([1-8]?\d(\.\d+)?|90(\.0+)?)[,°]\s*[-+]?(180(\.0+)?|((1[0-7]\d)|([1-9]?\d))(\.\d+)?°?)$/;
				var order = $('#nama_odp').val()
				$.ajax({
					type : 'POST',
					url  : '/check/odp/unique',
					data : {data :order,
						"_token": "{{ csrf_token() }}",
					},
					success : function(data){
						if(isEmpty(data) === false){
							check_odp = 'aman bos';
						}else{
							check_odp = 'santai cu';

						}
					}
				});

				if (project_nama === '') {
					$('.project_nama').css({display: "block"});
					$('#project_nama').css({border: "2px solid red"});
					$('#project_nama').focus();
				}else{
					$('.project_nama').css({display: "none"});
					$('#project_nama').css({border: ""});
				}

				if ((photo) == '/images/placeholder.gif'){
					$.toast({
						heading: 'Foto KML Kosong!',
						text: 'Silahkan Masukkan Foto KML!',
						position: 'top-right',
						stack: false,
						icon: 'error',
						hideAfter: 10000,
						showHideTransition: 'slide'
					})
				}

				if (nama_odp === '') {
					$('.nama_odp').css({display: "block"});
					$('#nama_odp').css({border: "2px solid red"});
					$('#nama_odp').focus();
				}else{
					$('.nama_odp').css({display: "none"});
					$('#nama_odp').css({border: ""});
				}

				if(!regex.test(No_koor_odp) && No_koor_odp !== ''){
					$('.validate_no_koor_odp').css({display: "block"});
					$('#No_koor_odp').css({border: "2px solid red"});
					$('#No_koor_odp').focus();
				}else{
					$('.validate_no_koor_odp').css({display: "none"});
					$('#No_koor_odp').css({border: ""})
				}

				if(!regex.test(No_koor_odc) && No_koor_odc !== ''){
					$('.validate_no_koor_odc').css({display: "block"});
					$('#No_koor_odc').css({border: "2px solid red"});
					$('#No_koor_odc').focus();
				}else{
					$('.validate_no_koor_odc').css({display: "none"});
					$('#No_koor_odc').css({border: ""});
				}

				if (No_koor_odp === '') {
					$('.no_koor_odp').css({display: "block"});
					$('#No_koor_odp').css({border: "2px solid red"});
					$('#No_koor_odp').focus();
				}else{
					$('.no_koor_odp').css({display: "none"});
					$('#No_koor_odp').css({border: ""});
				}

				if (No_koor_odc === '') {
					$('.no_koor_odc').css({display: "block"});
					$('#No_koor_odc').css({border: "2px solid red"});
					$('#No_koor_odc').focus();
				}else{
					$('.no_koor_odc').css({display: "none"});
					$('#No_koor_odc').css({border: ""});
				}
				if (project_nama === '' || nama_odp === '' || No_koor_odp === '' || No_koor_odc === ''|| !regex.test(No_koor_odp) || !regex.test(No_koor_odc) || ((photo) == '/images/placeholder.gif' || check_odp == 'aman bos')){
					return false;
				}else{
					$("#parent").toggleClass('preloader');
					$("#child").toggleClass('cssload-speeding-wheel');
					$("#parent").css({
						'background-color' : 'rgba(0,0,0,.8)',
						'z-index' : '300'
					});
					var notif=$.toast({
						position: 'mid-center',
						showHideTransition: 'plain',
						hideAfter: false
					});
					notif.update({
						heading: 'Pemberitahuan',
						text: 'Order UNSC dengan ODP '+nama_odp+' Telah Berhasil Ditambahkan!',
						position: 'mid-center',
						icon: 'success',
						showHideTransition: 'plain',
						stack: false
					});
				}
				
			});
		});
	</script>
	@endsection