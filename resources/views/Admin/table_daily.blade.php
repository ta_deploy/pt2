<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="/images/logo.png">
    <link href="/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">
    <link href="/css/colors/default-dark.css" id="theme" rel="stylesheet">
    <style type="text/css">
        th, td{
            text-align: center;
        }
    </style>
</head>
<body>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-static-top m-b-0">
            <div class="navbar-header">
                <div class="top-left-part"><a class="logo"><b><div class="dark-logo"/></div></b></a></div>
                <ul class="nav navbar-top-links navbar-left hidden-xs">
                </ul>
            </div>
        </nav>
        <div class="navbar-default sidebar" role="navigation">
        </div>
        <div id="page-wrapper">
            <div class="container-fluid" style="padding-top: 25px;">
                <div class="panel panel-warning">
                    <div class="panel-heading">Laporan Harian {{date('Y-m-d')}}</div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table id="teknisi" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Team</th>
                                        <th>Order</th>
                                        <th>Sisa</th>
                                        <th>Selesai</th>
                                        <th>Kendala</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $result = 1;
                                    @endphp
                                    @if(isset($group) || isset($group->odp))
                                        @foreach($group as $no => $m_o)
                                            @php $first = true @endphp
                                            @foreach($m_o->odp as $no => $mo)
                                            <tr>
                                                @if($first == true)
                                                <td rowspan="{{$m_o->count}}" class="align-middle">{{$result++}}</td>
                                                @endif
                                                @if($first == true)
                                                <td rowspan="{{$m_o->count}}" class="align-middle">{{$m_o->regu}}</td>
                                                @endif
                                                <td bgcolor="{{($mo->lt_status == 'Selesai'?'#00FF00':($mo->lt_status == 'Kendala' ? '#ff0000':''))}}" style="color:{{($mo->lt_status == 'Selesai'?'Black':($mo->lt_status == 'Kendala' ? 'White':''))}}">{{$mo->odp_nama}}</td>
                                                @if($first == true)
                                                <td rowspan="{{$m_o->count}}" class="align-middle">{{$m_o->count - $m_o->selesai}}</td>
                                                @endif
                                                @if($first == true)
                                                <td rowspan="{{$m_o->count}}" class="align-middle">{{$m_o->selesai}}</td>
                                                @php $first = false @endphp
                                                @endif
                                                <td>{{($mo->lt_status == 'Selesai')?'':$mo->lt_catatan}}</td>
                                            </tr>
                                            @endforeach
                                        @endforeach
                                    @else
                                        <td colspan ="6"><div class="alert alert-danger"> Tidak Ada Data Disini <i data-icon="g" class="linea-icon linea-basic fa-fw"></i></div></td>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <footer class="footer text-center">{{ date('Y') }} &copy;</footer>
    <script src="/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="/bootstrap/dist/js/tether.min.js"></script>
    <script src="/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
</body>
</html>
