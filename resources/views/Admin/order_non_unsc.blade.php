@extends('layout')
@section('title', (!empty($data_d) ? 'Edit Order' : 'Order Dispatch'))
@section('style')
<style type="text/css">
	.input-photos img {
		width: 100px;
		height: 150px;
		margin-bottom: 5px;
	}

	/* .select2-results { background-color: #353c48; } */
	.TI_large  {
		max-width: none;
		white-space: nowrap;
		background:white;
		border:1px solid lightgray;
		-webkit-box-shadow: 0px 3px 3px 0px rgba(0,0,0,0.3);
		-moz-box-shadow: 0px 3px 3px 0px rgba(0,0,0,0.3);
		box-shadow: 0px 3px 3px 0px rgba(0,0,0,0.3);
		color:gray;
		margin:0;
		padding:0;
	}
	.tooltip.bottom .tooltip-arrow {
		top: 0;
		left: 50%;
		margin-left: -5px;
		border-bottom-color: lightgray; /* black */
		border-width: 0 5px 5px;
	}
</style>
@endsection
@section('headerS')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.css">
<link rel="stylesheet" href="/bower_components/select2-bootstrap/select2-bootstrap.css" />
<link rel="stylesheet" href="/bower_components/bootstrap-switch/bootstrap-switch.min.css">
<link rel="stylesheet" href="/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" />

@endsection
@section('content')
<div class="modal fade modal_inputulang view_promitos modal_lg" id="view_promitos">
	<div class="modal_lg" role="document" style="width: 95%; margin: auto;">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel" style="width: 400px;"></h5>
			</div>
			<div class="modal-body modal_avail">
			</div>
		</div>
	</div>
</div>
<div class="form-group">
	<a type="button" class="btn btn-default" href="/"><i data-icon="&#xe00f;" class="linea-icon linea-aerrow fa-fw" style="font-size: 20px; vertical-align:middle;"></i>&nbsp;Kembali</a>
</div>
<div class="container-fluid">
	<form id="formbuatG" name="formbuatG" method="post" enctype="multipart/form-data">
		<div class="panel panel-primary">
			<div class="panel-heading">{{ (!empty($data_d) ? 'Edit Data' : 'Order Dispatch') }}</div>
			<div class="panel-body">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="jenis_wo" value="{{ @$jenis }}">
				<input type="hidden" class="form-control" id="serial" name="id" value="{{ old('id', $data_d->id ?? '') }}">
				{{-- <div class="form-group col-md-12">
					<label class="control-label" for="xxcs">Pilih PO</label>
					<select class="listkontak form-control" name="odp_po" id="odp_po" style="border: 2px solid #424a56">
						@foreach ($po as $po_c)
							<option value="{{ $po_c->id_project_Ta }}">{{ $po_c->id_project_Ta. ($po_c->juml_odp != 0 ? ' ('.$po_c->juml_odp .' buah)' : '' )}}</option>
						@endforeach
					</select>
				</div> --}}
				{{-- <div class="form-group">
					<label>Nomor Tenos</label>
					<input type="text" class="form-control" id="serial" name="serial" value="">
				</div> --}}
				{{-- <div class="form-group col-md-12 group_unique" style="display: none;">
					<textarea class="form-control" style="resize: none; " rows="10" id="conv_text"></textarea>
					<a type="button" class="btn btn-info btn-sm" id="encypt_lol">Isi Ke Form Otomatis</a>
				</div> --}}
				<div class="row col-md-12 mb-4">
					<div class="col-md-3">
						<label class="control-label" for="j_order">Jenis Orderan</label>
						<select class="form-control" name="j_order" id="j_order" style="border: 2px solid #424a56;">
							<option></option>
							@foreach ($j_or as $k => $v)
								<option value={{ $k }}>{{ $v }}</option>
							@endforeach
						</select>
					</div>
					<div class="col-md-3">
						<label class="control-label" for="j_pekerjaan">Jenis Pekerjaan</label>
						<select class="form-control" name="j_pekerjaan" id="j_pekerjaan" style="border: 2px solid #424a56;">
							<option></option>
							<option value="PT-2">PT-2</option>
							<option value="PT-2 PLUS">PT-2 PLUS</option>
						</select>
					</div>
					<div class="col-md-2 non_gamas">
						<label class="control-label" for="regu_tipe">Regu</label>
						<div class="bt-switch">
							<input type="checkbox" data-size="small" name="regu_tipe" id="regu_tipe" class="toogletogle" data-on-color="primary" data-off-color="info" data-on-text="OSOM" data-off-text="Project">
						</div>
					</div>
					<div class="col-md-2 non_gamas">
						<label class="control-label" for="saber">Order Saber</label>
						<div class="bt-switch">
							<input type="checkbox" data-size="small" name="saber" id="saber" class="toogletogle" data-on-color="primary" data-off-color="info" data-on-text="SABER" data-off-text="NO">
						</div>
					</div>
					<div class="col-md-2">
						<label class="control-label" for="booking_aspl">Booking ASPL</label>
						<div class="bt-switch">
							<input type="checkbox" data-size="small" name="booking_aspl" id="booking_aspl" class="toogletogle" data-on-color="primary" data-off-color="info" data-on-text="ASPL" data-off-text="NO">
						</div>
					</div>
					<div class="col-md-4 gamas_select">
						<label class="control-label" for="tipe_jointer">Jointer</label>
						<div class="bt-switch">
							<input type="checkbox" data-size="small" name="tipe_jointer" id="tipe_jointer" class="toogletogle" data-on-color="primary" data-off-color="info" data-on-text="Freelance" data-off-text="Pilih Tim">
						</div>
					</div>
				</div>
				<div class="row col-md-12 mb-2">
					<div class="col-md-6">
						<label class="control-label" for="project_nama">Nama Project</label>
						<div class="alert project_nama alert-danger" role="alert" style="display: none;">
							<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
							<span class="sr-only">Error:</span>
							Nama Project Tidak Boleh Kosong!
						</div>
						<input type="text" class="form-control input-sm" required id="project_nama" name="project_nama" value="{{ old('project_nama', $data_d->project_name ?? '') }}">
					</div>
					<div class="col-md-6 kpro">
						<label class="control-label" for="No_koor_pelanggan">Koordinat Pelanggan</label>
						<input type="text" class="form-control input-sm" id="No_koor_pelanggan" name="no_koor_pelanggan"
							value="{{ $data_d->odp_koor or '' }}">
					</div>
				</div>
				<div class="col-md-12 non_gamas">
					<label class="control-label" for="regu_tipe">List SC</label>
					<textarea class="form-control input-sm" readonly style="resize: vertical">{{ @$data_d->nomor_sc_log ? @$data_d->nomor_sc_log :(@$data_d->nomor_sc ? @$data_d->nomor_sc : '') }}</textarea>
				</div>
				<div class="row col-md-12 mb-2">
					@if (@$data_d->odp_nama)
						<div class="col-md-3">
							<label class="control-label" for="odp_nama">Nama ODP</label>
							<input type="text" class="form-control input-sm" id="odp_nama" name="odp_nama" value="{{ old('odp_nama', $data_d->odp_nama ?? '') }}" readonly>
						</div>
					@endif
					<div class="col-md-3">
						<label class="control-label" for="no_koor_odp">Koordinat ODP</label>
						<div class="alert no_koor_odp alert-danger" role="alert" style="display: none;">
							<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
							<span class="sr-only">Error:</span>
							Nomor Koordinat ODP tidak Boleh
						</div>
						<div class="alert validate_no_koor_odp alert-danger" role="alert" style="display: none;">
							<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
							<span class="sr-only">Error:</span>
							Gunakan Format Seperti Contoh Ini, -3.327974, 114.583997
						</div>
						<input type="text" required class="form-control input-sm" id="No_koor_odp" name="no_koor_odp" value="{{ old('no_koor_odp', $data_d->odp_koor ?? '') }}">
					</div>
					@if (@$data_d->aspl_nama)
						<div class="col-md-3">
							<label class="control-label" for="aspl_nama">Nama ASPL</label>
							<input type="text" class="form-control input-sm" id="aspl_nama" name="aspl_nama" value="{{ old('aspl_nama', $data_d->aspl_nama ?? '') }}" readonly>
						</div>
					@endif
					<div class="col-md-3 field_aspl">
						<label class="control-label" for="no_koor_aspl">Koordinat ASPL</label>
						<div class="alert validate_no_koor_aspl alert-danger" role="alert" style="display: none;">
							<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
							<span class="sr-only">Error:</span>
							Gunakan Format Seperti Contoh Ini, -3.327974, 114.583997
						</div>
						<input type="text" class="form-control input-sm" id="No_koor_aspl" name="no_koor_aspl" value="{{ old('no_koor_aspl', $data_d->aspl_koor ?? '') }}">
					</div>
				</div>
				<div class="row col-md-12 mb-2">
					<div class="nomor_urgent col-md-4" style="display: none;">
						<label class="control-label" for="nomor_id">Nomor ID</label>
						<input type="text" class="form-control input-sm" id="nomor_id" name="nomor_id" value="{{ old('nomor_id', $data_d->nomor_sc ?? '') }}" data-toggle="tooltip" data-placement="left" title="Harus di isi!">
					</div>
					<div class="col-md-3 normalisasis">
						<label class="control-label" for="nama_odp_before">Nama ODP Lama</label>
						<input type="text" class="form-control input-sm" id="nama_odp_before" name="nama_odp_before"
							data-inputmask="'mask': 'ODP-AAA-A{2,3}/9{3,4}'" value="{{ old('nama_odp_before', $data_d->nama_odp_before ?? '') }}">
					</div>
					<div class="col-md-5 normalisasis">
						<label class="control-label" for="no_koor_odp_before">Koordinat ODP Lama</label>
						<input type="text" class="form-control input-sm" id="No_koor_odp_before" name="no_koor_odp_before" value="{{ old('no_koor_odp_before', $data_d->odp_koor ?? '') }}">
					</div>
				</div>
				<div class="row col-md-12 mb-2">
					@if(@$data_d)
						<div class="col-md-12">
							<label class="control-label" for="change_odp">Ganti ODP</label>
							<div class="bt-switch">
								<input type="checkbox" data-size="small" name="change_odp" id="change_odp" class="toogletogle" data-on-color="primary" data-off-color="info" data-on-text="Ganti" data-off-text="Tidak">
							</div>
						</div>
					@endif
					<div class="col-md-2">
						<label class="control-label" for="sto">STO</label>
						<select class="form-control" required id="sto" name="sto" style="border: 2px solid #424a56">
							@if(!empty($data_d))
								<option value="{{$data_d->sto}}">{{$data_d->sto}}</option>
							@endif
							@foreach($sto as $esto)
								<option value="{{$esto->sto}}">{{$esto->sto}}</option>
							@endforeach
						</select>
					</div>
					<div class="col-md-2">
						<label class="control-label" for="nama_odc">Nama ODC</label>
						<select class="form-control" id="nama_odc" name="nama_odc" style="border: 2px solid #424a56">
							@if(!empty($data_d) && @$data_d->odc_nama)
								<option value="{{ explode('-', $data_d->odc_nama)[2] }}">{{ explode('-', $data_d->odc_nama)[2] }}</option>
							@endif
							@foreach($odc as $v)
								<option value="{{$v->odc}}">{{$v->odc}}</option>
							@endforeach
						</select>
					</div>
					{{-- <div class="col-md-3">
						<label class="control-label" for="xxcs">Koordinat ODC</label>
						<div class="alert no_koor_odc alert-danger" role="alert" style="display: none;">
							<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
							<span class="sr-only">Error:</span>
							Koordinat ODC Tidak Boleh Kosong
						</div>
						<div class="alert validate_no_koor_odc alert-danger" role="alert" style="display: none;">
							<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
							<span class="sr-only">Error:</span>
							Gunakan Format Seperti Contoh Ini, -3.327974, 114.583997
						</div>
						<input type="text" class="form-control input-sm" id="No_koor_odc" name="no_koor_odc" value="{{ old('no_koor_odc', $data_d->odc_koor ?? '') }}">
					</div> --}}
					<div class="col-md-4 regu_field">
						<label class="control-label" for="regu">Regu</label>
						<select class="form-control" name="regu" id="regu" required style="border: 2px solid #424a56;">
						</select>
					</div>
					{{-- <div class="col-md-3">
						<label class="control-label" for="xxcs">Masukkan ODP Cover</label>
						<input type="text" class="form-control input-sm" id="odp_cover" name="odp_cover" value="{{ old('odp_cover', $data_d->odp_cover ?? '') }}">
					</div> --}}
					<div class="col-md-2">
						<label class="control-label" for="tgl_kerja">Tanggal Dikerjakan</label>
						<div class="input-group">
							<input type="text" class="form-control input-sm" id="tgl_kerja" readonly name="tgl_kerja" value="{{ date('Y-m-d') }}">
							<div class="input-group-addon">
								<span class="glyphicon glyphicon-th tgl_kerja_wkwk"></span>
							</div>
						</div>
					</div>
					<div class="col-md-2">
						<label class="control-label">&nbsp;</label>
						<div class="input-control">
							<button type="button" class="btn btn-sm checking_odp btn-info text-white">Check Booking</button>
						</div>
					</div>
				</div>
				<div class="row col-md-12">
					<div class="col-md-4">
						<label class="control-label" for="kml_file">File KML</label>
						@if(@$status_file == true)
							@php
								$check_path = public_path() . '/upload/pt_2_3/' . $id . '/';
								$rfc_file = @preg_grep('~^\w.*$~', scandir($check_path) );

								if(count($rfc_file) != 0)
								{
									$files = array_values($rfc_file);
									$path = '/upload/pt_2_3/' . $id . '/'.$files[0];
								}
							@endphp
							<a href="{{ $path }}">Download File</a>
						@else
							<input type="file" class="form-control" id="kml_file" name="kml_file" data-toggle="tooltip" data-placement="left" title="Harus di isi!">
						@endif
					</div>
					<div class="col-md-2 check_booking_result" style="display: none;">
						<label class="control-label" for="nama_odp_selected">Nama ODP</label>
						<input type="text" class="form-control" id="nama_odp_selected" name="nama_odp_selected" readonly>
						<input type="hidden" name="nama_odp_selected_id">
					</div>
					<div class="col-md-4 check_booking_result" style="display: none;">
						<label class="control-label" for="nama_tim">Nama Team</label>
						<input type="text" class="form-control" id="nama_tim" name="nama_tim" readonly>
					</div>
				</div>
				<div class="row col-md-12">
					<div class="col-md-12">
						<label class="control-label" for="catatan">Catatan HD</label>
						<textarea class="form-control" id="catatan" style="resize: vertical;" name="catatan">{{ old('catatan', $data_d->project_name .'<br/>'. $data_d->catatan_HD ?? '') }}</textarea>
					</div>
				</div>
			</div>
				<div class="col-sm-12">
					<div class="panel panel-default">
						<div class="panel-heading">Screenshot</div>
						<div class="panel-body">
							<div class="row text-center input-photos" style="margin: 20px 0">
								@php
									$number = 1;
									clearstatcache();
								@endphp

								@foreach($photodispatch as $input)
									<div class="col-6 col-sm-3">
										@php
											if(empty($id)){
												$id = '';
											}
											$path = "/upload/pt_2/$id/$input";
											$th   = "$path-th.jpg";
											$img  = "$path.jpg";
											$path2 = "/upload/pt_2_2/{$id}/$input";
											$th2   = "$path2-th.jpg";
											$img2  = "$path2.jpg";
											$path3 = "/upload/pt_2_3/{$id}/$input";
											$th3   = "$path3-th.jpg";
											$img3  = "$path3.jpg";
											$flag = "";
											$name = "flag_".$input;
										@endphp

										@if (file_exists(public_path().$th))
											<a href="{{ $img }}">
												<img src="{{ $th }}" alt="{{ $input }}" id="img-{{$input}}" />
											</a>
											@php
												$flag = 1;
											@endphp
										@elseif (file_exists(public_path().$th2))
											<a href="{{ $img2 }}">
												<img src="{{ $th2 }}" alt="{{ $img2 }}" id="img-{{$input}}"/>
											</a>
											@php
												$flag = 1;
											@endphp
										@elseif (file_exists(public_path().$th3))
											<a href="{{ $img3 }}">
												<img src="{{ $th3 }}" alt="{{ $img3 }}" id="img-{{$input}}"/>
											</a>
											@php
												$flag = 1;
											@endphp
										@else
											<img src="/images/placeholder.gif" alt="" id="img-{{$input}}"/>
										@endif
										<br />
										<input type="text" class="hidden" name="flag_{{ $input }}" value="{{ $flag }}"/>
										<input type="file" class="hidden" name="photo-{{ $input }}" accept="image/jpeg" />
										<button type="button" class="btn btn-sm btn-info">
											<i class="glyphicon glyphicon-camera"></i>
										</button>
										<p>{{ str_replace('_',' ',$input) }}</p>
										{!! $errors->first($name, '<span class="label label-danger">:message</span>') !!}
									</div>
									@php
										$number++;
									@endphp
								@endforeach
							</div>
						</div>
					</div>
				</div>
				@if(@$data_d->lt_status != 'Selesai' || session('auth')->pt2_level == 5 )
					<div class="panel panel-default">
						<div class="panel-body">
							<div class="form-group col-md-12">
								<button type="submit" class="btn btn-block btn-primary save_me" style="text-align: center;"><span data-icon="M" class="linea-icon linea-basic fa-fw" style="font-size: 20px; color: #ffffff; "></span>&nbsp;Dispatch</button>
							</div>
						</div>
					</div>
				@endif
			</div>
		</form>
	</div>
	@endsection
	@section('footerS')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/jquery.inputmask.bundle.js"></script>
	<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.js"></script>
	<script src="/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
	<script src="/bower_components/bootstrap-switch/bootstrap-switch.min.js"></script>
	<script src="/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
	<script>
		$(function(){
			$(".bt-switch input[type='checkbox']").bootstrapSwitch();

			$('.tgl_kerja_wkwk').on('click', function(e){
				e.preventDefault();
				$("#tgl_kerja").datepicker("show");
			});

			function isEmpty(obj) {
				return !obj || Object.keys(obj).length === 0;
			}

			$('#booking_aspl').on('switchChange.bootstrapSwitch', function (event, state) {
				if($(this).is(':checked') ){
					$('.field_aspl').css({display: 'block'});
		    }else{
					$('.field_aspl').css({display: 'none'});
				}
			});

			$('#change_odp').on('switchChange.bootstrapSwitch', function (event, state) {
				if($(this).is(':checked') ){
					$('.checking_odp').css({display: 'block'});
		    }else{
					$('.checking_odp').css({display: 'none'});
				}
			});

			if($('#change_odp').prop('checked') ){
				$('.checking_odp').css({display: 'block'});
			}else{
				$('.checking_odp').css({display: 'none'});
			}

			$('#booking_aspl').bootstrapSwitch('state', true);
			$('#change_odp').bootstrapSwitch('state', false);

			// $("#nama_odp").keyup(function(){
			// 	var order = $(this).val();
			// 	$("#hasil").html('');
			// 	$.ajax({
			// 		type : 'POST',
			// 		url  : '/check/odp/unique',
			// 		data : {data :order,
			// 			"_token": "{{ csrf_token() }}",
			// 		},
			// 		success : function(data){
			// 			if(isEmpty(data) === false){
			// 				$("#hasil").html(order);
			// 				$('.twice_odp').css({
			// 					display : 'block'
			// 				});
			// 				$('#nama_odp').css({border: "2px solid red"});
			// 				$('#nama_odp').focus();
			// 			}else{
			// 				$("#hasil").html('');
			// 				$('#nama_odp').css({border: ""});
			// 				$('.twice_odp').css({
			// 					display : 'none'
			// 				});
			// 			}
			// 		}
			// 	});
			// });

			var usedNames = {};

			$("select[name='regu'] > option").each(function () {
				if (usedNames[this.value]) {
					$(this).remove();
				} else {
					usedNames[this.value] = this.text;
				}
			});

			// $("select[name='odp_po'] > option").each(function () {
			// 	if (usedNames[this.value]) {
			// 		$(this).remove();
			// 	} else {
			// 		usedNames[this.value] = this.text;
			// 	}
			// });

			$("select[name='sto'] > option").each(function () {
				if (usedNames[this.value]) {
					$(this).remove();
				} else {
					usedNames[this.value] = this.text;
				}
			});

			$("#nomor_id").on("keypress keyup",function (e){
				$(this).val($(this).val().replace(/\D/g, ''));
				var charCode = (e.which) ? e.which : e.keyCode;
				if (charCode > 31 && (charCode < 48 || charCode > 57)) {
					return false;
				}
			});

			$('#tgl_kerja').datepicker({
				format: 'yyyy-mm-dd'
			});

			// $('.odp_nama').on('keypress change', function(e){
			// 	var value = $(this).val();
			// 	if (e.which === 13 && value.length > 12 || e.type === 'change') {
			// 		$.ajax({
			// 			type: 'Get',
			// 			url : "/admin/get/odp/"+value,
			// 			success: function(data) {
			// 				/*console.log(data);*/
			// 				$('#serial').val(data.unsc);
			// 				$('#project_nama').val("SC"+data.unsc+"("+data.orderName+")");
			// 				$('#nama_odp').val(data.label_odp);
			// 				$('#No_koor_odp').val(data.koordinat_odp);
			// 				$('#nomor_unsc').val(data.unsc);
			// 				if ($("#sto").find("option[value=" + data.sto_un + "]").length) {
			// 					$("#sto").val(data.sto_un).trigger("change");
			// 				} else {
			// 					var sto_un = new Option(data.sto_un, data.sto_un, true, true);
			// 					$("#sto").append(sto_un).trigger('change');
			// 				}
			// 				if(data.kml != null && data.kml.length > 0){
			// 					if (data.kml.indexOf(".PNG") > -1){
			// 						$('#img-KML').attr('src',"/upload/files/"+data.kml);
			// 					}else{
			// 						$('.downlaod_kml').css({display: "block"});
			// 						$('.downlaod_kml').attr('href',"/upload/files/"+data.kml);
			// 					}
			// 				}

			// 				if(data.mcore != null && data.mcore.length > 0){
			// 					/*console.log(data.mcore);*/
			// 					$('.downlaod_mcore').css({display: "block"});
			// 					$('.downlaod_mcore').attr('href',"/upload/files/"+data.mcore);
			// 				}

			// 			},
			// 			error: function(e){
			// 				/*console.log(e);*/
			// 			}
			// 		});
			// 	}
			// });

			$(":input").inputmask();

			$('input[type=file]').change(function() {
				var phptes = {!! json_encode($photodispatch) !!};
				/*console.log(phptes[0]);
				console.log(this.name);*/
				var inputEl = this;
				if (inputEl.files && inputEl.files[0]) {
					$(inputEl).parent().find('input[type=text]').val(1);
					var reader = new FileReader();
					reader.onload = function(e) {
						$(inputEl).parent().find('img').attr('src', e.target.result);

					}
					reader.readAsDataURL(inputEl.files[0]);
				}
			});

			$('.input-photos').on('click', 'button', function() {
				$(this).parent().find('input[type=file]').click();
			});

			// $('.listkontak').select2({ width: '100%' });

			$('#sto').select2({
				width: '100%',
				placeholder: 'Silahkan Pilih STO'
			 });

			$('#nama_odc').select2({
				width: '100%',
				placeholder: 'Silahkan Pilih ODC'
			 });

			var rd = {!! json_encode($regu) !!},
			regu_data = [];

			$.each(rd, function(k, v){
				if(v.is_saber == 0){
					regu_data.push({
						'id': v.id_regu,
						'text': v.uraian
					})
				}
			})

			$('#regu').select2({
				width: '100%',
				data: regu_data
			});

			$('#j_order').select2({
				placeholder: 'Masukkan Jenis Orderan!',
				width: '100%'
			});

			$('#j_pekerjaan').select2({
				placeholder: 'Masukkan Jenis Pekerjaan!',
				width: '100%'
			});

			// $('.booking_odp').click(function(){
			// 	var sto = $('#sto').val();
			// 	var odc = $('#nama_odc').val();
			// 	var project = $('#project_nama').val();
			// 	var odp_koor = $('#No_koor_odp').val();

			// 	if(sto && odc && project && odp_koor){
			// 		$('.booking_odp').hide();
			// 		$.ajax({
			// 			type : 'get',
			// 			url  : '/get_booking_odp',
			// 			data : {
			// 				jenis_terminal: 'ODP',
			// 				jenis_pekerjaan: 'PT-2',
			// 				sto: $('#sto').val(),
			// 				odc: $('#nama_odc').val(),
			// 				nama_lop: $('#project_nama').val(),
			// 				koordinat: $('#No_koor_odp').val(),
			// 			},
			// 			success : function(data){
			// 				data = JSON.parse(data)
			// 				$('#nama_odp').val(data.booked.jenis_terminal + '/' + data.booked.sto + '/' + data.booked.odc + '/' + data.booked.index_terminal);
			// 				$('#id_promitos_odp').val(data.booked.id); //status booked jika dipilih
			// 			}
			// 		});
			// 	}else{
			// 		var notif=$.toast({
			// 			position: 'mid-center',
			// 			showHideTransition: 'plain',
			// 			hideAfter: false
			// 		});

			// 		notif.update({
			// 			heading: 'Peringatan',
			// 			text: 'Data Tidak Lengkap!',
			// 			position: 'mid-center',
			// 			icon: 'error',
			// 			showHideTransition: 'plain',
			// 			stack: false
			// 		});
			// 	}
			// })

			$('.checking_odp').click(function(){
				var sto = $('#sto').val();
				var odc = $('#nama_odc').val();

				if(sto && odc){
					$('.booking_odp').hide();
					$.ajax({
						type : 'get',
						url  : '/get_checked_book_odp',
						data : {
							jenis_terminal: 'ALL',
							sto: sto,
							odc: odc,
						},
						success : function(data){
							// console.log(data)
							var table = "<div class='form-group'>";
							table += "<a type='button' class='btn btn-default hapus_pilihan'><i data-icon='&#xe01c;' class='linea-icon linea-basic fa-fw' style='font-size: 20px;'></i>&nbsp;Hapus Pilihan</a></div>"
							table += "<div class='panel panel-success'>";
							table += "<div class='panel-heading'>List Data STO " + sto + " dan ODC " + odc + "</div>";
							table += "<div class='panel-body'>";
							table += "<ul class='nav nav-tabs'>";

							$.each(data, function(k, v){
								table += "<li class='" + (k == 'ODP' ? 'active' : '') + "'>";
								table += "<a href='#menu" + k + "' class='nav-link' role='tab' data-toggle='tab'>" + k + "</a>";
								table += "</li>";
							});

							table += "</ul>";
							table += "<div class='tab-content'>";
							var no = 0;

							$.each(data, function(k, v){
								table += "<div id='menu" + k + "' class='tab-pane fade'>";
								table += "<div class='page-wrap'>";
								table += "<div class='table-responsive'>";
								table += "<table class='table table_booked table-bordered'>";
								table += "<thead>";
								table += "<tr>";
								table += "<th>Nomor</th>";
								table += "<th>Nama "+ k + "</th>";
								table += "<th>Jenis Project</th>";
								table += "<th>Koordinat</th>";
								table += "<th>Tanggal Order</th>";
								table += "<th>Status</th>";
								table += "</tr>";
								table += "</thead>";
								table += "<tbody>";

								if(v !== null){
									$.each(v.data, function(kk, vv){
										table += "<tr>"
										table += "<td>" + ++no + "</td>"
										table += "<td>" + vv.jenis_terminal + '-' + vv.sto + '-' + vv.odc + '/' + vv.index_terminal + "</td>"
										table += "<td>" + vv.jenis_project + "</td>"
										table += "<td>" + vv.koordinat_kml + "</td>"
										table += "<td>" + vv.tgl_order + "</td>"
										table += "<td>" + (vv.status == 'Free' ? "<input type='radio' id='"+vv.id+"' data-regu='"+vv.id_regu+"' data-odp_aspl='"+ vv.jenis_terminal + '-' + vv.sto + '-' + vv.odc + '/' + vv.index_terminal +"' data-id_lop='"+vv.id+"' name='select_free'><label for='"+vv.id+"'>"+vv.status+"</label><br>" : vv.status) + "</td>"
										table += "</tr>"
									});
								}

								table += "</tbody>";
								table += "</table>";
								table += "</div>";
								table += "</div>";
								table += "</div>";
							});

							table += "</div>";
							table += "</div>";
							table += "</div>";

							$('.modal_avail').html(table);

							$(".hapus_pilihan").click(function(){
								$("input[name='select_free']").prop('checked',false);
								$('.check_booking_result').hide();
								$("input[name='nama_odp_selected_id']").val();
							});

							$("input[name='select_free']").click(function(){
								if($(this).is(':checked')){
									var regu = $(this).attr('data-regu'),
									id_lop = $(this).attr('data-id_lop'),
									odp_aspl = $(this).attr('data-odp_aspl');

									$.ajax({
										type : 'get',
										url  : '/regu_id/ajax/search',
										data : {
											regu: regu,
										},
										success : function(data){
											$('.check_booking_result').show();
											$('#nama_odp_selected').val(odp_aspl)
											$('#nama_tim').val(data.uraian ?? 'Tidak Ada Team')

											$("input[name='nama_odp_selected_id']").val(id_lop);
											$("input[name='nama_tim_id']").val(regu);
										}
									});
								}
							});

							$('#view_promitos').modal('toggle');

							$('#view_promitos').on('shown.bs.modal', function (e) {

							})
							$(".nav-tabs a").click(function(){
								$(this).tab('show');
							});

							$('#menuODP').toggleClass("in active");

							$('.table_booked').DataTable( {
								drawCallback: function () {
											$( 'table.import_list tbody tr td' ).css( 'padding', '5px 8px 5px 8px' );
									},
								fixedHeader: {
									header: true,
									footer: true
								},
								order:	[
									[ 0, "asc" ]
								],
								deferRender: true,
								scrollCollapse: true,
								scroller: true,
							}).columns.adjust().draw();
						}
					});
				}else{
					var notif=$.toast({
						position: 'mid-center',
						showHideTransition: 'plain',
						hideAfter: false
					});

					notif.update({
						heading: 'Peringatan',
						text: 'Data Tidak Lengkap! Silahkan Isi STO dan ODC!',
						position: 'mid-center',
						icon: 'error',
						showHideTransition: 'plain',
						stack: false
					});
				}
			})

			// $('.booking_aspl').click(function(){
			// 	var sto = $('#sto').val();
			// 	var odc = $('#nama_odc').val();
			// 	var project = $('#project_nama').val();
			// 	var odp_koor = $('#No_koor_odp').val();

			// 	if(sto && odc && project && odp_koor){
			// 		$('.booking_aspl').hide();
			// 		$.ajax({
			// 			type : 'get',
			// 			url  : '/get_booking_odp',
			// 			data : {
			// 				jenis_terminal: 'ASPL',
			// 				jenis_pekerjaan: 'PT-2',
			// 				sto: $('#sto').val(),
			// 				odc: $('#nama_odc').val(),
			// 				nama_lop: $('#project_nama').val(),
			// 				koordinat: $('#No_koor_odp').val(),
			// 			},
			// 			success : function(data){
			// 				data = JSON.parse(data)
			// 				$('#nama_odp').val(data.booked.jenis_terminal + '/' + data.booked.sto + '/' + data.booked.odc + '/' + data.booked.index_terminal);
			// 				$('#id_promitos_odp').val(data.booked.id); //status booked jika dipilih
			// 			}
			// 		});
			// 	}else{
			// 		var notif=$.toast({
			// 			position: 'mid-center',
			// 			showHideTransition: 'plain',
			// 			hideAfter: false
			// 		});

			// 		notif.update({
			// 			heading: 'Peringatan',
			// 			text: 'Data Tidak Lengkap!',
			// 			position: 'mid-center',
			// 			icon: 'error',
			// 			showHideTransition: 'plain',
			// 			stack: false
			// 		});
			// 	}
			// })

			data_saved = {!! json_encode($data_d) !!};

			if(typeof(data_saved.id_hero) != 'undefined' && data_saved.id_hero != 0){
				var sw = '';
				switch (data_saved.flagging) {
					case 'pt2_plus':
						sw = 'PT-2 PLUS';
					break;
					default:
						sw = 'PT-2 SIMPLE';
					break;
				}

				$('#j_pekerjaan').val(sw).change();
				$('#catatan').val(data_saved.keterangan_sdi)

				$('#j_order').select2({
					disabled: true
				});
			}

			$('#saber, #regu_tipe').on('switchChange.bootstrapSwitch', function (event, state) {
				var regu_data_contain = [],
				jenis_regu = ($('#regu_tipe').is(":checked") == true ? 1 : 0),
				jenis_saber = ($('#saber').is(":checked") == true ? 1 : 0);
				$("#regu").empty().trigger('change')

				$.each(rd, function(k, v){
					if(v.is_saber == jenis_saber && v.is_OSOM == jenis_regu){
						regu_data_contain.push({
							'id': v.id_regu,
							'text': v.uraian
						})
					}
				})

				$('#regu').select2({
					width: '100%',
					data: regu_data_contain
				});
			})

			$('#tipe_jointer').on('switchChange.bootstrapSwitch', function (event, state) {
				var regu_data_contain = [],
				jenis_regu = ($('#tipe_jointer').is(":checked") == true ? 1 : 0);
				console.log(jenis_regu)
				$("#regu").empty().trigger('change')

				if(jenis_regu == 1){
					$('.regu_field').hide();
					$('#regu').removeAttr('required');
				}else{
					$('.regu_field').show();
					$('#regu').attr('required',true);

					$.ajax({
						type : 'get',
						url  : '/jointer/ajax/search',
						success : function(data){
							var jointer_contain = [];
							$.each(data, function(k, v){
								jointer_contain.push({
									'id': v.id_regu,
									'text': `${v.nama1} (${v.nik1})`
								})
							});
							console.log(jointer_contain)
							$('#regu').select2({
								width: '100%',
								data: jointer_contain
							});
						}
					});
				}
			})

			$('#j_pekerjaan option').each(function() {
				$("#regu").empty().trigger('change')

				if($(this).is(':selected')){
					$('.regu_field').show();

					var regu_data_contain = [],
					jenis_regu = ($('#regu_tipe').is(":checked") == true ? 1 : 0),
					jenis_saber = ($('#saber').is(":checked") == true ? 1 : 0);

					$.each(rd, function(k, v){
						if(v.is_saber == jenis_saber && v.is_OSOM == jenis_regu){
							regu_data_contain.push({
								'id': v.id_regu,
								'text': v.uraian
							})
						}
					})

					$('#regu').select2({
						width: '100%',
						data: regu_data_contain
					});

					$('.non_gamas').show();
				}
			});

			$('#j_order option').each(function() {
				if($(this).is(':selected')){
					// console.log($(this).val())
					switch ($(this).val()) {
						case 'UNSC':
							$('.nomor_urgent').css({'display': 'block'});
							$('#nomor_id').attr('required',true);
						break;
						case '6':
							// $('.group_unique').css({'display': 'block'});
							$('#conv_text').val();
							// $('#conv_text').tooltip('dispose').tooltip(
							// 	{
							// 		'template': '<div class="tooltip" role="tooltip"><div class="arrow"></div> <div class="tooltip-inner TI_large"></div></div>',
							// 		'trigger':'focus',
							// 		'html' : true,
							// 		'title': "<div style='max-width:500px; position:relative; overflow:auto;'> <div style='width:500px; background:#F2F2F2; line-height:30px;-webkit-border-radius: 3px; -moz-border-radius: 3px; border-radius: 3px; text-align:left; padding-left:15px;'><strong>Contoh Format Data KPRO</strong></div><div style='height:30px; line-height:30px; text-align:left; padding-left:15px;'>511787159 <span style='color: red;'>*No SC</span></div><div style='height:30px; line-height:30px; text-align:left; padding-left:15px;'>ODP -3.24907684087, 116.215850943	(Untitled Placemark)</div><div style='height:30px; line-height:30px; text-align:left; padding-left:15px;'>ODC -3.257997, 116.207607 (ODC-KPL-FZ) (ODC-KPL-FZ)</div><div style='height:30px; line-height:30px; text-align:left; padding-left:15px;'>PELANGGAN	-3.24877114790893,116.2160183954896</div><div style='height:30px; line-height:30px; text-align:left; padding-left:15px;'>ODC-KPL-FZ</div><div style='height:30px; line-height:30px; text-align:left; padding-left:15px;'>DIST	4</div><div style='height:30px; line-height:30px; text-align:left; padding-left:15px;'>ODP-KPL-FZ/097</div><div style='width:500px; background:#F2F2F2; line-height:30px; float:left;'>Ini Hanya Contoh!</div></div>"
							// 	}
							// ).tooltip('show');
							$('.nomor_urgent').css({'display': 'block'});
							$('#nomor_id').attr('required',true);
							$('.kpro').css({'display': 'block'});
							$('.kpro > input').attr('required', true);
						break;
						case '7':
							// $('.group_unique').css({'display': 'block'});
							$('.normalisasis').css({'display': 'block'});
							$('.normalisasis > input').attr('required', true);
							$('.normalisasis ').css({'display': 'block'});
							$('#conv_text').val();
							// $('#conv_text').tooltip('dispose').tooltip(
							// 	{
							// 		'template': '<div class="tooltip" role="tooltip"><div class="arrow"></div><div class="tooltip-inner TI_large"></div></div>',
							// 		'trigger': 'focus',
							// 		'html': true,
							// 		'title': "<div style='max-width:500px; position:relative; overflow:auto;'><div style='width:500px; background:#F2F2F2; line-height:30px; a-webkit-border-radius: 3px;-moz-border-radius: 3px; border-radius: 3px; text-align:left; padding-left:15px;'>	<strong>Contoh Format Data Normalisasi</strong></div><div style='height:30px; line-height:30px; text-align:left; padding-left:15px;'>ODP-BJM-FBB/002</div><div style='height:30px; line-height:30px; text-align:left; padding-left:15px;'>ODP LAMA	-3.34057920654,114.580732351</div><div style='height:30px; line-height:30px; text-align:left; padding-left:15px;'>ODP BARU -3.34040212589,	114.580396164</div><div style='height:30px; line-height:30px; text-align:left; padding-left:15px;'>ODC -3.33558264329, 114.577204043	(ODC-BJM-FBB)</div><div style='height:30px; line-height:30px; text-align:left; padding-left:15px;'>ODC BJM FBB</div><div style='height:30px; line-height:30px; text-align:left; padding-left:15px;'>DIST 1</div><div style='height:30px; line-height:30px; text-align:left; padding-left:15px;'>ODP-BJM-FBB/091</div></div>"
							// 	}
							// ).tooltip('show');
						break;
						default:
							$('.nomor_urgent').css({'display': 'none'});
							// $('.group_unique').css({'display': 'none'});
							$('#nomor_id').val('');
							$('#nomor_id').removeAttr('required');
							$('.normalisasis > input').removeAttr('required');
						break;
					}
				}
			});

			$('#conv_text').tooltip({'title': ''});
			$('.normalisasis').css({'display': 'none'});

			$('#sto, #nama_odc').on('change', function(){
				var sto = $('#sto').val() ?? 'Kosong';
				odc = $('#nama_odc').val() ?? 'Kosong';

				$("#project_nama").val('PT2 ' + sto + '-' + odc + '')
			});

			$('#j_pekerjaan').on('change', function(){
				$("#regu").empty().trigger('change')
				$('.regu_field').show();

				var regu_data_contain = [],
				jenis_regu = ($('#regu_tipe').is(":checked") == true ? 1 : 0),
				jenis_saber = ($('#saber').is(":checked") == true ? 1 : 0);

				$.each(rd, function(k, v){
					if(v.is_saber == jenis_saber && v.is_OSOM == jenis_regu){
						regu_data_contain.push({
							'id': v.id_regu,
							'text': v.uraian
						})
					}
				})

				$('#regu').select2({
					width: '100%',
					data: regu_data_contain
				});

				$('.non_gamas').show();
			});


			$('#j_order').on('change', function(){
				switch ($(this).val()) {
					case 'UNSC':
						$('.nomor_urgent').css({'display': 'block'});
						$('#nomor_id').attr('required',true);
					break;
					case '6':
						// $('.group_unique').css({'display': 'block'});
						$('#conv_text').val();
						// $('#conv_text').tooltip('dispose').tooltip(
						// 	{
						// 		'template': '<div class="tooltip" role="tooltip"><div class="arrow"></div> <div class="tooltip-inner TI_large"></div></div>',
						// 		'trigger':'focus',
						// 		'html' : true,
						// 		'title': "<div style='max-width:500px; position:relative; overflow:auto;'> <div style='width:500px; background:#F2F2F2; line-height:30px;-webkit-border-radius: 3px; -moz-border-radius: 3px; border-radius: 3px; text-align:left; padding-left:15px;'><strong>Contoh Format Data KPRO</strong></div><div style='height:30px; line-height:30px; text-align:left; padding-left:15px;'>511787159 <span style='color: red;'>*No SC</span></div><div style='height:30px; line-height:30px; text-align:left; padding-left:15px;'>ODP -3.24907684087, 116.215850943	(Untitled Placemark)</div><div style='height:30px; line-height:30px; text-align:left; padding-left:15px;'>ODC -3.257997, 116.207607 (ODC-KPL-FZ) (ODC-KPL-FZ)</div><div style='height:30px; line-height:30px; text-align:left; padding-left:15px;'>PELANGGAN	-3.24877114790893,116.2160183954896</div><div style='height:30px; line-height:30px; text-align:left; padding-left:15px;'>ODC-KPL-FZ</div><div style='height:30px; line-height:30px; text-align:left; padding-left:15px;'>DIST	4</div><div style='height:30px; line-height:30px; text-align:left; padding-left:15px;'>ODP-KPL-FZ/097</div><div style='width:500px; background:#F2F2F2; line-height:30px; float:left;'>Ini Hanya Contoh!</div></div>"
						// 	}
						// ).tooltip('show');
						$('.nomor_urgent').css({'display': 'block'});
						$('#nomor_id').attr('required',true);
						$('.kpro').css({'display': 'block'});
						$('.kpro > input').attr('required', true);
					break;
					case '7':
						// $('.group_unique').css({'display': 'block'});
						$('.normalisasis').css({'display': 'block'});
						$('.normalisasis > input').attr('required', true);
						$('.normalisasis ').css({'display': 'block'});
						$('#conv_text').val();
						// $('#conv_text').tooltip('dispose').tooltip(
						// 	{
						// 		'template': '<div class="tooltip" role="tooltip"><div class="arrow"></div><div class="tooltip-inner TI_large"></div></div>',
						// 		'trigger': 'focus',
						// 		'html': true,
						// 		'title': "<div style='max-width:500px; position:relative; overflow:auto;'><div style='width:500px; background:#F2F2F2; line-height:30px; a-webkit-border-radius: 3px;-moz-border-radius: 3px; border-radius: 3px; text-align:left; padding-left:15px;'>	<strong>Contoh Format Data Normalisasi</strong></div><div style='height:30px; line-height:30px; text-align:left; padding-left:15px;'>ODP-BJM-FBB/002</div><div style='height:30px; line-height:30px; text-align:left; padding-left:15px;'>ODP LAMA	-3.34057920654,114.580732351</div><div style='height:30px; line-height:30px; text-align:left; padding-left:15px;'>ODP BARU -3.34040212589,	114.580396164</div><div style='height:30px; line-height:30px; text-align:left; padding-left:15px;'>ODC -3.33558264329, 114.577204043	(ODC-BJM-FBB)</div><div style='height:30px; line-height:30px; text-align:left; padding-left:15px;'>ODC BJM FBB</div><div style='height:30px; line-height:30px; text-align:left; padding-left:15px;'>DIST 1</div><div style='height:30px; line-height:30px; text-align:left; padding-left:15px;'>ODP-BJM-FBB/091</div></div>"
						// 	}
						// ).tooltip('show');
					break;
					default:
						$('.nomor_urgent').css({'display': 'none'});
						// $('.group_unique').css({'display': 'none'});
						$('#nomor_id').val('');
						$('#nomor_id').removeAttr('required');
						$('.normalisasis > input').removeAttr('required');
					break;
				}
			});

			// $('#encypt_lol').on('click', function(e){
			// 	var pecah = $('#conv_text').val().replace(/^\s*[\r\n]/gm, '').replace(/ +(?= )/g,'').split(/\r?\n/);
			// 	console.log(pecah[1].split(" "))
			// 	switch ($('#j_order').val()){
			// 		case '6':
			// 			var odp_koor= pecah[1].split(","),
			// 			odc_koor= pecah[2].split(","),
			// 			pelanggan = pecah[3].split(","),
			// 			sto = pecah[6].split('-');
			// 			console.log(sto)
			// 			$('#nomor_id').val(pecah[0])
			// 			$('#No_koor_pelanggan').val(pecah[3].match(/[-+]?([1-8]?\d(\.\d+)?|90(\.0+)?)[,°]\s*[-+]?(180(\.0+)?|((1[0-7]\d)|([1-9]?\d))(\.\d+)?°?)/g));
			// 			$('#project_nama').val(pecah[0]);
			// 			$('#No_koor_odp').val(pecah[1].match(/[-+]?([1-8]?\d(\.\d+)?|90(\.0+)?)[,°]\s*[-+]?(180(\.0+)?|((1[0-7]\d)|([1-9]?\d))(\.\d+)?°?)/g));
			// 			$('#No_koor_odc').val(pecah[2].match(/[-+]?([1-8]?\d(\.\d+)?|90(\.0+)?)[,°]\s*[-+]?(180(\.0+)?|((1[0-7]\d)|([1-9]?\d))(\.\d+)?°?)/g));
			// 			$('#nama_odp').val(pecah[6]);
			// 			$('#sto').val(sto[1]).trigger('change');
			// 			$('#catatan').val(pecah[5]);
			// 		break;
			// 		case '7':
			// 			var sto = pecah[4].split(' ');
			// 			$('#No_koor_odp_before').val(pecah[1].match(/[-+]?([1-8]?\d(\.\d+)?|90(\.0+)?)[,°]\s*[-+]?(180(\.0+)?|((1[0-7]\d)|([1-9]?\d))(\.\d+)?°?)/g));
			// 			$('#No_koor_odp').val(pecah[2].match(/[-+]?([1-8]?\d(\.\d+)?|90(\.0+)?)[,°]\s*[-+]?(180(\.0+)?|((1[0-7]\d)|([1-9]?\d))(\.\d+)?°?)/g));
			// 			$('#No_koor_odc').val(pecah[3].match(/[-+]?([1-8]?\d(\.\d+)?|90(\.0+)?)[,°]\s*[-+]?(180(\.0+)?|((1[0-7]\d)|([1-9]?\d))(\.\d+)?°?)/g));
			// 			$('#nama_odp').val(pecah[6]);
			// 			$('#nama_odp_before').val(pecah[0]);
			// 			$('#sto').val(sto[1]).trigger('change');
			// 			$('#catatan').val(pecah[5]);
			// 		break;
			// 	}
			// });
			if($.inArray(window.location.href.split('/')[5], ['PSB', 'KPRO']) !== -1 ){
				$('#j_order').val(6).trigger('change');
				$('#j_order').attr('disabled','disabled');
				$('#nomor_id').val(data_saved.orderId);
				$('#nomor_id').attr('readonly', true)
			}

			$('.save_me').on('click', function(e){
				e.preventDefault()

				$('#pt2plusval').val($('#pt2plus').val());
				var project_nama = $.trim($('#project_nama').val()),
				No_koor_odp = $.trim($('#No_koor_odp').val()),
				No_koor_odc = $.trim($('#No_koor_odc').val()),
				sto = $.trim($('#sto').val()),
				nama_odc = $.trim($('#nama_odc').val()),
				No_koor_aspl = $.trim($('#No_koor_aspl').val()),
				photo = $("#img-KML").attr('src'),
				check_odp,
				regex =/^[-+]?([1-8]?\d(\.\d+)?|90(\.0+)?)[,°]\s*[-+]?(180(\.0+)?|((1[0-7]\d)|([1-9]?\d))(\.\d+)?°?)$/;
				// datastring = $("#formbuatG").serialize();
				// var order = $('#nama_odp').val();
				// $.ajax({
				// 	type : 'POST',
				// 	url  : '/check/odp/unique',
				// 	data : {
				// data :order,
				// 		"_token": "{{ csrf_token() }}",
				// 	},
				// 	success : function(data){
				// 		if(isEmpty(data) === false){
				// 			check_odp = 'aman bos';
				// 		}else{
				// 			check_odp = 'santai cu';

				// 		}
				// 	}
				// });

				if (project_nama === '') {
					$('.project_nama').css({display: "block"});
					$('#project_nama').css({border: "2px solid red"});
					$('#project_nama').focus();
				}else{
					$('.project_nama').css({display: "none"});
					$('#project_nama').css({border: ""});
				}

				if ((photo) == '/images/placeholder.gif'){
					$.toast({
						heading: 'Foto KML Kosong!',
						text: 'Silahkan Masukkan Foto KML!',
						position: 'top-right',
						stack: false,
						icon: 'error',
						hideAfter: 10000,
						showHideTransition: 'slide'
					})
				}

				if($('#booking_aspl').prop('checked') ){
					if(!regex.test(No_koor_aspl) || No_koor_aspl === ''){
						$('.validate_no_koor_aspl').css({display: "block"});
						$('#No_koor_aspl').css({border: "2px solid red"});
						$('#No_koor_aspl').focus();
						return false;
					}else{
						$('.validate_no_koor_aspl').css({display: "none"});
						$('#No_koor_aspl').css({border: ""});
					}
				}

				// if (nama_odp === '') {
				// 	$('.nama_odp').css({display: "block"});
				// 	$('#nama_odp').css({border: "2px solid red"});
				// 	$('#nama_odp').focus();
				// }else{
				// 	$('.nama_odp').css({display: "none"});
				// 	$('#nama_odp').css({border: ""});
				// }

				if(!regex.test(No_koor_odp) && No_koor_odp !== ''){
					$('.validate_no_koor_odp').css({display: "block"});
					$('#No_koor_odp').css({border: "2px solid red"});
					$('#No_koor_odp').focus();
				}else{
					$('.validate_no_koor_odp').css({display: "none"});
					$('#No_koor_odp').css({border: ""})
				}

				if(!regex.test(No_koor_odc) && No_koor_odc !== ''){
					$('.validate_no_koor_odc').css({display: "block"});
					$('#No_koor_odc').css({border: "2px solid red"});
					$('#No_koor_odc').focus();
				}else{
					$('.validate_no_koor_odc').css({display: "none"});
					$('#No_koor_odc').css({border: ""});
				}

				if (No_koor_odp === '') {
					$('.no_koor_odp').css({display: "block"});
					$('#No_koor_odp').css({border: "2px solid red"});
					$('#No_koor_odp').focus();
				}else{
					$('.no_koor_odp').css({display: "none"});
					$('#No_koor_odp').css({border: ""});
				}

				if (No_koor_odc === '') {
					$('.no_koor_odc').css({display: "block"});
					$('#No_koor_odc').css({border: "2px solid red"});
					$('#No_koor_odc').focus();
				}else{
					$('.no_koor_odc').css({display: "none"});
					$('#No_koor_odc').css({border: ""});
				}
				if (project_nama === '' || sto === '' || nama_odc === '' || No_koor_odp === '' || !regex.test(No_koor_odp) || ((photo) == '/images/placeholder.gif' || check_odp == 'aman bos')){
					return false;
				}else{
					// var notif=$.toast({
					// 	position: 'mid-center',
					// 	showHideTransition: 'plain',
					// 	hideAfter: false
					// });
					// notif.update({
					// 	heading: 'Pemberitahuan',
					// 	text: 'Order dengan ODP Telah Berhasil Ditambahkan!',
					// 	position: 'mid-center',
					// 	icon: 'success',
					// 	showHideTransition: 'plain',
					// 	stack: false
					// });

					var sto = $('#sto').val();
					var odc = $('#nama_odc').val();


					if(sto && odc){
						$('.booking_odp').hide();
						$.ajax({
							type : 'get',
							url  : '/get_checked_book_odp',
							data : {
								jenis_terminal: 'ALL',
								sto: sto,
								odc: odc,
							},
							success : function(data){
								var result_data_free = [];

								if($('#nama_odp_selected').val().length != 0){
									result_data_free.push($('#nama_odp_selected').val() );
								}else{
									if(data.ODP.data.length != 0){
										data.ODP.data.sort(function(a,b)
										{
											return a.index_terminal - b.index_terminal;
										});

										find_free = $.grep(data.ODP.data, function(e){ return e.status == 'Free'; });

										if(find_free.length != 0)
										{
											result_data_free.push(`ODP-${find_free[0].sto}-${find_free[0].odc}/${find_free[0].index_terminal}`);
										}
										else
										{
											var last_call = data.ODP.data.slice(-1)[0];
											result_data_free.push(`ODP-${last_call.sto}-${last_call.odc}/${('000' + (parseInt(last_call.index_terminal) + 1)).slice(-3)}`);
										}
									}
									else
									{
										result_data_free.push(`ODP-${sto}-${odc}/001`);
									}
								}

								$('#booking_aspl').on('switchChange.bootstrapSwitch', function (event, state) {
									if($(this).is(':checked') ){
										if(data.ASPL.data.length != 0){
											data.ASPL.data.sort(function(a,b)
											{
												return a.index_terminal - b.index_terminal;
											});

											find_free = $.grep(data.ASPL.data, function(e){ return e.status == 'Free'; });

											if(find_free.length != 0)
											{
												result_data_free.push(`ASPL-${find_free[0].sto}-${find_free[0].odc}/${find_free[0].index_terminal}`);
											}
											else
											{
												var last_call = data.ODP.data.slice(-1)[0];
												result_data_free.push(`ASPL-${last_call.sto}-${last_call.odc}/${('000' + (parseInt(last_call.index_terminal) + 1)).slice(-3)}`);
											}
										}
										else
										{
											result_data_free.push(`ASPL-${sto}-${odc}/001`);
										}
									}
								});

								if (!$.isEmptyObject(data_saved) && $('#change_odp').prop('checked') || $.isEmptyObject(data_saved) ){
									Swal.fire({
										title: 'Konfirmasi Ulang?',
										text: `${result_data_free.join(' dan ')} Akan Di Order, Apakah Anda Yakin?`,
										type: 'warning',
										showCancelButton: true,
										confirmButtonColor: '#3085d6',
										cancelButtonColor: '#d33',
										confirmButtonText: 'Ya, Submit!',
										timerProgressBar: true,
									}).then((result) => {
										if (result.isConfirmed) {
											$('#formbuatG').submit();
										}
									});
								}else{
									$('#formbuatG').submit();
								}
							}
						});
					}else{
						var notif=$.toast({
							position: 'mid-center',
							showHideTransition: 'plain',
							hideAfter: false
						});

						notif.update({
							heading: 'Peringatan',
							text: 'Data Tidak Lengkap! Silahkan Isi STO dan ODC!',
							position: 'mid-center',
							icon: 'error',
							showHideTransition: 'plain',
							stack: false
						});
					}
				}
			});

			if (!$.isEmptyObject(data_saved) ){
				$('#j_order').val(data_saved.kategory_non_unsc).trigger("change");

				if(data_saved.is_OSOM == 1){
					$('#regu_tipe').bootstrapSwitch('state', true);
				}

				if(data_saved.is_saber == 1){
					$('#saber').click();
				}

				var regu_data_contain = [],
				jenis_regu = ($('#regu_tipe').is(":checked") == true ? 1 : 0),
				jenis_saber = ($('#saber').is(":checked") == true ? 1 : 0);
				$("#regu").empty().trigger('change')

				$.each(rd, function(k, v){
					if(v.is_saber == jenis_saber && v.is_OSOM == jenis_regu){
						regu_data_contain.push({
							'id': v.id_regu,
							'text': v.uraian
						})
					}
				})

				$('#regu').select2({
					width: '100%',
					data: regu_data_contain
				});
				console.log(regu_data_contain)

				if( (data_saved.aspl_nama?.length || 0) != 0){
					$('#booking_aspl').bootstrapSwitch('state', true);
				}else{
					$('#booking_aspl').bootstrapSwitch('state', false);
				}

				// $('#regu').val(data_saved.regu_id).trigger("change");
			}
			else
			{
				$('#sto, #nama_odc').val(null).change();
			}

			// $('#sto').val('TJL').change();
			// $('#nama_odc').val('FS').change()
		});
	</script>
	@endsection