@extends('layout')
@section('title', 'List Order Overspec')
@section('style')
<style type="text/css">
	th, td {
		text-align: center;
		white-space:nowrap;
	}
</style>
@endsection
@section('headerS')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/css/dataTables.bootstrap4.css">
@endsection
@section('content')
<div class="container-fluid" style="padding-top: 25px;">
	<div class="panel panel-warning">
		<div class="panel-heading">List Survey ODP Overspec</div>
		<div class="panel-body">
				<div class="table-responsive">
					<table id="groupPT2" class="table table-hover table-bordered">
						<thead>
							<tr>
								<th>No</th>
								<th>Nama ODP</th>
								<th>STO</th>
								<th>Koordinat</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							@foreach($data as $no => $dat)
							<tr>
								<td>{{ ++$no }}</td>
								<td>{{ $dat->odp_nama }}</td>
								<td>{{ $dat->sto }}</td>
								<td>{{ $dat->odp_koor }}</td>
								<td>
									@if (in_array(Session::get('auth')->pt2_level, [2, 5]))
										<a type="button" class="btn btn-light" href='/outside/add/pt2spl/{{$dat->id}}'><span data-icon="&#xe020;" class="linea-icon linea-basic fa-fw" style="font-size: 17px; top: 5px;"></span>&nbsp;Tambah</a>
									@endif
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
		</div>
	</div>
</div>
@endsection
@section('footerS')
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
	$(document).ready( function () {
		$('#groupPT2').DataTable({
			"ordering": false,
			"searching": false,
			"bLengthChange": false,
			"iDisplayLength": 20,
		});
		$('.dataTables_filter input').addClass('yourclass');
	} );
</script>
@endsection