@extends('layout')
<?php $title = strtoupper(str_replace('_', ' ', Request::segment(2))); ?>
@section('title', $title)
@section('style')
<style type="text/css">
    th{
        text-align: center;
        white-space:nowrap;
        vertical-align: middle;
    }

    hr {
        display: block;
        margin-top: 1em;
        margin-bottom: 1em;
        margin-left: auto;
        margin-right: auto;
        border-style: inset;
        border-width: 1px;
    }
    .tooltip-inner {
        min-width: 310px; /* the minimum width */
    }
    thead {
        font-size: 0.75em;
    }
    .btn-light {
        font-size: 1em;
    }
    .structure {
        font-size: 0.85em;
    }
</style>
@endsection
@section('content')
<div class="container-fluid" style="padding-top: 25px;">
    @if (in_array(Session::get('auth')->pt2_level, [2, 5]))
        <div class="form-group">
            <a type="button" class="btn btn-default" href="{{URL::to('/admin/add/non_un/input')}}"><span data-icon="D" class="linea-icon linea-ecommerce fa-fw" style="font-size: 20px; vertical-align:middle;" id="contactG"></span>&nbsp;Tambah Order Manual</a>
    @endif
    </div>
    <div id="body" style="padding-top: 2px;">
        <div class="panel panel-warning">
            <div class="panel-heading">TABLE PT2 WO {{ str_replace('_', ' ', Request::segment(2)) }}</div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table id="teknisi" class="table table-bordered">
                        <thead>
                            <tr>
                                <th rowspan="3" class="align-middle">Datel</th>
                                <th rowspan="3" class="align-middle">STO</th>
                                <th colspan="20" class="align-middle">{{ strtoupper(str_replace('_', ' ', Request::segment(2))) }}</th>
                            </tr>
                            <tr>
                                <th style="vertical-align: middle;" rowspan="2" >Sisa Order</th>
                                @if(Request::segment(2) == 'unsc_psb')
                                <th style="vertical-align: middle;" rowspan="2" >Available WO</th>
                                @endif
                                <th style="vertical-align: middle;" rowspan="2" >Need Progress</th>
                                <th style="vertical-align: middle;" rowspan="2" >OGP</th>
                                <th style="vertical-align: middle;" rowspan="2" >Pending</th>
                                <th style="vertical-align: middle;" rowspan="2" >Berangkat</th>
                                @if(Request::segment(2) == 'unsc_psb')
                                <th style="vertical-align: middle;" rowspan="2" >Bukan PT-2</th>
                                @endif
                                <th colspan="9" >Kendala</th>
                                <th colspan="2" >Selesai</th>
                            </tr>
                            <tr>
                                <th>Distribusi Full</th>
                                <th>Feeder Reti</th>
                                <th>Core Ccd</th>
                                <th>Tercover Odp Lain</th>
                                <th>Kendala Jalur Tidak Ada Tiang Kosong</th>
                                <th>Kendala Cuaca Hujan</th>
                                <th>Cancel</th>
                                <th>Feeder Full</th>
                                <th>Olt Full</th>
                                <th>Go-Live</th>
                                <th>No Go-Live</th>
                            </tr>
                        </thead>
                        <tbody class="structure">
                            <?php
                            $sumSisa_Order = $sumWO_tersedia = $sumnot_pt2 = $sumNeed_Progres = $sumOgp = $sumPending = $sumBerangkat = $sumDF = $sumFT = $sumCC = $sumTOL = $sumKK = $sumK = $sumKC = $sumC = $sumFF = $sumOF = $sumSelesai_golive = $sumselesai_blum_golive =0;
                            ?>
                            @foreach($data as $number => $d)
                            @php $first = true @endphp
                            @foreach($d as $no => $db )
                            <tr>
                                @if($first == true)
                                <td rowspan="{{ count($d) }}" class="align-middle"><a href="/listsh/{{ Request::segment(2) }}/{{ $number }}/all/all/{{ Request::segment(4) }}/{{ Request::segment(5) }}">{{$number}}</a></td>
                                @php $first = false @endphp
                                @endif
                                <?php
                                $sumSisa_Order          += $db->Sisa_Order;
                                @$sumWO_tersedia        += $db->WO_tersedia;
                                @$sumnot_pt2            += $db->not_pt2;
                                $sumNeed_Progres        += $db->Need_Progres;
                                $sumOgp                 += $db->Ogp;
                                $sumPending             += $db->Pending;
                                $sumBerangkat           += $db->Berangkat;
                                $sumK                   += $db->Kendala;
                                $sumDF                  += $db->DF;
                                $sumFT                  += $db->FT;
                                $sumCC                  += $db->CC;
                                $sumTOL                 += $db->TOL;
                                $sumKK                  += $db->KK;
                                $sumKC                  += $db->KC;
                                $sumC                   += $db->C;
                                $sumFF                  += $db->FF;
                                $sumOF                  += $db->OF;
                                $sumSelesai_golive      += $db->selesai_golive;
                                $sumselesai_blum_golive += $db->selesai_blum_golive;
                                ?>
                                <td>
                                    <a href="/listsh/{{ Request::segment(2) }}/{{ $db->datel }}/all/{{ $db->sto }}/{{ Request::segment(4) }}/{{ Request::segment(5) }}">{{ $db->sto }}</a>
                                </td>
                                <td>
                                    @if($db->Sisa_Order != 0)
                                    <a href="/listsh/{{ Request::segment(2) }}/{{ $db->datel }}/Sisa_Order/{{ $db->sto }}/{{ Request::segment(4) }}/{{ Request::segment(5) }}">{{ $db->Sisa_Order }}</a>
                                    @else
                                    -
                                    @endif
                                </td>
                                @if(Request::segment(2) == 'unsc_psb')
                                <td>
                                    @if($db->WO_tersedia != 0)
                                    <a href="/listsh/{{ Request::segment(2) }}/{{ $db->datel }}/WO_tersedia/{{ $db->sto }}/{{ Request::segment(4) }}/{{ Request::segment(5) }}">{{ $db->WO_tersedia }}</a>
                                    @else
                                    -
                                    @endif
                                </td>
                                @endif
                                <td>
                                    @if($db->Need_Progres != 0)
                                    <a href="/listsh/{{ Request::segment(2) }}/{{ $db->datel }}/Need_Progres/{{ $db->sto }}/{{ Request::segment(4) }}/{{ Request::segment(5) }}">{{ $db->Need_Progres }}</a>
                                    @else
                                    -
                                    @endif
                                </td>
                                <td>
                                    @if($db->Ogp != 0)
                                    <a href="/listsh/{{ Request::segment(2) }}/{{ $db->datel }}/Ogp/{{ $db->sto }}/{{ Request::segment(4) }}/{{ Request::segment(5) }}">{{ $db->Ogp }}</a>
                                    @else
                                    -
                                    @endif
                                </td>
                                <td>
                                    @if($db->Pending != 0)
                                    <a href="/listsh/{{ Request::segment(2) }}/{{ $db->datel }}/Ogp/{{ $db->sto }}/{{ Request::segment(4) }}/{{ Request::segment(5) }}">{{ $db->Pending }}</a>
                                    @else
                                    -
                                    @endif
                                </td>
                                <td>
                                    @if($db->Berangkat != 0)
                                    <a href="/listsh/{{ Request::segment(2) }}/{{ $db->datel }}/Berangkat/{{ $db->sto }}/{{ Request::segment(4) }}/{{ Request::segment(5) }}">{{ $db->Berangkat }}</a>
                                    @else
                                    -
                                    @endif
                                </td>
                                @if(Request::segment(2) == 'unsc_psb')
                                <td>
                                    @if($db->not_pt2 != 0)
                                    <a href="/listsh/{{ Request::segment(2) }}/{{ $db->datel }}/not_pt2/{{ $db->sto }}/{{ Request::segment(4) }}/{{ Request::segment(5) }}">{{ $db->not_pt2 }}</a>
                                    @else
                                    -
                                    @endif
                                </td>
                                @endif
                                <td>
                                    @if($db->DF != 0)
                                    <a href="/listsh/{{ Request::segment(2) }}/{{ $db->datel }}/DF/{{ $db->sto }}/{{ Request::segment(4) }}/{{ Request::segment(5) }}">{{ $db->DF }}</a>
                                    @else
                                    -
                                    @endif
                                </td>
                                <td>
                                    @if($db->FT != 0)
                                    <a href="/listsh/{{ Request::segment(2) }}/{{ $db->datel }}/FT/{{ $db->sto }}/{{ Request::segment(4) }}/{{ Request::segment(5) }}">{{ $db->FT }}</a>
                                    @else
                                    -
                                    @endif
                                </td>
                                <td>
                                    @if($db->CC != 0)
                                    <a href="/listsh/{{ Request::segment(2) }}/{{ $db->datel }}/CC/{{ $db->sto }}/{{ Request::segment(4) }}/{{ Request::segment(5) }}">{{ $db->CC }}</a>
                                    @else
                                    -
                                    @endif
                                </td>
                                <td>
                                    @if($db->TOL != 0)
                                    <a href="/listsh/{{ Request::segment(2) }}/{{ $db->datel }}/TOL/{{ $db->sto }}/{{ Request::segment(4) }}/{{ Request::segment(5) }}">{{ $db->TOL }}</a>
                                    @else
                                    -
                                    @endif
                                </td>
                                <td>
                                    @if($db->KK != 0)
                                    <a href="/listsh/{{ Request::segment(2) }}/{{ $db->datel }}/KK/{{ $db->sto }}/{{ Request::segment(4) }}/{{ Request::segment(5) }}">{{ $db->KK }}</a>
                                    @else
                                    -
                                    @endif
                                </td>
                                <td>
                                    @if($db->KC != 0)
                                    <a href="/listsh/{{ Request::segment(2) }}/{{ $db->datel }}/KC/{{ $db->sto }}/{{ Request::segment(4) }}/{{ Request::segment(5) }}">{{ $db->KC }}</a>
                                    @else
                                    -
                                    @endif
                                </td>
                                <td>
                                    @if($db->C != 0)
                                    <a href="/listsh/{{ Request::segment(2) }}/{{ $db->datel }}/C/{{ $db->sto }}/{{ Request::segment(4) }}/{{ Request::segment(5) }}">{{ $db->C }}</a>
                                    @else
                                    -
                                    @endif
                                </td>
                                <td>
                                    @if($db->FF != 0)
                                    <a href="/listsh/{{ Request::segment(2) }}/{{ $db->datel }}/FF/{{ $db->sto }}/{{ Request::segment(4) }}/{{ Request::segment(5) }}">{{ $db->FF }}</a>
                                    @else
                                    -
                                    @endif
                                </td>
                                <td>
                                    @if($db->OF != 0)
                                    <a href="/listsh/{{ Request::segment(2) }}/{{ $db->datel }}/OF/{{ $db->sto }}/{{ Request::segment(4) }}/{{ Request::segment(5) }}">{{ $db->OF }}</a>
                                    @else
                                    -
                                    @endif
                                </td>
                                <td>
                                    @if($db->selesai_golive != 0)
                                    <a href="/listsh/{{ Request::segment(2) }}/{{ $db->datel }}/selesai_golive/{{ $db->sto }}/{{ Request::segment(4) }}/{{ Request::segment(5) }}">{{ $db->selesai_golive }}</a>
                                    @else
                                    -
                                    @endif
                                </td>
                                <td>
                                    @if($db->selesai_blum_golive != 0)
                                    <a href="/listsh/{{ Request::segment(2) }}/{{ $number }}/selesai_blum_golive/{{ $db->sto }}/{{ Request::segment(4) }}/{{ Request::segment(5) }}">{{ $db->selesai_blum_golive }}</a>
                                    @else
                                    -
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <td rowspan="2" colspan="2">
                                    TOTAL
                                </td>
                                <td rowspan="2">
                                    @if($sumSisa_Order != 0)
                                    <a href="/listsh/{{ Request::segment(2) }}/{{ Request::segment(3) }}/Sisa_Order/all/{{ Request::segment(4) }}/{{ Request::segment(5) }}">{{ $sumSisa_Order }}</a>
                                    @else
                                    -
                                    @endif
                                </td>
                                @if(Request::segment(2) == 'unsc_psb')
                                <td rowspan="2">
                                    @if($sumWO_tersedia != 0)
                                    <a href="/listsh/{{ Request::segment(2) }}/{{ Request::segment(3) }}/WO_tersedia/all/{{ Request::segment(4) }}/{{ Request::segment(5) }}">{{ $sumWO_tersedia }}</a>
                                    @else
                                    -
                                    @endif
                                </td>
                                @endif
                                <td rowspan="2">
                                    @if($sumNeed_Progres != 0)
                                    <a href="/listsh/{{ Request::segment(2) }}/{{ Request::segment(3) }}/Need_Progres/all/{{ Request::segment(4) }}/{{ Request::segment(5) }}">{{ $sumNeed_Progres }}</a>
                                    @else
                                    -
                                    @endif
                                </td>
                                <td rowspan="2">
                                    @if($sumOgp != 0)
                                    <a href="/listsh/{{ Request::segment(2) }}/{{ Request::segment(3) }}/Ogp/all/{{ Request::segment(4) }}/{{ Request::segment(5) }}">{{ $sumOgp }}</a>
                                    @else
                                    -
                                    @endif
                                </td>
                                <td rowspan="2">
                                    @if($sumPending != 0)
                                    <a href="/listsh/{{ Request::segment(2) }}/{{ Request::segment(3) }}/Ogp/all/{{ Request::segment(4) }}/{{ Request::segment(5) }}">{{ $sumPending }}</a>
                                    @else
                                    -
                                    @endif
                                </td>
                                <td rowspan="2">
                                    @if($sumBerangkat != 0)
                                    <a href="/listsh/{{ Request::segment(2) }}/{{ Request::segment(3) }}/Berangkat/all/{{ Request::segment(4) }}/{{ Request::segment(5) }}">{{ $sumBerangkat }}</a>
                                    @else
                                    -
                                    @endif
                                </td>
                                @if(Request::segment(2) == 'unsc_psb')
                                <td rowspan="2">
                                    @if($sumnot_pt2 != 0)
                                    <a href="/listsh/{{ Request::segment(2) }}/{{ Request::segment(3) }}/not_pt2/all/{{ Request::segment(4) }}/{{ Request::segment(5) }}">{{ $sumnot_pt2 }}</a>
                                    @else
                                    -
                                    @endif
                                </td>
                                @endif
                                <td>
                                    @if($sumDF != 0)
                                    <a href="/listsh/{{ Request::segment(2) }}/{{ Request::segment(3) }}/DF/all/{{ Request::segment(4) }}/{{ Request::segment(5) }}">{{ $sumDF }}</a>
                                    @else
                                    -
                                    @endif
                                </td>
                                <td>
                                    @if($sumFT != 0)
                                    <a href="/listsh/{{ Request::segment(2) }}/{{ Request::segment(3) }}/FT/all/{{ Request::segment(4) }}/{{ Request::segment(5) }}">{{ $sumFT }}</a>
                                    @else
                                    -
                                    @endif
                                </td>
                                <td>
                                    @if($sumCC != 0)
                                    <a href="/listsh/{{ Request::segment(2) }}/{{ Request::segment(3) }}/CC/all/{{ Request::segment(4) }}/{{ Request::segment(5) }}">
                                    {{ $sumCC }}</a>
                                    @else
                                    -
                                    @endif
                                </td>
                                <td>
                                    @if($sumTOL != 0)
                                    <a href="/listsh/{{ Request::segment(2) }}/{{ Request::segment(3) }}/TOL/all/{{ Request::segment(4) }}/{{ Request::segment(5) }}">{{ $sumTOL }}</a>
                                    @else
                                    -
                                    @endif
                                </td>
                                <td>
                                    @if($sumKK != 0)
                                    <a href="/listsh/{{ Request::segment(2) }}/{{ Request::segment(3) }}/KK/all/{{ Request::segment(4) }}/{{ Request::segment(5) }}">{{ $sumKK }}</a>
                                    @else
                                    -
                                    @endif
                                </td>
                                <td>
                                    @if($sumKC != 0)
                                    <a href="/listsh/{{ Request::segment(2) }}/{{ Request::segment(3) }}/KC/all/{{ Request::segment(4) }}/{{ Request::segment(5) }}">{{ $sumKC }}</a>
                                    @else
                                    -
                                    @endif
                                </td>
                                <td>
                                    @if($sumC != 0)
                                    <a href="/listsh/{{ Request::segment(2) }}/{{ Request::segment(3) }}/C/all/{{ Request::segment(4) }}/{{ Request::segment(5) }}">{{ $sumC }}</a>
                                    @else
                                    -
                                    @endif
                                </td>
                                <td>
                                    @if($sumFF != 0)
                                    <a href="/listsh/{{ Request::segment(2) }}/{{ Request::segment(3) }}/FF/all/{{ Request::segment(4) }}/{{ Request::segment(5) }}">{{ $sumFF }}</a>
                                    @else
                                    -
                                    @endif
                                </td>
                                <td>
                                    @if($sumOF != 0)
                                    <a href="/listsh/{{ Request::segment(2) }}/{{ Request::segment(3) }}/OF/all/{{ Request::segment(4) }}/{{ Request::segment(5) }}">{{ $sumOF }}</a>
                                    @else
                                    -
                                    @endif
                                </td>
                                <td rowspan="2">
                                    @if($sumSelesai_golive != 0)
                                    <a href="/listsh/{{ Request::segment(2) }}/{{ Request::segment(3) }}/Selesai_golive/all/{{ Request::segment(4) }}/{{ Request::segment(5) }}">{{ $sumSelesai_golive }}</a>
                                    @else
                                    -
                                    @endif
                                </td>
                                <td rowspan="2">
                                    @if($sumselesai_blum_golive != 0)
                                    <a href="/listsh/{{ Request::segment(2) }}/{{ Request::segment(3) }}/selesai_blum_golive/all/{{ Request::segment(4) }}/{{ Request::segment(5) }}">{{ $sumselesai_blum_golive }}</a>
                                    @else
                                    -
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td colspan="9" style="text-align: center;">
                                   @if($sumK != 0)
                                   <a href="/listsh/{{ Request::segment(2) }}/{{ Request::segment(3) }}/Kendala/all/{{ Request::segment(4) }}/{{ Request::segment(5) }}">{{ $sumK }}</a>
                                   @else
                                   -
                                   @endif
                               </tr>
                           </tfoot>
                       </table>
                   </div>
               </div>
           </div>
       </div>
   </div>
   @endsection
   @section('footerS')
   <script type="text/javascript">
    $(document).ready(function () {
        $('[data-toggle="popover"]').popover();
    });
</script>
@endsection