@extends('layout')
@section('title', 'List Pekerjaan Tim')
@section('headerS')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link href="/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
@endsection
@section('style')
<style type="text/css">
	th, td{
		text-align: center;
	}
	div>table {
		float: left;
	}
	.down{
		-moz-transform:rotate(90deg);
		-webkit-transform:rotate(90deg);
		transform:rotate(90deg);
	}
	.rounded{
		border-radius: 10px 60px 60px 60px;
	}
</style>
@endsection
@section('content')
@if( count($rekap_rinci) > 0)
<div class="container-fluid" style="padding-top: 25px;">
	<div style="padding-top: 25px;">
			<div id="report_more_daily">
				<div class="row justify-content-center">
					<div class="col col-md-6">
						<div class="table-responsive" style="padding-bottom: 10px;">
							<table class="table table-bordered table-striped">
								<thead>
									<tr>
										<tr>
											<th colspan="6">{{ $bulan }}</th>
										</tr>
										<th>Kendala</th>
										<th>Selesai</th>
										<th>Ogp</th>
										<th>Pending</th>
										<th>Total</th>
									</tr>
								</thead>
								<?php
								$sum_selesai= $sum_kendala= $sum_pending= $sum_ogp= $sum_total= 0;
								?>
								<tbody>
									@foreach($rekap_rinci as $data)
										@foreach($data as $rekap)
										@php
											$sum_selesai+=$rekap->Selesai;
											$sum_kendala+=$rekap->kendala;
											$sum_pending+=$rekap->pending;
											$sum_ogp+=$rekap->ogp;
											$sum_total+=$rekap->total;
										@endphp
										@endforeach
									@endforeach
									<tr>
										<td id="kendala" data-kendala="{{ $sum_kendala }}">{{ $sum_kendala?:'-' }}</td>
										<td id="selesai" data-selesai="{{ $sum_selesai }}">{{ $sum_selesai?:'-' }}</td>
										<td id="ogp" data-ogp="{{ $sum_ogp }}">{{ $sum_ogp?:'-' }}</td>
										<td id="pending" data-pending="{{ $sum_pending }}">{{ $sum_pending?:'-' }}</td>
										<td id="total" data-total="{{ $sum_total }}">{{ $sum_total?:'-' }}</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			@forelse($detail_data as $no => $data)
				@foreach($data as $tanggal => $redata)
				<div class="panel rounded panel-info" style="margin-top: 20px;">
					<div class="table-responsive " id="teknisi" style="padding-bottom: 3px;">
						<p style="cursor: pointer;font-size: 12px;" class="label rounded label-success css_style no_{{$tanggal}}" data-number="{{ $tanggal }}" data-toggle="collapse" href="#collapse{{$tanggal}}" role="button" aria-expanded="false" aria-controls="collapse{{$tanggal}}">{{$tanggal}}<i class="fa fa-chevron-right rotate{{ $tanggal }}"></i></p>
						<div class="collapse multi-collapse" id="collapse{{$tanggal}}">
							<div class="panel-body">
								<div class="form-group">
									<input type="text" class="form-control" class="search" data-tanggal="{{ $tanggal }}" placeholder="Masukkan ODP tanggal {{ $tanggal }} Untuk Mencari Data">
								</div>
								<table class="table table-bordered">
									<thead>
										<tr>
											<th>Nama Proyek</th>
											<th>Sto</th>
											<th>Nama ODP</th>
											<th>Koordinat ODP</th>
											<th>Nama Regu</th>
											<th>Status</th>
											<th>Catatan Teknisi</th>
											<th>Unduh Pekerjaan</th>
											<th>Unduh ABD</th>
										</tr>
									</thead>
									<tbody>
										@foreach($redata as $number => $value)
										<tr>
											<td>{{ $value->project_name }}</td>
											<td>{{ $value->sto }}</td>
											<td>{{ $value->odp_nama }}</td>
											<td>{{ $value->lt_koordinat_odp }}</td>
											<td>{{ $value->regu_name }}</td>
											<td>{{ $value->lt_status }}</td>
											<td>{{ $value->lt_catatan }}</td>
											<td><a type="button" style="color: #6bacd5;" href="/Download/file/{{ $value->id }}" class="btn btn-light show_detail_photo"><span data-icon="Z" class="linea-icon linea-elaborate fa-fw" style="font-size: 27px; bottom: 10px;"></span></a></td>
											<td>@if($value->lt_status == 'Selesai')<a type="button" style="color: #FFD800FF;" href="/Download/abd/{{ $value->id }}" data-deploy="{{ $value->id }}" class="btn btn-light"><span data-icon="&#xe003;" class="linea-icon linea-basic fa-fw" style="font-size: 27px; bottom: 10px;"></span></a>@else Tidak Ada! @endif</td>
										</tr>
										@endforeach
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				@endforeach
			@empty
			@endforelse
	</div>
</div>
@else
<div class="alert alert-danger"> Tidak Ada Data Disini <i data-icon="g" class="linea-icon linea-basic fa-fw"></i></div>
@endif
@endsection
@section('footerS')
<script src="/bower_components/moment/min/moment.min.js"></script>
<script src="/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script>
	$(function(){
		var my_data = <?= json_encode($detail_data) ?>,
		lala = [];
		if(my_data != null){
			$.each(my_data, function( index, value ) {
				$.each(value, function( obj, mine_Value ) {
					lala.push(obj);

				});
			});
			$(".no_"+lala[0]).attr('aria-expanded', true);
			$("#collapse"+lala[0]).attr('aria-expanded', true);
			$("#collapse"+lala[0]).toggleClass('show');
			$(".rotate"+lala[0]).toggleClass('down');
		}

		$(".show_detail_photo").click(function(){
			var values = $(this).attr('data-id');
			var deploy = $(this).attr('data-deploy');
			var res = values.toUpperCase();
			$.ajax({
				type: 'GET',
				url : "/matrix/photo/"+deploy,
				success: function(data) {
					/*console.log(data);*/
					$('#photo_rekap_detail').modal('show');
					$('#odp_nama').text('Detail Foto ODP '+res);
					$('#Konten').html(data);
				},
				error: function(e){
					/*console.log(e);*/
				}
			});
		});

		$(".css_style").click(function () {
			var data = $(this).attr('data-number');
			/*console.log(data);*/
			$(".rotate"+data).toggleClass("down");
			$(".rotate"+data).css({
				"-moz-transition":"all .5s linear",
				"-webkit-transition":"all .5s linear",
				'transition': 'all .5s linear'
			});
		});

	});
</script>
@endsection