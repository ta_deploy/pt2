@extends('layout')
@section('title', 'Peta')
@section('headerS')
<meta name="viewport" content="initial-scale=1,maximum-scale=1,user-scalable=no" />
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin=""/>
<link rel="stylesheet" href="/bower_components/leaflet-routing-machine-3.2.12/dist/leaflet-routing-machine.css" />
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/leaflet-measure-path@1.5.0/leaflet-measure-path.css">
@endsection
@section('style')
<style type="text/css">
    .badge{
        font-size: 11px;
        color: black;
    }

    /* .leaflet-measure-path-measurement {
        font-size: 10px !important;
    } */
</style>
@endsection
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12 mt-4">
            <div class="form-group form-inline">
                <div style="padding-left: 0px;" class="checkbox checkbox-default checkbox-circle">
                    <input id="checkbox_belum" type="checkbox" class="input_map" name="status[]" value="'need_progres'">
                    <label for="checkbox_belum"><span class="badge" style="background-color: #bcbcbc">Progress</span></label>
                </div>
                <div class="checkbox checkbox-default checkbox-circle">
                    <input id="checkbox_ogp" type="checkbox" class="input_map" name="status[]" value="'Ogp'">
                    <label for="checkbox_ogp"><span class="badge" style="background-color: #9079fc">OGP</span></label>
                </div>
                <div class="checkbox checkbox-default checkbox-circle">
                    <input id="checkbox_selesai" type="checkbox" class="input_map" name="status[]" value="'Selesai'">
                    <label for="checkbox_selesai"><span class="badge" style="background-color: #65ba4a">Selesai</span></label>
                </div>
                <div class="checkbox checkbox-default checkbox-circle">
                    <input id="checkbox_kendala" type="checkbox" class="input_map" name="status[]" value="'Kendala'">
                    <label for="checkbox_kendala"><span class="badge" style="background-color: #efe387">Kendala</span></label>
                </div>
                <div class="checkbox checkbox-default checkbox-circle">
                    <input id="unsc" type="checkbox" class="unsc" value="unsc" name="kpro_urgent[]">
                    <label for="unsc"><span class="badge" style="background-color: #df0d10; color: white;">UNSC KPRO</span></label>
                </div>
                <div class="checkbox checkbox-default checkbox-circle">
                    <input id="near_odp" type="checkbox" class="unsc" value="near_odp" name="kpro_urgent[]">
                    <label for="near_odp"><span class="badge" style="background-color: #df0d10; color: white;">ODP GOLIVE TERDEKAT DENGAN UNSC</span></label>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class='input-group date'>
                <input type='text' class="form-control input_map" name='rangedate' value="{{date("Y-m-d")}} - {{date("Y-m-d")}}">
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar kalender" style="cursor: pointer;"></span>
                </span>
            </div>
        </div>
        {{-- <div class="col-md-12" style="margin-top: 10px;">
            <a type="button" class="btn btn-warning undo_btn">Undo</a>
        </div>
        <span class="estimasi_jarak"></span> --}}
    </div>
    <div id="map" style="height: 750px; margin-top: 10px;"></div>
</div>
@endsection
@section('footerS')
<script src="/bower_components/moment/min/moment.min.js"></script>
<script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>
<script src="https://unpkg.com/@turf/turf@6/turf.min.js"></script>
<script src="/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="/bower_components/leaflet-routing-machine-3.2.12/dist/leaflet-routing-machine.min.js"></script>
<script src="/bower_components/leaflet-routing-machine-3.2.12/examples/Control.Geocoder.js"></script>
<script src="https://cdn.jsdelivr.net/npm/leaflet-measure-path@1.5.0/leaflet-measure-path.js"></script>
<script type="text/javascript">
    $(function(){
        $('input[name="rangedate"]').daterangepicker({
            opens: 'right',
            locale: {
            format: 'YYYY-MM-DD'
            },
        });

        var mbAttr = 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        token = 'pk.eyJ1IjoicmVuZGlsaWF1dyIsImEiOiJjazhlMmF0YjkxMjZhM21wZXdjbHhoM2wxIn0.aJrybWWJSh5O8mDmLOAFPQ';
        // mbUrl = `https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=${token}`,
        // grayscale = L.tileLayer(mbUrl, {id: 'mapbox/light-v9', tileSize: 512, zoomOffset: -1, attribution: mbAttr}),
        // streets = L.tileLayer(mbUrl, {id: 'mapbox/streets-v11', tileSize: 512, zoomOffset: -1, attribution: mbAttr}),
        googleStreets = L.tileLayer('https://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}',{
            maxZoom: 20,
            subdomains:['mt0','mt1','mt2','mt3']
        }),
        googleHybrid = L.tileLayer('https://{s}.google.com/vt/lyrs=s,h&x={x}&y={y}&z={z}',{
            maxZoom: 20,
            subdomains:['mt0','mt1','mt2','mt3']
        }),
        googleSat = L.tileLayer('https://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}',{
            maxZoom: 20,
            subdomains:['mt0','mt1','mt2','mt3']
        }),
        googleTerrain = L.tileLayer('https://{s}.google.com/vt/lyrs=p&x={x}&y={y}&z={z}',{
            maxZoom: 20,
            subdomains:['mt0','mt1','mt2','mt3']
        }),
        googleAlteredRoad = L.tileLayer('https://{s}.google.com/vt/lyrs=r&x={x}&y={y}&z={z}',{
            maxZoom: 20,
            subdomains:['mt0','mt1','mt2','mt3']
        }),
        googleTraffic = L.tileLayer('https://{s}.google.com/vt/lyrs=m@221097413,traffic&x={x}&y={y}&z={z}', {
            maxZoom: 20,
            subdomains: ['mt0', 'mt1', 'mt2', 'mt3'],
        }),
        odp_all = L.layerGroup(),
        sc_relasi = L.layerGroup(),
        pLineGroup = L.layerGroup(),
        map = L.map('map', {
            center: ['-3.319000', '114.584100'],
            zoom: 15,
            maxZoom: 19,
            layers: [googleStreets, googleHybrid, googleSat, googleTerrain, googleTraffic, googleAlteredRoad, odp_all, sc_relasi, pLineGroup]
        }),
        baseLayers = {
            'Jalan': googleStreets,
            'Satelit': googleSat,
            'Satelit Dan Jalan': googleHybrid,
            'Jalur': googleTerrain,
            'Jalur 2': googleAlteredRoad,
            'Lalu Lintas': googleTraffic,
        },
        overlays = {
            'ODP': odp_all,
            'SC Kendala': sc_relasi,
            'Garis': pLineGroup,
        },
        layerControl = L.control.layers(baseLayers, overlays, {position: 'topleft'}).addTo(map),
        odp_green = new L.Icon({
            iconUrl: 'https://promitos.tomman.app/image/odp_green.png',
            iconSize: [20, 20],
            iconAnchor: [12, 41],
            popupAnchor: [1, -34]
        }),
        odp_purple = new L.Icon({
            iconUrl: 'https://promitos.tomman.app/image/purple_triangle.png',
            iconSize: [20, 20],
            iconAnchor: [12, 41],
            popupAnchor: [1, -34]
        }),
        odp_yellow = new L.Icon({
            iconUrl: 'https://promitos.tomman.app/image/odp_yellow.png',
            iconSize: [20, 20],
            iconAnchor: [12, 41],
            popupAnchor: [1, -34]
        }),
        avail_sc_blue = new L.Icon({
            iconUrl: 'https://promitos.tomman.app/image/avail_sc_blue.png',
            // iconSize: [20, 20],
            iconAnchor: [12, 41],
            popupAnchor: [1, -34]
        }),
        // circle = L.circle(map.getCenter(), {
        //     radius: 1100,
        //     color: 'red',
        //     fillOpacity: 0,
        // }).addTo(map),
        odp_grey = new L.Icon({
            iconUrl: 'https://promitos.tomman.app/image/odp_grey.png',
            iconSize: [20, 20],
            iconAnchor: [12, 41],
            popupAnchor: [1, -34]
        }),
        collect_odp = [],
        load_collect_odp = [],
        sum = 0
        data_tiang = null;

        // $.ajax({
        //     type: "GET",
        //     url: "/get_all_tiang",
        //     async: false,
        //     success: function(data){
        //         // console.log(data.slice(1, 10))
        //         data_tiang = data.slice(1, 10)
        //     }
        // });

        function getDistanceFromLatLonInKm(lat1,lon1, lat2,lon2) {
            var R = 6371; // Radius of the earth in km
            var dLat = deg2rad(lat2-lat1);  // deg2rad below
            var dLon = deg2rad(lon2-lon1);
            var a =
                Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2) ) *
                Math.sin(dLon/2) * Math.sin(dLon/2)
                ;
            var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a) );
            var d = R * c; // Distance in km
            return d;
        }

        function deg2rad(deg) {
            return deg * (Math.PI/180)
        }

        // var previousPoint = '',
        // koor_sc = '-3.3238308176859803,114.56867095679623',
        // load_koor = [],
        // check_dup = [],
        // load_distance = [],
        // totalDistance = [];

        // map.on("moveend", function () {
        //     var lat = map.getCenter().lat,
        //     lng = map.getCenter().lng;

        //     circle.setLatLng(map.getCenter() );
        //     map._renderer._update();

        //     $.each(data_tiang, function(k, v){
        //         let dst = (getDistanceFromLatLonInKm(lat, lng, v.latitude, v.longitude) * 1000).toFixed(2);
        //         data_tiang[k].distance = dst;
        //     })

        //     if(collect_odp.length != 0)
        //     {
        //         $.each(collect_odp, function(k, v){
        //             if( (getDistanceFromLatLonInKm(lat, lng, v._latlng.lat, v._latlng.lng) * 1000).toFixed(2) > 1000.00 ){
        //                 map.removeLayer(v)
        //             }
        //         })

        //         $.each(odp_all._layers, function(k, v){
        //             if( (getDistanceFromLatLonInKm(lat, lng, v._latlng.lat, v._latlng.lng) * 1000).toFixed(2) > 1000.00 ){
        //                 delete odp_all._layers[k]
        //             }
        //         })
        //     }

        //     $.each(data_tiang, function(k, v){
        //         var koor = `${v.latitude},${v.longitude}`;

        //         if(v.distance <= 1000.00 ){
        //             if(load_collect_odp.indexOf(koor) === -1){

        //                 var note = `Distribusi: <b>${v.dist}</b></br>Odc: <b>${v.odc}</b>`;
        //                 collect_odp.push(L.marker([v.latitude, v.longitude])
        //                 .bindPopup(note)
        //                 .addTo(odp_all)
        //                 .on('click', function(e) {
        //                     let koor_now = `${e.latlng.lat}, ${e.latlng.lng}`;

        //                     if(check_dup.indexOf(koor_now) === -1){
        //                         check_dup.push(koor_now)
        //                         load_koor.push([e.latlng.lat, e.latlng.lng])
        //                         var polyline = L.polyline(load_koor).addTo(pLineGroup).showMeasurements({
        //                             formatDistance: function(e){
        //                                 // return (Math.round(1000 * e / 1609.344) / 1000).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' Mil';
        //                                 // return Math.round(e / 1000).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' Km';
        //                                 return Math.round(e).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' Meter';
        //                             }
        //                         });

        //                         var marker = e.target._popup._source;

        //                         polyline.getLatLngs().forEach(function (latLng) {
        //                             var latlng_text = latLng.lat +','+ latLng.lng;

        //                             if(load_distance.indexOf(latlng_text) === -1){
        //                                 load_distance.push(latlng_text)
        //                             }

        //                             if (previousPoint.length != 0) {
        //                                 if(koor_sc.split(',').map(x => x.trim() ).join(',') != latlng_text){
        //                                     if(totalDistance.map(x => x.koor).indexOf(latlng_text) === -1){
        //                                         totalDistance.push({
        //                                             koor: latlng_text,
        //                                             jarak: parseFloat(previousPoint.distanceTo(latLng).toFixed(2) )
        //                                         });
        //                                     }

        //                                     marker.getPopup().setContent(`${note}</br>Total Jarak: <b>${totalDistance.map(x => x.jarak).reduce( (a, b) => a + b)}</b> meter</br> Jarak Dari Tiang Sebelumnya:<b>${previousPoint.distanceTo(latLng).toFixed(2)}</b> Meter`);

        //                                     marker.getPopup().update();
        //                                     marker.closePopup();
        //                                 }
        //                             }
        //                             previousPoint = latLng;
        //                         });

        //                         map.flyTo([e.latlng.lat, e.latlng.lng], 18);

        //                         $.each(load_distance, function(k, v){
        //                             var koor_odp = v.split(',').map(x => x.trim() ),
        //                             koor_sc_split = koor_sc.split(',').map(x => x.trim() );
        //                         });
        //                     }
        //                 }) );

        //                 load_collect_odp.push(koor)
        //             }
        //         }else{
        //             load_collect_odp = load_collect_odp.filter(function(e) { return e != koor })
        //         }
        //     })
        //     if(totalDistance.length != 0){
        //         $('.estimasi_jarak').html(`Total Estimasi Jarak ${totalDistance.map(x => x.jarak).reduce( (a, b) => a + b)} Meter`)
        //     }
        // });

        // navigator.geolocation.getCurrentPosition(position => {
        //     const { coords: { latitude, longitude }} = position;
        //     var marker = new L.marker([latitude, longitude], {
        //         autoPan: true
        //     }).addTo(map);

        //     map.flyTo([latitude, longitude], 16);
        // })

        // //tarik jalur jalan

        // // Final Code Here
        // function generateLocation(latitude, longitude, max, min = 0) {
        //     if (min > max) {
        //         throw new Error(`min(${min}) cannot be greater than max(${max})`);
        //     }

        //     // earth radius in km
        //     const EARTH_RADIUS = 6371;

        //     // 1° latitude in meters
        //     const DEGREE = EARTH_RADIUS * 2 * Math.PI / 360 * 1000;

        //     // random distance within [min-max] in km in a non-uniform way
        //     const maxKm = max * 1000;
        //     const minKm = min * 1000;
        //     const r = ((maxKm - minKm + 1) * Math.random() ** 0.5) + minKm;

        //     // random angle
        //     const theta = Math.random() * 2 * Math.PI;

        //     const dy = r * Math.sin(theta);
        //     const dx = r * Math.cos(theta);

        //     let newLatitude = latitude + dy / DEGREE;
        //     let newLongitude = longitude + dx / (DEGREE * Math.cos(deg2rad(latitude)));
        //     let textlatlng = `${newLatitude},${newLongitude}`;

        //     const distance = getDistanceFromLatLonInKm(latitude, longitude, newLatitude, newLongitude);

        //     return {
        //         newLatitude,
        //         newLongitude,
        //         textlatlng,
        //         distance: Math.round(distance)
        //     };
        // }

        // var temp_lat = -3.411178531169984,
        // temp_lng = 114.6655351999804,
        // get_t = generateLocation(temp_lat, temp_lng, 0.150, 0);

        // console.log(get_t, get_t.textlatlng)

        // var myRouter = L.Routing.control({
        //     waypoints: [
        //         // L.latLng(-3.4127080943070816,114.66433575817811),
        //         L.latLng(get_t.newLatitude, get_t.newLongitude),
        //         L.latLng(temp_lat, temp_lng)
        //     ],
        //     // router: L.Routing.mapbox(token, {
        //     //     language: 'id',
        //     //     urlParameters: {
        //     //         vehicle: 'foot'
        //     //     }
        //     // }),
        // 	geocoder: L.Control.Geocoder.nominatim(),
        //     routeWhileDragging: true,
        //     reverseWaypoints: true,
        //     showAlternatives: true,
        //     collapsible: true,
        //     autoRoute: true,
        //     routeWhileDragging: true,
        // }).on('routeselected', function(e) {
        //     var route = e.route,
        //     jarak = e.route.summary.totalDistance,
        //     waktu = e.route.summary.totalTime;
        //     console.log(jarak, waktu)
        //     alert('Showing route between waypoints:\n' + JSON.stringify(route.inputWaypoints, null, 2));
        // }).addTo(map);

        map.on('click', function(e){
            var coord = e.latlng,
            lat = coord.lat,
            lng = coord.lng;

            get_koor = `${lat},${lng}`;
            console.log(get_koor)
        });

        // //tarik jalur udara
        // var coords = [];

        // for (var i = 0; i < 2; i += 1) {
        //     coords.push(getRandomLatLng() );
        // }

        // var polyline = L.polyline(coords).addTo(map)
        // .showMeasurements({
        //     formatDistance: function(e){
        //         // return (Math.round(1000 * e / 1609.344) / 1000).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' Mil';
        //         // return Math.round(e / 1000).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' Km';
        //         return Math.round(e).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' Meter';
        //     }
        // });


        // $('.undo_btn').on('click', function(){
        //     // var polyline = L.polyline(coords).addTo(map)
        //     if(load_koor.length > 1){
        //         pLineGroup.clearLayers();
        //         load_koor.pop();
        //         check_dup.pop();
        //         totalDistance.pop();

        //         if(totalDistance.length != 0){
        //             sum = totalDistance.map(x => x.jarak).reduce( (a, b) => a + b);
        //         }else{
        //             sum = 0
        //         }

        //         $('.estimasi_jarak').html(`Total Estimasi Jarak ${sum} Meter`);
        //         map.closePopup()

        //         var polyline = L.polyline(load_koor).addTo(pLineGroup)

        //         polyline = L.polyline(load_koor).addTo(pLineGroup).showMeasurements({
        //             formatDistance: function(e){
        //                 // console.log(e, pLineGroup)
        //                 // console.log(Math.round(e).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' Meter')
        //                 // return (Math.round(1000 * e / 1609.344) / 1000).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' Mil';
        //                 // return Math.round(e / 1000).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' Km';
        //                 return Math.round(e).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' Meter';
        //             }
        //         });
        //     }
        // });

        // load_koor.push(koor_sc.split(',').map(x => x.trim() ) );

        // L.marker(koor_sc.split(',').map(x => x.trim() ), {icon: avail_sc_blue}).bindPopup(`tes`).addTo(map).on('click', function(e) {
        //     console.log(e.latlng, 'tesss', koor_sc.split(',').map(x => x.trim() ) )
        // });

        // map.flyTo(koor_sc.split(',').map(x => x.trim() ), 16);

        // function getRandomLatLng() {
        //     return [
        //         -3.328251 + 0.001 * Math.random(),
        //         114.584202 + 0.002 * Math.random()
        //     ];
        // }

        $('.input_map').on('change', function(){
            var value_data = [];

            $('input[name="status[]"]:checked').each(function() {
                value_data.push(this.value);
            });

            var tgl = $('input[name="rangedate"]').val().split(' - ');

            $.ajax({
                type: "GET",
                url: "/geo/allOrder",
                data:{
                    val: value_data,
                    tgl: tgl
                },
                success: function(data){
                    var data = JSON.parse(data);
                    odp_all.clearLayers();

                    $.each(data, function(k, v){
                        if(v.lt_status == "Ogp"){
                            icon = {icon: odp_purple};
                        }else if(v.lt_status == "Selesai"){
                            icon = {icon: odp_green};
                        }else if(v.lt_status == "Kendala"){
                            icon = {icon: odp_yellow};
                        }else{
                            icon = {icon: odp_grey};
                        }

                        var koor_odp = v.odp_koor.split(',').map(x => x.trim() );

                        if(koor_odp.length == 2){
                            L.marker(koor_odp, icon)
                            .bindPopup(`<strong>${v.odp_nama}</strong><br>${v.lt_status}<br>${v.regu_name}`)
                            .on('click', function(e){
                                $.ajax({
                                    url: 'https://promitos.tomman.app/hero/get_sc_by_koor',
                                    data: {isi: [v.odp_koor]},
                                    method: 'GET',
                                    dataType: 'JSON',
                                    beforeSend: function() {
                                        var notif=$.toast({
                                            position: 'mid-center',
                                            showHideTransition: 'plain',
                                            hideAfter: 3000
                                        });

                                        notif.update({
                                            heading: 'Notiffikasi',
                                            text: `Mohon Tunggu!`,
                                            position: 'mid-center',
                                            icon: 'info',
                                            showHideTransition: 'plain',
                                            stack: false
                                        });
                                    }
                                }).done( function(e){
                                    sc_relasi.clearLayers();
                                    $.each(e, function(k, v){
                                        data = JSON.parse(v);
                                        var notif=$.toast({
                                            position: 'mid-center',
                                            showHideTransition: 'plain',
                                            hideAfter: false
                                        });

                                        notif.update({
                                            heading: 'Notiffikasi',
                                            text: `Terdapat ${data.rows} SC Terdekat!`,
                                            position: 'mid-center',
                                            icon: 'success',
                                            showHideTransition: 'plain',
                                            stack: false
                                        });
                                        sc = data.data;
                                        $.each(sc, function(k2, v2){
                                            var koor = v2.lat +','+v2.lon,
                                            get_distance = getDistanceFromLatLonInKm(v2.lat, v2.lon, koor_odp[0], koor_odp[1]);

                                            L.marker(koor.split(','), {icon: avail_sc_blue} )
                                            .bindPopup(`SC: <b>${v2.orderId}</b></br>Koordinat SC: <u>${koor}</u></br>Jarak:<u><b>${v2.jarak}</b> Meter</u></br>Tanggal Lapor: <b>${v2.tgl_laporan_teknisi}</b></br>Tim: ${v2.tim}</br>Status Teknisi: <b>${v2.status_teknisi}</b></br>Jenis Layanan: <b>${v2.jenis_layanan}</b></br>Alamat: ${v2.orderAddr}<br/>`)
                                            .addTo(sc_relasi);
                                        })
                                    })
                                });
                            })
                            .addTo(odp_all);
                        }
                    });
                }
            });
        });

        $('.unsc').on('change', function(){
            $('.input_map').removeAttr('checked');

            var value_data = [];

            $('input[name="kpro_urgent[]"]:checked').each(function() {
                value_data.push(this.value);
            });

            $.ajax({
                type: "GET",
                url: "/geo/get_unsc",
                data:{
                    jenis: value_data
                },
                success: function(data){
                    var data = JSON.parse(data);

                    $.each(data, function(k, v){
                        if(v.nomor_sc_log == null){
                            button_search = `<a type='button' data-koor='${v.odp_koor}' class='btn search_sc btn-primary btn-sm'>Lihat SC</a>`;
                        }

                        let koor = v.odp_koor.split(',').map(x => x.trim() );
                        if(koor.length == 2){
                            L.marker(koor, {icon: blue_triangle})
                            .bindPopup(`<strong> SUDAH GO LIVE </br>${latLng.odp_nama}<br/>${latLng.sto}</strong><br/>${latLng.project_name}<br>${latLng.lt_status}<br>${latLng.tgl_selesai}`)
                            .on('click', function(e){
                                console.log(e, e.latlng)
                            })
                            .addTo(odp_all);
                        }
                    });
                }
            });
        });
    });
</script>
@endsection