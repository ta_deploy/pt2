@extends('layout')
@section('title', 'home')
@section('headerS')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link href="/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="/bower_components/select2-bootstrap/select2-bootstrap.css" />
@endsection
@section('style')
<style type="text/css">
    th {
        white-space: nowrap;
    }

    div>table {
        float: left;
    }

    .panel .panel-action a {
	    opacity: 100;
    }

    .ti-minus {
        font-size: 20px;
        color: white;
        font-weight: bold;
    }

    .ti-plus {
        font-size: 20px;
        color: white;
        font-weight: bold;
    }
</style>
@endsection
@section('content')
<div class="modal fade modal_detail_layout detail_layout modal_lg" id="detail_layout">
    <div class="modal_lg" role="document" style="width: 95%; margin: auto;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel" style="width: 400px;"><div id="nama_project"></div></h5>
            </div>
            <div class="modal-body" style="overflow-y:auto;padding: 0px 0px 0px 0px;">
                <div id="content"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid" style="padding-top: 25px;">
    <div class='row' style="margin-bottom: 15px;">
        <div class="col-md-6">
            <div id="canvas-wrapper" style="width: 100%; height: 100%; margin-left: auto; margin-right: auto;">
                <canvas id="graph"></canvas>
            </div>
        </div>
        <div class="col-md-6">
            <div id="canvas-wrapper2" style="width: 100%; height: 100%; margin-left: auto; margin-right: auto;">
                <canvas id="bar"></canvas>
            </div>
        </div>
    </div>
    @include('Partial.alerts')
    @if (Session::has('alerts_tele'))
      @foreach(Session::get('alerts_tele') as $alert)
        <div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
      @endforeach
    @endif
    <div class="panel panel-warning">
        <div class="panel-heading">Dashboard PT2 Tahun {{ Request::segment(2) }} <span class="jenis_wo"></span></div>
        <div class="panel-body">
            <div class="row row-in">
                <div class="col-lg-3 col-sm-6 row-in-br">
                    <div class="col-in row">
                        <div class="col-md-6 col-sm-6 col-xs-6"> <i data-icon="E" class="linea-icon linea-basic"></i>
                            <h5 class="text-muted vb">Sisa Order</h5>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <h3 class="counter text-right m-t-15 text-primary">{{ $status_header->Sisa_Order }}</h3>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="progress">
                                @php
                                    $total = 0;
                                    if($status_header->Sisa_Order > 0){
                                        $total = round(($status_header->Sisa_Order/$status_header->Sisa_Order) * 100);
                                    }
                                @endphp
                                <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="{{  json_decode( json_encode($status_header->Sisa_Order), true) }}" aria-valuemin="0" aria-valuemax="{{  json_decode( json_encode($status_header->Sisa_Order), true) }}" style="width: {{ $total }}%"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6 row-in-br  b-r-none">
                    <div class="col-in row">
                        <div class="col-md-6 col-sm-6 col-xs-6"> <i class="linea-icon linea-basic" data-icon="&#xe01b;"></i>
                            <h5 class="text-muted vb">Need Progress</h5>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <h3 class="counter text-right m-t-15 text-megna">{{ $status_header->Need_Progres }}</h3>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="progress">
                                @php
                                    $total = 0;
                                    if($status_header->Sisa_Order > 0)
                                    {
                                        $total = round(($status_header->Need_Progres/$status_header->Sisa_Order) * 100);
                                    }
                                @endphp
                                <div class="progress-bar progress-bar-megna" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: {{ $total }}%"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6 row-in-br">
                    <div class="col-in row">
                        <div class="col-md-6 col-sm-6 col-xs-6"> <i class="linea-icon linea-basic" data-icon="&#xe00b;"></i>
                            <h5 class="text-muted vb">Selesai</h5>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <h3 class="counter text-right m-t-15 text-success">{{ $status_header->Selesai }}</h3>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="progress">
                                @php
                                    $total = 0;
                                    if($status_header->Sisa_Order > 0)
                                    {
                                        $total = round(($status_header->Selesai/$status_header->Sisa_Order) * 100);
                                    }
                                @endphp
                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: {{ $total }}%"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6  b-0">
                    <div class="col-in row">
                        <div class="col-md-6 col-sm-6 col-xs-6"> <i class="linea-icon linea-basic" data-icon="&#xe016;"></i>
                            <h5 class="text-muted vb">Kendala</h5>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <h3 class="counter text-right m-t-15 text-danger">{{ $status_header->Kendala }}</h3>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="progress">
                                @php
                                $total = 0;
                                    if($status_header->Sisa_Order > 0)
                                    {
                                        $total = round(($status_header->Kendala/$status_header->Sisa_Order) * 100);
                                    }
                                @endphp
                                <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: {{ $total }}%"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-info">
        <div class="panel-heading header-date">Index Pencarian <span class="jenis_wo"></span></div>
        <div class="panel-body">
            <form id="formlistG" name="formlistG" method="get">
                <div class='input-group date col-md-4'>
                    <input type='text' class="form-control" name='rangedate' disabled value="{{ $tgl_a ?? date("Y-m-d", strtotime('first day of january this year') ) }} - {{ $tgl_b ?? date("Y-m-d", strtotime('last day of december this year') ) }}">
                    <input type="hidden" name="tgl_a" value="{{ $tgl_a ?? date('Y-m-d', strtotime('first day of january this year') ) }}">
                    <input type="hidden" name="tgl_f" value="{{ $tgl_b ?? date("Y-m-d", strtotime('last day of december this year') ) }}">
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar kalender" style="cursor: pointer;"></span>
                    </span>
                </div>
                <div class='input-group date col-md-4'>
                    <select class="form-control jenis_pt2" name="jenis_pt2">
                        <option value="all">ALL</option>
						@foreach ($list_wo as $v)
						    <option value="{{ $v }}">{{ $v }}</option>
						@endforeach
					</select>
                </div>
                <div class='input-group date col-md-1'>
                    <button type="submit" class="btn btn-primary">Cari</button>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        @foreach ($table_status as $key => $val)
            <div class="{{ (in_array($key, ['all_data']) ? 'col-md-12' : 'col-md-6' ) }}">
                <div class="panel panel-info">
                    <div class="panel-heading header-date">Order <u>{{ str_replace('_', ' ', $key) }} <span class="jenis_wo"></span></u>
                        <div class="panel-action">
                            <a href="#" data-perform="panel-collapse"><i style="font-size: 20px;" class="ti-minus"></i></a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table color-bordered-table success-bordered-table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Datel</th>
                                        <th>Sisa Order</th>
                                        <th>Undispatch</th>
                                        <th>Need Progress</th>
                                        <th>OGP</th>
                                        <th>Pending</th>
                                        <th>Kendala</th>
                                        <th>Selesai Fisik</th>
                                        <th>Belum Abd Valid 4</th>
                                        <th>Selesai Go Live</th>
                                        <th>Lanjut PT-1</th>
                                        <th>Total Order</th>
                                    </tr>
                                </thead>
                                @php
                                    $sumSisa_Order = $sumNeed_Progres = $sumundispatch = $sumWO_tersedia = $sumOgp = $sumPending = $sumKendala = $sumSelesai_fisik = $sumSelesai_golive = $sumbelum_abd = $sumberangkat = $sumlanjut_pt1 = $sumtotal_order = 0;
                                @endphp
                                <tbody>
                                    @foreach($val as $no => $data)
                                    @php
                                        $sumSisa_Order     += $data->Sisa_Order;
                                        $sumNeed_Progres   += $data->Need_Progres;
                                        $sumundispatch     += $data->undispatch;
                                        $sumberangkat      += $data->Berangkat;
                                        $sumOgp            += $data->Ogp;
                                        $sumPending        += $data->Pending;
                                        $sumKendala        += $data->Kendala;
                                        $sumSelesai_fisik  += $data->Selesai_fisik;
                                        $sumbelum_abd      += $data->belum_abd;
                                        $sumSelesai_golive += $data->selesai_golive;
                                        $sumlanjut_pt1     += $data->lanjut_pt1;
                                        $sumtotal_order    += $data->total_order;
                                    @endphp
                                    <tr>
                                        <td>{{ ++$no }}</td>
                                        <td><a href="/detail_dsh/{{ $key }}/{{ $data->datel }}/{{ $tgl_a }}/{{ $tgl_b }}/{{ $jenis_wo ?? 'all' }}">{{ $data->datel }}</a></td>
                                        <td style="text-align: center;">
                                            @if($data->Sisa_Order != 0)
                                                <a style="cursor: pointer; color: #3cbcf5" class='detail_modal' data-href="/listsh/{{ $key }}/{{ $data->datel }}/Sisa_Order/all/{{ $tgl_a }}/{{ $tgl_b }}/{{ $jenis_wo ?? 'all' }}">{{ $data->Sisa_Order }}</a>
                                            @else
                                                -
                                            @endif
                                        </td>
                                        <td style="text-align: center;">
                                            @if($data->undispatch != 0)
                                                <a style="cursor: pointer; color: #3cbcf5" class='detail_modal' data-href="/listsh/{{ $key }}/{{ $data->datel }}/undispatch/all/{{ $tgl_a }}/{{ $tgl_b }}/{{ $jenis_wo ?? 'all' }}">{{ $data->undispatch }}</a>
                                            @else
                                                -
                                            @endif
                                        </td>
                                        <td style="text-align: center;">
                                            @if($data->Need_Progres != 0)
                                                <a style="cursor: pointer; color: #3cbcf5" class='detail_modal' data-href="/listsh/{{ $key }}/{{ $data->datel }}/Need_Progres/all/{{ $tgl_a }}/{{ $tgl_b }}/{{ $jenis_wo ?? 'all' }}">{{ $data->Need_Progres }}</a>
                                            @else
                                                -
                                            @endif
                                        </td>
                                        <td style="text-align: center;">
                                            @if($data->Ogp != 0)
                                                <a style="cursor: pointer; color: #3cbcf5" class='detail_modal' data-href="/listsh/{{ $key }}/{{ $data->datel }}/Ogp/all/{{ $tgl_a }}/{{ $tgl_b }}/{{ $jenis_wo ?? 'all' }}">{{ $data->Ogp }}</a>
                                            @else
                                                -
                                            @endif
                                        </td>
                                        <td style="text-align: center;">
                                            @if($data->Pending != 0)
                                                <a style="cursor: pointer; color: #3cbcf5" class='detail_modal' data-href="/listsh/{{ $key }}/{{ $data->datel }}/Pending/all/{{ $tgl_a }}/{{ $tgl_b }}/{{ $jenis_wo ?? 'all' }}">{{ $data->Pending }}</a>
                                            @else
                                                -
                                            @endif
                                        </td>
                                        <td style="text-align: center;">
                                            @if($data->Kendala != 0)
                                                <a style="cursor: pointer; color: #3cbcf5" class='detail_modal' data-href="/listsh/{{ $key }}/{{ $data->datel }}/Kendala/all/{{ $tgl_a }}/{{ $tgl_b }}/{{ $jenis_wo ?? 'all' }}">{{ $data->Kendala }}</a>
                                            @else
                                                -
                                            @endif
                                        </td>
                                        <td style="text-align: center;">
                                            @if($data->Selesai_fisik != 0)
                                                <a style="cursor: pointer; color: #3cbcf5" class='detail_modal' data-href="/listsh/{{ $key }}/{{ $data->datel }}/Selesai_fisik/all/{{ $tgl_a }}/{{ $tgl_b }}/{{ $jenis_wo ?? 'all' }}">{{ $data->Selesai_fisik }}</a>
                                            @else
                                                -
                                            @endif
                                        </td>
                                        <td style="text-align: center;">
                                            @if($data->belum_abd != 0)
                                                <a style="cursor: pointer; color: #3cbcf5" class='detail_modal' data-href="/listsh/{{ $key }}/{{ $data->datel }}/belum_abd/all/{{ $tgl_a }}/{{ $tgl_b }}/{{ $jenis_wo ?? 'all' }}">{{ $data->belum_abd }}</a>
                                            @else
                                                -
                                            @endif
                                        </td>
                                        <td style="text-align: center;">
                                            @if($data->selesai_golive != 0)
                                                <a style="cursor: pointer; color: #3cbcf5" class='detail_modal' data-href="/listsh/{{ $key }}/{{ $data->datel }}/selesai_golive/all/{{ $tgl_a }}/{{ $tgl_b }}/{{ $jenis_wo ?? 'all' }}">{{ $data->selesai_golive }}</a>
                                            @else
                                                -
                                            @endif
                                        </td>
                                        <td style="text-align: center;">
                                            @if($data->lanjut_pt1 != 0)
                                                <a style="cursor: pointer; color: #3cbcf5" class='detail_modal' data-href="/listsh/{{ $key }}/{{ $data->datel }}/lanjut_pt1/all/{{ $tgl_a }}/{{ $tgl_b }}/{{ $jenis_wo ?? 'all' }}">{{ $data->lanjut_pt1 }}</a>
                                            @else
                                                -
                                            @endif
                                        </td>
                                        <td style="text-align: center;">
                                            @if($data->total_order != 0)
                                                <a style="cursor: pointer; color: #3cbcf5" class='detail_modal' data-href="/listsh/{{ $key }}/{{ $data->datel }}/total_order/all/{{ $tgl_a }}/{{ $tgl_b }}/{{ $jenis_wo ?? 'all' }}">{{ $data->total_order }}</a>
                                            @else
                                                -
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="2"><a style="cursor: pointer; color: #3cbcf5" class='detail_modal' data-href="/detail_dsh/{{ $key }}/all/{{ $tgl_a }}/{{ $tgl_b }}">Semua Datel</a></td>
                                        <td style="text-align: center;">
                                            @if ($sumSisa_Order != 0)
                                                <a style="cursor: pointer; color: #3cbcf5" class='detail_modal' data-href="/listsh/{{ $key }}/all/Sisa_Order/all/{{ $tgl_a }}/{{ $tgl_b }}/{{ $jenis_wo ?? 'all' }}">{{ $sumSisa_Order }}</a>
                                            @else
                                                -
                                            @endif
                                        </td>
                                        <td style="text-align: center;">
                                            @if ($sumundispatch != 0)
                                                <a style="cursor: pointer; color: #3cbcf5" class='detail_modal' data-href="/listsh/{{ $key }}/all/undispatch/all/{{ $tgl_a }}/{{ $tgl_b }}/{{ $jenis_wo ?? 'all' }}">{{ $sumundispatch }}</a>
                                            @else
                                                -
                                            @endif
                                        </td>
                                        <td style="text-align: center;">
                                            @if ($sumNeed_Progres != 0)
                                                <a style="cursor: pointer; color: #3cbcf5" class='detail_modal' data-href="/listsh/{{ $key }}/all/Need_Progres/all/{{ $tgl_a }}/{{ $tgl_b }}/{{ $jenis_wo ?? 'all' }}">{{ $sumNeed_Progres }}</a>
                                            @else
                                                -
                                            @endif
                                        </td>
                                        <td style="text-align: center;">
                                            @if ($sumOgp != 0)
                                                <a style="cursor: pointer; color: #3cbcf5" class='detail_modal' data-href="/listsh/{{ $key }}/all/Ogp/all/{{ $tgl_a }}/{{ $tgl_b }}/{{ $jenis_wo ?? 'all' }}">{{ $sumOgp }}</a>
                                            @else
                                                -
                                            @endif
                                        </td>
                                        <td style="text-align: center;">
                                            @if ($sumPending != 0)
                                                <a style="cursor: pointer; color: #3cbcf5" class='detail_modal' data-href="/listsh/{{ $key }}/all/Pending/all/{{ $tgl_a }}/{{ $tgl_b }}/{{ $jenis_wo ?? 'all' }}">{{ $sumPending }}</a>
                                            @else
                                                -
                                            @endif
                                        </td>
                                        <td style="text-align: center;">
                                            @if ($sumKendala != 0)
                                                <a style="cursor: pointer; color: #3cbcf5" class='detail_modal' data-href="/listsh/{{ $key }}/all/Kendala/all/{{ $tgl_a }}/{{ $tgl_b }}/{{ $jenis_wo ?? 'all' }}">{{ $sumKendala }}</a>
                                            @else
                                                -
                                            @endif
                                        </td>
                                        <td style="text-align: center;">
                                            @if ($sumSelesai_fisik != 0)
                                                <a style="cursor:  pointer; color: #3cbcf5" class='detail_modal' data-href="/listsh/{{ $key }}/all/Selesai_fisik/all/{{ $tgl_a }}/{{ $tgl_b }}/{{ $jenis_wo ?? 'all' }}">{{ $sumSelesai_fisik }}</a>
                                            @else
                                                -
                                            @endif
                                        </td>
                                        <td style="text-align: center;">
                                            @if ($sumbelum_abd != 0)
                                                <a style="cursor:  pointer; color: #3cbcf5" class='detail_modal' data-href="/listsh/{{ $key }}/all/belum_abd/all/{{ $tgl_a }}/{{ $tgl_b }}/{{ $jenis_wo ?? 'all' }}">{{ $sumbelum_abd }}</a>
                                            @else
                                                -
                                            @endif
                                        </td>
                                        <td style="text-align: center;">
                                            @if ($sumSelesai_golive != 0)
                                                <a style="cursor:  pointer; color: #3cbcf5" class='detail_modal' data-href="/listsh/{{ $key }}/all/selesai_golive/all/{{ $tgl_a }}/{{ $tgl_b }}/{{ $jenis_wo ?? 'all' }}">{{ $sumSelesai_golive }}</a>
                                            @else
                                                -
                                            @endif
                                        </td>
                                        <td style="text-align: center;">
                                            @if ($sumlanjut_pt1 != 0)
                                                <a style="cursor:  pointer; color: #3cbcf5" class='detail_modal' data-href="/listsh/{{ $key }}/all/lanjut_pt1/all/{{ $tgl_a }}/{{ $tgl_b }}/{{ $jenis_wo ?? 'all' }}">{{ $sumlanjut_pt1 }}</a>
                                            @else
                                                -
                                            @endif
                                        </td>
                                        <td style="text-align: center;">
                                            @if ($sumtotal_order != 0)
                                                <a style="cursor:  pointer; color: #3cbcf5" class='detail_modal' data-href="/listsh/{{ $key }}/all/total_order/all/{{ $tgl_a }}/{{ $tgl_b }}/{{ $jenis_wo ?? 'all' }}">{{ $sumtotal_order }}</a>
                                            @else
                                                -
                                            @endif
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
@endsection
@section('footerS')
<script src="/bower_components/waypoints/lib/jquery.waypoints.js"></script>
<script src="/bower_components/counterup/jquery.counterup.min.js"></script>
<script src="/bower_components/moment/min/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.5.0-beta4/html2canvas.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@3.9.1/dist/chart.min.js"></script>
<script src="/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.full.min.js"></script>
<script type="text/javascript">
    $(function(){

        $('.jenis_pt2').select2({
            width: '100%',
			placeholder: "Masukkan Jenis PT-2",
			allowClear: true,
        });

        $('.jenis_pt2').val(null).change();

        var jenis_wo = {!! json_encode($jenis_wo) !!};
        console.log(jenis_wo != null)
        if(jenis_wo != null){
            $('.jenis_pt2').val(jenis_wo).change();
            $('.jenis_wo').html(jenis_wo);
        }else{
            console.log('ini')
            $('.jenis_pt2').val('all').change();
            $('.jenis_wo').html('ALL');
        }

        function my_chart_curve(){
            var ctx =$('#graph'),
            data_chart = {!! json_encode($data_pt2_all_2) !!},
            dc = [],
            label = [],
            selesai = [],
            kendala = [];

            $.each(data_chart, function(k, v){
                label.push(v.bulan);
                selesai.push(v.selesai);
                kendala.push(v.kendala);
            })

            var chartdata = {
            labels: label,
            datasets: [
                {
                    label: 'Selesai',
                    data: selesai,
                    borderColor: '#004a7f',
                    backgroundColor: '#004a7f',
                    yAxisID: 'y',
                },
                {
                    label: 'Kendala',
                    data: kendala,
                    borderColor: '#fb9678',
                    backgroundColor: '#fb9678',
                    yAxisID: 'y',
                }
            ]
            };

            var line = new Chart(ctx, {
            type: 'line',
            data: chartdata,
            options: {
                responsive: true,
                maintainAspectRatio: false,
                interaction: {
                mode: 'index',
                intersect: false,
                },
                stacked: false,
                plugins: {
                title: {
                    display: true,
                    text: `Kurva Untuk Tahun ${new Date().getFullYear()}`
                }
                },
                scales: {
                y: {
                    type: 'linear',
                    display: true,
                    position: 'left',
                },
                y1: {
                    type: 'linear',
                    display: true,
                    position: 'right',
                    grid: {
                        drawOnChartArea: false, // only want the grid lines for one axis to show up
                    },
                },
                }
            },
            });
        }

        my_chart_curve();

        var data_bar = {!! json_encode($saldo_order) !!},
        data_chart = [],
        cm = [],
        label = [],
        color_my = [];

        $.each(data_bar, function(k, v){
            label.push(k);
            color_my.push('#F7464A');
            data_chart.push(v)
        })

        function chart_bar(){
            var ctx =$('#bar');
            var data = {
                type: "bar",
                data: {
                    labels: label,
                    datasets: [{
                        label: "SALDO ORDER PT-2",
                        fill: false,
                        lineTension: 0.1,
                        backgroundColor: color_my,
                        fillColor: "blue",
                        strokeColor: "green",
                        borderWidth: 1,
                        data: data_chart
                    }]
                },
                option: {
                    responsive: true,
                    maintainAspectRatio: false,
                }
            };
            var myfirstChart = new Chart(ctx, data);
        }

        chart_bar()

        $('input[name="rangedate"]').daterangepicker({
            opens: 'left',
            locale: {
                format: 'YYYY-MM-DD'
            }
        }, function(start, end){
            var month1 = start.format('YYYY-MM-DD'),
            month2 = end.format('YYYY-MM-DD')+" 23:59:59";
            $('input[name="tgl_a"]').val(month1);
            $('input[name="tgl_f"]').val(month2);
        });

        $('.kalender').click(function(e){
            e.preventDefault();
            $('input[name="rangedate"]').click();
        });

        $(".counter").counterUp({
            delay: 100,
            time: 1200
        });

        $('.detail_modal').on('click', function(){
            var href = $(this).attr('data-href');
            $.ajax({
                type: 'GET',
                url : href,
                success: function(data) {
                    $('#detail_layout').modal('show');
                    $('#nama_project').text('Detail Data');
                    $('#content').html(data);
                },
                error: function(e){
                }
            });
        })
    });
</script>
@endsection