
@if($files)
<ul>
	@foreach($files as $f)
		<li><a href="{{ $path2d.$f }}">{{ $f }}</a></li>
  @endforeach
</ul>
@else
	<i>No files detected, please hit button SyncImon</i>
@endif