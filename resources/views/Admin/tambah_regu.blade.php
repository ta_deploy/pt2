@extends('layout')
@section('title', 'Tambah Regu')
@section('content')
<div class="form-group">
	<a type="button" class="btn btn-default" href="{{URL::to('/admin/list/regu')}}"><i data-icon="&#xe00f;" class="linea-icon linea-aerrow fa-fw" style="font-size: 20px; vertical-align:middle;"></i>&nbsp;Kembali</a>
</div>
<div class="container-fluid" style="padding-top: 25px;">
	<form method="post" id="regu_form">
		<div class="panel panel-primary">
			<div class="panel-heading">Tambah Regu</div>
			<div class="panel-body">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<div class="form-group">
					<label>Nama Regu</label>
					<div class="alert uraian alert-danger" role="alert" style="display: none;">
						<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
						<span class="sr-only">Error:</span>
						Nama Regu Harus Diisi!
					</div>
					<input type="text" class="form-control" id="uraian" name="uraian" placeholder="Masukkan Nama Regu">
				</div>
				<div class="form-group">
					<label>STO</label>
					<select class="form-control" id="sto" name="sto">
						@foreach($sto as $esto)
						<option value="{{$esto->sto}}">{{$esto->sto}}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group">
					<label>NIK1/Nama</label>
					<div class="alert nik1 alert-danger" role="alert" style="display: none;">
						<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
						<span class="sr-only">Error:</span>
						Nama Harus Diisi!
					</div>
					<input type="text" class="form-control" id="nik1" name="nik1" placeholder="Masukkan Nik1/Nama">
				</div>
				<div class="form-group">
					<label>NIK2/Nama</label>
					<div class="alert nik2 alert-danger" role="alert" style="display: none;">
						<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
						<span class="sr-only">Error:</span>
						Nama Harus Diisi!
					</div>
					<input type="text" class="form-control" id="nik2" name="nik2" placeholder="Masukkan Nik2/Nama">
				</div>
				<div class="form-group">
					<label>Nik Team Leader</label>
					<div class="alert nik_tl alert-danger" role="alert" style="display: none;">
						<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
						<span class="sr-only">Error:</span>
						Nik Team Leader Harus Diisi!
					</div>
					<input type="text" class="form-control" id="nik_tl" name="nik_tl" placeholder="Masukkan Nik Team Leader">
				</div>
				<div class="form-group">
					<button type="submit" class="btn save_me btn-info btn-block" style="text-align: center"><span data-icon="M" class="linea-icon linea-basic fa-fw" style="font-size: 20px; color: #ffffff; "></span>&nbsp;Simpan</button>
				</div>
			</div>
		</div>
	</form>
</div>
@endsection
@section('footerS')
<script type="text/javascript">
	$(function(){
		$('.save_me').click(function(e){
			e.preventDefault();
			var uraian = $.trim($('#uraian').val()),
			nik1 = $.trim($('#nik1').val()),
			nik_tl = $.trim($('#nik_tl').val()),
			nik2 = $.trim($('#nik2').val()),
			datastring = $("#regu_form").serialize();

			if (uraian === '') {
				$('.uraian').css({display: "block"});
				$('#uraian').css({border: "2px solid red"});
				$('#uraian').focus();
			}else{
				$('.uraian').css({display: "none"});
				$('#uraian').css({border: ""});
			}

			if (nik1 === '') {
				$('.nik1').css({display: "block"});
				$('#nik1').css({border: "2px solid red"});
				$('#nik1').focus();
			}else{
				$('.nik1').css({display: "none"});
				$('#nik1').css({border: ""});
			}

			if (nik2 === '') {
				$('.nik2').css({display: "block"});
				$('#nik2').css({border: "2px solid red"});
				$('#nik2').focus();
			}else{
				$('.nik2').css({display: "none"});
				$('#nik2').css({border: ""});
			}

			if (nik_tl === '') {
				$('.nik_tl').css({display: "block"});
				$('#nik_tl').css({border: "2px solid red"});
				$('#nik_tl').focus();
			}else{
				$('.nik_tl').css({display: "none"});
				$('#nik_tl').css({border: ""});
			}

			if (uraian === '' || nik1 === '' || nik2 === '' || nik_tl === ''){
				return false;
			}else{
				var notif=$.toast({
					position: 'mid-center',
					showHideTransition: 'plain',
					hideAfter: false
				});
				$.ajax({
					type: 'POST',
					url : "{{route('simpan_regu')}}",
					beforeSend: function(){
						notif.update({
							heading: 'Pemberitahuan',
							text: 'Sedang Disimpan!',
							position: 'mid-center',
							icon: 'info',
							showHideTransition: 'plain',
							stack: false
						});
					},
					data : datastring,
					success: function(data) {
						notif.update({
							heading: 'Pemberitahuan',
							text: 'Tim '+uraian+' Telah Berhasil Disimpan!',
							position: 'mid-center',
							icon: 'success',
							stack: false
						});
						window.location = "/admin/list/regu";
						/*console.log(data);*/
					},
					error: function(e){
						var exception = e.responseJSON.exception;
						var file = e.responseJSON.file;
						var line = e.responseJSON.line;
						var message = e.responseJSON.message;
						/*console.log(e);*/
						$.ajax({
							type: 'POST',
							url: "/send/err",
							data: {
								"_token": "{{ csrf_token() }}",
								"exception" : exception,
								"file" : file,
								"line" : line,
								"message" : message
							},
							success: function (data) {
								/*console.log(data);*/
							},
							error: function(e){
								/*console.log(e);*/
							}
						});
						notif.update({
							heading: 'Error',
							text: 'Data Kesalahan Telah Dikirim. Untuk Lebih Lanjut Hubungi <a href="https://t.me/RendyL">TOMMAN</a>',
							showHideTransition: 'fade',
							icon: 'error',
							showHideTransition: 'plain',
							hideAfter: false,
							stack: false
						});
					}
				});
			}
		});
	});
</script>
@endsection