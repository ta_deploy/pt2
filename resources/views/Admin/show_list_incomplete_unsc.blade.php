@extends('layout')
@section('title', 'List Admin')
@section('headerS')
<link href="/bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
@endsection
@section('style')
<style type="text/css">
	th, td{
		text-align: center;
		white-space:nowrap;
	}

	@media (min-width: 768px) {
		.modal-xl {
			width: 90%;
			max-width:1200px;
		}
	}
</style>
@endsection
@section('content')
<div class="modal fade download_modal" id="download_modal">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Silahkan Download</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" style="overflow-y:auto;height : 450px">
				<h4>List Download</h4>
				<div id="download_list"></div>
				<input type="hidden" name="dwl_id" class="form-control" id="dwl_id">
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade odp_modal" id="odp_modal">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Daftar Odp</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<h4>Upload Excel</h4>
				<input type="hidden" name="id_deployer" id="id_deployer" />
				<input name="Upload_E" type="file" id="Upload_E" class="form-control-file Upload_E" />
				<h4>Daftar Odp</h4>
				<div id="odp_list"></div>
				<input type="hidden" name="odp_id" class="form-control" id="odp_id">
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
@if(isset($ogp[0]))
<div class="panel panel-info">
	<div class="panel-heading">Notifikasi</div>
	<div class="panel-body row">
		@foreach($ogp as $ogps)
		<div class="card col-sm-3" style="background-color: #353c48;">
			<ul class="list-group list-group-flush">
				<li class="list-group-item">{{$ogps->odp_nama}}</li>
				<li class="list-group-item">{{$ogps->regu_name}}</li>
			</ul>
		</div>
		@endforeach
	</div>
</div>
@endif
<div class="container-fluid" style="padding-top: 25px;">
	<div class="panel panel-warning">
		<div class="panel-heading">List Teknisi</div>
		<div class="panel-body">
			<ul class="nav nav-tabs">
				<li>
					<a href="#menupt2"  data-toggle="tab">List Pt.2(UNSC)</a>
				</li>
				<li>
					<a href="#menupt3" data-toggle="tab">List Progres</a>
				</li>
				<li>
					<a href="#menupt4" data-toggle="tab">List Selesai</a>
				</li>
				<li>
					<a href="#menupt5" data-toggle="tab">List Kendala</a>
				</li>
			</ul>
			<div class="tab-content">
				<div id="menupt2" class="tab-pane active">
					<form id="formlistG" name="formlistG">
						<div class="form-group">
							<input type="text" class="form-control" id="cari_pt2" placeholder="Masukkan SC Lama atau Lainnya">
						</div>
						<div class="table-responsive">
							<table id="groupPT2" class="table table-hover table-bordered" >
								<thead>
									<tr>
										<th>#</th>
										<th>SC Lama</th>
										<th>SC Status</th>
										<th>Progress UN</th>
										<th>Tgl Tinjut SDI</th>
										<th>OLT</th>
										<th>Label Odp</th>
										<th>Koordinat Odp</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php $result = 1; ?>
									@foreach($unsc as $no => $un)
									<tr>
										<td>{{$result++}}</td>
										<td>{{ $un->orderId }}</td>
										<td>{{ $un->orderStatus }}</td>
										<td>
											{{ $un->tinjut }}
										</td>
										<td class="tgl_tinjut_{{ $un->orderId }}">{{ $un->tgl_tinjut?date('Y-m-d H:i:s', $un->tgl_tinjut):'' }}</td>
										<td>{{ $un->olt }}</td>
										<td>{{ $un->label_odp }}</td>
										<td>{{ $un->koordinat_odp }}</td>
										<td><a type="button" class="btn btn-light" href='{{URL::to("/admin/dispatch/add/{$un->orderId}")}}'><span class="glyphicon glyphicon-plus" style="color:#000000; font-size: 15px" id="contactG"></span>&nbsp; {{($un->regu_id)?($un->regu_name):'Dispatch'}}</a></td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</form>
				</div>

				<div id="menupt3" class="tab-pane fade">
					<form id="formlistG" name="formlistG">
						<div class="form-group">
							<input type="text" class="form-control" id="cari_d" placeholder="Masukkan Nama ODP atau Lainnya">
						</div>
						<div class="table-responsive">
							<table id="groupPTd" class="table table-hover table-bordered" >
								<thead>
									<tr>
										<th>#</th>
										<th>Nama ODP</th>
										<th>Koordinat ODP</th>
										<th>Nama ODC</th>
										<th>Koordinat ODC</th>
										<th>Kategory</th>
										<th>Action</th>
										{{-- <th>#</th>
										<th>Nama Project</th>
										<th>Id Proaktif</th>
										<th>Id Ex</th>
										<th onclick="sort_table_pt2_d(4)" style="cursor: pointer;">STO</th>
										<th>Service Value</th>
										<th>Material Value</th>
										<th>Total RAB</th>
										<th>Status</th> --}}
									</tr>
								</thead>
								<tbody>
									<?php $result = 1; ?>
									@foreach($nonaunscprog as $no => $un)
									<tr>
										<td>{{$result++}}</td>
										<td>{{ $un->odp_nama }}</td>
										<td>{{ $un->odp_koor }}</td>
										<td>{{ $un->odc_nama }}</td>
										<td>{{ $un->odc_koor }}</td>
										<td>{{ ($un->kategory_non_unsc ==1)?'Non UNSC':'UNSC' }}</td>
										<td><a type="button" class="btn btn-light" href='{{($un->kategory_non_unsc ==0)?URL::to("/admin/dispatch/edit/{$un->deployer_id}"):URL::to("/admin/edit_non/{$un->deployer_id}")}}'><span class="glyphicon glyphicon-plus" style="color:#000000; font-size: 15px" id="contactG"></span>&nbsp; {{($un->regu_id)?($un->regu_name):'Dispatch'}}</a></td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</form>
				</div>

				<div id="menupt4" class="tab-pane fade">
					<form id="formlistG" name="formlistG">
						<div class="form-group">
							<input type="text" class="form-control" id="cari_syn" placeholder="Masukkan Nama ODP atau Lainnya">
						</div>
						<div class="table-responsive">
							<table id="groupPTsyn" class="table table-hover table-bordered" >
								<thead>
									<tr>
										<th>#</th>
										<th>Nama ODP</th>
										<th>Koordinat ODP</th>
										<th>Nama ODC</th>
										<th>Koordinat ODC</th>
										<th>Nama Regu</th>
										<th>Tanggal Selesai</th>
										{{-- <th>#</th>
										<th>Nama Project</th>
										<th>Id Proaktif</th>
										<th>Id Ex</th>
										<th onclick="sort_table_pt2_d(4)" style="cursor: pointer;">STO</th>
										<th>Service Value</th>
										<th>Material Value</th>
										<th>Total RAB</th>
										<th>Status</th> --}}
									</tr>
								</thead>
								<tbody>
									<?php $result = 1; ?>
									@foreach($unscdone as $no => $un)
									<tr>
										<td>{{$result++}}</td>
										<td>{{ $un->odp_nama }}</td>
										<td>{{ $un->odp_koor }}</td>
										<td>{{ $un->odc_nama }}</td>
										<td>{{ $un->odc_koor }}</td>
										<td>{{ $un->regu_name }}</td>
										<td>{{ $un->tgl_selesai }}</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</form>
				</div>

				<div id="menupt5" class="tab-pane fade">
					<form id="formlistG" name="formlistG">
						<div class="form-group">
							<input type="text" class="form-control" id="cari_k" placeholder="Masukkan Nama ODP atau Lainnya">
						</div>
						<div class="table-responsive">
							<table id="groupPTd" class="table table-hover table-bordered" >
								<thead>
									<tr>
										<th>#</th>
										<th>Nama ODP</th>
										<th>Koordinat ODP</th>
										<th>Nama ODC</th>
										<th>Koordinat ODC</th>
										<th>Kategory</th>
										<th>Action</th>
										{{-- <th>#</th>
										<th>Nama Project</th>
										<th>Id Proaktif</th>
										<th>Id Ex</th>
										<th onclick="sort_table_pt2_d(4)" style="cursor: pointer;">STO</th>
										<th>Service Value</th>
										<th>Material Value</th>
										<th>Total RAB</th>
										<th>Status</th> --}}
									</tr>
								</thead>
								<tbody>
									<?php $result = 1; ?>
									@foreach($kendala as $no => $un)
									<tr>
										<td>{{$result++}}</td>
										<td>{{ $un->odp_nama }}</td>
										<td>{{ $un->odp_koor }}</td>
										<td>{{ $un->odc_nama }}</td>
										<td>{{ $un->odc_koor }}</td>
										<td>{{ ($un->kategory_non_unsc ==1)?'Non UNSC':'UNSC' }}</td>
										<td><a type="button" class="btn btn-light" href='{{($un->kategory_non_unsc ==0)?URL::to("/admin/dispatch/edit/{$un->deployer_id}"):URL::to("/admin/edit_non/{$un->deployer_id}")}}'><span class="glyphicon glyphicon-plus" style="color:#000000; font-size: 15px" id="contactG"></span>&nbsp;Ubah Status Kendala</a></td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</form>
				</div>

			</div>
		</div>
	</div>
</div>
@endsection
@section('footerS')
<script src="/bower_components/datatables/jquery.dataTables.min.js"></script>
<script src="/bower_components/excel/zip/WebContent/zip.js"></script>
<script src="/bower_components/excel/async-master/lib/async.js"></script>
<script src="/bower_components/excel/underscore.js"></script>
<script src="/bower_components/excel/xlsxParser.js"></script>
<script type="text/javascript">
	$(function() {
		$("#cari_pt2").keyup(function(){
			var input, filter, table, tr, td1, i, td2, td3;
			input = document.getElementById("cari_pt2");
			filter = input.value.toUpperCase();
			table = document.getElementById("groupPT2");
			tr = table.getElementsByTagName("tr");
			for (i = 0; i < tr.length; i++) {
				td1 = tr[i].getElementsByTagName("td")[0];
				td2 = tr[i].getElementsByTagName("td")[1];
				td3 = tr[i].getElementsByTagName("td")[2];
				if (td1) {
					if (td1.innerHTML.toUpperCase().indexOf(filter) > -1) {
						tr[i].style.display = "";
					} else if (td2) {
						if (td2.innerHTML.toUpperCase().indexOf(filter) > -1) {
							tr[i].style.display = "";
						} else if (td3) {
							if (td3.innerHTML.toUpperCase().indexOf(filter) > -1) {
								tr[i].style.display = "";
							} else{
								tr[i].style.display = "none";
							}
						}
					}
				}
			}
		});

		$("#cari_syn").keyup(function(){
			var input, filter, table, tr, td1, i, td2, td3;
			input = document.getElementById("cari_syn");
			filter = input.value.toUpperCase();
			table = document.getElementById("groupPTsyn");
			tr = table.getElementsByTagName("tr");
			for (i = 0; i < tr.length; i++) {
				td1 = tr[i].getElementsByTagName("td")[0];
				td2 = tr[i].getElementsByTagName("td")[1];
				td3 = tr[i].getElementsByTagName("td")[2];
				if (td1) {
					if (td1.innerHTML.toUpperCase().indexOf(filter) > -1) {
						tr[i].style.display = "";
					} else if (td2) {
						if (td2.innerHTML.toUpperCase().indexOf(filter) > -1) {
							tr[i].style.display = "";
						} else if (td3) {
							if (td3.innerHTML.toUpperCase().indexOf(filter) > -1) {
								tr[i].style.display = "";
							} else{
								tr[i].style.display = "none";
							}
						}
					}
				}
			}
		});

		$("#cari_d").keyup(function(){
			var input, filter, table, tr, td1, i, td2, td3;
			input = document.getElementById("cari_d");
			filter = input.value.toUpperCase();
			table = document.getElementById("groupPTd");
			tr = table.getElementsByTagName("tr");
			for (i = 0; i < tr.length; i++) {
				td1 = tr[i].getElementsByTagName("td")[0];
				td2 = tr[i].getElementsByTagName("td")[1];
				td3 = tr[i].getElementsByTagName("td")[2];
				if (td1) {
					if (td1.innerHTML.toUpperCase().indexOf(filter) > -1) {
						tr[i].style.display = "";
					} else if (td2) {
						if (td2.innerHTML.toUpperCase().indexOf(filter) > -1) {
							tr[i].style.display = "";
						} else if (td3) {
							if (td3.innerHTML.toUpperCase().indexOf(filter) > -1) {
								tr[i].style.display = "";
							} else{
								tr[i].style.display = "none";
							}
						}
					}
				}
			}
		});

		$("#cari_un").keyup(function(){
			var input, filter, table, tr, td1, i, td2, td3;
			input = document.getElementById("cari_d");
			filter = input.value.toUpperCase();
			table = document.getElementById("groupPTd");
			tr = table.getElementsByTagName("tr");
			for (i = 0; i < tr.length; i++) {
				td1 = tr[i].getElementsByTagName("td")[0];
				td2 = tr[i].getElementsByTagName("td")[1];
				td3 = tr[i].getElementsByTagName("td")[2];
				if (td1) {
					if (td1.innerHTML.toUpperCase().indexOf(filter) > -1) {
						tr[i].style.display = "";
					} else if (td2) {
						if (td2.innerHTML.toUpperCase().indexOf(filter) > -1) {
							tr[i].style.display = "";
						} else if (td3) {
							if (td3.innerHTML.toUpperCase().indexOf(filter) > -1) {
								tr[i].style.display = "";
							} else{
								tr[i].style.display = "none";
							}
						}
					}
				}
			}
		});

		$(".download-btn").click(function(event){
			$("#download_list").html('');
			$('#dwl_id').val(event.target.dataset.id);
			var id = document.getElementById('dwl_id').value;
			$.get("/download/list/"+id, function(text) {
				$("#download_list").html(text);
			});
		});

		$(".odp-btn").click(function(event){
			$(".Upload_E").val('');
			$("#odp_list").html('');
			$('#odp_id').val(event.target.dataset.id);
			$('#id_deployer').val(event.target.dataset.id);
			var id = document.getElementById('odp_id').value;
			$.get("/odp/list/"+id, function(text) {
				$("#odp_list").html(text);
			});

		});

		$(".nav-tabs a").click(function(){
			$(this).tab('show');
		});

		var migrasi = [];
		$(".Upload_E").change(function(e){
			console.log("prepare upload");

			migrasi = [];
			var file = document.getElementById('Upload_E').files[0];
			zip.workerScriptsPath = "/bower_components/excel/zip/WebContent/";
			xlsxParser.parse(file).then(function(data) {
				for(var i=1;i<data.length;i++){
					var sto_koor        = data[i][1],
					odp_nama        = data[i][2],
					olt_port        = data[i][3],
					otb_odf         = data[i][4],
					otb_panel       = data[i][5],
					otb_port        = data[i][6],
					otb_kap         = data[i][7],
					feeder_nama     = data[i][8],
					feeder_kap      = data[i][9],
					odc_nama        = data[i][10],
					odc_panel       = data[i][11],
					odc_port        = data[i][12],
					odc_spl         = data[i][13],
					odc_kap         = data[i][14],
					odc_koor        = data[i][15],
					odp_panel       = data[i][16],
					odp_port        = data[i][17],
					odp_spl         = data[i][18],
					odp_koor        = data[i][19],
					dist_nama       = data[i][20],
					dist_kap        = data[i][21],
					dist_tenos      = data[i][22];
					if(odp_nama){
						migrasi.push({
							sto_koor: sto_koor,
							odp_nama:odp_nama,
							olt_port:olt_port,
							odf_otb:otb_odf,
							otb_panel:otb_panel,
							otb_port:otb_port,
							otb_kap:otb_kap,
							fe_nama:feeder_nama,
							fe_kap:feeder_kap,
							odc_nama:odc_nama,
							odc_panel:odc_panel,
							odc_port:odc_port,
							odc_spl:odc_spl,
							odc_kap:odc_kap,
							odc_koor:odc_koor,
							odp_panel:odp_panel,
							odp_port:odp_port,
							odp_spl:odp_spl,
							odp_koor:odp_koor,
							dist_nama:dist_nama,
							dist_kap:dist_kap,
							tenos:dist_tenos,
						});
						// var core_pt2 = $('#core_PT2_'+ex_id).val(JSON.stringify(migrasi));
					}
				}
				var id = $('#id_deployer').val();
				$.ajax({
					url:"{{ route('saveE') }}",
					method:'POST',
					data:{_token: "{{ csrf_token() }}", id:id, core_pt2:JSON.stringify(migrasi)},
					success: function(data){
						$("#odp_list").html(data);
					}
				});
			});

			// $('#odp_id').val(event.target.dataset.id);
			// var id = document.getElementById('odp_id').value;
			// casper.start('odp/excel'+id,function(){
			// 	var rows = this.evaluate(function(){
			// 		return document.querySelectorAll("#groupPT2 > thead").length;
			// 	});

			// 	for(var i = 0; i < rows; i++) {
			// 		this.captureSelector('myscreens/'+i+'.png', '#idTable > tbody  > tr:nth-of-type('+i+')');
			// 	}
			// });
			// casper.run();

		});
	});
function sort_table_pt2(n) {
	var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
	table = document.getElementById("groupPT2");
	switching = true;
	dir = "asc";
	while (switching) {
		switching = false;
		rows = table.getElementsByTagName("TR");
		for (i = 1; i < (rows.length - 1); i++) {
			shouldSwitch = false;
			x = rows[i].getElementsByTagName("TD")[n];
			y = rows[i + 1].getElementsByTagName("TD")[n];
			if (dir == "asc") {
				if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
					shouldSwitch= true;
					break;
				}
			} else if (dir == "desc") {
				if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
					shouldSwitch = true;
					break;
				}
			}
		}
		if (shouldSwitch) {
			rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
			switching = true;
			switchcount ++;
		} else {
			if (switchcount == 0 && dir == "asc") {
				dir = "desc";
				switching = true;
			}
		}
	}
}

function sort_table_pt2_d(n) {
	var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
	table = document.getElementById("groupPT2d");
	switching = true;
	dir = "asc";
	while (switching) {
		switching = false;
		rows = table.getElementsByTagName("TR");
		for (i = 1; i < (rows.length - 1); i++) {
			shouldSwitch = false;
			x = rows[i].getElementsByTagName("TD")[n];
			y = rows[i + 1].getElementsByTagName("TD")[n];
			if (dir == "asc") {
				if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
					shouldSwitch= true;
					break;
				}
			} else if (dir == "desc") {
				if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
					shouldSwitch = true;
					break;
				}
			}
		}
		if (shouldSwitch) {
			rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
			switching = true;
			switchcount ++;
		} else {
			if (switchcount == 0 && dir == "asc") {
				dir = "desc";
				switching = true;
			}
		}
	}
}

function sort_table_pt2_s(n) {
	var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
	table = document.getElementById("groupPTsyn");
	switching = true;
	dir = "asc";
	while (switching) {
		switching = false;
		rows = table.getElementsByTagName("TR");
		for (i = 1; i < (rows.length - 1); i++) {
			shouldSwitch = false;
			x = rows[i].getElementsByTagName("TD")[n];
			y = rows[i + 1].getElementsByTagName("TD")[n];
			if (dir == "asc") {
				if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
					shouldSwitch= true;
					break;
				}
			} else if (dir == "desc") {
				if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
					shouldSwitch = true;
					break;
				}
			}
		}
		if (shouldSwitch) {
			rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
			switching = true;
			switchcount ++;
		} else {
			if (switchcount == 0 && dir == "asc") {
				dir = "desc";
				switching = true;
			}
		}
	}
}

function sort_table_pt2_non_u(n) {
	var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
	table = document.getElementById("groupPTsyn");
	switching = true;
	dir = "asc";
	while (switching) {
		switching = false;
		rows = table.getElementsByTagName("TR");
		for (i = 1; i < (rows.length - 1); i++) {
			shouldSwitch = false;
			x = rows[i].getElementsByTagName("TD")[n];
			y = rows[i + 1].getElementsByTagName("TD")[n];
			if (dir == "asc") {
				if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
					shouldSwitch= true;
					break;
				}
			} else if (dir == "desc") {
				if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
					shouldSwitch = true;
					break;
				}
			}
		}
		if (shouldSwitch) {
			rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
			switching = true;
			switchcount ++;
		} else {
			if (switchcount == 0 && dir == "asc") {
				dir = "desc";
				switching = true;
			}
		}
	}
}

</script>
@endsection
