@extends('layout')
@section('title', 'Booking ODP')
@section('headerS')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.css">
@endsection
@section('style')
<style type="text/css">
    th, td{
        text-align: center;
        white-space:nowrap;
    }

    div>table {
        float: left
    }

    th, #odp_0 > tbody:nth-child(2) > tr:nth-child(1) > td:nth-child(10){
        text-align: center;
        white-space:nowrap;
        vertical-align: middle;
    }

	.panel .panel-action a
	{
        opacity: 1;
    }

	.dataTables_scrollHeadInner
	{
		width:100% !important;
	}

	.dataTables_scrollHeadInner table
	{
		width:100% !important;
	}

	#book_odp{
		overflow-y: scroll !important;
	}

	.dataTables_scroll
	{
		overflow: auto;
	}

	thead, tbody {
		font-size: 1em;
	}
</style>
@endsection
@section('content')
@include('Partial.alerts')
<div class="container-fluid" style="padding-top: 25px;">
    <div class="form-group">
        <a type="button" class="btn btn-default" href="/booking_odp/check"><span data-icon="D" class="linea-icon linea-ecommerce fa-fw" style="font-size: 20px; vertical-align:middle;" id="contactG"></span>&nbsp;Tambah Booking ODP</a>
    </div>

    <div class="panel panel-warning">
        <div class="panel-heading header-date">Ranked Periode {{ Request::segment(2) }}</div>
        <div class="panel-body">
            <table id="book_odp" cellspacing="0" style="width: 100%; height: 400px" class="table table-striped table-bordered table-responsive">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Mitra</th>
                        <th>Site ID</th>
                        <th>Nama Site</th>
                        <th>Nama ODP</th>
                        <th>Koordinat</th>
                        <th>Request</th>
                        <th>Tanggal Order</th>
                        <th>Eksekutor</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@endsection
@section('footerS')
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.js"></script>
<script>
    $(function(){

    var datanya = <?= json_encode($data) ?>,
	final_dta = [],
	get_dt_bo = [];

	if (datanya) {
		$.each( datanya, function( keyC1, valueC1 ) {
            get_dt_bo.push([
                valueC1[0] || null,
                valueC1[1] || null,
                valueC1[2] || null,
                valueC1[3] || null,
                valueC1[4] || null,
                valueC1[6] || null,
                valueC1[8] || null,
                valueC1[11] || null,
                valueC1[17] || null,
                "<a type=\"button\" style='color: #fec109;' class=\"btn btn-light\" href='/booking_odp/edit/"+ keyC1 +"'><span data-icon=\"&#xe005;\" class=\"linea-icon linea-basic fa-fw\" style=\"font-size: 10px;\"></span>&nbsp; Edit</a><a style='color: #fec109;' href=\"#\" type=\"button\" class=\"btn btn-light delete_btn\" data-id="+ keyC1 +" data-odp="+ valueC1[4] +"><span data-icon=\"&#xe01c;\" class=\"linea-icon linea-basic fa-fw\" style=\"font-size: 10px;\"></span>&nbsp; Hapus</a>"
            ]);
		});

		final_dta= {
			BO: get_dt_bo,
		};

	}

     $('#book_odp').DataTable( {
			drawCallback: function () {
        	$( 'table.import_list tbody tr td' ).css( 'padding', '5px 8px 5px 8px' );
    	},
		fixedHeader: {
			header: true,
			footer: true
		},
		order:	[
					[ 0, "desc" ]
				],
		data: final_dta.BO,
		deferRender: true,
		scrollCollapse: true,
		scroller: true,
	}).columns.adjust().draw();

        $('.delete_btn').on('click', function(){
            var odp = $(this).attr('data-odp'),
            id = $(this).attr('data-id');

            Swal.fire({
                title: 'Yakin Untuk Menghapus?',
                text: "Data yang terhapus, akan masuk ke Free ODP!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, Hapus!'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: "GET",
                        url: '/delete_BO/'+ id,
                        cache: false,
                        beforeSend: function() {
                            console.log('dang lah');
                            $("#parent").toggleClass('preloader');
                            $("#child").toggleClass('cssload-speeding-wheel');
                            $("#parent").css({
                                'background-color' : 'rgba(0,0,0,.8)'
                            });
                        },
                        success: function(data) {
                            Swal.fire(
                                'Terhapus!',
                                odp+' berhasil dipindahkan ke Free ODP.',
                                'success'
                            )
                            $("#parent").toggleClass('preloader');
                            $("#child").toggleClass('cssload-speeding-wheel');
                            $("#parent").css({
                                'background-color' : ''
                            });
                            location.reload();
                        }
                    });
                }
            });
        });
    });
</script>
@endsection