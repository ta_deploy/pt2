@extends('layout')
@section("title", Request::path() == 'booking_odp/check' ? 'Input Booking ODP' : 'Edit Booking ODP')
@section('style')
<style type="text/css">
	.select2-search {
		color: black;
	}

	.select2-results {
		/* background-color: #353c48; */
	}
</style>
@endsection
@section('content')
@if ($result['free_odp_msg'])
	@foreach ($result['free_odp_msg'] as $r)
		<div class="alert alert-info">{{ $r }}</div>
	@endforeach
@endif
<div class="form-group">
	<a type="button" class="btn btn-default" href="{{URL::to('/show/booking_odp')}}"><i data-icon="&#xe00f;" class="linea-icon linea-aerrow fa-fw" style="font-size: 20px; vertical-align:middle;"></i>&nbsp;Kembali</a>
</div>
<div class="container-fluid" style="padding-top: 25px;">
	<form method="post" id="booking_odp" action="/{{ Request::path() == 'booking_odp/check' ? 'booking_odp/input' : Request::path() }}">
		<div class="panel panel-info">
			<div class="panel-heading">Tambah Booking ODP</div>
			<div class="panel-body">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<div class="form-group">
					<label>Nama ODP</label>
					<input type="text" class="form-control" id="odp_nama" value="{{ implode(', ', $result['odp']) }}" readonly style="cursor: not-allowed; color: black;">
					<input type="hidden" name="odp_nama" value="{{ json_encode($result['odp'], true) }}">
				</div>
				<div class="form-group">
					<label>Mitra</label>
					<select class="form-control" id="mitra" name="mitra">
						@foreach ($mitra as $d)
							<option value="{{ $d[0] }}" {{ isset($result['mydata']['mitra']) ? 'selected' : '' }}>{{ $d[0] }} {{ isset($d[1]) ? "($d[1])" : '' }}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group">
					<label>Request</label>
					<input required type="text" class="form-control" id="req" name="req" placeholder="Masukkan Nama Request" value="{{ isset($result['mydata']['req']) ? $result['mydata']['req'] : '' }}">
				</div>
				<div class="form-group">
					<label>Site ID</label>
					<input required type="text" class="form-control" id="site_id" name="site_id" placeholder="Masukkan ID Site" value="{{ isset($result['mydata']['site_id']) ? $result['mydata']['site_id'] : '' }}">
				</div>
				<div class="form-group">
					<label>Site Name</label>
					<input required type="text" class="form-control" id="site_nm" name="site_nm" placeholder="Masukkan Nama Site" value="{{ isset($result['mydata']['site_nm']) ? $result['mydata']['site_nm'] : '' }}">
				</div>
				<div class="form-group">
					<label>Keterangan</label>
					<input required type="text" class="form-control" id="ket" name="ket" placeholder="Masukkan Nama Site" value="{{ isset($result['mydata']['ket']) ? $result['mydata']['ket'] : '' }}">
				</div>
				<div class="form-group">
					<label>Koordinat ODP</label>
					<div class="alert validate_no_koor_odp alert-danger" role="alert" style="display: none;">
						<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
						<span class="sr-only">Error:</span>
						Gunakan Format Seperti Contoh Ini, -3.327974, 114.583997
					</div>
					<input type="text" class="form-control" id="odp_koor" name="odp_koor" placeholder="Masukkan Koordinat ODP" required value="{{ isset($result['mydata']['koor']) ? $result['mydata']['koor'] : '' }}">
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-block btn-info save_me" style="text-align: center;"><span data-icon="M" class="linea-icon linea-basic fa-fw" style="font-size: 20px; color: #ffffff; "></span>&nbsp;Simpan Perubahan</button>
				</div>
			</div>
		</div>
	</form>
</div>
@endsection
@section('footerS')
<link href="https://cdn.jsdelivr.net/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/inputmask/4.0.9/jquery.inputmask.bundle.min.js" integrity="sha512-VpQwrlvKqJHKtIvpL8Zv6819FkTJyE1DoVNH0L2RLn8hUPjRjkS/bCYurZs0DX9Ybwu9oHRHdBZR9fESaq8Z8A==" crossorigin="anonymous"></script>
<script>
	$(function(){

		$("#odp_koor").inputmask();

		$("#mitra").select2({
			width: '100%',
			placeholder: 'Masukkan Nama Mitra',
		});

		$('#booking_odp').submit(function(e){
			var regex =/^[-+]?([1-8]?\d(\.\d+)?|90(\.0+)?)[,°]\s*[-+]?(180(\.0+)?|((1[0-7]\d)|([1-9]?\d))(\.\d+)?°?)$/;

			if(!regex.test($('#odp_koor').val()) && $('#odp_koor').val() !== ''){
				$('.validate_no_koor_odp').css({display: "block"});
				$('#odp_koor').css({border: "2px solid red"});
				$('#odp_koor').focus();
				e.preventDefault();
			}else{
				$('.validate_no_koor_odp').css({display: "none"});
				$('#odp_koor').css({border: ""})
			}
		});
	});
</script>
@endsection