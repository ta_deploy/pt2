@extends('layout')
@section("title", "Deep Searching Booking ODP")
@section('headerS')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.css">
@endsection
@section('content')
@section('style')
<style type="text/css">
    .select2-search {
        color:black;
    }

    .select2-results
	{
		/* background-color: #353c48; */
	}

	th, #odp_0 > tbody:nth-child(2) > tr:nth-child(1) > td:nth-child(10){
        text-align: center;
        white-space:nowrap;
        vertical-align: middle;
    }

	.panel .panel-action a
	{
        opacity: 1;
    }

	.dataTables_scrollHeadInner
	{
		width:100% !important;
	}

	.dataTables_scrollHeadInner table
	{
		width:100% !important;
	}

	#odp_0, #odp_1{
		overflow-y: scroll !important;
	}

	.dataTables_scroll
	{
		overflow: auto;
	}

	thead, tbody {
		font-size: 1em;
	}
</style>
@endsection
<div class="form-group">
	<a type="button" class="btn btn-default" href="{{URL::to('/show/booking_odp')}}"><i data-icon="&#xe00f;" class="linea-icon linea-aerrow fa-fw" style="font-size: 20px; vertical-align:middle;"></i>&nbsp;Kembali</a>
</div>
<div class="container-fluid" style="padding-top: 25px;">
	<form method="post" id="booking_odp">
		<div class="panel panel-info">
			<div class="panel-heading">Deep Searching Booking ODP dan Free ODP</div>
			<div class="panel-body">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<div class="form-group">
					<input type="text" class="form-control search" name="search" required>
				</div>
				<div class="form-group row">
					<div class="col-md-3">
						<input type="radio" id="odp" class="default_chk jenis_alp" name="type" value="odp" checked>
						<label for="odp">Pencarian Menggunakan ODP</label>
					</div>
					<div class="col-md-3">
						<input type="radio" id="alpro" class="jenis_alp" name="type" value="alpro">
						<label for="alpro">Pencarian Menggunakan Alpro</label>
					</div>
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-block btn-sm btn-info save_me" style="text-align: center;"><span data-icon="#" class="linea-icon linea-basic fa-fw" style="font-size: 20px; color: #ffffff; "></span>&nbsp;Cari</button>
				</div>
			</div>
		</div>
	</form>
	@if ($data)
		@php
			$no = 0;
		@endphp
		@foreach ($data as $key => $judul)
			<div class="panel panel-success">
				<div class="panel-heading header-date"> {!! $judul !!}
					<div class="panel-action">
						<a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a>
					</div>
				</div>
				<div class="panel-body">
					<table id="odp_{{$no++}}" cellspacing="0" style="width: 100%; height: 400px" class="table table-striped table-bordered table-responsive">
						<thead>
							@if ($key == 0)
								<tr>
									<th>#</th>
									<th>Mitra</th>
									<th>Site ID</th>
									<th>Nama Site</th>
									<th>Nama ODP</th>
									<th>Koordinat</th>
									<th>Request</th>
									<th>Tanggal Order</th>
									<th>Eksekutor</th>
									<th>Action</th>
								</tr>
							@else
							<tr>
								<td>#</td>
								<td>Tanggal Booking</td>
								<td>Nama ODP</td>
								<td>STO</td>
								<td>ODC</td>
								<td>Mitra</td>
								<td>Status</td>
								<td>Nama</td>
								<td>Keterangan</td>
							</tr>
							@endif
						</thead>
					</table>
				</div>
			</div>
		@endforeach
	@endif
</div>
@endsection
@section('footerS')
<link href="https://cdn.jsdelivr.net/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/jquery.inputmask.bundle.min.js"></script>
<script>
$(function() {
	var masking = 'ODP-AAA-A{2,3}/9{3,4}';

	$('.search').inputmask({
		mask: masking,
		clearIncomplete: true,
		greedy: false
	});

	$('.default_chk').prop('checked', true);

	$(".jenis_alp").change(function() {
		if (this.checked) {
			if ($(this).val() == 'alpro') {
				masking = 'ODP-AAA-A{2,3}';
			} else {
				masking = 'ODP-AAA-A{2,3}/9{3,4}';
			}
			$('.search').inputmask({
				mask: masking,
				clearIncomplete: true,
				greedy: false
			})
		}
	});

	var datanya = <?= json_encode($split) ?>,
	final_dta = [],
	get_dt_bo = [],
	get_dt_fo = [];

	if (datanya) {
		$.each( datanya, function( keyP, valueP ) {
			var no = 0;
			$.each( valueP, function( keyC1, valueC1) {
				if(keyP == 0){
					get_dt_bo.push([
						valueC1[0] || null,
						valueC1[1] || null,
						valueC1[2] || null,
						valueC1[3] || null,
						valueC1[4] || null,
						valueC1[6] || null,
						valueC1[8] || null,
						valueC1[11] || null,
						valueC1[17] || null,
						"<a type=\"button\" style='color: #fec109;' class=\"btn btn-light\" href='/booking_odp/edit/"+ keyC1 +"'><span data-icon=\"&#xe005;\" class=\"linea-icon linea-basic fa-fw\" style=\"font-size: 10px;\"></span>&nbsp; Edit</a><a style='color: #fec109;' href=\"#\" type=\"button\" class=\"btn btn-light delete_btn\" data-id="+ keyC1 +" data-odp="+ valueC1[4] +"><span data-icon=\"&#xe01c;\" class=\"linea-icon linea-basic fa-fw\" style=\"font-size: 10px;\"></span>&nbsp; Hapus</a>"
					]);
				}
				if(keyP == 1){
					get_dt_fo.push([
						++no,
						valueC1[5] || null,
						valueC1[0] || null,
						valueC1[1] || null,
						valueC1[2] || null,
						valueC1[8] || null,
						valueC1[3] || null,
						valueC1[6] || null,
						valueC1[4] || null
					]);
				}
			});
		});

		final_dta= {
			BO: get_dt_bo,
			FO: get_dt_fo
		};

	}

	$('#odp_0').DataTable( {
		drawCallback: function () {
        	$( 'table.import_list tbody tr td' ).css( 'padding', '5px 8px 5px 8px' );
    	},
		fixedHeader: {
			header: true,
			footer: true
		},
		order:	[
					[ 0, "desc" ]
				],
		data: final_dta.BO,
		deferRender: true,
		scrollCollapse: true,
		scroller: true,
	}).columns.adjust().draw();

	$('#odp_1').DataTable( {
		drawCallback: function () {
        	$( 'table.import_list tbody tr td' ).css( 'padding', '5px 8px 5px 8px' );
    	},
		fixedHeader: {
			header: true,
			footer: true
		},
		order:	[
					[ 0, "desc" ]
				],
		data: final_dta.FO,
		deferRender: true,
		scrollCollapse: true,
		scroller: true,
	}).columns.adjust().draw();

	$('.delete_btn').on('click', function(){
		var odp = $(this).attr('data-odp'),
		id = $(this).attr('data-id');

		Swal.fire({
			title: 'Yakin Untuk Menghapus?',
			text: "Data yang terhapus, akan masuk ke Free ODP!",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Ya, Hapus!'
		}).then((result) => {
			if (result.value) {
				$.ajax({
					type: "GET",
					url: '/delete_BO/'+ id,
					cache: false,
					beforeSend: function() {
						console.log('dang lah');
						$("#parent").toggleClass('preloader');
						$("#child").toggleClass('cssload-speeding-wheel');
						$("#parent").css({
							'background-color' : 'rgba(0,0,0,.8)'
						});
					},
					success: function(data) {
						Swal.fire(
							'Terhapus!',
							odp+' berhasil dipindahkan ke Free ODP.',
							'success'
						)
						$("#parent").toggleClass('preloader');
						$("#child").toggleClass('cssload-speeding-wheel');
						$("#parent").css({
							'background-color' : ''
						});
						location.reload();
					}
				});
			}
		});
	});
});
</script>
@endsection