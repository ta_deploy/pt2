@extends('layout')
@section("title", "Check Booking ODP")
@section('content')
@section('style')
<style type="text/css">
    .select2-search { 
        color:black;
    }
    /* .select2-results { background-color: #353c48; } */
</style>
@endsection
@if (Session::has('alerts'))
	@foreach(Session::get('alerts') as $P_alert)
		@foreach($P_alert as $alert)
			<div class="alert alert-{{ $alert['type'] }} alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> {!! $alert['text'] !!}
			</div>
		@endforeach
	@endforeach
@endif
<div class="form-group">
	<a type="button" class="btn btn-default" href="{{URL::to('/show/booking_odp')}}"><i data-icon="&#xe00f;" class="linea-icon linea-aerrow fa-fw" style="font-size: 20px; vertical-align:middle;"></i>&nbsp;Kembali</a>
</div>
<div class="container-fluid" style="padding-top: 25px;">
	<form method="post" id="booking_odp">
		<div class="panel panel-info">
			<div class="panel-heading">Check Booking ODP</div>
			<div class="panel-body">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<div class="form-group">
					<label>Nama ODP</label>
					@php
						if (session::has('value_suggest')) {
							$data_sug = session::get('value_suggest');
						} else {
							$data_sug = null;
						}
					@endphp
					<select type="text" class="form-control" id="odp_nama" name="odp_nama[]" multiple="multiple" required>
						@if ($data_sug)
							@foreach ($data_sug as $d)
								<option value="{{ $d }}" selected>{{ $d }}</option>
							@endforeach
						@endif
					</select>
					<code>*ODP yang ingin ditambahkan, harus dicek terlebih dahulu!</code>
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-block btn-info save_me" style="text-align: center;"><span data-icon="M" class="linea-icon linea-basic fa-fw" style="font-size: 20px; color: #ffffff; "></span>&nbsp;Check</button>
				</div>
			</div>
		</div>
	</form>
</div>
@endsection
@section('footerS')
<link href="https://cdn.jsdelivr.net/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/jquery.inputmask.bundle.min.js"></script>
<script>
$(function() {	
	$('#odp_nama').select2({
		width: '100%',
		tags: true,
		allowClear: true,
		minimumInputLength:14,
		placeholder: "Masukkan ODP yang Dicek"
	}).on("select2:open", function() {
		$(".select2-search__field").val('');
		$(".select2-search__field").attr('type', 'text');
		$(".select2-search__field").attr('style', 'width: 10em;');
		$(".select2-search__field").inputmask({
			mask: 'ODP-AAA-A{2,3}/9{3,4}',
			placeholder: ''
		});
	}).on("select2:close", function() {
		$(".select2-search__field").inputmask('remove');
		$(".select2-search__field").val(null);
	});
	
});
</script>
@endsection