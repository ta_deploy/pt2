<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="testinggggg">
    <meta name="author" content="asdfasdfasdf">
    <link rel="icon" type="image/png" sizes="16x16" href="/images/logo.png">
    <title>Absen</title>
    <link href="/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
    <link href="/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
    <link href="/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
    <link href="/bower_components/morrisjs/morris.css" rel="stylesheet">
    <link href="/css/animate.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">
    <link href="/css/colors/blue.css" id="theme" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <style type="text/css">
        .modal_detail_layout{
            overflow: auto;
            overflow-y: auto;
        }

        .swal2-container {
            z-index: 1999;
        }

        /* .select2-results { background-color: #353c48; } */

    </style>
</head>
<body>
    <div class="preloader">
        <div class="cssload-speeding-wheel"></div>
    </div>
    <div id="parent">
        <div id="child"></div>
    </div>
    <div id="wrapper">
      <nav class="navbar navbar-default navbar-static-top m-b-0">
          <div class="navbar-header"> <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse"><i class="ti-menu"></i></a>
              <div class="top-left-part">
                  <a class="logo" href="/"><b>
                      <img src="{{ asset('/images/logo.png/')}}" alt="home" class="dark-logo" style="width: 50px; height: 50px;" /></b>
                      <span class="hidden-xs">
                          <img src="{{ asset('/images/logo_ta.png/')}}" alt="home" class="dark-logo" style="margin-left:6px; width: 150px; height: 60px; position: absolute;"/>
                      </span>
                  </a>
              </div>
              <ul class="nav navbar-top-links navbar-left hidden-xs">
                  <li><a href="javascript:void(0)" class="open-close hidden-xs waves-effect waves-light"><i class="icon-arrow-left-circle ti-menu"></i></a></li>
              </ul>
          </div>
      </nav>
      <div id="page-wrapper">
        <div class="col-sm-12">
					<div class="panel panel-default">
						<div class="panel-heading">Screenshot</div>
              <div class="panel-body">
                <form id="validation-form" method="post" data-parsley-priority-enabled="false" novalidate enctype="multipart/form-data" autocomplete="off">
                  {{ csrf_field() }}
                  <div class="row text-center input-photos" style="margin: 20px 0">
                    @php
                      $number = 1;
                      clearstatcache();
                    @endphp
                      @php
		                    $path = '/upload/absensi/' . $data->nik. '/Foto_Kehadiran.jpg';
                      @endphp
                      <div class="col-sm-12">
                        <img src="{{ $path }}" alt="" id="img-Foto_kehadiran" style="width: 300px; height: 300px;"/>
                        <br />
                        <p>Foto Kehadiran</p>
                      </div>
                      <div class="col-sm-6">
                        <a href="/absen/appr/{{ $data->absen_id }}" style="color: rgb(82, 202, 52);" type="button" class="btn btn-default btn-block"><span data-icon="&#xe006;" class="linea-icon linea-aerrow fa-fw" style="font-size: 17px; "></span>Approve</a>
                      </div>
                      <div class="col-sm-6">
                        <a href="/absen/reject/{{ $data->absen_id }}" style="color: #CA3A34FF;" type="button" class="btn btn-default btn-block"><span data-icon="g" class="linea-icon linea-basic fa-fw" style="font-size: 17px; "></span>Reject</a>
                      </div>
                  </div>
                </form>
              </div>
						</div>
					</div>
				</div>
        <footer class="footer text-center">{{ date('Y') }} &copy;<img src="{{ asset('/images/s.png/')}}" alt="home" class="dark-logo" style="position:relative; width: 50px; height: 50px; bottom: 10px;left: 10px;" /></footer>
      </div>
</div>
<script src="/bower_components/jquery/dist/jquery.min.js"></script>
<script src="/bootstrap/dist/js/tether.min.js"></script>
<script src="/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
<script src="/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
<script src="/js/jquery.slimscroll.js"></script>
<script src="/js/waves.js"></script>
<script src="/bower_components/waypoints/lib/jquery.waypoints.js"></script>
<script src="/bower_components/counterup/jquery.counterup.min.js"></script>
<script src="/bower_components/raphael/raphael-min.js"></script>
<script src="/js/custom.min.js"></script>
<script src="/bower_components/toast-master/js/jquery.toast.js"></script>
<script src="/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
<script>
  $(function () {
    $('input[type=file]').change(function() {
      var inputEl = this;
      if (inputEl.files && inputEl.files[0]) {
          $(inputEl).parent().find('input[type=text]').val(1);
          var reader = new FileReader();
          reader.onload = function(e) {
              $(inputEl).parent().find('img').attr('src', e.target.result);
          }
          reader.readAsDataURL(inputEl.files[0]);
      }
    });

    $('.input-photos').on('click', '.btn_phto', function() {
      $(this).parent().find('input[type=file]').click();
    });
  });
</script>
</body>
</html>
