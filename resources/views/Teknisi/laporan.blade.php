@extends('layout')
@section('title', 'Update Laporan')
@section('headerS')
@section('style')
<style type="text/css">
	.input-photos img {
		width: 100px;
		height: 150px;
		margin-bottom: 5px;
	}
	.btn strong.glyphicon {
		opacity: 0;
	}
	.btn.active strong.glyphicon {
		opacity: 1;
	}
	.foto {
		border: 3px solid black;
	}
	/* .select2-results { background-color: #353c48; } */

	/* input[type=text]:disabled {
		background: #353c48;
	} */
</style>
@endsection
@section('headerS')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="/bower_components/select2-bootstrap/select2-bootstrap.css" />
@endsection
@section('content')
<div class="form-group">
	<a type="button" class="btn btn-default" href="{{URL::to('/')}}"><span class="glyphicon glyphicon-arrow-left" style="color:#000000; font-size: 20px; " id="contactG"></span>&nbsp;Kembali</a>
</div>
<div class="container-fluid">
	<form id="formbuatG" name="formbuatG" method="post" enctype="multipart/form-data">
		<div class="row">
			<div class="col-md-6">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="form-group col-md-12">
							<input type="hidden" class="form-control" id="serial" name="serial" value="{{ old('serial', $id ?? '') }}">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
						</div>
						<div class="form-group col-md-12">
							<label class="control-label" for="project_name">Nama Project</label>
							<div class="alert project_nama alert-danger" role="alert" style="display: none;">
								<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
								<span class="sr-only">Error:</span>
								Nama Project Tidak Boleh Kosong!
							</div>
							<input type="text" class="form-control" id="project_name" name="project_name" value="{{ old('serial', $data->lt_project_nama ? $data->lt_project_nama : $data->project_name) }}">
						</div>
						<div class="form-group col-md-12">
							<label class="control-label" for="status">Status</label>
							<select id="status" name="status" class="form-control" style="border: 2px solid #424a56">
								@if(is_null($data->lt_status) )
									<option value="Berangkat">Berangkat</option>
								@elseif($data->lt_status == 'Berangkat')
									<option value="Tiba">Tiba</option>
								@else
								@endif
								@if($data->lt_status && !in_array($data->lt_status, ['Berangkat', 'Tidak Sempat']))
									<option value="{{ $data->lt_status }}">{{ ($data->lt_status) }}</option>
								@endif
								<option value="On Progress">On Progress</option>
								<option value="Ogp">OGP</option>
								@if($data->lt_status != ' ')
									<option value="Kendala">Kendala</option>
									<option value="Pending">Pending</option>
									{{-- <option value="Odp Naik">ODP Naik</option> --}}
									<option value="Selesai">Selesai</option>
								@endif
							</select>
							<code class="small_note_ogp">* Hubungi Admin PT-2 Untuk Konfirmasi Port Dist Dan Feeder</code>
						</div>
						<div class="form-group col-md-12 div_detail_kendala" style="display: none;">
							<label class="control-label" for="kendala_detail">Detail Kendala</label>
							<select id="kendala_detail" name="kendala_detail" class="form-control" style="border: 2px solid #424a56">
								<option value="Distribusi Full">Distribusi Full</option>
								<option value="Feeder Reti">Feeder Reti</option>
								<option value="Core Ccd">Core Ccd</option>
								<option value="Tercover Odp Lain">Tercover Odp Lain</option>
								<option value="Kendala Jalur Tidak Ada Tiang Kosong">Kendala Jalur Tidak Ada Tiang Kosong</option>
								<option value="Kendala Cuaca Hujan">Kendala Cuaca Hujan</option>
								<option value="Cancel">Cancel</option>
								<option value="Feeder Full">Feeder Full</option>
								<option value="Olt Full">Olt Full</option>
							</select>
						</div>
						<div class="form-group col-md-12 field_odp_cover" style="display: none;">
							<label class="control-label" for="odp_cover_kendala">Nama ODP Cover</label>
							<input type="text" class="form-control" id="odp_cover_kendala" name="odp_cover_kendala" value="{{ old('odp_cover_kendala', $data->odp_cover_kendala ?? '') }}">
						</div>
						<div class="form-group col-md-12 field_core_ccd" style="display: none;">
							<label class="control-label" for="distrib_ccd">Distribusi</label>
							<input type="text" class="form-control" id="distrib_ccd" name="distrib_ccd" value="{{ old('distrib_ccd', $data->distrib_ccd ?? '') }}">
							<code>*Jika ada, masukkan dengan contoh format STO-ODC, contoh: ULI-FAY</code>
						</div>
						<div class="form-group col-md-12">
							<label class="control-label" for="nama_odp_santai">Nama ODP</label>
							<input type="text" class="form-control" id="nama_odp_santai" name="nama_odp_santai" value="{{ old('nama_odp_santai', $data->odp_nama ?? '') }}">
						</div>
						<div class="form-group col-md-12">
							<label class="control-label" for="koordinat_odp">Koordinat ODP</label>
							<div class="alert no_koor_odp alert-danger" role="alert" style="display: none;">
								<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
								<span class="sr-only">Error:</span>
								Nomor Koordinat ODP tidak Boleh
							</div>
							<input type="text" class="form-control" id="koordinat_odp" name="koordinat_odp" value="{{ old('koordinat_odp', $data->lt_koordinat_odp ? $data->odp_koor : $data->lt_koordinat_odp) }}">
						</div>
						<div class="form-group row col-md-12 col-12" style="padding-right: 0px;">
							<div class=" col-md-12">
								<div class="card">
									<div class="card-header">
										Input RFC
									</div>
									<div class="card-body">
										<div class="col-md-12">
											<span style="color: black;">*Jika Nomor RFC Tidak Muncul, Masukkan RFC Secara Lengkap Agar Dapat Dicari</span>
										</div>
										<div class="col-md-12" style="margin-bottom: 12px;">
											<h5 class="card-title">Masukkan Nomor RFC</h5>
											<div class="card-text">
												<div class="alert rfc_numbbah alert-danger" role="alert" style="display: none;">
													<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
													<span class="sr-only">Error:</span>
													Masukkan Nomor RFC!
												</div>
												<select class="rfc_number form-control" name="rfc_number[]" id="rfc_number" style="border: 2px solid #424a56;" multiple="multiple">
													@if($data->rfc_key)
														@php
															$exp = explode(',', $data->rfc_key);
														@endphp
														@foreach($exp as $x)
															<option value="{{ $x }}" selected>{{ $x }}</option>
														@endforeach
													@endif
												</select>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group col-md-12">
							<label class="control-label" for="sn_splicer">SN Splicer</label>
							<input type="text" class="form-control" id="sn_splicer" name="sn_splicer" {{ $data->id_maint == 0 ? 'disabled' : '' }} value="{{ old('sn_splicer', $data->sn_splicer ?? '') }}">
						</div>
						<div class="form-group col-md-12">
							<label class="control-label" for="catatan">Catatan</label>
							<textarea class="form-control" id="catatan" name="catatan">{{ old('catatan', $data->lt_catatan ?? '') }}</textarea>
						</div>
						<div class="form-group col-md-12">
							<label class="control-label" for="bot_valins">Balasan Bot Valins</label>
							<textarea class="form-control" id="bot_valins" name="bot_valins">{{ old('bot_valins', $data->bot_valins ?? '') }}</textarea>
						</div>
						<span style="font-size:10px;">
							{{-- <button type="button" data-toggle="modal" data-target="#material-modal" class="btn btn-sm btn-info material_button_anj">
								<span class="glyphicon glyphicon-list"></span>Material
							</button> --}}
							<div class="form-group col-md-12">
								<button type="button" data-toggle="modal" style="margin-left: 12px;" data-target="#material-modal" class="btn btn-sm btn-info material_button_anxj">
									<span class="glyphicon glyphicon-list"></span>Material Telkom
								</button>
							</div>
							<div class="form-group col-md-12">
								<ul class="list-group">
									<h3>Material Dari Pekerjaan Sebelumnya</h3>
									@if(@$load_outside_material)
										@foreach($load_outside_material as $v)
											<li class="list-group-item" style="background: yellow;">
												<span class="badge badge-primary" style="color: black;">{{ $v->qty }}</span>
												<strong style="color: black;"	>{{ $v->id_item }}</strong>
											</li>
										@endforeach
									@endif
								</ul>
							</div>
							<div class="form-group col-md-12">
								<ul id="material-list" class="list-group">
									{{-- <li class="list-group-item" v-repeat="list_T | hasQty " >
										<span class="badge badge-info" v-text="qty" style="color: black;"></span>
										<strong style="color: white;" v-text="id_item"></strong>
										<p style="color: #88ff00; font-size: 13px;" v-text="nama_item"></p>
										<div v-if="rfc.length != 0">
											<p v-text="rfc"></p>
											<p style="font-size: 13px; color: #00e5ff;">In-Tech=<span v-text="total"></span>; Pemakaian=<span v-text="qty" ></span></p>
										</div>
									</li> --}}
									<li class="list-group-item" v-repeat="list | hasQty " >
										<span class="badge badge-info" v-text="qty" style="color: black;"></span>
										<strong style="color: black;" v-text="id_item"></strong>
										<p style="color: #0ab10a; font-size: 13px;" v-text="nama_item"></p>
										<div v-if="rfc.length != 0">
											{{-- <p v-text="rfc"></p> --}}
											<p style="font-size: 13px; color: #002fff;">In-Tech=<span v-text="total"></span>; Pemakaian=<span v-text="qty" ></span></p>
										</div>
									</li>
								</ul>
							</div>
						</span>
						<div class="table-responsive">
							<caption>Log Pekerjaan</caption>
							<table id="teknisi" class="table table-bordered">
								<thead>
									<tr>
										<th>Nama</th>
										<th>Timestamp</th>
										<th>Keterangan</th>
									</tr>
								</thead>
								<tbody>
									@foreach($load_log as $v)
										<tr>
											<td>{{ $v->nama }} ({{ $v->updated_by }})</td>
											<td>{{ $v->timestamp_dispatch }}</td>
											<td>
												<ul>
													@foreach($v->keterangan as $vv)
														<li>{!! $vv !!}</li>
													@endforeach
												</ul>
											</td>
										</tr>
									@endforeach
								</tbody>
							</table>
						</div>
						<div class="table-responsive field_approved" style="display: none;">
							<div class="form-group col-md-12">
								<label class="control-label" for="catatan_admin">Catatan Admin</label>
								<textarea class="form-control" id="catatan_admin" style="border-color: red;" name="catatan_admin"></textarea>
							</div>
							<div class="form-group col-md-12 pt2_plus">
								<label class="control-label" for="auto_cad">Auto CAD</label>
								<input type="file" class="form-control" id="auto_cad" name="auto_cad">
							</div>
						</div>
					</div>
				</div>
			</div>
			<input type="hidden" name="materials" value="[]" />
			<div class="col-sm-6">
				<div class="panel panel-default">
					<div class="panel-heading">Dokumentasi</div>
					<div class="panel-body">
						<div class="row text-center input-photos" style="margin: 20px 0">
							@php
								$number = 1;
								clearstatcache();
							@endphp
							@foreach($foto as $input)
								<div class="col-6 col-sm-3">
									@php
										$check_path = "/upload/pt_2/teknisi/$id";
										$check_path_2 = "/upload/pt_2_2/teknisi/$id";
										$check_path_3 = "/upload/pt_2_3/teknisi/$id";

										if(file_exists(public_path().$check_path)){
											$path = "$check_path/$input";
										}elseif(file_exists(public_path().$check_path_2)){
											$path = "$check_path_2/$input";
										}else{
											$path = "$check_path_3/$input";
										}

										$th       = "$path-th.jpg";
										$img      = "$path.jpg";
										$nt       = "$path-catatan.txt";
										$nt_panel = "$path-panel.txt";
										$nt_port  = "$path-port.txt";
										$flag     = "";
										$name     = "flag_".$input;
									@endphp
									@if (file_exists(public_path().$th))
										@php
											$flag = 1;
											$images = $img;
											$src = $th;
											$th = $th;
										@endphp
									@else
										@php
											$images = "";
											$src = "/images/placeholder.gif";
											$th = "";
										@endphp
									@endif
									<a href="{{ $images }}">
										<img src="{{ $src }}" alt="{{ $input }}" id="img-{{ $input }}" class="photo_valid_dis" />
									</a>
									<br />
									<input type="text" class="hidden" name="flag_{{ $input }}" value="{{ $flag }}"/>
									<input type="file" class="hidden upload_foto_halal" name="photo_{{ $input }}" accept="image/jpeg" />
									<button type="button" class="btn btn-sm btn-info btn_upload_foto">
										<i class="glyphicon glyphicon-camera"></i>
									</button>
									<p>{{ str_replace('_',' ',$input) }}</p>

									@if(!in_array($input, ['Port_Feeder', 'Port_Distribusi']))

										@if (file_exists(public_path().$nt))
											@php
												$note = File::get(public_path("$path-catatan.txt"));
											@endphp
										@else
											@php $note ='' @endphp
										@endif
										<textarea class="form-control foto" id="catatan_{{$input}}" name="catatan_{{$input}}">{{ old('catatan_'.$input, $note ?? $note) }}</textarea>
									@else
										@if (@file_exists(public_path().$nt_panel))
											@if (@file_exists(public_path().$path."-panel.txt"))
												@php
													$note_panel = @File::get(@public_path("$path-panel.txt"));
												@endphp
											@endif
											@if (@file_exists(public_path().$path."-port.txt"))
												@php
													$note_port = @File::get(@public_path("$path-port.txt"));
												@endphp
											@endif
										@else
											@php
												$note_panel = $note_port = '';
											@endphp
										@endif
										<div class="form-group row" style="margin-bottom: 23px;">
											<div class="col-5 col-md-5">
												<label class="control-label pull-left" for="panel_{{$input}}">Panel</label>
											</div>
											<div class="col-7 col-md-7">
												<input type="tel" class="form-control" id="panel_{{$input}}" style="height: 25px; border: 3px solid black;" name="panel_{{$input}}" value="{{  old('panel_'.$input, $note_panel ?? $note_panel)  }}">
											</div>
										</div>
										<div class="form-group row " style="margin-bottom: 0px; margin-top: -3.56px;">
											<div class="col-5 col-md-5">
												<label class="control-label pull-left" for="port_{{$input}}">Port</label>
											</div>
											<div class="col-7 col-md-7">
												<input type="tel" class="form-control" id="port_{{$input}}" style="height: 25px; border: 3px solid black;" name="port_{{$input}}" value="{{ old('port_'.$input, $note_port ?? $note_port)  }}">
											</div>
										</div>
									@endif
									{!! $errors->first($name, '<span class="label label-danger">:message</span>') !!}
									<br />
								</div>
								<?php
									$number++;
								?>
							@endforeach
						</div>
					</div>
				</div>
			</div>
			{{-- @if(@$data->lt_status != 'Selesai') --}}
			<div class="col-sm-12">
				<div class="panel panel-default">
					<div class="panel-body field_btn_selesai">
					</div>
				</div>
			</div>
			{{-- @endif --}}
		</div>
	</form>
	<div class="modal fade" id="material-modal">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header" style="background: #eee">
					<strong style="color:black;">Laporan Material</strong>
					<button type="button" class="btn btn-default btn-xs" style="float:right;" data-dismiss="modal">Close</button>
				</div>
				<div class="modal-body" style="overflow-y:auto;height : 450px">
					<input id="searchinput" class="form-control input-sm" type="search" placeholder="Search..." />
					<ul id="searchlist" class="list-group">
						<li class="list-group-item" v-repeat="list">
							<strong v-text="id_item" class="ini_item_bujang" style="color: black;"></strong>
							<div class="input-group" style="width:150px;float:right;">
								<span class="input-group-btn">
									<button type="button" class="btn btn-default btn-sm" v-on="click: onMinus(this)">
										<span class="glyphicon glyphicon-minus"></span>
									</button>
								</span>
								<input type="tel" v-model="qty" v-on="keypress: onlyNumber($event), keyup: mtc(this, total)" style="border-top: 1px solid #000000" class="form-control text-center input-md"/>
								<span class="input-group-btn">
									<button type="button" class="btn btn-default btn-sm" v-on="click: onPlus(this, total)">
										<span class="glyphicon glyphicon-plus"></span>
									</button>
								</span>
							</div>
							<p v-text="nama_item" style="font-size:10px; color: black;"></p>
							<div v-if="rfc.length != 0">
								{{-- <p v-text="rfc" style="font-size:10px; margin-top: -10px; color: white;"></p> --}}
								<p style="font-size:10px; color: white;">In-Tech=<span v-text="total"></span>; Pemakaian=<span v-text="qty" ></span></p>
							</div>
						</li>
					</ul>
				</div>
				<div class="modal-footer" style="background: #eee; color: black;">
					Limit pemakaian Material diatas sesuai dengan BON di alista.
				</div>
			</div>
		</div>
	</div>
	{{-- <div class="modal fade" id="material-modal-T">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header" style="background: #eee">
					<strong style="color:black;">Laporan Material Telkom</strong>
					<button type="button" class="btn btn-default btn-xs" style="float:right;" data-dismiss="modal">Close</button>
				</div>
				<div class="modal-body" style="overflow-y:auto;height : 450px">
					<input id="searchinput_T" class="form-control input-sm" type="search" placeholder="Search..." />
					<ul id="searchlist" class="list-group">
						<li class="list-group-item" v-repeat="list_T">
							<strong v-text="id_item" class="ini_item_bujang" style="color: white;"></strong>
							<div class="input-group" style="width:150px;float:right;">
								<span class="input-group-btn">
									<button type="button" class="btn btn-default btn-sm" v-on="click: onMinus(this)">
										<span class="glyphicon glyphicon-minus"></span>
									</button>
								</span>
								<input type="tel" v-model="qty" v-on="keypress: onlyNumber($event), keyup: mtc(this, total)" style="border-top: 1px solid #eeeeee" class="form-control text-center input-md"/>
								<span class="input-group-btn">
									<button type="button" class="btn btn-default btn-sm" v-on="click: onPlus(this, total)">
										<span class="glyphicon glyphicon-plus"></span>
									</button>
								</span>
							</div>
							<p v-text="nama_item" style="font-size:10px; color: white;"></p>
							<div v-if="rfc.length != 0">
								<p v-text="rfc" style="font-size:10px; margin-top: -10px; color: white;"></p>
								<p style="font-size:10px; color: white;">In-Tech=<span v-text="total"></span>; Pemakaian=<span v-text="qty" ></span></p>
							</div>
						</li>
					</ul>
				</div>
				<div class="modal-footer" style="background: #eee; color: black;">
					Limit pemakaian Material diatas sesuai dengan BON di alista.
				</div>
			</div>
		</div>
	</div> --}}
</div>
@endsection
@section('footerS')
<script src="/bower_components/bootstrap-list-filter/bootstrap-list-filter.min.js"></script>
<script src="/bower_components/vue/dist/vue.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.full.min.js"></script>
<script>
	$(function(){
		var	material_loads = {!! json_encode($material_load) !!},
		materials,
		reliable = {!! json_encode($materials) !!},
		vueData = {
			list: materials
		}

		/*var data_rfc ='';

		$('body').on('keyup', '.rfc_hasil .select2-search__field',  function() {
			data_rfc =  $(this).val();
		});

		function callback(response) {
			if(response.length == 1){
				data_rfc= '';
			}
		}*/

		/*$('.rfc_number').on("select2:open", function(e) {
			// Dropdown = single, Selection = multiple
			var $search = $('.rfc_number').data('select2').dropdown.$search;
			$search.val(data_rfc);
			if(data_rfc.length >= 18){
				console.log('more than 18')
				var e = jQuery.Event("keyup");
				e.keyCode = e.which = 50;
				$('.rfc_hasil .select2-search__field').trigger('keypress');
			}
		});*/

		$('.rfc_number').select2({
			width: '100%',
			placeholder: "Masukkan Nomor RFC",
			allowClear: true,
			tags: true,
			ajax: {
				url: "/teknisi/find/rfc",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						searchTerm: params.term
					};
				},
				processResults: function (response) {
					return {
						results: response
					};
				},
				cache: true,
				success: function(value) {
					// callback(value);
				}
			}
		});

		// $('.rfc_number').change(function() {
		// 	if($(this).val() !== null){
		// 		$.ajax({
		// 			url: "/teknisi/rfc/item",
		// 			data: {data: $(this).val()},
		// 			success: function(data) {
		// 				var objects = {},
		// 				array = [];
		// 				$.each( data, function( key, val ) {
		// 					objects[key] = {qty: val.jumlah, id_item: val.id_barang, no_rfc: val.no_rfc};
		// 				});
		// 				array = objects;
		// 				material_loads = array;
		// 				r_material();
		// 			},
		// 			error: function(e){
		// 				/*console.log(e);*/
		// 			}
		// 		});
		// 	}
		// });

		// $("#rfc_number").append('<option value="5">Twitter</option>');
		// $("#rfc_number").append('<option value="6">Twittessr</option>');
		// $("#rfc_number").val(['5', '6']).trigger('change');

		function r_material(){
			var lepas_rfc=[],
			lepas_save=[],
			lepas_sisa=[],
			materials_raw = $.each( reliable, function( raw_key, raw_value ) {
				$.each( material_loads, function( final_key, final_value ) {
					/*mencocokan nilai jika dua2nya sama nama barang (jaga2 bukan dari rfc)*/
					/*if(raw_value.id_item != final_value.id_item){
						raw_value.total = raw_value.qty
					}*/
					/*mencocokan nomor rfc jika dua barang sama*/
					if(raw_value.id_item == final_value.id_item){
						raw_value.qty = final_value.qty
						raw_value.rfc = final_value.rfc_key
						// raw_value.total = final_value.qty
					}
				});
			});

			$.each( materials_raw, function( tes, data ) {
				if(data.rfc !== null){
					lepas_rfc.push(data);
				}

				if(data.qty != 0 && data.rfc === null){
					lepas_save.push(data);
				}

				if(data.qty == 0 && data.rfc === null){
					lepas_sisa.push(data);
				}
			});

			if(lepas_rfc.length != 0){
				materials = lepas_rfc.concat(lepas_save,lepas_sisa);
			}else{
				materials = lepas_sisa.concat(lepas_save);
			}
			vueData.list = materials;
		}

    r_material();

    var usedNames = {};
    $("select[name='status'] > option").each(function () {
			if (usedNames[this.value]) {
				$(this).remove();
			} else {
				usedNames[this.value] = this.text;
			}
    });

    $('.upload_foto_halal').change(function() {
			/*console.log(this.name);*/
			var inputEl = this;
			if (inputEl.files && inputEl.files[0]) {
				/*$(inputEl).parent().find('input[type=text]').val(1);*/
				var reader = new FileReader();
				reader.onload = function(e) {
					$(inputEl).parent().find('img').attr('src', e.target.result);
				}
				reader.readAsDataURL(inputEl.files[0]);
			}
    });

    // console.log(materials);


    Vue.filter('hasQty', function(value) {
			return value.filter(function(a) {
				return a.qty > 0
			});
    });

    var listVm = new Vue({
			el: '#material-list',
			data: vueData
    });

    var modalVm = new Vue({
			el: '#material-modal',
			data: vueData,
			methods: {
				onPlus: function(item, a) {
					if (item.qty >= a)
						item.qty = a;
					else
						item.qty++;
				},
				onMinus: function(item) {
					if (item.qty <= 0)
						item.qty = 0;
					else
						item.qty--;
				},
				onlyNumber ($event) {
					let keyCode = ($event.keyCode ? $event.keyCode : $event.which);
					console.log($event)
					if ((keyCode < 48 || keyCode > 57) && keyCode != 8) {
						$event.preventDefault();
					}
				},
				mtc (item, total){
					if(item.qty > total){
						item.qty = total;
					}
				}
			}
    });

    // var modalVm1 = new Vue({
    //     el: '#material-modal-T',
    //     data: vueData,
    //     methods: {
    //         onPlus: function(item, a) {
    //             if (item.qty >= a)
    //                 item.qty = a;
    //             else
    //                 item.qty++;
    //         },
    //         onMinus: function(item) {
    //             if (item.qty <= 0)
    //                 item.qty = 0;
    //             else
    //                 item.qty--;
    //         },
    //         onlyNumber ($event) {
    //             let keyCode = ($event.keyCode ? $event.keyCode : $event.which);
    //             console.log($event)
    //             if ((keyCode < 48 || keyCode > 57) && keyCode != 8) {
    //                 $event.preventDefault();
    //             }
    //         },
    //         mtc (item, total){
    //             if(item.qty > total){
    //                 item.qty = total;
    //             }
    //         }
    //     }
    // });



		$('#formbuatG').submit(function() {
			var result = [];
			materials.forEach(function(item) {
				if (item.qty > 0) result.push({id_item: item.id_item, qty: item.qty});
			});
			$('input[name=materials]').val(JSON.stringify(result));
		});

		$('.input-photos').on('click', 'button', function() {
			$(this).parent().find('input[type=file]').click();
		});

		$("#searchinput").keyup(function() {
			setTimeout(function(){
				var input, filter, ul, li, a, i;
				input = document.getElementById("searchinput");
				filter = input.value.toUpperCase();
				ul = document.getElementById("searchlist");
				li = ul.getElementsByTagName("li");
				for (i = 0; i < li.length; i++) {
					a = li[i].getElementsByClassName("ini_item_bujang")[0];
					console.log(a.innerHTML);
					if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
						li[i].style.display = "";
					} else {
						li[i].style.display = "none";
					}
				}
			}, 1000);
		},);

		// $("#searchinput_T").keyup(function() {
		// 	setTimeout(function(){
		// 		var input, filter, ul, li, a, i;
		// 		input = document.getElementById("searchinput_T");
		// 		filter = input.value.toUpperCase();
		// 		ul = document.getElementById("searchlist");
		// 		li = ul.getElementsByTagName("li");
		// 		for (i = 0; i < li.length; i++) {
		// 			a = li[i].getElementsByClassName("ini_item_bujang")[0];
		// 			console.log(a.innerHTML);
		// 			if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
		// 				li[i].style.display = "";
		// 			} else {
		// 				li[i].style.display = "none";

		// 			}
		// 		}
		// 	}, 1000);
		// },);

		$('#kendala_detail').val('').trigger('change');
		$('#status').val('On Progress').change()

		$('#status').select2({
			placeholder: 'Silahkan Pilih Status',
			width: '100%',
		});

		$('#kendala_detail').select2({ width: '100%' });

		var data = {!! json_encode($data) !!};

		if(!data.lt_status){
			$('#status').val(null).change()
		}

		if(data.rfc_key != null && !data.rfc_key){
			var data_load_rfc = data.rfc_key.split(',');
			$('#rfc_number').val(data_load_rfc).change();
		}

		$('#kendala_detail').on('change', function(){
			var isi = $(this).val();

			if(isi == 'Tercover Odp Lain'){
				$('.field_odp_cover').show();
				$('#odp_cover_kendala').attr('required', 'required');
			}else{
				$('.field_odp_cover').hide();
				$('#odp_cover_kendala').removeAttr('required');
			}

			if(isi == 'Core Ccd'){
				$('.field_core_ccd').show();
				$('#distrib_ccd').attr('required', 'required');
			}else{
				$('.field_odp_cover').hide();
				$('#distrib_ccd').removeAttr('required');
			}
		})

		if(data.lt_status){
			$('#status').val(data.lt_status).change()
		}

		if(data.lt_status == 'Kendala'){
			$('.div_detail_kendala').css({'display': 'block'});
			$('#kendala_detail').val('Distribusi Full').trigger('change');
		}

		if(data.lt_status == 'Selesai'){
			$('input, select, textarea').attr('disabled', true);
			$('.material_button_anj, .btn_upload_foto').css({
				display: 'none'
			});

			if($.inArray({!! session('auth')->pt2_level !!}, [2, 5]) !== -1){
				$('.field_approved').show();

				$('html, body').animate({
					scrollTop: $("#catatan_admin").offset().top
				}, 2000);

				$(".pt2_plus > :input, #catatan_admin, input[name='_token']").removeAttr('disabled');

				if(data.jenis_wo == 'PT-2 PLUS'){
					$('.pt2_plus').show();
				}else{
					$('.pt2_plus').show();
				}

				$('.field_btn_selesai').html('<div class="form-group col-md-6"><button type="submit" name="btn_isi" value="submit_admin" class="btn save_me btn-primary" style="text-align: center">Approved</button><button type="submit" name="btn_isi" value="reject_admin" class="btn save_me btn-warning" style="text-align: center; margin-left: 10px;">Reject</button></div>')
			}

		}else{
			$('.field_btn_selesai').html('<div class="form-group col-md-12"><button type="submit" class="btn save_me btn-primary" style="text-align: center">Simpan Laporan</button></div>')
		}

		function load_status(value){
			if(value == 'Kendala'){
				$('.div_detail_kendala').css({'display': 'block'});
				$('#kendala_detail').val('Distribusi Full').trigger('change');
			}else{
				$('.div_detail_kendala').css({'display': 'none'});
				$('#kendala_detail').val(' ').trigger('change');
			}

			if(value == 'Ogp'){
				$('.small_note_ogp').show();
			}else{
				$('.small_note_ogp').hide();
			}

			if(value == 'Selesai'){
				$('#bot_valins').attr('required', 'required');
			}else{
				$('#bot_valins').removeAttr('required');
			}
		}

		load_status($('#status').val() )

		$('#status').on('change', function(){
			load_status($(this).val() )
		});

		$('.save_me').click(function(e){
			var project_nama = $.trim($('#project_name').val() ),
			rfc_numbah = $.trim($('#rfc_number').val()),
			No_koor_odp = $.trim($('#koordinat_odp').val() ),
			serial = $.trim($('#serial').val() ),
			status=$.trim($('select[name="status"]').val() ),
			form = $('#formbuatG')[0],
			result = [];
			/*console.log(status);*/
			// materialsT.forEach(function(item) {
			// 	if (item.qty > 0) result.push({id_item: item.id_item, qty: item.qty});
			// });

			materials.forEach(function(items) {
				if (items.qty > 0) result.push({id_item: items.id_item, qty: items.qty});
			});

			$('input[name=materials]').val(JSON.stringify(result));

			/*console.log(photo_feeder);*/

			if (project_nama === '') {
				$('.project_nama').css({display: "block"});
				$('#project_name').css({border: "2px solid red"});
				$('#project_name').focus();
			}else{
				$('.project_nama').css({display: "none"});
				$('#project_nama').css({border: ""});
			}

			if (No_koor_odp === '') {
				$('.no_koor_odp').css({display: "block"});
				$('#koordinat_odp').css({border: "2px solid red"});
				$('#koordinat_odp').focus();
			}else{
				$('.no_koor_odp').css({display: "none"});
				$('#koordinat_odp').css({border: ""});
			}

			var count_v_u = 0,
			count_v = 0;

			if (status != 'Selesai' && status != 'SELESAI' && status != 'selesai'){
				if(status == 'Ogp'){
					var array_undone_submit_photo = ['Port_Distribusi', 'Port_Feeder', 'ODC', 'Progress'];
				}else{
					var array_undone_submit_photo = ['Port_Distribusi', 'ODC', 'Progress'];
				}

				$.each(array_undone_submit_photo, function( index, nama_foto_undone ) {
					var replaces = nama_foto_undone.replace("_", " ");
					if(($("#img-"+nama_foto_undone).attr('src')) == '/images/placeholder.gif' ){
						$.toast({
							heading: 'Foto '+replaces+' Kosong!',
							text: 'Silahkan Masukkan Foto '+replaces+' !',
							position: 'top-right',
							stack: false,
							icon: 'error',
							stack: 4,
							hideAfter: 10000,
							showHideTransition: 'slide'
						});
						$("#img-"+nama_foto_undone).css({border: "2px solid red"});
						$("#img-"+nama_foto_undone).focus();
						count_v_u++;
						return false;

					}else{
						$("#img-"+nama_foto_undone).css({border: ""});
					}

					if($.inArray(nama_foto_undone, ['Port_Feeder', 'Port_Distribusi']) == -1) {
						if($('input[name="port_'+nama_foto+'"]').val() == ''){
							$.toast({
								heading: 'Port '+replaces+' Kosong!',
								text: 'Silahkan Masukkan Port '+replaces+' !',
								position: 'top-right',
								stack: false,
								icon: 'error',
								stack: 4,
								hideAfter: 10000,
								showHideTransition: 'slide'
							});

							$('input[name="port_'+nama_foto+'"]').css({border: "2px solid red"});
							$('input[name="port_'+nama_foto+'"]').focus();
							count_v++;
							return false;
						}else{
							$('input[name="port_'+nama_foto+'"]').css({border: ""});
						}

						// if($('input[name="panel_'+nama_foto+'"]').val() == ''){
						// 	$.toast({
						// 		heading: 'Panel '+replaces+' Kosong!',
						// 		text: 'Silahkan Masukkan Panel '+replaces+' !',
						// 		position: 'top-right',
						// 		stack: false,
						// 		icon: 'error',
						// 		stack: 4,
						// 		hideAfter: 10000,
						// 		showHideTransition: 'slide'
						// 	});
						// 	$('input[name="panel_'+nama_foto+'"]').css({border: "2px solid red"});
						// 	$('input[name="panel_'+nama_foto+'"]').focus();
						// 	count_v++;
						// }else{
						// 	$('input[name="panel_'+nama_foto+'"]').css({border: ""});
						// }
					}else{
						if($('textarea[name="catatan_'+nama_foto_undone+'"]').val() == ''){
							$.toast({
								heading: 'Catatan '+replaces+' Kosong!',
								text: 'Silahkan Masukkan Catatan '+replaces+' !',
								position: 'top-right',
								stack: false,
								icon: 'error',
								stack: 4,
								hideAfter: 10000,
								showHideTransition: 'slide'
							});
							$('textarea[name="catatan_'+nama_foto_undone+'"]').css({border: "2px solid red"});
							$('textarea[name="catatan_'+nama_foto_undone+'"]').focus();
							count_v_u++;
							return false;
						}else{
							$('textarea[name="catatan_'+nama_foto_undone+'"]').css({border: ""});
						}
					}
				});
			}

			if (data.lt_status != 'Selesai' && (status =='Selesai' || status == 'SELESAI' || status == 'selesai'|| status == 'UpV') ){
				/*var ext = $('#upload_rfc').val().split('.').pop().toLowerCase();
				if($.inArray(ext, ['pdf']) == -1) {
					$.toast({
						heading: 'Error!',
						text: 'Data Harus Berupa PDF!',
						position: 'top-right',
						stack: false,
						icon: 'error',
						hideAfter: 10000,
						showHideTransition: 'slide'
					})
				}*/

				if (rfc_numbah === '' ) {
					$('.rfc_numbbah').css({display: "block"});
					$('#rfc_number').css({border: "2px solid red"});
					$('#rfc_number').focus();
					return false;
				}else{
					$('.rfc_numbbah').css({display: "none"});
					$('#rfc_number').css({border: ""});
				}

				if($('input[name=materials]').val().length  == 2){
					$.toast({
						heading: 'Material Kosong!',
						text: 'Material Kosong!',
						position: 'top-right',
						stack: false,
						icon: 'error',
						hideAfter: 10000,
						showHideTransition: 'slide'
					})
					$('.material_button_anj').focus();
					return false;
				}

				if (status == 'UpV'){
					/*console.log(status);*/
					for(var i = 1; i < 4; i++){
						if (($('#img-Qc_'+i).attr('src')) == '/images/placeholder.gif'){
							$.toast({
								heading: 'Foto QC '+i+' Kosong!',
								text: 'Silahkan Masukkan QC '+i+' dan Beri Catatan!',
								position: 'top-right',
								stack: false,
								icon: 'error',
								hideAfter: 10000,
								stack: 4,
								showHideTransition: 'slide'
							})
							$('#img-Qc_'+i).focus();
							count_v++;
						}

						if($('textarea[name="catatan_Qc_'+i+'"]').val() == ''){
							$.toast({
								heading: 'Catatan QC '+i+' Kosong!',
								text: 'Silahkan Masukkan Catatan QC '+i+' !',
								position: 'top-right',
								stack: false,
								icon: 'error',
								stack: 4,
								hideAfter: 10000,
								showHideTransition: 'slide'
							});
							$('textarea[name="catatan_Qc_'+i+'"]').css({border: "2px solid red"});
							$('textarea[name="catatan_Qc_'+i+'"]').focus();
							count_v++;
						}else{
							$('textarea[name="catatan_'+i+'"]').css({border: ""});
						}
					}
				}

				var array_done = ['Port_Feeder', 'ODP', 'QRcode_Tiang', 'QRcode_ODP', 'QRcode_SPL', 'Port_Distribusi', 'OLT', 'FTM_2', 'O_side', 'E_side', 'ODC', 'Photo_Rfc', 'Qc_1_(Jarak_Jauh_ODP_Depan)', 'Qc_2_(Jarak_Jauh_ODP_Samping)', 'Qc_3_(Aksesoris)', 'Jarak_Jauh_ODP_Samping', 'Aksesoris', 'Photo_Rfc_2', 'Redaman_IN_ODP', 'Redaman_OUT_ODP', 'Closure_Sambung', 'Action'];

				$.each(array_done, function( index, nama_foto ) {
					var replaces = nama_foto.replace("_", " ");
					if(($("#img-"+nama_foto).attr('src')) == '/images/placeholder.gif' ){
						$.toast({
							heading: 'Foto '+replaces+' Kosong!',
							text: 'Silahkan Masukkan Foto '+replaces+' !',
							position: 'top-right',
							stack: false,
							icon: 'error',
							stack: 4,
							hideAfter: 10000,
							showHideTransition: 'slide'
						});
						$("#img-"+nama_foto).css({border: "2px solid red"});
						$("#img-"+nama_foto).focus();
						count_v++;
					}else{
						$("#img-"+nama_foto).css({border: ""});
					}

					if(nama_foto == 'Port_Feeder'){
						if($('input[name="port_'+nama_foto+'"]').val() == ''){
							$.toast({
								heading: 'Port '+replaces+' Kosong!',
								text: 'Silahkan Masukkan Port '+replaces+' !',
								position: 'top-right',
								stack: false,
								icon: 'error',
								stack: 4,
								hideAfter: 10000,
								showHideTransition: 'slide'
							});
							$('input[name="port_'+nama_foto+'"]').css({border: "2px solid red"});
							$('input[name="port_'+nama_foto+'"]').focus();
							count_v++;
						}else{
							$('input[name="port_'+nama_foto+'"]').css({border: ""});
						}

						if($('input[name="panel_'+nama_foto+'"]').val() == ''){
							$.toast({
								heading: 'Panel '+replaces+' Kosong!',
								text: 'Silahkan Masukkan Panel '+replaces+' !',
								position: 'top-right',
								stack: false,
								icon: 'error',
								stack: 4,
								hideAfter: 10000,
								showHideTransition: 'slide'
							});
							$('input[name="panel_'+nama_foto+'"]').css({border: "2px solid red"});
							$('input[name="panel_'+nama_foto+'"]').focus();
							count_v++;
						}else{
							$('input[name="panel_'+nama_foto+'"]').css({border: ""});
						}
					}else{
						if($('textarea[name="catatan_'+nama_foto+'"]').val() == ''){
							$.toast({
								heading: 'Catatan '+replaces+' Kosong!',
								text: 'Silahkan Masukkan Catatan '+replaces+' !',
								position: 'top-right',
								stack: false,
								icon: 'error',
								stack: 4,
								hideAfter: 10000,
								showHideTransition: 'slide'
							});
							$('textarea[name="catatan_'+nama_foto+'"]').css({border: "2px solid red"});
							$('textarea[name="catatan_'+nama_foto+'"]').focus();
							count_v++;
						}else{
							$('textarea[name="catatan_'+nama_foto+'"]').css({border: ""});
						}
					}
				});
			}

			if (project_nama === '' || No_koor_odp === '' || count_v > 0 || count_v_u > 0){
				return false;
			}else{
				$("#parent").toggleClass('preloader');
				$("#child").toggleClass('cssload-speeding-wheel');
				$("#parent").css({
					'background-color' : 'rgba(0,0,0,.8)',
					'z-index' : '300'
				});

				var notif = $.toast({
					position: 'mid-center',
					showHideTransition: 'plain',
					hideAfter: false
				});

				notif.update({
					heading: 'Pemberitahuan',
					text: 'Project Telah Berhasil Disimpan!',
					position: 'top-center',
					icon: 'success',
					showHideTransition: 'plain',
					stack: false
				});
				/*$.ajax({
					type: 'POST',
					enctype: 'multipart/form-data',
					url : "/laporan/"+serial,
					beforeSend: function(){
						notif.update({
							heading: 'Pemberitahuan',
							text: 'Sedang Disimpan!',
							position: 'mid-center',
							icon: 'info',
							showHideTransition: 'plain',
							stack: false
						});
					},
					data : new FormData(form),
					contentType: false,
					cache: false,
					processData: false,
					success: function(data) {
						notif.update({
							heading: 'Pemberitahuan',
							text: 'Project Telah Berhasil Disimpan!',
							position: 'mid-center',
							icon: 'success',
							showHideTransition: 'plain',
							stack: false
						});
						window.location = "/";
					},
					error: function(e){
						var exception = ( typeof object !== "undefined" && e.responseJSON.exception ) ? e.responseJSON.exception : '';
						var file = ( typeof object !== "undefined" && e.responseJSON.file ) ? e.responseJSON.file : '';
						var line = ( typeof object !== "undefined" && e.responseJSON.line ) ? e.responseJSON.line : '';
						var message = ( typeof object !== "undefined" && e.responseJSON.message ) ? e.responseJSON.message : '';
						var status_error = e.status ? e.status : '' ;
						var link = window.location.href ;

						$.ajax({
							type: 'POST',
							url: "/send/err",
							data: {
								"_token": "",
								"exception" : exception,
								"file" : file,
								"line" : line,
								"message" : message,
								"status_error" : status_error,
								"link" : link
							},
							success: function (data) {
							},
							error: function(e){
							}
						});
						notif.update({
							heading: 'Error',
							text: 'Data Kesalahan Telah Dikirim. Untuk Lebih Lanjut, Klik link <a href="https://t.me/RendyL">Ini</a>',
							showHideTransition: 'fade',
							icon: 'error',
							showHideTransition: 'plain'
						});
					}
				});*/
			}
		});
	});
</script>
@endsection