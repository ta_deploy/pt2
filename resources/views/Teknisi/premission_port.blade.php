@extends('layout')
@section('title', 'Permission Port')
@section('headerS')
@section('style')
<style type="text/css">
.input-photos img {
	width: 100px;
	height: 150px;
	margin-bottom: 5px;
}
.btn strong.glyphicon {         
	opacity: 0;       
}
.btn.active strong.glyphicon {        
	opacity: 1;       
}
.foto {
	border: 3px solid white;
}
</style>
@endsection
@section('headerS')
@endsection
@section('content')
<div class="form-group">
	<a type="button" class="btn btn-default" href="{{URL::to('/')}}"><span class="glyphicon glyphicon-arrow-left" style="color:#000000; font-size: 20px; " id="contactG"></span>&nbsp;Kembali</a>
</div>
<div class="container-fluid">
	<form id="formbuatG" name="formbuatG" method="post" enctype="multipart/form-data">
		<div class="row">
			<div class="panel panel-default">
				<div class="panel-body">
					<input type="hidden" class="form-control" id="serial" name="serial" value="{{$id}}">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">	
					<div class="col-sm-12">
						<div class="panel panel-default">
							<div class="panel-heading">Foto Port</div>
							<div class="panel-body">
								<div class="row text-center input-photos" style="margin: 20px 0">
									@php
										$number = 1;
										clearstatcache();
									@endphp
									@foreach($foto as $input)
									<div class="col-6">
										@php
										$path = "/upload/pt_2/teknisi/{$id}/$input";
										$th   = "$path-th.jpg";
										$img  = "$path.jpg";
										$nt = "$path-catatan.txt";
										$flag = "";
										$name = "flag_".$input;
										@endphp
										@if (file_exists(public_path().$th))
										<a href="{{ $img }}">
											<img src="{{ $th }}" alt="{{ $input }}" id="img-{{$th}}"/>
										</a>
										@php
										$flag = 1;
										@endphp
										@else
										<img src="/images/placeholder.gif" alt="" id="img-{{ $input }}"/>
										@endif
										<br />
										<input type="text" class="hidden" name="flag_{{ $input }}" value="{{ $flag }}"/>
										<input type="file" class="hidden" name="photo-{{ $input }}" accept="image/jpeg" />
										<button type="button" class="btn btn-sm btn-info">
											<i class="glyphicon glyphicon-camera"></i>
										</button>
										<p>{{ str_replace('_',' ',$input) }}</p>
										@if (file_exists(public_path().$nt))
										@php
											$note = File::get(public_path("$path-catatan.txt"));
										@endphp
										<textarea class="form-control foto" id="catatan" name="catatan_{{$input}}">{{$note}}</textarea>
										@else
										<textarea class="form-control foto" id="catatan" name="catatan_{{$input}}"></textarea>
										@endif
										{!! $errors->first($name, '<span class="label label-danger">:message</span>') !!}
										<br />
									</div>
									@php
										$number++;										
									@endphp
									@endforeach
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-12">
						<div class="panel panel-default">
							<div class="panel-body">
								<div class="form-group col-md-12">
									<button type="submit" class="btn btn-block save_me btn-primary" style="text-align: center;">Submit Data</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
@endsection
@section('footerS')
<script>
	$(function(){
		$('.input-photos').on('click', 'button', function() {
			$(this).parent().find('input[type=file]').click();
		});
		$('input[type=file]').change(function() {
			/*console.log(this.name);*/
			var inputEl = this;
			if (inputEl.files && inputEl.files[0]) {
				$(inputEl).parent().find('input[type=text]').val(1);
				var reader = new FileReader();
				reader.onload = function(e) {
					$(inputEl).parent().find('img').attr('src', e.target.result);

				}
				reader.readAsDataURL(inputEl.files[0]);
			}
		});
	});

	$('.save_me').click(function(e){
		e.preventDefault();
		var photo_feeder = $("#img-Port_Feeder").attr('src'),
		photo_distrib = $("#img-Port_Distribusi").attr('src');

		if((photo_distrib) == '/images/placeholder.gif' || $('textarea[name="catatan_Port_Distribusi"]').val() == ''){
			$.toast({
				heading: 'Foto Distribusi Kosong!',
				text: 'Silahkan Masukkan Foto Distribusi dan Beri Catatan!',
				position: 'top-right',
				stack: false,
				icon: 'error',
				stack: 4,
				hideAfter: 10000,
				showHideTransition: 'slide'
			})
			$('textarea[name="catatan_Port_Distribusi"]').css({border: "2px solid red"});
			$('textarea[name="catatan_Port_Distribusi"]').focus();
		}else{
			$('textarea[name="catatan_Port_Distribusi"]').css({border: ""});
		}

		if ((photo_feeder) == '/images/placeholder.gif' || $('textarea[name="catatan_Port_Feeder"]').val() == ''){
			$.toast({
				heading: 'Foto Feeder Kosong!',
				text: 'Silahkan Masukkan Foto Feeder dan Beri Catatan!',
				position: 'top-right',
				stack: false,
				icon: 'error',
				hideAfter: 10000,
				stack: 4,
				showHideTransition: 'slide'
			})
			$('textarea[name="catatan_Port_Feeder"]').css({border: "2px solid red"});
			$('textarea[name="catatan_Port_Feeder"]').focus();
		}else{
			$('textarea[name="catatan_Port_Feeder"]').css({border: ""});
		}

		if((photo_distrib) == '/images/placeholder.gif' || $('textarea[name="catatan_Port_Distribusi"]').val() == '' (photo_feeder) == '/images/placeholder.gif' || $('textarea[name="catatan_Port_Feeder"]').val() == '' ){
			return false;
		}
		var notif=$.toast({
			position: 'mid-center',
			showHideTransition: 'plain',
			hideAfter: false
		});
		$.ajax({
			type: 'POST',
			url : "/laporan/"+serial,
			beforeSend: function(){
				notif.update({
					heading: 'Pemberitahuan',
					text: 'Sedang Disimpan!',
					position: 'mid-center',
					icon: 'info',
					showHideTransition: 'plain',
					stack: false
				});							
			},
			data : new FormData(form),
			contentType: false,
			cache: false,
			processData: false,
			success: function(data) {
				notif.update({
					heading: 'Pemberitahuan',
					text: 'Project Telah Berhasil Disimpan!',
					position: 'mid-center',
					icon: 'success',
					showHideTransition: 'plain',
					stack: false
				});
				window.location = "/";
				/*console.log(data);*/
			},
			error: function(e){
				var exception = e.responseJSON.exception;
				var file = e.responseJSON.file;
				var line = e.responseJSON.line;
				var message = e.responseJSON.message;
				/*console.log(e);*/
				$.ajax({
					type: 'POST',
					url: "/send/err/",
					data: {
						"_token": "{{ csrf_token() }}",
						"exception" : exception,
						"file" : file,
						"line" : line,
						"message" : message
					},
					success: function (data) {
						/*console.log(data);*/
					}
				});
				notif.update({
					heading: 'Error',
					text: 'Data Kesalahan Telah Dikirim. Untuk Lebih Lanjut Hubungi <a href="https://t.me/RendyL">TOMMAN</a>',
					showHideTransition: 'fade',
					icon: 'error',
					showHideTransition: 'plain',
					allowToastClose: false
				});
			}
		});	
	});
</script>
@endsection