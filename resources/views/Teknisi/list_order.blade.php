@extends('layout')
@section('title', 'List Teknisi')
@section('headerS')
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="/bower_components/footable/css/footable.core.css" rel="stylesheet">
<link href="/bower_components/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link rel="stylesheet" href="/bower_components/jPages-master/css/animate.css">
<link rel="stylesheet" href="/bower_components/jPages-master/css/jPages.css">

@endsection
@section('style')
<style type="text/css">
    .btn-light{
        white-space: pre-wrap; /* css-3 */
        white-space: -moz-pre-wrap; /* Mozilla, since 1999 */
        white-space: -pre-wrap; /* Opera 4-6 */
        white-space: -o-pre-wrap; /* Opera 7 */
        word-wrap: break-word; /* Internet Explorer 5.5+ */
    }

    @media only screen and (max-device-width: 430px) {
        ::placeholder {
            color: peachpuff;
            font-size: 11.8px;
        }
    }

    .label-green{
        background-color: #66A955FF;
    }

    .holder {
        display: inline-block;
    }

    .holder a, .holder span {
        color: black;
        float: left;
        font-size: 13px;
        padding: 8px 16px;
        text-decoration: none;
        transition: background-color .3s;
    }

    .holder a.jp-current {
        background-color: #586069;
        color: white;
        border-radius: 5px;
    }

    .holder a, .holder span {
        background-color: #4a5056;
        color: white;
        border-radius: 5px;
    }

    .holder span {
        font-family: "Times New Roman", Times, serif;
    }

    .holder a:hover {
        background-color: #4a5056;
        border-radius: 5px;
    }

    .holder a:first-child {
        border-top-left-radius: 5px;
        border-bottom-left-radius: 5px;
    }

    .holder a:last-child {
        border-top-right-radius: 5px;
        border-bottom-right-radius: 5px;
    }

</style>
@endsection
@section('content')
<div class="container-fluid" style="padding-top: 25px;">
    @if (Session::has('alerts_tele'))
      @foreach(Session::get('alerts_tele') as $alert)
        <div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
      @endforeach
    @endif
    <div class="panel panel-warning">
        <div class="panel-heading">List Teknisi</div>
        <div class="panel-body">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#menupt2" id="#menup2" class="nav-link" role="tab" data-toggle="tab">List Siap WO<span style="color: black;" class="label kerja label-rouded label-default pull-right"></span></a>
                </li>
                <li>
                    <a href="#menupt6" id="#menup6" class="nav-link" role="tab" data-toggle="tab">List Bukan PT-2<span class="label not_pt2 label-rouded label-info pull-right"></span></a>
                </li>

                <li>
                    <a href="#menupt3" id="#menup3" class="nav-link" role="tab" data-toggle="tab">List Selesai<span class="label selesai label-rouded label-success pull-right"></span></a>
                </li>
                <li>
                    <a href="#menupt4" id="#menup4" class="nav-link" role="tab" data-toggle="tab">List Kendala<span class="label kendala label-rouded label-danger pull-right"></span></a>
                </li>
                <li>
                    <a href="#menupt5" id="#menup5" class="nav-link" role="tab" data-toggle="tab">List OGP<span class="label ogp label-rouded label-primary pull-right"></span></a>
                </li>
                <li>
                    <a href="#menupt1" id="#menup5_1" class="nav-link" role="tab" data-toggle="tab">List Reject<span class="label reject label-rouded label-primary pull-right"></span></a>
                </li>
                <li>
                    <a href="#menupt7" id="#menup7" class="nav-link" role="tab" data-toggle="tab">List Pending<span class="label pending label-rouded label-green pull-right"></span></a>
                </li>
            </ul>
            <div class="tab-content">
                @for($i = 2; $i <= 7; $i++)
                <div id="menupt{{$i}}" class="tab-pane fade">
                    <div class="page-wrap">
                        <div class="form-group">
                            <input type="text" class="form-control" id="cari_{{$i}}" placeholder="Masukkan Nama ODP Atau Id UNSC atau Id Proaktif" style="width: 100%;">
                        </div>
                        <table id="footablesz_{{$i}}" data-page-navigation=".pagination{{$i}}" class="teknisi_{{$i}} table toggle-circle table-hover table-bordered" >
                            <thead>
                                <tr>
                                    <th data-toggle="true">Nama ODP</th>
                                    <th data-toggle="true">Jenis Pekerjaan</th>
                                    <th data-hide="phone">Id UNSC</th>
                                    <th data-hide="phone">Proaktif</th>
                                    <th data-hide="phone">Koordinat</th>
                                    @if(!in_array($i, [2, 6]))
                                        <th data-hide="phone">Tanggal Selesai</th>
                                    @endif
                                    @if($i != 3)
                                        <th data-hide="phone">Action</th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody id="data_table{{$i}}">
                                <?php $result = 1; ?>
                                @if($i == 2)
                                    @foreach($list as $no => $un)
                                    <tr>
                                        <td data-title="Data">{{$un->odp_nama}}</td>
                                        <td data-title="Data">{{$un->jenis}}</td>
                                        <td data-title="Data">{{$un->nomor_sc or '-'}}</td>
                                        <td data-title="Data">{{$un->proaktif_id}}</td>
                                        <td data-title="Data">{{$un->odp_koor}}</td>
                                        <td data-title="Data" style="padding-top: 8px;">
                                        @if (in_array(Session::get('auth')->pt2_level, [0, 2, 5]))
                                            <a type="button" style="outline:none;background-color: Transparent;border: none;" class="btn btn-light"  href='{{URL::to("/laporan/{$un->id }")}}'><span data-icon="&#xe00a;" class="linea-icon linea-basic fa-fw" style="font-size: 15px;"></span>&nbsp;{{
                                            (empty($un->lt_status))?'Mulai Berangkat':
                                            (($un->id == $un->nomor_sc)?'Update UNSC':'Update Non UNSC') }}</a>
                                        @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                @endif

                                @if($i == 6)
                                    @foreach($list_not_pt2 as $no => $un)
                                    <tr>
                                        <td data-title="Data">{{$un->odp_nama}}</td>
                                        <td data-title="Data">{{ $list_d->status_nama }}</td>
                                        <td data-title="Data">{{$un->nomor_sc or '-'}}</td>
                                        <td data-title="Data">{{$un->proaktif_id}}</td>
                                        <td data-title="Data">{{$un->odp_koor}}</td>
                                        <td data-title="Data" style="padding-top: 8px;"><a type="button" style="outline:none;background-color: Transparent;border: none;" class="btn btn-light"  href='{{URL::to("/laporan/{$un->id }")}}'><span data-icon="&#xe00a;" class="linea-icon linea-basic fa-fw" style="font-size: 15px;"></span>&nbsp;{{
                                            (empty($un->lt_status))?'Mulai Berangkat':
                                            (($un->id == $un->nomor_sc)?'Update UNSC':'Update Non UNSC') }}</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                @endif

                                @if($i == 3)
                                    @foreach($list_done as $list_d)
                                    <tr>
                                        <td data-title="Data">{{ $list_d->odp_nama }}</td>
                                        <td data-title="Data">{{ $list_d->status_nama }}</td>
                                        <td data-title="Data">{{ $list_d->nomor_sc or '-'}}</td>
                                        <td data-title="Data">{{ $list_d->proaktif_id}}</td>
                                        <td data-title="Data">{{ $list_d->odp_koor }}</td>
                                        <td data-title="Data">{{ $list_d->tgl_selesai }}</td>
                                    </tr>
                                    @endforeach
                                @endif

                                @if($i == 4)
                                    @foreach($list_ken as $list_d)
                                    <tr>
                                        <td>{{$list_d->odp_nama}}</td>
                                        <td data-title="Data">{{ $list_d->status_nama }}</td>
                                        <td>{{$list_d->nomor_sc or '-'}}</td>
                                        <td>{{$list_d->proaktif_id}}</td>
                                        <td>{{$list_d->odp_koor}}</td>
                                        <td>{{$list_d->tgl_selesai}}</td>
                                        <td style="padding-top: 8px;"><a type="button" class="btn btn-light" style="outline:none;background-color: Transparent;border: none;" href='{{URL::to("/laporan/{$list_d->id }")}}'><span data-icon="&#xe00a;" class="linea-icon linea-basic fa-fw" style="font-size: 15px;"></span>&nbsp;{{($list_d->id == $list_d->nomor_sc)?'Ganti Kendala Order UNSC':'Ubah Kendala Order Non UNSC'}}</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                @endif

                                @if($i == 5)
                                    @foreach($list_ogp as $list_d)
                                    <tr>
                                        <td>{{$list_d->odp_nama}}</td>
                                        <td data-title="Data">{{ $list_d->status_nama }}</td>
                                        <td>{{$list_d->nomor_sc or '-'}}</td>
                                        <td data-title="Data">{{$list_d->proaktif_id}}</td>
                                        <td>{{$list_d->odp_koor}}</td>
                                        <td>{{$list_d->tgl_selesai}}</td>
                                        <td style="padding-top: 8px;"><a type="button" class="btn btn-light link_button" style="outline:none;background-color: Transparent;border: none;" href='{{URL::to("/laporan/{$list_d->id }")}}'><span data-icon="&#xe00a;" class="linea-icon linea-basic fa-fw" style="font-size: 15px;"></span>&nbsp; Melanjutkan Order</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                @endif

                                @if($i == 1)
                                    @foreach($list_ogp_reject as $list_d)
                                    <tr>
                                        <td>{{$list_d->odp_nama}}</td>
                                        <td data-title="Data">{{ $list_d->status_nama }}</td>
                                        <td>{{$list_d->nomor_sc or '-'}}</td>
                                        <td data-title="Data">{{$list_d->proaktif_id}}</td>
                                        <td>{{$list_d->odp_koor}}</td>
                                        <td>{{$list_d->tgl_selesai}}</td>
                                        <td style="padding-top: 8px;"><a type="button" class="btn btn-light link_button" style="outline:none;background-color: Transparent;border: none;" href='{{URL::to("/laporan/{$list_d->id }")}}'><span data-icon="&#xe00a;" class="linea-icon linea-basic fa-fw" style="font-size: 15px;"></span>&nbsp; Melanjutkan Order</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                @endif

                                @if($i == 7)
                                    @foreach($list_pending as $list_d)
                                    <tr>
                                        <td>{{$list_d->odp_nama}}</td>
                                        <td data-title="Data">{{ $list_d->status_nama }}</td>
                                        <td>{{$list_d->nomor_sc or '-'}}</td>
                                        <td data-title="Data">{{$list_d->proaktif_id}}</td>
                                        <td>{{$list_d->odp_koor}}</td>
                                        <td>{{$list_d->tgl_selesai}}</td>
                                        <td style="padding-top: 8px;"><a type="button" class="btn btn-light" style="outline:none;background-color: Transparent;border: none;" href='{{URL::to("/laporan/{$list_d->id }")}}'><span data-icon="&#xe00a;" class="linea-icon linea-basic fa-fw" style="font-size: 15px;"></span>&nbsp; Order Ulang lagi</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                        <div style="text-align:center">
                            <div class="holder data_holder{{$i}}"></div>
                        </div>
                    </div>
                </div>
                @endfor
            </div>
        </div>
    </div>
</div>
@endsection
@section('footerS')
<script src="/bower_components/footable/js/footable.all.min.js"></script>
<script src="/bower_components/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
<script src="/js/footable-init.js"></script>
<script src="/bower_components/jPages-master/js/jPages.js"></script>
<script type="text/javascript">
    $(function() {

        $(".nav-tabs a").click(function(){
            $(this).tab('show');
        });

        $('#menupt2').toggleClass( "in active" );

        $(".data_holder2").jPages({
            containerID: "data_table2",
            previous   : "←",
            next       : "→",
            perPage    : 10,
            midRange   : 3,
            endRange   : 1,
            delay      : 20
        });

        $('#footablesz_2').footable();

        $('.nav-link').on('shown.bs.tab', function () {
            var number = $(this).attr('id').substr(6,1);
            $('#footablesz_'+number).footable();
            $('#footablesz_'+number).data('page-size', Math.pow(new Date().getFullYear(),2));
            $('#footablesz_'+number).trigger('footable_resize');
            $('#footablesz_'+number).trigger('footable_expand_all');

            $(".data_holder"+number).jPages({
                containerID: "data_table"+number,
                previous   : "←",
                next       : "→",
                perPage    : 10,
                midRange   : 3,
                endRange   : 1,
                delay      : 20
            });
        });

        $('.nav-link').on('hide.bs.tab', function(e){
            var number = $(this).attr('id').substr(6,1);
            $(".data_holder"+number).jPages('destroy');
        });

        var list_kerja = {!! json_encode($list) !!},
        list_selesai = {!! json_encode($list_done) !!},
        list_kendala = {!! json_encode($list_ken) !!},
        list_ogp = {!! json_encode($list_ogp) !!},
        list_pending = {!! json_encode($list_pending) !!},
        list_not_pt2 = {!! json_encode($list_not_pt2) !!},
        arr = [ "2", "3", "4", "5", "6", "7" ];

        $(".kerja").text(list_kerja.length);
        $(".selesai").text(list_selesai.length);
        $(".kendala").text(list_kendala.length);
        $(".ogp").text(list_ogp.length);
        $(".pending").text(list_pending.length);
        $(".not_pt2").text(list_not_pt2.length);

        jQuery.each( arr, function( i, val ) {
            $("#cari_"+val).keyup(function(){
                var input, filter, table, tr, td1, i, td2, td3;
                input = document.getElementById("cari_"+val);
                filter = input.value.toUpperCase();
                table = document.getElementsByClassName("teknisi_"+val);
                tr = table[0].getElementsByTagName("tr");
                for (i = 0; i < tr.length; i++) {
                    td1 = tr[i].getElementsByTagName("td")[0];
                    td2 = tr[i].getElementsByTagName("td")[1];
                    td3 = tr[i].getElementsByTagName("td")[2];
                    if (td1) {
                        if (td1.innerHTML.toUpperCase().indexOf(filter) > -1) {
                            tr[i].style.display = "";
                        } else if (td2) {
                            if (td2.innerHTML.toUpperCase().indexOf(filter) > -1) {
                                tr[i].style.display = "";
                            } else if (td3) {
                                if (td3.innerHTML.toUpperCase().indexOf(filter) > -1) {
                                    tr[i].style.display = "";
                                } else{
                                    tr[i].style.display = "none";
                                }
                            }
                        }
                    }
                }
            });
        });
    });
</script>
@endsection