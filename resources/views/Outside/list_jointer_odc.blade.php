@extends('layout')
@section('title', 'List PT-2 SIMPLE KPRO')
@section('style')
<style type="text/css">
	th{
		text-align: center;
		white-space:nowrap;
	}
	div>table {
		float: left
	}
</style>
@endsection
@section('content')
<div class="container-fluid" style="padding-top: 25px;">
	<div class="container-fluid" style="padding-top: 25px;">
    <div class="panel panel-primary">
      <div class="panel-heading header-date">List ODC {{ $judul }} Datel {{ $datel }} {{ strcasecmp($sto, 'All') != 0 ? 'STO '. $sto : ''}}</div>
      <div class="panel-body">
        <div class="table-responsive">
          <table id="teknisi" class="table table-striped color-bordered-table info-bordered-table">
            <thead>
              <tr>
                <th>Datel</th>
                <th>ODC</th>
                <th>Jumlah WO</th>
              </tr>
            </thead>
            <tbody>
              @foreach($fd as $no => $v)
                @php
                  $first = TRUE;
                @endphp
                @foreach($v['list'] as $k => $vv)
                  <tr>
                    @if($first)
                      <td style="vertical-align: middle;" rowspan="{{ count($v['list']) }}">{{ $v['datel'] }}</td>
                      @php
                        $first = FALSE;
                      @endphp
                    @endif
                    <td style="text-align: center;"><a href="/outside/jointer_list/{{ $sub_judul }}/{{ urlencode($datel) }}/{{ Request::segment(5) }}/{{ $k }}">{{ $k }}</a></td>
                    <td style="text-align: center;"><a href="/outside/jointer_list/{{ $sub_judul }}/{{ urlencode($datel) }}/{{ Request::segment(5) }}/{{ $k }}">{{ $vv }}</a></td>
                  </tr>
                @endforeach
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection