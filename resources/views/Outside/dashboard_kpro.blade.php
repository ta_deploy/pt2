@extends('layout')
@section('title', 'List PT-2 SIMPLE KPRO')
@section('headerS')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.css">
<link href="/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.js"></script>
@endsection
@section('style')
<style type="text/css">
	th{
		text-align: center;
		white-space:nowrap;
	}
	div>table {
		float: left
	}
</style>
@endsection
@section('content')
<div class="container-fluid" style="padding-top: 25px;">
	<div class="row">
		<div class="col-md-6">
			<div class="panel panel-info">
				<div class="panel-heading header-date">LIST KPRO SUDAH DIKERJAKAN JOINTER
					<div class="panel-action">
						<a href="#" data-perform="panel-collapse"><i style="font-size: 20px;" class="ti-minus"></i></a>
					</div>
				</div>
				<div class="panel-body">
					<div class="table-responsive">
						<table class="table color-bordered-table success-bordered-table">
							<thead>
								<tr>
									<th>Regu</th>
									<th>Datel</th>
									<th>STO</th>
									<th>Jumlah WO</th>
								</tr>
							</thead>
							<tbody>
								@php
									$total = 0;
								@endphp
								@foreach($list_pt2 as $k => $v)
									@php
										$first = true;
										// dd($v);
										$sum_1 = array_map(function($x){
											return count($x['sto']);
										}, $v);

										$sum_1 = array_sum($sum_1);
									@endphp
									@foreach($v as $kk => $vv)
										@php
											$second = true;
										@endphp
										@foreach($vv['sto'] as $kkk => $vvv)
											@php
												$total += $vvv;
											@endphp
											<tr>
												@if($first == true)
													<td rowspan="{{ $sum_1 }}" class="align-middle" style="text-align: center;"><a href="/outside/OS_pt/pt/{{ urlencode($k) }}/all/all">{{ $k }}</a></td>
													@php
														$first = false;
													@endphp
												@endif
												@if($second == true)
													<td rowspan="{{ count($vv['sto']) }}" class="align-middle" style="text-align: center;"><a href="/outside/OS_pt/pt/{{ urlencode($k) }}/{{ urlencode($kk) }}/all">{{ $kk }}</a></td>
													@php
														$second = false;
													@endphp
												@endif
												<td style="text-align: center;">{{ $kkk }}</td>
												<td style="text-align: center;"><a href="/outside/OS_pt/pt/{{ urlencode($k) }}/{{ urlencode($kk) }}/{{ $kkk }}">{{ $vvv }}</a></td>
											</tr>
										@endforeach
									@endforeach
								@endforeach
							</tbody>
							<tfoot>
								<tr>
									<td colspan="3" style="text-align: center;">Total</td>
									<td style="text-align: center;"><a href="/outside/OS_pt/pt/all/all/all">{{ $total }}</a></td>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="panel panel-primary">
				<div class="panel-heading header-date">List KPRO SIAP DIKERJAKAN
					<div class="panel-action">
						<a href="#" data-perform="panel-collapse"><i style="font-size: 20px;" class="ti-minus"></i></a>
					</div>
				</div>
				<div class="panel-body">
					<div class="table-responsive">
						<table class="table color-bordered-table success-bordered-table">
							<thead>
								<tr>
									<th>Jenis Pekerjaan</th>
									<th>Datel</th>
									<th>STO</th>
									<th>Jumlah WO</th>
								</tr>
							</thead>
							<tbody>
								@php
									$total = 0;
								@endphp
								@foreach($list_kpro as $k => $v)
									@php
										$first = true;
										$sum_1 = array_map(function($x){
											return count($x['sto']);
										}, $v);

										$sum_1 = array_sum($sum_1);
									@endphp
									@foreach($v as $kk => $vv)
										@php
											$second = true;
										@endphp
										@foreach($vv['sto'] as $kkk => $vvv)
											@php
												$total += $vvv;
											@endphp
											<tr>
												@if($first == true)
													<td rowspan="{{ $sum_1 }}" class="align-middle" style="text-align: center;"><a href="/outside/OS_pt/kpro/{{ urlencode($k) }}/all/all">{{ $k }}</a></td>
													@php
														$first = false;
													@endphp
												@endif
												@if($second == true)
													<td rowspan="{{ count($vv['sto']) }}" class="align-middle" style="text-align: center;"><a href="/outside/OS_pt/kpro/{{ urlencode($k) }}/{{ urlencode($kk) }}/all">{{ $kk }}</a></td>
													@php
														$second = false;
													@endphp
												@endif
												<td style="text-align: center;">{{ $kkk }}</td>
												<td style="text-align: center;"><a href="/outside/OS_pt/kpro/{{ urlencode($k) }}/{{ urlencode($kk) }}/{{ $kkk }}">{{ $vvv }}</a></td>
											</tr>
										@endforeach
									@endforeach
								@endforeach
							</tbody>
							<tfoot>
								<tr>
									<td colspan="3" style="text-align: center;">Total</td>
									<td style="text-align: center;"><a href="/outside/OS_pt/kpro/all/all/all">{{ $total }}</a></td>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('footerS')
<script src="/bower_components/moment/min/moment.min.js"></script>
<script src="/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.js"></script>
<script>
	$(function(){
		$('#pt2_kpro').toggleClass( "in active" );

		$('.delete_rev').click(function(){
			var value = $(this).data('id');
			Swal.fire({
				title: '<strong>Hapus Material <u>'+value+'</u></strong>',
				type: 'warning',
				html:
				'<b>'+value+'</b>, ' +
				'Tidak Bisa Dikembalikan! <br/>' +
				'Apakah Anda Yakin?',
				showCloseButton: true,
				showCancelButton: true,
				focusConfirm: false,
				confirmButtonText:
				'<span style="font-size: 20px;"><i data-icon="&#xe01c;" class="linea-icon linea-basic"></i> Ya!</span>',
				cancelButtonText:
				'<span style="font-size: 20px;"><i data-icon="&#xe016;" class="linea-icon linea-aerrow"></i></span>',
			}).then((result) => {
				if (result.value) {
					$.ajax({
						type: "GET",
						data: {id : value},
						url: "/material/revenue_delete",
						cache: false,
						success: function(response) {
							Swal.fire(
								'Berhasil!',
								note,
								'success'
								)
							location.reload();
						}
					});
				}
			});
		});

	})

</script>
@endsection