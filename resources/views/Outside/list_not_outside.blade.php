@extends('layout')
@section('title', 'List PT-2 SIMPLE KPRO')
@section('headerS')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.css">
<link href="/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.js"></script>
@endsection
@section('style')
<style type="text/css">
	th{
		text-align: center;
		white-space:nowrap;
	}
	div>table {
		float: left
	}
</style>
@endsection
@section('content')
<div class="container-fluid" style="padding-top: 25px;">
	<div class="container-fluid" style="padding-top: 25px;">
    <div class="panel panel-warning">
      <div class="panel-heading header-date">List Order {{ $judul }} Datel {{ $datel }} {{ strcasecmp($sto, 'All') != 0 ? 'STO '. $sto : ''}}</div>
      <div class="panel-body">
        <div class="table-responsive">
          <table id="teknisi" class="table table-striped table-hover table-bordered">
            <thead>
              <tr>
                @if(!in_array($jenis, ['nog_m']))
                  <th>Nomor SC</th>
                  <th>Nama Order</th>
                @endif
                <th>Nama ODP</th>
                <th>Koordinat ODP</th>
                @if($jenis == 'nog_m')
                  <th>Sumber Data</th>
                @endif
                <th>Tanggal Create</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              @foreach($data as $no => $r)
                @if($jenis == 'nog_m')
                  <tr>
                    <td>{{ $r->nama_odp }}</td>
                    <td>{{ $r->koordinat_odp }}</td>
                    <td>{{ $r->jenis_order }}</td>
                    <td>{{ $r->created_at }}</td>
                    <td>
                      <a type="button" class="btn btn-info btn-sm" href='/outside/add/NOG/{{ $r->id }}'><i data-icon="U" class="linea-icon linea-basic fa-fw" style="color:#ffffff; font-size: 20px; vertical-align:middle;"></i><span style="color:#ffffff;">&nbsp;Proses</span></a>
                    </td>
                  </tr>
                @else
                  <tr>
                    <td>{{ $r->orderId }}</td>
                    <td>{{ $r->orderName }}</td>
                    <td>{{ $r->nama_odp }}</td>
                    <td>{{ $r->kordinat_odp }}</td>
                    <td>{{ $r->tgl_dispatch }}</td>
                    <td>
                      <a type="button" class="btn btn-info btn-sm" href='/outside/add/PSB/{{ $r->orderId }}'><i data-icon="U" class="linea-icon linea-basic fa-fw" style="color:#ffffff; font-size: 20px; vertical-align:middle;"></i><span style="color:#ffffff;">&nbsp;Proses</span></a>
                    </td>
                  </tr>
                @endif
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('footerS')
<script src="/bower_components/moment/min/moment.min.js"></script>
<script src="/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.js"></script>
<script>
$(function(){
  $('.table').DataTable()
})
</script>
@endsection