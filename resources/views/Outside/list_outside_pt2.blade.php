@extends('layout')
@section('title', 'List PT-2 SIMPLE KPRO')
@section('headerS')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.css">
<link href="/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.js"></script>
@endsection
@section('style')
<style type="text/css">
	th{
		text-align: center;
		white-space:nowrap;
	}
	div>table {
		float: left
	}
</style>
@endsection
@section('content')
<div class="container-fluid" style="padding-top: 25px;">
	<div class="container-fluid" style="padding-top: 25px;">
    <div class="panel panel-warning">
      <div class="panel-heading header-date">List Order {{ $judul }} Datel {{ $datel }} {{ strcasecmp($sto, 'All') != 0 ? 'STO '. $sto : ''}}</div>
      <div class="panel-body">
        <div class="table-responsive">
          <table id="teknisi" class="table table-striped table-hover table-bordered">
            <thead>
              <tr>
                <th>Nama ODP KPRO</th>
                <th>Status KPRO</th>
                <th>Jenis PSB</th>
                <th>STO KPRO</th>
                @if($jenis != 'kpro')
                  <th>Headline PT-2</th>
                  <th>ODP</th>
                  <th>Status</th>
                  <th>Detail Status</th>
                  <th>Nama Regu</th>
                @else
                  <th>Action</th>
                @endif
              </tr>
            </thead>
            <tbody>
              @foreach($data as $no => $r)
                <tr>
                  @if($jenis != 'kpro')
                    <td>{{ $r['odp_kpro'] }}</td>
                    <td>{{ $r['status_kpro'] }}</td>
                    <td>{{ $r['jenispb'] }}</td>
                    <td>{{ $r['sto_kpro'] }}</td>
                    <td>{{ $r['project_name'] }}</td>
                    <td>{{ $r['odp_nama'] }}</td>
                    <td>{{ $r['lt_status'] }}</td>
                    <td>{{ $r['kendala_detail'] }}</td>
                    <td>{{ $r['regu_name'] }}</td>
                  @else
                    <td>{{ $r->odp }}</td>
                    <td>{{ $r->status_prj }}</td>
                    <td>{{ $r->jenispb }}</td>
                    <td>{{ $r->sto }}</td>
                    <td>
                      <a type="button" class="btn btn-info btn-sm" href='/outside/add/KPRO_SIMPLE/{{ $r->id }}'><i data-icon="U" class="linea-icon linea-basic fa-fw" style="color:#ffffff; font-size: 20px; vertical-align:middle;"></i><span style="color:#ffffff;">&nbsp;Proses</span></a>
                    </td>
                  @endif
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('footerS')
<script src="/bower_components/moment/min/moment.min.js"></script>
<script src="/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.js"></script>
<script>
$(function(){
  $('.table').DataTable()
})
</script>
@endsection