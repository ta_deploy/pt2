@extends('layout')
@section('title', 'List Pekerjaan Jointer')
@section('headerS')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link href="/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.js"></script>
@endsection
@section('style')
<style type="text/css">
	th {
		text-align: center;
		white-space:nowrap;
	}
	div>table {
		float: left
	}
</style>
@endsection
@section('content')
<div class="container-fluid" style="padding-top: 25px;">
	<div class="row">
		<div class="col-md-12">
      <div class="panel panel-warning">
        <div class="panel-heading header-date">Order Jointer
          <div class="panel-action">
            <a href="#" data-perform="panel-collapse"><i style="font-size: 20px;" class="ti-minus"></i></a>
          </div>
        </div>
        <div class="panel-body">
          <div class="table-responsive">
            <table class="table color-bordered-table success-bordered-table">
              <thead>
                <tr>
                  <th>Datel</th>
                  <th>STO</th>
                  <th>Jumlah WO</th>
                </tr>
              </thead>
              <tbody>
                @php
                  $total = 0;
                @endphp
                @foreach($list_jointer_pt2 as $k => $v)
                  @php
                    $first = true;
                  @endphp
                  @foreach($v['sto'] as $kk => $vv)
                    @php
                      $total += $vv;
                    @endphp
                    <tr>
                      @if($first == true)
                        <td rowspan="{{ count($v['sto']) }}" class="align-middle" style="text-align: center;"><a href="/outside/jointer_odc/pt2/{{ urlencode($k) }}/all">{{ $k }}</a></td>
                        @php
                          $first = false;
                        @endphp
                      @endif
                      <td style="text-align: center;">{{ $kk }}</td>
                      <td style="text-align: center;"><a href="/outside/jointer_odc/pt2/{{ urlencode($k) }}/{{ $kk }}">{{ $vv }}</a></td>
                    </tr>
                  @endforeach
                @endforeach
              </tbody>
              <tfoot>
                <tr>
                  <td colspan="2" style="text-align: center;">Total</td>
                  <td style="text-align: center;"><a href="/outside/jointer_odc/pt2/all/all/">{{ $total }}</a></td>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
      </div>
    </div>
		<div class="col-md-12">
      <div class="panel panel-warning">
        <div class="panel-heading header-date">Order Marina - Jointer
          <div class="panel-action">
            <a href="#" data-perform="panel-collapse"><i style="font-size: 20px;" class="ti-minus"></i></a>
          </div>
        </div>
        <div class="panel-body">
          <div class="table-responsive">
            <table class="table color-bordered-table success-bordered-table">
              <thead>
                <tr>
                  <th>Datel</th>
                  <th>STO</th>
                  <th>Jumlah WO</th>
                </tr>
              </thead>
              <tbody>
                @php
                  $total = 0;
                @endphp
                @foreach($list_jointer_marina as $k => $v)
                  @php
                    $first = true;
                  @endphp
                  @foreach($v['sto'] as $kk => $vv)
                    @php
                      $total += $vv;
                    @endphp
                    <tr>
                      @if($first == true)
                        <td rowspan="{{ count($v['sto']) }}" class="align-middle" style="text-align: center;"><a href="/outside/jointer_odc/marina/{{ urlencode($k) }}/all">{{ $k }}</a></td>
                        @php
                          $first = false;
                        @endphp
                      @endif
                      <td style="text-align: center;">{{ $kk }}</td>
                      <td style="text-align: center;"><a href="/outside/jointer_odc/marina/{{ urlencode($k) }}/{{ $kk }}">{{ $vv }}</a></td>
                    </tr>
                  @endforeach
                @endforeach
              </tbody>
              <tfoot>
                <tr>
                  <td colspan="2" style="text-align: center;">Total</td>
                  <td style="text-align: center;"><a href="/outside/jointer_odc/marina/all/all/">{{ $total }}</a></td>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
      </div>
    </div>
		<div class="col-md-12">
      <div class="panel panel-info">
        <div class="panel-heading header-date">Order BTS - Jointer
          <div class="panel-action">
            <a href="#" data-perform="panel-collapse"><i style="font-size: 20px;" class="ti-minus"></i></a>
          </div>
        </div>
        <div class="panel-body">
          <div class="table-responsive">
            <table class="table color-bordered-table success-bordered-table">
              <thead>
                <tr>
                  <th>Datel</th>
                  <th>STO</th>
                  <th>Jumlah WO</th>
                </tr>
              </thead>
              <tbody>
                @php
                  $total = 0;
                @endphp
                @foreach($list_jointer_bts as $k => $v)
                  @php
                    $first = true;
                  @endphp
                  @foreach($v['sto'] as $kk => $vv)
                    @php
                      $total += $vv;
                    @endphp
                    <tr>
                      @if($first == true)
                        <td rowspan="{{ count($v['sto']) }}" class="align-middle" style="text-align: center;"><a href="/outside/jointer_odc/bts/{{ urlencode($k) }}/all">{{ $k }}</a></td>
                        @php
                          $first = false;
                        @endphp
                      @endif
                      <td style="text-align: center;">{{ $kk }}</td>
                      @if ($k == 'BANJARMASIN - KYG')
                      @endif
                      <td style="text-align: center;"><a href="/outside/jointer_odc/bts/{{ urlencode($k) }}/{{ $kk }}">{{ $vv }}</a></td>
                    </tr>
                  @endforeach
                @endforeach
              </tbody>
              <tfoot>
                <tr>
                  <td colspan="2" style="text-align: center;">Total</td>
                  <td style="text-align: center;"><a href="/outside/jointer_odc/bts/all/all/">{{ $total }}</a></td>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
      </div>
    </div>
	</div>
</div>
@endsection
@section('footerS')
<script src="/bower_components/moment/min/moment.min.js"></script>
<script src="/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script>
	$(function(){
		$('#marina_nog').toggleClass( "in active" );

		$('.delete_rev').click(function(){
			var value = $(this).data('id');
			Swal.fire({
				title: '<strong>Hapus Material <u>'+value+'</u></strong>',
				type: 'warning',
				html:
				'<b>'+value+'</b>, ' +
				'Tidak Bisa Dikembalikan! <br/>' +
				'Apakah Anda Yakin?',
				showCloseButton: true,
				showCancelButton: true,
				focusConfirm: false,
				confirmButtonText:
				'<span style="font-size: 20px;"><i data-icon="&#xe01c;" class="linea-icon linea-basic"></i> Ya!</span>',
				cancelButtonText:
				'<span style="font-size: 20px;"><i data-icon="&#xe016;" class="linea-icon linea-aerrow"></i></span>',
			}).then((result) => {
				if (result.value) {
					$.ajax({
						type: "GET",
						data: {id : value},
						url: "/material/revenue_delete",
						cache: false,
						success: function(response) {
							Swal.fire(
								'Berhasil!',
								note,
								'success'
								)
							location.reload();
						}
					});
				}
			});
		});

	})

</script>
@endsection