<table id="teknisi" class="table table-striped">
	<thead>
		<tr>
			<th>#</th>
			<th>Nomor PO</th>
			<th>ID Project TA</th>
			<th>Alokasi Jumlah ODP</th>
			<th>Odp Terpasang</th>
			<th>Nilai PO</th>
		</tr>
	</thead>
	<tbody>
		@php $result = 1; @endphp
		@if(!empty($report_po))
		@foreach($report_po as $po)
		<tr>
			<td>{{$result++}}</td>
			<td>{{$po->no_po}}</td>
			<td> <a type="button" class="btn btn-light" href='{{URL::to("/rekapPO/edit/{$po->id_tes}")}}' target="_blank">&nbsp;{{$po->id_project_Ta}}</a></td>
			<td>{{$po->juml_odp}}</td>
			<td>{{$po->odp_namnya}}</td>
			<td>{{$po->nilai_po}}</td>
		</tr>
		@endforeach
		@else
		<td colspan="6"><div class="alert alert-danger"> Tidak Ada Data Disini <i data-icon="g" class="linea-icon linea-basic fa-fw"></i></div></td>
		@endif
	</tbody>
</table>