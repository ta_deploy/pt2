@extends('layout')
@section('title', "Hasil Pencarian " . ($data['jenis'] == 'non_id' ? $cari : 'data') )
@section('style')
<style type="text/css">
	th, td{
		text-align: center;
		white-space:nowrap;
	}
</style>
@endsection
@section('content')
<div class="modal fade modal_detail_layout detail_layout modal_lg" id="detail_layout">
    <div class="modal_lg" role="document" style="width: 95%; margin: auto;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel" style="width: 400px;"><div id="nama_project"></div></h5>
                <div style="float: left; clear: both;" id='download_photo_'>
                    <a type="button" class="btn btn-info download_zip_photo" href="" style="text-align: center; color: #ffffff;"><span data-icon="&#xe035;" class="linea-icon linea-aerrow fa-fw" style="font-size: 20px; color: #ffffff; "></span>&nbsp;Unduh Foto Teknisi</a>
										<a type="button" class="btn btn-info download_abd_file" href="" style="margin-left: 10px; text-align: center; color: #ffffff;"><span data-icon="M" class="linea-icon linea-software fa-fw" style="font-size: 20px; color: #ffffff; "></span>&nbsp;Unduh ABD</a>
                </div>
                <div class="row">
                    <div id="download_file_rfc">
                        <a type="button" class="btn btn-warning download_file_pdf" href="" download style="text-align: center; margin-left: 6px; color: #ffffff;"><span data-icon="&#xe00b;" class="linea-icon linea-basic fa-fw" style="font-size: 20px; color: #ffffff; "></span>&nbsp;Unduh PDF RFC</a>
                    </div>
                    <div id="delete_mine">
                        <a type="button" style="text-align: center; margin-left: 6px; color: #ffffff;" class="btn btn-danger layout_del" style="float: right;"><i data-icon="&#xe01d;" class="linea-icon linea-basic fa-fw"></i>Delete</a>
                    </div>
                    <div id="saldo_wo">
                        <a type="button" class="btn btn-info layout_saldo" style="float: right;margin-right: 12px;"></a>
                    </div>
                </div>
            </div>
            <div class="modal-body" style="overflow-y:auto;padding: 0px 0px 0px 0px;">
                <div id="content"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid" style="padding-top: 25px;">
	<div class="panel panel-success">
		<div class="panel-heading">Hasil Pencarian <u>{{ ($data['jenis'] == 'non_id' ? $cari : 'data') }}</u></div>
		<div class="panel-body">
			@if(!empty($data['data']) )
				@foreach($data['data'] as $d)
					<div class="card card-outline-primary text-left text-dark">
						<div class="card-block">
							<h3>{{ $d->project_name }} / (Status <b>{{ $d->lt_status }}</b>)</h3>

							@if ($d->pelanggan_koor)
								<span>Koordinat Pelanggan: <b>{{ $d->pelanggan_koor }}</b></span><br/>
							@endif

							<span> <b><u>{{ $d->nomor_sc }}</u></b> </span><br/>
							<span>CATATAN:</span><br/>
							<span><u>{{ $d->catatan_HD }}</u></span><br/>

							@if ($d->odp_nama_before)
								<span>Nama ODP Sebelumnya: <b>{{ $d->odp_nama_before }}</b></span><br />
							@endif

							<span>ODP: <b><u>{{ $d->odp_nama or 'Belum Diinput' }}</u></b></span><br/>
							@if ($d->aspl_nama)
								<span>Nama ASPL: <b><u>{{ $d->aspl_nama }}</u></b></span><br />
							@endif

							<span>REGU: <b><u>{{ $d->regu_name or 'Belum Diinput' }}</u></b></span><br/>
							<span>STATUS REGU: <b style="color: {{ $d->stts_regu == 1 ? 'lime' : 'red' }}"><u>{{ $d->stts_regu == 1 ? 'Aktif' : 'Non-Aktif' }}</u></b></span><br/>

							@if ($d->odp_koor_before)
								<span>Koordinat ODP Sebelumnya: <b>{{ $d->odp_koor_before }}</b></span><br />
							@endif

							<span>KOORDINAT: <b><u>{{ $d->odp_koor or 'Tidak Ada'}}</u></b></span><br/>

							@if($d->note == 'pt2')
							@php
							if(empty($d->lt_status) && empty($d->regu_id)){
								$d->lt_status = 'Belum Dispatch';
							}elseif(empty($d->lt_status) && !empty($d->regu_id)){
								$d->lt_status = 'Need Progress';
							}
							$date1=date_create($d->tgl_pengerjaan);
							$date2=date_create($d->tgl_selesai);
							$diff=date_diff($date1,$date2);
							$waktu['h']= $diff->format('%d') != 0 ? $diff->format('%d').' Hari': '';
							$waktu['j']= $diff->format('%h') != 0 ? $diff->format('%h').' Jam': '';
							$waktu['m']= $diff->format('%i') != 0 ? $diff->format('%i').' Menit': '';
							$time = implode(' ', $waktu);
							@endphp

							<span>Tanggal WO Created: <b><u>{{ $d->tgl_pengerjaan }}</u></b></span><br/>
							<span>Tanggal WO Selesai: <b><u>{{ $d->tgl_selesai }}</u></b></span><br/>
							<span>Durasi: <b style="color: lime;"><u>{{ $time }}</u></b></span><br/>

							@if(in_array($d->lt_status, ['Selesai', 'Kendala']))
								@php
								if($d->lt_status =='Selesai' ){
									if($d->GOLIVE == 0){
										$detail = 'Belum Go live';
									}else{
										$detail = 'Go live';
									}
								}elseif($d->lt_status =='Kendala' ){
									$detail = $d->kendala_detail;
								}
								@endphp
								<span>Detail Status: <b><u>{{ $detail }}</u></b></span><br/>
								@endif
							@endif

							@if($d->status != 'Completed')
								<span>UMUR WO: <b><u>{{ !empty($d->umur) ? $d->umur.' Hari' : 'Belum Ada' }}</u></b></span><br/>
							@endif

							<div class="card-footer">
								<span class="label label-rounded label-info" style="color: black;">{{ $d->status }}</span>
								<span class="time pull-right" style="right: 30px; font-size: 17px;">{{ $d->tanggal }}</span>
								@if($d->sl != 75 && isset($d->kategory_non_unsc) && $d->kategory_non_unsc == 0 && !in_array($d->status, ['Kendala', 'Selesai']))
									<a type="button" data-id="{{ $d->renew_id }}" style="color: white;" class="label ask_push label-rounded label-success">Dorong Ke PT-2</a>
								@else
								@if($d->note == 'psb')
									<a type="button" href="/admin/dispatch/add_s/{{ $d->id }}" style="color: white;" class="label label-rounded label-success">Lanjut WO PT-2</a>
								@elseif($d->note == 'pt2')
								@if(!empty($d->regu_id))
									<a type="button" style="color: black;" data-id="{{ $d->id }}" class="label liat_dunk label-rounded label-danger">Lihat Detail / Hapus</a>
								@endif
								@if($d->status != 'Completed')
									{{-- @if($d->kategory_non_unsc == 0)
										<a type="button" href="/admin/dispatch/edit/add_s/{{ $d->id }}" style="color: #000080;" class="label label-rounded label-warning">Edit Data</a>
									@else
										<a type="button" href="/admin/edit/non_un/{{ $d->id }}" style="color: #000080;" class="label label-rounded label-warning">Edit Data</a>
									@endif --}}
									<a type="button" href="/admin/edit/non_un/{{ $d->id }}" style="color: #000080;" class="label label-rounded label-warning">Edit Data</a>
								@endif
								@if($d->status == 'Completed' && in_array(session('auth')->pt2_level, [2, 5]) )
									{{-- @if($d->kategory_non_unsc == 0)
										<a type="button" href="/admin/dispatch/edit/add_s/{{ $d->id }}" style="color: #000080;" class="label label-rounded label-warning">Edit Data</a>
									@else
										<a type="button" href="/admin/edit/non_un/{{ $d->id }}" style="color: #000080;" class="label label-rounded label-warning">Edit Data</a>
									@endif --}}
									<a type="button" href="/laporan/{{ $d->id }}" style="color: #000080;" class="label label-rounded label-warning">Cek Pekerjaan</a>
								@endif
								@if($d->delete_clm == 1 )
									<a type="button" data-id="{{ $d->id }}" style="color: black;" class="ask_revert label label-rounded label-primary">Kembalikan Orderan</a>
								@endif
								@if($d->umur >= 3 && $d->delete_clm == 0 && $d->lt_status != 'Selesai')
									<a type="button" data-id="{{ $d->id }}" style="color: black;" class="ask_age label label-rounded label-default">Renew Data</a>
								@endif
								@if($d->stts_regu == 0)
									<a type="button" data-id="{{ $d->id }}" style="color: black;" class="ask_reactive label label-rounded label-primary">Aktifkan Regu</a>
								@endif
								@endif
								@endif
							</div>
						</div>
					</div>
				@endforeach
            @else
            	Tidak Ada Data
            @endif
        </div>
    </div>
</div>
@endsection
@section('footerS')
<script type="text/javascript">
	$(function(){
		var detch_del = $('.layout_del').detach(),
		detch_saldo = $('.layout_saldo').detach(),
		detch_download_phto = $('.download_zip_photo').detach(),
		detch_abd = $('.download_abd_file').detach(),
		detch_download_rfc = $('.download_file_pdf').detach();

		function convertMiliseconds(miliseconds, format) {
			var days, hours, minutes, seconds, total_hours, total_minutes, total_seconds;

			total_seconds = parseInt(Math.floor(miliseconds / 1000));
			total_minutes = parseInt(Math.floor(total_seconds / 60));
			total_hours = parseInt(Math.floor(total_minutes / 60));
			days = parseInt(Math.floor(total_hours / 24));

			seconds = parseInt(total_seconds % 60);
			minutes = parseInt(total_minutes % 60);
			hours = parseInt(total_hours % 24);

			switch(format) {
				case 's':
				return total_seconds;
				case 'm':
				return total_minutes;
				case 'h':
				return total_hours;
				case 'd':
				return days;
				default:
				return { d: days, h: hours, m: minutes, s: seconds };
			}
		};

		$('.ask_push').on('click', function () {
				var valuen = $(this).attr('data-id');
				console.log(valuen);
				Swal.fire({
						title: 'Seriusan?',
						text: "Data Akan Dipindahkan Ke PT-2",
						type: 'warning',
						showCancelButton: true,
						confirmButtonColor: '#3085d6',
						cancelButtonColor: '#d33',
						confirmButtonText: 'Ya, Pindahkan!'
				}).then((result) => {
						if (result.value) {
								$.ajax({
										type: "GET",
										url: "/admin/push/pt2/"+valuen,
										cache: false,
										success: function(response) {
												Swal.fire(
														'Migrasi!',
														'Orderan berhasil Dipindahkan',
														'success'
														)
												location.reload();
										}
								});
						}
				});
		});

		$('.ask_revert').on('click', function () {
				var valuen = $(this).attr('data-id');
				console.log(valuen);
				Swal.fire({
						title: 'Seriusan?',
						text: "Data Akan Dikembalikan Ke PT-2",
						type: 'warning',
						showCancelButton: true,
						confirmButtonColor: '#3085d6',
						cancelButtonColor: '#d33',
						confirmButtonText: 'Ya, Pindahkan!'
				}).then((result) => {
						if (result.value) {
								$.ajax({
										type: "GET",
										url: "/admin/recovery/remove/order/"+valuen,
										cache: false,
										success: function(response) {
												Swal.fire(
														'Migrasi!',
														'Orderan berhasil Dikembalikan',
														'success'
														)
												location.reload();
										}
								});
						}
				});
		});

		$('.ask_age').on('click', function () {
				var valuen = $(this).attr('data-id');
				console.log(valuen);
				Swal.fire({
						title: 'Seriusan?',
						text: "Umur Data Akan Diperbaharui",
						type: 'warning',
						showCancelButton: true,
						confirmButtonColor: '#3085d6',
						cancelButtonColor: '#d33',
						confirmButtonText: 'Ya, Pindahkan!'
				}).then((result) => {
						if (result.value) {
								$.ajax({
										type: "GET",
										url: "/admin/recovery/age/order/"+valuen,
										cache: false,
										success: function(response) {
												Swal.fire(
														'Migrasi!',
														'Umur Order Berhasil Diperbaharui',
														'success'
														)
												location.reload();
										}
								});
						}
				});
		});

		$('.ask_reactive').on('click', function () {
				var valuen = $(this).attr('data-id');
				console.log(valuen);
				Swal.fire({
						title: 'Seriusan?',
						text: "Regu ini akan diaktifkan kembali!",
						type: 'warning',
						showCancelButton: true,
						confirmButtonColor: '#3085d6',
						cancelButtonColor: '#d33',
						confirmButtonText: 'Ya, Aktifkan!'
				}).then((result) => {
						if (result.value) {
								$.ajax({
										type: "GET",
										url: "/regu/reactive/"+valuen,
										cache: false,
										success: function(response) {
												Swal.fire(
														'Aktif!',
														'Regu Sudah Aktif!',
														'success'
														)
												location.reload();
										}
								});
						}
				});
		});

		$('.liat_dunk').on('click', function(e){
			value = $(this).attr('data-id'),
			res = value.toUpperCase();
			$.ajax({
				type: 'GET',
				data: {data : value, angka : 0},
				url : "/admin/get_odp/LY",
				beforeSend: function() {
					$("#parent").toggleClass('preloader');
					$("#child").toggleClass('cssload-speeding-wheel');
					$("#parent").css({
						'background-color' : 'rgba(0,0,0,.8)'
					});
				},
				success: function(data) {
					$("#parent").toggleClass('preloader');
					$("#child").toggleClass('cssload-speeding-wheel');
					$("#parent").css({
						'background-color' : ''
					});
					if(data.lt_status == 'Selesai'){
						$.ajax({
							url:"/Download/pdf/"+data.id,
							type:'HEAD',
							error: function()
							{
							},
							success: function()
							{
								detch_download_rfc.appendTo( "#download_file_rfc" );
								$('.download_file_pdf').attr("href", '/Download/pdf/'+data.id);
							}
						});
					}
					$.ajax({
						url:"/Download/file/"+data.id,
						type:'HEAD',
						error: function(e)
						{
						},
						success: function()
						{
							detch_download_phto.appendTo( "#download_photo_" );
							$('.download_zip_photo').attr("href", "/Download/file/"+data.id);

							detch_abd.appendTo( "#download_photo_" );
							$('.download_abd_file').attr("href", "/Download/abd/"+data.id);
						}
					});

					var today = new Date().getTime(),
					raw_today = new Date(),
					Christmas = new Date(data.tgl_pengerjaan).getTime(),
					raw_Christmas = new Date(data.tgl_pengerjaan),
					total_kerja = Math.floor((raw_today - raw_Christmas) / 86400000),
					work = total_kerja;
					console.log(raw_Christmas, raw_today)
					var HOLIDAYS = [new Date(2015,1-1,1).getTime(), new Date(2015,1-1,26).getTime(),
					new Date(2015,4-1,3).getTime(), new Date(2015,4-1,6).getTime(),
					new Date(2015,4-1,25).getTime(), new Date(2015,12-1,25).getTime(),
					new Date(2015,12-1,26).getTime()],
					d = raw_Christmas;
					while (d <= raw_today) {
						if ((d.getDay() || 7) > 6) {
							work--;
						}
						else if ($.inArray(d.getTime(), HOLIDAYS) > -1) {
							work--;
						}
						d.setDate(d.getDate() + 1);
					}
					var diffMs = (today - Christmas - ((total_kerja - work) * 86400000)),
					diffDays = convertMiliseconds(diffMs, 'd');
					console.log(diffMs, diffDays)
					if(data.lt_status != 'Selesai' || data.lt_status !== null){
						detch_del.appendTo( "#delete_mine" );
						$('.layout_del').attr('data-id', data.id);
					}

				},
				error: function(e){
				}
			});

			$.ajax({
				type: 'GET',
				data: {data : value, angka : 1},
				url : "/admin/get_odp/LY",
				success: function(data) {
					$('#detail_layout').modal('show');
					$('#nama_project').text('Detail Data');
					$('#content').html(data);
				},
				error: function(e){
					console
				}
			});
		});

		$('#delete_mine').on('click', function () {
			var valuen = $('.layout_del').attr('data-id');

			Swal.fire({
				title: 'Seriusan?',
				text: "Tidak akan bisa dikembalikan jika terhapus",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Ya, hapus!'
			}).then((result) => {
				if (result.value) {
					$.ajax({
						type: "GET",
						data: {data : valuen},
						url: "/admin/delete/mydispatch&",
						cache: false,
						success: function(response) {
							Swal.fire(
								'Terhapus!',
								'Orderan berhasil terhapus',
								'success'
								)
							$('#detail_layout').trigger('click.dismiss.bs.modal')
							location.reload();
						}
					});
				}
			});
		});
	});
</script>
@endsection