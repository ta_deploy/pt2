<style>
	.sticky_bitch {
		position: -webkit-sticky;
		position: sticky;
		top: 0;
	}
</style>
<div class="row col-md-12" style="height : 480px;">
	<div class="col-md-6">
		<div class="row sticky_bitch">
			<div class="col-md-8">
				<div class="panel panel-primary">
					<div class="panel-heading">Data Optima</div>
					<div class="panel-body">
						<div class="form-group-sm-sm col-md-12">
							<label class="control-label" for="input-ont">Nomor UNSC : </label>
							<label class="control-label" for="input-ont" id="unsc"><u>{{ !empty($data->scid) ? $data->scid : 'Tidak Ada' }}</u></label>
						</div>
						<div class="form-group-sm col-md-12">
							<label class="control-label" for="input-ont">Nama Project : </label>
							<label class="control-label" for="input-ont"><u>{{ $data->project_name }} </u></label>
						</div>
						<div class="form-group-sm col-md-12">
							<label class="control-label" for="input-ont">Catatan HD : </label>
							<hr style="margin: 5px 0 5px 0;">
							<label class="control-label" for="input-ont">{{ $data->catatan_HD }}</label>
							<hr style="margin: 5px 0 5px 0;">
						</div>
						<div class="form-group-sm col-md-12">
							<label class="control-label" for="input-ont">ID Proaktif : </label>
							<label class="control-label" for="input-ont"><u>{{ $data->proaktif_id }}</u></label>
						</div>
						<div class="form-group-sm col-md-12">
							<label class="control-label" for="input-ont">STO : </label>
							<label class="control-label" for="input-ont"><u>{{ $data->sto }}</u></label>
						</div>
						<div class="form-group-sm col-md-12">
							<label class="control-label" for="input-ont">Status : </label>
							<label class="control-label" for="input-ont"><u>{{ $data->status }}</u></label>
						</div>
						<div class="form-group-sm col-md-12">
							<label class="control-label" for="input-ont">Nama ODP : </label>
							<label class="control-label" for="input-ont" ><u>{{ $data->odp_nama }}</u></label>
						</div>
						@if($data->aspl_nama)
							<div class="form-group-sm col-md-12">
								<label class="control-label" for="input-ont">Nama ASPL : </label>
								<label class="control-label" for="input-ont" ><u>{{ $data->aspl_nama }}</u></label>
							</div>
						@endif
						<div class="form-group-sm col-md-12">
							<label class="control-label" for="input-ont">Koordinat ODC : </label>
							<?php
							if($data->odc_koor){
								if (strpos($data->odc_koor, ',') !== false) {
									$odc = explode(",",$data->odc_koor);
								}elseif (strpos($data->odc_koor, '°') !== false) {
									$odc = explode("°",$data->odc_koor);
								}
							}else{
								$odc[0] = 0;
								$odc[1] = 0;
							}
							?>
							<label class="control-label" for="input-ont"><a href="/geo/find/lat{{ $odc[0] }}/long{{ $odc[1] }}" target="_blank">{{ $data->odc_koor }}</a></label>
						</div>
						<div class="form-group-sm col-md-12">
							<label class="control-label" for="input-ont">Koordinat ODP : </label>
							<?php
							if($data->odc_koor){
								if (strpos($data->odp_koor, ',') !== false) {
									$odp = explode(",",$data->odp_koor);
								}elseif (strpos($data->odp_koor, '°') !== false) {
									$odp = explode("°",$data->odp_koor);
								}
							}else{
								$odp[0] = 0;
								$odp[1] = 0;
							}
							?>
							<label class="control-label" for="input-ont"><a href="/geo/find/lat{{ $odp[0]  }}/long{{ $odp[1] }}" target="_blank">{{ $data->odp_koor }}</a></label>
						</div>
						<div class="form-group-sm col-md-12">
							<label class="control-label" for="input-ont">Regu Yang Mengerjakan : </label>
							<label class="control-label" for="input-ont"><u>{{ $data->regu_name }}</u></label>
						</div>
						@if (!empty($data->lt_status) && $data->lt_status != 'Berangkat' )
						<div class="form-group-sm col-md-12">
							<label class="control-label" for="input-ont">Tanggal Selesai : </label>
							<label class="control-label" for="input-ont"><u>{{ (!empty($data->tgl_selesai) ? $data->tgl_selesai : 'Belum Ada') }}</u></label>
						</div>
						<div class="form-group-sm col-md-12">
							<label class="control-label" for="input-ont">Status Teknisi : </label>
							<label class="control-label" for="input-ont"><u>{{ (!empty($data->lt_status) ? $data->lt_status : 'Belum Ada') }}</u></label>
						</div>
						<div class="form-group-sm col-md-12">
							<label class="control-label" for="input-ont">Nama Project (Teknisi) : </label>
							<label class="control-label" for="input-ont"><u>{{ (!empty($data->lt_project_nama) ? $data->lt_project_nama : 'Belum Ada') }}</u></label>
						</div>
						<div class="form-group-sm col-md-12">
							<label class="control-label" for="input-ont">Koordinat (Teknisi) : </label>
							@if(!empty($data->lt_koordinat_odp))
							<?php
							if (strpos($data->lt_koordinat_odp, ',') !== false) {
								$odp_lt = explode(",",$data->lt_koordinat_odp);
							}elseif (strpos($data->lt_koordinat_odp, '°') !== false) {
								$odp_lt = explode("°",$data->lt_koordinat_odp);
							}elseif (strpos($data->lt_koordinat_odp, ' ') !== false) {
								$odp_lt = explode(" ",$data->lt_koordinat_odp);
							}
							?>
							<label class="control-label" for="input-ont">
								<?php if(preg_match("/[a-z]/i", $data->lt_koordinat_odp)){ ?>
								{{ rawurldecode($data->lt_koordinat_odp) }}
								<?php }else{ ?>
								<a href="/geo/find/lat{{ $odp_lt[0] }}/long{{ $odp_lt[1] }}" target="_blank">{{ $data->lt_koordinat_odp }}</a>
								<?php } ?>
							</label>
							@endif
						</div>
						<div class="form-group-sm col-md-12">
							<label class="control-label" for="input-ont">Catatan Teknisi : </label>
							<hr style="margin: 5px 0 5px 0;">
							<label class="control-label" for="input-ont">{{ (!empty($data->lt_catatan) ? $data->lt_catatan : 'Belum Ada') }}</label>
							<hr style="margin: 5px 0 5px 0;">
						</div>
						@endif
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="panel panel-success">
					<div class="panel-heading">KML</div>
					<div class="panel-body" >
						<div class="input-photos" style="margin: 20px 0">
							<?php
							$number = 1;
							clearstatcache();
							?>
							@foreach($photodispatch as $input)
							<?php
							$check_path = "/upload/pt_2/$data->id";
							$check_path_2 = "/upload/pt_2_2/$data->id";
							$check_path_3 = "/upload/pt_2_3/$data->id";

							if(file_exists(public_path().$check_path)){
								$path = "$check_path/$input";
							}elseif(file_exists(public_path().$check_path_2)){
								$path = "$check_path_2/$input";
							}else{
								$path = "$check_path_3/$input";
							}

							$th   = "$path-th.jpg";
							$img  = "$path.jpg";
							$flag = "";
							$name = "flag_".$input;
							?>
							@if (file_exists(public_path().$th))
							<div class="row col-6 col-sm-3">
								<a href="{{ $img }}" target="_blank">
									<img style="width: 100px; height: 150px; margin-left: 23px;" id="myimg" src="{{ $th }}" alt="{{ $input }}" id="img-{{$input}}"/>
								</a>
								<?php
								$flag = 1;
								?>
								<br />
								<p style="margin-left: 60px;" align="center">{{ str_replace('_',' ',$input) }}</p>
								{!! $errors->first($name, '<span class="label label-danger">:message</span>') !!}
							</div>
							@endif
							<?php
							$number++;
							?>
							@endforeach
						</div>
						<div class="block_download">
							<div class="col-md-12 form-group">
								<a type="button" href="#" class="btn btn-success downlaod_mcore" style="text-align: center; color: #ffffff; display: none;"><span data-icon="f" class="linea-icon linea-elaborate fa-fw" style="font-size: 20px; color: #ffffff; "></span>&nbsp;MCORE</a>
							</div>
							<div class="col-md-12 form-group">
								<a type="button" href="#" class="btn btn-info downlaod_kml" style="text-align: center; color: #ffffff; display: none;"><span data-icon="f" class="linea-icon linea-elaborate fa-fw" style="font-size: 20px; color: #ffffff; "></span>&nbsp;KML</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="panel panel-info">
			<div class="panel-heading">Laporan Teknisi</div>
			<div class="panel-body">
				<div class="row">
					<?php
					$number = 1;
					clearstatcache();
					?>
					@foreach($photo as $input)
					<?php
					$check_path = "/upload/pt_2/teknisi/$data->id";
					$check_path_2 = "/upload/pt_2_2/teknisi/$data->id";
					$check_path_3 = "/upload/pt_2_3/teknisi/$data->id";

					if(file_exists(public_path().$check_path)){
						$path = "$check_path/$input";
					}elseif(file_exists(public_path().$check_path_2)){
						$path = "$check_path_2/$input";
					}else{
						$path = "$check_path_3/$input";
					}
					$th   = "$path-th.jpg";
					$img  = "$path.jpg";
					$nt_panel = "$path-panel.txt";
					$nt   = "$path-catatan.txt";
					$flag = "";
					$name = "flag_".$input;
					?>
					@if (file_exists(public_path().$th))
					<div class="col-6 col-sm-3">
						<a href="{{ $img }}" target="_blank">
							<img style="margin: 0 auto; width: 100px; height: 150px;" src="{{ $th }}" alt="{{ $input }}" />
						</a>
						<?php
						$flag = 1;
						?>
						<br />
						<p>{{ str_replace('_',' ',$input) }}</p>
						@if($input != 'Port_Feeder' && $input != 'ODC')

						@if (file_exists(public_path().$nt))
						<?php $note = File::get(public_path("$path-catatan.txt")); ?>
						@else
						<?php $note ='' ?>
						@endif
						<textarea readonly class="form-control foto" id="catatan_{{$input}}" name="catatan_{{$input}}">{{$note}}</textarea>
						@else

						@if (file_exists(public_path().$nt_panel))
						<?php $note_panel = File::get(public_path("$path-panel.txt"));
						$note_port = File::get(public_path("$path-port.txt")); ?>
						@else
						<?php $note_panel = '';
						$note_port = '';?>
						@endif
						<div class="form-group row" style="margin-bottom: 23px;">
							<div class="col-5 col-md-5">
								<label class="control-label pull-left" for="input-ont">Panel</label>
							</div>
							<div class="col-7 col-md-7">
								<input readonly type="text" class="form-control" id="panel_{{$input}}" style="height: 25px; border: 3px solid white;" name="panel_{{$input}}" value="{{$note_panel}}">
							</div>
						</div>
						<div class="form-group row " style="margin-bottom: 0px; margin-top: -3.56px;">
							<div class="col-5 col-md-5">
								<label class="control-label pull-left" for="input-ont">Port</label>
							</div>
							<div class="col-7 col-md-7">
								<input readonly type="text" class="form-control" id="port_{{$input}}" style="height: 25px; border: 3px solid white;" name="port_{{$input}}" value="{{$note_port}}">
							</div>
						</div>
						@endif
						{!! $errors->first($name, '<span class="label label-danger">:message</span>') !!}
						<br />
					</div>
					@endif
					<?php
					$number++;
					?>
					@endforeach
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$(function () {
		var value = <?= json_encode($data) ?>;
		if(value.scid != null){
			$.ajax({
				type: 'Get',
				url : "/admin/get/odp/"+value.scid+"/orderan unsc",
				success: function(data) {
					/*console.log(data)*/
					if(data.kml != null && data.kml.length > 0){
						if (data.kml.indexOf(".PNG") > -1){
							$('#img-KML').attr('src',"/upload/files/"+data.kml);
						}else{
							$('.downlaod_kml').css({display: "block"});
							$('.downlaod_kml').attr('href',"/upload/files/"+data.kml);
						}
					}

					if(data.mcore != null && data.mcore.length > 0){
						/*console.log(data.mcore);*/
						$('.downlaod_mcore').css({display: "block"});
						$('.downlaod_mcore').attr('href',"/upload/files/"+data.mcore);
					}

				},
				error: function(e){
					/*console.log(e);*/
				}
			});
		}
	});
</script>