<table id="teknisi"  class="table table-striped">
	<thead>
		<tr>
			<th>#</th>
			<th>Node</th>
			@foreach($head as $h)
			<th>{{ $h }}</th>
			@endforeach
		</tr>
	</thead>
	<tbody>
		@if(!empty($data))
		@foreach($data as $no => $r)
		<tr>
			<td>{{ ++$no }}</td>
			<td>{{ $r['odp_nama'] }}</td>
			@foreach($r['id_item'] as $no2 => $item)
			<td>{!! $item ?"<span class=\"label label-info\">$item</span>": '-' !!}</td>
			@endforeach
		</tr>
		@endforeach
		@else
		<td colspan="2">Tidak Ada Data Yang Tersedia</td>
		@endif
	</tbody>
</table>