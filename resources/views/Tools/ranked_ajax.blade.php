<div class="table-responsive">
    <table id="teknisi" class="table table-striped">
        <thead>
            <tr>
                <th>#</th>
                <th>Regu</th>
                <th>Team Leader</th>
                <th>Jumlah Naik</th>
                <th>Jumlah Kendala</th>
                <th>Persentase</th>
                <th>Point</th>
            </tr>
        </thead>
        <tbody>
            @php
            $selesai =$Kendala = $points  = $totals  = 0;
            @endphp
            @foreach($ranked as $no => $r)
            <?php
            $point = $r->selesai * 25;
            $selesai += $r->selesai;
            $Pending += $r->Pending;
            $Kendala += $r->Kendala;
            $points += $point;
            $total = (($r->selesai * 100) + ($r->Kendala * 30)) / ($r->selesai + $r->Kendala);
            $totals += $total;
            ?>
            <tr>
                <td>{{ ++$no }}</td>
                <td>{{ $r->uraian }}</td>
                <td>{{ $r->TL == 0 ? 'Tidak Ada TEAM LEADER' : $r->TL }}</td>
                <td>{{ $r->selesai }}</td>
                <td>{{ $r->Pending }}</td>
                <td>{{ $r->Kendala }}</td>
                <td>{{ $point?:'-' }}</td>
                <td>{{ number_format((float)$total, 2, ',', '') }}%</td>
            </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <td colspan="3">Total Keseluruhan</td>
                <td>{{ $selesai }}</td>
                <td>{{ $Pending }}</td>
                <td>{{ $Kendala }}</td>
                <td colspan="2">{{ $points }}</td>
            </tr>
        </tfoot>
    </table>
</div>