@extends('layout')
@section('title', 'List User Jointer')
@section('style')
<style type="text/css">
    th, td{
        text-align: center;
        white-space:nowrap;
    }

    .panel .panel-action a{
        opacity: 1;
    }
</style>
@endsection
@section('content')
<div class="container-fluid" style="padding-top: 25px;">
  <div class="panel">
    <div class="panel-heading">List Team Jointer</div>
    <div class="panel-body">
      <ul class="nav nav-tabs">
        @php
          $first = TRUE;
        @endphp
        @foreach ($list as $k => $v)
          @php
            $cls = '';

            if($first == TRUE)
            {
              $cls = 'active';
              $first = FALSE;
            }
          @endphp
          <li class="{{ $cls }}">
            <a href="#tab_{{ $k }}" class="nav-link" role="tab" data-toggle="tab">Witel {{ $k }}<span class="label label-rouded label-danger pull-right">{{ count($v) }}</span></a>
          </li>
        @endforeach
      </ul>
      <div class="tab-content">
        @php
          $first = TRUE;
        @endphp
        @foreach ($list as $k => $v)
          @php
            $no = 0;
            array_map(function($x) use(&$no){
              foreach($x['list'] as $vx)
              {
                $no++;
              }
            }, $v);

            $get_nok = array_filter($v, function($x){
              return $x['status'] == 'nok';
            });

            $get_kosong = array_filter($v, function($x){
              return $x['status'] == 'unknown';
            });

            $get_no_list = array_filter($v, function($x){
              return count($x['list']) == 0;
            });

            $get_waspang_no_active = array_filter($v, function($x){
              return count($x['ACTIVE']) == 0;
            });
          @endphp
          @php
            $cls = '';

            if($first == TRUE)
            {
              $cls = 'in active';
              $first = FALSE;
            }
          @endphp
          <div id="tab_{{ $k }}" class="tab-pane fade {{ $cls }}">
            <h1>LIST USER JOINTER SEKTOR {{ $k }} <span style="color: #03a9f3">{{ count($v) - count(@$get_no_list) }} Waspang</span> Dan <span style="color: red">{{ $no }} Unit</span></h1>
            <div class="row">
              @if($get_nok)
                <div class="col-md-4">
                  <h4><u>Waspang Yang Tidak Terdaftar ({{ count($get_nok) }} Buah)</u></h4>
                  <ul>
                    @foreach($get_nok as $kx => $vx)
                      <li style="color: red;"><b>{{ $kx }}</b></li>
                    @endforeach
                  </ul>
                </div>
              @endif
              @if(@$get_kosong['Kosong'])
                <div class="col-md-4">
                  <h4><u>Teknisi Jointer Tanpa Waspang ({{ count($get_kosong['Kosong']['list']) }} Buah)</u></h4>
                  <ul>
                    @foreach($get_kosong['Kosong']['list'] as $vx)
                      <li style="color: red;"><b>{{ $vx['nama'] }}</b> ({{ $vx['nik'] }})</li>
                    @endforeach
                  </ul>
                </div>
              @endif
              @if($get_no_list)
                <div class="col-md-4">
                  <h4><u>Waspang Tanpa Teknisi Jointer ({{ count($get_no_list) }} Buah)</u></h4>
                  <ul>
                    @foreach($get_no_list as $vx)
                      <li style="color: red;"><b>{{ $vx['nama'] }}</b> ({{ $vx['nik'] }}) / <b style="color: {{ $vx['ACTIVE'] == 1 ? '#03a9f3' : '#ff6849' }};"><u>{{ $vx['ACTIVE'] == 1 ? 'Aktif' : 'Belum Aktif' }}</u></b></li>
                    @endforeach
                  </ul>
                </div>
              @endif
              @if($get_waspang_no_active)
                <div class="col-md-4">
                  <h4><u>Waspang Belum Aktivasi ({{ count($get_waspang_no_active) }} Buah)</u></h4>
                  <ul>
                    @foreach($get_waspang_no_active as $vx)
                      <li style="color: red;"><b>{{ $vx['nama'] }}</b> ({{ $vx['nik'] }}) / <b style="color: {{ $vx['ACTIVE'] == 1 ? '#03a9f3' : '#ff6849' }};"><u>{{ $vx['ACTIVE'] == 1 ? 'Aktif' : 'Belum Aktif' }}</u></b></li>
                    @endforeach
                  </ul>
                </div>
              @endif
            </div>
            @foreach($v as $k => $data)
              <div class="panel panel-info">
                @if($data['list'])
                  <div class="panel-heading" style="color: {{ in_array(0, array_column($data['list'], 'ACTIVE') ) ? 'red' : '' }}">Waspang: {{ $data['nama'] }} ({{ $data['nik'] }}) <b>{{ count($data['list']) }} Buah</b>
                    <div class="panel-action">
                      <a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a>
                    </div>
                  </div>
                  <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                      @foreach ($data['list'] as $regu)
                        <div class="col-md-4" style="margin-bottom: 10px;">
                          <div class="card text-center card-custom">
                            <div class="card-block" style="background-color: {{ $regu['ACTIVE'] == 1 ? '' : 'red' }}; color: {{ $regu['ACTIVE'] == 1 ? '' : 'white' }};">
                              <h4 class="card-title" style="color: {{ $regu['ACTIVE'] == 1 ? '' : 'white' }};"><u>{{ $regu['nama']}} ({{ $regu['nik']}})</u></h4>
                              <p class="card-text" style="text-align: left;">
                                Witel: <b>{{ $regu['Witel_New'] }}</b>
                                <br/>
                                Status: <b>{{ $regu['ACTIVE'] == 1 ? 'SUDAH AKTIVASI' : 'BELUM AKTIVASI' }}</b>
                                <br/>
                                Register: {{ $regu['dateUpdate'] }}
                              </p>
                            </div>
                          </div>
                        </div>
                      @endforeach
                    </div>
                  </div>
                @endif
              </div>
            @endforeach
          </div>
        @endforeach
      </div>
    </div>
  </div>
</div>
@endsection
@section('footerS')
<script type="text/javascript">
  $(function(){
    $(".nav-tabs a").click(function(){
      $(this).tab('show');
    });

  });
</script>
@endsection
