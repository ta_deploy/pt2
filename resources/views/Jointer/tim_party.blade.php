@extends('layout')
@section('title', 'List Party')
@section('headerS')
<meta name="viewport" content="width=device-width, initial-scale=1">
@endsection
@section('style')
<style type="text/css">

</style>
@endsection
@section('content')
<div class="container-fluid" style="padding-top: 25px;">
  @if (Session::has('alerts_tele'))
    @foreach(Session::get('alerts_tele') as $alert)
      <div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
    @endforeach
  @endif
  <div class="form-group">
    @if(session('auth')->pt2_level == 0)
      @if(count($data_party) == 0)
        @if ($check_regu_exists)
          <a type="button" class="btn btn-sm btn-info" href="/jointer/sync_regu"><i data-icon="Y" class="linea-icon linea-basic fa-fw" id="contactG" style="font-size: 17px; top: 5px;"></i>&nbsp;Sinkron Regu</a>
        @else
          <a type="button" class="btn btn-sm btn-info" href="/jointer/create_party"><i data-icon="Y" class="linea-icon linea-basic fa-fw" id="contactG" style="font-size: 17px; top: 5px;"></i>&nbsp;Bentuk Tim</a>
        @endif
      @elseif($status == 'leader')
          <a type="button" class="btn btn-sm btn-warning" href="/jointer/edit_party"><i data-icon="U" class="linea-icon linea-basic fa-fw" id="contactG" style="font-size: 17px; top: 5px;"></i>&nbsp;Ubah Tim</a>
          <a type="button" class="btn btn-sm btn-danger pop_up" data-title="Apakah Anda Yakin Untuk Membubarkan Tim?" data-text="Seluruh Order Akan Hilang!" href="/jointer/dismiss_party"><i data-icon="&#xe01c;" class="linea-icon linea-basic fa-fw" id="contactG" style="font-size: 17px; top: 5px;"></i>&nbsp;Bubar Tim</a>
      @endif
    @endif
    @if($check_status)
      @if($check_status->status == 0)
        <div class="alert alert-info">
          User <b>{{ $check_status->leader_name }} ({{ $check_status->leader_nik }})</b> Mengundang Anda Kedalam Tim <b>{{ $check_status->party_name }}</b>
          <a type="button" class="btn btn-sm btn-success btn-rounded" href="/jointer/unit_join/1"><i data-icon="&#xe006;" class="linea-icon linea-basic fa-fw" id="contactG" style="font-size: 17px; top: 5px;"></i>&nbsp;Terima</a>&nbsp;
          / <a type="button" class="btn btn-sm pop_up btn-warning btn-rounded" href="/jointer/unit_join/0"><i data-icon="&#xe016;" class="linea-icon linea-aerrow fa-fw" id="contactG" style="font-size: 17px; top: 5px;"></i>&nbsp;Tolak</a>
        </div>
      @endif
      @if($check_status->status == 1)
        <a type="button" data-title="Apakah Anda Yakin Untuk Keluar Dari Tim?" data-text="Seluruh Order Akan Hilang!" class="btn btn-sm btn-warning pop_up btn-rounded" href="/jointer/out_team"><i data-icon="!" class="linea-icon linea-elaborate fa-fw" id="contactG" style="font-size: 17px; top: 5px;"></i>&nbsp;Keluar</a>
      @endif
    @endif
  </div>
  @if(count($data_party) != 0 && $status == 'leader' || $check_status && $check_status->status == 1)
    <div class="panel panel-warning">
      <div class="panel-heading">List Team Jointer</div>
      <div class="panel-body">
        <ul class="nav nav-tabs">
          <li class="active">
            <a href="#current_party" class="nav-link" role="tab" data-toggle="tab">Party Sekarang<span class="label kerja label-rouded label-default pull-right"></span></a>
          </li>
          <li>
            <a href="#history_party" class="nav-link" role="tab" data-toggle="tab">Log Party<span class="label pending label-rouded label-green pull-right"></span></a>
          </li>
        </ul>
        <div class="tab-content">
          <div id="current_party" class="tab-pane fade">
            <div class="page-wrap">
              <div class="form-group">
              </div>
              <table class="table toggle-circle table-hover table-bordered" >
                <thead>
                  <tr>
                    <th>Nama Tim</th>
                    <th>Nama Ketua</th>
                    <th>Nama</th>
                    <th>Status</th>
                    <th>Tanggal Masuk</th>
                  </tr>
                </thead>
                <tbody>
                  @forelse($data_party as $v)
                    @php
                      $first = TRUE;
                    @endphp
                    @foreach($v['list'] as $vv)
                      <tr>
                        @if($first)
                          <td rowspan="{{ count($v['list']) }}" style="vertical-align: middle;">{{ $vv->party_name }}</td>
                          <td rowspan="{{ count($v['list']) }}" style="vertical-align: middle;">{{ $vv->leader_name }} ({{ $vv->leader_nik }})</td>
                        @endif
                          <td>{{ $vv->unit_name }} ({{ $vv->unit_nik }})</td>
                          <td>
                            @switch($vv->status)
                              @case(1)
                                <span class="label label-primary">Diterima</span>
                              @break
                              @case(99)
                                <span class="label label-success">Ketua Tim</span>
                              @break
                              @case(3)
                                <span class="label label-info">Bergabung</span> / <a href="/jointer/approve/{{ $vv->unit_nik }}" style="color: white;" class="label label-primary"><i data-icon="E" class="linea-icon linea-elaborate fa-fw" id="contactG" style="font-size: 17px; top: 5px;"></i>&nbsp;Terima</a>
                              @break
                              @default
                              <span class="label label-red">Menunggu Di Terima</span>
                            @endswitch
                          </td>
                          @if($first)
                            <td rowspan="{{ count($v['list']) }}" style="vertical-align: middle;">{{ $vv->created_at }}</td>
                            @php
                              $first = FALSE;
                            @endphp
                          @endif
                      </tr>
                    @endforeach
                  @empty
                  @endforelse
                </tbody>
              </table>
            </div>
          </div>
          <div id="history_party" class="tab-pane fade">
            <div class="page-wrap">
              <div class="form-group">
              </div>
              <table class="table toggle-circle table-hover table-bordered" >
                <thead>
                  <tr>
                    <th>Nama</th>
                    <th>Tanggal</th>
                    <th>Status</th>
                    <th>Keterangan</th>
                  </tr>
                </thead>
                <tbody>
                  @forelse($get_log_party as $v)
                    <tr>
                      <td>{{ $v->nama }} ({{ $v->nik }})</td>
                      <td>{{ $v->created_at }}</td>
                      @php
                        switch ($v->kondisi) {
                          case 2:
                            $txt = 'Join';
                          break;
                          case 3:
                            $txt = 'Keluar';
                          break;
                          case 4:
                            $txt = 'Dikeluar';
                          break;
                          default:
                            $txt = 'Membuat Tim';
                        }
                      @endphp
                      <td>{{ $txt }}</td>
                      <td>{{ $v->keterangan ??  'User '. $txt . ' Tanggal ' . $v->created_at }}</td>
                    </tr>
                  @empty
                  @endforelse
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  @else
    @if(is_null($check_status) )
      @if(!$check_regu_exists)
        <div class="panel panel-warning">
          <div class="panel-heading">Gabung Team Jointer</div>
          <div class="panel-body">
            <table class="table toggle-circle table-hover table-bordered" >
              <thead>
                <tr>
                  <th>Nama Tim</th>
                  <th>Nama Leader</th>
                  <th>Nama</th>
                  <th>Regu</th>
                  <th>Tanggal Masuk</th>
                  <th>Pekerjaan</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @forelse($data_all_party as $v)
                  @php
                    $first = TRUE;
                  @endphp
                  @foreach($v['list'] as $vv)
                    <tr>
                      @if($first)
                        <td rowspan="{{ count($v['list']) }}" style="vertical-align: middle;">{{ $vv->party_name }}</td>
                        <td rowspan="{{ count($v['list']) }}" style="vertical-align: middle;">{{ $vv->leader_name }} ({{ $vv->leader_nik }})</td>
                      @endif
                      <td>{{ $vv->unit_name }} ({{ $vv->unit_nik }})</td>
                      <td>{{ $vv->uraian ?? '-' }}</td>
                      @if($first)
                        <td rowspan="{{ count($v['list']) }}" style="vertical-align: middle;">{{ $vv->created_at }}</td>
                      @endif
                      <td>-</td>
                      @if($first)
                        @if(count($v['list']) <= 3)
                          <td rowspan="{{ count($v['list'])  }}" style="vertical-align: middle;">
                            <a type="button" class="btn btn-light" style="color: #03a9f3;" href="/jointer/join_party/{{ $vv->leader_nik }}"><i data-icon="8" class="linea-icon linea-basic fa-fw" id="contactG" style="font-size: 17px; top: 5px;"></i>&nbsp;Gabung Tim</a>
                          </td>
                        @else
                          Tim Sudah Memenuhi Kuota (4 ORANG)
                        @endif
                        @php
                          $first = FALSE;
                        @endphp
                      @endif
                    </tr>
                  @endforeach
                @empty
                @endforelse
              </tbody>
            </table>
          </div>
        </div>
      @endif
    @elseif($check_status->status == 0)
      <h2>Daftar Tim Tidak Akan Dimunculkan Sebelum Menerima/Menolak Bergabung Dengan Tim Sebelumnya!</h2>
    @else
      <div class="alert alert-info">Menunggu Approve Oleh Tim Party {{ $check_status->leader_name }} ({{ $check_status->leader_nik }})<a href="/jointer/batal_gabung/{{ session('auth')->id_user }}" class="btn btn-rounded btn-sm btn-warning"><i data-icon="G" class="linea-icon linea-basic fa-fw" id="contactG" style="font-size: 17px; top: 5px;"></i>&nbsp;Batal Gabung?</a></div>
    @endif
  @endif
</div>
@endsection
@section('footerS')
<script type="text/javascript">
  $(function() {
    $(".nav-tabs a").click(function(){
      $(this).tab('show');
    });

    $('#current_party').toggleClass("in active");

    $('.pop_up').on('click', function(e){
      e.preventDefault()
      var title = $(this).attr('data-title'),
      href = $(this).attr('href'),
      text = $(this).attr('data-text');

      Swal.fire({
        title: title,
        text: text,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, Saya Mengerti!!'
      }).then((result) => {
        if(result.isConfirmed){
          window.location.href = href;
        }
      });
    });
  });
</script>
@endsection