@extends('layout')
@section("title", 'Create Party')
@section('headerS')
<link href="/bower_components/bootstrap-switch/bootstrap-switch.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css" integrity="sha512-nMNlpuaDPrqlEls3IX/Q56H36qvBASwb3ipuo3MxeWbsQB1881ox0cRv7UPTgBlriqoynt35KjEwgGUeUXIPnw==" crossorigin="anonymous" />
@endsection
@section('content')
@include('Partial.alerts')
@if (Session::has('alerts_tele'))
  @foreach(Session::get('alerts_tele') as $alert)
    <div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
  @endforeach
@endif
<div class="container-fluid" style="padding-top: 25px;">
	<form method="post" id="input_hero">
		<div class="panel panel-info">
			<div class="panel-heading">Buat Party</div>
			<div class="panel-body">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="row col-md-12" style="margin-bottom: 120px;">
          <div class="col-md-6">
            <label>Nama Party</label>
					  <input type="text" class="form-control" id="party_name" name="party_name" required placeholder="Masukkan Nama Tim" value="{{ old('party_name', $data->party_name ?? '') }}">
          </div>
          <div class="col-md-6">
            <label>Rekan Tim</label>
            <select id="unit_id" name="unit_id[]" class="form-control" multiple="" required>
              @foreach($get_jointer_nik as $v)
                <option value="{{ $v['id'] }}">{{ $v['text'] }}</option>
              @endforeach
            </select>
          </div>
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-block btn-info bt_sv" style="text-align: center;"><span data-icon="M" class="linea-icon linea-basic fa-fw" style="font-size: 20px; color: #ffffff; "></span>&nbsp;Simpan Perubahan</button>
				</div>
			</div>
		</div>
	</form>
</div>
@endsection
@section('footerS')
<script src="/bower_components/bootstrap-switch/bootstrap-switch.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js" integrity="sha512-2ImtlRlf2VVmiGZsjm9bEyhjGW4dU7B6TNwh/hx/iSByxNENtj3WVE6o/9Lj4TJeVXPi4bnOIMXFIJJAeufa0A==" crossorigin="anonymous"></script>
<script>
	$(function(){
    $('#unit_id').select2({
      width: '100%',
      placeholder: 'Rekan Bisa Dipilih Lebih Dari Satu',
      maximumSelectionLength: 3,
    });

    var load_data = {!! json_encode($load_data_tim ?? null) !!};

    if(load_data)
    {
      $('#party_name').val(load_data[0].party_name);

      var load_nik = [];
      $.each(load_data, function(k, v){
        load_nik.push(v.unit_nik);
      })

      $('#unit_id').val(load_nik).change();
    }
  });
</script>
@endsection