@extends('layout')
@section('title', 'Rekap Bulanan')
@section('headerS')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link href="/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
@endsection
@section('style')
<style type="text/css">
th, td{
	text-align: center;
}
div>table {
	float: left;
}
.down{
	-moz-transform:rotate(90deg);
	-webkit-transform:rotate(90deg);
	transform:rotate(90deg);
}
.rounded{
	border-radius: 10px 60px 60px 60px;
}
</style>
@endsection
@section('content')
<div class="container-fluid" style="padding-top: 25px;">
	<button type="button" class="btn btn-default" id="button_photo"><span data-icon="[" class="linea-icon linea-basic fa-fw" style="font-size: 20px; vertical-align:middle;" ></span>&nbsp;Kirim Data Bulan Ini Di Group Telegram PT2 <span id="send_photo"></span></button>
	<div style="padding-top: 25px;">
		<div class="modal fade photo_rekap photo_rekap_detail modal_lg" id="photo_rekap_detail">
			<div class="modal_lg" role="document" style="width: 95%; margin: auto; margin-top: 10px;">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel" style="width: 400px;"><div id="odp_nama"></div></h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body" style="overflow-y:auto;padding: 0px 0px 0px 0px;">
						<div id="Konten"></div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
			<div id="report_more_daily">
				<div class="row justify-content-center">
					<div class="col col-md-6">
						<div class="table-responsive" style="padding-bottom: 10px;">
							<table class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>{{ $bulan }}</th>
										<th>Sektor</th>
										<th>Kendala</th>
										<th>Selesai</th>
										<th>Ogp</th>
										<th>Pending</th>
										<th>Total</th>
									</tr>
								</thead>
								@php
									$sum_selesai= $sum_kendala= $sum_pending= $sum_ogp= $sum_total=0;
								@endphp
								<tbody>
									@foreach($rekap_rinci as $data)
										@foreach($data as $rekap)
										<tr>
											<td>{{ $rekap->tgl ?: '-' }}</td>
											<td>{{ $rekap->sto_n ?: '-' }}</td>
											<td>{{ $rekap->kendala ?: '-' }}</td>
											<td>{{ $rekap->Selesai ?: '-' }}</td>
											<td>{{ $rekap->ogp ?: '-' }}</td>
											<td>{{ $rekap->pending ?: '-' }}</td>
											<td>{{ $rekap->total ?: '-' }}</td>
										</tr>
										@php
											$sum_selesai+=$rekap->Selesai;
											$sum_kendala+=$rekap->kendala;
											$sum_pending+=$rekap->pending;
											$sum_ogp+=$rekap->ogp;
											$sum_total+=$rekap->total;
										@endphp
										@endforeach
									@endforeach
								</tbody>
								<tfoot>
									<tr>
										<td colspan="2">Grand Total</td>
										<td id="kendala" data-kendala="{{ $sum_kendala }}">{{ $sum_kendala?:'-' }}</td>
										<td id="selesai" data-selesai="{{ $sum_selesai }}">{{ $sum_selesai?:'-' }}</td>
										<td id="ogp" data-ogp="{{ $sum_ogp }}">{{ $sum_ogp?:'-' }}</td>
										<td id="pending" data-pending="{{ $sum_pending }}">{{ $sum_pending?:'-' }}</td>
										<td id="total" data-pending="{{ $sum_total }}">{{ $sum_total?:'-' }}</td>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
			@foreach($detail_data as $no => $data)
			@foreach($data as $tanggal => $redata)
			<div class="panel rounded panel-info" style="margin-top: 20px;">
				<div class="table-responsive " id="teknisi" style="padding-bottom: 3px;">
					<p style="cursor: pointer;" class="label rounded label-success css_style no_{{$tanggal}}" data-number="{{ $tanggal }}" data-toggle="collapse" href="#collapse{{$tanggal}}" role="button" aria-expanded="false" aria-controls="collapse{{$tanggal}}">{{$tanggal}}<i class="fa fa-chevron-right rotate{{ $tanggal }}"></i></p>
					<div class="collapse multi-collapse" id="collapse{{$tanggal}}">
						<div class="panel-body">
							<div class="form-group">
								<input type="text" class="form-control" class="search" data-tanggal="{{ $tanggal }}" placeholder="Masukkan ODP tanggal {{ $tanggal }} Untuk Mencari Data">
							</div>
							<table class="table table-bordered">
								<thead>
									<tr>
										<th>Nama Proyek</th>
										<th>Sto</th>
										<th>Nama ODP</th>
										<th>Koordinat ODP</th>
										<th>Nama Regu</th>
										<th>Status</th>
										<th>Catatan Teknisi</th>
										<th>Foto</th>
									</tr>
								</thead>
								<tbody>
									@foreach($redata as $number => $value)
									<tr>
										<td>{{ $value->project_name }}</td>
										<td>{{ $value->sto }}</td>
										<td>{{ $value->odp_nama }}</td>
										<td>{{ $value->lt_koordinat_odp }}</td>
										<td>{{ $value->regu_name }}</td>
										<td>{{ $value->lt_status }}</td>
										<td>{{ $value->lt_catatan }}</td>
										<td><a type="button" style="color: #6bacd5;" data-id="{{ $value->odp_nama }}" data-id_pt2="{{ $value->id }}" class="btn btn-light show_detail_photo"><span data-icon="^" class="linea-icon linea-basic fa-fw" style="font-size: 27px; bottom: 10px;"></span></a></td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			@endforeach
			@endforeach
	</div>
</div>
@endsection
@section('footerS')
<script src="/bower_components/moment/min/moment.min.js"></script>
<script src="/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script>
	function startTimer(duration, display) {
		var timer = duration, minutes, seconds;
		var settan = setInterval(function () {
			minutes = parseInt(timer / 60, 10)
			seconds = parseInt(timer % 60, 10);

			minutes = minutes < 10 ? "0" + minutes : minutes;
			seconds = seconds < 10 ? "0" + seconds : seconds;

			display.text('(' + minutes + ":" + seconds + ')');

			if (--timer < 0) {
				timer = duration;
			}

			if (timer == 0) {
				clearInterval(settan);
				display.text('');
				$('#button_photo').attr('disabled', false);
				$( "#button_photo" ).css({
					border: ""
				});
			}
			if (timer != 0) {
				$('#button_photo').attr('disabled', true);
			}
		}, 1000);
	}

	$(function(){

		$(".show_detail_photo").click(function(){
			var value = $(this).attr('data-id');
			var id_pt2 = $(this).attr('data-id_pt2');
			var res = value.toUpperCase();
			$.ajax({
				type: 'GET',
				url : "/matrix/photo/"+id_pt2,
				success: function(data) {
					/*console.log(data);*/
					$('#photo_rekap_detail').modal('show');
					$('#odp_nama').text('Detail Foto ODP '+res);
					$('#Konten').html(data);
				},
				error: function(e){
					/*console.log(e);*/
				}
			});
		});

		$('#button_photo').on('click', function(){
			var fiveMinutes = 60 * 1,
			display = $('#send_photo');
			startTimer(fiveMinutes, display);
			$(this).attr('disabled', true);
			$(this).css({
				border: "2px solid #C83C36FF"
			});
		});

		$(".css_style").click(function () {
			var data = $(this).attr('data-number');
			/*console.log(data);*/
			$(".rotate"+data).toggleClass("down");
			$(".rotate"+data).css({
				"-moz-transition":"all .5s linear",
				"-webkit-transition":"all .5s linear",
				'transition': 'all .5s linear'
			});
		});

	});
</script>
@endsection