@extends('layout')
@section('title', 'Umur Jointer')
@section('headerS')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link href="/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
@endsection
@section('style')
<style type="text/css">
	th, td{
		text-align: center;
		white-space:nowrap;
	}
	div>table {
		float: left
	}
</style>
@endsection
@section('content')
<div class="container-fluid" style="padding-top: 25px;">
  <div class="panel panel-info">
    <div class="panel-heading header-date">Umur Pekerjaan</div>
    <div class="panel-body">
      <div class="table-responsive">
        <table id="teknisi" class="table table-striped">
          <thead>
            <tr>
              <th>#</th>
              <th>Mitra</th>
              <th>Regu</th>
              <th>Total Pekerjaan</th>
              <th>Kurang 1 Hari</th>
              <th>1 Sampai 3 Hari</th>
              <th>4 Sampai 7 Hari</th>
              <th>Lebih Dari 7 Hari</th>
            </tr>
          </thead>
          <tbody>
            @foreach($data as $k => $v)
              <tr>
                <td>{{ ++$k }}</td>
                <td>{{ $v['mitra_amija'] }}</td>
                <td style="background-color: {{ $v['ACTIVE'] == 1 ? '' : 'Red' }}; color: {{ $v['ACTIVE'] == 1 ? '' : 'black' }}">{{ $v['regu'] }} {!! $v['ACTIVE'] == 1 ? '' : '|| TIDAK AKTIF' !!}</td>
                <td>
                  @if($v['isi']['total'] == 0)
                    -
                  @else
                    <a href="/report/detail_pt2_age/{{ $v['mitra_amija'] }}/{{ $v['regu_id'] }}/all">{{ $v['isi']['total'] }}</a>
                  @endif
                </td>
                <td>
                  @if($v['isi']['_1'] == 0)
                    -
                  @else
                    <a href="/report/detail_pt2_age/{{ $v['mitra_amija'] }}/{{ $v['regu_id'] }}/_1">{{ $v['isi']['_1'] }}</a>
                  @endif
                </td>
                <td>
                  @if($v['isi']['1_3'] == 0)
                    -
                  @else
                    <a href="/report/detail_pt2_age/{{ $v['mitra_amija'] }}/{{ $v['regu_id'] }}/1_3">{{ $v['isi']['1_3'] }}</a>
                  @endif
                </td>
                <td>
                  @if($v['isi']['4_7'] == 0)
                    -
                  @else
                    <a href="/report/detail_pt2_age/{{ $v['mitra_amija'] }}/{{ $v['regu_id'] }}/4_7">{{ $v['isi']['4_7'] }}</a>
                  @endif
                </td>
                <td>
                  @if($v['isi']['7_'] == 0)
                    -
                  @else
                    <a href="/report/detail_pt2_age/{{ $v['mitra_amija'] }}/{{ $v['regu_id'] }}/7_">{{ $v['isi']['7_'] }}</a>
                  @endif
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection
@section('footerS')
<script src="/bower_components/moment/min/moment.min.js"></script>
<script src="/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script>
	$(function(){

		$('input[name="rangedate"]').daterangepicker({
			opens: 'left'
		}, function(start, end){
			var month1 = start.format('YYYY-MM-DD'),
			month2 = end.format('YYYY-MM-DD')+" 23:59:59";
		});

		$('.kalender').click(function(e){
			e.preventDefault();
			$('input[name="rangedate"]').click();
		});


	});

</script>
@endsection