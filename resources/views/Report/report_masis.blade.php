@extends('layout')
@section('title', 'Report Material')
@section('headerS')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link href="/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
@endsection
@section('style')
<style type="text/css">
	th, td {
		text-align: center;
		white-space:nowrap;
	}
	div>table {
		float: left
	}
</style>
@endsection
@section('content')
<div class="container-fluid" style="padding-top: 25px;">
	<div id="canvas-wrapper2" style="width: 100%; height: 100%; margin-left: auto; margin-right: auto;">
		<canvas id="bar"></canvas>
	</div>
	<a type="button" href="/report_pekerjaan_up/{{ Request::segment(3) }}" class="btn btn-default"><span data-icon="&#xe007;" class="linea-icon linea-basic fa-fw" style="font-size: 20px; vertical-align:middle;" ></span>&nbsp;Unduh Excel</a>
	<div style="padding-top: 25px;">
			<div class="panel panel-warning">
				<div class="panel-heading header-date">Report Pekerjaan Selesai Periode {{ Request::segment(3) }}</div>
				<div class="panel-body">
					<div class="table-responsive">
						<table id="teknisi"  class="table table-striped table-bordered">
							<thead>
								<tr>
									<th rowspan="3" class="align-middle">Datel</th>
									{{-- <th rowspan="3" class="align-middle">STO</th> --}}
									<th rowspan="3" class="align-middle">Mitra</th>
									<th rowspan="3" class="align-middle">Nama Tim</th>
									<th rowspan="3" class="align-middle">Total</th>
									<th style="text-align: center" colspan="{{ count($dayss) }}">{{ $bulan }}</th>
								</tr>
								<tr>
									@foreach($dayss as $tgl => $h)
										<th>{{ $tgl }}</th>
									@endforeach
								</tr>
								<tr>
									@foreach($dayss as $tgl => $h)
										<th>{{ $h }}</th>
									@endforeach
								</tr>
							</thead>
							@if($data)
								<tbody>
									@php
										$x = 0;
									@endphp
									@foreach($data as $d => $val)
										<tr>
											<td>{{ $val['datel'] }}</td>
											{{-- <td>{{ $val['sto'] }}</td> --}}
											<td>{{ $val['mitra'] }}</td>
											<td>{{ $val['regu'] }}</td>
											<td>
												<a target="_blank" class="btn btn-sm btn-info btn-circle direct_link" style="color:white;border-radius:25px;" href="{{ array_sum($val['hari']) != 0 ? '/detail_report_m/'. urlencode($val['regu']) .'/'. Request::segment(3) : '' }}">{{ array_sum($val['hari']) != 0 ? array_sum($val['hari']) : '-' }}</a>
											</td>
											@foreach($val['hari'] as $tgl => $no)
												<td style={!! Request::segment(3) == date('Y-m') && $tgl > date('d') ? 'background-color:grey;color:grey;' : '' !!}>
													@php
														$x += $no;
													@endphp
													@if($no != 0)
														<a target="_blank" class="btn btn-sm btn-info btn-circle direct_link" style="color:white;border-radius:25px;" href="{{ $no != 0 ? '/detail_report_m/'. urlencode($val['regu']) .'/'. Request::segment(3) .'-'. $tgl : '' }}">{{ $no }}</a>
													@else
														-
													@endif
												</td>
											@endforeach
										</tr>
									@endforeach
								</tbody>
								<tfoot>
									@foreach($dayss as $tgl => $h)
										@php
											$sum = 0;
										@endphp
										@foreach($data as $d => $val)
											@foreach($val['hari'] as $t => $no)
												@if($tgl == $t)
													@php
														$rekap[$tgl] = $sum+=$no;
													@endphp
												@endif
											@endforeach
										@endforeach
									@endforeach
									<td colspan="3">Total</td>
									<td>
										@if($x != 0)
											<a target="_blank" class="btn btn-sm btn-info btn-circle direct_link" style="color:white;border-radius:25px;" href="{{ $x != 0 ? '/detail_report_m/all/'. Request::segment(3) : '' }}">{{ $x }}</a>
										@else
											-
										@endif
									</td>
									@foreach($rekap as $tgl => $no)
										<td style={!! Request::segment(3) == date('Y-m') && $tgl > date('d') ? 'background-color:grey;color:grey;' : '' !!}>
											@if($no != 0)
												<a target="_blank" class="btn btn-sm btn-info btn-circle direct_link" style="color:white;border-radius:25px;" href="{{ $no != 0 ? '/detail_report_m/all/'. Request::segment(3) .'-'. $tgl : '' }}">{{ $no }}</a>
											@else
												-
											@endif
										</td>
									@endforeach
								</tfoot>
							@else
								@php
									$colspan = date('t', strtotime(Request::segment(3)));
								@endphp
								<tr>
									<td colspan="{{ $colspan + 6 }}">Tidak Ada Data!</td>
								</tr>
							@endif
						</table>
					</div>
				</div>
			</div>
	</div>
</div>
@endsection
@section('footerS')
<script src="/bower_components/moment/min/moment.min.js"></script>
<script src="/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@3.9.1/dist/chart.min.js"></script>
<script>
	var data_bar = {!! json_encode($data_go_live) !!},
	data_chart = [],
	cm = [],
	label = [],
	color_my = [];

	$.each(data_bar, function(k, v){
			label.push(k);
			color_my.push('#F7464A');
			data_chart.push(v)
	})

	function chart_bar(){
			var ctx =$('#bar');
			var data = {
				type: "bar",
				data: {
					labels: label,
					datasets: [{
						label: "SALDO GO-LIVE PT-2",
						fill: false,
						lineTension: 0.1,
						backgroundColor: color_my,
						fillColor: "blue",
						strokeColor: "green",
						borderWidth: 1,
						data: data_chart
					}]
				},
				option: {
					responsive: true,
					maintainAspectRatio: false,
				}
			};
			var myfirstChart = new Chart(ctx, data);
	}

	chart_bar()
</script>
@endsection