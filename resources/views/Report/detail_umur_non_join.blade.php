@extends('layout')
@section('title', 'List Pekerjaan Non Jointer')
@section('headerS')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.css">
<link href="/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.js"></script>
@endsection
@section('style')
<style type="text/css">
	th{
		text-align: center;
		white-space:nowrap;
	}
	div>table {
		float: left
	}
</style>
@endsection
@section('content')
<div class="container-fluid" style="padding-top: 25px;">
	<div class="container-fluid" style="padding-top: 25px;">
    <div class="panel panel-warning">
      <div class="panel-heading header-date">List Pekerjaan Jointer</div>
      <div class="panel-body">
        <div class="table-responsive">
          <table id="teknisi" class="table table-striped table-hover table-bordered">
            <thead>
              <tr>
                <th>Nama ODP</th>
                <th>Koordinat ODP</th>
                <th>Jenis Pekerjaan</th>
                <th>Tanggal Create</th>
                <th>Tanggal Terakhir</th>
                <th>Status</th>
                <th>Umur</th>
              </tr>
            </thead>
            <tbody>
              @foreach($data as $no => $v)
                <tr>
                  <td>{{ $v->odp_nama }}</td>
                  <td>{{ $v->odp_koor }}</td>
                  <td>{{ $v->status_pst }}</td>
                  <td>{{ $v->tgl_buat }}</td>
                  <td>{{ $v->modified_at }}</td>
                  <td>{{ $v->lt_status }}</td>
                  @php
                    $date2 = strtotime(date('Y-m-d H:i:s') );

                    $date_progg = strtotime($v->modified_at);
                    $seconds_prog = $date2 - $date_progg;

                    $daily_progg = floor(abs($seconds_prog / (60 * 60 * 24) ) );
                  @endphp
                  <td>{{ $daily_progg }} Hari</td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('footerS')
<script src="/bower_components/moment/min/moment.min.js"></script>
<script src="/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.js"></script>
<script>
$(function(){
  $('.table').DataTable({
    order: [
      [4, 'desc']
    ],
  });
})
</script>
@endsection