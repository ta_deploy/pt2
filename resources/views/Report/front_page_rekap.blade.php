@extends('layout')
@section('title', 'Rekap Tahunan')
@section('headerS')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link href="/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.js"></script>
@endsection
@section('style')
<style type="text/css">
    th, td{
        text-align: center;
    }
    div>table {
        float: left;
    }
    .down{
        -moz-transform:rotate(90deg);
        -webkit-transform:rotate(90deg);
        transform:rotate(90deg);
    }
    .rounded{
        border-radius: 12px 17px 17px 17px;
    }
</style>
@endsection
@section('content')
<div class="container-fluid" style="padding-top: 25px;">
    <div id="canvas-wrapper" style="margin-left: auto; margin-right: auto;">
        <canvas id="graph"></canvas>
    </div>
    <div style="padding-top: 25px;">
        <a type="button" href="/rekap/download/{{ Request::segment(2)}}&J=all&M=0" class="btn btn-default" id="button_photo"><span data-icon="&#xe007;" class="linea-icon linea-basic fa-fw" style="font-size: 20px; vertical-align:middle;" ></span>&nbsp;Unduh Excel Tahun ini</a>
        <div id="report_more_daily">
            @foreach($rekap_rinci as $bulan => $data)
            @if(!empty($data))
            <div class="panel rounded panel-info" style="margin-top: 20px;">
                <div class="table-responsive" style="padding-bottom: 3.8px;">
                    <p style="cursor: pointer;" class="label rounded label-success css_style no_{{$bulan}}" data-number="{{ $bulan }}" data-toggle="collapse" data-target="#collapse{{$bulan}}" role="button">{{$bulan}}<i class="fa fa-chevron-right rotate{{ $bulan }}"></i></p>
                    <div class="collapse" id="collapse{{$bulan}}">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <tr>
                                        <th colspan="10">{{ $bulan }}<a type="button" class="btn btn-light" style="outline:none;background-color: Transparent;border: none;" href="/{{ Request::segment(1) }}&J=all&M={{date('m',strtotime($bulan)) }}">(Lihat Detailnya)</a></th>
                                    </tr>
                                    <th>Witel</th>
                                    <th>STO</th>
                                    <th>Siap Dispatch</th>
                                    <th>Sudah Dispatch</th>
                                    <th>OGP</th>
                                    <th>Kendala</th>
                                    <th>Finish</th>
                                    <th>Sisa Order</th>
                                    <th>Grand Total</th>
                                </tr>
                            </thead>
                            <?php
                            $sum_WO_orderan_siap_dispatch = $sum_sudah_dispatch = $sum_ogp = $sum_kendala = $sum_finish = $sum_sisa_order_belumselesai = $sum_grand = 0;
                            ?>
                            <tbody>
                                @foreach($rekap_rinci as $Month => $data)
                                    @if($bulan == $Month)
                                        @foreach($data as $daerah => $data1)
                                            @php $first = true @endphp
                                        @foreach($data1->data_utama as $main_data)
                                            @php
                                                $sum_WO_orderan_siap_dispatch   += $main_data->WO_orderan_siap_dispatch;
                                                $sum_sudah_dispatch             += $main_data->sudah_dispatch;
                                                $sum_ogp                        += $main_data->ogp;
                                                $sum_kendala                    += $main_data->kendala;
                                                $sum_finish                     += $main_data->finish;
                                                $sum_sisa_order_belumselesai    += $main_data->sisa_order_belumselesai + $main_data->WO_orderan_siap_dispatch;
                                            @endphp
                                            <tr>
                                                @php
                                                    $sum_grand += $data1->Grands;
                                                @endphp
                                                @if($first == true)
                                                    <td rowspan="{{ $data1->count }}" class="align-middle">{{ $main_data->datel }}</td>
                                                @endif
                                                <td class="align-middle">{{ $main_data->sto_n }}</td>
                                                <td class="align-middle">{{ $main_data->WO_orderan_siap_dispatch == 0 ? '-' : $main_data->WO_orderan_siap_dispatch }}</td>
                                                <td class="align-middle">{{ $main_data->sudah_dispatch == 0 ? '-' : $main_data->sudah_dispatch }}</td>
                                                <td class="align-middle">{{ $main_data->ogp == 0 ? '-' : $main_data->ogp }}</td>
                                                <td class="align-middle">{{ $main_data->kendala == 0 ? '-' : $main_data->kendala }}</td>
                                                <td class="align-middle">{{ $main_data->finish == 0 ? '-' : $main_data->finish }}</td>
                                                <td class="align-middle">{{ $main_data->sisa_order_belumselesai + $main_data->WO_orderan_siap_dispatch }}</td>
                                                @if($first == true)
                                                <td rowspan="{{ $data1->count }}" class="align-middle">{{ $data1->Grands }}</td>
                                                @php
                                                    $first = false
                                                @endphp
                                                @endif
                                            </tr>
                                        @endforeach
                                        @endforeach
                                    @endif
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="2"></td>
                                    <td class="align-middle">{{ $sum_WO_orderan_siap_dispatch }}</td>
                                    <td class="align-middle">{{ $sum_sudah_dispatch }}</td>
                                    <td class="align-middle">{{ $sum_ogp }}</td>
                                    <td class="align-middle">{{ $sum_kendala }}</td>
                                    <td class="align-middle">{{ $sum_finish }}</td>
                                    <td class="align-middle">{{ $sum_sisa_order_belumselesai }}</td>
                                    <td class="align-middle" colspan="2">{{ $sum_grand }}</td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
            @else
            Tidak ada data
            @endif
            @endforeach
        </div>
    </div>
</div>
@endsection
@section('footerS')
<script src="/bower_components/moment/min/moment.min.js"></script>
<script src="/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script>
    function startTimer(duration, display) {
        var timer = duration, minutes, seconds;
        var settan = setInterval(function () {
            minutes = parseInt(timer / 60, 10)
            seconds = parseInt(timer % 60, 10);

            minutes = minutes < 10 ? "0" + minutes : minutes;
            seconds = seconds < 10 ? "0" + seconds : seconds;

            display.text('(' + minutes + ":" + seconds + ')');

            if (--timer < 0) {
                timer = duration;
            }

            if (timer == 0) {
                clearInterval(settan);
                display.text('');
                $('#button_photo').attr('disabled', false);
                $( "#button_photo" ).css({
                    border: ""
                });
            }
            if (timer != 0) {
                $('#button_photo').attr('disabled', true);
            }
        }, 1000);
    }

    $(function(){
        var link_split=window.location.href.split('=');
        var barGraph;
        function my_chart(year){
            var ctx =$('#graph');
            $.ajax({
                url:"/rekap/Y="+year+"/matrix/search",
                type:"GET",
                success: function(data) {
                    var dynamicColors = function() {
                        var r = Math.floor(Math.random() * 255);
                        var g = Math.floor(Math.random() * 255);
                        var b = Math.floor(Math.random() * 255);
                        return "rgb(" + r + "," + g + "," + b + ", 0.2)";
                    };
                    var label = [],
                    count = [],
                    color_my = [];
                    $.each( data, function( key, val ) {
                        label.push(key);
                        var raw_count = 0;
                        $.each( val, function( key_val, data_val ) {
                            if(key == data_val.alamat){
                               raw_count += data_val.finishs;
                           }
                       });
                        count.push(raw_count);
                        color_my.push(dynamicColors());
                    });
                    var chartdata = {
                        labels: label.reverse(),
                        datasets : [{
                            label: 'Rekap ODP Naik Tahun '+year,
                            fill: false,
                            lineTension: 0.1,
                            data: count.reverse(),
                            backgroundColor: color_my,
                            borderColor: color_my,
                            borderWidth: 1
                        }]
                    };
                    barGraph = Chart.Bar(ctx, {
                        data: chartdata,
                        options: {
                            legend: {
                               /*display: false*/
                               labels: {
                                    fontColor: "black",
                                    fontSize: 14
                                },
                                onHover: function(e) {
                                    e.target.style.cursor = 'pointer';
                                }
                            },
                            tooltips: {
                                mode: 'index',
                                intersect: false,
                                callbacks: {
                                    label: function(tooltipItem) {
                                        return tooltipItem.yLabel;
                                    }
                                }
                            },
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        fontColor: "black",
                                        beginAtZero: true
                                    }
                                }],
                                xAxes: [{
                                    ticks: {
                                        fontColor: "black",
                                        beginAtZero: true
                                    }
                                }]
                            },
                            hover: {
                                onHover: function(e) {
                                    var point = this.getElementAtEvent(e);
                                    if (point.length) e.target.style.cursor = 'pointer';
                                    else e.target.style.cursor = 'default';
                                }
                            }
                        }
                    });

                        var canvas = document.getElementById('graph');
                        canvas.onclick = function(evt) {
                            var activePoint = barGraph.getElementAtEvent(evt)[0];
                            if(activePoint){
                            var data = activePoint._chart.config.data;
                            var datasetIndex = activePoint._datasetIndex;
                            var label = data.labels[activePoint._index];
                            var value = data.datasets[datasetIndex].data[activePoint._index];
                            var month_int = activePoint._index + 1;
                            console.log(label)
                            window.open('/rekap/Y='+link_split[1]+'&J=all&M='+month_int,'_blank');
                        }
                    };
                },
                error: function(data) {
                    /*console.log(data);*/
                }
            });
        }
        my_chart(link_split[1]);

        var my_data = <?= json_encode($rekap_rinci) ?>,
        lala = [];
        if(my_data != null){
            $.each(my_data, function( index, value ) {
                lala.push(index);

            });
            $(".no_"+lala[0]).attr('aria-expanded', true);
            $("#collapse"+lala[0]).attr('aria-expanded', true);
            $("#collapse"+lala[0]).toggleClass('show');
            $(".rotate"+lala[0]).toggleClass('down');
        }

        $(".show_detail_photo").click(function(){
            var value = $(this).attr('data-id');
            var deploy = $(this).attr('data-deploy');
            var res = value.toUpperCase();
            $.ajax({
                type: 'GET',
                url : "/matrix/photo/"+deploy,
                success: function(data) {
                    /*console.log(data);*/
                    $('#photo_rekap_detail').modal('show');
                    $('#odp_nama').text('Detail Foto ODP '+res);
                    $('#Konten').html(data);
                },
                error: function(e){
                    /*console.log(e);*/
                }
            });
        });

        $('#button_photo').on('click', function(){
            var fiveMinutes = 60 * 1,
            display = $('#send_photo');
            startTimer(fiveMinutes, display);
            $(this).attr('disabled', true);
            $(this).css({
                border: "2px solid #C83C36FF"
            });
        });

        $(".css_style").click(function () {
            var data = $(this).attr('data-number');
            $(".rotate"+data).toggleClass("down");
            $("#").collapsing
            $(".rotate"+data).css({
                "-moz-transition":"all .5s linear",
                "-webkit-transition":"all .5s linear",
                'transition': 'all .5s linear'
            });
        });

    });
</script>
@endsection