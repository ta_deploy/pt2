@extends('layout')
@section('title', 'List Pekerjaan Jointer')
@section('headerS')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.css">
<link href="/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.js"></script>
@endsection
@section('style')
<style type="text/css">
	th{
		text-align: center;
		white-space:nowrap;
	}
	div>table {
		float: left
	}
</style>
@endsection
@section('content')
<div class="container-fluid" style="padding-top: 25px;">
	<div class="container-fluid" style="padding-top: 25px;">
    <div class="panel panel-warning">
      <div class="panel-heading header-date">List Pekerjaan Jointer</div>
      <div class="panel-body">
        <div class="table-responsive">
          <table id="teknisi" class="table table-striped table-hover table-bordered">
            <thead>
              <tr>
                <th>Nama ODP</th>
                <th>Koordinat ODP</th>
                <th>Jenis Pekerjaan</th>
                <th>Tanggal Create</th>
                <th>Tanggal Selesai</th>
                <th>Keterangan</th>
              </tr>
            </thead>
            <tbody>
              @foreach($data as $no => $r)
                <tr>
                  <td>{{ $r->odp_nama }}</td>
                  <td>{{ $r->odp_koor }}</td>
                  <td>{{ $r->status_pst }}</td>
                  <td>{{ $r->tgl_buat }}</td>
                  <td>{{ $r->tgl_selesai }}</td>
                  <td><span class="label label-{{ $r->id ? 'warning' : 'info' }}" for="input-status">{{ $r->id ? 'Masuk Tomman Jointer' : 'New' }}</span></td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('footerS')
<script src="/bower_components/moment/min/moment.min.js"></script>
<script src="/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.js"></script>
<script>
$(function(){
  $('.table').DataTable({
    order: [
      [4, 'desc']
    ],
  });
})
</script>
@endsection