@extends('layout')
@section('title', 'Matrix By Pekerjaan')
@section('headerS')
<link href="/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
@endsection
@section('style')
<style type="text/css">
	th, td{
		text-align: center;
		white-space:nowrap;
	}
	div>table {
		float: left
	}
</style>
@endsection
@section('content')
<div class="container-fluid" style="padding-top: 25px;">
	<div class="panel panel-info">
		<div class="panel-heading header-date">Matrix Jointer</div>
		<div class="panel-body">
			<div class='input-group date'>
				<input type='text' class="form-control" name='rangedate' value="{{date("m/d/Y", strtotime("first day of this month"))}} - {{date("m/d/Y", strtotime("last day of this month"))}}"  disabled>
				<span class="input-group-addon">
					<span class="glyphicon glyphicon-calendar kalender" style="cursor: pointer;"></span>
				</span>
			</div>
			<div class="table-responsive">
				<table id="teknisi" class="table table-striped">
					<thead>
						<tr>
							<th>Jenis Order</th>
							<th>SALDO</th>
							<th>IN-TECH</th>
							<th>SELESAI</th>
							<th>KENDALA</th>
						</tr>
					</thead>
					<tbody>
						@foreach($data as $k => $v)
							<tr>
								<td>{{ $k }}</td>
								<td>
									@if($v['saldo'] == 0)
										-
									@else
										<a href="/jointer_report/detail_dataM/all/all/all/{{ Request::Segment(3) }}/{{ $k }}">{{ $v['saldo'] }}</a>
									@endif
								</td>
								<td>
									@if($v['in_tech'] == 0)
										-
									@else
										<a href="/jointer_report/detail_dataM/all/all/all/{{ Request::Segment(3) }}/{{ $k }}">{{ $v['in_tech'] }}</a>
									@endif
								</td>
								<td>
									@if($v['selesai'] == 0)
										-
									@else
										<a href="/jointer_report/detail_dataM/all/all/all/{{ Request::Segment(3) }}/{{ $k }}">{{ $v['selesai'] }}</a>
									@endif
								</td>
								<td>
									@if($v['kendala'] == 0)
										-
									@else
										<a href="/jointer_report/detail_dataM/all/all/all/{{ Request::Segment(3) }}/{{ $k }}">{{ $v['kendala'] }}</a>
									@endif
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection