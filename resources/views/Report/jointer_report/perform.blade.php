@extends('layout')
@section('title', 'Performa Jointer')
@section('headerS')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link href="/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.js"></script>
@endsection
@section('style')
<style type="text/css">
	th, td{
		text-align: center;
		white-space:nowrap;
	}
	div>table {
		float: left
	}
</style>
@endsection
@section('content')
<div class="container-fluid" style="padding-top: 25px;">
	<div id="canvas-wrapper">
		<canvas id="graph"></canvas>
	</div>
	<div class="panel panel-warning">
		<div class="panel-heading header-date">Performa Jointer Periode {{ Request::segment(3) }}</div>
		<div class="panel-body">
			<div class='input-group date'>
				<input type='text' class="form-control" name='rangedate' value="{{date("m/d/Y", strtotime("first day of this month"))}} - {{date("m/d/Y", strtotime("last day of this month"))}}"  disabled>
				<span class="input-group-addon">
					<span class="glyphicon glyphicon-calendar kalender" style="cursor: pointer;"></span>
				</span>
			</div>
			<div class="table-responsive">
				<table id="teknisi" class="table table-striped">
					<thead>
						<tr>
							<th>#</th>
							<th>Nama</th>
							<th>Mitra</th>
							<th>Total Diambil Bulan Ini</th>
							<th>Belum Dikerjakan</th>
							<th>Selesai</th>
							<th>Total Core</th>
						</tr>
					</thead>
					<tbody>
						@foreach($data as $k => $v)
							<tr>
								<td>{{ ++$k }}</td>
								<td style="color: {{ $v['total_qty'] == 0 ? 'red' : '' }}">{{ $v['uraian'] }}</td>
								<td>{{ $v['mitra_amija'] }}</td>
								<td style="color: {!! (50 - ($v['isi']['pickup'] + $v['isi']['selesai']) ) == 0 ? 'red' : '' !!}"><b>{{ $v['isi']['pickup'] + $v['isi']['selesai'] }}</b> Dari <b>{{ 50 - ($v['isi']['pickup'] + $v['isi']['selesai']) }}</b> Order</td>
								<td>
									@if($v['isi']['pickup'] == 0)
										-
									@else
										<a href="/tes">{{ $v['isi']['pickup'] }}</a>
									@endif
								</td>
								<td>
									@if($v['isi']['selesai'] == 0)
										-
									@else
										<a href="/tes">{{ $v['isi']['selesai'] }}</a>
									@endif
								</td>
								<td>
									@if($v['isi']['core_splice'] == 0)
										-
									@else
										<a href="/tes">{{ $v['isi']['core_splice'] }}</a>
									@endif
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection
@section('footerS')
<script src="/bower_components/moment/min/moment.min.js"></script>
<script src="/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script>
	$(function(){

		$('input[name="rangedate"]').daterangepicker({
			opens: 'left'
		}, function(start, end){
			var month1 = start.format('YYYY-MM-DD'),
			month2 = end.format('YYYY-MM-DD')+" 23:59:59";
		});

		$('.kalender').click(function(e){
			e.preventDefault();
			$('input[name="rangedate"]').click();
		});


	});

</script>
@endsection