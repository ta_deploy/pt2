@extends('layout')
@section('title', 'Umur Jointer')
@section('headerS')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link href="/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
@endsection
@section('style')
<style type="text/css">
	th, td{
		text-align: center;
		white-space:nowrap;
	}
	div>table {
		float: left
	}
</style>
@endsection
@section('content')
<div class="container-fluid" style="padding-top: 25px;">
  <div class='input-group date' style="margin-bottom: 25px;">
    <input type='text' class="form-control" name='rangedate' value="{{date("m/d/Y", strtotime("first day of this month"))}} - {{date("m/d/Y", strtotime("last day of this month"))}}"  disabled>
    <span class="input-group-addon">
      <span class="glyphicon glyphicon-calendar kalender" style="cursor: pointer;"></span>
    </span>
  </div>
  @foreach($data as $k => $v)
    <div class="panel panel-info">
      @switch($k)
        @case('pickup')
          <div class="panel-heading header-date">Pekerjaan Yang Belum Diambil</div>
        @break
        @default
          <div class="panel-heading header-date">Pekerjaan Yang Belum Dikerjakan</div>
        @break
      @endswitch
      <div class="panel-body">
        <div class="table-responsive">
          <table id="teknisi" class="table table-striped">
            <thead>
              <tr>
                <th>#</th>
                <th>Nama</th>
                <th>Mitra</th>
                <th>Total Pekerjaan</th>
                <th>Kurang 1 Hari</th>
                <th>1 Sampai 3 Hari</th>
                <th>4 Sampai 7 Hari</th>
                <th>Lebih Dari 7 Hari</th>
              </tr>
            </thead>
            <tbody>
              @foreach($v as $kk => $vv)
                <tr>
                  <td>{{ ++$kk }}</td>
                  <td>{{ $vv['uraian'] }}</td>
                  <td>{{ $vv['mitra_amija'] }}</td>
                  <td>
                    @if($vv['isi']['total'] == 0)
                      -
                    @else
                      {{ $vv['isi']['total'] }}
                    @endif
                  </td>
                  <td>
                    @if($vv['isi']['_1'] == 0)
                      -
                    @else
                      {{ $vv['isi']['_1'] }}
                    @endif
                  </td>
                  <td>
                    @if($vv['isi']['1_3'] == 0)
                      -
                    @else
                      {{ $vv['isi']['1_3'] }}
                    @endif
                  </td>
                  <td>
                    @if($vv['isi']['4_7'] == 0)
                      -
                    @else
                      {{ $vv['isi']['4_7'] }}
                    @endif
                  </td>
                  <td>
                    @if($vv['isi']['7_'] == 0)
                      -
                    @else
                      {{ $vv['isi']['7_'] }}
                    @endif
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  @endforeach
</div>
@endsection
@section('footerS')
<script src="/bower_components/moment/min/moment.min.js"></script>
<script src="/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script>
	$(function(){

		$('input[name="rangedate"]').daterangepicker({
			opens: 'left'
		}, function(start, end){
			var month1 = start.format('YYYY-MM-DD'),
			month2 = end.format('YYYY-MM-DD')+" 23:59:59";
		});

		$('.kalender').click(function(e){
			e.preventDefault();
			$('input[name="rangedate"]').click();
		});


	});

</script>
@endsection