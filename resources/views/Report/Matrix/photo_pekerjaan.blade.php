<div class="row">
	<div style="float: left; clear: both; margin-botton: 4px;" id="download_photo_">
		<a type="button" class="btn btn-info download_zip_photo" style="text-align: center; color: #ffffff;"><span data-icon="&#xe035;" class="linea-icon linea-aerrow fa-fw" style="font-size: 20px; color: #ffffff; "></span>&nbsp;Unduh Foto Teknisi</a>
	</div>
	<div id="download_file_rfc" style="margin-botton: 4px;">
		<a type="button" class="btn btn-warning download_file_pdf" download style="text-align: center; margin-left: 6px; color: #ffffff;"><span data-icon="&#xe00b;" class="linea-icon linea-basic fa-fw" style="font-size: 20px; color: #ffffff; "></span>&nbsp;Unduh PDF RFC</a>
	</div>
	<?php
	$number = 1;
	clearstatcache();
	$check_path = "/upload/pt_2/teknisi/$id->id";
	$check_path_2 = "/upload/pt_2_2/teknisi/$id->id";
	$check_path_3 = "/upload/pt_2_3/teknisi/$id->id";
	?>
	@if (file_exists(public_path().$check_path) || file_exists(public_path().$check_path_2) || file_exists(public_path().$check_path_3))
	<div class="col-md-12">
		<div class="panel panel-info">
			<div class="panel-heading">Laporan Teknisi</div>
			<div class="panel-body">
				<div class="row">
					<?php
					$number = 1;
					clearstatcache();
					?>
					@foreach($photo as $input)
					<?php
					if (file_exists(public_path().$check_path)){
						$path = "$check_path/$input";
					}elseif (file_exists(public_path().$check_path_2)){
						$path = "$check_path_2/$input";
					}else{
						$path = "$check_path_3/$input";
					}
					$th   = "$path-th.jpg";
					$img  = "$path.jpg";
					$nt = "$path-catatan.txt";
					$nt_panel = "$path-panel.txt";
					$flag = "";
					$name = "flag_".$input;
					?>
					@if (file_exists(public_path().$th))
					<div class="col-6 col-sm-3">
						<a href="{{ $img }}">
							<img style="margin: 0 auto; width: 100px; height: 150px; margin-left: 23px;" src="{{ $th }}" alt="{{ $input }}" />
						</a>
						<?php
						$flag = 1;
						?>
						<br />
						<p>{{ str_replace('_',' ',$input) }}</p>
						@if($input != 'Port_Feeder' && $input != 'ODC')

						@if (file_exists(public_path().$nt))
						<?php $note = File::get(public_path("$path-catatan.txt")); ?>
						@else
						<?php $note ='' ?>
						@endif
						<textarea readonly class="form-control foto" id="catatan_{{$input}}" name="catatan_{{$input}}">{{$note}}</textarea>
						@else

						@if (file_exists(public_path().$nt_panel))
						<?php $note_panel = File::get(public_path("$path-panel.txt"));
						$note_port = File::get(public_path("$path-port.txt")); ?>
						@else
						<?php $note_panel = '';
						$note_port = '';?>
						@endif
						<div class="form-group row" style="margin-bottom: 23px;">
							<div class="col-5 col-md-5">
								<label class="control-label pull-left" for="input-ont">Panel</label>
							</div>
							<div class="col-7 col-md-7">
								<input readonly type="text" class="form-control" id="panel_{{$input}}" style="height: 25px; border: 3px solid white;" name="panel_{{$input}}" value="{{$note_panel}}">
							</div>
						</div>
						<div class="form-group row " style="margin-bottom: 0px; margin-top: -3.56px;">
							<div class="col-5 col-md-5">
								<label class="control-label pull-left" for="input-ont">Port</label>
							</div>
							<div class="col-7 col-md-7">
								<input type="text" readonly class="form-control" id="port_{{$input}}" style="height: 25px; border: 3px solid white;" name="port_{{$input}}" value="{{$note_port}}">
							</div>
						</div>
						@endif
						{!! $errors->first($name, '<span class="label label-danger">:message</span>') !!}
						<br />
					</div>
					@endif
					<?php
					$number++;
					?>
					@endforeach
				</div>
			</div>
		</div>
	</div>
	@endif
</div>
<script type="text/javascript">
	function start_init(){
		var detch_download_phto = $('.download_zip_photo').detach(),
		detch_download_rfc = $('.download_file_pdf').detach(),
		id_data = {!! json_encode($id) !!};
		$.ajax({
			type: 'GET',
			data: {data : id_data.id, angka : 0},
			url : "/admin/get_odp/LY",
			beforeSend: function() {
				console.log('dang lah');
				$("#parent").toggleClass('preloader');
				$("#child").toggleClass('cssload-speeding-wheel');
				$("#parent").css({
					'background-color' : 'rgba(0,0,0,.8)'
				});
			},
			success: function(data) {
				console.log(data);
				$("#parent").toggleClass('preloader');
				$("#child").toggleClass('cssload-speeding-wheel');
				$("#parent").css({
					'background-color' : ''
				});
				if(data.lt_status == 'Selesai'){
					$('.download_file_pdf').attr("href", '/Download/file/pdf/'+data.id);
					$.ajax({
						url:"/Download/file/pdf/"+data.id,
						type:'HEAD',
						error: function()
						{
						},
						success: function()
						{
							detch_download_rfc.appendTo( "#download_file_rfc" );
						}
					});
				}

				$.ajax({
					url:"/Download/file/"+data.id,
					type:'HEAD',
					error: function()
					{
					},
					success: function()
					{
						detch_download_phto.appendTo( "#download_photo_" );
						$('.download_zip_photo').attr("href", "/Download/file/"+data.id);
					}
				});
			}
		});
	}

	start_init()
</script>