@extends('layout')
@section('title', 'Matrix By Detail')
@section('headerS')
<link href="/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css" integrity="sha512-nMNlpuaDPrqlEls3IX/Q56H36qvBASwb3ipuo3MxeWbsQB1881ox0cRv7UPTgBlriqoynt35KjEwgGUeUXIPnw==" crossorigin="anonymous" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.js"></script>
@endsection
@section('style')
<style type="text/css">
    th{
        white-space: nowrap;
    }
    div>table {
        float: left;
    }

    .label-green{
        background-color: #66A955FF;
    }

    .label-dark_orange{
        background-color: #FF8C00;
    }

    .label-warna_jalan{
        background-color: #004A7FFF;
    }

    .label-tidak_sempat{
        background-color: #FF00D3FF;
    }

    .label-primary{
        background-color: #ab8ce4;
    }

    .label-default{
        background-color: #98a6ad;
    }

    .badge{
        font-size: 11px;
        color: black;
    }

    #teknisi {
        font-size: 0.85em;
    }

    .panel .panel-action a {
	    opacity: 100;
    }

    .ti-minus {
        font-size: 20px;
        color: white;
        font-weight: bold;
    }

    .ti-plus {
        font-size: 20px;
        color: white;
        font-weight: bold;
    }
</style>
@endsection
@section('content')
<div class="container-fluid" style="padding-top: 25px;">
    <div class='row' style="margin-bottom: 15px;">
        <div class="col-md-6">
            <div id="canvas-wrapper" style="width: 60%; height: 60%; margin-left: auto; margin-right: auto;">
                <canvas id="graph"></canvas>
            </div>
        </div>
        <div class="col-md-6">
            <div id="canvas-wrapper" style="width: 100%; height: 100%; margin-left: auto; margin-right: auto;">
                <canvas id="bar"></canvas>
            </div>
        </div>
    </div>
    <div class="panel panel-warning">
        <div class="panel-heading header-date">Matrix Periode {{ Request::segment(2) }}</div>
        <div class="panel-body">
            <form id="formlistG" name="formlistG" method="get">
                <div class="row">
                    <div class="form-group col-md-3">
                        <select id="stts_regu" name="stts_regu[]" class="form-control find_stts_regu" multiple="">
                            <option value="'1'">Regu Aktif</option>
                            <option value="'0'">Regu Tidak Aktif</option>
                        </select>
                    </div>
                    <div class="form-group col-md-3">
                        <select id="status" name="status[]" class="form-control find_status" multiple="">
                            <option value="'need_progres'">Need Progress</option>
                            <option selected value="'Ogp'">OGP</option>
                            <option value="'Selesai'">Selesai</option>
                            <option value="'Kendala'">Kendala</option>
                            <option selected value="'Pending'">Pending</option>
                            <option value="'Berangkat'">Berangkat</option>
                            <option value="'>7'">Lebih 7 Hari (Saldo)</option>
                            <option value="'Tidak Sempat'">Tidak Sempat</option>
                        </select>
                    </div>
                    <div class="form-group col-md-3">
                        <select id="jenis" name="jenis[]" class="form-control find_jenis" multiple="">
                            @foreach($get_all_status as $k => $v)
                                <option value="{{ $k }}">{{ $v }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-3">
                        <div class='input-group date'>
                            <input type='text' class="form-control" name='rangedate' value="{{date("m/d/Y")}} - {{date("m/d/Y")}}">
                            <input type="hidden" name="tgl_a">
                            <input type="hidden" name="tgl_f">
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar kalender" style="cursor: pointer;"></span>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-12">
                        <div class='input-group'>
                            <button type="submit" class="btn btn-block btn-primary">Cari</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="panel panel-warning">
		<div class="panel-heading header-date">
            Performa Jointer
            <div class="panel-action">
                <a href="#" data-perform="panel-collapse"><i style="font-size: 20px;" class="ti-minus"></i></a>
            </div>
        </div>
		<div class="panel-body">
			<div class="table-responsive">
				<table id="teknisi" class="table table-striped">
					<thead>
						<tr>
							<th>#</th>
							<th>Nama</th>
							<th>Mitra</th>
							<th>Total Diambil Bulan Ini</th>
							<th>Belum Dikerjakan</th>
							<th>Selesai</th>
							<th>Total Core</th>
						</tr>
					</thead>
					<tbody>
						@foreach($data_perform as $k => $v)
							<tr>
								<td>{{ ++$k }}</td>
								<td style="color: {{ $v['total_qty'] == 0 ? 'red' : '' }}">{{ $v['uraian'] }}</td>
								<td>{{ $v['mitra_amija'] }}</td>
								<td style="color: {!! (50 - ($v['isi']['pickup'] + $v['isi']['selesai']) ) == 0 ? 'red' : '' !!}"><b>{{ $v['isi']['pickup'] + $v['isi']['selesai'] }}</b> Dari <b>{{ 50 - ($v['isi']['pickup'] + $v['isi']['selesai']) }}</b> Order</td>
								<td>
									@if($v['isi']['pickup'] == 0)
										-
									@else
										<a href="#" style="color: black;">{{ $v['isi']['pickup'] }}</a>
									@endif
								</td>
								<td>
									@if($v['isi']['selesai'] == 0)
										-
									@else
										<a href="#" style="color: black;">{{ $v['isi']['selesai'] }}</a>
									@endif
								</td>
								<td>
									@if($v['isi']['core_splice'] == 0)
										-
									@else
										<a href="#" style="color: black;">{{ $v['isi']['core_splice'] }}</a>
									@endif
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>

    <div class="panel panel-primary">
        <div class="panel-heading header-date">
            Tampilan Per DATEL
            <div class="panel-action">
                <a href="#" data-perform="panel-collapse"><i style="font-size: 20px;" class="ti-minus"></i></a>
            </div>
        </div>
        <div class="panel-body">
            <div class="table-responsive">
                <table id="teknisi" class="table table-striped">
                    <thead>
                        <tr>
                            <th style="text-align: center;">Datel</th>
                            <th style="text-align: center;">Total Wo</th>
                            <th style="text-align: center;">Selesai</th>
                            <th style="text-align: center;">Kendala</th>
                            <th style="text-align: center;">Pending</th>
                            <th style="text-align: center;">Ogp</th>
                            <th style="text-align: center;">Berangkat</th>
                            <th style="text-align: center;">Sisa</th>
                            <th style="text-align: center;">Tidak Sempat</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $sum_totalwo = $sum_selesai = $sum_kendala = $sum_pending = $sum_ogp = $sum_sisa = $sum_jalan = $TS= 0;
                            $nomor = 1;
                        @endphp
                        @foreach($matrix['table'] as $k => $m)
                            @php
                                $sum_totalwo += $m['totalwo'];
                                $sum_selesai += $m['selesai'];
                                $sum_kendala += $m['kendala'];
                                $sum_pending += $m['pending'];
                                $sum_ogp     += $m['ogp'];
                                $sum_jalan   += $m['jalan'];
                                $sum_sisa    += $m['sisa'];
                                $TS          += $m['TS'];
                            @endphp
                            <tr>
                                <td style='text-align: center;'>
                                    <a class="btn btn-sm btn-info direct_link" style="color: white; border-radius: 25px;" href="/matrix/detail_data/{{ $m['datel'] }}/all/all">{{ $m['datel'] }}</a>
                                </td>
                                <td style='text-align: center;'>
                                    @if ($m['totalwo'] != 0)
                                        <a class="btn btn-sm btn-info btn-circle direct_link" style="color: white; border-radius: 25px;" href="/matrix/detail_data/{{ $m['datel'] }}/all/totalwo">{{ $m['totalwo'] }}</a>
                                    @else
                                        -
                                    @endif
                                </td>
                                <td style='text-align: center;'>
                                    @if ($m['selesai'] != 0)
                                        <a class="btn btn-sm btn-info btn-circle direct_link" style="color: white; border-radius: 25px;" href="/matrix/detail_data/{{ $m['datel'] }}/all/selesai">{{ $m['selesai'] }}</a>
                                    @else
                                        -
                                    @endif
                                </td>
                                <td style='text-align: center;'>
                                    @if ($m['kendala'] != 0)
                                        <a class="btn btn-sm btn-info btn-circle direct_link" style="color: white; border-radius: 25px;" href="/matrix/detail_data/{{ $m['datel'] }}/all/kendala">{{ $m['kendala'] }}</a>
                                    @else
                                        -
                                    @endif
                                </td>
                                <td style='text-align: center;'>
                                    @if ($m['pending'] != 0)
                                        <a class="btn btn-sm btn-info btn-circle direct_link" style="color: white; border-radius: 25px;" href="/matrix/detail_data/{{ $m['datel'] }}/all/pending">{{ $m['pending'] }}</a>
                                    @else
                                        -
                                    @endif
                                </td>
                                <td style='text-align: center;'>
                                    @if ($m['ogp'] != 0)
                                        <a class="btn btn-sm btn-info btn-circle direct_link" style="color: white; border-radius: 25px;" href="/matrix/detail_data/{{ $m['datel'] }}/all/ogp">{{ $m['ogp'] }}</a>
                                    @else
                                        -
                                    @endif
                                </td>
                                <td style='text-align: center;'>
                                    @if ($m['jalan'] != 0)
                                        <a class="btn btn-sm btn-info btn-circle direct_link" style="color: white; border-radius: 25px;" href="/matrix/detail_data/{{ $m['datel'] }}/all/jalan">{{ $m['jalan'] }}</a>
                                    @else
                                        -
                                    @endif
                                </td>
                                <td style='text-align: center;'>
                                    @if ($m['sisa'] != 0)
                                        <a class="btn btn-sm btn-info btn-circle direct_link" style="color: white; border-radius: 25px;" href="/matrix/detail_data/{{ $m['datel'] }}/all/sisa">{{ $m['sisa'] }}</a>
                                    @else
                                        -
                                    @endif
                                </td>
                                <td style='text-align: center;'>
                                    @if ($m['TS'] != 0)
                                        <a class="btn btn-sm btn-info btn-circle direct_link" style="color: white; border-radius: 25px;" href="/matrix/detail_data/{{ $m['datel'] }}/all/TS">{{ $m['TS'] }}</a>
                                    @else
                                        -
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <td style='text-align: center;'>Total</td>
                            <td style='text-align: center;' id="total" data-total="{{ $sum_totalwo }}">
                                @if($sum_totalwo != 0)
                                    <a class="btn btn-sm btn-info btn-circle direct_link" style="color: white; border-radius: 25px;" href="/matrix/detail_data/all/all/totalwo">{{ $sum_totalwo }}</a>
                                @else
                                    -
                                @endif
                            </td>
                            <td style='text-align: center;' id="selesai" data-selesai="{{ $sum_selesai }}">
                                @if($sum_selesai != 0)
                                    <a class="btn btn-sm btn-info btn-circle direct_link" style="color: white; border-radius: 25px;" href="/matrix/detail_data/all/all/selesai">{{ $sum_selesai }}</a>
                                @else
                                    -
                                @endif
                            </td>
                            <td style='text-align: center;' id="kendala" data-kendala="{{ $sum_kendala }}">
                                @if($sum_kendala != 0)
                                    <a class="btn btn-sm btn-info btn-circle direct_link" style="color: white; border-radius: 25px;" href="/matrix/detail_data/all/all/kendala">{{ $sum_kendala }}</a>
                                @else
                                    -
                                @endif
                            </td>
                            <td style='text-align: center;' id="pending" data-pending="{{ $sum_pending }}">
                                @if($sum_pending != 0)
                                    <a class="btn btn-sm btn-info btn-circle direct_link" style="color: white; border-radius: 25px;" href="/matrix/detail_data/all/all/pending">{{ $sum_pending }}</a>
                                @else
                                    -
                                @endif
                            </td>
                            <td style='text-align: center;' id="ogp" data-ogp="{{ $sum_ogp }}">
                                @if($sum_ogp != 0)
                                    <a class="btn btn-sm btn-info btn-circle direct_link" style="color: white; border-radius: 25px;" href="/matrix/detail_data/all/all/ogp">{{ $sum_ogp }}</a>
                                @else
                                    -
                                @endif
                            </td>
                            <td style='text-align: center;' id="jalan" data-jalan="{{ $sum_jalan }}">
                                @if($sum_jalan != 0)
                                    <a class="btn btn-sm btn-info btn-circle direct_link" style="color: white; border-radius: 25px;" href="/matrix/detail_data/all/all/jalan">{{ $sum_jalan }}</a>
                                @else
                                    -
                                @endif
                            </td>
                            <td style='text-align: center;' id="sisa" data-sisa="{{ $sum_sisa }}">
                                @if($sum_sisa != 0)
                                    <a class="btn btn-sm btn-info btn-circle direct_link" style="color: white; border-radius: 25px;" href="/matrix/detail_data/all/all/sisa">{{ $sum_sisa }}</a>
                                @else
                                    -
                                @endif
                            </td>
                            <td style='text-align: center;' id="ts" data-ts="{{ $TS }}">
                                @if($TS != 0)
                                    <a class="btn btn-sm btn-info btn-circle direct_link" style="color: white; border-radius: 25px;" href="/matrix/detail_data/all/all/ts">{{ $TS }}</a>
                                @else
                                    -
                                @endif
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

    <div class="panel panel-primary">
        <div class="panel-heading header-date">
            Tampilan Per Mitra
            <div class="panel-action">
                <a href="#" data-perform="panel-collapse"><i style="font-size: 20px;" class="ti-minus"></i></a>
            </div>
        </div>
        <div class="panel-body">
            <div class="table-responsive">
                <table id="teknisi" class="table table-striped">
                    <thead>
                        <tr>
                            <th style="text-align: center;">Mitra</th>
                            <th style="text-align: center;">Total Wo</th>
                            <th style="text-align: center;">Selesai</th>
                            <th style="text-align: center;">Kendala</th>
                            <th style="text-align: center;">Pending</th>
                            <th style="text-align: center;">Ogp</th>
                            <th style="text-align: center;">Berangkat</th>
                            <th style="text-align: center;">Sisa</th>
                            <th style="text-align: center;">Tidak Sempat</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $sum_totalwo = $sum_selesai = $sum_kendala = $sum_pending = $sum_ogp = $sum_sisa = $sum_jalan = $TS= 0;
                            $nomor = 1;
                        @endphp
                        @foreach($matrix['matrix_per_mitra'] as $k => $m)
                            @php
                                $sum_totalwo += $m['totalwo'];
                                $sum_selesai += $m['selesai'];
                                $sum_kendala += $m['kendala'];
                                $sum_pending += $m['pending'];
                                $sum_ogp     += $m['ogp'];
                                $sum_jalan   += $m['jalan'];
                                $sum_sisa    += $m['sisa'];
                                $TS          += $m['TS'];
                            @endphp
                            <tr>
                                <td style='text-align: center;'>
                                    <a class="btn btn-sm btn-info direct_link" style="color: white; border-radius: 25px;" href="/matrix/detail_data/all/{{ $m['mitra'] }}/all">{{ $k }}</a>
                                </td>
                                <td style='text-align: center;'>
                                    @if ($m['totalwo'] != 0)
                                        <a class="btn btn-sm btn-info btn-circle direct_link" style="color: white; border-radius: 25px;" href="/matrix/detail_data/all/{{ $m['mitra'] }}/totalwo">{{ $m['totalwo'] }}</a>
                                    @else
                                        -
                                    @endif
                                </td>
                                <td style='text-align: center;'>
                                    @if ($m['selesai'] != 0)
                                        <a class="btn btn-sm btn-info btn-circle direct_link" style="color: white; border-radius: 25px;" href="/matrix/detail_data/all/{{ $m['mitra'] }}/selesai">{{ $m['selesai'] }}</a>
                                    @else
                                        -
                                    @endif
                                </td>
                                <td style='text-align: center;'>
                                    @if ($m['kendala'] != 0)
                                        <a class="btn btn-sm btn-info btn-circle direct_link" style="color: white; border-radius: 25px;" href="/matrix/detail_data/all/{{ $m['mitra'] }}/kendala">{{ $m['kendala'] }}</a>
                                    @else
                                        -
                                    @endif
                                </td>
                                <td style='text-align: center;'>
                                    @if ($m['pending'] != 0)
                                        <a class="btn btn-sm btn-info btn-circle direct_link" style="color: white; border-radius: 25px;" href="/matrix/detail_data/all/{{ $m['mitra'] }}/pending">{{ $m['pending'] }}</a>
                                    @else
                                        -
                                    @endif
                                </td>
                                <td style='text-align: center;'>
                                    @if ($m['ogp'] != 0)
                                        <a class="btn btn-sm btn-info btn-circle direct_link" style="color: white; border-radius: 25px;" href="/matrix/detail_data/all/{{ $m['mitra'] }}/ogp">{{ $m['ogp'] }}</a>
                                    @else
                                        -
                                    @endif
                                </td>
                                <td style='text-align: center;'>
                                    @if ($m['jalan'] != 0)
                                        <a class="btn btn-sm btn-info btn-circle direct_link" style="color: white; border-radius: 25px;" href="/matrix/detail_data/all/{{ $m['mitra'] }}/jalan">{{ $m['jalan'] }}</a>
                                    @else
                                        -
                                    @endif
                                </td>
                                <td style='text-align: center;'>
                                    @if ($m['sisa'] != 0)
                                        <a class="btn btn-sm btn-info btn-circle direct_link" style="color: white; border-radius: 25px;" href="/matrix/detail_data/all/{{ $m['mitra'] }}/sisa">{{ $m['sisa'] }}</a>
                                    @else
                                        -
                                    @endif
                                </td>
                                <td style='text-align: center;'>
                                    @if ($m['TS'] != 0)
                                        <a class="btn btn-sm btn-info btn-circle direct_link" style="color: white; border-radius: 25px;" href="/matrix/detail_data/all/{{ $m['mitra'] }}/TS">{{ $m['TS'] }}</a>
                                    @else
                                        -
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <td style='text-align: center;'>Total</td>
                            <td style='text-align: center;' id="total" data-total="{{ $sum_totalwo }}">
                                @if($sum_totalwo != 0)
                                    <a class="btn btn-sm btn-info btn-circle direct_link" style="color: white; border-radius: 25px;" href="/matrix/detail_data/all/all/totalwo">{{ $sum_totalwo }}</a>
                                @else
                                    -
                                @endif
                            </td>
                            <td style='text-align: center;' id="selesai" data-selesai="{{ $sum_selesai }}">
                                @if($sum_selesai != 0)
                                    <a class="btn btn-sm btn-info btn-circle direct_link" style="color: white; border-radius: 25px;" href="/matrix/detail_data/all/all/selesai">{{ $sum_selesai }}</a>
                                @else
                                    -
                                @endif
                            </td>
                            <td style='text-align: center;' id="kendala" data-kendala="{{ $sum_kendala }}">
                                @if($sum_kendala != 0)
                                    <a class="btn btn-sm btn-info btn-circle direct_link" style="color: white; border-radius: 25px;" href="/matrix/detail_data/all/all/kendala">{{ $sum_kendala }}</a>
                                @else
                                    -
                                @endif
                            </td>
                            <td style='text-align: center;' id="pending" data-pending="{{ $sum_pending }}">
                                @if($sum_pending != 0)
                                    <a class="btn btn-sm btn-info btn-circle direct_link" style="color: white; border-radius: 25px;" href="/matrix/detail_data/all/all/pending">{{ $sum_pending }}</a>
                                @else
                                    -
                                @endif
                            </td>
                            <td style='text-align: center;' id="ogp" data-ogp="{{ $sum_ogp }}">
                                @if($sum_ogp != 0)
                                    <a class="btn btn-sm btn-info btn-circle direct_link" style="color: white; border-radius: 25px;" href="/matrix/detail_data/all/all/ogp">{{ $sum_ogp }}</a>
                                @else
                                    -
                                @endif
                            </td>
                            <td style='text-align: center;' id="jalan" data-jalan="{{ $sum_jalan }}">
                                @if($sum_jalan != 0)
                                    <a class="btn btn-sm btn-info btn-circle direct_link" style="color: white; border-radius: 25px;" href="/matrix/detail_data/all/all/jalan">{{ $sum_jalan }}</a>
                                @else
                                    -
                                @endif
                            </td>
                            <td style='text-align: center;' id="sisa" data-sisa="{{ $sum_sisa }}">
                                @if($sum_sisa != 0)
                                    <a class="btn btn-sm btn-info btn-circle direct_link" style="color: white; border-radius: 25px;" href="/matrix/detail_data/all/all/sisa">{{ $sum_sisa }}</a>
                                @else
                                    -
                                @endif
                            </td>
                            <td style='text-align: center;' id="ts" data-ts="{{ $TS }}">
                                @if($TS != 0)
                                    <a class="btn btn-sm btn-info btn-circle direct_link" style="color: white; border-radius: 25px;" href="/matrix/detail_data/all/all/ts">{{ $TS }}</a>
                                @else
                                    -
                                @endif
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

    <div class="panel panel-primary">
		<div class="panel-heading header-date">
            Tampilan Per Pekerjaan
            <div class="panel-action">
                <a href="#" data-perform="panel-collapse"><i style="font-size: 20px;" class="ti-minus"></i></a>
            </div>
        </div>
		<div class="panel-body">
			<div class="table-responsive">
				<table id="teknisi" class="table table-striped">
					<thead>
						<tr>
							<th>Jenis Order</th>
							<th>SALDO</th>
							<th>IN-TECH</th>
							<th>SELESAI</th>
							<th>KENDALA</th>
						</tr>
					</thead>
					<tbody>
						@foreach($matrix['matrix_per_pekerjaan'] as $k => $v)
							<tr>
								<td>{{ $k }}</td>
								<td>
									@if($v['saldo'] == 0)
										-
									@else
										<a class="btn btn-sm btn-info btn-circle direct_link" style="color: white; border-radius: 25px;" href="/jointer_report/detail_dataM/all/all/all/{{ $k }}">{{ $v['saldo'] }}</a>
									@endif
								</td>
								<td>
									@if($v['in_tech'] == 0)
										-
									@else
										<a class="btn btn-sm btn-info btn-circle direct_link" style="color: white; border-radius: 25px;" href="/jointer_report/detail_dataM/all/all/all/{{ $k }}">{{ $v['in_tech'] }}</a>
									@endif
								</td>
								<td>
									@if($v['selesai'] == 0)
										-
									@else
										<a class="btn btn-sm btn-info btn-circle direct_link" style="color: white; border-radius: 25px;" href="/jointer_report/detail_dataM/all/all/all/{{ $k }}">{{ $v['selesai'] }}</a>
									@endif
								</td>
								<td>
									@if($v['kendala'] == 0)
										-
									@else
										<a class="btn btn-sm btn-info btn-circle direct_link" style="color: white; border-radius: 25px;" href="/jointer_report/detail_dataM/all/all/all/{{ $k }}">{{ $v['kendala'] }}</a>
									@endif
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>

    @foreach($matrix['matrix'] as $kx => $mx)
        @php
            $sum_totalwo = $sum_selesai = $sum_kendala = $sum_pending = $sum_ogp = $sum_sisa = $sum_jalan = $TS= 0;
            $nomor = 1;
        @endphp
        <div class="panel panel-info">
            <div class="panel-heading header-date">
                TERITORI {{ $kx }} (<b>{{ count($mx) }} Buah</b>)
                <div class="panel-action">
                    <a href="#" data-perform="panel-collapse"><i style="font-size: 20px;" class="ti-minus"></i></a>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table id="teknisi" class="table table-striped">
                        <thead>
                            <tr>
                                <th style="width:20%;">Regu</th>
                                <th>Datel</th>
                                <th>Total Wo</th>
                                <th>Selesai</th>
                                <th>Kendala</th>
                                <th>Pending</th>
                                <th>Ogp</th>
                                <th>Berangkat</th>
                                <th>Sisa</th>
                                <th>List</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($mx as $k => $m)
                            @php $first = true @endphp
                            @foreach ($m as $kk => $vv)
                                @php $sec = true @endphp
                                @foreach ($vv['order'] as $vvv)
                                    <tr>
                                        @if($first == true)
                                            @php
                                                $count_colspan = array_sum(array_map(function($x){
                                                $cs = 0;
                                                $cs += count($x['order']);
                                                return $cs;}, $m) );
                                            @endphp
                                            <td style='text-align: center; vertical-align: middle;' rowspan="{{ $count_colspan }}">{{ $k }}</td>
                                            @php ($first = false) @endphp
                                        @endif
                                        @if($sec == true)
                                            <?php
                                                $sum_totalwo += $vv['totalwo'];
                                                $sum_selesai += $vv['selesai'];
                                                $sum_kendala += $vv['kendala'];
                                                $sum_pending += $vv['pending'];
                                                $sum_ogp     += $vv['ogp'];
                                                $sum_jalan   += $vv['jalan'];
                                                $sum_sisa    += $vv['sisa'];
                                                $TS          += $vv['TS'];
                                            ?>
                                            <td style='text-align: center; vertical-align: middle;' rowspan="{{ count($vv['order']) }}">{{ $kk }}</td>
                                            <td style='text-align: center; vertical-align: middle;' rowspan="{{ count($vv['order']) }}">{{ $vv['totalwo'] == 0 ? '-' : $vv['totalwo'] }}</td>
                                            <td style='text-align: center; vertical-align: middle;' rowspan="{{ count($vv['order']) }}">{{ $vv['selesai'] == 0 ? '-' : $vv['selesai'] }}</td>
                                            <td style='text-align: center; vertical-align: middle;' rowspan="{{ count($vv['order']) }}">{{ $vv['kendala'] == 0 ? '-' : $vv['kendala'] }}</td>
                                            <td style='text-align: center; vertical-align: middle;' rowspan="{{ count($vv['order']) }}">{{ $vv['pending'] == 0 ? '-' : $vv['pending'] }}</td>
                                            <td style='text-align: center; vertical-align: middle;' rowspan="{{ count($vv['order']) }}">{{ $vv['ogp'] == 0 ? '-' : $vv['ogp'] }}</td>
                                            <td style='text-align: center; vertical-align: middle;' rowspan="{{ count($vv['order']) }}">{{ $vv['jalan'] == 0 ? '-' : $vv['jalan'] }}</td>
                                            <td style='text-align: center; vertical-align: middle;' rowspan="{{ count($vv['order']) }}">{{ $vv['sisa'] == 0 ? '-' : $vv['sisa'] }}</td>
                                            @php ($sec = false) @endphp
                                        @endif
                                        <td>
                                            <span data-id="{{ $vvv->odp_nama }}" style="cursor: pointer; color: #000000FF;" class="pull-left show_detail_photo badge label-{{
                                                ($vvv->lt_status == 'Selesai' ? 'success':
                                                ($vvv->lt_status == 'Ogp'?'primary':
                                                ($vvv->lt_status == 'Kendala'?'red':
                                                ($vvv->lt_status == 'Pending'?'dark_orange':
                                                ($vvv->lt_status == 'Tidak Sempat'?'tidak_sempat':
                                                ($vvv->lt_status == 'Berangkat'?'warna_jalan':
                                                ($vvv->lt_status == 'saldo'?'info':
                                                'default')))))))
                                                }}"><a target="_blank" style="color: {{ $vvv->lt_status == 'Berangkat' ? 'white' : 'black' }};" data-toggle="popover" data-placement="top" title="{{ $vvv->odp_nama }} <br/> {{ empty($vvv->lt_status) ? 'Belum Dikerjakan' : ucwords($vvv->lt_status) }}! {{ $vvv->kendala_detail ? '<br/>'.$vvv->kendala_detail : '' }}  <br/> {{empty($vvv->tgl_selesai) ? $vvv->tgl_pengerjaan : $vvv->tgl_selesai}}" data-html="true" data-content="
                                                @if($vvv->lt_status != 'Selesai')
                                                    <a type='button' class='btn btn-info btn-sm' target='_blank' href='{{ !empty($vvv->pt2_stts) ? '' :
                                                    (($vvv->kategory_non_unsc == 0) ? URL::to("/admin/dispatch/edit/add_s/{$vvv->id}") : URL::to("/admin/edit/non_un/{$vvv->id}")) }}'>
                                                    <i data-icon='&#xe005;' class='linea-icon linea-basic fa-fw' style='color:#ffffff; font-size: 20px;'></i>&nbsp;Edit</a>
                                                @endif
                                                <a type='button' style='margin-left:12px;' class='btn btn-info btn-sm' target='_blank' href='/search/data?search={{ $vvv->odp_nama }}'><i data-icon='%' class='linea-icon linea-basic fa-fw' style='color:#ffffff; font-size: 20px;'></i>&nbsp;lihat</a>">{{ $vvv->jenis_wo }} // {{ $get_all_status[$vvv->kategory_non_unsc] }} // {{ $vvv->odp_nama }} // {{ $vvv->project_name }} // {{ $vvv->lt_status ?? 'KOSONG' }}</a>
                                            </span>
                                            <span style=" margin: 0 0 0 6px; color: #000000FF" class="badge">{{ $vvv->durasi }}</span>
                                            @if(empty($vvv->pt2_stts))
                                                <br>
                                            @else
                                                <a type="button" data-id="{{ $vvv->renew_id }}" data-odp="{{ $vvv->odp_nama }}" style=" margin: 0 0 10px 6px; color: #000000FF" class="badge ask_push badge-danger">{{ $vvv->pt2_stts }}</a><br>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @endforeach
                        @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="2">SUM</td>
                                <td id="total" data-total="{{ $sum_totalwo }}">{{ $sum_totalwo?:'-' }}</td>
                                <td id="selesai" data-selesai="{{ $sum_selesai }}">{{ $sum_selesai?:'-' }}</td>
                                <td id="kendala" data-kendala="{{ $sum_kendala }}">{{ $sum_kendala?:'-' }}</td>
                                <td id="pending" data-pending="{{ $sum_pending }}">{{ $sum_pending?:'-' }}</td>
                                <td id="ogp" data-ogp="{{ $sum_ogp }}">{{ $sum_ogp?:'-' }}</td>
                                <td id="jalan" data-jalan="{{ $sum_jalan }}">{{ $sum_jalan?:'-' }}</td>
                                <td id="sisa" data-sisa="{{ $sum_sisa }}">{{ $sum_sisa?:'-' }}</td>
                                <td style="display:none" id="ts" data-ts="{{ $TS }}">{{ $TS?:'-' }}</td>
                                <td></td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    @endforeach
</div>
@endsection
@section('footerS')
<script src="/bower_components/moment/min/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js" integrity="sha512-2ImtlRlf2VVmiGZsjm9bEyhjGW4dU7B6TNwh/hx/iSByxNENtj3WVE6o/9Lj4TJeVXPi4bnOIMXFIJJAeufa0A==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.5.0-beta4/html2canvas.js"></script>
<script src="/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript">
    function startTimer(duration, display, name_item) {
        $('.find_jenis').select2({
            placeholder: 'Pilih Jenis WO'
        });

        $('.find_status').select2({
            placeholder: 'Pilih Status WO'
        });

        $('.find_stts_regu').select2({
            placeholder: 'Pilih Status Regu'
        });

        var timer = duration, minutes, seconds;
        var settan = setInterval(function () {
            minutes = parseInt(timer / 60, 10)
            seconds = parseInt(timer % 60, 10);

            minutes = minutes < 10 ? "0" + minutes : minutes;
            seconds = seconds < 10 ? "0" + seconds : seconds;

            display.text('(' + minutes + ":" + seconds + ')');

            if (--timer < 0) {
                timer = duration;
            }

            if (timer == 0) {
                clearInterval(settan);
                display.text('');
                name_item.attr('disabled', false);
                name_item.css({
                    border: ""
                });
            }
            if (timer != 0) {
                name_item.attr('disabled', true);
            }
        }, 1000);
    }

    $(function(){
        var get_url = window.location.href.split('/')[4],
        tgl_support = {!! json_encode($tgl ?? null) !!};

        if(tgl_support != null){
            get_url = tgl_support;
        }

        $.each($('.direct_link'), function(k, v){
            var load_url = $(this).attr('href');
            $(this).attr('href', load_url +'/'+ get_url);
        });

        var load = {!! json_encode($req) !!};

        if(!$.isEmptyObject(load) ){
            // console.log(load)

            $('#stts_regu').val(load.stts_regu).change();
            $('#status').val(load.status).change();
            $('#jenis').val(load.jenis).change();

            $('input[name="tgl_a"]').val(load.tgl_a);
            $('input[name="tgl_f"]').val(load.tgl_f);

            var tgl_a = load.tgl_a.split('-'),
            tgl_f = load.tgl_f.split(' ')[0].split('-');
            // console.log(tgl_a, tgl_f)
            $('input[name="rangedate"]').val(`${tgl_a[1]}/${tgl_a[2]}/${tgl_a[0]} - ${tgl_f[1]}/${tgl_f[2]}/${tgl_f[0]}`);
        }

        $('.ask_push').on('click', function () {
            var valuen = $(this).attr('data-id'),
            odp = $(this).attr('data-odp');
            console.log(valuen);
            Swal.fire({
                title: 'Seriusan?',
                text: odp +" Akan Dipindahkan Ke PT-2",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, Pindahkan!'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: "GET",
                        url: "/admin/push/pt2/"+valuen,
                        cache: false,
                        success: function(response) {
                            Swal.fire(
                                'Migrasi!',
                                'Orderan berhasil Dipindahkan',
                                'success'
                                )
                            location.reload();
                        }
                    });
                }
            });
        });

        startTimer(60 * 1, $('#send_photo'), $('#button_photo'));

        $('#button_photo').on('click', function(){
            var fiveMinutes = 60 * 1,
            display = $('#send_photo');
            startTimer(fiveMinutes, display,$('#button_photo'));
            $(this).attr('disabled', true);
            $(this).css({
                border: "2px solid #C83C36FF"
            });
            document.getElementById('formlistG').scrollIntoView();
            html2canvas($("#formlistG"), {
                logging: true,
                dpi: 192,
                allowTaint: true
            }).then(function(canvas) {
                var dataImage = canvas.toDataURL("image/png");
                $.ajax({
                    type: "POST",
                    url: "/send_photo_button",
                    data: {
                        data:dataImage,
                        _token: "{{ csrf_token() }}" ,
                        name:"Foto_matrix_button.png"
                    },
                    beforeSend: function() {
                        console.log('dang lah');
                        $("#parent").toggleClass('preloader');
                        $("#child").toggleClass('cssload-speeding-wheel');
                        $("#parent").css({
                            'background-color' : 'rgba(0,0,0,.8)'
                        });
                    },
                    success: function(data) {
                        $("#parent").toggleClass('preloader');
                        $("#child").toggleClass('cssload-speeding-wheel');
                        $("#parent").css({
                            'background-color' : ''
                        });
                    }
                });
            });
        });

        startTimer(60 * 1, $('#send_photo_laphar'), $('#button_photo_Laphar'));

        $('#button_photo_Laphar').on('click', function(){
            var fiveMinutes = 60 * 1,
            display = $('#send_photo');
            startTimer(fiveMinutes, display, $('#button_photo_Laphar'));
            $(this).attr('disabled', true);
            $(this).css({
                border: "2px solid #C83C36FF"
            });
            $.ajax({
                type: "GET",
                url: "/admin/send_laphar",
                beforeSend: function() {
                    console.log('dang lah');
                    $("#parent").toggleClass('preloader');
                    $("#child").toggleClass('cssload-speeding-wheel');
                    $("#parent").css({
                        'background-color' : 'rgba(0,0,0,.8)'
                    });
                },
                success: function(data) {
                    $("#parent").toggleClass('preloader');
                    $("#child").toggleClass('cssload-speeding-wheel');
                    $("#parent").css({
                        'background-color' : ''
                    });
                }
            });
        });

        $(".order").click(function() {
            $(this).toggleClass("new");
        });

        $('input[name="rangedate"]').val();
        var total = $('#total').data('total'),
        selesai = $('#selesai').data('selesai'),
        kendala = $('#kendala').data('kendala'),
        pending = $('#pending').data('pending'),
        ogp = $('#ogp').data('ogp'),
        TS = $('#ts').data('ts'),
        upv = $('#upv').data('upv'),
        jalan = $('#jalan').data('jalan'),
        sisa = $('#sisa').data('sisa'),
        myDoughnutChart,
        checkedString = 'empty';

        function my_chart(){
            var ctx =$('#graph');
            var color_my = [];

            var chartdata = {
                labels:  [
                    'Selesai',
                    'Kendala',
                    'Ogp',
                    'Berangkat',
                    'Sisa',
                    'Pending',
                    'Tidak Sempat'
                ],
                datasets : [
                    {
                        fill: false,
                        lineTension: 0.1,
                        data: [selesai, kendala, ogp, jalan, sisa, pending, TS],
                        backgroundColor: [
                            "#00c292",
                            "#fb3a3a",
                            "#ab8ce4",
                            "#004a7f",
                            "#98a6ad",
                            "#ff8c00",
                            "#ff00d3"
                        ],
                        borderColor:[
                            "#00c292",
                            "#fb3a3a",
                            "#ab8ce4",
                            "#004a7f",
                            "#98a6ad",
                            "#ff8c00",
                            "#ff00d3"
                        ],
                        borderWidth: 1
                    }
                ]
            };
            myDoughnutChart = new Chart(ctx, {
                type: 'pie',
                data: chartdata,
                options: {
                    legend: {
                        position: 'top',
                        labels: {
                            fontColor: "black",
                        }
                    }
                }
            });
        }

        my_chart();

        var data_bar = {!! json_encode($matrix['table']) !!},
        data_chart = [],
        cm = [],
        color_my = [];

        label = ['Jalan', 'Kendala', 'Ogp', 'Pending', 'Selesai'];

        var dynamicColors = function() {
            var r = Math.floor(Math.random() * 255);
            var g = Math.floor(Math.random() * 255);
            var b = Math.floor(Math.random() * 255);
            return "rgb(" + r + "," + g + "," + b + ", 0.8)";
        };

        $.each(data_bar, function(k, v){
            data_bar[k].color = dynamicColors();
        })

        $.each(data_bar, function(k, v){
            data_chart.push({
                label: k,
                backgroundColor: v.color,
                fillColor: "blue",
                strokeColor: "green",
                data: [v.jalan, v.kendala, v.ogp, v.pending, v.selesai]
            })
        })

        function chart_bar(){
            var ctx =$('#bar');
            var data = {
            type: "bar",
            data: {
                labels: label,
                datasets: data_chart
            }
            };
            var myfirstChart = new Chart(ctx, data);
        }

        chart_bar()

        var value = $('input[name="rangedate"]').val(),
        date    = value.split(' '),
        slice1  = date[0].split("/").reverse(),
        month1_ = slice1[0]+'-'+slice1[2]+'-'+slice1[1],
        slice2  = date[2].split("/").reverse(),
        month2_ = slice2[0]+'-'+slice2[2]+'-'+slice2[1]+' 23:59:59';

        $('input[name="tgl_a"]').val(month1_);
        $('input[name="tgl_f"]').val(month2_);

        $('[data-toggle="tooltip"]').tooltip();

        $('input[name="rangedate"]').daterangepicker({
            opens: 'right'
        }, function(start, end){
            var month1 = start.format('YYYY-MM-DD'),
            month2 = end.format('YYYY-MM-DD')+" 23:59:59";
            $('input[name="tgl_a"]').val(month1);
            $('input[name="tgl_f"]').val(month2);
        });

        $('.kalender').click(function(e){
            e.preventDefault();
            $('input[name="rangedate"]').click();
        });

    });
</script>
@endsection