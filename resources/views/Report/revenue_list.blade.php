@extends('layout')
@section('title', 'List Material')
@section('headerS')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link href="/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.js"></script>
@endsection
@section('style')
<style type="text/css">
	th{
		text-align: center;
		white-space:nowrap;
	}
	div>table {
		float: left
	}
</style>
@endsection
@section('content')
<div class="container-fluid" style="padding-top: 25px;">
		<a type="button" href="/material/revenue_setting/input" class="btn btn-default btn-sm" id="button_photo" style="margin-bottom: 25px;"><span data-icon="U" class="linea-icon linea-basic fa-fw" style="font-size: 20px; vertical-align:middle;" ></span>&nbsp;Tambah Material <span id="send_photo"></span></a>
		<div class="panel panel-warning">
			<div class="panel-heading header-date">Ranked Periode {{ Request::segment(2) }}</div>
			<div class="panel-body">
				<div class="table-responsive">
					<table id="teknisi" class="table table-striped table-hover table-bordered">
						<thead>
							<tr>
								<th rowspan ="2" style="vertical-align: middle;">#</th>
								<th rowspan ="2" style="vertical-align: middle;">Nama Item</th>
								<th rowspan ="2" style="vertical-align: middle; width: 30%;">Desc</th>
								<th rowspan ="2" style="vertical-align: middle;">Satuan</th>
								<th colspan ="2">Telkom</th>
								<th colspan ="2">Mitra</th>
								<th rowspan ="2" colspan="2" style="vertical-align: middle;">Action</th>
							</tr>
							<tr>
								<th>Harga</th>
								<th>jasa</th>
								<th>Harga</th>
								<th>jasa</th>
							</tr>
						</thead>
						<tbody>
							@foreach($data as $no => $r)
							<tr>
								<td>{{ ++$no }}</td>
								<td>{{ $r->id_item }}</td>
								<td>{{ $r->uraian }}</td>
								<td>{{ $r->satuan }}</td>
								<td>{{ number_format($r->material_telkom, 0, '', ',') }}</td>
								<td>{{ number_format($r->jasa_telkom, 0, '', ',') }}</td>
								<td>{{ number_format($r->material_ta, 0, '', ',') }}</td>
								<td>{{ number_format($r->jasa_ta, 0, '', ',') }}</td>
								<td>
									@if (in_array(Session::get('auth')->pt2_level, [2, 5]))
										<a type="button" href='{{URL::to("/material/revenue_setting/{$r->id_item}")}}' class="btn btn-light btn-sm" id="button_photo"><span data-icon="-" class="linea-icon linea-software fa-fw" style="font-size: 20px; vertical-align:middle;" ></span>&nbsp;Edit<span id="send_photo"></span></a>
									@endif
								</td>
								<td>
									@if (in_array(Session::get('auth')->pt2_level, [2, 5]))
										<a type="button" class="btn btn-light btn-sm delete_rev" data-id='{{$r->id_item}}' href='#'><i data-icon="&#xe01c;" class="linea-icon linea-basic fa-fw" style="color:#ff0000; font-size: 20px; vertical-align:middle;"></i><span style="color:#ff0000;">&nbsp;Hapus</span></a>
									@endif
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
</div>
@endsection
@section('footerS')
<script src="/bower_components/moment/min/moment.min.js"></script>
<script src="/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script>
	$(function(){
		$('.delete_rev').click(function(){
			var value = $(this).data('id');
			Swal.fire({
				title: '<strong>Hapus Material <u>'+value+'</u></strong>',
				type: 'warning',
				html:
				'<b>'+value+'</b>, ' +
				'Tidak Bisa Dikembalikan! <br/>' +
				'Apakah Anda Yakin?',
				showCloseButton: true,
				showCancelButton: true,
				focusConfirm: false,
				confirmButtonText:
				'<span style="font-size: 20px;"><i data-icon="&#xe01c;" class="linea-icon linea-basic"></i> Ya!</span>',
				cancelButtonText:
				'<span style="font-size: 20px;"><i data-icon="&#xe016;" class="linea-icon linea-aerrow"></i></span>',
			}).then((result) => {
				if (result.value) {
					$.ajax({
						type: "GET",
						data: {id : value},
						url: "/material/revenue_delete",
						cache: false,
						success: function(response) {
							Swal.fire(
								'Berhasil!',
								note,
								'success'
								)
							location.reload();
						}
					});
				}
			});
		});

	})

</script>
@endsection