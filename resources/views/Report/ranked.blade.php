@extends('layout')
@section('title', 'Ranked')
@section('headerS')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link href="/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.js"></script>
@endsection
@section('style')
<style type="text/css">
    th, td{
        text-align: center;
        white-space:nowrap;
    }
    div>table {
        float: left
    }
</style>
@endsection
@section('content')
<div class="container-fluid" style="padding-top: 25px;">
    <!-- <button type="button" class="btn btn-default" id="button_photo"><span data-icon="[" class="linea-icon linea-basic fa-fw" style="font-size: 20px; vertical-align:middle;" ></span>&nbsp;Kirim Group Telegram Laporan PT2<span id="send_photo"></span></button> -->
    <div id="canvas-wrapper">
        <canvas id="graph"></canvas>
    </div>
    <div class="panel panel-warning">
        <div class="panel-heading header-date">Ranked Periode {{ Request::segment(2) }}</div>
        <div class="panel-body">
            <form id="formlistG" name="formlistG">
                <div class='input-group date'>
                    {{-- <input type='text' class="form-control" name='rangedate' value="{{date("m/d/Y", strtotime("first day of this month"))}} - {{date("m/d/Y", strtotime("last day of this month"))}}"  disabled> --}}
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar kalender" style="cursor: pointer;"></span>
                    </span>
                </div>
            </form>
            <div class="table-responsive">
                <table id="teknisi" class="table table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Regu</th>
                            <th>Team Leader</th>
                            <th>Jumlah Naik</th>
                            <th>Jumlah Pending</th>
                            <th>Jumlah Kendala</th>
                            <th>Jumlah Belum Progress</th>
                            {{-- <th>Persentase</th> --}}
                            {{-- <th>Point</th> --}}
                        </tr>
                    </thead>
                    <tbody>
                        @php
                        $selesai = $Kendala = $Pending = $points  = $totals = $np = 0;
                        @endphp
                        @foreach($ranked as $no => $r)
                        @php
                        $point   = $r->selesai*25;
                        $selesai += $r->selesai;
                        $Pending += $r->Pending;
                        $Kendala += $r->Kendala;
                        $np += $r->np;
                        // $points  += $point;
                        // try {
                        //     $total   = @((($r->selesai * 100) + ($r->Kendala * 30)) / ($r->selesai + $r->Kendala)) ;
                        //     if (in_array($total, [INF, NAN])) {
                        //         throw new DivisionByZeroError('Division by zero error');
                        //     }
                        // } catch (DivisionByZeroError $e) {
                        //     $total = 0;
                        // }
                        // $totals  += $total;
                        @endphp
                        <tr>
                            <td>{{ ++$no }}</td>
                            <td>{{ $r->uraian }} {!! $r->ACTIVE == 0 ? "<b style='color: red;'>Tidak Aktif!</b>" : '' !!}</td>
                            <td>{{ $r->TL == 0 ? 'Tidak Ada TEAM LEADER' : $r->TL }}</td>
                            <td>{{ $r->selesai == 0 ? '-' : $r->selesai }}</td>
                            <td>{{ $r->Pending == 0 ? '-' : $r->Pending }}</td>
                            <td>{{ $r->Kendala == 0 ? '-' : $r->Kendala }}</td>
                            <td>{{ $r->np == 0 ? '-' : $r->np }}</td>
                            {{-- <td>{{ $point?:'-' }}</td> --}}
                            {{-- <td>{{ number_format((float)$total, 2, ',', '') }}%</td> --}}
                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="3">Total Keseluruhan</td>
                            <td>{{ $selesai }}</td>
                            <td>{{ $Pending }}</td>
                            <td>{{ $Kendala }}</td>
                            <td>{{ $np }}</td>
                            {{-- <td colspan="2">{{ $points }}</td> --}}
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@section('footerS')
<script src="/bower_components/moment/min/moment.min.js"></script>
<script src="/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script>
    function startTimer(duration, display) {
        var timer = duration, minutes, seconds;
        var settan = setInterval(function () {
            minutes = parseInt(timer / 60, 10)
            seconds = parseInt(timer % 60, 10);

            minutes = minutes < 10 ? "0" + minutes : minutes;
            seconds = seconds < 10 ? "0" + seconds : seconds;

            display.text('(' + minutes + ":" + seconds + ')');

            if (--timer < 0) {
                timer = duration;
            }

            if (timer == 0) {
                clearInterval(settan);
                display.text('');
                $('#button_photo').attr('disabled', false);
                $( "#button_photo" ).css({
                    border: ""
                });
            }
            if (timer != 0) {
                $('#button_photo').attr('disabled', true);
            }
        }, 1000);
    }

    $(function(){

        $('#button_photo').on('click', function(){
            var fiveMinutes = 60 * 1,
            display = $('#send_photo');
            startTimer(fiveMinutes, display);
            $(this).attr('disabled', true);
            $(this).css({
                border: "2px solid #C83C36FF"
            });
        });

        var barGraph;
        function my_chart(month1,month2){
            var ctx =$('#graph');
            $.ajax({
                url:"/ranked/ajax/"+month1+"/"+month2,
                type:"GET",
                dataType:"json",
                success: function(data) {
                    /*console.log(JSON.parse(JSON.stringify(data)));*/
                    var dynamicColors = function() {
                        var r = Math.floor(Math.random() * 255);
                        var g = Math.floor(Math.random() * 255);
                        var b = Math.floor(Math.random() * 255);
                        return "rgb(" + r + "," + g + "," + b + ", 0.2)";
                    };
                    var label_regu = [],
                    count = [],
                    color_my = [];

                    for (var i in data) {
                        label_regu.push(data[i].uraian.match(/.{1,10}/g));
                        count.push(data[i].selesai *25);
                        color_my.push(dynamicColors());
                        /*console.log("warna "+color_my);*/
                    }

                    var chartdata = {
                        labels: label_regu,
                        datasets : [
                            {
                                label: 'Team PT2',
                                fill: false,
                                lineTension: 0.1,
                                data: count,
                                backgroundColor: color_my,
                                borderColor: color_my ,
                                borderWidth: 1
                            }
                        ]
                    };

                    console.log(chartdata, label_regu, count)

                    barGraph = Chart.Bar(ctx, {
                        data: chartdata,
                        options: {
                            legend: {
                                display: false
                            },
                            tooltips: {
                                mode: 'index',
                                intersect: false,
                                callbacks: {
                                    label: function(tooltipItem) {
                                        return tooltipItem.yLabel;
                                    }
                                }
                            },
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        fontColor: "black",
                                        beginAtZero: true
                                    }
                                }],
                                xAxes: [{
                                    ticks: {
                                        fontColor: "black",
                                        beginAtZero: true
                                    }
                                }]
                            }
                        }
                    });
                },
                error: function(data) {
                    /*console.log(data);*/
                }
            });
        }

        my_chart({!! json_encode($month1) !!}, {!! json_encode($month2) !!});

        // var value = $('input[name="rangedate"]').val(),
        // date = value.split(' '),
        // slice1 = date[0].split("/").reverse(),
        // month1 = slice1[0]+'-'+slice1[2]+'-'+slice1[1],
        // slice2 = date[2].split("/").reverse(),
        // month2 = slice2[0]+'-'+slice2[2]+'-'+slice2[1];

        // $('input[name="rangedate"]').daterangepicker({
        //     opens: 'left'
        // }, function(start, end){
        //     var month1 = start.format('YYYY-MM-DD'),
        //     month2 = end.format('YYYY-MM-DD');
        //     /*console.log(month1);*/
        //     $.ajax({
        //         url:"/ranked/search/"+month1+"/"+month2,
        //         type:'GET',
        //         beforeSend: function(){
        //             $.toast({
        //                 heading: 'Pemberitahuan',
        //                 text: 'Harap Tunggu Sebentar',
        //                 position: 'mid-center',
        //                 icon: 'info',
        //                 hideAfter: 2000,
        //                 stack: false
        //             })
        //         },
        //         success: function(data){
        //             $('#teknisi').html(data);
        //             $('.header-date').html("Ranked Periode "+month1+" Sampai "+end.format('DD-MM-YYYY'));
        //         }

        //     });
        //     $.ajax({
        //         url:"/ranked/ajax/"+month1+"/"+month2,
        //         type:"GET",
        //         dataType:"json",
        //         success: function(data) {
        //             /*console.log(data);*/
        //             var dynamicColors = function() {
        //                 var r = Math.floor(Math.random() * 255);
        //                 var g = Math.floor(Math.random() * 255);
        //                 var b = Math.floor(Math.random() * 255);
        //                 return "rgb(" + r + "," + g + "," + b + ", 0.2)";
        //             }
        //             var color_my = [];
        //             barGraph.data.labels = [];
        //             barGraph.data.datasets[0].data = [];
        //             for(var i in data){
        //                 color_my.push(dynamicColors());
        //                 barGraph.data.datasets[0].data[i] = data[i].selesai *25;
        //                 barGraph.data.labels[i]=data[i].uraian;
        //                 barGraph.data.datasets[0].backgroundColor=color_my;
        //                 barGraph.data.datasets[0].borderColor=color_my;
        //             }
        //             barGraph.update();
        //         }
        //     })
        // });

        // $('.kalender').click(function(e){
        //     e.preventDefault();
        //     $('input[name="rangedate"]').click();
        // });
    });
</script>
@endsection