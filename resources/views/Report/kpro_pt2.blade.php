@extends('layout')
@section('title', 'Matrix')
@section('headerS')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link href="/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css"
	integrity="sha512-nMNlpuaDPrqlEls3IX/Q56H36qvBASwb3ipuo3MxeWbsQB1881ox0cRv7UPTgBlriqoynt35KjEwgGUeUXIPnw=="
	crossorigin="anonymous" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.js"></script>
@endsection
@section('style')
<style type="text/css">
	th {
		white-space: nowrap;
	}

	div>table {
		float: left;
	}

	.detail_matrix_layout {
		overflow: auto;
		overflow-y: auto;
	}

	.label-green {
		background-color: #66A955FF;
	}

	.label-dark_orange {
		background-color: #FF8C00;
	}

	.label-warna_jalan {
		background-color: #004A7FFF;
	}

	.label-tidak_sempat {
		background-color: #FF00D3FF;
	}

	.label-primary {
		background-color: #ab8ce4;
	}

	.label-default {
		background-color: #98a6ad;
	}

	.badge {
		font-size: 11px;
		color: black;
	}

	#teknisi {
		font-size: 0.85em;
	}
</style>
@endsection
@section('content')
<div class="container-fluid" style="padding-top: 25px;">
	<div id="canvas-wrapper" style="width: 40%; height: 40%; margin-left: auto; margin-right: auto;">
		<canvas id="graph"></canvas>
	</div>
	<div class="panel panel-warning">
		<div class="panel-heading header-date">Matrix Periode {{ Request::segment(2) }}</div>
		<div class="panel-body">
			<form id="formlistG" name="formlistG" method="get">
				<div class="form-group col-md-12">
					<div class="form-inline">
                        <div class="checkbox checkbox-default checkbox-circle">
                            <input id="checkbox_belum" type="checkbox" name="status[]" value="'need_progres'">
							<label for="checkbox_belum"><span class="badge label-default order">Need
									Progress</span></label>
						</div>
						<div class="checkbox checkbox-default checkbox-circle">
							<input id="checkbox_ogp" type="checkbox" name="status[]" value="'Ogp'">
							<label for="checkbox_ogp"><span class="badge label-primary">OGP</span></label>
						</div>
						<div class="checkbox checkbox-default checkbox-circle">
							<input id="checkbox_selesai" type="checkbox" name="status[]" value="'Selesai'">
							<label for="checkbox_selesai"><span class="badge badge-success">Selesai</span></label>
						</div>
						<div class="checkbox checkbox-default checkbox-circle">
							<input id="checkbox_kendala" type="checkbox" name="status[]" value="'Kendala'">
							<label for="checkbox_kendala"><span class="badge badge-danger">Kendala</span></label>
						</div>
						<div class="checkbox checkbox-default checkbox-circle">
							<input id="checkbox_Pending" type="checkbox" name="status[]" value="'Pending'">
							<label for="checkbox_Pending"><span class="badge label-green">Pending</span></label>
						</div>
						<div class="checkbox checkbox-default checkbox-circle" style="margin-left: 10px;">
							<input id="checkbox_Jalan" type="checkbox" name="status[]" value="'Berangkat'">
							<label for="checkbox_Jalan"><span class="badge label-warna_jalan">Berangkat</span></label>
						</div>
						<div class="checkbox checkbox-default checkbox-circle" style="margin-left: 10px;">
							<input id="checkbox_lebih7" type="checkbox" name="status[]" value="'>7'">
							<label for="checkbox_lebih7"><span class="badge label-info">Lebih 7 Hari
									(Saldo)</span></label>
						</div>
						<div class="checkbox checkbox-default checkbox-circle" style="margin-left: 10px;">
							<input id="checkbox_tidak_sempat" type="checkbox" name="status[]" value="'Tidak Sempat'">
							<label for="checkbox_tidak_sempat"><span class="badge label-tidak_sempat">Tidak
									Sempat</span></label>
						</div>
					</div>
				</div>
				<div class="form-group col-md-4">
					<select id="jenis" name="jenis[]" class="form-control find_jenis" multiple="">
						<option value=0>UNSC</option>
						<option value=1>NON UNSC</option>
						<option value=2>IN</option>
						<option value=3>PT2 PLUS</option>
						<option value=4>UNSC MANUAL</option>
					</select>
				</div>
				<div class='input-group date col-md-4'>
					<input type='text' class="form-control" name='rangedate' disabled
						value="{{date("m/d/Y")}} - {{date("m/d/Y")}}">
					<input type="hidden" name="tgl_a">
					<input type="hidden" name="tgl_f">
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-calendar kalender" style="cursor: pointer;"></span>
					</span>
				</div>
				<div class='input-group date col-md-4'>
					<button type="submit" class="btn btn-primary">Cari</button>
				</div>
			</form>
			<div class="table-responsive">
				<table id="teknisi" class="table table-striped">
					<thead>
						<tr>
							<th>#</th>
							<th style="width:20%;">Regu</th>
							<th>Total Wo</th>
							<th>Selesai</th>
							<th>Kendala</th>
							<th>Pending</th>
							<th>Ogp</th>
							<th>Berangkat</th>
							<th>Sisa</th>
							<th>List</th>
						</tr>
					</thead>
					<tbody>
						@php
						$sum_totalwo = $sum_selesai = $sum_kendala = $sum_pending = $sum_ogp = $sum_sisa = $sum_jalan = $TS = 0;
						$nomor = 1;
						@endphp
						@foreach($matrix as $no => $m)
						@if(!empty($m->totalwo))
						<tr>
							<td>{{ $nomor++ }}</td>
							<td>{{ $m->uraian }}</td>
							<td>{{ $m->totalwo?:'-' }}</td>
							<td>{{ $m->selesai?:'-' }}</td>
							<td>{{ $m->kendala?:'-' }}</td>
							<td>{{ $m->pending?:'-' }}</td>
							<td>{{ $m->ogp?:'-' }}</td>
							<td>{{ $m->jalan?:'-' }}</td>
							<td>{{ $m->sisa?:'-' }}</td>
							<td>
								@if(isset($m->order))
								@foreach(@$m->order as $order)
								<span data-id="{{ $order->odp_nama }}" data-nama="{{ $order->id }}"
									style="cursor: pointer; margin: 0 0 10px 0; color: #000000FF;" class="pull-left show_detail_photo badge label-{{
                                    ($order->lt_status == 'Selesai' ? 'success':
                                    ($order->lt_status == 'Ogp'?'primary':
                                    ($order->lt_status == 'Kendala'?'danger':
                                    ($order->lt_status == 'Pending'?'green':
                                    ($order->lt_status == 'Tidak Sempat'?'tidak_sempat':
                                    ($order->lt_status == 'Berangkat'?'warna_jalan':
                                    ($order->lt_status == 'saldo'?'info':
                                    'default')))))))
                                    }}"><a target="_blank" style="color: black;" data-toggle="popover"
										data-placement="top"
										title="{{ empty($order->lt_status) ? 'Belum Dikerjakan' : ucwords($order->lt_status) }}! <br/> {{empty($order->tgl_selesai) ? $order->tgl_pengerjaan : $order->tgl_selesai}}"
										data-html="true"
										data-content="<a type='button' class='btn btn-info btn-sm' target='_blank' href='{{ !empty($order->pt2_stts) ? '' : (($order->kategory_non_unsc == 0) ? URL::to("/admin/dispatch/edit/add_s/{$order->id}") : URL::to("/admin/edit/non_un/{$order->id}")) }}'><i data-icon='&#xe005;' class='linea-icon linea-basic fa-fw' style='color:#ffffff; font-size: 20px;'></i>&nbsp;Edit</a>">{{ $order->odp_nama }}</a>
								</span>
								<span style=" margin: 0 0 10px 6px; color: #000000FF"
									class="badge">{{ $order->durasi }}</span>
								@if(empty($order->pt2_stts))
								<br>
								@else
								<a type="button" data-id="{{ $order->renew_id }}" data-odp="{{ $order->odp_nama }}"
									style=" margin: 0 0 10px 6px; color: #000000FF"
									class="badge ask_push badge-danger">{{ $order->pt2_stts }}</a><br>
								@endif
								@endforeach
								@endif
							</td>
						</tr>
						<?php
                        $sum_totalwo+=$m->totalwo;
                        $sum_selesai+=$m->selesai;
                        $sum_kendala+=$m->kendala;
                        $sum_pending+=$m->pending;
                        $sum_ogp+=$m->ogp;
                        $sum_jalan+=$m->jalan;
                        $sum_sisa+=$m->sisa;
                        $TS +=$m->TS;
                        ?>
						@endif
						@endforeach
						<tr>
							<td colspan="2">SUM</td>
							<td id="total" data-total="{{ $sum_totalwo }}">{{ $sum_totalwo?:'-' }}</td>
							<td id="selesai" data-selesai="{{ $sum_selesai }}">{{ $sum_selesai?:'-' }}</td>
							<td id="kendala" data-kendala="{{ $sum_kendala }}">{{ $sum_kendala?:'-' }}</td>
							<td id="pending" data-pending="{{ $sum_pending }}">{{ $sum_pending?:'-' }}</td>
							<td id="ogp" data-ogp="{{ $sum_ogp }}">{{ $sum_ogp?:'-' }}</td>
							<td id="jalan" data-jalan="{{ $sum_jalan }}">{{ $sum_jalan?:'-' }}</td>
							<td id="sisa" data-sisa="{{ $sum_sisa }}">{{ $sum_sisa?:'-' }}</td>
							<td style="display:none" id="ts" data-ts="{{ $TS }}">{{ $TS?:'-' }}</td>
							<td></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection
@section('footerS')
<script src="/bower_components/moment/min/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js"
	integrity="sha512-2ImtlRlf2VVmiGZsjm9bEyhjGW4dU7B6TNwh/hx/iSByxNENtj3WVE6o/9Lj4TJeVXPi4bnOIMXFIJJAeufa0A=="
	crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.5.0-beta4/html2canvas.js"></script>
<script src="/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript">
	function startTimer(duration, display, name_item) {
        $('.find_jenis').select2({
            placeholder: 'Pilih Jenis WO'
        });
        var timer = duration, minutes, seconds;
        var settan = setInterval(function () {
            minutes = parseInt(timer / 60, 10)
            seconds = parseInt(timer % 60, 10);

            minutes = minutes < 10 ? "0" + minutes : minutes;
            seconds = seconds < 10 ? "0" + seconds : seconds;

            display.text('(' + minutes + ":" + seconds + ')');

            if (--timer < 0) {
                timer = duration;
            }

            if (timer == 0) {
                clearInterval(settan);
                display.text('');
                name_item.attr('disabled', false);
                name_item.css({
                    border: ""
                });
            }
            if (timer != 0) {
                name_item.attr('disabled', true);
            }
        }, 1000);
    }

    $(function(){
        $('.ask_push').on('click', function () {
            var valuen = $(this).attr('data-id'),
            odp = $(this).attr('data-odp');
            console.log(valuen);
            Swal.fire({
                title: 'Seriusan?',
                text: odp +" Akan Dipindahkan Ke PT-2",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, Pindahkan!'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: "GET",
                        url: "/admin/push/pt2/"+valuen,
                        cache: false,
                        success: function(response) {
                            Swal.fire(
                                'Migrasi!',
                                'Orderan berhasil Dipindahkan',
                                'success'
                                )
                            location.reload();
                        }
                    });
                }
            });
        });

        startTimer(60 * 1, $('#send_photo'), $('#button_photo'));

        $('#button_photo').on('click', function(){
            var fiveMinutes = 60 * 1,
            display = $('#send_photo');
            startTimer(fiveMinutes, display,$('#button_photo'));
            $(this).attr('disabled', true);
            $(this).css({
                border: "2px solid #C83C36FF"
            });
            document.getElementById('formlistG').scrollIntoView();
            html2canvas($("#formlistG"), {
                logging: true,
                dpi: 192,
                allowTaint: true
            }).then(function(canvas) {
                var dataImage = canvas.toDataURL("image/png");
                $.ajax({
                    type: "POST",
                    url: "/send_photo_button",
                    data: {
                        data:dataImage,
                        _token: "{{ csrf_token() }}" ,
                        name:"Foto_matrix_button.png"
                    },
                    beforeSend: function() {
                        console.log('dang lah');
                        $("#parent").toggleClass('preloader');
                        $("#child").toggleClass('cssload-speeding-wheel');
                        $("#parent").css({
                            'background-color' : 'rgba(0,0,0, .8)'
                        });
                    },
                    success: function(data) {
                        $("#parent").toggleClass('preloader');
                        $("#child").toggleClass('cssload-speeding-wheel');
                        $("#parent").css({
                            'background-color' : ''
                        });
                    }
                });
            });
        });

        startTimer(60 * 1, $('#send_photo_laphar'), $('#button_photo_Laphar'));

        $('#button_photo_Laphar').on('click', function(){
            var fiveMinutes = 60 * 1,
            display = $('#send_photo');
            startTimer(fiveMinutes, display, $('#button_photo_Laphar'));
            $(this).attr('disabled', true);
            $(this).css({
                border: "2px solid #C83C36FF"
            });
            $.ajax({
                type: "GET",
                url: "/admin/send_laphar",
                beforeSend: function() {
                    console.log('dang lah');
                    $("#parent").toggleClass('preloader');
                    $("#child").toggleClass('cssload-speeding-wheel');
                    $("#parent").css({
                        'background-color' : 'rgba(0,0,0,.8)'
                    });
                },
                success: function(data) {
                    $("#parent").toggleClass('preloader');
                    $("#child").toggleClass('cssload-speeding-wheel');
                    $("#parent").css({
                        'background-color' : ''
                    });
                }
            });
        });


        $('input[name="status[]"]').prop("checked", true);

        $(".order").click(function() {
            $(this).toggleClass("new");
        });

        $('input[name="rangedate"]').val();
        var total = $('#total').data('total'),
        selesai = $('#selesai').data('selesai'),
        kendala = $('#kendala').data('kendala'),
        pending = $('#pending').data('pending'),
        ogp = $('#ogp').data('ogp'),
        TS = $('#ts').data('ts'),
        upv = $('#upv').data('upv'),
        jalan = $('#jalan').data('jalan'),
        sisa = $('#sisa').data('sisa'),
        myDoughnutChart,
        checkedString = 'empty';

        function my_chart(){
            var ctx =$('#graph');
            var color_my = [];

            var chartdata = {
                labels:  [
                'Selesai ('+selesai+')',
                'Kendala ('+kendala+')',
                'Ogp ('+ogp+')',
                'Berangkat ('+jalan+')',
                'Sisa ('+sisa+')',
                'Pending ('+pending+')',
                'Tidak Sempat ('+TS+')'
                ],
                datasets : [
                {
                    fill: false,
                    lineTension: 0.1,
                    data: [selesai, kendala, ogp, jalan, sisa, pending, TS],
                    backgroundColor: [
                    "#00c292",
                    "#fb9678",
                    "#ab8ce4",
                    "#004a7f",
                    "#98a6ad",
                    "#66a955",
                    "#ff00d3"
                    ],
                    borderColor:[
                    "#00c292",
                    "#fb9678",
                    "#ab8ce4",
                    "#004a7f",
                    "#98a6ad",
                    "#66a955",
                    "#ff00d3"
                    ],
                    borderWidth: 1
                }
                ]
            };
            myDoughnutChart = new Chart(ctx, {
                type: 'doughnut',
                data: chartdata,
                options: {
                    legend: {
                        position: 'right',
                        labels: {
                            fontColor: "black",
                        }
                    }
                }
            });
        }

        my_chart();

        var value = $('input[name="rangedate"]').val(),
        date    = value.split(' '),
        slice1  = date[0].split("/").reverse(),
        month1_ = slice1[0]+'-'+slice1[2]+'-'+slice1[1],
        slice2  = date[2].split("/").reverse(),
        month2_ = slice2[0]+'-'+slice2[2]+'-'+slice2[1]+' 23:59:59';
        $('input[name="tgl_a"]').val(month1_);
        $('input[name="tgl_f"]').val(month2_);

        $('[data-toggle="tooltip"]').tooltip();

        $('input[name="rangedate"]').daterangepicker({
            opens: 'left'
        }, function(start, end){
            var month1 = start.format('YYYY-MM-DD'),
            month2 = end.format('YYYY-MM-DD')+" 23:59:59";
            $('input[name="tgl_a"]').val(month1);
            $('input[name="tgl_f"]').val(month2);
        });

        $('.kalender').click(function(e){
            e.preventDefault();
            $('input[name="rangedate"]').click();
        });

    });
</script>
@endsection