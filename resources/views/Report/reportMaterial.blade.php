@extends('layout')
@section('title', 'Report Material')
@section('headerS')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link href="/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
@endsection
@section('style')
<style type="text/css">
    th, td {
        text-align: center;
        white-space:nowrap;
    }
    div>table {
        float: left
    }
</style>
@endsection
@section('content')
<div class="container-fluid" style="padding-top: 25px;">
    <a type="button" href="#" class="btn btn-default" id="download_excel_material"><span data-icon="&#xe007;" class="linea-icon linea-basic fa-fw" style="font-size: 20px; vertical-align:middle;" ></span>&nbsp;Unduh Excel</a>
    <div style="padding-top: 25px;">
        <div class="panel panel-warning">
            <div class="panel-heading header-date">Report Material Periode {{ Request::segment(2) }}</div>
            <div class="panel-body">
                <form id="formlistG" name="formlistG">
                    <div class='input-group date'>
                        <input type='text' class="form-control" name='rangedate' value="{{date("m/d/Y", strtotime("first day of this month"))}} - {{date("m/d/Y", strtotime("last day of this month"))}}" readonly>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar kalender" style="cursor: pointer;"></span>
                        </span>
                    </div>
                </form>
                <div class="table-responsive">
                    <table id="teknisi"  class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Node</th>
                                @foreach($head as $h)
                                <th>{{ $h }}</th>
                                @endforeach
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $no => $r)
                            <tr>
                                <td>{{ ++$no }}</td>
                                <td>{{ $r['odp_nama'] }}</td>
                                @foreach($r['id_item'] as $no2 => $item)
                                <td>{!! $item ?"<span class=\"label label-info\">$item</span>": '-' !!}</td>
                                @endforeach
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('footerS')
<script src="/bower_components/moment/min/moment.min.js"></script>
<script src="/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script>
    function startTimer(duration, display) {
        var timer = duration, minutes, seconds;
        var settan = setInterval(function () {
            minutes = parseInt(timer / 60, 10)
            seconds = parseInt(timer % 60, 10);

            minutes = minutes < 10 ? "0" + minutes : minutes;
            seconds = seconds < 10 ? "0" + seconds : seconds;

            display.text('(' + minutes + ":" + seconds + ')');

            if (--timer < 0) {
                timer = duration;
            }

            if (timer == 0) {
                clearInterval(settan);
                display.text('');
                $('#button_photo').attr('disabled', false);
                $( "#button_photo" ).css({
                    border: ""
                });
            }
        }, 1000);
    }

    $(function(){

        function formatDate_report_material_awal() {
            var d = new Date(),
            month = '' + (d.getMonth() + 1),
            year = d.getFullYear(),
            day = '01';

            if (month.length < 2) month = '0' + month;

            return [year, month, day].join('-');
        }

        function formatDate_report_material_akhir() {
            var d = new Date(),
            month = '' + (d.getMonth() +1),
            year = d.getFullYear(),
            day = d.getDate();

            if (month.length < 2) month = '0' + month;
            if (day < 10) day = '0' + day;

            return [year, month, day].join('-');
        }

        function download_excel_laporan(){
            var oldUrl = $('#download_excel_material').attr('href'),
            newUrl = oldUrl.replace("#", "/Download/file/material_exc/"+formatDate_report_material_awal()+"/"+formatDate_report_material_akhir());
            return $('#download_excel_material').attr("href", newUrl);
        }

        download_excel_laporan();

        $('#button_photo').on('click', function(){
            var fiveMinutes = 60 * 1,
            display = $('#send_photo');
            startTimer(fiveMinutes, display);
            $(this).attr('disabled', true);
            $(this).css({
                border: "2px solid #C83C36FF"
            });
        });

        $('input[name="rangedate"]').daterangepicker({
            opens: 'left'
        }, function(start, end){
            var month1 = start.format('YYYY-MM-DD'),
            month2 = end.format('YYYY-MM-DD')+" 23:59:59";
            $.ajax({
                url:"/reportMaterial/search/"+month1+"/"+month2,
                type:'GET',
                beforeSend: function(){
                    $.toast({
                        heading: 'Pemberitahuan',
                        text: 'Harap Tunggu Sebentar',
                        position: 'mid-center',
                        icon: 'info',
                        hideAfter: 2000,
                        stack: false
                    })		
                },
                success: function(data){
                    $('#teknisi').html(data);
                    $('.header-date').html("Report Material Periode "+start.format('DD-MM-YYYY')+" Sampai "+end.format('DD-MM-YYYY'));
                    $('#download_excel_material').attr("href", "/Download/file/material_exc/"+month1+"/"+month2);
                }

            });
        });

        $('.kalender').click(function(e){
            e.preventDefault();
            $('input[name="rangedate"]').click();
        });

    });

</script>
@endsection