@extends('layout')
@section('title', 'Detail Rekap Bulanan')
@section('headerS')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link href="/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.js"></script>
@endsection
@section('style')
<style type="text/css">
    th, td{
        text-align: center;
    }
    div>table {
        float: left;
    }
    .down{
        -moz-transform:rotate(90deg);
        -webkit-transform:rotate(90deg);
        transform:rotate(90deg);
    }
    .rounded{
        border-radius: 10px 60px 60px 60px;
    }
    #goes_up {
        display: none;
        position: fixed;
        bottom: 10px;
        top:572px;
        right: 30px;
        z-index: 99;
        font-size: 28px;
        border: none;
        outline: none;
        background-color: #00c292;
        color: white;
        cursor: pointer;
        padding: 15px;
        border-radius: 30px;
        -webkit-transition: background-color 0.5s ease;
        -moz-transition: background-color 0.5s ease;
        -ms-transition: background-color 0.5s ease;
        -o-transition: background-color 0.5s ease;
        transition: background-color 0.5s ease;
    }
    #goes_up:hover {
        background-color: #555;
    }
</style>
@endsection
@section('content')
<button type="button" onclick="topFunction()" id="goes_up" title="Go to top"><div data-icon="&#xe010;" class="linea-icon linea-aerrow"></div> </button>
<div class="container-fluid" style="padding-top: 25px;">
    <div id="canvas-wrapper" style="margin-left: auto; margin-right: auto;">
        <canvas id="graph"></canvas>
    </div>
    @if(!empty($rekap_rinci))
    <div class="form-group">
        <label>Unduh File</label>
        <select class="form-control" id="dwn_file" name="dwn_file">
           <option value="all">Semua Data</option>
           <option value="w1">Minggu Pertama</option>
           <option value="w2">Minggu Kedua</option>
           <option value="w3">Minggu Ketiga</option>
           <option value="w4">Minggu Keempat</option>
        </select>
    </div>
    <a type="button" class="btn btn-default" id="excel_dwn"><span data-icon="&#xe007;" class="linea-icon linea-basic fa-fw" style="font-size: 20px; vertical-align:middle;" ></span>&nbsp;Unduh Excel</a>
    <div style="padding-top: 25px;">
        <div class="modal fade photo_rekap photo_rekap_detail modal_lg" id="photo_rekap_detail">
            <div class="modal_lg" role="document" style="width: 95%; margin: auto; margin-top: 10px;">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel" style="width: 400px;"><div id="odp_nama"></div></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" style="overflow-y:auto;padding: 0px 0px 0px 0px;">
                        <div id="Konten"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        @foreach($detail_data as $no => $data)
            @foreach($data as $tanggal => $redata)
            <div class="panel rounded panel-info" style="margin-top: 20px;">
                <div class="table-responsive" id="teknisi" style="padding-bottom: 3px;">
                    <p style="cursor: pointer;font-size: 12px;" class="label rounded label-success css_style no_{{$tanggal}}" data-number="{{ $tanggal }}" data-toggle="collapse" href="#collapse{{$tanggal}}" id="{{ $tanggal }}" role="button" aria-expanded="false" aria-controls="collapse{{$tanggal}}">{{$tanggal}}<i class="fa fa-chevron-right rotate{{ $tanggal }}"></i></p>
                    <div class="collapse multi-collapse" id="collapse{{$tanggal}}">
                        <div class="panel-body">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Witel</th>
                                        <th>Sto</th>
                                        <th>Sisa Order</th>
                                        <th>Siap Dispatch</th>
                                        <th>Sudah Dispatch</th>
                                        <th>Need Progress</th>
                                        <th>OGP</th>
                                        <th>Kendala</th>
                                        <th>Selesai Fisik</th>
                                        <th>Selesai Go-Live</th>
                                    </tr>
                                </thead>
                                @php
                                    $sum_WO_orderan_siap_dispatch = $sum_sudah_dispatch = $sum_ogp = $sum_kendala = $sum_need_progress = $sum_selesai_fisik = $sum_selesai_live = $sum_sisa_order_belumselesai = $sum_grand = 0;
                                @endphp
                                <tbody>
                                    @foreach($rekap_rinci as $Month => $data)
                                        @if($tanggal == $Month)
                                            @foreach($data as $daerah => $data1)
                                                @php $first = true @endphp
                                                @foreach($data1->data_utama as $main_data)
                                                    @php
                                                        $sum_sisa_order_belumselesai  += $main_data->sisa_order_belumselesai + $main_data->WO_orderan_siap_dispatch;
                                                        $sum_WO_orderan_siap_dispatch += $main_data->WO_orderan_siap_dispatch;
                                                        $sum_sudah_dispatch           += $main_data->sudah_dispatch;
                                                        $sum_ogp                      += $main_data->ogp;
                                                        $sum_kendala                  += $main_data->kendala;
                                                        $sum_need_progress            += $main_data->need_progress;
                                                        $sum_selesai_fisik            += $main_data->selesai_fisik;
                                                        $sum_selesai_live             += $main_data->selesai_live;
                                                    @endphp
                                                    <tr>
                                                        @if($first == true)
                                                        <td rowspan="{{ $data1->count }}" class="align-middle">{{ $main_data->datel }}</td>
                                                        @php $first = false @endphp
                                                        @endif
                                                        <td class="align-middle">{{ $main_data->sto_n }}</td>
                                                        <td class="align-middle">{{ $main_data->sisa_order_belumselesai + $main_data->WO_orderan_siap_dispatch }}</td>
                                                        <td class="align-middle">{{ $main_data->WO_orderan_siap_dispatch == 0 ? '-' : $main_data->WO_orderan_siap_dispatch }}</td>
                                                        <td class="align-middle">{{ $main_data->sudah_dispatch == 0 ? '-' : $main_data->sudah_dispatch }}</td>
                                                        <td class="align-middle">{{ $main_data->need_progress == 0 ? '-' : $main_data->need_progress }}</td>
                                                        <td class="align-middle">{{ $main_data->ogp == 0 ? '-' : $main_data->ogp }}</td>
                                                        <td class="align-middle">{{ $main_data->kendala == 0 ? '-' : $main_data->kendala }}</td>
                                                        <td class="align-middle">{{ $main_data->selesai_fisik == 0 ? '-' : $main_data->selesai_fisik }}</td>
                                                        <td class="align-middle">{{ $main_data->selesai_live == 0 ? '-' : $main_data->selesai_live }}</td>
                                                    </tr>
                                                @endforeach
                                            @endforeach
                                        @endif
                                    @endforeach
                                    <tr>
                                        <td colspan="2"></td>
                                        <td class="align-middle">{{ $sum_sisa_order_belumselesai }}</td>
                                        <td class="align-middle">{{ $sum_WO_orderan_siap_dispatch }}</td>
                                        <td class="align-middle">{{ $sum_sudah_dispatch }}</td>
                                        <td class="align-middle">{{ $sum_need_progress }}</td>
                                        <td class="align-middle">{{ $sum_ogp }}</td>
                                        <td class="align-middle">{{ $sum_kendala }}</td>
                                        <td class="align-middle">{{ $sum_selesai_fisik }}</td>
                                        <td class="align-middle">{{ $sum_selesai_live }}</td>
                                    </tr>
                                </tbody>
                            </table>
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Nama Proyek</th>
                                        <th>Sto</th>
                                        <th>Nama ODP</th>
                                        <th>Koordinat ODP</th>
                                        <th>Nama Regu</th>
                                        <th>Status</th>
                                        <th>Catatan Teknisi</th>
                                        <th>Foto</th>
                                        <th>ABD</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($redata as $number => $value)
                                    <tr>
                                        <td>{{ $value->project_name }}</td>
                                        <td>{{ $value->sto }}</td>
                                        <td>{{ $value->odp_nama }}</td>
                                        <td>{{ $value->lt_koordinat_odp }}</td>
                                        <td>{{ $value->regu_name }}</td>
                                        @php
                                        if($value->GOLIVE == 1  ){
                                            $value->lt_status = $value->lt_status."<br/> ( Go-Live)";

                                        }
                                        @endphp
                                        <td style="{{ $value->GOLIVE == 1 ? 'background:#00FF00; color:Black;' : ''  }}">{!! $value->lt_status or 'Dispatch' !!}</td>
                                        <td>{{ $value->lt_catatan }}</td>
                                        <td><a type="button" style="color: #6bacd5;" data-id="{{ $value->odp_nama }}" data-id_pt2="{{ $value->id }}" class="btn btn-light show_detail_photo"><span data-icon="]" class="linea-icon linea-basic fa-fw" style="font-size: 27px; bottom: 10px;"></span></a></td>
                                        <td>@if($value->lt_status == 'Selesai')<a type="button" style="color: #FFD800FF;" href="/Download/file/pdf/{{ $value->id }}" data-id_pt2="{{ $value->id }}" class="btn btn-light download_abd"><span data-icon="&#xe003;" class="linea-icon linea-basic fa-fw" style="font-size: 27px; bottom: 10px;"></span></a>@else Tidak Ada! @endif</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        @endforeach
    </div>
    @else
    <div class="alert alert-danger"> Tidak Ada Data Disini <i data-icon="g" class="linea-icon linea-basic fa-fw"></i></div>
    @endif
</div>
@endsection
@section('footerS')
<script src="/bower_components/moment/min/moment.min.js"></script>
<script src="/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script>
    window.onscroll = function() {scrollFunction()};

    function scrollFunction() {
        if($('body').scrollTop > 800 || document.documentElement.scrollTop > 800) {
            $("#goes_up").css({
                'display' :  "block"
            });
        }else{
            $("#goes_up").css({
                'display' :  "none"
            });
        }
    }

    function topFunction() {
        window.scrollTo({
            top: 0,
            behavior: 'smooth'
        });
        document.documentElement.scrollTo({
            top: 0,
            behavior: 'smooth'
        });
    }

    $(function(){
        var link_split = window.location.href.split('=');
        var barGraph;
        function my_chart(year, month){
            var ctx =$('#graph');
            $.ajax({
                url:"/rekap/ajax/matrix/Y="+year+"&M="+month+"/matrix/search",
                type:"GET",
                success: function(data) {
                    var dynamicColors = function() {
                        var r = Math.floor(Math.random() * 255);
                        var g = Math.floor(Math.random() * 255);
                        var b = Math.floor(Math.random() * 255);
                        return "rgb(" + r + "," + g + "," + b + ", 0.2)";
                    };
                    var label = [],
                    count = [],
                    color_my = [];
                    $.each( data, function( key, val ) {
                        $.each( val, function( key_val, data_val ) {
                            label.push(key_val);
                            count.push(data_val.Total);
                            color_my.push(dynamicColors());
                        });
                    });
                    console.log(label);
                    console.log(count);
                    var chartdata = {
                        labels: label.reverse(),
                        datasets : [
                        {
                            label: 'Rekap ODP Naik Bulan '+year,
                            fill: false,
                            lineTension: 0.1,
                            data: count.reverse(),
                            backgroundColor: color_my,
                            borderColor: color_my ,
                            borderWidth: 1
                        }
                        ]
                    };
                    barGraph = Chart.Bar(ctx, {
                        data: chartdata,
                        options: {
                            legend: {
                                display: false
                            },
                            tooltips: {
                                mode: 'index',
                                intersect: false,
                                callbacks: {
                                    label: function(tooltipItem) {
                                        return tooltipItem.yLabel;
                                    }
                                }
                            },
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        fontColor: "black",
                                        beginAtZero: true
                                    }
                                }],
                                xAxes: [{
                                    ticks: {
                                        fontColor: "black",
                                        beginAtZero: true
                                    }
                                }]
                            }
                        }
                    });
                    var canvas = document.getElementById('graph');
                    canvas.onclick = function(evt) {
                        var activePoint = barGraph.getElementAtEvent(evt)[0];
                        var data = activePoint._chart.config.data;
                        var datasetIndex = activePoint._datasetIndex;
                        var label = data.labels[activePoint._index];
                        var value = data.datasets[datasetIndex].data[activePoint._index];
                        console.log(label, value, activePoint._index+1);
                        document.getElementById(label).scrollIntoView({
                            behavior: "smooth",
                            block: "start",
                            inline: "nearest"
                        });
                        $(".no_"+label).attr('aria-expanded', true);
                        $("#collapse"+label).attr('aria-expanded', true);
                        $("#collapse"+label).toggleClass('show');
                        $(".rotate"+label).toggleClass('down');
                    };
                },
                error: function(data) {
                    /*console.log(data);*/
                }
            });
        }
        my_chart(link_split[1].substr(0, 4),link_split[3]);
        var my_data = {!! json_encode($detail_data) !!},
        lala = [];

        if(my_data != null){
            $.each(my_data, function( index, value ) {
                $.each(value, function( obj, mine_Value ) {
                    lala.push(obj);
                });
            });

            $(".no_"+lala[0]).attr('aria-expanded', true);
            $("#collapse"+lala[0]).attr('aria-expanded', true);
            $("#collapse"+lala[0]).toggleClass('show');
            $(".rotate"+lala[0]).toggleClass('down');
        }

        $(".show_detail_photo").click(function(){
            var value = $(this).attr('data-id');
            var id_pt2 = $(this).attr('data-id_pt2');
            var res = value.toUpperCase();
            $.ajax({
                type: 'GET',
                url : "/matrix/photo/"+id_pt2,
                beforeSend: function() {
                    /*console.log('dang lah');*/
                    $("#parent").toggleClass('preloader');
                    $("#child").toggleClass('cssload-speeding-wheel');
                    $("#parent").css({
                        'background-color' : 'rgba(0,0,0,.8)'
                    });
                },
                success: function(data) {
                    $("#parent").toggleClass('preloader');
                    $("#child").toggleClass('cssload-speeding-wheel');
                    $("#parent").css({
                        'background-color' : ''
                    });
                    /*console.log(data);*/
                    $('#photo_rekap_detail').modal('show');
                    $('#odp_nama').text('Detail Foto ODP '+res);
                    $('#Konten').html(data);
                },
                error: function(e){
                    /*console.log(e);*/
                }
            });
        });

        // $(".css_style").click(function () {
        //     var data = $(this).attr('data-number');
        //     /*console.log(data);*/
        //     $(".rotate"+data).toggleClass("down");
        //     $(".rotate"+data).css({
        //         "-moz-transition":"all .5s linear",
        //         "-webkit-transition":"all .5s linear",
        //         'transition': 'all .5s linear'
        //     });
        // });

        $("#dwn_file").on('change', function(){
            if($(this).val() == 'all'){
                $("#excel_dwn").attr('href', '/rekap/download/Y=' + link_split[1].substr(0, 4)+'&J='+ link_split[2].substring(0, link_split[2].length - 2) +'&M='+link_split[3])
            }else{
                $("#excel_dwn").attr('href', '/rekap/download/Y=' + link_split[1].substr(0, 4)+'&J='+ $(this).val() +'&M='+link_split[3])

            }
        })

        $("#dwn_file").val('all').change()
    });
</script>
@endsection