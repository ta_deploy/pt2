@extends('layout')
@section("title", empty($edit) ? "Tambah Material" : "Edit Material")
@section('content')
<div class="form-group">
	<a type="button" class="btn btn-default" href="{{URL::to('/regu/list')}}"><i data-icon="&#xe00f;" class="linea-icon linea-aerrow fa-fw" style="font-size: 20px; vertical-align:middle;"></i>&nbsp;Kembali</a>
</div>
<div class="container-fluid" style="padding-top: 25px;">
	<form method="post" id="regu_form">
		<div class="panel panel-info">
			<div class="panel-heading"><?= empty($edit) ? "Tambah Material" : "Edit Material"?></div>
			<div class="panel-body">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<div class="form-group">
					<label>Nama Material</label>
					<div class="alert id_item alert-danger" role="alert" style="display: none;">
						<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
						<span class="sr-only">Error:</span>
						Material Harus Diisi!
					</div>
					<input type="text" class="form-control" id="id_item" name="id_item" placeholder="Masukkan Nama Material" value="{{$edit->id_item or ''}}">
				</div>
				<div class="form-group">
					<label>Deskrispi</label>
					<div class="alert uraian alert-danger" role="alert" style="display: none;">
						<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
						<span class="sr-only">Error:</span>
						Deskripsi Harus Diisi!
					</div>
					<input type="text" class="form-control" id="uraian" name="uraian" placeholder="Masukkan Deskripsi" value="{{$edit->uraian or ''}}">
				</div>
				<div class="form-group">
					<label>Satuan</label>
					<div class="alert satuan alert-danger" role="alert" style="display: none;">
						<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
						<span class="sr-only">Error:</span>
						Satuan Harus Diisi!
					</div>
					<input type="text" class="form-control" id="satuan" name="satuan" placeholder="Masukkan Satuan Material" value="{{$edit->satuan or ''}}">
				</div>
				<div class="form-group">
					<label>Harga Material Telkom</label>
					<input type="text" class="form-control harga" id="material_telkom" name="material_telkom" value="{{ $edit->material_telkom != 0 ? $edit->material_telkom : '' }}" placeholder="0">
				</div>
				<div class="form-group">
					<label>Harga Jasa Telkom</label>
					<input type="text" class="form-control harga" id="jasa_telkom" name="jasa_telkom" value="{{ $edit->jasa_telkom != 0 ? $edit->jasa_telkom : '' }}" placeholder="0">
				</div>
				<div class="form-group">
					<label>Harga Material Mitra</label>
					<input type="text" class="form-control harga" id="material_ta" name="material_ta" value="{{ $edit->material_ta != 0 ? $edit->material_ta : '' }}" placeholder="0">
				</div>
				<div class="form-group">
					<label>Harga Jasa Mitra</label>
					<input type="text" class="form-control harga" id="jasa_ta" name="jasa_ta" value="{{ $edit->jasa_ta != 0 ? $edit->jasa_ta : '' }}" placeholder="0">
				</div>
				<div class="form-group">
					<label>Asal Barang</label>
					<select class="form-control" name="asal">
						<option value="1">Telkom</option>
						<option value="0">Telkom Akses</option>
					</select>
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-block btn-info save_me" style="text-align: center;"><span data-icon="M" class="linea-icon linea-basic fa-fw" style="font-size: 20px; color: #ffffff; "></span>&nbsp;Simpan Perubahan</button>
				</div>
			</div>
		</div>
	</form>
</div>
@endsection
@section('footerS')
<script>
	$(function(){
		$(".harga").on("keypress keyup",function (e){
			$(this).val($(this).val().replace(/\D/g, ''));
			var charCode = (e.which) ? e.which : e.keyCode;
			if (charCode > 31 && (charCode < 48 || charCode > 57)) {
				return false;
			}
		});

		$('.save_me').click(function(e){
			e.preventDefault();
			var id_item = $.trim($('#id_item').val()),
			uraian = $.trim($('#uraian').val()),
			satuan = $.trim($('#satuan').val()),
			datastring = $("#regu_form").serialize();

			if (id_item === '') {
				$('.id_item').css({display: "block"});
				$('#id_item').css({border: "2px solid red"});
				$('#id_item').focus();
			}else{
				$('.id_item').css({display: "none"});
				$('#id_item').css({border: ""});
			}

			if (uraian === '') {
				$('.uraian').css({display: "block"});
				$('#uraian').css({border: "2px solid red"});
				$('#uraian').focus();
			}else{
				$('.uraian').css({display: "none"});
				$('#uraian').css({border: ""});
			}

			if (satuan === '') {
				$('.satuan').css({display: "block"});
				$('#satuan').css({border: "2px solid red"});
				$('#satuan').focus();
			}else{
				$('.satuan').css({display: "none"});
				$('#satuan').css({border: ""});
			}

			if (id_item === '' || uraian === '' || satuan === ''){
				return false;
			}else{
				var notif=$.toast({
					position: 'mid-center',
					showHideTransition: 'plain',
					hideAfter: false
				});
				if (url_nav_tab.match('/')) {
					var id = url_nav_tab.split('/')[5];
				}
				$.ajax({
					type: 'POST',
					url : '/material/revenue_setting/'+id,
					beforeSend: function(){
						notif.update({
							heading: 'Pemberitahuan',
							text: 'Sedang Disimpan!',
							position: 'mid-center',
							icon: 'info',
							showHideTransition: 'plain',
							stack: false
						});
					},
					data : datastring,
					success: function(data) {
						notif.update({
							heading: 'Pemberitahuan',
							text: 'Material '+id_item+' Telah Berhasil Disimpan!',
							position: 'mid-center',
							icon: 'success',
							stack: false
						})
						window.location = "/regu/list";
						/*console.log(data);*/
					},
					error: function(e){
						var exception = ( typeof object !== "undefined" && e.responseJSON.exception ) ? e.responseJSON.exception : '';
						var file = ( typeof object !== "undefined" && e.responseJSON.file ) ? e.responseJSON.file : '';
						var line = ( typeof object !== "undefined" && e.responseJSON.line ) ? e.responseJSON.line : '';
						var message = ( typeof object !== "undefined" && e.responseJSON.message ) ? e.responseJSON.message : '';
						var status_error = e.status ? e.status : '' ;
						var link = window.location.href ;
						/*console.log(e);*/
						$.ajax({
							type: 'POST',
							url: "/send/err",
							data: {
								"_token": "{{ csrf_token() }}",
								"exception" : exception,
								"file" : file,
								"line" : line,
								"message" : message,
								"status_error" : status_error,
								"link" : link
							},
							success: function (data) {
								/*console.log(data);*/
							},
							error: function(e){
								/*console.log(e);*/
							}
						});
						notif.update({
							heading: 'Error',
							text: 'Data Kesalahan Telah Dikirim. Untuk Lebih Lanjut, Klik link <a href="https://t.me/RendyL">Ini</a>',
							showHideTransition: 'fade',
							icon: 'error',
							showHideTransition: 'plain',
							allowToastClose: false
						});
					}
				});
			}
		});
	});
</script>
@endsection