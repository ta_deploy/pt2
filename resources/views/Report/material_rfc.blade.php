@extends('layout')
@section('title', 'Rekap Material RFC')
@section('headerS')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link href="/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
<link rel="stylesheet" href="/bower_components/jPages-master/css/animate.css">
<link rel="stylesheet" href="/bower_components/jPages-master/css/jPages.css">
@endsection
@section('style')
<style type="text/css">
	.modal_ket_material{
		overflow: auto;
		overflow-y: auto;
		top:3%;
	}

	th, td{
		text-align: center;
		white-space:nowrap;
	}
	div > table {
		float: left
	}

	.holder {
		display: inline-block;
	}

	.holder a, .holder span {
		color: black;
		float: left;
		font-size: 13px;
		padding: 8px 16px;
		text-decoration: none;
		transition: background-color .3s;
	}

	.holder a.jp-current {
		background-color: #586069;
		color: white;
		border-radius: 5px;
	}

	.holder a {
		background-color: #4a5056;
		color: white;
		border-radius: 5px;
	}

	.holder a:hover {
		background-color: #4a5056;
		border-radius: 5px;
	}

	.holder a:first-child {
		border-top-left-radius: 5px;
		border-bottom-left-radius: 5px;
	}

	.holder a:last-child {
		border-top-right-radius: 5px;
		border-bottom-right-radius: 5px;
	}
	.modal_detail_layout{
		overflow: auto;
		overflow-y: auto;
	}
	
</style>
@endsection
@section('content')
<div class="modal fade modal_ket_material ket_material modal_lg" id="ket_material">
	<div class="modal_lg" role="document" style="width: 95%; margin: auto;">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel" style="width: 400px;"><div id="nama_rfc"></div></h5>
			</div>
			<div class="modal-body" style="overflow-y:auto;padding: 0px 0px 0px 0px;">
				<table class="table table-striped table-bordered toggle-circle">
					<thead>
						<tr>
							<th>RFC</th>
							<th>Pemakai</th>
							<th>ID Item</th>
							<th>Item</th>
							<th>Keluar  Gudang</th>
							<th>Terpakai</th>
							<th>Kembali</th>
						</tr>
					</thead>
					<tbody id='testis'>
					</tbody>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid" style="padding-top: 25px;">
	<div style="padding-top: 25px;">
   <div class="panel panel-warning">
    <div class="panel-heading header-date">Matrix Periode {{ Request::segment(2) }}</div>
    <div class="panel-body">
        <div class="table-responsive">
          <div class="form-group">
           <input type="text" class="form-control" id="cari_rfc" placeholder="Masukkan Nomor RFC" style="width: 100%;">
         </div>
         <table id="groupPT2" class="table table-striped table-bordered toggle-circle">
           <thead>
            <tr>
             <th data-toggle="true">Nomor RFC</th>
             <th>Tim</th>
             <th>Nik 1</th>
             <th>Nik 2</th>
             <th>Terpakai</th>
             <th>Keluar Gudang</th>
             <th>Kembali</th>
           </tr>
         </thead>
         <tbody id="data_table">
          @foreach($data as $no => $order)
          <tr>
           <td><a type="button" class="btn btn-info btn-sm rfc_detail_lapo" href="" style="color:white; border-radius: 12px;" data-rfc= "{{ $order->rfc_n }}"> {{ $order->rfc_n }}</a></td>
           <td>{{ $order->regu_name }}</td>
           <td>{{ $order->nik1 or '-' }}</td>
           <td>{{ $order->nik2 or '-' }}</td>
           <td>{{ $order->terpakai != 0 ? $order->terpakai : '-'  }}</td>
           <td>{{ $order->keluar_gudang != 0 ? $order->keluar_gudang : '-'  }}</td>
           <td>{{ $order->kembali != 0 ? $order->kembali : '-'  }}</td>
         </tr>
         @endforeach
       </tbody>
     </table>
     <div style="text-align:center">
       <div class="holder"></div>
     </div>
   </div>
 </div>
</div>
</div>
</div>
@endsection
@section('footerS')
<script src="/bower_components/moment/min/moment.min.js"></script>
<script src="/bower_components/jPages-master/js/jPages.js"></script>
<script>

	$(function(){
		$('.rfc_detail_lapo').on('click', function(e){
			e.preventDefault();
			value = $(this).data('rfc');
			$.ajax({
				type: 'GET',
				data: {data : value},
				url : "/rfc/search/please/help",
			}).done(function(data) {
				console.log(data);
				$('#ket_material').modal('show');
				$('#nama_rfc').text('Tabel RFC '+data[0].rfc);
				var value_isi;
				$.each( data, function( key, value ) {
					console.log(value.id_item);
					value_isi += '<tr><td>'+value.rfc+'</td><td>'+value.created_by+'</td><td>'+value.id_item+'</td><td>'+value.nama_barang+'</td><td>'+value.keluar_gudang+'</td><td>'+value.terpakai+'</td><td>'+value.kembali+'</td></tr>'
				});
				$('#testis').html(value_isi);
			}).fail(function(data){
				console.log(data);
			});
		});

		function paging(){
			$("div.holder").jPages({
				containerID : "data_table",
				previous : "←",
				next : "→",
				perPage : 20,
				delay : 20
			});
		}

		paging();

		$("#cari_rfc").keyup(function(){
			var input, filter, table, tr, td1, i, td1_val;
			input = document.getElementById("cari_rfc");
			filter = input.value.toUpperCase();
			table = document.getElementById("groupPT2");
			tr = table.getElementsByTagName("tr");
			for (i = 0; i < tr.length; i++) {
				td1 = tr[i].getElementsByTagName("td")[0];
				if (td1) {
					td1_val = td1.textContent || td1.innerText;
					if (td1_val.toUpperCase().indexOf(filter) > -1) {
						tr[i].style.display = "";
					} else{
						tr[i].style.display = "none";
					}
				}
			}
			paging();

		});
		
		$('.kalender').click(function(e){
			e.preventDefault();
			$('input[name="rangedate"]').click();
		});
	});
</script>
@endsection