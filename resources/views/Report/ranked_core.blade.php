@extends('layout')
@section('title', 'List Core')
@section('style')
<style type="text/css">
	th, td{
		text-align: center;
		white-space:nowrap;
	}
</style>
@endsection
@section('content')
<div class="container-fluid" style="padding-top: 25px;">
	<div class="panel panel-warning">
		<div class="panel-heading">List Data Validasi Go-Live</div>
		<div class="panel-body">
				<div class="table-responsive">
					<table id="teknisi" class="table table-bordered">
						<thead>
							<tr>
								<th>No</th>
								<th>Nama</th>
								<th>Material</th>
								<th>Jumlah Core Terpasang</th>
								<th>Total</th>
							</tr>
						</thead>
						<tbody>
							@php
								$result = 1;
							@endphp
							@foreach($fd as $k => $v)
								@php
									$first = TRUE;
								@endphp
								@foreach($v['material'] as $vv)
									<tr>
										@if($first == TRUE)
											<td rowspan="{{ count($v['material']) }}" style='vertical-align: middle;'>{{ $result++ }}</td>
											<td rowspan="{{ count($v['material']) }}" style='vertical-align: middle;'>{{ $v['nama'] }} ({{ $v['nik'] }})</td>
										@endif
										<td class="align-middle">{{ $vv['id_item'] }}</td>
										<td class="align-middle">{{ $vv['qty'] }}</td>
										@if($first == TRUE)
											<td rowspan="{{ count($v['material']) }}" style='vertical-align: middle;'>{{ $v['qty_total'] }}</td>
											@php
												$first = FALSE;
											@endphp
										@endif
									</tr>
								@endforeach
							<tr>
							@endforeach
						</tbody>
					</table>
				</div>
		</div>
	</div>
</div>
@endsection
