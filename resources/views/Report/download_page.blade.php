@extends('layout')
@section('title', 'Download Data')
@section('headerS')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>
<link href="/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet"/>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
@endsection
@section('style')
<style type="text/css">
  .select2-results {
    /* background-color: #353c48; */
  }
</style>
@endsection
@section('content')
<div class="container-fluid" style="padding-top: 25px;">
  <form id="formlistG" name="formlistG" method="post">
    {{ csrf_field() }}
    <div class="panel panel-warning">
      <div class="panel-heading header-date">Download Data</div>
      <div class="panel-body">
        <div class="form-group col-md-12">
          <div class='input-group date'>
            <input type='text' class="form-control" name='rangedate' value="{{ date("m/d/Y", strtotime("first day of this month")) }} - {{ date("m/d/Y", strtotime("last day of this month")) }}"/>
            <span class="input-group-addon">
              <span class="glyphicon glyphicon-calendar kalender" style="cursor: pointer;"></span>
            </span>
            <input type="hidden" name="date_v" value="{{ date("Y-m-d", strtotime("first day of this month")) }} - {{ date("Y-m-d H:i:s", strtotime("last day of this month")) }}"/>
          </div>
        </div>
        <div class="form-group col-md-12">
          <select class="form-control select_data" name="status">
            <option value="umur">Umur Saldo Yang Belum Dikerjakan Teknisi</option>
            <option value="sync_psb">Semua PT-2 Dengan PSB</option>
            <option value="matrix">Data Matrix</option>
            <option value="estimasi_kendala">Estimasi Kendala</option>
          </select>
        </div>
      </div>
      <button type="submit" class="btn btn-block btn-primary save_me" style="text-align: center;">
        <span data-icon="O" class="linea-icon linea-basic fa-fw" style="font-size: 20px; color: #ffffff; "></span>
        &nbsp;Cari
      </button>
    </div>
  </form>
</div>
@endsection
@section('footerS')
<script src="/bower_components/moment/min/moment.min.js"></script>
<script src="/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script>
  $(function(){

    $('.select_data').select2();

    $('input[name="rangedate"]').daterangepicker({
      opens: 'left'
    }, function(start, end){
      var month1 = start.format('YYYY-MM-DD'),
      month2 = end.format('YYYY-MM-DD')+" 23:59:59";
      $('input[name="date_v"]').val(month1+' - '+month2);
    });

    $('.kalender').click(function(e){
      e.preventDefault();
      $('input[name="rangedate"]').click();
    });
  });

</script>
@endsection
