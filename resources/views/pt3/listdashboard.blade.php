@extends('layout')
@section('title', 'PT3')
@section('headerS')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link href="/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
@endsection
@section('style')
<style type="text/css">
th, td{
	text-align: center;
	white-space:nowrap;
}
div>table {
	float: left
}
</style>
@endsection
@section('content')
<div class="container-fluid" style="padding-top: 25px;">
	<div class="panel panel-warning">
		<div class="panel-heading header-date">Dashboard PT3</div>
		<div class="panel-body">
			<div class="table-responsive">
				<table id="teknisi" class="table table-striped">
					<thead>
						<tr>
							<th>#</th>
							<th>Nama Lop</th>
							<th>PIC</th>
							<th>Waspang</th>
						</tr>
					</thead>
					<tbody>
						@foreach($list as $no => $l)
						<tr>
							<td>{{ ++$no }}</td>
							<td>{{ $l->nama_lop }}</td>
							<td>{{ $l->pid }}</td>
							<td>{{ $l->waspang }}</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection
@section('footerS')
<script src="/bower_components/moment/min/moment.min.js"></script>
<script src="/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="/bower_components/Chart.js/Chart.min.js"></script>
<script>
	$(function(){
	});
</script>
@endsection
