@extends('layout')
@section('title', 'PT3')
@section('headerS')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link class="btn btn-sm btn-info btn-circle" style="color: white; border-radius: 25px;" href="/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
@endsection
@section('style')
<style type="text/css">
th, td{
	text-align: center;
	white-space:nowrap;
}
div > table {
	float: left
}
</style>
@endsection
@section('content')
<div class="container-fluid" style="padding-top: 25px;">
	<div class="panel panel-warning">
		<div class="panel-heading header-date">Dashboard PT3</div>
		<div class="panel-body">
			<div class="table-responsive">
				<table id="teknisi" class="table table-striped">
					<thead>
						<tr>
							<th>#</th>
							<th>STO</th>
							<th>Pengiriman Material</th>
							<th>Pragelaran</th>
							<th>Tanam Tiang</th>
							<th>Gelar Kabel</th>
							<th>Pasang Terminal</th>
							<th>Terminasi</th>
							<th>TestComm</th>
							<th>Total</th>
						</tr>
					</thead>
					<tbody>
						@foreach($data as $no => $d)
						<tr>
							<td>{{ ++$no }}</td>
							<td>{{ $d->STO }}</td>
							<td>
								@if($d->Pengiriman_Material != 0)
									<a class="btn btn-sm btn-info btn-circle" style="color: white; border-radius: 25px;" href="/pt3/list/{{$d->STO}}/Pengiriman_Material">{{ $d->Pengiriman_Material  }}</a>
								@else
								-
								@endif
							</td>
							<td>
								@if($d->Pragelaran != 0)
									<a class="btn btn-sm btn-info btn-circle" style="color: white; border-radius: 25px;" href="/pt3/list/{{$d->STO}}/Pragelaran">{{ $d->Pragelaran  }}</a>
								@else
								-
								@endif
							</td>
							<td>
								@if($d->Tanam_Tiang != 0)
									<a class="btn btn-sm btn-info btn-circle" style="color: white; border-radius: 25px;" href="/pt3/list/{{$d->STO}}/Tanam_Tiang">{{ $d->Tanam_Tiang  }}</a>
								@else
								-
								@endif
							</td>
							<td>
								@if($d->Gelar_Kabel != 0)
									<a class="btn btn-sm btn-info btn-circle" style="color: white; border-radius: 25px;" href="/pt3/list/{{$d->STO}}/Gelar_Kabel">{{ $d->Gelar_Kabel  }}</a>
								@else
								-
								@endif
							</td>
							<td>
								@if($d->Pasang_Terminal != 0)
									<a class="btn btn-sm btn-info btn-circle" style="color: white; border-radius: 25px;" href="/pt3/list/{{$d->STO}}/Pasang_Terminal">{{ $d->Pasang_Terminal  }}</a>
								@else
								-
								@endif
							</td>
							<td>
								@if($d->Terminasi != 0)
									<a class="btn btn-sm btn-info btn-circle" style="color: white; border-radius: 25px;" href="/pt3/list/{{$d->STO}}/Terminasi">{{ $d->Terminasi  }}</a>
								@else
								-
								@endif
							</td>
							<td>
								@if($d->TestComm != 0)
									<a class="btn btn-sm btn-info btn-circle" style="color: white; border-radius: 25px;" href="/pt3/list/{{$d->STO}}/TestComm">{{ $d->TestComm  }}</a>
								@else
								-
								@endif
							</td>
							<td>
								@if($d->Total != 0)
									<a class="btn btn-sm btn-info btn-circle" style="color: white; border-radius: 25px;" href="/pt3/list/{{$d->STO}}/0">{{ $d->Total  }}</a>
								@else
								-
								@endif
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection
@section('footerS')
<script src="/bower_components/moment/min/moment.min.js"></script>
<script src="/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="/bower_components/Chart.js/Chart.min.js"></script>
<script>
	$(function(){
	});
</script>
@endsection
