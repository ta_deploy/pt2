@extends('layout')
@section('title', 'Dashboard Hero')
@section('headerS')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
@endsection
@section('style')
<style type="text/css">
th, td{
	text-align: center;
	white-space:nowrap;
}
div > table {
	float: left
}
</style>
@endsection
@section('content')
<div class="container-fluid" style="padding-top: 25px;">
	<div class="panel panel-warning">
		<div class="panel-heading header-date">Dashboard Pekerjaan HERO</div>
		<div class="panel-body">
			<div class="table-responsive">
				<table id="teknisi" class="table table-striped">
					<thead>
						<tr>
							<th rowspan="2">#</th>
							<th rowspan="2">Datel</th>
							<th rowspan="2">STO</th>
							<th rowspan="2">Jumlah Order</th>
							<th rowspan="2">Sisa</th>
							<th colspan="3">Progress</th>
						</tr>
            <tr>
              <th>PT-2</th>
              <th>PT-2 PLUS</th>
              <th>Kendala</th>
            </tr>
					</thead>
					<tbody>
            @php
              $no = 0;
            @endphp
            @foreach($fd as $v)
              @php
                $first = true;
              @endphp
              @foreach($v['list'] as $kk => $vv)
                <tr>
                  @if($first == true)
                    <td style="vertical-align: middle;" rowspan="{{ count($v['list']) }}">{{ ++$no }}</td>
                    <td style="vertical-align: middle;" rowspan="{{ count($v['list']) }}"> {{ $v['datel'] }} </td>
                    @php
                      $first = false;
                    @endphp
                  @endif
                  <td>{{ $kk }}</td>
                  <td style='text-align: center;'>
                    @if ($vv['all'] != 0)
                        <a class="btn btn-sm btn-info direct_link" style="color: white; border-radius: 25px;" href="/hero/table_HERO/{{ Request::segment(3) }}?datel={{ $v['datel'] }}&sto[]={{ $kk }}">{{ $vv['all'] }}</a>
                    @else
                        -
                    @endif
                  </td>
                  <td style='text-align: center;'>
                    @if ($vv['none'] != 0)
                        <a class="btn btn-sm btn-info direct_link" style="color: white; border-radius: 25px;" href="/hero/table_HERO/{{ Request::segment(3) }}?datel={{ $v['datel'] }}&sto[]={{ $kk }}">{{ $vv['none'] }}</a>
                    @else
                        -
                    @endif
                  </td>
                  <td style='text-align: center;'>
                    @if ($vv['pt2'] != 0)
                        <a class="btn btn-sm btn-info direct_link" style="color: white; border-radius: 25px;" href="/hero/table_HERO/{{ Request::segment(3) }}?datel={{ $v['datel'] }}&sto[]={{ $kk }}">{{ $vv['pt2'] }}</a>
                    @else
                        -
                    @endif
                  </td>
                  <td style='text-align: center;'>
                    @if ($vv['pt2_plus'] != 0)
                        <a class="btn btn-sm btn-info direct_link" style="color: white; border-radius: 25px;" href="/hero/table_HERO/{{ Request::segment(3) }}?datel={{ $v['datel'] }}&sto[]={{ $kk }}">{{ $vv['pt2_plus'] }}</a>
                    @else
                        -
                    @endif
                  </td>
                  <td style='text-align: center;'>
                    @if ($vv['kendala'] != 0)
                        <a class="btn btn-sm btn-info direct_link" style="color: white; border-radius: 25px;" href="/hero/table_HERO/{{ Request::segment(3) }}?datel={{ $v['datel'] }}&sto[]={{ $kk }}">{{ $vv['kendala'] }}</a>
                    @else
                        -
                    @endif
                  </td>
                </tr>
              @endforeach
            @endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection
@section('footerS')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script>
	$(function(){
    $('.flagging_odp_hero').select2({
      width: '100%',
      placeholder: 'Pilih Flagging',
    });

    $.each($('.flagging_odp_hero'), function(k, v){
      var flg = $(this).data('flag');
      console.log(flg)
      if(flg == '-'){
        $(this).val(null).change();
      }
    })

    $('.flagging_odp_hero').on('change', function(){
      var isi = $(this).val(),
      hero = $(this).data('id_hero'),
      notif = $.toast({
        position: 'mid-center',
        showHideTransition: 'plain',
        hideAfter: false
      });

      $.ajax({
        type: 'POST',
        url : "/hero/save_flagging",
        data : {
          id_hero: hero,
          isi: isi,
          _token: "{{ csrf_token() }}"
        },
      })
      .done(function(){
        notif.update({
          heading: 'Pemberitahuan',
          text: 'Flagging Hero Berhasil Diubah!!',
          position: 'mid-center',
          icon: 'success',
          stack: false
        });
      });

      if(isi == 'Kendala'){
        $(this).parent().next().find('.create_wo').css({
          display: 'none'
        });
      }else{
        $(this).parent().next().find('.create_wo').css({
          display: 'block'
        });
      }
    });
	});
</script>
@endsection
