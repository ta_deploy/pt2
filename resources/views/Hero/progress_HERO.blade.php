@extends('layout')
@section('title', 'List Progress HERO')
@section('headerS')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link href="/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
@endsection
@section('style')
<style type="text/css">
    th {
        white-space: nowrap;
    }

    div>table {
      float: left;
    }

    .panel .panel-action a{
      opacity: 1;
    }
</style>
@endsection
@section('content')
<div class="modal fade modal_detail_layout detail_layout modal_lg" id="detail_layout">
    <div class="modal_lg" role="document" style="width: 95%; margin: auto;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel" style="width: 400px;"><div id="nama_project"></div></h5>
            </div>
            <div class="modal-body" style="overflow-y:auto;padding: 0px 0px 0px 0px;">
                <div id="content"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid" style="padding-top: 25px;">
  @include('Partial.alerts')
  @if (Session::has('alerts_tele'))
    @foreach(Session::get('alerts_tele') as $alert)
      <div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
    @endforeach
  @endif
  <div class="row">
    @foreach ($fd as $key => $val)
      <div class="col-md-12">
          <div class="panel panel-info">
          <div class="panel-heading header-date">Order <u>{{ str_replace('_', ' ', $key) }}</u>
            <div class="panel-action">
              <a href="#" data-perform="panel-collapse"><i style="font-size: 20px;" class="ti-minus"></i></a>
            </div>
          </div>
          <div class="panel-body">
            <div class="table-responsive">
              <table class="table color-bordered-table success-bordered-table">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Datel</th>
                    <th>Tanpa Regu</th>
                    <th>Sisa Order</th>
                    <th>Need Progress</th>
                    <th>OGP</th>
                    <th>Pending</th>
                    <th>Kendala</th>
                    <th>Selesai Fisik</th>
                  </tr>
                </thead>
                @php
                  $sumSisa_Order = $sumNo_Regu = $sumBerangkat = $sumOgp = $sumPending = $sumKendala = $sumSelesai = 0;
                @endphp
                <tbody>
                  @foreach($val as $no => $data)
                  @php
                    $sumSisa_Order += $data->no_regu;
                    $sumNo_Regu    += $data->no_work;
                    $sumBerangkat  += $data->berangkat;
                    $sumOgp        += $data->ogp;
                    $sumPending    += $data->pending;
                    $sumKendala    += $data->kendala;
                    $sumSelesai    += $data->selesai;
                  @endphp
                  <tr>
                    <td>{{ ++$no }}</td>
                    <td>{{ $data->datel }}</td>
                    <td style="text-align: center;">
                      @if($data->no_regu != 0)
                        {{ $data->no_regu }}
                      @else
                        -
                      @endif
                    </td>
                    <td style="text-align: center;">
                      @if($data->no_work != 0)
                        {{ $data->no_work }}
                      @else
                        -
                      @endif
                    </td>
                    <td style="text-align: center;">
                      @if($data->berangkat != 0)
                        {{ $data->berangkat }}
                      @else
                        -
                      @endif
                    </td>
                    <td style="text-align: center;">
                      @if($data->ogp != 0)
                        {{ $data->ogp }}
                      @else
                        -
                      @endif
                    </td>
                    <td style="text-align: center;">
                      @if($data->kendala != 0)
                        {{ $data->kendala }}
                      @else
                        -
                      @endif
                    </td>
                    <td style="text-align: center;">
                      @if($data->pending != 0)
                        {{ $data->pending }}
                      @else
                        -
                      @endif
                    </td>
                    <td style="text-align: center;">
                      @if($data->selesai != 0)
                        {{ $data->selesai }}
                      @else
                        -
                      @endif
                    </td>
                  </tr>
                  @endforeach
                </tbody>
                <tfoot>
                  <tr>
                    <td colspan="2">Semua Datel</td>
                    <td style="text-align: center;">
                      @if ($sumSisa_Order != 0)
                        {{ $sumSisa_Order }}
                      @else
                        -
                      @endif
                    </td>
                    <td style="text-align: center;">
                      @if ($sumNo_Regu != 0)
                        {{ $sumNo_Regu }}
                      @else
                        -
                      @endif
                    </td>
                    <td style="text-align: center;">
                      @if ($sumBerangkat != 0)
                        {{ $sumBerangkat }}
                      @else
                        -
                      @endif
                    </td>
                    <td style="text-align: center;">
                      @if ($sumOgp != 0)
                        {{ $sumOgp }}
                      @else
                        -
                      @endif
                    </td>
                    <td style="text-align: center;">
                      @if ($sumPending != 0)
                        {{ $sumPending }}
                      @else
                        -
                      @endif
                    </td>
                    <td style="text-align: center;">
                      @if ($sumKendala != 0)
                        {{ $sumKendala }}
                      @else
                        -
                      @endif
                    </td>
                    <td style="text-align: center;">
                      @if ($sumSelesai != 0)
                        {{ $sumSelesai }}
                      @else
                        -
                      @endif
                    </td>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
      </div>
    @endforeach
  </div>
</div>
@endsection
@section('footerS')
<script src="/bower_components/waypoints/lib/jquery.waypoints.js"></script>
<script src="/bower_components/counterup/jquery.counterup.min.js"></script>
<script src="/bower_components/moment/min/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.5.0-beta4/html2canvas.js"></script>
<script src="/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@3.9.1/dist/chart.min.js"></script>
<script type="text/javascript">
  $(function(){
    $('input[name="rangedate"]').daterangepicker({
      opens: 'left',
      locale: {
        format: 'YYYY-MM-DD'
      }
    }, function(start, end){
      var month1 = start.format('YYYY-MM-DD'),
      month2 = end.format('YYYY-MM-DD')+" 23:59:59";
      $('input[name="tgl_a"]').val(month1);
      $('input[name="tgl_f"]').val(month2);
    });

    $('.kalender').click(function(e){
      e.preventDefault();
      $('input[name="rangedate"]').click();
    });

    $('.detail_modal').on('click', function(){
      var href = $(this).attr('data-href');
      $.ajax({
        type: 'GET',
        url : href,
        success: function(data) {
          $('#detail_layout').modal('show');
          $('#nama_project').text('Detail Data');
          $('#content').html(data);
        },
        error: function(e){
        }
      });
    })
  });
</script>
@endsection