@extends('layout')
@section('title', 'Table Hero')
@section('headerS')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
<link href="/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
@endsection
@section('style')
<style type="text/css">
div > table {
	float: left
}

</style>
@endsection
@section('content')
<div class="container-fluid" style="padding-top: 25px;">
  <div class="panel panel-warning">
    <div class="panel-body">
        <form id="formlistG" name="formlistG" method="get">
            <div class="row">
                <div class="form-group col-md-3">
                  <select id="datel" name="datel" class="form-control find_datel">
                  </select>
                </div>
                <div class="form-group col-md-3">
                  <select id="sto" name="sto[]" class="form-control find_sto" multiple="">
                  </select>
                </div>
                <div class="form-group col-md-3">
                  <select id="flag" name="flag[]" class="form-control find_flag" multiple="">
                    <option value="non_unsc">PT-2</option>
                    <option value="pt2_plus">PT-2 PLUS</option>
                    <option value="Kendala">KENDALA</option>
                  </select>
                </div>
                <div class="form-group col-md-3">
                    <div class='input-group date'>
                        <input type='text' class="form-control" name='rangedate' value="{{ str_replace('-', '/', $start) }} - {{ str_replace('-', '/', $end) }}">
                        <input type="hidden" name="tgl_a">
                        <input type="hidden" name="tgl_f">
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar kalender" style="cursor: pointer;"></span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-12">
                    <div class='input-group'>
                        <button type="submit" class="btn btn-block btn-primary">Cari</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
  </div>

  <div class="panel panel-warning">
    <div class="panel-heading">List WO Hero</div>
    <div class="panel-body">
      <ul class="nav nav-tabs">
        <li class="active">
          <a href="#menu_3" id="#menup3" class="nav-link" role="tab" data-toggle="tab">Siap Dikerjaan<span style="color: black;" class="label kerja label-rouded label-info pull-right">{{ $count_ready }}</span></a>
        </li>
        <li>
          <a href="#menu_0" id="#menup0" class="nav-link" role="tab" data-toggle="tab">Belum Di Flagging SDI<span style="color: black;" class="label kerja label-rouded label-danger pull-right">{{ $count_not_ready }}</span></a>
        </li>
      </ul>
      <div class="tab-content">
        <div id="menu_3" class="tab-pane fade">
          <div class="page-wrap">
            <table class="teknisi_3 table toggle-circle table-hover table-bordered table-responsive">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Datel</th>
                  <th>STO</th>
                  <th>Nama Lokasi</th>
                  <th>Koordinat ODP PLAN</th>
                  <th>Tanggal Input</th>
                  <th>Tanggal Done</th>
                  <th>Catatan Hero</th>
                  <th>Catatan SDI</th>
                  <th>Catatan Admin</th>
                  <th>Status HERO</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @php
                  $no = 0;
                @endphp
                @foreach($fd_ready as $v)
                  @php
                    $first = true;
                  @endphp
                  @foreach($v['list'] as $vv)
                    <tr>
                      @if($first == true)
                        <td rowspan="{{ count($v['list']) }}" style="vertical-align: middle;">{{ ++$no }}</td>
                        <td rowspan="{{ count($v['list']) }}" style="vertical-align: middle;"> {{ $v['datel'] }} </td>
                        @php
                          $first = false;
                        @endphp
                      @endif
                      <td>{{ $vv->sto }}</td>
                      <td>{{ $vv->project_name }}</td>
                      <td>{{ $vv->odp_koor }}</td>
                      <td>{{ $vv->tgl_buat }}</td>
                      <td>{{ $vv->tgl_selesai ?? '-' }}</td>
                      <td>{{ preg_replace("/(.{60})/", "$1\n\r", $vv->keterangan_hero) }}</td>
                      <td>{{ preg_replace("/(.{60})/", "$1\n\r", $vv->keterangan_sdi) }}</td>
                      <td>{{ $vv->catatan_HD }}</td>
                      <td>
                        @switch($vv->flagging)
                          @case('non_unsc')
                            PT-2
                          @break
                          @case('pt2_plus')
                            PT-2 Plus
                          @break
                        @endswitch</td>
                      <td>{{ $vv->lt_status ?? '-' }}</td>
                      <td>
                        @if(session('auth')->pt2_level == 7)
                          <b>Hanya Admin Yang Bisa Akses</b>
                        @elseif($vv->status_wo_pt2 == 'ADA' && is_null($vv->regu_name) )
                          <a href="/admin/edit/non_un/{{ $vv->id }}" target="_blank" type="button" style="color: #03a9f3;" class="btn btn-light btn-sm create_wo"><i data-icon='&#xe005;' class='linea-icon linea-basic fa-fw' style='font-size: 17px;'></i>&nbsp;Create WO</a>
                        @else
                          <b>WO Sedang Dalam Progress Dibuat</b>
                        @endif
                      </td>
                    </tr>
                  @endforeach
                @endforeach
              </tbody>
            </table>
            <div style="text-align:center">
              <div class="holder data_holder3"></div>
            </div>
          </div>
        </div>
        <div id="menu_0" class="tab-pane fade">
          <div class="page-wrap">
            <table class="teknisi_0 table toggle-circle table-hover table-bordered table-responsive">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Datel</th>
                  <th>STO</th>
                  <th>Nama Lokasi</th>
                  <th>Koordinat ODP PLAN</th>
                  <th>Tanggal Input</th>
                  <th>Tanggal Done</th>
                  <th>Catatan Hero</th>
                  <th>Catatan SDI</th>
                  <th>Catatan Admin</th>
                  <th>Status HERO</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @php
                  $no = 0;
                @endphp
                @foreach($fd_not_ready as $v)
                  @php
                    $first = true;
                  @endphp
                  @foreach($v['list'] as $vv)
                    <tr>
                      @if($first == true)
                        <td rowspan="{{ count($v['list']) }}" style="vertical-align: middle;">{{ ++$no }}</td>
                        <td rowspan="{{ count($v['list']) }}" style="vertical-align: middle;"> {{ $v['datel'] }} </td>
                        @php
                          $first = false;
                        @endphp
                      @endif
                      <td>{{ $vv->sto }}</td>
                      <td>{{ $vv->project_name }}</td>
                      <td>{{ $vv->odp_koor }}</td>
                      <td>{{ $vv->tgl_buat }}</td>
                      <td>{{ $vv->tgl_selesai ?? '-' }}</td>
                      <td>{{ preg_replace("/(.{60})/", "$1\n\r", $vv->keterangan_hero) }}</td>
                      <td>{{ preg_replace("/(.{60})/", "$1\n\r", $vv->keterangan_sdi) }}</td>
                      <td>{{ $vv->catatan_HD }}</td>
                      <td>
                        @switch($vv->flagging)
                          @case('non_unsc')
                            PT-2
                          @break
                          @case('pt2_plus')
                            PT-2 Plus
                          @break
                        @endswitch</td>
                      <td>{{ $vv->lt_status ?? '-' }}</td>
                      <td>
                        @if(session('auth')->pt2_level == 7)
                          <b>Hanya Admin Yang Bisa Akses</b>
                        @elseif($vv->status_wo_pt2 == 'ADA' && is_null($vv->regu_name) )
                          <a href="/admin/edit/non_un/{{ $vv->id }}" target="_blank" type="button" style="color: #03a9f3;" class="btn btn-light btn-sm create_wo"><i data-icon='&#xe005;' class='linea-icon linea-basic fa-fw' style='font-size: 17px;'></i>&nbsp;Create WO</a>
                        @elseif($vv->lt_status != 'Selesai' || is_null($vv->lt_status) )
                          <b>Sudah Di Pickup Tim</b>
                        @else
                          <b>WO Sedang Dalam Progress Dibuat</b>
                        @endif
                      </td>
                    </tr>
                  @endforeach
                @endforeach
              </tbody>
            </table>
            <div style="text-align:center">
              <div class="holder data_holder0"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('footerS')
<script src="/bower_components/moment/min/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script>
	$(function(){
    $(".nav-tabs a").click(function(){
      $(this).tab('show');
    });

    $('#menu_3').toggleClass("in active");

    var data_datel_sto = {!! json_encode($datel) !!},
    datel = {};

    $.each(data_datel_sto, function(k, v){
      datel[v.datel] = {
        id: v.datel,
        text: v.datel,
      };
    });

    result_datel = [];

    result_datel.push(Object.keys(datel).map((k) => datel[k]) );

    result_datel = result_datel[0];

    $('.find_datel').select2({
      width: '100%',
      placeholder: 'Pilih Datel',
      data: result_datel
    });

    $('.find_datel').on('change', function(){
      $('.find_sto').empty();
      var isi = $(this).val(),
      find_sto = $.grep(data_datel_sto, function(e){ return e.datel == isi; }),
      sto = {};

      $.each(find_sto, function(k, v){
        sto[v.sto] = {
          id: v.sto,
          text: v.sto,
        };
      });

      result_sto = [];

      result_sto.push(Object.keys(sto).map((k) => sto[k]) );

      result_sto = result_sto[0];

      $('.find_sto').select2({
        width: '100%',
        placeholder: 'STO Bisa Dipilih Lebih Dari Satu',
        data: result_sto
      });
    });

    $('.find_datel').val(null).change();

    $('.find_flag').select2({
      placeholder: 'Flagging Bisa Dipilih Lebih Dari Satu'
    });

    var value = $('input[name="rangedate"]').val(),
      date    = value.split(' '),
      slice1  = date[0].split("/").reverse(),
      month1_ = slice1[0]+'-'+slice1[2]+'-'+slice1[1],
      slice2  = date[2].split("/").reverse(),
      month2_ = slice2[0]+'-'+slice2[2]+'-'+slice2[1];

      $('input[name="tgl_a"]').val(month1_);
      $('input[name="tgl_f"]').val(month2_);

      $('[data-toggle="tooltip"]').tooltip();

      $('input[name="rangedate"]').daterangepicker({
          opens: 'right',
          locale: {
            format: 'YYYY-MM-DD'
          }
      }, function(start, end){
          var month1 = start.format('YYYY-MM-DD'),
          month2 = end.format('YYYY-MM-DD');
          $('input[name="tgl_a"]').val(month1);
          $('input[name="tgl_f"]').val(month2);
      });

      $('.kalender').click(function(e){
          e.preventDefault();
          $('input[name="rangedate"]').click();
      });
	});
</script>
@endsection
