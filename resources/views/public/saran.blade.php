@extends('layout')
@section('title', 'Saran dan Kritik')
@section('headerS')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link href="/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
<link href="/bower_components/bootstrap-switch/bootstrap-switch.min.css" rel="stylesheet">
@endsection
@section('style')
<style type="text/css">

</style>
@endsection
@section('content')
<div class="container-fluid" style="padding-top: 25px;">
	<a type="button" class="btn btn-default" href="{{URL::to('/saran/add')}}"><span data-icon="D" class="linea-icon linea-ecommerce fa-fw" style="font-size: 20px; vertical-align:middle;" id="contactG"></span>&nbsp;Tambah Saran dan Kritik</a>
</div>
<div class="container-fluid" style="padding-top: 25px;">
	<div class="panel panel-warning">
		<div class="panel-heading header-date">Dashboard PT3</div>
		<div class="panel-body">
			<div class="table-responsive">
				<table id="teknisi" class="table table-striped">
					<thead>
						<tr>
							<th>#</th>
							<th>Pesan</th>
							<th>Status</th>
						</tr>
					</thead>
					<tbody>
						@if(count($data))
							@foreach($data as $no => $d)
							<tr>
								<td>{{ ++$no }}</td>
								<td>{{ $d->pesan }}</td>
								<td>
									@if(session('auth')->pt2_level == 5)
									<div class="m-b-30 bt-switch">
										<input type="checkbox" class="toogletogle" data-on-color="danger" {{ $d->status == 1 ? 'checked disabled' : '' }} id="{{ $d->id }}" data-off-color="warning" data-on-text="Selesai" data-off-text="Proses">
									</div>
									@else
									{{ $d->status == 1 ? 'Selesai' : 'Di Proses' }}
									@endif
								</td>
							</tr>
							@endforeach
						@endif
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection
@section('footerS')
<script src="/bower_components/bootstrap-switch/bootstrap-switch.min.js"></script>
<script>
	$(function(){
		$(".bt-switch input[type='checkbox']").bootstrapSwitch();

		$('.toogletogle').on('switchChange.bootstrapSwitch', function (event, state) {
			if ($(this).is(':checked')){
				var notif=$.toast({
					position: 'mid-center',
					showHideTransition: 'plain',
					hideAfter: false
				});
				$.ajax({
					type: 'POST',
					url : '/save_update_list',
					beforeSend: function(){
						notif.update({
							heading: 'Pemberitahuan',
							text: 'Sedang Disimpan!',
							position: 'mid-center',
							icon: 'info',
							showHideTransition: 'plain',
							stack: false
						});
					},
					data : {id: $(this).attr('id'), _token: "{{ csrf_token() }}", },
					success: function(data) {
						window.location.href = "/saran";
					}
				});
			}
		});
	});
</script>
@endsection
