@extends('layout')
@section("title", "Tambah Saran Dan Kritik")
@section('content')
<div class="form-group">
	<a type="button" class="btn btn-default" href="{{URL::to('/saran')}}"><i data-icon="&#xe00f;" class="linea-icon linea-aerrow fa-fw" style="font-size: 20px; vertical-align:middle;"></i>&nbsp;Kembali</a>
</div>
<div class="container-fluid" style="padding-top: 25px;">
	<form method="post" id="regu_form">
		<div class="panel panel-info">
			<div class="panel-heading">Tambah Saran Dan Kritik</div>
			<div class="panel-body">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<div class="form-group">
					<label>Tambah saran</label>
					<div class="alert saran alert-danger" role="alert" style="display: none;">
						<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
						<span class="sr-only">Error:</span>
						Saran dan Kritik Tolong Ditambahkan!
					</div>
					<textarea class="form-control" id="saran" name="saran" placeholder="Masukkan Saran Dan Kritik" ></textarea>
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-block btn-info save_me" style="text-align: center;"><span data-icon="M" class="linea-icon linea-basic fa-fw" style="font-size: 20px; color: #ffffff; "></span>&nbsp;Simpan Perubahan</button>
				</div>
			</div>
		</div>
	</form>
</div>
@endsection
@section('footerS')
<script>
	$(function(){
		$('.save_me').click(function(e){
			e.preventDefault();
			var saran = $.trim($('#saran').val()),
			datastring = $("#regu_form").serialize();

			if (saran === '') {
				$('.saran').css({display: "block"});
				$('#saran').css({border: "2px solid red"});
				$('#saran').focus();
			}else{
				$('.saran').css({display: "none"});
				$('#saran').css({border: ""});
			}


			if (saran === ''){
				return false;
			}else{
				var notif=$.toast({
					position: 'mid-center',
					showHideTransition: 'plain',
					hideAfter: false
				});
				url_sent = "/saran/add";
				$.ajax({
					type: 'POST',
					url : url_sent,
					beforeSend: function(){
						notif.update({
							heading: 'Pemberitahuan',
							text: 'Sedang Disimpan!',
							position: 'mid-center',
							icon: 'info',
							showHideTransition: 'plain',
							stack: false
						});	
					},
					data : datastring,
					success: function(data) {
						notif.update({
							heading: 'Pemberitahuan',
							text: 'Saran Dan Kritik Telah Berhasil Disimpan!',
							position: 'mid-center',
							icon: 'success',
							stack: false
						})			
						window.location = "/saran";
						/*console.log(data);*/
					},
					error: function(e){
						var exception = ( typeof object !== "undefined" && e.responseJSON.exception ) ? e.responseJSON.exception : '';
						var file = ( typeof object !== "undefined" && e.responseJSON.file ) ? e.responseJSON.file : '';
						var line = ( typeof object !== "undefined" && e.responseJSON.line ) ? e.responseJSON.line : '';
						var message = ( typeof object !== "undefined" && e.responseJSON.message ) ? e.responseJSON.message : '';
						var status_error = e.status ? e.status : '' ;
						var link = window.location.href ;
						/*console.log(e);*/
						$.ajax({
							type: 'POST',
							url: "/send/err",
							data: {
								"_token": "{{ csrf_token() }}",
								"exception" : exception,
								"file" : file,
								"line" : line,
								"message" : message,
								"status_error" : status_error,
								"link" : link
							},
							success: function (data) {
								/*console.log(data);*/
							},
							error: function(e){
								/*console.log(e);*/
							}
						});
						notif.update({
							heading: 'Error',
							text: 'Data Kesalahan Telah Dikirim. Untuk Lebih Lanjut, Klik link <a href="https://t.me/RendyL">Ini</a>',
							showHideTransition: 'fade',
							icon: 'error',
							showHideTransition: 'plain',
							allowToastClose: false
						});
					}
				});
			}
		});
	});
</script>
@endsection