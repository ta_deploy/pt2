@extends('layout')
@section('title', 'Material Outstanding')
@section('headerS')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link href="/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
@endsection
@section('style')
<style type="text/css">
    th, td{
        text-align: center;
        white-space:nowrap;
    }
    div>table {
        float: left
    }
</style>
@endsection
@section('content')
<div class="container-fluid" style="padding-top: 25px;">
    <div class="form-group">
        <a type="button" class="btn btn-default" href="{{URL::to('/rekapPO/create')}}"><i data-icon="v" class="linea-icon linea-ecommerce fa-fw" id="contactG" style="font-size: 20px;"></i>&nbsp;Tambah PO</a>
    </div>
    <ul class="nav nav-tabs">
        <li><a href="#menupt2"  data-toggle="tab">List PO</a></li>
        <li><a href="#menupt3" data-toggle="tab">List Non PO</a></li>
    </ul>
    <div class="tab-content">
        <div id="menupt2" class="tab-pane active">
            <div class="panel panel-warning">
                <div class="panel-heading header-date">Matrix Periode {{ Request::segment(2) }}</div>
                <div class="panel-body">
                    <div class='input-group date'>
                        <input type='text' class="form-control" name='rangedate' value="{{date("m/d/Y", strtotime("first day of this month"))}} - {{date("m/d/Y", strtotime("last day of this month"))}}" readonly>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar kalender" style="cursor: pointer;"></span>
                        </span>
                    </div>
                    <div class="table-responsive">
                        <table id="teknisi" class="table table-striped">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nomor PO</th>
                                    <th>ID Project TA</th>
                                    <th>Alokasi Jumlah ODP</th>
                                    <th>Odp Terpasang</th>
                                    <th>Nilai PO</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $result = 1; ?>
                                @foreach($report_po as $po)
                                <tr>
                                    <td>{{ $result++}}</td >
                                    <td>{{ $po->no_po }}</td>
                                    <td> <a type="button" class="btn btn-light" href='{{ URL::to("/rekapPO/edit/{$po->id_tes}") }}' target="_blank">&nbsp;{{ $po->id_project_Ta }}</a></td>
                                    <td>{{ $po->juml_odp }}</td>
                                    <td>{{ $po->odp_namnya }}</td>
                                    <td>Rp {{ number_format($po->nilai_po,2,",",".") }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div id="menupt3" class="tab-pane">
            <form id="formlistG" name="formlistG">
                <div class="panel panel-warning">
                    <div class="panel-heading header-date">Matrix Periode {{ Request::segment(2) }}</div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table id="teknisi" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Project</th>
                                        <th>ID Project TA</th>
                                        <th>Nama ODP</th>
                                        <th>STO</th>
                                        <th>Nama Regu</th>
                                        <th>Status</th>
                                        <th colspan="2">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $result = 1; ?>
                                    @foreach($report_non_po as $list_po)
                                    <tr>
                                        <td>{{ $result++}}</td>
                                        <td>{{ $list_po->project_name }}</td>
                                        <td>{{ $list_po->proaktif_id }}</td>
                                        <td>{{ $list_po->odp_nama }}</td>
                                        <td>{{ $list_po->sto }}</td>
                                        <td>{{ $list_po->regu_name }}</td>
                                        <td>{{ $list_po->status }}</td>
                                        <td>
                                            @if($list_po->status != 'Completed' && in_array(Session::get('auth')->pt2_level, [2, 5]))
                                            <a style="color: #CA3A34FF;" type="button" class="btn delete btn-light" href='{{URL::to("/rekapPO/delete/po/{$list_po->id}")}}'><span data-icon="&#xe01c;" class="linea-icon linea-basic fa-fw" style="font-size: 17px; "></span>Delete</a>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('footerS')
<script src="/bower_components/moment/min/moment.min.js"></script>
<script src="/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="/bower_components/Chart.js/Chart.min.js"></script>
<script>
    $(function(){
        $(".nav-tabs a").click(function(){
            $(this).tab('show');
        });

        $('input[name="rangedate"]').daterangepicker({
            opens: 'left'
        }, function(start, end){
            var month1 = start.format('YYYY-MM-DD'),
            month2 = end.format('YYYY-MM-DD')+" 23:59:59";
            /*console.log(month1);*/
            $.ajax({
                url:"/rekapPO/search/"+month1+"/"+month2,
                type:'GET',
                beforeSend: function(){
                    $.toast({
                        heading: 'Pemberitahuan',
                        text: 'Harap Tunggu Sebentar',
                        position: 'mid-center',
                        icon: 'info',
                        hideAfter: 2000,
                        stack: false
                    })
                },
                success: function(data){
                    $('#teknisi').html(data);
                    $('.header-date').html("Matrix Periode "+start.format('DD-MM-YYYY')+" Sampai "+end.format('DD-MM-YYYY'));
                }

            });
        });

        $('.kalender').click(function(e){
            e.preventDefault();
            $('input[name="rangedate"]').click();
        });
    });
</script>
@endsection
