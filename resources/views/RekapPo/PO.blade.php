@extends('layout')
@section('title', 'Tambah PO')
@section('style')
<style type="text/css">
.select2-search {
	color:black;
}
/* .select2-results { background-color: #353c48; } */
</style>
@endsection
@section('content')
<div class="form-group">
	<a type="button" class="btn btn-default" href="/rekapPO/list/{{ date('Y-m') }}"><span class="glyphicon glyphicon-menu-left" style="color:#000000; font-size: 15px; " id="contactG"></span>&nbsp;Kembali</a>
</div>
<div class="container-fluid" style="padding-top: 25px;">
	<form method="post" role="form" id="po_form">
		<div class="panel panel-primary">
			<div class="panel-heading">Form PO</div>
			<div class="panel-body">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<div class="form-group row">
					<div class="col-md-3">
						<label>Nomor PO</label>
					</div>
					<div class="col-md-9">
						<div class="alert nomor alert-danger" role="alert" style="display: none;">
							<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
							<span class="sr-only">Error:</span>
							Nomor PO Harus Diisi!
						</div>
						<input type="text" class="form-control" id="nomor" name="nomor" placeholder="Masukkan Nomor Project">
					</div>
				</div>
				<div class="form-group row">
					<div class="col-md-3">
						<label>ID Project TA</label>
					</div>
					<div class="col-md-9">
						<div class="alert id_project alert-danger" role="alert" style="display: none;">
							<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
							<span class="sr-only">Error:</span>
							ID Project TA harus Diisi!
						</div>
						<input type="text" class="form-control" id="id_project" name="id_project" placeholder="Masukkan ID Project TA">
					</div>
				</div>
				<div class="form-group row">
					<div class="col-md-3">
						<label>Nama Project</label>
					</div>
					<div class="col-md-9">
						<div class="alert nama_project alert-danger" role="alert" style="display: none;">
							<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
							<span class="sr-only">Error:</span>
							Nama Project Harus Diisi!
						</div>
						<input type="text" class="form-control" id="nama_project" name="nama_project" placeholder="Masukkan Nama Project">
					</div>
				</div>
				<div class="form-group row">
					<div class="col-md-3">
						<label>Alokasi Jumlah ODP</label>
					</div>
					<div class="col-md-9">
						<input type="text" class="form-control" id="juml_odp" name="juml_odp" placeholder="Masukkan Nilai Project" data-toggle="tooltip" data-placement="bottom" title="Kosongkan Jika Tidak Memiliki Jumlah Project">
					</div>
				</div>
				<div class="form-group row">
					<div class="col-md-3">
						<label>Nilai PO</label>
					</div>
					<div class="col-md-9">
						<div class="alert project_nama alert-danger" role="alert" style="display: none;">
							<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
							<span class="sr-only">Error:</span>
							Nilai PO Harus Diisi!
						</div>
						<input type="text" class="form-control" id="po_val" name="po_val" placeholder="Masukkan Nilai Project">
					</div>
				</div>
				<div class="form-group row">
					<button type="submit" class="btn save_me btn-info glyphicon glyphicon-floppy-saved btn-block" style="font-size: 15px; center">&nbsp;Simpan</button>
				</div>
			</div>
		</div>
	</form>
</div>
@endsection
@section('footerS')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script type="text/javascript">
	$('#po').select2({
		width : '100%',
		placeholder: "Pilih Project...",
		minimumInputLength: 2,
		multiple: true,
		allowClear: true,
		ajax: {
			url: "{{ route('po_search') }}",
			dataType: 'json',
			data: function (params) {
				return {
					q: $.trim(params.term)
				};
			},
			processResults: function (data) {
				return {
					results: data
				};
			},
			cache: true
		}
	});

	var format = function(num){
		var str = num.toString().replace("", ""), parts = false, output = [], i = 1, formatted = null;
		if(str.indexOf(".") > 0) {
			parts = str.split(".");
			str = parts[0];
		}
		str = str.split("").reverse();
		for(var j = 0, len = str.length; j < len; j++) {
			if(str[j] != ",") {
				output.push(str[j]);
				if(i%3 == 0 && j < (len - 1)) {
					output.push(",");
				}
				i++;
			}
		}
		formatted = output.reverse().join("");
		return("" + formatted + ((parts) ? "." + parts[1].substr(0, 2) : ""));
	};

	$(function(){
		$("#juml_odp").on("keypress keyup blur",function (event) {
			$(this).val($(this).val().replace(/[^\d].+/, ""));
			if ((event.which < 48 || event.which > 57)) {
				event.preventDefault();
			}
		});
		// $('#po_val').prop('readonly', true);
		// $('#juml_odp').keyup(function(){
		// 	if($(this).val().length === 0){
		// 		console.log('ini kosong');
		// 		$('#po_val').prop('readonly', true);
		// 	}else{
		// 		$('#po_val').prop('readonly', false)
		// 	}
		// });
		// $('#po_val').keyup(function(e){
		// 	$(this).val(format($(this).val()));
		// });

		$('.save_me').click(function(e){
			e.preventDefault();
			var nomor = $.trim($('#nomor').val()),
			id_project = $.trim($('#id_project').val()),
			nama_project = $.trim($('#nama_project').val()),
			juml_odp = $.trim($('#juml_odp').val()),
			po_val = $.trim($('#po_val').val()),
			c_nomor = $('.nomor'),
			c_id_project = $('.id_project'),
			c_nama_project = $('.nama_project'),
			c_juml_odp = $('.juml_odp'),
			c_po_val = $('.po_val'),
			notif=$.toast({
				position: 'mid-center',
				showHideTransition: 'plain',
				hideAfter: false
			}),
			datastring = $("#po_form").serialize();

			if (nomor === '') {
				$('.nomor').css({display: "block"});
				$('#nomor').css({border: "2px solid red"});
				$('#nomor').focus();
			}else{
				$('.nomor').css({display: "none"});
				$('#nomor').css({border: ""});
			}

			if (id_project === '') {
				$('.id_project').css({display: "block"});
				$('#id_project').css({border: "2px solid red"});
				$('#id_project').focus();
			}else{
				$('.id_project').css({display: "none"});
				$('#id_project').css({border: ""});
			}

			if (nama_project === '') {
				$('.nama_project').css({display: "block"});
				$('#nama_project').css({border: "2px solid red"});
				$('#nama_project').focus();
			}else{
				$('.nama_project').css({display: "none"});
				$('#nama_project').css({border: ""});
			}
			if(juml_odp.length !== 0){
				if (juml_odp === '') {
					$('.juml_odp').css({display: "block"});
					$('#juml_odp').css({border: "2px solid red"});
					$('#juml_odp').focus();
				}else{
					$('.juml_odp').css({display: "none"});
					$('#juml_odp').css({border: ""});
				}

				if (po_val === '') {
					$('.po_val').css({display: "block"});
					$('#po_val').css({border: "2px solid red"});
					$('#po_val').focus();
				}else{
					$('.po_val').css({display: "none"});
					$('#po_val').css({border: ""});
				}
			}
			if (nomor === '' || id_project === '' || nama_project === '' ){
				return false;
			}else if(juml_odp.length !== 0 && (juml_odp === '' || po_val === '')){
				return false;
			}else{
				$.ajax({
					type: 'POST',
					url : "{{ route('simpan_po') }}",
					beforeSend: function(){
						notif.update({
							heading: 'Pemberitahuan',
							text: 'Sedang Disimpan!',
							position: 'mid-center',
							icon: 'info',
							showHideTransition: 'plain',
							stack: false
						});
					},
					data : datastring,
					success: function(data) {
						notif.update({
							heading: 'Pemberitahuan',
							text: 'ID Project '+id_project+' Telah Berhasil Disimpan!',
							position: 'mid-center',
							icon: 'success',
							stack: false
						})
						window.location = "/rekapPO/list/{{ date('Y-m') }}";
						/*console.log(data);*/
					},
					error: function(e){
						var exception = e.responseJSON.exception;
						var file = e.responseJSON.file;
						var line = e.responseJSON.line;
						var message = e.responseJSON.message;
						var link = window.location.href ;
						/*console.log(e);*/
						$.ajax({
							type: 'POST',
							url: "/send/err/",
							data: {
								"_token": "{{ csrf_token() }}",
								"exception" : exception,
								"file" : file,
								"line" : line,
								"link" : link,
								"message" : message
							},
							success: function (data) {
								/*console.log(data);*/
							}
						});
						notif.update({
							heading: 'Error',
							text: 'Data Kesalahan Telah Dikirim. Untuk Lebih Lanjut Hubungi <a href="https://t.me/RendyL">TOMMAN</a>',
							showHideTransition: 'fade',
							icon: 'error',
							showHideTransition: 'plain',
							hideAfter: false,
							stack: false
						});
					}
				});
			}
		});
	});
</script>
@endsection
