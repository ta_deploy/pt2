@extends('layout')
@section('title', 'Tambah PO')
@section('style')
<style type="text/css">
    .select2-search {
        color:black;
    }
    /* .select2-results { background-color: #353c48; } */
</style>
@endsection
@section('content')
<div class="form-group">
    <a type="button" class="btn btn-default" href="/rekapPO/list/{{ date('Y-m') }}"><i data-icon="&#xe00f;" class="linea-icon linea-aerrow fa-fw" style="font-size: 20px; vertical-align:middle;"></i>&nbsp;Kembali</a>
</div>
<div class="container-fluid" style="padding-top: 25px;">
    <form method="post" role="form" id="po_form">
        <div class="panel panel-primary">
            <div class="panel-heading">Form PO</div>
            <div class="panel-body">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" id="id" value="{{$show->id_tes}}">
                <div class="form-group row">
                    <div class="col-md-3">
                        <label>Nomor PO</label>
                    </div>
                    <div class="col-md-9">
                        <div class="alert nomor alert-danger" role="alert" style="display: none;">
                            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                            <span class="sr-only">Error:</span>
                            Nomor PO Harus Diisi!
                        </div>
                        <input type="text" class="form-control" id="nomor" name="nomor" placeholder="Masukkan Nomor Project" value="{{ old('nomor', $show->no_po ?? '') }}">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-3">
                        <label>ID Project TA</label>
                    </div>
                    <div class="col-md-9">
                        <div class="alert id_project alert-danger" role="alert" style="display: none;">
                            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                            <span class="sr-only">Error:</span>
                            ID Project TA harus Diisi!
                        </div>
                        <input type="text" class="form-control" id="id_project" name="id_project" placeholder="Masukkan ID Project TA" value="{{ old('id_project', $show->id_project_Ta ?? '') }}">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-3">
                        <label>Nama Project</label>
                    </div>
                    <div class="col-md-9">
                        <div class="alert nama_project alert-danger" role="alert" style="display: none;">
                            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                            <span class="sr-only">Error:</span>
                            Nama Project Harus Diisi!
                        </div>
                        <input type="text" class="form-control" id="nama_project" name="nama_project" placeholder="Masukkan Nama Project" value="{{ old('nama_project', $show->project_nama ?? '') }}">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-3">
                        <label>Alokasi Jumlah ODP</label>
                    </div>
                    <div class="col-md-9">
                        <div class="alert project_nama alert-danger" role="alert" style="display: none;">
                            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                            <span class="sr-only">Error:</span>
                            Jumlah ODP Harus Diisi!
                        </div>
                        <input type="text" class="form-control" id="juml_odp" name="juml_odp" placeholder="Masukkan Nilai Project" value="{{ old('juml_odp', $show->juml_odp ?? '') }}">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-3">
                        <label>Nilai PO</label>
                    </div>
                    <div class="col-md-9">
                        <div class="alert project_nama alert-danger" role="alert" style="display: none;">
                            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                            <span class="sr-only">Error:</span>
                            Nilai PO Harus Diisi!
                        </div>
                        <input type="text" class="form-control" id="po_val" name="po_val" placeholder="Masukkan Nilai Project" value="{{ old('po_val', $show->nilai_po ?? '') }}">
                    </div>
                </div>
                <div class="form-group row">
                    <button type="submit" class="btn btn-primary btn-block save_me" style="text-align: center;"><span data-icon="M" class="linea-icon linea-basic fa-fw" style="font-size: 20px; color: #ffffff; "></span>&nbsp;Simpan</button>
                </div>
            </div>
        </div>
        <table id="groupPT2" class="table table-hover table-bordered" >
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama Project</th>
                    <th>Nama ODP</th>
                    <th>STO</th>
                    <th>Nama Regu</th>
                    <th>Status</th>
                    <th colspan="2">Action</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $i = 1;
                @endphp
                @foreach($list as $list_po)
                <tr>
                    <td>{{ $i++}}</td>
                    <td>{{$list_po->project_name}}</td>
                    <td>{{$list_po->odp_nama}}</td>
                    <td>{{$list_po->sto}}</td>
                    <td>{{$list_po->regu_name}}</td>
                    <td>{{$list_po->status}}</td>
                    <td>
                        @if($list_po->status != 'Completed' && in_array(Session::get('auth')->pt2_level, [2, 5]))
                        <a style="color: #CA3A34FF;" type="button" class="btn delete btn-light" href='{{URL::to("/rekapPO/delete/po/{$list_po->id}")}}'><span data-icon="&#xe01c;" class="linea-icon linea-basic fa-fw" style="font-size: 17px; "></span>Delete</a>
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </form>
</div>
@endsection
@section('footerS')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script type="text/javascript">
    $('#po').select2({
        width : '100%',
        placeholder: "Pilih Project...",
        minimumInputLength: 2,
        multiple: true,
        allowClear: true,
        ajax: {
            url: "{{ route('po_search') }}",
            dataType: 'json',
            data: function (params) {
                return {
                    q: $.trim(params.term)
                };
            },
            processResults: function (data) {
                return {
                    results: data
                };
            },
            cache: true
        }
    });
    $(function(){
        $('.save_me').click(function(e){
            e.preventDefault();
            var nomor = $.trim($('#nomor').val()),
            id = $.trim($('#id').val()),
            id_project = $.trim($('#id_project').val()),
            nama_project = $.trim($('#nama_project').val()),
            juml_odp = $.trim($('#juml_odp').val()),
            po_val = $.trim($('#po_val').val()),
            c_nomor = $('.nomor'),
            c_id_project = $('.id_project'),
            c_nama_project = $('.nama_project'),
            c_juml_odp = $('.juml_odp'),
            c_po_val = $('.po_val'),
            notif=$.toast({
                position: 'mid-center',
                showHideTransition: 'plain',
                hideAfter: false
            }),
            datastring = $("#po_form").serialize();

            if (nomor === '') {
                $('.nomor').css({display: "block"});
                $('#nomor').css({border: "2px solid red"});
                $('#nomor').focus();
            }else{
                $('.nomor').css({display: "none"});
                $('#nomor').css({border: ""});
            }

            if (id_project === '') {
                $('.id_project').css({display: "block"});
                $('#id_project').css({border: "2px solid red"});
                $('#id_project').focus();
            }else{
                $('.id_project').css({display: "none"});
                $('#id_project').css({border: ""});
            }

            if (nama_project === '') {
                $('.nama_project').css({display: "block"});
                $('#nama_project').css({border: "2px solid red"});
                $('#nama_project').focus();
            }else{
                $('.nama_project').css({display: "none"});
                $('#nama_project').css({border: ""});
            }

            if (juml_odp === '') {
                $('.juml_odp').css({display: "block"});
                $('#juml_odp').css({border: "2px solid red"});
                $('#juml_odp').focus();
            }else{
                $('.juml_odp').css({display: "none"});
                $('#juml_odp').css({border: ""});
            }

            if (po_val === '') {
                $('.po_val').css({display: "block"});
                $('#po_val').css({border: "2px solid red"});
                $('#po_val').focus();
            }else{
                $('.po_val').css({display: "none"});
                $('#po_val').css({border: ""});
            }
            if (nomor === '' || id_project === '' || nama_project === '' || juml_odp === '' || po_val === ''){
                return false;
            }else{
                $.ajax({
                    type: 'POST',
                    url : "/rekapPO/edit/{{$show->id_tes}}",
                    beforeSend: function(){
                        notif.update({
                            heading: 'Pemberitahuan',
                            text: 'Sedang Disimpan!',
                            position: 'mid-center',
                            icon: 'info',
                            showHideTransition: 'plain',
                            stack: false
                        });
                    },
                    data : datastring,
                    success: function(data) {
                        notif.update({
                            heading: 'Pemberitahuan',
                            text: 'ID Project '+id_project+' Telah Berhasil Diperbaharui!',
                            position: 'mid-center',
                            icon: 'success',
                            stack: false
                        });
                        window.location = "/rekapPO/{{ date('Y-m') }}";
                        /*console.log(data);*/
                    },
                    error: function(e){
                        var exception = e.responseJSON.exception;
                        var file = e.responseJSON.file;
                        var line = e.responseJSON.line;
                        var message = e.responseJSON.message;
                        var link = window.location.href ;
                        /*console.log(e);*/
                        $.ajax({
                            type: 'POST',
                            url: "/send/err",
                            data: {
                                "_token": "{{ csrf_token() }}",
                                "exception" : exception,
                                "file" : file,
                                "line" : line,
                                "message" : message,
                                "link" : link
                            },
                            success: function (data) {
                                /*console.log(data);*/
                            },
                            error: function(e){
                                /*console.log(e);*/
                            }
                        });
                        notif.update({
                            heading: 'Error',
                            text: 'Data Kesalahan Telah Dikirim. Untuk Lebih Lanjut, Klik link <a href="https://t.me/RendyL">Ini</a>',
                            showHideTransition: 'fade',
                            icon: 'error',
                            showHideTransition: 'plain',
                            hideAfter: false,
                            stack: false
                        });
                    }
                });
            }
        });
});
</script>
@endsection
