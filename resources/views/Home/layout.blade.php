<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="/images/favicon.png">
    <title>@yield('title')</title>
    <!-- Bootstrap Core CSS -->
    <link href="/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
    <!-- toast CSS -->
    <link href="/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
    <!-- morris CSS -->
    <link href="/bower_components/morrisjs/morris.css" rel="stylesheet">
    <!-- animation CSS -->
    <link href="/css/animate.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="/css/style.css" rel="stylesheet">
    <!-- color CSS -->
    <link href="/css/colors/default-dark.css" id="theme" rel="stylesheet">
</head>

<body>
    <div class="preloader">
        <div class="cssload-speeding-wheel"></div>
    </div>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-static-top m-b-0">
            <div class="navbar-header"> <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse"><i class="ti-menu"></i></a>
                <div class="top-left-part"><a class="logo" href="index.html"><b><img src="{{ asset('/images/logo.png/')}}" alt="home" class="dark-logo" style="width: 60px; height: 60px;" /></a></div>
                    <ul class="nav navbar-top-links navbar-left hidden-xs">
                        <li><a href="javascript:void(0)" class="open-close hidden-xs waves-effect waves-light"><i class="icon-arrow-left-circle ti-menu"></i></a></li>
                        <li>
                        </li>
                    </ul>
                </div>
            </nav>
            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse slimscrollsidebar">
                    <div class="user-profile">
                        <div class="dropdown user-pro-body">
                            <div><img src="/images/user.png" alt="user-img" class="img-circle"></div> <a href="#" class="dropdown-toggle u-dropdown" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Steave Gection <span class="caret"></span></a>
                            <ul class="dropdown-menu animated flipInY">
                                <li><a href="#"><i class="ti-user"></i> My Profile</a></li>
                                <li><a href="#"><i class="ti-wallet"></i> My Balance</a></li>
                                <li><a href="#"><i class="ti-email"></i> Inbox</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="#"><i class="ti-settings"></i> Account Setting</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="login.html"><i class="fa fa-power-off"></i> Logout</a></li>
                            </ul>
                        </div>
                    </div>
                    {{-- menunya --}}
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search hidden-sm hidden-md hidden-lg"></li>
                        <li> <a href="calendar.html" class="waves-effect"><span class="hide-menu">Berita Acara</span></a></li>
                        <li> <a href="index.html" class="waves-effect active"><span class="hide-menu"> Admin <span class="fa arrow"></span> <span class="label label-rouded label-custom pull-right">4</span></span></a>
                            <ul class="nav nav-second-level">
                                <li> <a href="index.html">Employee</a> </li>
                                <li> <a href="index2.html">User</a> </li>
                                <li> <a href="index3.html">Team</a> </li>
                            </ul>
                        </li>
                        <li> <a href="index.html" class="waves-effect active"><span class="hide-menu"> DSHR <span class="fa arrow"></span> <span class="label label-rouded label-custom pull-right">4</span></span></a>
                            <ul class="nav nav-second-level">
                                <li> <a href="index.html">Transaksi</a> </li>
                                <li> <a href="index2.html">DSHR Cluster</a> </li>
                            </ul>
                        </li>
                        <li> <a href="calendar.html" class="waves-effect"><span class="hide-menu">Assurance</span></a></li>
                        <li> <a href="#" class="waves-effect"><span class="hide-menu">Provisioning<span class="fa arrow"></span> <span class="label label-rouded label-info pull-right">13</span> </span></a>
                            <ul class="nav nav-second-level">
                                <li><a href="cards.html">Cards</a></li>
                                <li><a href="panels-wells.html">WO HD Fallout</a></li>
                                <li><a href="panel-ui-block.html">Dashboard</a></li>
                                <li><a href="panel-ui-block.html">Report PSB Harian</a></li>
                                <li><a href="panel-ui-block.html">Timeslot</a></li>
                                <li><a href="panel-ui-block.html">Monitoring Laporan</a></li>
                                <li><a href="panel-ui-block.html">Work Order</a></li>
                                <li><a href="panel-ui-block.html">Follow Up</a></li>
                                <li><a href="panel-ui-block.html">MS2N NOSFF</a></li>
                                <li><a href="panel-ui-block.html">MS2N Channel</a></li>
                                <li><a href="panel-ui-block.html">Laporan Material SVHA</a></li>
                                <li><a href="panel-ui-block.html">Laporan Material Migrasi</a></li>
                                <li><a href="panel-ui-block.html">Custom Grab SC</a></li>

                            </ul>
                        </li>
                        <li> <a href="forms.html" class="waves-effect"><span class="hide-menu">Matrix<span class="fa arrow"></span></span></a>
                            <ul class="nav nav-second-level">
                                <li><a href="form-basic.html">All Sector</a></li>
                                <li><a href="form-layout.html">Sector ASR</a></li>
                                <li><a href="form-advanced.html">Matrix HD</a></li>
                                <li><a href="form-material-elements.html">Ranking No Update</a></li>
                                <li><a href="form-float-input.html">Matrix Order Maintenance</a></li>
                            </ul>
                        </li>
                        <li> <a href="forms.html" class="waves-effect"><span class="hide-menu">Migrasi<span class="fa arrow"></span></span></a>
                            <ul class="nav nav-second-level">
                                <li><a href="form-basic.html">Workorder</a></li>
                                <li><a href="form-layout.html">Migrasi</a></li>
                                <li><a href="form-layout.html">Assurance</a></li>
                                <li><a href="form-layout.html">Provinsioning</a></li>
                                <li><a href="form-layout.html">Telco</a></li>
                            </ul>
                        </li>
                        <li> <a href="forms.html" class="waves-effect"><span class="hide-menu">Leaderboards<span class="fa arrow"></span></span></a>
                            <ul class="nav nav-second-level">
                                <li><a href="form-basic.html">Alpha ASR UPATEK</a></li>
                                <li><a href="form-layout.html">Alpha ASR CUI</a></li>
                                <li><a href="form-layout.html">Alpha PROV</a></li>
                                <li><a href="form-layout.html">Delta A</a></li>
                            </ul>
                        </li>
                        <li> <a href="forms.html" class="waves-effect"><span class="hide-menu">Dashboard<span class="fa arrow"></span></span></a>
                            <ul class="nav nav-second-level">
                                <li><a href="form-basic.html">Rekon</a></li>
                                <li><a href="form-layout.html">Ranking No Update</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- Left navbar-header end -->
            <!-- Page Content -->
            <div id="page-wrapper">
            </div>
            <!-- /#page-wrapper -->
        </div>
        <!-- /#wrapper -->
        <!-- jQuery -->
        <script src="/bower_components/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap Core JavaScript -->
        <script src="/bootstrap/dist/js/tether.min.js"></script>
        <script src="/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
        <!-- Menu Plugin JavaScript -->
        <script src="/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
        <!--slimscroll JavaScript -->
        <script src="/js/jquery.slimscroll.js"></script>
        <!--Wave Effects -->
        <script src="/js/waves.js"></script>
        <!--Counter js -->
        <script src="/bower_components/waypoints/lib/jquery.waypoints.js"></script>
        <script src="/bower_components/counterup/jquery.counterup.min.js"></script>
        <!--Morris JavaScript -->
        <script src="/bower_components/raphael/raphael-min.js"></script>
        <script src="/bower_components/morrisjs/morris.js"></script>
        <!-- Custom Theme JavaScript -->
        <script src="/js/custom.min.js"></script>
        <script src="/js/dashboard1.js"></script>
        <!-- Sparkline chart JavaScript -->
        <script src="/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
        <script src="/bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>
        <script src="/bower_components/toast-master/js/jquery.toast.js"></script>
       {{--  <script type="text/javascript">
            $(document).ready(function() {
                $.toast({
                    heading: 'Welcome to Elite admin',
                    text: 'Use the predefined ones, or specify a custom position object.',
                    position: 'top-right',
                    loaderBg: '#ff6849',
                    icon: 'info',
                    hideAfter: 3500,
                    stack: 6
                })
            });
        </script> --}}
        <!--Style Switcher -->
        <script src="/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
    </body>

    </html>
