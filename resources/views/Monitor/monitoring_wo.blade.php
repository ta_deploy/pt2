@extends('layout')
@section('title', 'Monitoring Revenue')
@section('headerS')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link href="/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/chart.js@3.8.2/dist/chart.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css" integrity="sha512-nMNlpuaDPrqlEls3IX/Q56H36qvBASwb3ipuo3MxeWbsQB1881ox0cRv7UPTgBlriqoynt35KjEwgGUeUXIPnw==" crossorigin="anonymous" />
@endsection
@section('style')
<style type="text/css">
    th, td{
        text-align: center;
        white-space:nowrap;
    }
    div>table {
        float: left
    }
</style>
@endsection
@section('content')
<div class="container-fluid" style="padding-top: 25px;">
  <div class="modal fade modal_detail_layout detail_layout modal_lg" id="detail_layout">
    <div class="modal_lg" role="document" style="width: 95%; margin: auto; margin-top: 20px;">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel" style="width: 400px;"><div id="nama_project"></div></h5>
        </div>
        <div class="modal-body" style="overflow-y:auto;padding: 0px 0px 0px 0px;">
          <div class="panel panel-warning">
            <div class="panel-body">
              <div class="table-responsive">
                <table id="teknisi" class="table table-striped">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Mitra</th>
                      <th>Regu</th>
                      <th>Jumlah Close</th>
                      <th>Pekerjaan</th>
                    </tr>
                  </thead>
                  <tbody class="body_detail">
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  {{-- <div id="canvas-wrapper">
    <canvas id="graph"></canvas>
  </div> --}}
  <div class="row">
    <div class="col-md-12" style="margin-bottom: 15px;">
      <a href="/material/revenue" type="button" class="btn btn-default btn-sm" id="button_photo"><span data-icon="&#xe00a;" class="linea-icon linea-basic fa-fw" style="font-size: 20px; vertical-align:middle;" ></span>&nbsp;List Material</a>
    </div>
    <div class="col-md-12">
      <a href="/reportMaterial/{{ date('Y-m') }}" target="_blank" type="button" class="btn btn-info btn-sm" id="button_photo"><span data-icon="&#xe005;" class="linea-icon linea-basic fa-fw" style="font-size: 20px; vertical-align:middle;" ></span>&nbsp;Report Material</a>
      <a href="/rfc/{{ date('Y-m-d',strtotime('first day of this month')) }}/{{ date('Y-m-d') }}" target="_blank" type="button" class="btn btn-primary btn-sm" id="button_photo" style="margin-left:15px;"><span data-icon="x" class="linea-icon linea-ecommerce fa-fw" style="font-size: 20px; vertical-align:middle;" ></span>&nbsp;Rekap Material RFC</a>
      <a href="/report/naik_core/{{ date('Y-m') }}" target="_blank" type="button" class="btn btn-danger btn-sm" id="button_photo" style="margin-left:15px;"><span data-icon="|" class="linea-icon linea-software fa-fw" style="font-size: 20px; vertical-align:middle;" ></span>&nbsp;List Core</a>
    </div>
  </div>
  <div class="col-md-12">
    <div class="panel panel-warning" style="margin-top: 20px;">
      <div class="panel-body">
        <form id="formlistG" name="formlistG">
          <div class="row">
            <div class="form-group col-md-12">
              <div class='input-group date'>
                <input type='text' class="form-control" name='rangedate' value="{{date("m/d/Y", strtotime("first day of this month"))}} - {{date("m/d/Y", strtotime("last day of this month"))}}"  disabled>
                <input type="hidden" name="tgl_a">
                <input type="hidden" name="tgl_f">
                <span class="input-group-addon">
                  <span class="glyphicon glyphicon-calendar kalender" style="cursor: pointer;"></span>
                </span>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="form-group col-md-12">
              <div class='input-group'>
                <button type="submit" class="btn btn-block btn-primary">Cari</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
    @forelse($data['rekap_revenue'] as $k => $v)
      <div class="panel panel-info">
        <div class="panel-heading header-date">Revenue Per Mitra {{ $k }}
          <div class="panel-action">
            <a href="#" data-perform="panel-collapse"><i style="font-size: 20px; color: white;" class="ti-minus"></i></a>
          </div>
        </div>
        <div class="panel-body">
          <div class="table-responsive">
            <table id="teknisi" class="table table-striped">
              <thead>
                <tr>
                  <th style="vertical-align: middle;">#</th>
                  <th style="vertical-align: middle;">Mitra</th>
                  <th style="vertical-align: middle;">Jumlah Close</th>
                  <th style="vertical-align: middle;">Nilai Material</th>
                  <th style="vertical-align: middle;">Nilai Jasa</th>
                </tr>
              </thead>
              <tbody>
                @php
                  $total_mat_ta = $total_jasa_ta = $total_close = $no = 0;
                @endphp
                @forelse($v as $kk => $vv)
                  @php
                    $total_mat_ta  += $vv['material_ta'];
                    $total_jasa_ta += $vv['jasa_ta'];
                  @endphp
                  <tr>
                    <td>{{ ++$no }}</td>
                    <td>{{ $vv['mitra'] }}</td>
                    @if(count($vv['jml_close']) != 0)
                      @php
                        $total_close += array_sum($vv['jml_close']);
                      @endphp
                      <td><a class="btn btn-sm btn-info btn-circle detail_modal" style="color: white; border-radius: 25px;" data-jenis_wo='{{ $k }}' data-mitra='{{ $vv['mitra'] }}'>{{ array_sum($vv['jml_close']) }}</a></td>
                    @else
                      <td>-</td>
                    @endif
                    <td>Rp. {{ number_format($vv['material_ta']) }}</td>
                    <td>Rp. {{ number_format($vv['jasa_ta']) }}</td>
                  </tr>
                @empty

                @endforelse

              </tbody>
              <tfoot>
                <tr>
                  <td colspan="2">Total Keseluruhan</td>
                  <td><a class="btn btn-sm btn-info btn-circle detail_modal" style="color: white; border-radius: 25px;" data-mitra='all'>{{ $total_close }}</a></td>
                  <td>Rp. {{ number_format($total_mat_ta) }}</td>
                  <td>Rp. {{ number_format($total_jasa_ta) }}</td>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
      </div>
    @empty
    @endforelse
  </div>
</div>
@endsection
@section('footerS')
<script src="/bower_components/moment/min/moment.min.js"></script>
<script src="/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js" integrity="sha512-2ImtlRlf2VVmiGZsjm9bEyhjGW4dU7B6TNwh/hx/iSByxNENtj3WVE6o/9Lj4TJeVXPi4bnOIMXFIJJAeufa0A==" crossorigin="anonymous"></script>
<script>
   $(function(){
    $('#jenis').select2({
      placeholder: 'Pilih Jenis Pekerjaan',
      width: '100%'
    });

    var value = $('input[name="rangedate"]').val(),
    date    = value.split(' '),
    slice1  = date[0].split("/").reverse(),
    month1_ = slice1[0]+'-'+slice1[2]+'-'+slice1[1],
    slice2  = date[2].split("/").reverse(),
    month2_ = slice2[0]+'-'+slice2[2]+'-'+slice2[1];

    $('input[name="tgl_a"]').val(month1_);
    $('input[name="tgl_f"]').val(month2_);

    $('input[name="rangedate"]').daterangepicker({
      opens: 'right'
    }, function(start, end){
      var month1 = start.format('YYYY-MM-DD'),
      month2 = end.format('YYYY-MM-DD');
      $('input[name="tgl_a"]').val(month1);
      $('input[name="tgl_f"]').val(month2);
    });

    $('.kalender').click(function(e){
      e.preventDefault();
      $('input[name="rangedate"]').click();
    });

    var load = {!! json_encode($load) !!};

    if(!$.isEmptyObject(load) ){
      $('input[name="tgl_a"]').val(load.tgl_a);
      $('input[name="tgl_f"]').val(load.tgl_f);

      var tgl_a = load.tgl_a.split('-'),
      tgl_f = load.tgl_f.split('-');

      $('input[name="rangedate"]').val(`${tgl_a[1]}/${tgl_a[2]}/${tgl_a[0]} - ${tgl_f[1]}/${tgl_f[2]}/${tgl_f[0]}`);
    }

    $('.detail_modal').on('click', function(){
      var value = $('input[name="rangedate"]').val(),
      jenis_wo = $(this).attr('data-jenis_wo'),
      date    = value.split(' '),
      slice1  = date[0].split("/").reverse(),
      month1_ = slice1[0]+'-'+slice1[2]+'-'+slice1[1],
      slice2  = date[2].split("/").reverse(),
      month2_ = slice2[0]+'-'+slice2[2]+'-'+slice2[1],
      mitra = $(this).attr('data-mitra');

      $.ajax({
        type: 'GET',
        url : '/detail_monitoring',
        data:{
          jenis_wo: jenis_wo,
          mitra: mitra,
          start: month1_,
          end: month2_
        },
        success: function(data) {
          console.log(data)
          var body_detail = '',
          no = 0;
          $('#nama_project').text(`Close Per Teknisi Mitra ${mitra}`)
          $.each(data, function(k, v){
            body_detail += '<tr>';
            body_detail += `<td>${++no}</td>`;
            body_detail += `<td>${v.mitra}</td>`;
            body_detail += `<td>${v.regu}</td>`;
            body_detail += `<td>${Object.keys(v.job).length}</td>`;
            body_detail += '<td>';

            $.each(v.job, function(kk, vv){
              body_detail += `<a target='_blank' href='/search/data_id?search=${kk}'><span class='label label-rounded label-info' style='margin-left: 10px;'>${vv.pekerjaan} (${vv.odp_nama})</span></a>`;
            })

            body_detail += '</td>';
            body_detail += '</tr>';
          })

          $('#detail_layout').modal('show');
          $('.body_detail').html(body_detail);
        },
        error: function(e){
        }
      });
    })

  })

</script>
@endsection