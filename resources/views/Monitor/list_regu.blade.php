@extends('layout')
@section('title', "List Regu ". session('auth')->mitra )
@section('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.css">
<style type="text/css">
    th, td{
        text-align: center;
    }

    .table{
        font-size: 0.924em;
    }
    .btn {
        font-size: 1em;
    }
</style>
@endsection
@section('content')
<div class="container-fluid" style="padding-top: 25px;">
    <div class="panel panel-warning">
        <div class="panel-heading">List Regu {{ session('auth')->mitra }}</div>
        <div class="panel-body">
            @if($data)
            <div class="table-responsive">
                <table id="groupPT2" class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Regu</th>
                            <th>Nik 1</th>
                            <th>Nik 2</th>
                            <th>status</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php $no = 0; @endphp
                        @foreach($data as $no => $un)
                        <tr>
                            <td>{{ ++$no }}</td>
                            <td>{{ $un->uraian }}</td>
                            <td>{{ $un->nik_1 ?? $un->nik1 }}</td>
                            <td>{{ $un->nik_2 ?? $un->nik2 }}</td>
                            <td>{{ $un->ACTIVE == 0 ? 'NON-AKTIF' : 'AKTIF' }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            @else
            Tidak Ada Data!
            @endif
        </div>
    </div>
</div>
@endsection
@section('footerS')
<script src="/bower_components/moment/min/moment.min.js"></script>
<script src="/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.js"></script>
<script>
$(function(){
  $('.table').DataTable()
})
</script>
@endsection
