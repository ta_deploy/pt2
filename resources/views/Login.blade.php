<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" type="image/png" sizes="16x16" href="/images/logo.png">
	<title>Jointer</title>
	<link href="/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
	<link href="/bower_components/bootstrap-ext">
	<link href="css/animate.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
	<link href="css/colors/blue.css" id="theme" rel="stylesheet">
</head>
<body>
	<div class="preloader">
		<div class="cssload-speeding-wheel"></div>
	</div>
	@if (Session::has('alerts_tele'))
    @foreach(Session::get('alerts_tele') as $alert)
      <div class="alert alert-{{ $alert['type'] }}">{!! $alert['text'] !!}</div>
    @endforeach
  @endif
	<section id="wrapper" class="login-register">
        <div class="login-box" style="border-radius: 25px;">
            <div class="white-box" style="border-radius: 20px;">
				<form class="form-horizontal form-material" id="loginform" method="post">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<img src="{{ asset('/images/logo_ta.png/')}}" alt="Home" style="width:370px; height:200px;">
					<div class="form-group m-t-40">
						<div class="col-xs-12">
							<input class="form-control" name="user" type="text" required="" placeholder="Username">
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-12">
							<input class="form-control" name="pass" type="password" required="" placeholder="Password">
						</div>
					</div>
					<div class="form-group text-center m-t-20">
						<div class="col-xs-12">
							<button type="submit" class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Log In</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</section>

	<script src="/bower_components/jquery/dist/jquery.min.js"></script>
	<script src="/bootstrap/dist/js/tether.min.js"></script>
	<script src="/bootstrap/dist/js/bootstrap.min.js"></script>
	<script src="/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
	<script src="/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
	<script src="/js/jquery.slimscroll.js"></script>
	<script src="/js/waves.js"></script>
	<script src="/js/custom.min.js"></script>
	<script src="/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
</body>

</html>
