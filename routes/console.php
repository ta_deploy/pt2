<?php

use Illuminate\Foundation\Inspiring;
use App\Http\Controllers\GrabController;
use App\Http\Controllers\ReportController;
use App\DA\AdminModel;
use App\DA\ReportModel;
use App\Http\Controllers\AdminController;
/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
	$this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');

Artisan::command('grabAlistaDate {date}', function ($date) {
	GrabController::grabAlistaDate($date);
});

Artisan::command('grabAlistaMonth {date}', function ($date) {
	GrabController::grabAlistaMonth($date);
});

Artisan::command('grabAlista', function () {
	GrabController::grabAlista();
});

// aktifkan
Artisan::command('send_laporan', function(AdminModel $adminmodel) {
	$adminmodel->send_laporan();
});
// aktifkan
Artisan::command('excel_dm', function(ReportModel $reportmodel) {
	$reportmodel->list_odp_naik_download();
});
Artisan::command('3_excel_dm', function(ReportModel $reportmodel) {
	$reportmodel->list_odp_3_status();
});
//disabled sementara
Artisan::command('send_teknisi_prog_Tele', function(AdminModel $adminmodel) {
	$adminmodel->telegram_sent_dispatch();
});
// disabled sementara
Artisan::command('remind_unfinish', function () {
	AdminModel::remind_unfinish();
});

Artisan::command('refresh_pending', function(AdminModel $adminmodel) {
	$adminmodel->eksekusi_pending_order();
});

Artisan::command('sync_pt1', function(AdminController $AdminController) {
	$AdminController->sync_pt1();
});

Artisan::command('ubah_status_tidak_smpt', function(AdminModel $adminmodel) {
	$adminmodel->eksekusi_tidak_smpt_order();
});

Artisan::command('byRfc {rfc} {gudang}', function ($rfc, $gudang) {
	GrabController::grabAlistaByRfc($rfc, $gudang);
});

Artisan::command('loginkpro', function () {
	ReportController::pt2_kpro();
});
