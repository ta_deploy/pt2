<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/sync_pt1', 'AdminController@sync_pt1');

Route::get('/absen/{jns}/{id}', 'LoginController@action_absen');

Route::get('/login', 'LoginController@index');
Route::post('/login', 'LoginController@login');

Route::get('/get_all_tiang', 'AdminController@get_tiang');

Route::get('/logistik/grabalista', 'GrabController@grabAlista');
Route::get('/logistik/grabalistabetween/{start}/{end}', 'GrabController@grabAlistaBetween');

Route::get('/error/405/ooops/', 'AdminController@error405');

Route::get('/regu/ajax/search', 'AdminController@regu_ajax_search');
Route::get('/regu_id/ajax/search', 'AdminController@regu_id_ajax_search');
Route::get('/jointer/ajax/search', 'AdminController@jointer_get');
Route::get('/Download/file/{id}', 'AdminController@download_photo');

//API
Route::post('/admin/input_HERO', 'ApiController@save_hero');
Route::get('/admin/get_HERO/{date}', 'ApiController@get_data_hero');
Route::post('/admin/get_data_unsc', 'ApiController@get_data_unsc');

Route::get('/get_checked_book_odp', 'AdminController@get_checked_book_odp');

Route::get('/', function () {
  return redirect("/home/" . date('Y') );
});

Route::group(['middleware' => 'auth'], function () {
  Route::get('/logout', 'LoginController@logout');

  Route::get('/absensi', 'LoginController@absensi_check');
  Route::post('/absensi', 'LoginController@submit_absen');

  Route::group(['middleware' => 'check_role:all'], function () {
    Route::get('/home/{y}', 'HomeController@index');
    Route::get('/detail_dsh/{jenis}/{datel}/{tahun1}/{tahun2}', 'AdminController@list_dashboard');
    Route::get('/listsh/{jenis}/{datel}/{type}/{sto}/{tahun1}/{tahun2}/{jenis_wo}', 'AdminController@list_detail_dsh');
    Route::get('/download_data_detail/{jenis}/{datel}/{type}/{sto}/{tahun1}/{tahun2}/{jenis_wo}', 'AdminController@download_detail_dsh');

    //booking ODP
    Route::get('/show/booking_odp', 'BookingOdpController@show_gd');
    Route::get('/booking_odp/check', 'BookingOdpController@create_booking_odp');
    Route::post('/booking_odp/check', 'BookingOdpController@check_odp_booking');
    Route::get('/searchD/booking_odp', 'BookingOdpController@deepS_odp');
    Route::post('/searchD/booking_odp', 'BookingOdpController@find_deepS_odp');
    Route::get('/booking_odp/edit/{id}', 'BookingOdpController@edit_data_booking');
    Route::post('/booking_odp/edit/{id}', 'BookingOdpController@update_data_booking');
    Route::post('/booking_odp/input', 'BookingOdpController@save_data_booking_odp');
    Route::get('/delete_BO/{id}', 'BookingOdpController@delete_booking_odp');

    //geo search
    Route::get('/geo/allOrder', 'ReportController@geo_getAllOrder');
    Route::get('/geo/get_unsc', 'ReportController@geo_unsc');

    //ranked_ajax
    Route::get('/ranked/ajax/{month}/{month2}', 'ReportController@getRanked_ajax');

  });

  Route::group(['middleware' => 'check_role:admin,superadmin,monitor,monitor_marina,waspang_jointer,TL'], function () {
    Route::get('/admin/get_odp/LY', 'AdminController@find_one_odp_layout');

    Route::prefix('matrix')->group(function () {
      //report
      Route::get('{tgl}', 'ReportController@getMatrix');
      Route::get('detail_data/{detail}/{mitra}/{jenis_header}/{tgl}', 'ReportController@getDetailMatrix');
      Route::get('photo/{id}', 'ReportController@getphoto_Matrix');
    });

    Route::get('/search/data', 'AdminController@search_Data');
  });

  Route::group(['middleware' => 'check_role:admin,superadmin,hero'], function () {
    Route::prefix('hero')->group(function () {
      //HERO
      Route::get('table_HERO', 'HeroController@table_hero');
      Route::post('save_flagging', 'HeroController@post_flagging');
      Route::get('dashboard_HERO/{date}', 'HeroController@dashboard_hero');
      Route::get('progress/{date}', 'HeroController@dashboard_progress_hero');
    });
  });

  Route::get('/list_absen', 'AdminController@list_absen');
  Route::get('/absen_view/{id}', 'AdminController@absen_view');

  Route::group(['middleware' => 'check_role:admin,superadmin'], function () {
    Route::get('/cek_user', 'AdminController@register_check');
    //monitoring
    Route::get('/detail_monitoring', 'AdminController@detail_monitor_wo');

    Route::prefix('geo')->group(function () {
      //halaman map view
      Route::get('map', 'ReportController@geo');
      Route::get('find/lat{x}/long{y}', 'ReportController@geo_find');
    });

    Route::prefix('outside')->group(function () {
      //OUTSIDE PT2
      Route::get('order_outs/pt2', 'OutsideController@dashboard_outside_pt2');
      Route::get('add/{jenis}/{id}', 'OutsideController@input_Nog');
      Route::post('add/{jenis}/{id}', 'OutsideController@save_Nog');

      Route::get('OS_pt/{jenis}/{reg_jp}/{datel}/{sto}/', 'OutsideController@list_outside_pt2');
      Route::get('OS_N/{jenis}/{datel}/{sto}', 'OutsideController@list_outside_nog');

      Route::get('pt2_kpro', 'OutsideController@list_pt2_kpro');
    });

    Route::prefix('admin')->group(function () {
      Route::get('monitoring', 'AdminController@monitoring_data');

      Route::get('add/pt2spl/{id}', 'AdminController@order_pt2spl');
      Route::get('order/bank', 'AdminController@odp_bank');

      //non unsc
      Route::get('add/non_un/{value}', 'AdminController@order_nonun');
      Route::post('add/non_un/{value}', 'AdminController@save_dispatch_nonunsc');
      Route::get('edit/non_un/{value}', 'AdminController@edit_order_nonun');
      Route::post('edit/non_un/{value}', 'AdminController@save_dispatch_nonunsc');

      //unsc
      Route::Get('dispatch/add_s/{value}', 'AdminController@order_nonun_S');
      Route::post('dispatch/add_s/{value}', 'AdminController@save_dispatch_unsc');
      Route::get('dispatch/edit/add_s/{value}', 'AdminController@edit_order_nonun_S');
      Route::post('dispatch/edit/add_s/{value}', 'AdminController@save_dispatch_unsc');
    });

    Route::prefix('regu')->group(function () {
      //regu
      Route::get('list', 'ReguController@showregu');
      Route::get('history/{id}/{stts}', 'ReguController@list_detail_history');
      Route::get('add', 'ReguController@add_regu');
      Route::post('add', 'ReguController@save_regu')->name('simpan_regu');
      Route::get('hapus/{id}', 'ReguController@delete_regu');
      Route::get('edit/{id}', 'ReguController@edit_regu');
      Route::post('edit/{id}', 'ReguController@update_regu');
      Route::get('reactive/{id}', 'ReguController@reactive_regu');
    });

    //pencarian
    Route::get('/search/regu', 'AdminController@live_search_regu')->name('regu_live_search');
    Route::get('/search/data_id', 'AdminController@search_DataID');
    Route::get('/admin/push/pt2/{id}', 'AdminController@push_pt2');
    Route::get('/admin/get/odp/{value}/{value2}', 'AdminController@find_one_odp');
    Route::get('/admin/recovery/{jenis}/order/{id}', 'AdminController@recovery_data');
    Route::get('/Download/abd/{id}', 'AdminController@ABD');
    Route::get('/Download/pdf/{id}', 'AdminController@download_pdf_rfc');
    Route::get('/admin/delete/mydispatch&', 'AdminController@delete_mine_data');

    //telegram bot
    Route::get('/admin/send_laphar', 'AdminController@kirim_laphar');
    Route::post('/send_photo_button', 'AdminController@send_photo_button');
    Route::get('/Admin/LapHar', 'AdminController@laporan_harian');

    //go_live
    Route::get('/saldo_finish', 'AdminController@list_finish');
    Route::post('/upload_abd', 'AdminController@upload_abd');
    Route::get('/admin/find_live/sc', 'AdminController@ajax_psb_myir');

    //progress team
    Route::get('/admin/progress/team/{Y}/{m}', 'AdminController@get_data_progerss');

    Route::prefix('ranked')->group(function () {
      Route::get('{tgl}', 'ReportController@getRanked');
      Route::get('search/{month}/{month2}', 'ReportController@getRanked_search')->name('RA_search');
    });

    Route::prefix('rekap')->group(function () {
      Route::get('Y={year}&J={jenis}&M={month}', 'ReportController@getRekap');
      Route::get('Y={year}', 'ReportController@getRekap_Year');
      Route::get('Y={year}/matrix/search', 'ReportController@getRekap_Year_graph');
      Route::get('ajax/matrix/Y={year}&M={month}/matrix/search', 'ReportController@getRekap_matrix_ajax');
      Route::get('download/Y={year}&J={jenis}&M={month}', 'ReportController@upload_excel');
    });

    Route::get('/rfc/{month1}/{month2}', 'ReportController@ger_rfc');
    Route::get('/rfc/search/please/help', 'ReportController@get_rfc_detail');

    Route::get('/reportMaterial/{tgl}', 'ReportController@reportMaterial');
    Route::get('/Download/file/material_exc/{x}/{y}', 'ReportController@rekap_material');

    Route::get('/reportMaterial/search/{month}/{month2}', 'ReportController@reportMaterial_search')->name('RM_search');

    Route::prefix('rekapPO')->group(function () {
      //po
      Route::get('/list/{tgl}', 'RekapPoController@rekapPO');
      Route::get('/search/{month}/{month2}', 'RekapPoController@rekapPO_search')->name('PO_search');

      Route::get('/create', 'RekapPoController@po');
      Route::post('/create', 'RekapPoController@save_po')->name('simpan_po');
      Route::get('/delete/po/{id}', 'RekapPoController@delete_po');
      Route::get('/edit/{id}', 'RekapPoController@edit_po');
      Route::post('/edit/{id}', 'RekapPoController@update_po');
      Route::get('/search_data', 'RekapPoController@live_search_project')->name('po_search');
    });

    Route::prefix('material')->group(function () {
      Route::get('revenue', 'ReportController@list_material');
      Route::get('revenue_setting/{id}', 'ReportController@edit_revenue');
      Route::post('revenue_setting/{id}', 'ReportController@edit_revenue');
      Route::get('revenue_delete', 'ReportController@delete_Revenue');
    });

    Route::get('/report/naik_bulanan/{d}', 'ReportController@up_monthly');
    Route::get('/report_pekerjaan_up/{d}', 'ReportController@up_monthly_excel');
    Route::get('/detail_report_m/{tim}/{d}', 'ReportController@detail_report');

    Route::get('/report/naik_core/{d}', 'ReportController@naik_core');

    Route::get('/report/age', 'ReportController@umur_pekerjaan');
    Route::get('/report/detail_pt2_age/{mitra}/{regu}/{jenis}', 'ReportController@umur_pekerjaan_detail');

    Route::get('/rekap_pt2_kpro', 'ReportController@pt2_kpro');

    //download
    Route::get('/download/laporan', 'ReportController@report_daily');
    Route::post('/download/laporan', 'ReportController@result_report_daily');

    Route::prefix('pt3')->group(function () {
      //pt3
      Route::get('dashboard', 'Pt3Controller@dashboard');
      Route::get('list/{sto}/{sts}', 'Pt3Controller@list');
    });
  });

  //public
  Route::get('/saran', 'PublicController@saran_public');
  Route::get('/saran/add', 'PublicController@add_saran');
  Route::post('/saran/add', 'PublicController@save_saran');
  Route::post('/save_update_list', 'PublicController@save_update_list');

  Route::group(['middleware' => 'check_role:waspang_jointer,superadmin'], function () {
    Route::prefix('jointer_report')->group(function () {
      // Route::get('jointer_age/{date}', 'JointerController@jointer_age');
      Route::get('matrix/{date}', 'JointerController@matrix');
      Route::get('detail_dataM/{datel}/{sto}/{mitra}/{jenis}/{tgl}', 'JointerController@detail_data');
    });

    Route::prefix('jointer')->group(function () {
      Route::get('list', 'JointerController@jointer_view');
    });
  });

  Route::group(['middleware' => 'check_role:waspang_jointer,superadmin,teknisi,admin'], function () {
    Route::prefix('jointer')->group(function () {
      Route::get('list_rfc_teknisi', 'JointerController@list_rfc_teknisi');
      Route::get('request_stok_material', 'JointerController@request_stok_material');
      Route::post('request_stok_material', 'JointerController@submit_request_stok_material');

      Route::get('upload_reservasi/{id}', 'JointerController@upload_reservasi');
      Route::post('upload_reservasi/{id}', 'JointerController@save_reservasi');
    });
  });

  Route::group(['middleware' => 'check_role:all'], function () {
    //teknisi
    Route::get('/laporan/{id}', 'TeknisiController@form_laporan');
    Route::post('/laporan/{id}', 'TeknisiController@save_laporan')->name('laporanTeknisi_Save');
  });
  Route::group(['middleware' => 'check_role:teknisi,superadmin'], function () {

    Route::get('/grabImon/{ex_id}', 'GrabController@grab_imon');
    Route::get('/teknisi/find/rfc', 'TeknisiController@find_base_rfc');
    Route::get('/teknisi/find/designator', 'TeknisiController@find_base_designator');
    Route::get('/teknisi/rfc/item', 'TeknisiController@rfc_search');
    Route::get('/outside/jointer/pt2', 'OutsideController@jointer_pt2');
    Route::get('/outside/jointer_odc/{jenis}/{datel}/{odc}', 'OutsideController@jointer_pt2_list_odc');
    Route::get('/outside/jointer_list/{jenis}/{datel}/{sto}/{odc}', 'OutsideController@jointer_pt2_list');
    Route::get('/outside/pickup_jointer/{jenis}/{id_outside}', 'OutsideController@pickup_jointer');

    Route::prefix('jointer')->group(function () {
      //PARTY
      Route::get('/tim_party', 'JointerController@tim_party');
      Route::get('/create_party', 'JointerController@create_party');
      Route::post('/create_party', 'JointerController@save_party');

      Route::get('/sync_regu', 'JointerController@sync_regu');

      Route::get('/edit_party', 'JointerController@edit_party');
      Route::post('/edit_party', 'JointerController@update_party');

      Route::get('/join_party/{nik_leader}', 'JointerController@join_party');
      Route::get('/batal_gabung/{nik}', 'JointerController@abort_join');
      Route::get('/approve/{nik}', 'JointerController@approve_join');
      Route::get('/unit_join/{jenis}', 'JointerController@unit_join');

      Route::get('/dismiss_party', 'JointerController@dismiss_party');
      Route::get('/out_team', 'JointerController@out_party');
    });
  });

  Route::group(['middleware' => 'check_role:monitor,superadmin,monitor_marina'], function () {
    Route::get('/reading_kmz', 'MonitorController@read_kmz');

    Route::prefix('monitor')->group(function () {
      //geo
      Route::get('geo', 'ReportController@geo');
      Route::get('geo/find/lat{x}/long{y}', 'ReportController@geo_find');

      //regu
      Route::get('my_team', 'MonitorController@my_team');

      //ranked
      Route::get('ranked/{tgl}', 'ReportController@getRanked');
      Route::get('ranked/search/{month}/{month2}', 'ReportController@getRanked_search')->name('RA_search');
    });
  });
});
